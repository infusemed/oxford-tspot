//
//  PDFVideoAnnotation.h
//  BalloonsPlusTesting
//
//  Created by Kevin Ray on 4/17/13.
//  Copyright (c) 2013 Infuse Medical. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface PDFVideoAnnotation : NSObject{
    CGRect pdfRectangle;
}

@property (readonly, assign) CGRect pdfRectangle;
@property (copy) NSString* stringPath;
@property (copy) NSString* stringName;
@property (copy) NSString* stringThumbPath;

- (id)initWithPDFDictionary:(CGPDFDictionaryRef)newAnnotationDictionary videoName:(NSString*)videoName videoPath:(NSString*)videoPath;

@end
