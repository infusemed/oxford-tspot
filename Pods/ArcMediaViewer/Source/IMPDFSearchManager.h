//
//  IMPDFSearchManager.h
//  ARCMediaViewer
//
//  Created by Jeff Spaulding on 3/15/16.
//  Copyright © 2016 Infuse Medical. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "MFTextItem.h"

@interface IMPDFSearchManager : NSObject
@property (nonatomic, assign) int currentIndex;
@property (nonatomic, readonly) NSUInteger numberOfMatches;
-(id)initWithFilePath:(NSString*)filePath;
-(MFTextItem*)currentItem;
-(NSArray<MFTextItem*>*)results;
-(NSArray<MFTextItem*>*)resultsForPage:(int)pageIndex;
-(BOOL)setSearchString:(NSString*)searchString;
@end
