//
//  ICloudArchiver.h
//  TestICloud
//
//  Created by Jeff Spaulding on 3/8/16.
//  Copyright © 2016 Jeff Spaulding. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "AlertControllerPresenterDelegate.h"
@interface ICloudArchiver : NSObject
@property (nonatomic, assign, readonly) BOOL useICloud;
-(id)initWithDelegate:(id<AlertControllerPresenterDelegate>)delegate AppKey:(NSString*)appKey;
-(void)saveFormData;
-(void)restoreSavedFormData;
@end
