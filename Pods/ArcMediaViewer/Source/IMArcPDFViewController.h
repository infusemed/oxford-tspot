//
//  IMArcPDFViewController.h
//  ArcMediaViewer
//
//  Created by Infuse Medical on 2/14/14.
//  Copyright (c) 2014 Infuse Medical. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <AVFoundation/AVFoundation.h>
#import "IMArcMediaViewerChild.h"
#import "PDFTiledView.h"
#import "PDFScrollView.h"
#import "PDFThumbnailTableView.h"
#import "PDFThumbnailViewer.h"
#import <MessageUI/MessageUI.h>
#import <MobileCoreServices/MobileCoreServices.h>
#import "UIImage+FixOrientation.h"
#import "PDFUploadInfo.h"
#import "MFTextItem.h"
#import "IMPDFSearchManager.h"

@protocol PDFViewControllerDelegate <NSObject>
-(void) documentIsForm;
-(void) emailPDFForm;
@end


@interface IMArcPDFViewController : IMArcMediaViewerChild
<
    MFMailComposeViewControllerDelegate,
    UITextFieldDelegate,
    UIPopoverControllerDelegate,
    PDFScrollViewDelegate,
    UIScrollViewDelegate,
    PDFThumbnailTableViewDelegate,
    PDFThumbnailViewerDelegate,
    UIAlertViewDelegate,
    UITableViewDelegate,
    UITableViewDataSource,
    UIImagePickerControllerDelegate,
    UINavigationControllerDelegate
>
{
    NSString* _stringDocumentPath;
	NSMutableArray* _arrRecycledPages;
	NSMutableArray* _arrVisiblePages;
    NSMutableArray* _arrThumnailBtns;
	NSMutableArray* _arrPageQue;
    NSMutableDictionary* _dictSavedInfo;
    
	CGPDFDocumentRef _cgpdfDocumentRef;
    
    CGRect _rectCurrent;
    CGPoint _pointPrev;
    size_t _pageCount;
    
	UIScrollView* _scrollViewDocument;
	UIImageView* _titleBar;
	UIInterfaceOrientation _newOrientation;
    UIButton* _thumbnailBtn;
	UILabel* _labelPageNumber;
	UILabel* _labelRenderingFromsForEmail;
    UIPopoverController* _popoverControllerLoadForm;
    UIPopoverController* _popOverController;
    UIActivityIndicatorView* _spinningWheel;
    NSString * _stringFormName;
    UITextField* _textFieldSave;
    UIAlertView* _alertViewSave;
    NSString * _stringTempFormName;
    
    NSString * _preRotationFormName;
    UIAlertView* _alertViewOverwrite;
    BOOL _shouldUploadFile;
    
    PDFThumbnailViewer* _thumbnailViewer;
    PDFThumbnailTableView* _thumbnailTable;
    
    size_t _intPageCount;
    size_t _intCurrentPage;
    int _intThumnailTableViewHeight;
    int _intNextThumbnailToLoad;
    int _intCurrentPageRenderingForEmailing;
    
    BOOL _boolSelfWillClose;
    BOOL _boolIsForm;
    BOOL _boolCanLoadNextPage;
    BOOL _boolIsRotating;
    BOOL _boolThumbnailViewShowing;
    
    NSMutableArray * _fieldNamesArray;
    NSMutableArray * _fieldTypeArray;
    
}
@property (nonatomic, strong)IMPDFSearchManager* searchDocument;
@property (nonatomic, strong)SignatureStyler styleSignature;
@property (assign) UIColor* colorBetweenPages;
@property (nonatomic, strong) UIScrollView* scrollViewDocument;
@property (nonatomic, strong) UILabel * labelPageNumber;
@property (nonatomic) BOOL formEnabled;


-(void)loadPDFFormWithName:(NSString*)formName;
@end
