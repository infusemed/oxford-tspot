//
//  ICloudArchiver.m
//  TestICloud
//
//  Created by Jeff Spaulding on 3/8/16.
//  Copyright © 2016 Jeff Spaulding. All rights reserved.
//
#import <UIKit/UIKit.h>
#import "ICloudArchiver.h"
#import "ICloudFormsDocument.h"

@interface ICloudArchiver(){
    id<AlertControllerPresenterDelegate> _delegate;
    ICloudFormsDocument * _document;
    NSString* _appKey;
    BOOL _shouldRestore;
}
@end

@implementation ICloudArchiver

-(id)initWithDelegate:(id<AlertControllerPresenterDelegate>)delegate AppKey:(NSString *)appKey{
    self = [super init];
    if (self) {
        _delegate = delegate;
        _appKey = appKey;
        _useICloud = false;
        _shouldRestore = false;
        
        [[NSNotificationCenter defaultCenter]
         addObserver: self
         selector: @selector (iCloudAccountAvailabilityChanged:)
         name: NSUbiquityIdentityDidChangeNotification
         object: nil];
        
        NSFileManager* fileManager = [NSFileManager defaultManager];
        id currentiCloudToken = fileManager.ubiquityIdentityToken;
        if (currentiCloudToken) {
            [self initICloud];
        }
    }
    return self;
}

-(void)initICloud{
    NSFileManager* fileManager = [NSFileManager defaultManager];
    id currentiCloudToken = fileManager.ubiquityIdentityToken;
    if (currentiCloudToken) {
        BOOL hasMadeChoice = [[NSUserDefaults standardUserDefaults] boolForKey:@"hasMadeICloudChoice"];
        if(hasMadeChoice){
            _useICloud =[[NSUserDefaults standardUserDefaults] boolForKey:@"useICloud"];
            if(_useICloud){
                NSData *newTokenData = [NSKeyedArchiver archivedDataWithRootObject: currentiCloudToken];
                [[NSUserDefaults standardUserDefaults] setObject: newTokenData forKey: [NSString stringWithFormat:@"com.apple.%@.UbiquityIdentityToken",_appKey]];
                dispatch_async (dispatch_get_global_queue (DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^(void) {
                    NSURL* myContainer = [[NSFileManager defaultManager] URLForUbiquityContainerIdentifier: nil];
                    if (myContainer != nil) {
                        _document = [[ICloudFormsDocument alloc] initWithFileName:_appKey];
                        [_document openWithCompletionHandler:^(BOOL success) {
                            if(_shouldRestore){
                                [_document restoreFormDataIfNeeded];
                            }
                        }];
                    }
                });
            }else{
            }
        }else{
            UIAlertController* ac = [UIAlertController alertControllerWithTitle:@"Choose Storage Option" message:@"Allow form data to be backed up to iCloud?" preferredStyle:UIAlertControllerStyleAlert];
            [ac addAction:[UIAlertAction actionWithTitle:@"Local Only" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
                [[NSUserDefaults standardUserDefaults] setBool:YES forKey:@"hasMadeICloudChoice"];
                [[NSUserDefaults standardUserDefaults] setBool:NO forKey:@"useICloud"];
            }]];
            [ac addAction:[UIAlertAction actionWithTitle:@"Use iCloud" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
                [[NSUserDefaults standardUserDefaults] setBool:YES forKey:@"hasMadeICloudChoice"];
                [[NSUserDefaults standardUserDefaults] setBool:YES forKey:@"useICloud"];
                [self initICloud];
            }]];
            [_delegate presentAlertController:ac];
        }

    } else {
        [[NSUserDefaults standardUserDefaults] removeObjectForKey: [NSString stringWithFormat:@"com.apple.%@.UbiquityIdentityToken",_appKey]];
        
    }
    
    
}
-(void)saveFormData{
    if(_useICloud){
        [_document saveWithCompletionHandler:^(BOOL success) {
            
        }];
    }
}
-(void)restoreSavedFormData{
    _shouldRestore = true;
    if(_useICloud){
        [_document restoreFormDataIfNeeded];
    }
}
-(void)iCloudAccountAvailabilityChanged:(NSNotification*)notification{
    [self initICloud];
}
@end
