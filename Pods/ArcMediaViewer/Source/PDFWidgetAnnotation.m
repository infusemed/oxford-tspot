//
//  PDFWidgetAnnotation.m
//  BalloonsPlus
//
//  Created by Kevin Ray on 8/8/11.
//  Copyright 2011 Infuse Medical. All rights reserved.
//

#import "PDFWidgetAnnotation.h"


@implementation PDFWidgetAnnotation

@synthesize pdfRectangle;
@synthesize auxInfo;
@synthesize tempDictionary;
@synthesize widgetType = _widgetType;
@synthesize widgetTextAlignment;
@synthesize fontSize, linkTarget, pageNum;
@synthesize defaultText;

- (id)initWithPDFDictionary:(CGPDFDictionaryRef)newAnnotationDictionary PDFPageRef:(CGPDFPageRef)pageRef pageNum:(int)pageNumber
{
    self = [super init];
    if (self) {
        self->annotationDictionary = newAnnotationDictionary;
        
        //selfClass = self;
        pageNum = pageNumber;
        
        //** For extracting what is in the pdf **//
        //auxInfo = [[NSMutableDictionary alloc] init];
        //tempDictionary = auxInfo;
        //[self extractPDFDictionary];
        
        
        //NSLog(@"%@", tempDictionary);
        
        [self getWidgetType:pageRef];
        
        // Normalize and cache the annotation rect definition for faster hit testing.
        CGPDFArrayRef rectArray = NULL;
        CGPDFDictionaryGetArray(annotationDictionary, "Rect", &rectArray);
        if (rectArray != NULL) {
            CGPDFReal llx = 0;
            CGPDFArrayGetNumber(rectArray, 0, &llx);
            CGPDFReal lly = 0;
            CGPDFArrayGetNumber(rectArray, 1, &lly);
            CGPDFReal urx = 0;
            CGPDFArrayGetNumber(rectArray, 2, &urx);
            CGPDFReal ury = 0;
            CGPDFArrayGetNumber(rectArray, 3, &ury);
            
            if (llx > urx) {
                CGPDFReal temp = llx;
                llx = urx;
                urx = temp;
            }
            if (lly > ury) {
                CGPDFReal temp = lly;
                lly = ury;
                ury = temp;
            }
            
            CGRect pageRect = CGPDFPageGetBoxRect(pageRef, kCGPDFCropBox);
            
            pdfRectangle = CGRectMake(llx, lly-pageRect.origin.y, urx - llx, ury - lly);
        }
        
        
    }
    
    return self;
}

- (void)dealloc {
    self->annotationDictionary = NULL;
}

- (BOOL)hitTest:(CGPoint)point {
    if ((pdfRectangle.origin.x <= point.x) &&
        (pdfRectangle.origin.y <= point.y) &&
        (point.x <= pdfRectangle.origin.x + pdfRectangle.size.width) &&
        (point.y <= pdfRectangle.origin.y + pdfRectangle.size.height)) {
        return YES;
    } else {
        return NO;
    }
}
-(void) getLinkFromBtn:(CGPDFPageRef)pageRef
{
    CGPDFDocumentRef document = CGPDFPageGetDocument(pageRef);
    CGPDFArrayRef destArray = NULL;
    CGPDFDictionaryRef actionDictionary = NULL;
    if (CGPDFDictionaryGetDictionary(annotationDictionary, "A", &actionDictionary)) {
        const char* actionType;
        if (CGPDFDictionaryGetName(actionDictionary, "N", &actionType)) {
            
            if (strcmp(actionType, "PrevPage") == 0) {
                linkTarget = [[NSNumber alloc] initWithInt:pageNum-1]; 
            }
            else if (strcmp(actionType, "NextPage") == 0) {
                linkTarget = [[NSNumber alloc] initWithInt:pageNum+1]; 
            }
        }
        else if (CGPDFDictionaryGetName(actionDictionary, "S", &actionType)) {
            if (strcmp(actionType, "GoTo") == 0) {
                CGPDFDictionaryGetArray(actionDictionary, "D", &destArray);
            }
            /*
             CGPDFStringRef nameRef = NULL;
             if (!destArray) {
             CGPDFDictionaryGetString(actionDictionary, "D", &nameRef);
             const char *name = (const char *)CGPDFStringGetBytePtr(nameRef);
             NSString* nameLink = [[NSString alloc] initWithCString: name encoding: NSASCIIStringEncoding];
             NSString* hello = nameLink;
             }*/
            CGPDFStringRef destName;
            if (CGPDFDictionaryGetString(actionDictionary, "D", &destName)) {
                // Traverse the Dests tree to locate the destination array.
                CGPDFDictionaryRef catalogDictionary = CGPDFDocumentGetCatalog(document);
                CGPDFDictionaryRef namesDictionary = NULL;
                if (CGPDFDictionaryGetDictionary(catalogDictionary, "Names", &namesDictionary)) {
                    CGPDFDictionaryRef destsDictionary = NULL;
                    if (CGPDFDictionaryGetDictionary(namesDictionary, "Dests", &destsDictionary)) {
                        const char *destinationName = (const char *)CGPDFStringGetBytePtr(destName);
                        destArray = [self findDestinationByName: destinationName inDestsTree: destsDictionary];
                    }
                }
            }
            if (strcmp(actionType, "URI") == 0) {
                CGPDFStringRef uriRef = NULL;
                if (CGPDFDictionaryGetString(actionDictionary, "URI", &uriRef)) {
                    const char *uri = (const char *)CGPDFStringGetBytePtr(uriRef);
                    linkTarget = [[NSString alloc] initWithCString: uri encoding: NSASCIIStringEncoding];
                }
            }
        }
        
    }
    
    if (destArray != NULL) {
        
        int targetPageNumber = 0;
        // First entry in the array is the page the links points to.
        CGPDFDictionaryRef pageDictionaryFromDestArray = NULL;
        if (CGPDFArrayGetDictionary(destArray, 0, &pageDictionaryFromDestArray)) {
            size_t documentPageCount = CGPDFDocumentGetNumberOfPages(document);
            for (int i = 1; i <= documentPageCount; i++) {
                CGPDFPageRef page = CGPDFDocumentGetPage(document, i);
                CGPDFDictionaryRef pageDictionaryFromPage = CGPDFPageGetDictionary(page);
                if (pageDictionaryFromPage == pageDictionaryFromDestArray) {
                    targetPageNumber = i;
                    break;
                }
            }
        } else {
            // Some PDF generators use incorrectly the page number as the first element of the array 
            // instead of a reference to the actual page.
            CGPDFInteger pageNumber = 0;
            if (CGPDFArrayGetInteger(destArray, 0, &pageNumber)) {
                targetPageNumber = (int)(pageNumber + 1);
            }
        }
        
        if (targetPageNumber > 0) {
            linkTarget = [[NSNumber alloc] initWithInt: targetPageNumber]; 
        }
    }
    
    if (linkTarget) {
        _widgetType = WidgetTypeLinkBtn;
    }
    else
    {
        _widgetType = WidgetTypeCheckbox;
    }
    
    

}
- (void)getPropertiesOfTextField:(CGPDFDictionaryRef)widgetDictionary
{
    _widgetType = WidgetTypeTextField;
    
    //get font size
    CGPDFStringRef fontRef;
    if(CGPDFDictionaryGetString(widgetDictionary, "DA", &fontRef))
    {
        NSString* fontString = (NSString *)CFBridgingRelease(CGPDFStringCopyTextString(fontRef));
        NSArray* fontStringSplit = [fontString componentsSeparatedByString:@" "];
        fontSize =  [[fontStringSplit objectAtIndex:1] intValue];
    }
    
    //get default text
    CGPDFStringRef defaultTextRef;
    if(CGPDFDictionaryGetString(widgetDictionary, "V", &defaultTextRef))
    {
        defaultText = (NSString *)CFBridgingRelease(CGPDFStringCopyTextString(defaultTextRef));
    }
    
    //check if it is a textview or a textfield
    CGPDFInteger objectInteger;
    if(CGPDFDictionaryGetInteger(widgetDictionary, "Ff", &objectInteger))
    {
        if(objectInteger & (1<<12)){
            _widgetType = WidgetTypeTextView;
        }
    }
    
    //get text alignment
    CGPDFInteger textAlignmentRef;
    if(CGPDFDictionaryGetInteger(annotationDictionary, "Q", &textAlignmentRef))
    {
        if (textAlignmentRef == 1)
            widgetTextAlignment = WidgetTextAlignmentCenter;
        else if (textAlignmentRef == 2)
            widgetTextAlignment = WidgetTextAlignmentRight;
        
    }

}
- (void)getWidgetType:(CGPDFPageRef)pageRef
{
    CGPDFDictionaryRef widgetDictionary = annotationDictionary;
    CGPDFDictionaryRef parent = NULL;
    
    CGPDFStringRef fieldName;
    _fieldName = @"";
    if(CGPDFDictionaryGetString(annotationDictionary, "T", &fieldName)){
        _fieldName = (NSString *)CFBridgingRelease(CGPDFStringCopyTextString(fieldName));
    }
    
    const char *widgetNameType;
    if(CGPDFDictionaryGetName(widgetDictionary, "FT", &widgetNameType))
    {

        if (strcmp(widgetNameType, "Tx") == 0) {
            [self getPropertiesOfTextField:widgetDictionary];
        }
        
        else if (strcmp(widgetNameType, "Btn") == 0) {
            [self getLinkFromBtn:pageRef];
            CGPDFInteger Ff;
            if(CGPDFDictionaryGetInteger(annotationDictionary, "Ff", &Ff)){
                if (Ff == 2) {
                    _fieldName = @"required checklist";
                }
            }
        }
        else if (strcmp(widgetNameType, "Sig") == 0) {
            _widgetType = WidgetTypeSignature;
        }
    }
    else if (CGPDFDictionaryGetDictionary(annotationDictionary, "Parent", &parent)) 
    {
        const char *widgetNameType;
        
        if(CGPDFDictionaryGetName(parent, "FT", &widgetNameType))
        {
            if([_fieldName isEqualToString:@""]){
                if(CGPDFDictionaryGetString(parent, "T", &fieldName)){
                    _fieldName = (NSString *)CFBridgingRelease(CGPDFStringCopyTextString(fieldName));
                }
            }
            if (strcmp(widgetNameType, "Tx") == 0) {
                [self getPropertiesOfTextField:widgetDictionary];
            }
            else if (strcmp(widgetNameType, "Btn") == 0)
            {
                CGPDFDictionaryRef actionDictionary;
                if (CGPDFDictionaryGetDictionary(annotationDictionary, "A", &actionDictionary)) {
                    [self getLinkFromBtn:pageRef];
                }else{
                    CGPDFStringRef parentName;
                    //CGPDFDictionaryApplyFunction(annotationDictionary, enumeratesDictionaryValues, NULL);
                    
                    const char * dv;
                    if(CGPDFDictionaryGetName(parent, "DV", &dv)){
                        _hasDefaultCheckValue = YES;
                    }
                    const char * value;
                    if(CGPDFDictionaryGetName(annotationDictionary, "AS", &value)){
                        if (_hasDefaultCheckValue) {
                            if (strcmp(value,dv)==0) {
                                _checkedByDefault = YES;
                            }
                        }
                        _fieldValue = [NSString stringWithFormat:@"%s",value];
                    }
                    
                    
                    
                    if(CGPDFDictionaryGetString(parent, "T",&parentName)){
                        
                        
                        NSLog(@"\n\n\n\n\n\n==============================================");
                        //CGPDFDictionaryApplyFunction(annotationDictionary, enumeratesDictionaryValues,nil);
                        
                        _widgetType = WidgetTypeRadioBtn;
                        _radioButtonParentName = [NSString stringWithFormat:@"%@",CGPDFStringCopyTextString(parentName)];
                        _fieldName = _radioButtonParentName;
                    }
                }
            }
        }
    }

    /*
     NSMutableString *str = @"";
     for(NSInteger numberCopy = btnType; numberCopy > 0; numberCopy >>= 1)
     {
     // Prepend "0" or "1", depending on the bit
     [str insertString:((numberCopy & 1) ? @"1" : @"0") atIndex:0];
     }
     if ([str characterAtIndex:11] == '1')
     _widgetType = WidgetTypeCheckbox;
     else
     _widgetType = WidgetTypeCheckbox;
     */
}
- (CGPDFArrayRef)findDestinationByName:(const char *)destinationName inDestsTree:(CGPDFDictionaryRef)node {
    CGPDFArrayRef destinationArray = NULL;
    CGPDFArrayRef limitsArray = NULL;
    if (CGPDFDictionaryGetArray(node, "Limits", &limitsArray)) {
        CGPDFStringRef lowerLimit = NULL;
        CGPDFStringRef upperLimit = NULL;
        if (CGPDFArrayGetString(limitsArray, 0, &lowerLimit)) {
            if (CGPDFArrayGetString(limitsArray, 1, &upperLimit)) {
                const unsigned char *ll = CGPDFStringGetBytePtr(lowerLimit);
                const unsigned char *ul = CGPDFStringGetBytePtr(upperLimit);
                if ((strcmp(destinationName, (const char*)ll) < 0) ||
                    (strcmp(destinationName, (const char*)ul) > 0)) {
                    return NULL;
                }
            }
        }
    }
    
    CGPDFArrayRef namesArray = NULL;
    if (CGPDFDictionaryGetArray(node, "Names", &namesArray)) {
        size_t namesCount = CGPDFArrayGetCount(namesArray);
        for (int i = 0; i < namesCount; i = i + 2) {
            CGPDFStringRef destName;
            if (CGPDFArrayGetString(namesArray, i, &destName)) {
                const unsigned char *dn = CGPDFStringGetBytePtr(destName);
                if (strcmp((const char*)dn, destinationName) == 0) {
                    CGPDFDictionaryRef destinationDictionary = NULL;
                    if (CGPDFArrayGetDictionary(namesArray, i + 1, &destinationDictionary)) {
                        if (CGPDFDictionaryGetArray(destinationDictionary, "D", &destinationArray)) {
                            return destinationArray;
                        }
                    }
                    if (!destinationDictionary) {
                        if (CGPDFArrayGetArray(namesArray, i + 1, &destinationArray)) {
                            return destinationArray;
                        }
                    }
                    
                }
            }
        }
    }
    
    CGPDFArrayRef kidsArray = NULL;
    if (CGPDFDictionaryGetArray(node, "Kids", &kidsArray)) {
        size_t kidsCount = CGPDFArrayGetCount(kidsArray);
        for (int i = 0; i < kidsCount; i++) {
            CGPDFDictionaryRef kidNode = NULL;
            if (CGPDFArrayGetDictionary(kidsArray, i, &kidNode)) {
                destinationArray = [self findDestinationByName: destinationName inDestsTree: kidNode];
                if (destinationArray != NULL) {
                    return destinationArray;
                }
            }
        }
    }
    
    return NULL;
}
// temporary C function to print out keys
void printPDFKeys(const char *key, CGPDFObjectRef ob, void *info) {
    NSLog(@"key = %s", key);
}

void enumeratesDictionaryValues(const char *key, CGPDFObjectRef object, void *info)
{
    NSLog(@"---------- key ------ %s ---------------", key);
    CGPDFObjectType type = CGPDFObjectGetType(object);
    switch (type)
    {
        case kCGPDFObjectTypeName:
        {
            const char * objectString;
            if (CGPDFObjectGetValue(object, kCGPDFObjectTypeName, &objectString))
            {
                NSLog(@"(name) %s = %s", key, objectString);
            }
        }
            break;
        case kCGPDFObjectTypeString:
        {
            CGPDFStringRef objectString;
            if (CGPDFObjectGetValue(object, kCGPDFObjectTypeString, &objectString))
            {
                NSString *tempStr = (NSString *)CFBridgingRelease(CGPDFStringCopyTextString(objectString));
                NSLog(@"(string) %s = %@", key, tempStr);
            }
        }
            break;
        case kCGPDFObjectTypeInteger:
        {
            CGPDFInteger objectInteger;
            if (CGPDFObjectGetValue(object, kCGPDFObjectTypeInteger, &objectInteger))
            {
                NSLog(@"(int) %s = %ld", key, objectInteger);
            }
        }
            break;
        case kCGPDFObjectTypeBoolean:
        {
            CGPDFBoolean objectBool;
            if (CGPDFObjectGetValue(object, kCGPDFObjectTypeBoolean, &objectBool))
            {
                NSLog(@"(boolean) %s = %d", key, objectBool);
            }
        }
            break;
        case kCGPDFObjectTypeArray:
        {
            CGPDFArrayRef objectArray;
            if (CGPDFObjectGetValue(object, kCGPDFObjectTypeArray, &objectArray))
            {
                NSLog(@"[");
                enumeratesArrayValues(objectArray);
                NSLog(@"]");
            }
        }
        case kCGPDFObjectTypeDictionary:
        {
            CGPDFDictionaryRef objectDict;
            if (CGPDFObjectGetValue(object, kCGPDFObjectTypeDictionary, &objectDict)) {
                
                NSLog(@"{");
                CGPDFDictionaryApplyFunction(objectDict, enumeratesDictionaryValues, NULL);
                NSLog(@"}");
            }
        }
            break;
        default:
            break;
    }
    
}

void enumeratesArrayValues(CGPDFArrayRef arr)
{
    for (int i = 0; i < CGPDFArrayGetCount(arr); i++)
    {
        CGPDFObjectRef object;
        CGPDFArrayGetObject(arr, i, &object);
        CGPDFObjectType type = CGPDFObjectGetType(object);
        switch (type)
        {
            case kCGPDFObjectTypeString:
            {
                CGPDFStringRef objectString;
                if (CGPDFObjectGetValue(object, kCGPDFObjectTypeString, &objectString))
                {
                    NSString *tempStr = (NSString *)CFBridgingRelease(CGPDFStringCopyTextString(objectString));
                    NSLog(@"(string) = %@", tempStr);
                }
            }
            case kCGPDFObjectTypeInteger:
            {
                CGPDFInteger objectInteger;
                if (CGPDFObjectGetValue(object, kCGPDFObjectTypeInteger, &objectInteger))
                {
                    NSLog(@"(int) = %ld", objectInteger);
                }
            }
            case kCGPDFObjectTypeBoolean:
            {
                CGPDFBoolean objectBool;
                if (CGPDFObjectGetValue(object, kCGPDFObjectTypeBoolean, &objectBool))
                {
                    NSLog(@"(boolean) = %d", objectBool);
                }
            }
            case kCGPDFObjectTypeArray:
            {
                CGPDFArrayRef objectArray;
                if (CGPDFObjectGetValue(object, kCGPDFObjectTypeArray, &objectArray))
                {
                    enumeratesArrayValues(objectArray);
                }
            }
            case kCGPDFObjectTypeDictionary:{
                NSLog(@"Dictionary");
                //
                //                CGPDFDictionaryRef objectDict;
                //                if (CGPDFObjectGetValue(object, kCGPDFObjectTypeDictionary, &objectDict)) {
                //
                //                    NSLog(@"{");
                //                    CGPDFDictionaryApplyFunction(objectDict, enumeratesDictionaryValues, NULL);
                //                    NSLog(@"}");
                //                }
            }
            default:
                break;
        }
    }
}





@end
