//
//  ImportedImageView.m
//  BalloonsPlus
//
//  Created by Kevin Ray on 7/3/12.
//  Copyright (c) 2012 Infuse Medical. All rights reserved.
//

#import "ImportedImageView.h"

@implementation ImportedImageView

@synthesize closeBtn;
@synthesize imageReleased;
@synthesize imageName;

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
    }
    return self;
}

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect
{
    // Drawing code
}
*/

@end
