//
//  TiledPDFView+Links.m
//  LinksNavigation
//
//  Created by Sorin Nistor on 6/21/11.
//  Copyright 2011 iPDFdev.com. All rights reserved.
//

#import "PDFInteractiveOverlay+Videos.h"
#import "PDFVideoAnnotation.h"
#import <MediaPlayer/MediaPlayer.h>
#import <AVFoundation/AVFoundation.h>

@implementation PDFInteractiveOverlay (Videos)

- (void) drawVideos
{
	pageRenderRect = self.bounds;//[PDFPageRenderer renderPage: pdfPage inContext: context inRectangle: self.bounds];
    
    for (int i = 0; i < [pageVideos count]; i++) {
        PDFVideoAnnotation *videoAnnotation = [pageVideos objectAtIndex: i];
        CGPoint pt1 = [self convertPDFPointToViewPointForVideos: videoAnnotation.pdfRectangle.origin];
        CGPoint pt2 = CGPointMake(videoAnnotation.pdfRectangle.origin.x + videoAnnotation.pdfRectangle.size.width, 
                                  videoAnnotation.pdfRectangle.origin.y + videoAnnotation.pdfRectangle.size.height);
        pt2 = [self convertPDFPointToViewPointForVideos: pt2];
        
        UIButton* button = [UIButton buttonWithType:UIButtonTypeCustom];
        button.backgroundColor = [UIColor blackColor];
        button.frame = CGRectMake(pt1.x, pt1.y, pt2.x - pt1.x, pt2.y - pt1.y);
        [button addTarget:self action:@selector(videoTouched:) forControlEvents:UIControlEventTouchUpInside];
        button.tag = i;
        [self addSubview:button];
        
        
        // add the play image
        CGRect rect = button.frame;
        UIGraphicsBeginImageContext(rect.size);
        CGContextRef context = UIGraphicsGetCurrentContext();
        CGContextSetRGBStrokeColor(context, 1.0f, 1.0f, 1.0f, 1.0f);
        float lineWidth = 8.0;
        CGContextSetLineWidth(context, lineWidth);
        //draw thumb image
        UIImage* thumbImage =[UIImage imageWithContentsOfFile:videoAnnotation.stringThumbPath];
        if (thumbImage)
            [thumbImage drawInRect:CGRectMake(0, 0, rect.size.width, rect.size.height)];
        //draw circle
        int radius = (rect.size.width < rect.size.height) ? rect.size.width : rect.size.height;
        radius  *= .8;
        CGRect circleRect = CGRectMake((rect.size.width-radius)/2, (rect.size.height-radius)/2, radius, radius);
        CGContextSetFillColorWithColor(context, [[UIColor colorWithWhite:0.0 alpha:.5] CGColor]);
        CGContextFillEllipseInRect(context, circleRect);
        CGContextStrokeEllipseInRect(context, circleRect);
        //draw triangle
        CGContextSetFillColorWithColor(context, [[UIColor whiteColor] CGColor]);
        CGContextBeginPath(context);
        CGContextMoveToPoint   (context, CGRectGetMidX(circleRect)-(circleRect.size.width * 0.2), CGRectGetMinY(circleRect)+(circleRect.size.height * 0.2));  // top left
        CGContextAddLineToPoint(context, CGRectGetMidX(circleRect)+(circleRect.size.width * 0.35), CGRectGetMidY(circleRect));  // mid right
        CGContextAddLineToPoint(context, CGRectGetMidX(circleRect)-(circleRect.size.width * 0.2), CGRectGetMaxY(circleRect)-(circleRect.size.height * 0.2));  // bottom left
        CGContextClosePath(context);
        CGContextFillPath(context);
        //get image
        UIImage *img = UIGraphicsGetImageFromCurrentImageContext();
        UIGraphicsEndImageContext();
        
        [button setImage:img forState:UIControlStateNormal];
        
    }
}

-(void) videoTouched:(UIButton*)btn
{
    
    [self.delegate keepTitleAlpha];
    PDFVideoAnnotation *video = [pageVideos objectAtIndex:btn.tag];
    if (![[NSFileManager defaultManager] fileExistsAtPath:video.stringPath])
        return;

    if (_videoPlayer) {
        [_videoPlayer.view removeFromSuperview];
        _videoPlayer = nil;
    }
    _videoPlayer = [[MPMoviePlayerController alloc] init];
    
    
    CGPoint pt1 = [self convertPDFPointToViewPointForVideos: video.pdfRectangle.origin];
    CGPoint pt2 = CGPointMake(
                              video.pdfRectangle.origin.x + video.pdfRectangle.size.width,
                              video.pdfRectangle.origin.y + video.pdfRectangle.size.height);
    pt2 = [self convertPDFPointToViewPointForVideos: pt2];
    
    [[_videoPlayer view] setFrame:CGRectMake(pt1.x, pt1.y, pt2.x - pt1.x, pt2.y - pt1.y)];
    
     [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(moviePlayBackDidFinish:)
                                             name:MPMoviePlayerPlaybackDidFinishNotification
                                             object:_videoPlayer];
    
    NSURL* movieURL = [NSURL fileURLWithPath:video.stringPath];
    [_videoPlayer setContentURL:movieURL];
    [_videoPlayer.backgroundView setBackgroundColor:[UIColor blackColor]];
    [_videoPlayer.view setBackgroundColor:[UIColor blackColor]];
    //[_video setControlStyle:MPMovieControlStyleDefault];
    //if (loopVideo) {
    [_videoPlayer setRepeatMode:MPMovieRepeatModeOne];
    //}
    //else
    //  [_video setRepeatMode:MPMovieRepeatModeNone];
    
    //[video setScalingMode:MPMovieScalingModeFill];
    _videoPlayer.shouldAutoplay = TRUE;
    [self addSubview:_videoPlayer.view];
    [_videoPlayer setFullscreen:NO animated:NO];
    
   
}
- (void) moviePlayBackDidFinish:(NSNotification*)notification
{
	[[NSNotificationCenter defaultCenter] removeObserver:self
                                                    name:MPMoviePlayerPlaybackDidFinishNotification
                                                  object:_videoPlayer];
    [_videoPlayer.view removeFromSuperview];
    _videoPlayer = nil;
    
    
}

- (void)loadPageVideos:(CGPDFPageRef)page {
    CGPDFDictionaryRef pageDictionary = CGPDFPageGetDictionary(page);
    CGPDFArrayRef annotsArray = NULL;
    
    CGPDFDictionaryGetArray(pageDictionary, "Annots", &annotsArray);
    if (annotsArray != NULL) {
        size_t annotsCount = CGPDFArrayGetCount(annotsArray);
        
        for (int j = 0; j < annotsCount; j++) {
            CGPDFDictionaryRef annotationDictionary = NULL;            
            if (CGPDFArrayGetDictionary(annotsArray, j, &annotationDictionary)) {
                const char *annotationType;
                CGPDFDictionaryGetName(annotationDictionary, "Subtype", &annotationType);
                
               if (strcmp(annotationType, "RichMedia") == 0) {
                   
                   CGPDFDictionaryRef richMediaContentDictionary = NULL;
                   
                   if (CGPDFDictionaryGetDictionary(annotationDictionary,  "RichMediaContent", &richMediaContentDictionary)) {
                       
                       
                       CGPDFDictionaryRef assetsDictionary = NULL;
                       
                       if (CGPDFDictionaryGetDictionary(richMediaContentDictionary,  "Assets", &assetsDictionary)) {
                           
                           CGPDFArrayRef namesArray = NULL;
                           if (CGPDFDictionaryGetArray(assetsDictionary, "Names", &namesArray)) {
                               
                               size_t namesCount = CGPDFArrayGetCount(namesArray);
                               
                               for (int k = 0; k < namesCount; k+=2) {
                                   
                                   CGPDFStringRef videoName;
                                   if (CGPDFArrayGetString(namesArray, k, &videoName)) {
                                       NSString *fileName = (NSString *)CFBridgingRelease(CGPDFStringCopyTextString(videoName));
                                       
                                       NSArray* componentsOfName =[fileName componentsSeparatedByString:@"."];
                                       if ([componentsOfName count]> 1) {
                                           NSString* extention = [componentsOfName objectAtIndex:1];
                                           if([extention isEqualToString:@"mp4"] || [extention isEqualToString:@"m4v"] || [extention isEqualToString:@"mpeg"] || [extention isEqualToString:@"mov"]){
                                               NSString* videoPath = [NSTemporaryDirectory() stringByAppendingPathComponent:fileName];
                                               if (![[NSFileManager defaultManager] fileExistsAtPath:videoPath]){
                                                   
                                                   CGPDFDictionaryRef videoContentsDictionary = NULL;
                                                   if (CGPDFArrayGetDictionary(namesArray, k+1, &videoContentsDictionary)) {
                                                       CGPDFDictionaryRef efDictionary = NULL;
                                                       if (CGPDFDictionaryGetDictionary(videoContentsDictionary,  "EF", &efDictionary)) {
                                                           CGPDFStreamRef fDictionaryStream = NULL;
                                                           if (CGPDFDictionaryGetStream(efDictionary,  "F", &fDictionaryStream)) {
                                                               //CGPDFDictionaryRef fDictionary = CGPDFStreamGetDictionary(fDictionaryStream);
                                                               
                                                               
                                                               //CGPDFReal legnth = 0;
                                                               //CGPDFDictionaryGetNumber(fDictionary, "Length", &legnth);
                                                               
                                                               
                                                               CGPDFDataFormat format;
                                                               CFDataRef videoCFData = NULL;
                                                               
                                                               videoCFData = CGPDFStreamCopyData(fDictionaryStream, &format);
                                                               
                                                               NSData* videoNSData = (NSData*)CFBridgingRelease(videoCFData);
                                                               
                                                               //NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
                                                               //NSString *documentsDirectory = [paths objectAtIndex:0]; // Get documents directory
                                                               
                                                               [videoNSData writeToFile:videoPath atomically:TRUE];
                                                               
                                                               NSURL *videoURL = [NSURL fileURLWithPath:videoPath];
                                                               AVURLAsset *asset = [[AVURLAsset alloc] initWithURL:videoURL options:nil];
                                                               AVAssetImageGenerator *generate = [[AVAssetImageGenerator alloc] initWithAsset:asset];
                                                               
                                                               NSError *error = NULL;
                                                               CMTime time = CMTimeMake(1, 60);
                                                               
                                                               CGImageRef imgRef = [generate copyCGImageAtTime:time actualTime:NULL error:&error];
                                                               UIImage *thumbnail = [UIImage imageWithCGImage:imgRef];
                                                               
                                                               // Convert UIImage to JPEG
                                                               NSData *imgData = UIImageJPEGRepresentation(thumbnail, 1);
                                                               
                                                               // Identify the home directory and file name
                                                               NSString  *jpgPath = [NSTemporaryDirectory() stringByAppendingPathComponent:[NSString stringWithFormat:@"thumb%@.jpg",[componentsOfName objectAtIndex:0]]];
                                                               
                                                               // Write the file.  Choose YES atomically to enforce an all or none write. Use the NO flag if partially written files are okay which can occur in cases of corruption
                                                               [imgData writeToFile:jpgPath atomically:YES];
                                                               
                                                               
                                                               NSLog(@"video");
                                                           }
                                                       }
                                                   }
                                               }

                                               CGPDFDictionaryRef videoContentsDictionary = NULL;
                                               if (CGPDFArrayGetDictionary(namesArray, k+1, &videoContentsDictionary)) {
                                                   CGPDFDictionaryRef efDictionary = NULL;
                                                   if (CGPDFDictionaryGetDictionary(videoContentsDictionary,  "EF", &efDictionary)) {
                                                       CGPDFStreamRef fDictionaryStream = NULL;
                                                       if (CGPDFDictionaryGetStream(efDictionary,  "F", &fDictionaryStream)) {
                                                           //CGPDFDictionaryRef fDictionary = CGPDFStreamGetDictionary(fDictionaryStream);
                                                           
                                                           
                                                           NSString* videoPath = [NSTemporaryDirectory() stringByAppendingPathComponent:fileName];
                                                           
                                                           PDFVideoAnnotation *videoAnnotation = [[PDFVideoAnnotation alloc] initWithPDFDictionary:annotationDictionary videoName:[componentsOfName objectAtIndex:0] videoPath:videoPath];
                                                           [pageVideos addObject: videoAnnotation];
                                                           
                                                       }
                                                   }
                                               }
                                               break;
                                           }
                                       }
                                   }
                               }
                           }
                           /*
                           const char *subType;
                           CGPDFDictionaryGetName(richMediaContentDictionary, "Subtype", &subType);
                           
                           
                           if (strcmp(subType, "Video") == 0) {
                               
                               
                           }*/
                       }
                       
                   }
                }
            }
        }
    }
}

- (CGPoint)convertPDFPointToViewPointForVideos:(CGPoint)pdfPoint {
    CGPoint viewPoint = CGPointMake(0, 0);
    
    CGRect cropBox = CGPDFPageGetBoxRect(pdfPage, kCGPDFCropBox);
    
    int rotation = CGPDFPageGetRotationAngle(pdfPage);
    
    switch (rotation) {
        case 90:
        case -270:
            viewPoint.x = pageRenderRect.size.width * (pdfPoint.y - cropBox.origin.y) / cropBox.size.height;
            viewPoint.y = pageRenderRect.size.height * (pdfPoint.x - cropBox.origin.x) / cropBox.size.width;
            break;
        case 180:
        case -180:
            viewPoint.x = pageRenderRect.size.width * (cropBox.size.width - (pdfPoint.x - cropBox.origin.x)) / cropBox.size.width;
            viewPoint.y = pageRenderRect.size.height * (pdfPoint.y - cropBox.origin.y) / cropBox.size.height;
            break;
        case -90:
        case 270:
            viewPoint.x = pageRenderRect.size.width * (cropBox.size.height - (pdfPoint.y - cropBox.origin.y)) / cropBox.size.height;
            viewPoint.y = pageRenderRect.size.height * (cropBox.size.width - (pdfPoint.x - cropBox.origin.x)) / cropBox.size.width;
            break;
        case 0:
        default:
            viewPoint.x = pageRenderRect.size.width * (pdfPoint.x - cropBox.origin.x) / cropBox.size.width;
            viewPoint.y = pageRenderRect.size.height * (cropBox.size.height - pdfPoint.y) / cropBox.size.height;
            break;
    }
    
    viewPoint.x = viewPoint.x + pageRenderRect.origin.x;
    viewPoint.y = viewPoint.y + pageRenderRect.origin.y;
    
    return viewPoint;
}

@end
