//
//  PDFUploadInfo.h
//  ArcMediaViewer
//
//  Created by Infuse Medical on 7/10/14.
//  Copyright (c) 2014 Infuse Medical. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface PDFUploadInfo : NSObject <NSCoding>
@property (nonatomic, strong) NSString * uploadURL;
@property (nonatomic, strong) NSString * company;
@property (nonatomic, strong) NSString * key;
@property (nonatomic, assign) int config_id;
@property (nonatomic, strong) NSString * folder_name;
@property (nonatomic, strong) NSString * file_name;
@property (nonatomic, strong) NSString * file_path;
@property (nonatomic, assign) BOOL showCompletionMessage;
@property (nonatomic, assign) BOOL showFailureMessage;

-(id)initWithFilePath:(NSString*)filePath;

@end
