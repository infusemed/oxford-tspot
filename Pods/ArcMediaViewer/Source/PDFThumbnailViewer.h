//
//  PDFThumbnailViewer.h
//  BalloonsPlus
//
//  Created by Kevin Ray on 4/6/12.
//  Copyright (c) 2012 Infuse Medical. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <QuartzCore/QuartzCore.h>

@protocol PDFThumbnailViewerDelegate
-(void) jumpToPageFromThumbnail:(int)page;
@end

@interface PDFThumbnailViewer : UIView <UIPopoverControllerDelegate>
{
    int _intPageCount;
    int _intStartX;
    int _intEndX;
    int _intThumbnailSpacing;
    int _intCurrentPageViewing;
    float _floatDisplayPageIncrement;
    
    NSMutableArray* _arrThumbnailBtnArray;
    
    UIPopoverController* _popoverController;
    UIView* _viewThumbnailEnclosure;
    UIImageView* _imageViewPageIndicator;
    UIButton* _btnHighlighedThumb;
}
@property (assign) NSObject <PDFThumbnailViewerDelegate>* delegate;

@property (nonatomic, strong) NSMutableArray* arrAllThumbnailImages;

- (id)initWithFrame:(CGRect)frame pageCount:(int)pageCount;
-(void) addThumnail:(UIImage*)image forPageNumber:(int)pageNum;
-(void) viewWidthChanged;
-(void) highlightThumbnailForPage:(int)pageNum;
@end
