//
//  PDFInteractiveOverlayVideos.m
//  BalloonsPlusTesting
//
//  Created by Kevin Ray on 4/16/13.
//  Copyright (c) 2013 Infuse Medical. All rights reserved.
//

#import "PDFInteractiveOverlayVideos.h"

@implementation PDFInteractiveOverlayVideos

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
    }
    return self;
}

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect
{
    // Drawing code
}
*/

@end
