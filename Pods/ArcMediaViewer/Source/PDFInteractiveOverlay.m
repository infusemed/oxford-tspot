//
//  LowResView.m
//  BalloonsPlus
//
//  Created by Kevin Ray on 7/20/11.
//  Copyright 2011 Infuse Medical. All rights reserved.
//

#import "PDFInteractiveOverlay.h"
#import "PDFInteractiveOverlay+Links.h"
#import "PDFInteractiveOverlay+Widgets.h"
#import "PDFInteractiveOverlay+Videos.h"
#import "SignatureButton.h"
#import "PDFWidgetAnnotation.h"
#import "IMArcMediaViewer.h"

@implementation PDFInteractiveOverlay

@synthesize delegate;
@synthesize arrSavedFormInfo;
@synthesize viewParent;
@synthesize tempPageDictionary;
@synthesize pageDictionary;
@synthesize scale;
@synthesize boolEmailing;
@synthesize stringFileName;
@synthesize radioButtonParents;

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        pageLinks = [[NSMutableArray alloc] init];
        pageWidgets = [[NSMutableArray alloc] init];
        pageVideos = [[NSMutableArray alloc] init];
        _arrAllFormObjects = [[NSMutableArray alloc] init];
        _arrHighlights = [[NSMutableArray alloc] init];
        self.formEnabled = YES;
    }
    return self;
}

- (void)setPage:(CGPDFPageRef)newPage pageNumber:(int)pageNumber widgetIndexOffset:(int)offset
{
    _pageNumber = pageNumber;
    
    [pageLinks removeAllObjects];
    CGPDFPageRelease(self->pdfPage);
    self->pdfPage = CGPDFPageRetain(newPage);
    
    [self loadPageLinks: newPage];
    [self drawButtons];
    [self loadPageWidgets:newPage withIndexOffset:offset];
    [self drawFormObjects];
    [self loadPageVideos:newPage];
    [self drawVideos];
}

- (void)removeHighlights
{
    for (UIView* highlight in _arrHighlights)
        [highlight removeFromSuperview];
    
    [_arrHighlights removeAllObjects];
}

- (NSArray *)selections
{
    return selections;
}

- (void)dismissKeyboard
{
    for (int i = 0; i < [_arrAllFormObjects count]; i++)
    {
        if ([[_arrAllFormObjects objectAtIndex:i] isKindOfClass:[UITextField class]]) {
            UITextField* textfield = [_arrAllFormObjects objectAtIndex:i];
            if (textfield.isFirstResponder)
                [textfield resignFirstResponder];
        }
    }
}

- (void)saveFormData
{
    for (int i = 0; i < [_arrAllFormObjects count]; i++)
    {
        NSString* objectInfo  = @"invalid";
        if ([[_arrAllFormObjects objectAtIndex:i] isKindOfClass:[SignatureButton class]]) {
            objectInfo = ([(SignatureButton*)[_arrAllFormObjects objectAtIndex:i] imageForState:UIControlStateNormal]!=nil)? @"Signed" : @"Unsigned";
        }else if ([[_arrAllFormObjects objectAtIndex:i] isKindOfClass:[UIButton class]]) {
            objectInfo = ([[_arrAllFormObjects objectAtIndex:i] isSelected])? @"Selected" : @"Unselected";
        }
        else if ([[_arrAllFormObjects objectAtIndex:i] isKindOfClass:[UITextField class]]) {
            UITextField* textfield = [_arrAllFormObjects objectAtIndex:i];
            objectInfo = textfield.text;
        }
        else if ([[_arrAllFormObjects objectAtIndex:i] isKindOfClass:[UIPlaceHolderTextView class]]) {
            UIPlaceHolderTextView* customTextView = [_arrAllFormObjects objectAtIndex:i];
            NSDictionary *someDictionary = @{@"text" : customTextView.text, @"fontSize" : [NSNumber numberWithInt:customTextView.fontSize]};
            [self.arrSavedFormInfo replaceObjectAtIndex:i withObject:someDictionary];
            continue;
        }
        
        [self.arrSavedFormInfo replaceObjectAtIndex:i withObject:objectInfo];
    }
}

-(void) clearFormDataWithDefaultValues:(NSDictionary*)defaultValues andFieldNames:(NSArray*)fieldNames
{
    for (int i = 0; i < [_arrAllFormObjects count]; i++)
    {
        NSString* objectInfo  = @"invalid";
        if ([[_arrAllFormObjects objectAtIndex:i] isKindOfClass:[UIButton class]]) {
            UIButton* btn = [_arrAllFormObjects objectAtIndex:i];
            PDFWidgetAnnotation *widgetAnnotation = [pageWidgets objectAtIndex: i];
            if (widgetAnnotation.widgetType == WidgetTypeCheckbox){
                if(btn.selected) {
                    [self checkboxTouched:btn];
                }
            }else if(widgetAnnotation.hasDefaultCheckValue){
                if(widgetAnnotation.checkedByDefault){
                    [self radioButtonTouched:btn];
                }
            }else{
                if(btn.selected) {
                    [self checkboxTouched:btn];
                }
            }

            objectInfo = @"Unselected";
            
            if (btn.currentImage)
                [btn setImage:nil forState:UIControlStateNormal];
        }
        else if ([[_arrAllFormObjects objectAtIndex:i] isKindOfClass:[UITextField class]]) {
            UITextField* textfield = [_arrAllFormObjects objectAtIndex:i];
            textfield.text = @"";
            if (textfield.isFirstResponder)
                [textfield resignFirstResponder];
            objectInfo = @"";
        }
        else if ([[_arrAllFormObjects objectAtIndex:i] isKindOfClass:[UIPlaceHolderTextView class]]) {
            [[_arrAllFormObjects objectAtIndex:i] setText:@""];
            objectInfo = @"";
        }
        if (defaultValues!=nil) {
            if(fieldNames!=nil){
                NSString * defaultValue = [defaultValues objectForKey:[fieldNames objectAtIndex:i]];
                if(defaultValue!=nil){
                    objectInfo = defaultValue;
                }
            }
        }
        [self.arrSavedFormInfo replaceObjectAtIndex:i withObject:objectInfo];
    }
    NSString *folderPath = [IMArcMediaViewer getTemporaryStorageLocationWithPDFFileName:self.stringFileName];
    NSFileManager *fm = [NSFileManager defaultManager];
    NSError *error = nil;
    NSCharacterSet* notDigits = [[NSCharacterSet decimalDigitCharacterSet] invertedSet]; //deleting any folders that are like page1,2,3,4 pageX starts with page ends with number
    for (NSString *file in [fm contentsOfDirectoryAtPath:folderPath error:&error]) {
        if([file rangeOfString:@"page"].location==0&&
           [[file stringByReplacingOccurrencesOfString:@"page" withString:@""] rangeOfCharacterFromSet:notDigits].location == NSNotFound){
            BOOL success = [fm removeItemAtPath:[NSString stringWithFormat:@"%@/%@", folderPath, file] error:&error];
            if (!success || error) {
                // it failed.
            }
        }
    }
}

- (void)dealloc
{
    if(_videoPlayer !=nil){
        [_videoPlayer pause];
        [_videoPlayer.view removeFromSuperview];
        _videoPlayer = nil;
    }
    CGPDFPageRelease(pdfPage);
}
-(void)stopVideos{
    
    if(_videoPlayer !=nil){
        [_videoPlayer pause];
        [_videoPlayer.view removeFromSuperview];
        _videoPlayer = nil;
    }
}
- (void) createFolder:(NSString*)folderName
{
	NSError* error;
	NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
	NSString *documentsDirectory = [paths objectAtIndex:0];
	NSString *dataPath = [documentsDirectory stringByAppendingPathComponent:folderName];
	
	if (![[NSFileManager defaultManager] fileExistsAtPath:dataPath])
		[[NSFileManager defaultManager] createDirectoryAtPath:dataPath withIntermediateDirectories:NO attributes:nil error:&error]; //Create folder
}

- (void) createFolderFromFullPath:(NSString*)folderPath
{
    NSError* error;
    
    if (![[NSFileManager defaultManager] fileExistsAtPath:folderPath])
        [[NSFileManager defaultManager] createDirectoryAtPath:folderPath withIntermediateDirectories:YES attributes:nil error:&error]; //Create folder
}
-(void) saveImagesWithFormName:(NSString*)formName forTemp:(BOOL)forTemp
{
    NSString* savedFilePath = [IMArcMediaViewer getSavedFormDataLocationWithPDFFileName:self.stringFileName savedFormName:formName];
    for (int i = 0; i < [_arrAllFormObjects count]; i++)
    {
        if ([[_arrAllFormObjects objectAtIndex:i] isKindOfClass:[SignatureButton class]]) {
            
            NSString *pagePath = [savedFilePath stringByAppendingFormat:@"/page%li",(long)self.tag];
            [self createFolderFromFullPath:pagePath];
            
            SignatureButton* signatureToSave = [_arrAllFormObjects objectAtIndex:i];
            NSData* imageData = UIImagePNGRepresentation(signatureToSave.currentImage);
            
            NSFileManager *fileManager = [NSFileManager defaultManager];
            NSString *fullPath = [NSString stringWithFormat:@"%@/signature%li.png", pagePath, (long)signatureToSave.tag];
            
            
            [fileManager createFileAtPath:fullPath contents:imageData attributes:nil];
        }
    }
}

-(void) disableFields:(NSArray*)disabledFields{
    _disabledFields = disabledFields;
}

-(void) loadDefaultValues:(NSDictionary*)defaultValues andFieldNames:(NSArray*)fieldNames{
    _defaultValues = [NSMutableArray new];
    for (NSString* key in fieldNames) {
        [_defaultValues addObject:[defaultValues objectForKey:key]?[defaultValues objectForKey:key]:@""];
    }
}

@end
