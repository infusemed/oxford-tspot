//
//  PDFUploader.h
//  BostonScientific
//
//  Created by Infuse Medical on 3/6/14.
//  Copyright (c) 2014 Dave Stevenson. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "PDFUploadInfo.h"

@interface PDFUploader : NSObject
+(void)startUploading;
+(void)startUploadingPDFInfo:(PDFUploadInfo*)pdfInfo;

@end
