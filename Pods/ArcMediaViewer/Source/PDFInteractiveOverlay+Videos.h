//
//  TiledPDFView+Links.h
//  LinksNavigation
//
//  Created by Sorin Nistor on 6/21/11.
//  Copyright 2011 iPDFdev.com. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "PDFInteractiveOverlay.h"

@interface PDFInteractiveOverlay (Videos)

- (void) drawVideos;
- (CGPoint)convertPDFPointToViewPointForVideos:(CGPoint)pdfPoint;
- (void)loadPageVideos:(CGPDFPageRef)page;

@end
