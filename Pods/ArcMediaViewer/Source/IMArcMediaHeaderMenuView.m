//
//  IMArcMediaHeaderMenuView.m
//  iPhoneTest
//
//  Created by Infuse Medical on 5/12/14.
//  Copyright (c) 2014 Infuse Medical. All rights reserved.
//

#import "IMArcMediaHeaderMenuView.h"

@implementation IMArcMediaHeaderMenuView

-(id)initWithFrame:(CGRect)frame
       menuOptions:(NSArray*)menuOptions
         fromPoint:(CGPoint)point
          delegate:(id)delegate
{
    self = [super initWithFrame:frame];
    if(self){
        _menuOptions = menuOptions;
        _delegate = delegate;
        _point = point;
        
        _buttonExit = [UIButton buttonWithType:UIButtonTypeCustom];
        [_buttonExit addTarget:self action:@selector(buttonFadeOutPressed:) forControlEvents:UIControlEventTouchUpInside];
        [_buttonExit setFrame:frame];
        [_buttonExit setBackgroundColor:[UIColor colorWithWhite:0.0 alpha:0.3]];
        [self addSubview:_buttonExit];
        
        _tableViewOptions = [[UITableView alloc] initWithFrame:CGRectMake(frame.size.width-315, _point.y+20, 310, _menuOptions.count*40)];
        [_tableViewOptions setDataSource:self];
        [_tableViewOptions setDelegate:self];
        [_tableViewOptions setBackgroundColor:[UIColor whiteColor]];
        [self addSubview:_tableViewOptions];
        
        
        [_tableViewOptions.layer setCornerRadius:10];
        
        
     }
    return self;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [_menuOptions count];
}

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

-(UITableViewCell*)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    UITableViewCell * cell= [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"reuse"];
    [cell setBackgroundColor:[UIColor whiteColor]];
    [cell.textLabel setText:[_menuOptions objectAtIndex:indexPath.row]];
    [cell.textLabel setTextAlignment:NSTextAlignmentCenter];
    
    UIView * whiteView = [[UIView alloc] initWithFrame:cell.frame];
    [whiteView setBackgroundColor:[UIColor whiteColor]];
    [cell setSelectedBackgroundView:whiteView];
    
    return cell;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [self fadeOut:^{
        [_delegate menuOptionSelectedAtIndex:(int)indexPath.row];
    }];
}
-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return 40.f;
}
-(void)buttonFadeOutPressed:(UIButton*)sender{
    [self fadeOut:nil];
}

-(void)fadeOut:(void (^)(void))callback{
    [UIView animateWithDuration:0.4 animations:^{
        [self setAlpha:0.0];
    } completion:^(BOOL finished) {
        [self removeFromSuperview];
        if(callback!=nil){
            callback();
        }
    }];
}

@end
