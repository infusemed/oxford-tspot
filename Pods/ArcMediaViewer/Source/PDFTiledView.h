

#import <UIKit/UIKit.h>
#import <QuartzCore/QuartzCore.h>



@interface PDFTiledView : UIView {
	CGPDFPageRef pdfPage;
	CGFloat myScale;
	
	CATiledLayer *tiledLayer;
    
    
}


- (id)initWithFrame:(CGRect)frame andScale:(CGFloat)scale;
- (void)setPage:(CGPDFPageRef)newPage;
-(void) removeTileLayer;
-(void) stopEverythingNow;

@property (assign) CGPDFDocumentRef	pdfDcoument;
@property (assign) BOOL	stopEverything;
@property (assign) int index;
@end
