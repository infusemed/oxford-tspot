//
//  ImportedImageView.h
//  BalloonsPlus
//
//  Created by Kevin Ray on 7/3/12.
//  Copyright (c) 2012 Infuse Medical. All rights reserved.
//

#import <UIKit/UIKit.h>
@interface ImportedImageView : UIImageView

@property (assign) UIButton* closeBtn;
@property (assign) UIImageView* closeBtnImage;
@property (assign) BOOL imageReleased;
@property (copy) NSString* imageName;
@property (copy) NSString* imageID;
@end
