//
//  UIAlertView+IMMVBlockAlert.m
//  ARCMediaViewer
//
//  Created by Jordan Helzer on 6/3/15.
//  Copyright (c) 2015 Infuse Medical. All rights reserved.
//

#import "UIAlertView+IMMVBlockAlert.h"

static void(^dismiss_block)(NSInteger buttonIdx);

@implementation UIAlertView (IMMVBlockAlert)

- (id)initWithTitle:(NSString *)title message:(NSString *)message dismissBlock:(void ( ^ )( NSInteger buttonIndex ))block cancelButtonTitle:(NSString *)cancelButtonTitle otherButtonTitles:(NSString *)otherButtonTitles, ...
{
    self = [self initWithTitle:title message:message delegate:[self class] cancelButtonTitle:cancelButtonTitle otherButtonTitles:otherButtonTitles, nil];
    
    if(self)
    {
        dismiss_block  = [block copy];
    }
    
    return self;
}

+ (void)alertView:(UIAlertView*) alertView didDismissWithButtonIndex:(NSInteger) buttonIndex
{
    if(dismiss_block)
    {
        dismiss_block(buttonIndex);
        dismiss_block = nil;
    }
}

@end
