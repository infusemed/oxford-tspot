//
//  UIImage+FixOrientation.h
//  BalloonsPlusTesting
//
//  Created by Kevin Ray on 3/27/13.
//  Copyright (c) 2013 Infuse Medical. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIImage (FixOrientation)

- (UIImage *)fixOrientation;

@end
