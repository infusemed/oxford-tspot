//
//  CULSignatureView.h
//  Bard
//
//  Created by Brook Harker on 7/19/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <QuartzCore/QuartzCore.h>
#import <UIKit/UIKit.h>


@interface CULSignatureView : UIView {
    CGMutablePathRef* _paths;
    int _strokeCount;
    int _chunks;
    BOOL _enabled;
    float _strokeWidth;
    UIColor* _strokeColor;
    CGPoint previousPoint1;
    CGPoint previousPoint2;
    CGPoint currentPoint;
    UIImageView* imageView;
    
}


@property(nonatomic, assign)BOOL enabled;
@property(nonatomic, assign)float strokeWidth;
@property(nonatomic, retain)UIColor* strokeColor;

- (void) clear;
- (UIImage*) imageFromSignature;



@end
