//
//  IMArcVideoViewController.m
//  ArcMediaViewer
//
//  Created by Infuse Medical on 2/14/14.
//  Copyright (c) 2014 Infuse Medical. All rights reserved.
//

#import "IMArcVideoViewController.h"

@interface IMArcVideoViewController ()

@end

@implementation IMArcVideoViewController

-(id)initWithFrame:(CGRect)frame Delegate:(id<IMArcMediaViewerChildDelegate>)delegate{
    self = [super initWithFrame:frame Delegate:delegate];
    if(self){
        
        [self.view bringSubviewToFront:[super headerView]];
        [self.view bringSubviewToFront:[super buttonBack]];
    }
    return self;
}

- (void) moviePlayBackStateDidChange:(NSNotification*)notification
{
    MPMoviePlaybackState playbackState = [_video playbackState];
    
    switch (playbackState) {
            
        case MPMoviePlaybackStatePaused:
            //[[super headerView] showHeader];
            break;
            
        case MPMoviePlaybackStatePlaying:
            [[super headerView] hideHeader:YES];
            
            break;
            
        case MPMoviePlaybackStateInterrupted:
        case MPMoviePlaybackStateSeekingBackward:
        case MPMoviePlaybackStateSeekingForward:
        case MPMoviePlaybackStateStopped:
            break;
    }
}
#pragma mark -
#pragma mark UIViewController methods

-(void) dealloc
{
    if(_video!=nil){
        [[NSNotificationCenter defaultCenter] removeObserver:self
                                                        name:MPMoviePlayerNowPlayingMovieDidChangeNotification
                                                      object:_video];
        _video = nil;
    }
}


- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(BOOL)respondsToExtension:(NSString*)extension{
    return ([[extension lowercaseString] isEqualToString:@"mp4"] ||
            [[extension lowercaseString] isEqualToString:@"mov"] ||
            [[extension lowercaseString] isEqualToString:@"m4v"]);
}

-(void)loadFileAtPath:(NSString*)filePath{
    
    NSURL* movieURL = [NSURL fileURLWithPath:filePath];
    _video = [[MPMoviePlayerController alloc] initWithContentURL:movieURL];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(moviePlayBackStateDidChange:)
                                                 name:MPMoviePlayerPlaybackStateDidChangeNotification
                                               object:_video];
    
    [_video prepareToPlay];
    [_video.backgroundView setBackgroundColor:[UIColor blackColor]];
    [_video.view setBackgroundColor:[UIColor blackColor]];
    if ([[NSUserDefaults standardUserDefaults] boolForKey:@"RepeatVideosOnce"]) {
        [_video setRepeatMode:MPMovieRepeatModeOne];
    }
    
    _video.shouldAutoplay = TRUE;
    [self.view insertSubview:_video.view belowSubview:[super headerView]];
    [_video setControlStyle:MPMovieControlStyleDefault];
    [self setFrame:self.view.frame];
    
    
    [self.view bringSubviewToFront:[super headerView]];
    [self.view bringSubviewToFront:[super buttonBack]];
    
    
    UITapGestureRecognizer *tapRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(tapGestureRecognized:)];
    [tapRecognizer setDelegate:self];
    [_video.view addGestureRecognizer:tapRecognizer];
}
-(void)setFrame:(CGRect)frame{
    [super setFrame:frame];
    
    CGRect videoFrame = frame;
    videoFrame.origin = CGPointMake(0, 0);
    [_video.view setFrame:videoFrame];
}
-(void)stop{
    if(_video!=nil){
        [_video stop];
    }
}


-(void)buttonBackPressed:(UIButton*)sender{
    [self stop];
    [super buttonBackPressed:sender];
}

@end
