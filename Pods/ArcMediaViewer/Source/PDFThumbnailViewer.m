//
//  PDFThumbnailViewer.m
//  BalloonsPlus
//
//  Created by Kevin Ray on 4/6/12.
//  Copyright (c) 2012 Infuse Medical. All rights reserved.
//

#import "PDFThumbnailViewer.h"
#import "IMArcMediaFooterThumbPeekView.h"

#define ThumbnailWidth 50.0
#define ThumbnailHeight 35.0
#define SpacingFromSides 20.0
#define AdjustBtnCenterY 4

@implementation PDFThumbnailViewer{
    IMArcMediaFooterThumbPeekView * _peek;
}
@synthesize delegate;
@synthesize arrAllThumbnailImages;

- (id)initWithFrame:(CGRect)frame pageCount:(int)pageCount
{
    self = [super initWithFrame:frame];
    if (self) {
        
        self.opaque = FALSE;
        
        UIImageView* bgImage = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"pdf_footer.png"]];
        [self addSubview:bgImage];
        
        if([[[UIDevice currentDevice] systemVersion] floatValue]>=7.f){
            self.opaque = NO;
            self.backgroundColor = [UIColor clearColor];
            [bgImage removeFromSuperview];
            
            UIView * view = [[UIView alloc] initWithFrame:self.bounds];
            [view setClipsToBounds:YES];
            [self insertSubview:view atIndex:0];
            
            UIToolbar *BlurToolbar = [[UIToolbar alloc] initWithFrame:CGRectMake(0, -1, self.bounds.size.width, self.bounds.size.height+1)];
            
            BlurToolbar.autoresizingMask = self.autoresizingMask;
            [BlurToolbar setAlpha:0.99];
            [view addSubview:BlurToolbar];
        }
        
        
        _intCurrentPageViewing = 1;
        _intPageCount = pageCount;
        arrAllThumbnailImages = [[NSMutableArray alloc] init];
        _arrThumbnailBtnArray = [[NSMutableArray alloc] init];
        
        _viewThumbnailEnclosure = [[UIView alloc] initWithFrame:CGRectMake(SpacingFromSides, 0, frame.size.width - SpacingFromSides, frame.size.height)];
        [self addSubview:_viewThumbnailEnclosure];
        
        _imageViewPageIndicator = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"media_viewer_triangle"]];
        _imageViewPageIndicator.frame = CGRectMake(frame.size.width/2, frame.size.height-_imageViewPageIndicator.frame.size.height, _imageViewPageIndicator.frame.size.width, _imageViewPageIndicator.frame.size.height);
        [_imageViewPageIndicator setAlpha:0.8];
        [self addSubview:_imageViewPageIndicator];
        
        _intThumbnailSpacing = 7;
        _floatDisplayPageIncrement = 1;
        
        float numberOfPagesCanDisplay = (self.frame.size.width - (SpacingFromSides*2))/ (ThumbnailWidth + _intThumbnailSpacing);
        if (numberOfPagesCanDisplay < _intPageCount) {
            _floatDisplayPageIncrement = _intPageCount / numberOfPagesCanDisplay;
        }
        _floatDisplayPageIncrement = ceilf(_floatDisplayPageIncrement);
        
        
        UILongPressGestureRecognizer* longPress = [[UILongPressGestureRecognizer alloc] initWithTarget:self action:@selector(longPress:)];
        longPress.minimumPressDuration = 0.5;
        [_viewThumbnailEnclosure addGestureRecognizer:longPress];
        
        UIPanGestureRecognizer *pan = [[UIPanGestureRecognizer alloc] initWithTarget:self action:@selector(didPan:)];
        pan.minimumNumberOfTouches = 1;
        pan.maximumNumberOfTouches = 1;
        [_viewThumbnailEnclosure addGestureRecognizer:pan];
    }
    return self;
}
-(void) thumbnailSelected:(UIButton*)btn
{
    [_popoverController dismissPopoverAnimated:TRUE];
    [self.delegate jumpToPageFromThumbnail:(int)btn.tag];
    
    [self highlightThumbnailForPage:(int)btn.tag];
}
-(void) addThumnail:(UIImage*)image forPageNumber:(int)pageNum
{
    if ((pageNum-1) % (int)_floatDisplayPageIncrement != 0)
    {
        [arrAllThumbnailImages addObject:image];
        return;
    }
    
    float sizeDivider;
    if (image.size.width > image.size.height)
        sizeDivider = ThumbnailWidth/image.size.width;
    else
        sizeDivider = ThumbnailHeight/image.size.height;
    
    UIImageView* imageViewThumb = [[UIImageView alloc] initWithImage:image];
    imageViewThumb.frame = CGRectMake(0, 0, image.size.width*sizeDivider, image.size.height*sizeDivider);
    
    int thumbWidth = imageViewThumb.frame.size.width;
    
    UIButton* btnThumb = [UIButton buttonWithType:UIButtonTypeCustom];
    int newBtnXPos;
    if (pageNum == 1) 
    {
        newBtnXPos = 0;
        _btnHighlighedThumb = btnThumb;
    }
    else
        newBtnXPos= CGRectGetMaxX([[_arrThumbnailBtnArray lastObject] frame]) + _intThumbnailSpacing;
    
    btnThumb.frame = CGRectMake(newBtnXPos, 0, imageViewThumb.frame.size.width, imageViewThumb.frame.size.height);
    btnThumb.center = CGPointMake(btnThumb.center.x, (self.frame.size.height/2) +AdjustBtnCenterY);
    [btnThumb addTarget:self action:@selector(thumbnailSelected:) forControlEvents:UIControlEventTouchUpInside];
    btnThumb.tag = pageNum;
    
    
    btnThumb.layer.masksToBounds = NO;
    btnThumb.layer.cornerRadius = 2; // if you like rounded corners
    btnThumb.layer.shadowOffset = CGSizeMake(0, 0);
    btnThumb.layer.shadowRadius = 1;
    btnThumb.layer.shadowOpacity = 0.9;
    btnThumb.layer.shouldRasterize = TRUE;
    
    [btnThumb addSubview:imageViewThumb];
    
    [_arrThumbnailBtnArray addObject:btnThumb];
    
    [_viewThumbnailEnclosure addSubview:btnThumb];
    
    [arrAllThumbnailImages addObject:image];
    
    _viewThumbnailEnclosure.frame = CGRectMake(0, 0, CGRectGetMaxX(btnThumb.frame), _viewThumbnailEnclosure.frame.size.height);
    _viewThumbnailEnclosure.center = CGPointMake(self.frame.size.width/2, self.frame.size.height/2);
    
    _imageViewPageIndicator.center = CGPointMake(((_intCurrentPageViewing*thumbWidth)+_viewThumbnailEnclosure.frame.origin.x) - (thumbWidth/2), _imageViewPageIndicator.center.y);

}
-(void) longPress:(UIGestureRecognizer*)gesture
{
    if (gesture.state == UIGestureRecognizerStateBegan || gesture.state == UIGestureRecognizerStateChanged) 
    {
        UIView *view = gesture.view;
        CGPoint point = [gesture locationInView:view];
        
        float thumbWidth = (_viewThumbnailEnclosure.frame.size.width / [arrAllThumbnailImages count]);
        int thumbToShow = point.x / thumbWidth;
        
        thumbToShow = MAX(thumbToShow, 0);
        thumbToShow = (int)MIN(thumbToShow, [arrAllThumbnailImages count] - 1);
        
        NSString * labelText =[NSString stringWithFormat:@"%i of %i", thumbToShow + 1, _intPageCount];
        
        UIImage* imageToShow = [arrAllThumbnailImages objectAtIndex:thumbToShow];
        
        if(UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad){
            UIViewController* vc = [[UIViewController alloc] init];
            vc.view.frame = CGRectMake(0, 0, imageToShow.size.width, imageToShow.size.height);
            
            UIButton* btnThumb = [UIButton buttonWithType:UIButtonTypeCustom];
            btnThumb.frame =  CGRectMake(0, 0, imageToShow.size.width, imageToShow.size.height);
            [btnThumb setImage:imageToShow forState:UIControlStateNormal];
            [btnThumb addTarget:self action:@selector(thumbnailSelected:) forControlEvents:UIControlEventTouchUpInside];
            btnThumb.tag = thumbToShow +1;
            
            [vc.view addSubview:btnThumb];
            
            UILabel* pageNum = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, 80, 30)];
            pageNum.backgroundColor = [UIColor colorWithWhite:0.1 alpha:.7];
            pageNum.textAlignment = NSTextAlignmentCenter;
            pageNum.textColor = [UIColor colorWithWhite:1.0 alpha:1.0];
            pageNum.font = [UIFont fontWithName:@"HelveticaNeue-Bold" size:14];
            pageNum.text = labelText;
            [vc.view addSubview:pageNum];
            
            CGSize textSize = [[pageNum text] sizeWithAttributes:@{NSFontAttributeName : [pageNum font]}];
            pageNum.frame = CGRectMake(0, 0, textSize.width + 16, textSize.height + 6);
            pageNum.center = CGPointMake(vc.view.frame.size.width/2, vc.view.frame.size.height/2);
            pageNum.layer.cornerRadius = 10; // if you like rounded corners
            
            [_popoverController dismissPopoverAnimated:FALSE];
            
            //popover
            _popoverController = [[UIPopoverController alloc] initWithContentViewController:vc];
            _popoverController.delegate = self;
            [_popoverController setPopoverContentSize:vc.view.frame.size animated:FALSE];
            [_popoverController setPassthroughViews:[NSArray arrayWithObject:self]];
            [_popoverController presentPopoverFromRect:CGRectMake((thumbToShow*thumbWidth)+_viewThumbnailEnclosure.frame.origin.x, -5, thumbWidth, self.frame.size.height) inView:self permittedArrowDirections:(UIPopoverArrowDirectionDown) animated:YES];
        }else{
            [_peek removeFromSuperview];
            _peek = [[IMArcMediaFooterThumbPeekView alloc] initWithFrame:self.superview.frame
                                                                                                  Image:imageToShow
                                                                                                  Label:labelText
                                                                                              OverPoint:self.center];
            [self.superview addSubview:_peek];
        }
        
    }
    else if (gesture.state == UIGestureRecognizerStateEnded) 
    {
        UIView *view = gesture.view;
        CGPoint point = [gesture locationInView:view];
        
        int thumbWidth = (_viewThumbnailEnclosure.frame.size.width / [arrAllThumbnailImages count]);
        int thumbToShow = point.x / thumbWidth;
        
        if(UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad){
            [_popoverController dismissPopoverAnimated:TRUE];
        }else{
            [_peek removeFromSuperview];
        }
        [self.delegate jumpToPageFromThumbnail:thumbToShow+1];
        
        [self highlightThumbnailForPage:thumbToShow+1];
        
    }
}
-(void) didPan:(UIGestureRecognizer*)gesture
{
    UIPanGestureRecognizer *panGesture = (UIPanGestureRecognizer *) gesture;

    if (panGesture.state == UIGestureRecognizerStateBegan || panGesture.state == UIGestureRecognizerStateChanged) 
    {
        UIView *view = panGesture.view;
        CGPoint point = [gesture locationInView:view];
        
        float thumbWidth = (_viewThumbnailEnclosure.frame.size.width / [arrAllThumbnailImages count]);
        int thumbToShow = point.x / thumbWidth;
        
        thumbToShow = MAX(thumbToShow, 0);
        thumbToShow = (int)MIN(thumbToShow, [arrAllThumbnailImages count] - 1);
        
        UIImage* imageToShow = [arrAllThumbnailImages objectAtIndex:thumbToShow];
        
        UIViewController* vc = [[UIViewController alloc] init];
        vc.view.frame = CGRectMake(0, 0, imageToShow.size.width, imageToShow.size.height);
        
        UIButton* btnThumb = [UIButton buttonWithType:UIButtonTypeCustom];
        btnThumb.frame =  CGRectMake(0, 0, imageToShow.size.width, imageToShow.size.height);
        [btnThumb setImage:imageToShow forState:UIControlStateNormal];
        [btnThumb addTarget:self action:@selector(thumbnailSelected:) forControlEvents:UIControlEventTouchUpInside];
        btnThumb.tag = thumbToShow +1;
    
        [vc.view addSubview:btnThumb];
        
        UILabel* pageNum = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, 80, 30)];
        pageNum.backgroundColor = [UIColor colorWithWhite:0.1 alpha:.7];
        pageNum.textAlignment = NSTextAlignmentCenter;
        pageNum.textColor = [UIColor colorWithWhite:1.0 alpha:1.0];
        pageNum.font = [UIFont fontWithName:@"HelveticaNeue-Bold" size:14];
        pageNum.text = [NSString stringWithFormat:@"%i of %i", thumbToShow + 1, _intPageCount]; 
        [vc.view addSubview:pageNum];
        
        CGSize textSize = [[pageNum text] sizeWithAttributes:@{NSFontAttributeName : [pageNum font]}];
        pageNum.frame = CGRectMake(0, 0, textSize.width + 16, textSize.height + 6);
        pageNum.center = CGPointMake(vc.view.frame.size.width/2, vc.view.frame.size.height/2);
        pageNum.layer.cornerRadius = 10; // if you like rounded corners
        
        [_popoverController dismissPopoverAnimated:FALSE];
        
        //popover
        _popoverController = [[UIPopoverController alloc] initWithContentViewController:vc];
        _popoverController.delegate = self;
        [_popoverController setPopoverContentSize:vc.view.frame.size animated:FALSE];
        [_popoverController setPassthroughViews:[NSArray arrayWithObject:self]];
        //UIButton* btnToShow = [_arrThumbnailBtnArray objectAtIndex:thumbToShow];
        [_popoverController presentPopoverFromRect:CGRectMake((thumbToShow*thumbWidth)+_viewThumbnailEnclosure.frame.origin.x, -5, thumbWidth, self.frame.size.height) inView:self permittedArrowDirections:(UIPopoverArrowDirectionDown) animated:YES];
        
        //_imageViewPageIndicator.center = CGPointMake(((thumbToShow*thumbWidth)+_viewThumbnailEnclosure.frame.origin.x) + (thumbWidth/2), _imageViewPageIndicator.center.y);

        //[_popoverController release];
    }
    else if (panGesture.state == UIGestureRecognizerStateEnded) 
    {
        UIView *view = panGesture.view;
        CGPoint point = [gesture locationInView:view];
        
        float thumbWidth = (_viewThumbnailEnclosure.frame.size.width / [arrAllThumbnailImages count]);
        int thumbToShow = point.x / thumbWidth;
        
        thumbToShow = MAX(thumbToShow, 0);
        thumbToShow = (int)MIN(thumbToShow, [arrAllThumbnailImages count] - 1);
        
        
        [_popoverController dismissPopoverAnimated:TRUE];
        [self.delegate jumpToPageFromThumbnail:thumbToShow+1];
        
        [self highlightThumbnailForPage:thumbToShow+1];
    }
}

-(void) viewWidthChanged
{
    //int currentPage = _btnHighlighedThumb.tag;
    for (UIButton* btn in _arrThumbnailBtnArray)
        [btn removeFromSuperview];
    [_arrThumbnailBtnArray removeAllObjects];
    _viewThumbnailEnclosure.frame = CGRectMake(0, 0, 1, self.frame.size.height);
    
    _floatDisplayPageIncrement = 1;
    
    float numberOfPagesCanDisplay = (self.frame.size.width - (SpacingFromSides*2))/ (ThumbnailWidth + _intThumbnailSpacing);
    if (numberOfPagesCanDisplay < _intPageCount) {
        _floatDisplayPageIncrement = _intPageCount / numberOfPagesCanDisplay;
    }
    _floatDisplayPageIncrement = ceilf(_floatDisplayPageIncrement);
    
    //_intStartX = (self.frame.size.width -((ThumbnailWidth + _intThumbnailSpacing) * (_intPageCount/_floatDisplayPageIncrement)))/2;
    _intStartX = 0;
    _intEndX = _intStartX;
    
    for (int i = 0; i < [arrAllThumbnailImages count]; i+=_floatDisplayPageIncrement) {
        UIButton* btnThumb = [UIButton buttonWithType:UIButtonTypeCustom];
        //btnPlaceHolder.frame = CGRectMake(_intEndX, 0, ThumbnailWidth, ThumbnailWidth);
        [btnThumb addTarget:self action:@selector(thumbnailSelected:) forControlEvents:UIControlEventTouchUpInside];
        //btnPlaceHolder.backgroundColor = [UIColor grayColor];
        btnThumb.tag = i+1;
        
        UIImage* imageThumb = [arrAllThumbnailImages objectAtIndex:i];
        float sizeDivider;
        if (imageThumb.size.width > imageThumb.size.height)
            sizeDivider = ThumbnailWidth/imageThumb.size.width;
        else
            sizeDivider = ThumbnailHeight/imageThumb.size.height;
        
        UIImageView* imageViewThumb = [[UIImageView alloc] initWithImage:imageThumb];
        imageViewThumb.frame = CGRectMake(0, 0, imageThumb.size.width*sizeDivider, imageThumb.size.height*sizeDivider);
        
        [btnThumb addSubview:imageViewThumb];

        int newBtnXPos;
        if (i == 0) 
            newBtnXPos = 0;
        else
            newBtnXPos= CGRectGetMaxX([[_arrThumbnailBtnArray lastObject] frame]) + _intThumbnailSpacing;
        
        btnThumb.frame = CGRectMake(newBtnXPos, 0, imageViewThumb.frame.size.width, imageViewThumb.frame.size.height);
        btnThumb.center = CGPointMake(btnThumb.center.x, (self.frame.size.height/2)+AdjustBtnCenterY);

        [_viewThumbnailEnclosure addSubview:btnThumb];
        [_arrThumbnailBtnArray addObject:btnThumb];
        
        _intEndX += btnThumb.frame.size.width + _intThumbnailSpacing;
    }
    
    _viewThumbnailEnclosure.frame = CGRectMake(0, 0,_intEndX, _viewThumbnailEnclosure.frame.size.height);
    _viewThumbnailEnclosure.center = CGPointMake(self.frame.size.width/2, self.frame.size.height/2);
    
    float thumbWidth = (_viewThumbnailEnclosure.frame.size.width / [arrAllThumbnailImages count]);
    if(arrAllThumbnailImages.count>0){
        _imageViewPageIndicator.center = CGPointMake(((_intCurrentPageViewing*thumbWidth)+_viewThumbnailEnclosure.frame.origin.x) - (thumbWidth/2), _imageViewPageIndicator.center.y);
    }
    //_btnHighlighedThumb = [_arrThumbnailBtnArray objectAtIndex:
}
-(void) highlightThumbnailForPage:(int)pageNum
{
    
    _intCurrentPageViewing = pageNum;
    
    if ([arrAllThumbnailImages count] > 0)
    {
        float thumbWidth = (_viewThumbnailEnclosure.frame.size.width / [arrAllThumbnailImages count]);
        [UIView animateWithDuration:0.3f delay:0.0f options:UIViewAnimationOptionCurveEaseOut animations:^{
            _imageViewPageIndicator.center = CGPointMake(((pageNum*thumbWidth)+_viewThumbnailEnclosure.frame.origin.x) - (thumbWidth/2), _imageViewPageIndicator.center.y);
        } completion:^(BOOL completed){ 
        }];
    }
}
-(void) dealloc
{
}
@end
