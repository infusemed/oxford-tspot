//
//  IMArcPDFViewController.m
//  ArcMediaViewer
//
//  Created by Infuse Medical on 2/14/14.
//  Copyright (c) 2014 Infuse Medical. All rights reserved.
//

#import "IMArcPDFViewController.h"
#import "PDFUploader.h"
#import "UIImage+FixOrientation.h"
#import "PDFWidgetAnnotation.h"
#import "IMArcMediaViewer.h"
#import "UIAlertView+IMMVBlockAlert.h"
#import "PDFInteractiveOverlay+Widgets.h"
#import "ICloudArchiver.h"

#define FILE_NAME_ALERT_TAG 2838

@interface IMArcPDFViewController ()<AlertControllerPresenterDelegate,UISearchBarDelegate> {
    UIDocumentInteractionController *_docController;
    NSString *_userFileName;
    BOOL firstTimeLoaded;
    UITextField *_alertTextField;
    IMArcMediaHeaderMenuView* _loadPopover;
    ICloudArchiver *_icloudArchiver;
    UISearchBar * _searchbar;
    UIView * _resultsView;
    UIView * _resultsSubView;
    UILabel * _resultsLabel;
    UISegmentedControl* _resultsPrevNext;
    UIButton * _resultsButtonDone;
    UIBarButtonItem * _searchButton;
    UIToolbar* _searchButtonToolbar;
	
	BOOL _searchResultsReset;
}

@property (nonatomic, strong) NSMutableDictionary * dictSavedInfo;

@end

@implementation IMArcPDFViewController

-(id)initWithFrame:(CGRect)frame Delegate:(id<IMArcMediaViewerChildDelegate>)delegate{
    self = [super initWithFrame:frame Delegate:delegate];
    if (self) {
        firstTimeLoaded = YES;
        _rectCurrent = frame;
        _boolCanLoadNextPage = TRUE;
        _intNextThumbnailToLoad = 1;
        self.minZoomScale = 1.0f;
        _fieldNamesArray = [NSMutableArray new];
        _fieldTypeArray = [NSMutableArray new];
        
    }
    return self;
}

- (void) observeValueForKeyPath:(NSString *)keyPath ofObject:(id)object change:(NSDictionary<NSString *,id> *)change context:(void *)context
{
	if(![NSThread isMainThread]){
		dispatch_async(dispatch_get_main_queue(), ^{
			[self observeValueForKeyPath:keyPath ofObject:object change:change context:context];
		});
		return;
	}
	if(object == self.searchDocument) {
		[_resultsLabel setText:[NSString stringWithFormat:@" %lu results found",(unsigned long)self.searchDocument.numberOfMatches]];
		[_resultsView setHidden:[_searchbar.text isEqualToString:@""]];
		[_resultsPrevNext setEnabled:self.searchDocument.numberOfMatches>1];
		if(_searchResultsReset) {
			_searchResultsReset = NO;
			[self goToResultsIndex];
		}
	}else{
		[super observeValueForKeyPath:keyPath ofObject:object change:change context:context];
	}
}

-(void)viewDidAppear:(BOOL)animated{
    [super viewDidAppear:animated];
    
    _icloudArchiver = [[ICloudArchiver alloc] initWithDelegate:self AppKey:[[NSBundle mainBundle] bundleIdentifier]];
    [_icloudArchiver restoreSavedFormData];
}

#pragma mark - UISearchBarDelegatge methods
- (void)searchBar:(UISearchBar *)searchBar textDidChange:(NSString *)searchText{
	_resultsLabel.text = @"";
    [self.searchDocument setSearchString:searchText];
	[_resultsView setHidden:[_searchbar.text isEqualToString:@""]];
	[_resultsPrevNext setEnabled:self.searchDocument.numberOfMatches>1];
	self.searchDocument.currentIndex = 0;
	_searchResultsReset = YES;
}

- (void)searchBarSearchButtonClicked:(UISearchBar *)searchBar
{
	[searchBar resignFirstResponder];
}

-(void)searchButtonPressed:(UIBarButtonItem*)item{
    [self.headerView.labelTitle setHidden:YES];
    [_searchbar setHidden:NO];
    [_searchButtonToolbar setHidden:YES];
    [_searchbar becomeFirstResponder];
}

-(void)goToResultsIndex{
    if (self.searchDocument.currentIndex  < 0) {
        self.searchDocument.currentIndex  = (int)(self.searchDocument.numberOfMatches-1);
    }
    if (self.searchDocument.currentIndex  > (int)(self.searchDocument.numberOfMatches-1)) {
        self.searchDocument.currentIndex  = 0;
    }
    [self setFrame:self.view.frame];
    if(self.searchDocument.numberOfMatches>0){
        [self jumpToPage:(int)(self.searchDocument.currentItem.page)];
    }
}

- (BOOL)shouldShowMenu {
    [self addSearchBar];
    
    if ([self blockMenu]) {
        return NO;
    }
    
    if([self optionsForMenu].count == 0){
        return NO;
    }
    if (!_boolIsForm && [self nonFormsBlockMenu]) {
        return NO;
    }
    BOOL showMenu = NO;
    if (_boolIsForm) {
        showMenu = YES;
    }
    if (![super blockEmailOption]) {
        showMenu = YES;
    }
    if ([self allowOpenInMenuOption]) {
        showMenu = YES;
    }
    
    int x = self.headerView.frame.size.width;
    if(self.headerView.buttonMenu != nil){
        x = self.headerView.buttonMenu.frame.origin.x;
    }
    
    [_searchbar setFrame:CGRectMake(x-200, 5, 195, 30)];
    if(_searchbar.frame.origin.x < self.buttonBack.frame.origin.x + self.buttonBack.frame.size.width){
        int right = _searchbar.frame.origin.x + _searchbar.frame.size.width;
        int left = self.buttonBack.frame.origin.x+self.buttonBack.frame.size.width;
        [_searchbar setFrame:CGRectMake(left, 5, right- left, 30)];
    }
    [_searchButtonToolbar setFrame:CGRectMake(x-10, 5,30, 30)];
    [self.headerView.labelTitle setFrame:CGRectMake(self.buttonBack.frame.origin.x + self.buttonBack.frame.size.width, 5, _searchbar.frame.origin.x, 30)];
    return showMenu;
}
-(void)addSearchBar{
    
    if (_searchbar == nil) {
        _searchbar = [UISearchBar new];
        _searchbar.searchBarStyle = UISearchBarStyleMinimal;
        
        _searchButtonToolbar = [UIToolbar new];
        [_searchButtonToolbar setBackgroundImage:[UIImage new] forToolbarPosition:UIToolbarPositionAny barMetrics:UIBarMetricsDefault];
        [_searchButtonToolbar setBackgroundColor:[UIColor clearColor]];
        _searchButton = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemSearch target:self action:@selector(searchButtonPressed:)];
        [_searchButtonToolbar setItems:@[_searchButton]];
        [_searchButtonToolbar setHidden:YES];
        
        if([UIDevice currentDevice].userInterfaceIdiom == UIUserInterfaceIdiomPhone){
            [_searchbar setHidden:YES];
            [_searchButtonToolbar setHidden:NO];
        }
        
        _resultsView = [UIView new];
        _resultsSubView = [[UIView alloc] init];
        _resultsLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, 10, 190, 20)];
        
        [_resultsLabel setTextAlignment:NSTextAlignmentRight];
        [_resultsSubView addSubview:_resultsLabel];
        
        _resultsPrevNext = [[UISegmentedControl alloc] initWithItems:@[@"<",@">"]];
        [_resultsPrevNext setFrame:CGRectMake(200, 10, 50, 20)];
        [_resultsPrevNext addTarget:self action:@selector(resultsPrevNextPressed:) forControlEvents:UIControlEventValueChanged];
        [_resultsSubView addSubview:_resultsPrevNext];
        
        _resultsButtonDone = [UIButton buttonWithType:UIButtonTypeSystem];
        [_resultsButtonDone setFrame:CGRectMake(250, 10, 50, 20)];
        [_resultsButtonDone setTitle:@"Done" forState:UIControlStateNormal];
        [_resultsButtonDone addTarget:self action:@selector(searchDonePressed:) forControlEvents:UIControlEventTouchUpInside];
        [_resultsSubView addSubview:_resultsButtonDone];
    }
    [_searchbar setFrame:CGRectMake(self.headerView.frame.size.width-200, 5, 195, 30)];
    if(_searchbar.frame.origin.x < self.buttonBack.frame.origin.x + self.buttonBack.frame.size.width){
        int right = _searchbar.frame.origin.x + _searchbar.frame.size.width;
        int left = self.buttonBack.frame.origin.x+self.buttonBack.frame.size.width;
        [_searchbar setFrame:CGRectMake(left, 5, right- left, 30)];
    }
    [_searchButtonToolbar setFrame:CGRectMake(self.headerView.frame.size.width-60, 5,30, 30)];
    
    [self.headerView.labelTitle setFrame:CGRectMake(self.buttonBack.frame.origin.x + self.buttonBack.frame.size.width, 5, _searchbar.frame.origin.x, 30)];
    [self.headerView addSubview:_searchbar];
    [self.headerView addSubview:_searchButtonToolbar];
    [_searchbar setDelegate:self];
    [_resultsView setBackgroundColor:[UIColor colorWithWhite:0.95 alpha:1.0]];
    [_resultsView setFrame:CGRectMake(0, 0, self.headerView.frame.size.width, self.headerView.frame.size.height*2)];
    [self.view insertSubview:_resultsView belowSubview:self.headerView];
    
    [_resultsSubView setFrame:CGRectMake(self.headerView.frame.size.width-300, self.headerView.frame.size.height, 300, self.headerView.frame.size.height)];
    [_resultsView setHidden:[_searchbar.text isEqualToString:@""]];
    [_resultsView addSubview:_resultsSubView];
    
    
}
-(void)resultsPrevNextPressed:(UISegmentedControl*)sender{
    if (sender.selectedSegmentIndex==0) {
        self.searchDocument.currentIndex --;
    }else{
        self.searchDocument.currentIndex ++;
    }
    [self goToResultsIndex];
    sender.selectedSegmentIndex = -1;
    
}
-(void)searchDonePressed:(UIButton*)sender{
    [_searchbar setText:@""];
    [_searchbar resignFirstResponder];
    [self searchBar:_searchbar textDidChange:@""];
    [self performSelector:@selector(hideTitleBar) withObject:nil afterDelay:0.4];
    
    if([UIDevice currentDevice].userInterfaceIdiom == UIUserInterfaceIdiomPhone){
        
        [self.headerView.labelTitle setHidden:NO];
        [_searchbar setHidden:YES];
        [_searchButtonToolbar setHidden:NO];
    }
}
- (NSArray *)optionsForMenu {
    NSMutableArray *menuOptions = [NSMutableArray arrayWithCapacity:5];
    if(![super blockEmailOption] || _boolIsForm){
        [menuOptions addObject:@"Send Email"];
    }
    if (_boolIsForm) {
        [menuOptions addObjectsFromArray:@[@"Import Image",@"Clear Form",  @"Save Form", @"Load Form"]];
    }
    
    if ([self allowOpenInMenuOption]) {
        [menuOptions addObject:@"Open in..."];
    }
    
    if ([super uploadURL] !=nil){
        [menuOptions addObject:@"Upload and Email"];
    }
    
    
    return menuOptions;
}

-(void)menuButtonPressed:(int)buttonIndex{
    if(buttonIndex == -1){
        return;
    }
    [self resignFirstResponderWithView:self.view];
    if([super blockEmailOption] && !_boolIsForm){
        buttonIndex +=1;
    }
    switch (buttonIndex) {
        case 0:
        {
            _shouldUploadFile = NO;
            if (_boolIsForm || ![super blockEmailOption]) {
                [self emailFile];
            } else {
                [self showOpenInOptions];
            }
        }
            break;
        case 1:{
            if (_boolIsForm ) {
                [self startMediaBrowserFromViewController];
            } else {
                [self showOpenInOptions];
            }
        }
            break;
        case 2:{
            [self clearFormData];
        }
            break;
        case 3:{
            if ([[[_stringDocumentPath componentsSeparatedByString:@"."] lastObject] isEqualToString:@"pdf"]){
                _alertViewSave = nil;
                _alertViewSave= [[UIAlertView alloc] initWithTitle:@"Save" message:@"Please enter a name for saving:" delegate:self cancelButtonTitle:@"Cancel" otherButtonTitles:@"Save", nil];
                [_alertViewSave setAlertViewStyle:UIAlertViewStylePlainTextInput];
                
                _textFieldSave = [_alertViewSave textFieldAtIndex:0];
                _textFieldSave.font = [UIFont systemFontOfSize:15.0];  //font size
                _textFieldSave.autocapitalizationType = UITextAutocapitalizationTypeNone;
                _textFieldSave.autocorrectionType = UITextAutocorrectionTypeNo;	// no auto correction support
                _textFieldSave.keyboardType = UIKeyboardTypeDefault;  // type of the keyboard
                _textFieldSave.returnKeyType = UIReturnKeyDone;  // type of the return key
                _textFieldSave.clearButtonMode = UITextFieldViewModeWhileEditing;	// has a clear 'x' button to the right
                [_textFieldSave becomeFirstResponder];
                _textFieldSave.delegate = self;
                
                if(_stringFormName)
                    _textFieldSave.text = _stringFormName;
                [_alertViewSave show];
            }
        }
            break;
        case 4:{
            [self showFormLoadPopoverChooser];
        }
            break;
        case 5:
            if([self allowOpenInMenuOption]){
                [self showOpenInOptions];
                break;
            }else if ([super uploadURL] !=nil){
                _shouldUploadFile = YES;
                [self emailFile];
            }
            break;
        case 6:
            if([self allowOpenInMenuOption] && [super uploadURL] !=nil){
                _shouldUploadFile = YES;
                [self emailFile];
            }
            break;
    }

}

- (void)showOpenInOptions {
    [self performSelector:@selector(openInMenuOptionsDelayed) withObject:nil afterDelay:0.2];
}
- (void)openInMenuOptionsDelayed
{
    _docController = [UIDocumentInteractionController interactionControllerWithURL:[NSURL fileURLWithPath:_stringDocumentPath]];
    [_docController presentOpenInMenuFromRect:CGRectMake(self.view.frame.size.width - 40, 10, 300, 400) inView:self.view animated:YES];
}


#pragma mark - AlertControllerPresenterDelegate
-(void)presentAlertController:(UIAlertController*)ac{
    [self presentViewController:ac animated:YES completion:^{
        
    }];
}
-(void) showFormLoadPopoverChooser{
    [self performSelector:@selector(showFormLoadPopoverChooserDelayed) withObject:nil afterDelay:0.2];
}
-(void) showFormLoadPopoverChooserDelayed
{
    if(UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad){
        UIViewController* tableViewHolder = [[UIViewController alloc] init];
        
        UITableView* tableView = [[UITableView alloc] initWithFrame:CGRectMake(0, 0, 300, 300)];
        tableView.delegate = self;
        tableView.dataSource = self;
        tableView.backgroundColor = [UIColor whiteColor];
        tableView.separatorColor = [UIColor blackColor];
        tableView.rowHeight = 40;
        [tableViewHolder setView:tableView];
        
        
        _popoverControllerLoadForm=[[UIPopoverController alloc] initWithContentViewController:tableViewHolder];
        [_popoverControllerLoadForm setPopoverContentSize:tableView.frame.size];
        _popoverControllerLoadForm.delegate=self;
        [_popoverControllerLoadForm presentPopoverFromRect:CGRectMake(_rectCurrent.size.width/2, _rectCurrent.size.height/2, 1, 1) inView:self.view permittedArrowDirections:0 animated:YES];
    }else{
        
        NSMutableArray* arrOfFormDictionaries = [self getArrayOfFormDictionaries];
        
        NSMutableArray* forms = [NSMutableArray new];
        for (NSDictionary* dict in arrOfFormDictionaries) {
            [forms addObject:[dict objectForKey:@"Form_Name"]];
        }
        
        _loadPopover = [[IMArcMediaHeaderMenuView alloc] initWithFrame:self.view.frame menuOptions:forms fromPoint:self.view.center delegate:self];
        [self.view addSubview:_loadPopover];
    }
}

#pragma mark - IMArcMediaHeaderMenuDelegate Methods

-(void)menuOptionSelectedAtIndex:(int)index{
    
    [_loadPopover removeFromSuperview];
    _loadPopover = nil;
    [self clearFormData];
    [self loadPDFFormAtIndex:(int)index];
    
    [_popoverControllerLoadForm dismissPopoverAnimated:YES];
}

#pragma mark -

-(BOOL)respondsToExtension:(NSString*)extension{
    return [[extension lowercaseString] isEqualToString:@"pdf"];
}
-(void)loadFileAtPath:(NSString*)filePath{
    _stringDocumentPath = filePath;
    
    if(self.searchDocument)
	{
		[self.searchDocument removeObserver:self forKeyPath:@"numberOfMatches"];
	}
    self.searchDocument = [[IMPDFSearchManager alloc] initWithFilePath:filePath];
	[self.searchDocument addObserver:self forKeyPath:@"numberOfMatches" options:NSKeyValueObservingOptionNew context:nil];
    
    NSURL *pdfUrl = [NSURL fileURLWithPath:filePath];
    CGPDFDocumentRef pdfRef = CGPDFDocumentCreateWithURL((CFURLRef)pdfUrl);
    _pageCount = CGPDFDocumentGetNumberOfPages(pdfRef);
    CGPDFDocumentRelease(pdfRef);
    
    //retrieve the saved dictionary from NSUserDefaults
    NSMutableArray* components =  [[[[filePath componentsSeparatedByString:@"/"] lastObject] componentsSeparatedByString:@"."] mutableCopy];
    [components removeLastObject];
    NSString* docTitle = [components componentsJoinedByString:@"."];
    
    //Saved Forms
    NSMutableDictionary* dictAllPDFsInfo = [[[NSUserDefaults standardUserDefaults] dictionaryForKey:@"PDF_Saved_Info"] mutableCopy];
    if (!dictAllPDFsInfo) {
        dictAllPDFsInfo = [[NSMutableDictionary alloc] init];
        [[NSUserDefaults standardUserDefaults] setObject:dictAllPDFsInfo forKey:@"PDF_Saved_Info"];
    }
    
    NSArray* arrOfFormDictionaries = [dictAllPDFsInfo objectForKey:docTitle];
    if (!arrOfFormDictionaries || [arrOfFormDictionaries isKindOfClass:[NSDictionary class]]) {
        arrOfFormDictionaries = [[NSMutableArray alloc] init];
        [dictAllPDFsInfo setObject:arrOfFormDictionaries forKey:docTitle];
        
        [[NSUserDefaults standardUserDefaults] setObject:dictAllPDFsInfo forKey:@"PDF_Saved_Info"];
    }
    
    if (!_dictSavedInfo){
        _dictSavedInfo = [[NSMutableDictionary alloc] init];
    }

    [self setupPDF];
    [self.view bringSubviewToFront:_thumbnailViewer];
    [self.view bringSubviewToFront:[super headerView]];
    [self.view bringSubviewToFront:[super buttonBack]];
    [self updatePageNumber];
    
}


-(BOOL)resignFirstResponderWithView:(UIView*)v{
    if ([v isFirstResponder]) {
        [v resignFirstResponder];
        return true;
    }
    for (UIView* subview in v.subviews) {
        if([self resignFirstResponderWithView:subview]){
            return true;
        }
    }
    return false;
}
-(void)setFrame:(CGRect)frame{
    [super setFrame:frame];
    _rectCurrent = frame;
    
    _scrollViewDocument.contentSize = CGSizeMake(frame.size.width * _pageCount, frame.size.height);
    [_labelPageNumber setCenter:CGPointMake(frame.size.width/2,100)];
    
    _intThumnailTableViewHeight = _rectCurrent.size.height-40;
    
    for (PDFScrollView *page in _arrVisiblePages) {
        page.stopEverything = TRUE;
        [page removeFromSuperview];
    }
    
    _thumbnailViewer.frame = CGRectMake(0, _rectCurrent.size.height - _thumbnailViewer.frame.size.height, _rectCurrent.size.width, _thumbnailViewer.frame.size.height);
    [_thumbnailViewer viewWidthChanged];
    
    [_arrVisiblePages removeAllObjects];
    [_arrPageQue removeAllObjects];
    
	_boolCanLoadNextPage = TRUE;
	
	CGFloat viewHeight = frame.size.height - _thumbnailViewer.frame.size.height - _titleBar.frame.size.height;
	_scrollViewDocument.frame = CGRectMake(frame.origin.x, _thumbnailViewer.frame.size.height, frame.size.width, viewHeight);
    _scrollViewDocument.contentSize = CGSizeMake(_scrollViewDocument.frame.size.width * _intPageCount, viewHeight);
	[_scrollViewDocument scrollRectToVisible:CGRectMake((_intCurrentPage -1)*_scrollViewDocument.frame.size.width, 0, _scrollViewDocument.frame.size.width, _scrollViewDocument.frame.size.height) animated:NO];
    
    _boolIsRotating = FALSE;  //this is to prevent scrollViewDidScroll method from changing _currentPage befor it gets to tile the pages when rotated.
    [self tilePages];
}

-(void)hideTitleBar{
    if ([_searchbar.text isEqualToString:@""] && ![_searchbar isFirstResponder]) {
        [super hideTitleBar];
        [self titleBarWillDisappear];
    }
}
-(void)showTitleBar{
    [super showTitleBar];
    [self titleBarWillAppear];
}

-(void) updatePageNumber
{
    _labelPageNumber.text = [NSString stringWithFormat:@"%zu of %zu", _intCurrentPage, _pageCount];

    CGSize textSize =  [_labelPageNumber.text sizeWithAttributes:@{NSFontAttributeName: [_labelPageNumber font]}];
    _labelPageNumber.frame = CGRectIntegral(CGRectMake(0, 0, textSize.width + 20, textSize.height + 6));
    [self setFrame:self.view.frame];
    
}

-(void) setupPDF
{
	self.view.backgroundColor = [UIColor colorWithWhite:.9 alpha:1];
	
	NSURL *pdfUrl = [NSURL fileURLWithPath:_stringDocumentPath];
	
    _cgpdfDocumentRef = CGPDFDocumentCreateWithURL((CFURLRef)pdfUrl);
    
    
    
	_intPageCount = CGPDFDocumentGetNumberOfPages(_cgpdfDocumentRef);
    //[self performSelectorInBackground:@selector(preloadAllEmbeddedVideos) withObject:nil];
    [self preloadAllEmbeddedVideos];
	
    NSMutableArray* arrAllForms = [_dictSavedInfo objectForKey:@"All_Forms_Array"];
    if (!arrAllForms) {
        arrAllForms = [[NSMutableArray alloc] init];
        [_dictSavedInfo setObject:arrAllForms forKey:@"All_Forms_Array"];
    }
    if ([arrAllForms count] != _intPageCount) {
        [arrAllForms removeAllObjects];
        for (int i = 0; i < _intPageCount; i++) {
            [arrAllForms addObject:[[NSMutableDictionary alloc] init]];
        }
    }
    
	//CGRect pagingScrollViewFrame = CGRectMake(rect.origin.x, _title.frame.size.height, rect.size.width, rect.size.height-_title.frame.size.height);
	CGRect pagingScrollViewFrame = CGRectMake(0, 0, _rectCurrent.size.width, _rectCurrent.size.height);
	[self.view setFrame:_rectCurrent];
    _scrollViewDocument = [[UIScrollView alloc] initWithFrame:pagingScrollViewFrame];
    [_scrollViewDocument setClipsToBounds:NO];
    _scrollViewDocument.pagingEnabled = YES;
    _scrollViewDocument.backgroundColor = [UIColor clearColor];
    _scrollViewDocument.showsVerticalScrollIndicator = NO;
    _scrollViewDocument.showsHorizontalScrollIndicator = NO;
    _scrollViewDocument.contentSize = CGSizeMake(pagingScrollViewFrame.size.width * _intPageCount,
                                                     _scrollViewDocument.frame.size.height);
	_scrollViewDocument.bouncesZoom = NO;
	_scrollViewDocument.bounces = NO;
    
    
    UITapGestureRecognizer *tapRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(tapGestureRecognized:)];
    [tapRecognizer setDelegate:self];
    [_scrollViewDocument addGestureRecognizer:tapRecognizer];
    
	//pagingScrollViewFrame = CGRectMake(0, 0, rect.size.width, rect.size.height-_title.frame.size.height);
	//pagingScrollView.bounds = pagingScrollViewFrame;
    _scrollViewDocument.delegate = self;
    [self.view addSubview:_scrollViewDocument];
	
	// Step 2: prepare to tile content
    _arrRecycledPages = [[NSMutableArray alloc] init];
    _arrVisiblePages  = [[NSMutableArray alloc] init];
	_arrPageQue = [[NSMutableArray alloc] init];
	
	
	_intCurrentPage = 1;
	
    [self drawThumbnails];
    
    
    UIImage* footerImage = [UIImage imageNamed:@"pdf_footer.png"];
    _thumbnailViewer = [[PDFThumbnailViewer alloc] initWithFrame:CGRectMake(0, _rectCurrent.size.height - footerImage.size.height, _rectCurrent.size.width, footerImage.size.height) pageCount:(int)_intPageCount];
    _thumbnailViewer.delegate = self;
    [self.view addSubview:_thumbnailViewer];
    
    
    //Title bar
    
    _labelPageNumber = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, 80, 30)];
    _labelPageNumber.backgroundColor = [UIColor colorWithWhite:0.1 alpha:.7];
    _labelPageNumber.textAlignment = NSTextAlignmentCenter;
    _labelPageNumber.textColor = [UIColor colorWithWhite:1.0 alpha:1.0];
    _labelPageNumber.font = [UIFont fontWithName:@"HelveticaNeue-Bold" size:18];
    [_titleBar addSubview:_labelPageNumber];
    [self updatePageNumber];
    
    _labelPageNumber.layer.cornerRadius = 10;
    
    
    
}

-(void)loadPDFFormWithName:(NSString*)formName{
    
    NSMutableArray* components =  [[[[_stringDocumentPath componentsSeparatedByString:@"/"] lastObject] componentsSeparatedByString:@"."] mutableCopy];
    [components removeLastObject];
    NSString* docTitle = [components componentsJoinedByString:@"."];
    
    NSDictionary* dictAllPDFsInfo = [[NSUserDefaults standardUserDefaults] dictionaryForKey:@"PDF_Saved_Info"];
    NSArray* arrOfFormDictionaries = [dictAllPDFsInfo objectForKey:docTitle];
    int index = 0;
    for (NSDictionary * dict in arrOfFormDictionaries) {
        if([[dict objectForKey:@"Form_Name"] isEqualToString:formName]){
            [self loadPDFFormAtIndex:index];
            return;
        }
        index++;
    }
}

-(void) loadPDFFormAtIndex:(int)indexSelected
{
    NSMutableArray* components =  [[[[_stringDocumentPath componentsSeparatedByString:@"/"] lastObject] componentsSeparatedByString:@"."] mutableCopy];
    [components removeLastObject];
    NSString* docTitle = [components componentsJoinedByString:@"."];
    
    NSDictionary* dictAllPDFsInfo = [[NSUserDefaults standardUserDefaults] dictionaryForKey:@"PDF_Saved_Info"];
    NSArray* arrOfFormDictionaries = [dictAllPDFsInfo objectForKey:docTitle];
    
    _dictSavedInfo = (NSMutableDictionary *)CFBridgingRelease(CFPropertyListCreateDeepCopy(kCFAllocatorDefault, (CFDictionaryRef)[arrOfFormDictionaries objectAtIndex:indexSelected], kCFPropertyListMutableContainers));
    for (PDFScrollView *page in _arrVisiblePages) {
        page.stringFormName = [_dictSavedInfo objectForKey:@"Form_Name"];
        _stringFormName = page.stringFormName;
        page.stopEverything = TRUE;
        //if page is removed before low res is finished drawing then alert that the next page in que can be displayed
        if (!page.pageLoaded){
            _boolCanLoadNextPage = TRUE;
        }
        [page removeFromSuperview];
    }
    [self copySavedImagesToTempFolder];
    [_arrVisiblePages removeAllObjects];
    
    [self tilePages];
}

-(void)copySavedImagesToTempFolder{
    NSString * stringFileName = [[[[_stringDocumentPath componentsSeparatedByString:@"/"] lastObject] componentsSeparatedByString:@"."]objectAtIndex:0];
    NSString * sourcePath = [IMArcMediaViewer getSavedFormDataLocationWithPDFFileName:stringFileName savedFormName:_stringFormName];
    NSString * destPath = [IMArcMediaViewer getTemporaryStorageLocationWithPDFFileName:stringFileName];
    
    NSError * error = nil;
    NSArray * directoryContents = [[NSFileManager defaultManager] contentsOfDirectoryAtPath:sourcePath error:&error];
    for (NSString * subDir in directoryContents) {
        NSString * fromPath = [NSString stringWithFormat:@"%@/%@",sourcePath,subDir];
        NSString * toPath = [NSString stringWithFormat:@"%@/%@/",destPath,subDir];
        [[NSFileManager defaultManager] removeItemAtPath:toPath error:&error];
        [[NSFileManager defaultManager] copyItemAtPath:fromPath toPath:toPath error:&error];
    }
    
}


#pragma mark - UIPopoverControllerDelegate Functions

-(void)popoverControllerDidDismissPopover:(UIPopoverController *)popoverController
{
    popoverController = nil;
    
    [[UIApplication sharedApplication] setStatusBarHidden:YES];
}

#pragma mark -
#pragma mark TableView delegates
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    NSMutableArray* arrOfFormDictionaries = [self getArrayOfFormDictionaries];
    
    return [arrOfFormDictionaries count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell* cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:nil];
	//[cell setSelectionStyle:UITableViewCellSelectionStyleNone];
    
    NSMutableArray* arrOfFormDictionaries = [self getArrayOfFormDictionaries];
    
    cell.textLabel.text = [[arrOfFormDictionaries objectAtIndex:indexPath.row] objectForKey:@"Form_Name"];
    
	return cell;
}
- (void) tableView:(UITableView*)tableView didSelectRowAtIndexPath:(NSIndexPath*)indexPath
{
    [tableView deselectRowAtIndexPath:indexPath animated:TRUE];
    
    [self clearFormData];
    [self loadPDFFormAtIndex:(int)indexPath.row];
    
    [_popoverControllerLoadForm dismissPopoverAnimated:YES];
}
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath {
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        
        NSMutableArray* arrOfFormDictionaries = [self getArrayOfFormDictionaries];
        [arrOfFormDictionaries removeObjectAtIndex:indexPath.row];
        
        NSMutableArray* components =  [[[[_stringDocumentPath componentsSeparatedByString:@"/"] lastObject] componentsSeparatedByString:@"."] mutableCopy];
        [components removeLastObject];
        NSString* docTitle = [components componentsJoinedByString:@"."];
        
        NSMutableDictionary* dictAllPDFsInfo = [[[NSUserDefaults standardUserDefaults] dictionaryForKey:@"PDF_Saved_Info"] mutableCopy];
        [dictAllPDFsInfo setObject:arrOfFormDictionaries forKey:docTitle];
        
        
        [[NSUserDefaults standardUserDefaults] setObject:dictAllPDFsInfo forKey:@"PDF_Saved_Info"];
        
        [tableView deleteRowsAtIndexPaths:[NSArray arrayWithObject:indexPath] withRowAnimation:UITableViewRowAnimationTop];
        
    }
}

-(NSMutableArray*) getArrayOfFormDictionaries
{
    NSMutableArray* components =  [[[[_stringDocumentPath componentsSeparatedByString:@"/"] lastObject] componentsSeparatedByString:@"."] mutableCopy];
    [components removeLastObject];
    NSString* docTitle = [components componentsJoinedByString:@"."];
    
    NSDictionary* dictAllPDFsInfo = [[NSUserDefaults standardUserDefaults] dictionaryForKey:@"PDF_Saved_Info"];
    NSMutableArray* arrOfFormDictionaries = [[dictAllPDFsInfo objectForKey:docTitle] mutableCopy];
    return arrOfFormDictionaries;
}
#pragma mark -
#pragma mark Thumbnails
- (void) drawThumbnails
{
    
	dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
        
        NSURL *pdfUrl = [NSURL fileURLWithPath:_stringDocumentPath];
        CGPDFDocumentRef pdf = CGPDFDocumentCreateWithURL((CFURLRef)pdfUrl);
        
        for (int i = 1; i <= _intPageCount; i++) {
            
            if (_boolSelfWillClose)
            {
                CGPDFDocumentRelease(pdf);
                pdf = NULL;
                return;
            }
            
            CGPDFPageRef page = CGPDFDocumentGetPage(pdf, _intNextThumbnailToLoad);
            CGRect pageRect = CGPDFPageGetBoxRect(page, kCGPDFCropBox);
            
            int rotation = CGPDFPageGetRotationAngle(page);
            if (90 == rotation || 270 == rotation)
                pageRect.size = CGSizeMake(pageRect.size.height, pageRect.size.width);
            
            float pdfScale = 200/pageRect.size.width;
            pageRect.size = CGSizeMake(pageRect.size.width*pdfScale, pageRect.size.height*pdfScale);
            
            if (_intNextThumbnailToLoad == 1) {
                [_thumbnailTable setRowHeight:pageRect.size.height+ 40];
            }
            
            UIGraphicsBeginImageContext(pageRect.size);
            
            CGContextRef context = UIGraphicsGetCurrentContext();
            
            
            CGContextSaveGState(context);
            if (90 == rotation)
            {
                CGContextTranslateCTM(context, pageRect.size.width/2, pageRect.size.height/2);
                
                
                CGContextRotateCTM(context, 3.14/2);
                
                // Flip the context so that the PDF page is rendered
                // right side up.
                CGContextTranslateCTM(context, -pageRect.size.height/2, pageRect.size.width/2);
            }
            else if (270 == rotation)
            {
                CGContextTranslateCTM(context, pageRect.size.width/2, pageRect.size.height/2);
                CGContextRotateCTM(context, 3.14*1.5);
                CGContextTranslateCTM(context, -pageRect.size.height/2, pageRect.size.width/2);
            }
            else
                CGContextTranslateCTM(context, 0.0, pageRect.size.height);
            
            CGContextTranslateCTM(context, -pageRect.origin.x*pdfScale, pageRect.origin.y*pdfScale);
            
            CGContextScaleCTM(context, 1.0, -1.0);
            
            CGContextScaleCTM(context, pdfScale,pdfScale);
            
            // First fill the background with white.
            CGContextSetRGBFillColor(context, 1.0,1.0,1.0,1.0);
            CGContextFillRect(context, CGContextGetClipBoundingBox(context));
            
            CGContextDrawPDFPage(context, page);
            CGContextRestoreGState(context);
            
            UIImage *backgroundImage;
            
            backgroundImage= UIGraphicsGetImageFromCurrentImageContext();
            UIGraphicsEndImageContext();
            
            if (backgroundImage!=nil) {
            
                dispatch_async(dispatch_get_main_queue(), ^{
                    [_thumbnailViewer addThumnail:backgroundImage forPageNumber:i];
                    if (i == 1) {
                        
                        [self tilePages];  //start displaying the pages
                    }
                });
            }
            
            _intNextThumbnailToLoad++;
        }
        
        CGPDFDocumentRelease(pdf),
        pdf = NULL;
	});
}
- (void)preloadAllEmbeddedVideos {
   
    
    for (int i = 0; i <= _intPageCount; i++) {
        
        CGPDFPageRef page = CGPDFDocumentGetPage(_cgpdfDocumentRef, i);
        CGPDFPageRetain(page);
        
        CGPDFDictionaryRef pageDictionary = CGPDFPageGetDictionary(page);
        CGPDFArrayRef annotsArray = NULL;
        
        CGPDFDictionaryGetArray(pageDictionary, "Annots", &annotsArray);
        if (annotsArray != NULL) {
            size_t annotsCount = CGPDFArrayGetCount(annotsArray);
            
            for (int j = 0; j < annotsCount; j++) {
                CGPDFDictionaryRef annotationDictionary = NULL;
                if (CGPDFArrayGetDictionary(annotsArray, j, &annotationDictionary)) {
                    const char *annotationType;
                    CGPDFDictionaryGetName(annotationDictionary, "Subtype", &annotationType);
                    
                    PDFWidgetAnnotation * widget = [[PDFWidgetAnnotation alloc] initWithPDFDictionary:annotationDictionary PDFPageRef:page pageNum:i];
                    //NSLog(@"%@", widget.fieldName);
                    [_fieldTypeArray addObject:[NSNumber numberWithInteger:widget.widgetType]];
                    [_fieldNamesArray addObject:widget.fieldName];
                    // break on or right after above, and you will have the list of keys NSLogged
                    if (strcmp(annotationType, "RichMedia") == 0) {
                        
                        CGPDFDictionaryRef richMediaContentDictionary = NULL;
                        
                        if (CGPDFDictionaryGetDictionary(annotationDictionary,  "RichMediaContent", &richMediaContentDictionary)) {
                            
                            
                            CGPDFDictionaryRef assetsDictionary = NULL;
                            
                            if (CGPDFDictionaryGetDictionary(richMediaContentDictionary,  "Assets", &assetsDictionary)) {
                                
                                CGPDFArrayRef namesArray = NULL;
                                if (CGPDFDictionaryGetArray(assetsDictionary, "Names", &namesArray)) {
                                    
                                    size_t namesCount = CGPDFArrayGetCount(namesArray);
                                    
                                    for (int k = 0; k < namesCount; k+=2) {
                                        
                                        CGPDFStringRef videoName;
                                        if (CGPDFArrayGetString(namesArray, k, &videoName)) {
                                            NSString *fileName = (NSString *)CFBridgingRelease(CGPDFStringCopyTextString(videoName));
                                            
                                            NSArray* componentsOfName =[fileName componentsSeparatedByString:@"."];
                                            if ([componentsOfName count]> 1) {
                                                NSString* extention = [componentsOfName objectAtIndex:1];
                                                if([extention isEqualToString:@"mp4"] || [extention isEqualToString:@"m4v"] || [extention isEqualToString:@"mpeg"] || [extention isEqualToString:@"mov"]){
                                                    
                                                    NSString* videoPath = [NSTemporaryDirectory() stringByAppendingPathComponent:fileName];
//                                                    if ([[NSFileManager defaultManager] fileExistsAtPath:videoPath])
//                                                        break;
                                                    
                                                    CGPDFDictionaryRef videoContentsDictionary = NULL;
                                                    if (CGPDFArrayGetDictionary(namesArray, k+1, &videoContentsDictionary)) {
                                                        CGPDFDictionaryRef efDictionary = NULL;
                                                        if (CGPDFDictionaryGetDictionary(videoContentsDictionary,  "EF", &efDictionary)) {
                                                            CGPDFStreamRef fDictionaryStream = NULL;
                                                            if (CGPDFDictionaryGetStream(efDictionary,  "F", &fDictionaryStream)) {
                                                                //CGPDFDictionaryRef fDictionary = CGPDFStreamGetDictionary(fDictionaryStream);
                                                                
                                                                
                                                                //CGPDFReal legnth = 0;
                                                                //CGPDFDictionaryGetNumber(fDictionary, "Length", &legnth);
                                                                
                                                                
                                                                CGPDFDataFormat format;
                                                                CFDataRef videoCFData = NULL;
                                                                
                                                                videoCFData = CGPDFStreamCopyData(fDictionaryStream, &format);
                                                                
                                                                NSData* videoNSData = (NSData*)CFBridgingRelease(videoCFData);
                                                                
                                                                //NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
                                                                //NSString *documentsDirectory = [paths objectAtIndex:0]; // Get documents directory
                                                                
                                                                [videoNSData writeToFile:videoPath atomically:TRUE];
                                                                
                                                                NSURL *videoURL = [NSURL fileURLWithPath:videoPath];
                                                                
                                                                AVURLAsset *videoAsset = [[AVURLAsset alloc] initWithURL:videoURL options:nil];
                                                                AVAssetImageGenerator *generator = [AVAssetImageGenerator assetImageGeneratorWithAsset:videoAsset];
                                                                AVVideoComposition *composition =  [AVVideoComposition videoCompositionWithPropertiesOfAsset:videoAsset];
                                                                generator.requestedTimeToleranceBefore = kCMTimeZero;
                                                                generator.requestedTimeToleranceAfter = kCMTimeZero;
                                                              
                                                                NSError *error = NULL;
                                                                CMTime time = CMTimeMakeWithSeconds(1, composition.frameDuration.timescale);
                                                                CGImageRef refImg = [generator copyCGImageAtTime:time actualTime:NULL error:&error];
                                                                
                                                                if (error) {
                                                                     NSLog(@"error==%@, Refimage==%@", error, refImg);
                                                                }
                                                               
                                                                UIImage *thumbnail= [[UIImage alloc] initWithCGImage:refImg];
                                                                
                                                                // Convert UIImage to JPEG
                                                                NSData *imgData = UIImageJPEGRepresentation(thumbnail, 1);
                                                                
                                                                // Identify the home directory and file name
                                                                NSString  *jpgPath = [NSTemporaryDirectory() stringByAppendingPathComponent:[NSString stringWithFormat:@"thumb%@.jpg",[componentsOfName objectAtIndex:0]]];
                                                                
                                                                // Write the file.  Choose YES atomically to enforce an all or none write. Use the NO flag if partially written files are okay which can occur in cases of corruption
                                                                [imgData writeToFile:jpgPath atomically:YES];
                                                                
                                                                NSLog(@"video");
                                                            }
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
        CGPDFPageRelease(page);
        
    }
}- (NSData*)outputPDF {
    
    NSString* templatePath  = _stringDocumentPath;
    
    CFURLRef url = CFURLCreateWithFileSystemPath (NULL, (CFStringRef)templatePath, kCFURLPOSIXPathStyle, 0);
    //open template file
    CGPDFDocumentRef templateDocument = CGPDFDocumentCreateWithURL(url);
    CFRelease(url);
    
    //get the pdf size by looking at the size of the first page
    CGPDFPageRef sizePage = CGPDFDocumentGetPage(templateDocument, 1);
    CGRect pageRect = CGPDFPageGetBoxRect(sizePage, kCGPDFMediaBox);
    
    //create empty pdf file;
    NSMutableData * pdfData = [NSMutableData new];
    UIGraphicsBeginPDFContextToData(pdfData, pageRect, nil);
    
    //get amount of pages in template
    size_t count = CGPDFDocumentGetNumberOfPages(templateDocument);
    
    //for each page in template
    for (int pageNumber = 1; pageNumber <= count; pageNumber++) {
        //get bounds of template page
        CGPDFPageRef templatePage = CGPDFDocumentGetPage(templateDocument, pageNumber);
        CGRect templatePageBounds = CGPDFPageGetBoxRect(templatePage, kCGPDFCropBox);
        
        
        int rotation = CGPDFPageGetRotationAngle(templatePage);
        if (90 == rotation || 270 == rotation){
            templatePageBounds.size = CGSizeMake(templatePageBounds.size.height, templatePageBounds.size.width);
            templatePageBounds.origin = CGPointMake(templatePageBounds.origin.y, templatePageBounds.origin.x);
        }
        
        //create empty page with corresponding bounds in new document
        UIGraphicsBeginPDFPageWithInfo(templatePageBounds, nil);
        CGContextRef context = UIGraphicsGetCurrentContext();
        
        CGContextSaveGState(context);
        
        if (90 == rotation) {
            CGContextTranslateCTM(context, templatePageBounds.size.width/2, templatePageBounds.size.height/2);
            CGContextRotateCTM(context, 3.14/2);
            CGContextTranslateCTM(context, -templatePageBounds.size.height/2, templatePageBounds.size.width/2);
        } else if (270 == rotation) {
            CGContextTranslateCTM(context, templatePageBounds.size.width/2, templatePageBounds.size.height/2);
            CGContextRotateCTM(context, 3.14*1.5);
            CGContextTranslateCTM(context, -templatePageBounds.size.height/2, templatePageBounds.size.width/2);
        } else {
            CGContextTranslateCTM(context, 0.0, templatePageBounds.size.height);
        }
        
        CGContextTranslateCTM(context, -templatePageBounds.origin.x, templatePageBounds.origin.y);
        CGContextScaleCTM(context, 1.0, -1.0);
        CGContextDrawPDFPage(context, templatePage);
        CGContextRestoreGState(context);
        
        [self drawOverlayPDFDocument:templateDocument withPageRect:templatePageBounds onTemplatePage:templatePage forPageNumber:pageNumber];
        

    }
    CGPDFDocumentRelease(templateDocument);
    UIGraphicsEndPDFContext();
    return pdfData;
}

-(void)drawOverlayPDFDocument:(CGPDFDocumentRef)templateDocument withPageRect:(CGRect)pageRect onTemplatePage:(CGPDFPageRef)templatePage forPageNumber:(int)pageNumber{
    
    NSMutableDictionary*  dictSavedDocumentInfo = [[_dictSavedInfo objectForKey:@"All_Forms_Array"] objectAtIndex:pageNumber-1];
    
    NSMutableArray* pdfFormInfo = [dictSavedDocumentInfo objectForKey:@"SavedFormInfo"];
    if (!pdfFormInfo) {
        pdfFormInfo = [[NSMutableArray alloc] init];
        [dictSavedDocumentInfo setObject:pdfFormInfo forKey:@"SavedFormInfo"];
    }
    
    int widgetOffset =0;
    for (int pageIndex  = 1; pageIndex < pageNumber; pageIndex++) {
        widgetOffset += [PDFInteractiveOverlay widgetCountForPage:CGPDFDocumentGetPage(templateDocument, pageIndex)];
    }
    
    PDFInteractiveOverlay* interactiveOverlay = [[PDFInteractiveOverlay alloc] initWithFrame:pageRect];
    interactiveOverlay.formEnabled = self.formEnabled;
    interactiveOverlay.stringFileName = [[_stringDocumentPath lastPathComponent] stringByDeletingPathExtension];
    interactiveOverlay.stringFormName = _stringFormName;
    interactiveOverlay.boolEmailing = YES;
    interactiveOverlay.arrSavedFormInfo = pdfFormInfo;
    interactiveOverlay.tag = pageNumber;
    [interactiveOverlay setDisabledFields:self.disabledFields];
    [interactiveOverlay loadDefaultValues:self.defaultValues andFieldNames:_fieldNamesArray];
    interactiveOverlay.scale = 1;
    [interactiveOverlay setPage:templatePage pageNumber:pageNumber widgetIndexOffset:widgetOffset];
    [interactiveOverlay drawFormObjectsOnPDF];
    
    NSArray *_arrImportedImagesInfo = [dictSavedDocumentInfo objectForKey:@"ArrayOfImportedImages"];
    if (!_arrImportedImagesInfo) {
        _arrImportedImagesInfo = [[NSMutableArray alloc] init];
        [dictSavedDocumentInfo setObject:_arrImportedImagesInfo forKey:@"ArrayOfImportedImages"];
    }
    
    for (NSMutableDictionary* importedImageInfo in _arrImportedImagesInfo)
    {
        BOOL fileExists = [[NSFileManager defaultManager] fileExistsAtPath:[importedImageInfo objectForKey:@"imageName"]];
        if (fileExists){
            
            UIImage * img =[UIImage imageWithContentsOfFile:[importedImageInfo objectForKey:@"imageName"]];
            CGRect frame = CGRectMake([[importedImageInfo objectForKey:@"originX"] intValue],
                                      [[importedImageInfo objectForKey:@"originY"] intValue],
                                      [[importedImageInfo objectForKey:@"width"] intValue],
                                      [[importedImageInfo objectForKey:@"height"] intValue]);
            
            [img drawInRect:frame];
        }
    }
}

-(void) documentIsForm
{
    _boolIsForm = TRUE;
    [self.delegate documentIsForm];
    
    [super setFrame:self.view.frame];
}


-(void) titleBarWillDisappear
{
    
    [UIView animateWithDuration:0.5f delay:0.0f options:UIViewAnimationOptionCurveEaseInOut animations:^{
        _thumbnailViewer.alpha = 0;
    } completion:^(BOOL completed){
    }];
    
}
-(void) titleBarWillAppear
{
    
    [UIView animateWithDuration:0.5f delay:0.0f options:UIViewAnimationOptionCurveEaseInOut animations:^{
        _thumbnailViewer.alpha = 1;
    } completion:^(BOOL completed){
    }];
    
}
-(void) prevPage
{
	
    _intCurrentPage--;
    if (_intCurrentPage < 1) {
        _intCurrentPage = 1;
    }
    
    if (_titleBar)
        [self updatePageNumber];
    
    [_scrollViewDocument scrollRectToVisible:CGRectMake((_intCurrentPage-1)*_scrollViewDocument.frame.size.width, 0, _scrollViewDocument.frame.size.width, _scrollViewDocument.frame.size.height) animated:YES];
    
	
    [self tilePages];
}

-(void) nextPage
{
    _intCurrentPage++;
    if (_intCurrentPage > _intPageCount) {
        _intCurrentPage = _intPageCount;
    }
    if (_titleBar)
        [self updatePageNumber];
    
    [_scrollViewDocument scrollRectToVisible:CGRectMake((_intCurrentPage-1)*_scrollViewDocument.frame.size.width, 0, _scrollViewDocument.frame.size.width, _scrollViewDocument.frame.size.height) animated:YES];
    
    [self tilePages];
}
-(void) jumpToPage:(int)page
{
    [_scrollViewDocument scrollRectToVisible:CGRectMake((page-1)*_scrollViewDocument.frame.size.width, 0, _scrollViewDocument.frame.size.width, _scrollViewDocument.frame.size.height) animated:NO];
	_intCurrentPage = page;
    
    [self tilePages];
    
}
-(void) jumpToPageFromThumbnail:(int)page
{
    [self jumpToPage:page];
    
}
- (void)tilePages
{
	@synchronized(self)
	{
		// Calculate which pages are visible
		CGRect visibleBounds = _scrollViewDocument.bounds;
        
		int firstNeededPageIndex;
		int lastNeededPageIndex;
        
        firstNeededPageIndex = floorf(CGRectGetMinX(visibleBounds) / CGRectGetWidth(visibleBounds));
        lastNeededPageIndex  = floorf((CGRectGetMaxX(visibleBounds)-1) / CGRectGetWidth(visibleBounds));
        
		
		firstNeededPageIndex--;
		lastNeededPageIndex++;
        
        firstNeededPageIndex = MAX(firstNeededPageIndex, 0);
        lastNeededPageIndex  = (int)MIN(lastNeededPageIndex, _intPageCount - 1);
        //}
		
		// Recycle no-longer-visible pages
		for (PDFScrollView *page in _arrVisiblePages) {
			if (page.index < firstNeededPageIndex+1 || page.index > lastNeededPageIndex+1) {
				//int ind = page.index;
				[_arrRecycledPages addObject:page];
				//set to true so threads that finish in page know that it has been removed
				page.stopEverything = TRUE;
				//if page is removed before low res is finished drawing then alert that the next page in que can be displayed
				if (!page.pageLoaded) {
					_boolCanLoadNextPage = TRUE;
                }
                [page stopVideos];
				[page removeFromSuperview];
			}
            if(page.index != _intCurrentPage){
                [page stopVideos];
            }
		}
		[_arrVisiblePages removeObjectsInArray:_arrRecycledPages];
		[_arrRecycledPages removeAllObjects];
        
		// add missing pages
		for (int index = firstNeededPageIndex; index <= lastNeededPageIndex; index++) {
			if (![self isDisplayingPageForIndex:index+1]) {
				BOOL loadPage = TRUE;
				for (NSNumber* num in _arrPageQue)
				{
					if ([num intValue] == index) {
						loadPage = FALSE;
					}
				}
				if (loadPage) {
					[_arrPageQue addObject:[NSNumber numberWithInt:index]];
				}
			}
		}
        [self loadNextPage];
	}
}

- (BOOL)isDisplayingPageForIndex:(NSUInteger)index
{
    BOOL foundPage = NO;
    for (PDFScrollView *page in _arrVisiblePages) {
        if (page.index == index) {
            foundPage = YES;
            break;
        }
    }
    return foundPage;
}
-(void) loadNextPage
{
	@synchronized(self)
	{
		if (_boolCanLoadNextPage && [_arrPageQue count] > 0) {
			if ([_arrPageQue count] > 3) {
				[_arrPageQue removeObjectsInRange:NSMakeRange(0, [_arrPageQue count] - 3)];
			}
            int queueIndex = 0;
            //load the page the user wants to see first then load the prev and next pages
            for (int i = 0; i < [_arrPageQue count]; i++) {
                if ([[_arrPageQue objectAtIndex:i] intValue]+1 == _intCurrentPage) {
                    queueIndex = i;
                    break;
                }
            }
			
            int pageNumber = [[_arrPageQue objectAtIndex:queueIndex] intValue]+1;
            if ([_thumbnailViewer.arrAllThumbnailImages count] < pageNumber) {
                [self performSelector:@selector(tilePages) withObject:nil afterDelay:1];
                return;
            }
            
			PDFScrollView *page;
			
			CGRect rect = _scrollViewDocument.frame;
            rect.origin.x = rect.size.width * [[_arrPageQue objectAtIndex:queueIndex] intValue];
            rect.origin.y = 0;
            
			page = [[PDFScrollView alloc] initWithFrame:rect PageNumber:pageNumber Path:_stringDocumentPath ScrollLeftRight:YES formEnabled:self.formEnabled minZoomScale:self.minZoomScale];
            page.searchDocument = self.searchDocument;
            page.stringFormName= _stringFormName;
            page.styleSignature = self.styleSignature;
            [page renderLowResForViewing];
            page.dictSavedDocumentInfo = [[_dictSavedInfo objectForKey:@"All_Forms_Array"] objectAtIndex:pageNumber-1];
            [page drawReallyLowResFirst:[_thumbnailViewer.arrAllThumbnailImages objectAtIndex:pageNumber - 1]];
            page.scrollView.backgroundColor = _colorBetweenPages;
            
			[page setDelegate:self];
            
            [page saveFormData];
			
			if (!_scrollViewDocument) {
				return;
			}
			[_scrollViewDocument addSubview:page];
			[_arrVisiblePages addObject:page];
            
			[_arrPageQue removeObjectAtIndex:queueIndex];
			_boolCanLoadNextPage = FALSE;
            [page loadDefaultValues:[super defaultValues] andFieldNames:_fieldNamesArray];
            [page loadDisabledFields:[self boolArrayOfDisabledFields]];
		}
	}
	
}
-(void) finishedLoading
{
	_boolCanLoadNextPage = TRUE;
	[self loadNextPage];
    
    if(_stringFormName == nil || [@"" isEqualToString:_stringFormName]){
        if(firstTimeLoaded){
            firstTimeLoaded = NO;
            [self clearFormData];
        }
    }
}

#pragma mark -
#pragma mark ScrollView delegate methods

- (void)scrollViewDidScroll:(UIScrollView *)scrollView
{
	if (_boolIsRotating)
        return;
    
	CGRect visibleBounds = _scrollViewDocument.bounds;
    int firstNeededPageIndex;
    firstNeededPageIndex = floorf(CGRectGetMinX(visibleBounds) / CGRectGetWidth(visibleBounds));
	
	_intCurrentPage = firstNeededPageIndex+1;
	if (_titleBar)
		[self updatePageNumber];
}

-(void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView
{
    [_thumbnailViewer highlightThumbnailForPage:(int)_intCurrentPage];
    [self tilePages];
}
-(void)lockZoomAndScrolling
{
    _scrollViewDocument.scrollEnabled = FALSE;
}

-(void)unlockZoomAndScrolling
{
    _scrollViewDocument.scrollEnabled = TRUE;
}

#pragma mark -
#pragma mark UIViewController methods
- (void) willRotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation duration:(NSTimeInterval)duration
{
	CGRect appFrame = [[UIScreen mainScreen] bounds];
	if (UIInterfaceOrientationIsLandscape(interfaceOrientation)) {
		// Need to flip the X-Y coordinates for landscape
		_rectCurrent = CGRectMake(appFrame.origin.y, appFrame.origin.x, appFrame.size.height, appFrame.size.width);
	}
	else {
		_rectCurrent = appFrame;
	}
    _boolIsRotating = TRUE;
	_newOrientation = interfaceOrientation;
    
    for (PDFScrollView *page in _arrVisiblePages)
    {
        [page dismissKeyboard];
        [page saveFormData];
		[page stopThread];
    }
    //[self performSelector:@selector(reload) withObject:nil afterDelay:0];
    
    if(![_stringFormName isEqualToString:@"TEMPFORMFORROTATION"]){
        _preRotationFormName = _stringFormName;
    }
    
    _stringFormName = @"TEMPFORMFORROTATION";
    
    [self saveFormWithName:_stringFormName];
    
}
- (void)didRotateFromInterfaceOrientation:(UIInterfaceOrientation)fromInterfaceOrientation
{
	if ((UIInterfaceOrientationIsLandscape(fromInterfaceOrientation) && UIInterfaceOrientationIsLandscape(_newOrientation))||
		(UIInterfaceOrientationIsPortrait(fromInterfaceOrientation) && UIInterfaceOrientationIsPortrait(_newOrientation)) )
	{
        _boolIsRotating = FALSE;
		return;
	}
    
    _intThumnailTableViewHeight = _rectCurrent.size.height-40;
    
    for (PDFScrollView *page in _arrVisiblePages) {
        page.stopEverything = TRUE;
        [page removeFromSuperview];
    }
    
    if (_titleBar) {
        _labelPageNumber.center = CGPointMake(_rectCurrent.size.width/2, _titleBar.frame.size.height + (_labelPageNumber.frame.size.height/2));
        
        _thumbnailViewer.frame = CGRectMake(0, _rectCurrent.size.height - _thumbnailViewer.frame.size.height, _rectCurrent.size.width, _thumbnailViewer.frame.size.height);
        [_thumbnailViewer viewWidthChanged];
        
	}
    [_arrVisiblePages removeAllObjects];
    [_arrPageQue removeAllObjects];
    
	_boolCanLoadNextPage = TRUE;
	
	_scrollViewDocument.frame = CGRectMake(_rectCurrent.origin.x, 0, _rectCurrent.size.width, _rectCurrent.size.height);
    _scrollViewDocument.contentSize = CGSizeMake(_scrollViewDocument.frame.size.width * _intPageCount, _scrollViewDocument.frame.size.height);
	[_scrollViewDocument scrollRectToVisible:CGRectMake((_intCurrentPage -1)*_scrollViewDocument.frame.size.width, 0, _scrollViewDocument.frame.size.width, _scrollViewDocument.frame.size.height) animated:NO];
    
    _boolIsRotating = FALSE;  //this is to prevent scrollViewDidScroll method from changing _currentPage befor it gets to tile the pages when rotated.
    [self tilePages];
    
    [self performSelector:@selector(finishRotation) withObject:nil afterDelay:2.0];
}
-(void)finishRotation{
    
    //[self loadPDFFormWithName:@"TEMPFORMFORROTATION"];
    
    [self removeTempForm];
}
-(void)removeTempForm{
    _stringFormName = _preRotationFormName;
    NSMutableArray* components =  [[[[_stringDocumentPath componentsSeparatedByString:@"/"] lastObject] componentsSeparatedByString:@"."] mutableCopy];
    [components removeLastObject];
    NSString* docTitle = [components componentsJoinedByString:@"."];
    NSDictionary* dictAllPDFsInfo = [[NSUserDefaults standardUserDefaults] dictionaryForKey:@"PDF_Saved_Info"];
    NSMutableDictionary * newDictAllPDFsInfo = [NSMutableDictionary new];
    for (NSString * key in dictAllPDFsInfo.allKeys) {
        if(![key isEqualToString:docTitle]){
            [newDictAllPDFsInfo setObject:[dictAllPDFsInfo objectForKey:key] forKey:key];
        }
    }
    NSMutableArray * newArray = [NSMutableArray new];
                                           
    NSArray* arrOfFormDictionaries = [dictAllPDFsInfo objectForKey:docTitle];
    
    for (NSDictionary * formDict in arrOfFormDictionaries) {
        if(![[formDict objectForKey:@"Form_Name"] isEqualToString:@"TEMPFORMFORROTATION"]){
            [newArray addObject:formDict];
        }
    }
    [newDictAllPDFsInfo setObject:newArray forKey:docTitle];
    [[NSUserDefaults standardUserDefaults] setObject:newDictAllPDFsInfo forKey:@"PDF_Saved_Info"];
    [[NSUserDefaults standardUserDefaults] synchronize];
}
- (void)didReceiveMemoryWarning
{
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    CGRect visibleBounds = _scrollViewDocument.bounds;
    
    int firstNeededPageIndex;
    int lastNeededPageIndex;
    
    firstNeededPageIndex = floorf(CGRectGetMinX(visibleBounds) / CGRectGetWidth(visibleBounds));
    lastNeededPageIndex  = floorf((CGRectGetMaxX(visibleBounds)-1) / CGRectGetWidth(visibleBounds));
   
    
    if (_boolIsForm) {
        firstNeededPageIndex = 0;
        lastNeededPageIndex  = (int)(_intPageCount - 1);
    }
    else
    {
        firstNeededPageIndex = MAX(firstNeededPageIndex, 0);
        lastNeededPageIndex  = (int)MIN(lastNeededPageIndex, _intPageCount - 1);
    }
    
    // Recycle no-longer-visible pages
    for (PDFScrollView *page in _arrVisiblePages) {
        if (page.index < firstNeededPageIndex+1 || page.index > lastNeededPageIndex+1) {
            //int ind = page.index;
            [_arrRecycledPages addObject:page];
            //set to true so threads that finish in page know that it has been removed
            page.stopEverything = TRUE;
            //if page is removed before low res is finished drawing then alert that the next page in que can be displayed
            if (!page.pageLoaded) {
                _boolCanLoadNextPage = TRUE;
            }
            [page removeFromSuperview];
        }
    }
    
    [_arrVisiblePages removeObjectsInArray:_arrRecycledPages];
    [_arrRecycledPages removeAllObjects];
    // Release any cached data, images, etc that aren't in use.
}
-(void) CreatePDFFile:(CGRect)pageRect mailBody:(NSString*)p_mailBody signature:(UIImage*)p_signature
{
	NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
	NSString *saveDirectory = [paths objectAtIndex:0];
	NSString *saveFileName = @"order.pdf";
	NSString *newFilePath = [saveDirectory stringByAppendingPathComponent:saveFileName];
	const char *filename = [newFilePath UTF8String];
	
	// This code block sets up our PDF Context so that we can draw to it
	CGContextRef pdfContext;
	CFStringRef path;
	CFURLRef url;
	CFMutableDictionaryRef myDictionary = NULL;
	
	// Create a CFString from the filename we provide to this method when we call it
	path = CFStringCreateWithCString (NULL, filename, kCFStringEncodingUTF8);
	// Create a CFURL using the CFString we just defined
	url = CFURLCreateWithFileSystemPath (NULL, path, kCFURLPOSIXPathStyle, 0);
	CFRelease (path);
	
	// This dictionary contains extra options mostly for 'signing' the PDF
    
	myDictionary = CFDictionaryCreateMutable(NULL, 0,
											 &kCFTypeDictionaryKeyCallBacks,
											 &kCFTypeDictionaryValueCallBacks);
	CFDictionarySetValue(myDictionary, kCGPDFContextTitle, CFSTR("Order PDF"));
	CFDictionarySetValue(myDictionary, kCGPDFContextCreator, CFSTR("Order"));
    
	// Create our PDF Context with the CFURL, the CGRect we provide, and the above defined dictionary
	pdfContext = CGPDFContextCreateWithURL (url, &pageRect, myDictionary);
	// Cleanup our mess
	CFRelease(myDictionary);
	CFRelease(url);
	// Done creating our PDF Context, now it's time to draw to it
	
    
    
    for(int i = 1; i <= _intPageCount; i++)
    {
        
        // Starts our first page
        CGContextBeginPage (pdfContext, &pageRect);
        
        
        
        const char *picture = "cartPage";
        CGImageRef image;
        CGDataProviderRef provider;
        CFStringRef picturePath;
        CFURLRef pictureURL;
        
        
        picturePath = CFStringCreateWithCString (NULL, picture, kCFStringEncodingUTF8);
        pictureURL = CFBundleCopyResourceURL(CFBundleGetMainBundle(), CFSTR("cartPage"), CFSTR("png"), NULL);
        CFRelease(picturePath);
        provider = CGDataProviderCreateWithURL (pictureURL);
        CFRelease (pictureURL);
        image = CGImageCreateWithPNGDataProvider (provider, NULL, true, kCGRenderingIntentDefault);
        CFRelease(provider);
        CGContextDrawImage (pdfContext, CGRectMake(0, 0, 760, 900),image);
        CGImageRelease (image);
        
        UIGraphicsPopContext();
        
        // We are done drawing to this page, let's end it
        // We could add as many pages as we wanted using CGContextBeginPage/CGContextEndPage
        CGContextEndPage (pdfContext);
        
        //[self.view setNeedsDisplay];
        
    }
    
	CGContextRelease (pdfContext);
}

-(void) createPDFImagesForForms
{
    if (_intCurrentPageRenderingForEmailing == 0) {
        [self createFormsFolder];
        
        
        _labelRenderingFromsForEmail = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, 80, 80)];
        _labelRenderingFromsForEmail.center = CGPointMake(self.view.frame.size.width/2, self.view.frame.size.height/2);
        _labelRenderingFromsForEmail.backgroundColor = [UIColor colorWithWhite:0.1 alpha:.7];
        _labelRenderingFromsForEmail.textAlignment = NSTextAlignmentCenter;
        _labelRenderingFromsForEmail.textColor = [UIColor colorWithWhite:1.0 alpha:1.0];
        _labelRenderingFromsForEmail.font = [UIFont fontWithName:@"HelveticaNeue-Bold" size:18];
        [self.view addSubview:_labelRenderingFromsForEmail];
        
        _labelRenderingFromsForEmail.layer.cornerRadius = 10;
        
        
        _spinningWheel = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleWhiteLarge];
        _spinningWheel.frame = CGRectMake(0, 0, 100, 100);
        _spinningWheel.center = CGPointMake(self.view.frame.size.width/2, self.view.frame.size.height/2);
        [self.view addSubview:_spinningWheel];
        [_spinningWheel startAnimating];
    }
    CGRect rect = _scrollViewDocument.frame;
    rect.origin.x = rect.size.width * _intCurrentPageRenderingForEmailing + 1;
    rect.origin.y = 0;
    
    PDFScrollView* page = [[PDFScrollView alloc] initWithFrame:rect PageNumber:_intCurrentPageRenderingForEmailing+1 Path:_stringDocumentPath ScrollLeftRight:YES formEnabled:self.formEnabled minZoomScale:self.minZoomScale];
    page.searchDocument = self.searchDocument;
    page.stringFormName= _stringFormName;
    [page setEmailScale:self.emailScale];
    page.dictSavedDocumentInfo = [[_dictSavedInfo objectForKey:@"All_Forms_Array"] objectAtIndex:_intCurrentPageRenderingForEmailing];
    page.styleSignature = self.styleSignature;
    [page loadDefaultValues:[super defaultValues] andFieldNames:_fieldNamesArray];
    [page loadDisabledFields:[self boolArrayOfDisabledFields]];
    [page renderLowResForEmailing];
    [page createAndSaveFormImage];
    
    _intCurrentPageRenderingForEmailing++;
    if (_intCurrentPageRenderingForEmailing < _intPageCount)
        [self performSelector:@selector(createPDFImagesForForms) withObject:nil afterDelay:0.001];
        //[self createPDFImagesForForms];
    else
    {
        [self createEmailableFileForForms];
        _intCurrentPageRenderingForEmailing = 0;
        
        [_labelRenderingFromsForEmail removeFromSuperview];
        _labelRenderingFromsForEmail = nil;
        [_spinningWheel removeFromSuperview];
        _spinningWheel = nil;
    }
}
-(NSArray*)boolArrayOfDisabledFields{
    NSMutableArray * array = [NSMutableArray new];
    for (NSString * fieldName in _fieldNamesArray) {
        if (self.disabledFields && [self.disabledFields indexOfObject:fieldName]!= NSNotFound) {
            [array addObject:[NSNumber numberWithBool:YES]];
        }else{
            [array addObject:[NSNumber numberWithBool:NO]];
        }
    }
    return array;
}
-(void) createEmailableFileForForms
{
    
	NSError* error;
	NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
	NSString *documentsDirectory = [paths objectAtIndex:0]; // Get documents folder
	NSString *dataPath = [documentsDirectory stringByAppendingPathComponent:@"Forms"];
	
	if (![[NSFileManager defaultManager] fileExistsAtPath:dataPath])
		[[NSFileManager defaultManager] createDirectoryAtPath:dataPath withIntermediateDirectories:NO attributes:nil error:&error]; //Create folder
    
    UIImageView* backgroundImage = [[_arrVisiblePages objectAtIndex:0] backgroundImageView];
    CGRect pageRect = CGRectMake(0, 0, backgroundImage.frame.size.width, backgroundImage.frame.size.height);
    NSString* fileName  = [[_stringDocumentPath componentsSeparatedByString:@"/"] lastObject];
    
	NSString *saveDirectory = [paths objectAtIndex:0];
	NSString *saveFileName = [NSString stringWithFormat:@"Forms/%@", fileName];
	NSString *newFilePath = [saveDirectory stringByAppendingPathComponent:saveFileName];
	const char *filename = [newFilePath UTF8String];
	
	// This code block sets up our PDF Context so that we can draw to it
	CGContextRef pdfContext;
	CFStringRef path;
	CFURLRef url;
	CFMutableDictionaryRef myDictionary = NULL;
	
	// Create a CFString from the filename we provide to this method when we call it
	path = CFStringCreateWithCString (NULL, filename, kCFStringEncodingUTF8);
	// Create a CFURL using the CFString we just defined
	url = CFURLCreateWithFileSystemPath (NULL, path, kCFURLPOSIXPathStyle, 0);
	CFRelease (path);
	
	// This dictionary contains extra options mostly for 'signing' the PDF
    
	myDictionary = CFDictionaryCreateMutable(NULL, 0,
											 &kCFTypeDictionaryKeyCallBacks,
											 &kCFTypeDictionaryValueCallBacks);
	CFDictionarySetValue(myDictionary, kCGPDFContextTitle, CFSTR("Form PDF"));
	CFDictionarySetValue(myDictionary, kCGPDFContextCreator, CFSTR("Form"));
    
	// Create our PDF Context with the CFURL, the CGRect we provide, and the above defined dictionary
	pdfContext = CGPDFContextCreateWithURL (url, &pageRect, myDictionary);
	// Cleanup our mess
	CFRelease(myDictionary);
	CFRelease(url);
    
    
    NSMutableArray * pdfPageFileNames = [NSMutableArray new];
    
    NSString* subFolder = [[_stringDocumentPath lastPathComponent]stringByDeletingPathExtension];
    NSString * tempFolder = [IMArcMediaViewer getTemporaryStorageLocationWithPDFFileName:subFolder];
    
	// Done creating our PDF Context, now it's time to draw to it
    for(int i = 1; i <= _intPageCount; i++)
    {
        NSString *fullPath = [tempFolder stringByAppendingPathComponent:[NSString stringWithFormat:@"/PDFSnapshots/page%i.png", i]];
        
        
        CGImageRef image;
        CGDataProviderRef provider;
        //CFStringRef picturePath;
        CFURLRef pictureURL;
        
        //picturePath = CFStringCreateWithCString (NULL, picture, kCFStringEncodingUTF8);
        //pictureURL = CFBundleCopyResourceURL(CFBundleGetMainBundle(), CFSTR("cartPage"), CFSTR("png"), NULL);
        //CFRelease(picturePath);
        pictureURL = (CFURLRef)CFBridgingRetain([NSURL fileURLWithPath:fullPath]);
        
        
        provider = CGDataProviderCreateWithURL (pictureURL);
        CFRelease (pictureURL);
        image = CGImageCreateWithPNGDataProvider (provider, NULL, true, kCGRenderingIntentDefault);
        
        NSUInteger width = CGImageGetWidth(image);
        NSUInteger height = CGImageGetHeight(image);
        
        
        CGRect pageRect = CGRectMake(0, 0, width, height);
        // Starts our first page
        CGContextBeginPage (pdfContext, &pageRect);
        
        //const char *picture = "cartPage";
        CFRelease(provider);
        CGContextDrawImage (pdfContext, pageRect,image) ;
        CGImageRelease (image);
        
        UIGraphicsPopContext();
        
        // We are done drawing to this page, let's end it
        // We could add as many pages as we wanted using CGContextBeginPage/CGContextEndPage
        CGContextEndPage (pdfContext);
        
        //[self.view setNeedsDisplay];
        
        
        [pdfPageFileNames  addObject:fullPath];
        
    }
    
    
    if(_shouldUploadFile){
        if([super requireAllFields]&&![self allFieldsFilledIn]){
            UIAlertView * alert  = [[UIAlertView alloc] initWithTitle:@"Required Fields" message:@"Please fill out all fields" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles: nil];
            [alert show];
            return;
        }
        
        NSMutableData *pdfData = [[NSMutableData alloc] init];
        CGRect pageSize = pageRect;
        
        UIGraphicsBeginPDFContextToData(pdfData, pageRect, nil);
        
        for (NSString * filePath in pdfPageFileNames) {
            UIGraphicsBeginPDFPage();
            [[UIImage imageWithContentsOfFile:filePath] drawInRect:pageSize];
        }
        
        // Close the PDF context and write the contents out.
        UIGraphicsEndPDFContext();
        [self uploadFileToServer:_stringFormName fileData:pdfData];
    }
    
    
	// We are done with our context now, so we release it
    // We are done with our context now, so we release it
	CGContextRelease (pdfContext);
    [self emailPDFForm];
}

-(BOOL)allFieldsFilledIn{
    
    NSArray * allFormsArray = [_dictSavedInfo objectForKey:@"All_Forms_Array"];
    int fieldIndex = 0;
    int pageIndex = 0;
    for (NSDictionary * thisForm in allFormsArray) {
        NSArray * formFields = [thisForm objectForKey:@"SavedFormInfo"];
        for (NSObject*field in formFields) {
            NSString * fieldName = _fieldNamesArray[fieldIndex];
            if ([[fieldName lowercaseString] isEqualToString:@"required checklist"]) {
                if([(NSString*)field isEqualToString:@"Unselected"]){
                    return NO;
                }
            }
            if([_fieldTypeArray[fieldIndex] integerValue] == WidgetTypeSignature){
                if([(NSString*)field isEqualToString:@"Unsigned"]){
                    return NO;
                }
            }
            if([field isKindOfClass:[NSString class]]){
                if(field==nil||[@"" isEqualToString:((NSString*)field)]){
                    return NO;
                }
            }
            fieldIndex++;
        }
        pageIndex++;
    }
    return  YES;
}
- (void) createFormsFolder
{
    NSError* error;
    NSString* subFolder = [[_stringDocumentPath lastPathComponent]stringByDeletingPathExtension];
    NSString * tempFolder = [IMArcMediaViewer getTemporaryStorageLocationWithPDFFileName:subFolder];
	NSString *dataPath = [tempFolder stringByAppendingString:@"/PDFSnapshots"];
	
	if (![[NSFileManager defaultManager] fileExistsAtPath:dataPath])
		[[NSFileManager defaultManager] createDirectoryAtPath:dataPath withIntermediateDirectories:NO attributes:nil error:&error]; //Create folder
    else
    {
        NSError* error;
        [[NSFileManager defaultManager] removeItemAtPath:dataPath error:&error];
        [[NSFileManager defaultManager] createDirectoryAtPath:dataPath withIntermediateDirectories:NO attributes:nil error:&error];
    }
}
-(CGPoint) getLeftMostPostionOfTitleBarBtns
{
    return _thumbnailBtn.frame.origin;
}
-(void) setTitleBar:(UIImageView *)titleBar
{
    _titleBar = titleBar;
}
-(UIImageView*) titleBar
{
    return _titleBar;
}
-(void) keepTitleAlpha
{
    _titleBar.tag = 1;
}
-(void) saveFormData
{
    /*
    if(_dictSavedInfo!=nil){
        NSMutableArray* components =  [[[[_stringDocumentPath componentsSeparatedByString:@"/"] lastObject] componentsSeparatedByString:@"."] mutableCopy];
        [components removeLastObject];
        NSString* docTitle = [components componentsJoinedByString:@"."];
        
        NSMutableDictionary* dictAllPDFsInfo = [[[NSUserDefaults standardUserDefaults] dictionaryForKey:@"PDF_Saved_Info"] mutableCopy];
        [dictAllPDFsInfo setObject:_dictSavedInfo forKey:docTitle];
        [[NSUserDefaults standardUserDefaults] setObject:dictAllPDFsInfo forKey:@"PDF_Saved_Info"];
    }
     */
}
-(void) clearFormData
{
    _stringFormName = nil;
    for (PDFScrollView *page in _arrVisiblePages)
    {
        page.stringFormName = nil;
        [page clearFormDataWithDefaultValues:[super defaultValues] andFieldNames:_fieldNamesArray];
    }
    
    [self clearSignatures];
    
    NSArray* arrFormInfo = [_dictSavedInfo objectForKey:@"All_Forms_Array"];
    if(arrFormInfo)
    {
        for (int i = 0; i < [arrFormInfo count]; i++) {
            BOOL skipPage = FALSE;
            for (PDFScrollView *page in _arrVisiblePages)
            {
                if (page.index == i + 1) {
                    skipPage = TRUE;
                    break;
                }
            }
            if (skipPage)
                continue;
            
            NSMutableDictionary* formInfoForPage = [[_dictSavedInfo objectForKey:@"All_Forms_Array"] objectAtIndex:i];
            [formInfoForPage removeObjectForKey:@"SavedFormInfo"];
        }
    }
    [self saveFormData];
    
}
-(BOOL) shouldAutorotate
{
    return FALSE;
}
- (void) dealloc
{
	if(self.searchDocument)
	{
		[self.searchDocument removeObserver:self forKeyPath:@"numberOfMatches"];
	}
    //save PDF info
    for (PDFScrollView *page in _arrVisiblePages)
    {
        [page saveFormData];
        page.stopEverything = TRUE;
        page.delegate = nil;
        [page stopVideos];
        [page removeFromSuperview];
    }
    
    [self saveFormData];
    
    [_arrPageQue removeAllObjects];
    
}
-(void)stop{
    
    for (PDFScrollView *page in _arrVisiblePages)
    {
        [page saveFormData];
        page.stopEverything = TRUE;
        page.delegate = nil;
        [page stopVideos];
    }
    
}


#pragma mark Email

- (NSString *)mimeTypeForExtension:(NSString *)extension
{
    NSString *mimeType;
    if ([extension isEqualToString:@"png"]) {
        mimeType = @"image/png";
    } else if ([extension isEqualToString:@"jpg"]) {
        mimeType = @"image/jpg";
    } else if ([extension isEqualToString:@"xls"]) {
        mimeType = @"application/xls";
    } else if ([extension isEqualToString:@"xlsx"]) {
        mimeType = @"application/xlsx";
    } else if ([extension isEqualToString:@"doc"]) {
        mimeType = @"application/doc";
    } else if ([extension isEqualToString:@"mp4"]) {
        mimeType = @"video/mp4";
    } else if ([extension isEqualToString:@"m4v"]) {
        mimeType = @"video/m4v";
    } else if ([extension isEqualToString:@"mov"]) {
        mimeType = @"video/mov";
    } else {
        mimeType = @"application/pdf";
    }
    return mimeType;
}

- (void)emailFile {
    [self performSelector:@selector(emailFileDelayed) withObject:nil afterDelay:0.2];
}

- (BOOL)validFileName:(NSString *)fileName
{
    NSRange range = [fileName rangeOfCharacterFromSet:[[NSCharacterSet characterSetWithCharactersInString:@"abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLKMNOPQRSTUVWXYZ0123456789_- ."] invertedSet]];
    if (range.location != NSNotFound) {
        NSString *message = [NSString stringWithFormat:@"File name contained an invalid character: %@\n", [fileName substringWithRange:range]];
        __weak typeof(self) weakSelf = self;
        [[[UIAlertView alloc] initWithTitle:@"Invalid file name"
                                    message:message
                                   dismissBlock:^(NSInteger buttonIndex) {
                                       [weakSelf emailFileDelayed];
                                   }
                          cancelButtonTitle:@"OK"
                          otherButtonTitles:nil] show];
        return NO;
    }
    return YES;
}

- (void)showFileNameAlertAgain
{
    [self emailFileDelayed];
}

- (void)emailFileDelayed {
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Attachment"
                                                    message:@"Please enter the name of the attachment you would like to send:"
                                                   delegate:self
                                          cancelButtonTitle:@"Cancel"
                                          otherButtonTitles:@"OK", nil];
    [alert setTag:FILE_NAME_ALERT_TAG];
    [alert setAlertViewStyle:UIAlertViewStylePlainTextInput];
    _alertTextField = [alert textFieldAtIndex:0];
    NSString *documentTitle = self.headerView.labelTitle.text;
    [_alertTextField setText:documentTitle];
    [_alertTextField setClearButtonMode:UITextFieldViewModeAlways];
    [alert show];
}

-(void) emailPDFForm
{
    
	NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString* fileName  = [[_stringDocumentPath componentsSeparatedByString:@"/"] lastObject];
    
	NSString *saveDirectory = [paths objectAtIndex:0];
	NSString *saveFileName = [NSString stringWithFormat:@"Forms/%@", fileName];
	NSString *newFilePath = [saveDirectory stringByAppendingPathComponent:saveFileName];
    
    [self showEmailComposerAndAttachFile:newFilePath];
}

- (void) showEmailComposerAndAttachFile:(NSString*)filePath
{
	NSString *emailComposerBody = @"";
    NSString *filePathName  = [[_stringDocumentPath componentsSeparatedByString:@"/"] lastObject];
    
	if ([MFMailComposeViewController canSendMail]) {
		NSString *extension = [filePathName pathExtension];
        
        if (!_userFileName || _userFileName.length == 0) {
            _userFileName = filePathName;
        }
        
        if (![_userFileName.pathExtension isEqualToString:extension]) {
            _userFileName = [_userFileName stringByAppendingPathExtension:extension];
        }
		
		MFMailComposeViewController *emailDialog = [[MFMailComposeViewController alloc] init];
		emailDialog.mailComposeDelegate = self;
		[emailDialog setSubject:@""];
        NSData* data = [NSData dataWithContentsOfFile:filePath options:NSDataReadingMapped error:nil];
        [emailDialog addAttachmentData:data mimeType:[self mimeTypeForExtension:extension] fileName:_userFileName];

        if (self.emailBody) {emailComposerBody = self.emailBody;}
        if (self.emailSubject) {[emailDialog setSubject:self.emailSubject];}
        if (self.emailToRecipients) {[emailDialog setToRecipients:[self.emailToRecipients componentsSeparatedByString:@";"]];}
        if (self.ccRecipients) {[emailDialog setCcRecipients:[self.ccRecipients componentsSeparatedByString:@";"]];}
        if (self.bCCRecipients) {[emailDialog setBccRecipients:[self.bCCRecipients componentsSeparatedByString:@";"]];}
        
		[emailDialog setMessageBody:emailComposerBody isHTML:YES];
		
		[self presentViewController:emailDialog animated:YES completion:nil];
	} else {
		UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Can't Send Mail" message:@"Please setup your mail account in Settings" delegate:self cancelButtonTitle:@"Thanks" otherButtonTitles:nil];
		[alert show];
	}
	
}

- (void) showEmailComposerAndAttachPDFData:(NSData*)pdfData
{
    NSString *emailComposerBody = @"";
    NSString *filePathName  = [[_stringDocumentPath componentsSeparatedByString:@"/"] lastObject];
    
    if ([MFMailComposeViewController canSendMail]) {
        NSString *extension = [filePathName pathExtension];
        
        if (!_userFileName || _userFileName.length == 0) {
            _userFileName = filePathName;
        }
        
        if (![_userFileName.pathExtension isEqualToString:extension]) {
            _userFileName = [_userFileName stringByAppendingPathExtension:extension];
        }
        
        MFMailComposeViewController *emailDialog = [[MFMailComposeViewController alloc] init];
        emailDialog.mailComposeDelegate = self;
        [emailDialog setSubject:@""];
        [emailDialog addAttachmentData:pdfData mimeType:[self mimeTypeForExtension:extension] fileName:_userFileName];
        
        if (self.emailBody) {emailComposerBody = self.emailBody;}
        if (self.emailSubject) {[emailDialog setSubject:self.emailSubject];}
        if (self.emailToRecipients) {[emailDialog setToRecipients:[self.emailToRecipients componentsSeparatedByString:@";"]];}
        if (self.ccRecipients) {[emailDialog setCcRecipients:[self.ccRecipients componentsSeparatedByString:@";"]];}
        if (self.bCCRecipients) {[emailDialog setBccRecipients:[self.bCCRecipients componentsSeparatedByString:@";"]];}
        
        [emailDialog setMessageBody:emailComposerBody isHTML:YES];
        
        [self presentViewController:emailDialog animated:YES completion:nil];
    } else {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Can't Send Mail" message:@"Please setup your mail account in Settings" delegate:self cancelButtonTitle:@"Thanks" otherButtonTitles:nil];
        [alert show];
    }
    
}

// Dismisses the email composition interface when users tap Cancel or Send. Proceeds to update the message field with the result of the operation.
- (void)mailComposeController:(MFMailComposeViewController*)controller didFinishWithResult:(MFMailComposeResult)result error:(NSError*)error
{
    [self dismissViewControllerAnimated:YES completion:^{
        _userFileName = nil;
    }];
}

#pragma mark -

- (void)startMediaBrowserFromViewController{
    [self performSelector:@selector(startMediaBrowserFromViewControllerDelayed) withObject:nil afterDelay:0.2];
}
- (BOOL) startMediaBrowserFromViewControllerDelayed
{
    
    if(UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad){
    
        if (([UIImagePickerController isSourceTypeAvailable: UIImagePickerControllerSourceTypeSavedPhotosAlbum] == NO)){
            return NO;
        }
        
        
        UIImagePickerController *mediaUI = [[UIImagePickerController alloc] init];
        mediaUI.sourceType = UIImagePickerControllerSourceTypeSavedPhotosAlbum;
        
        [mediaUI setMediaTypes:@[(NSString *)kUTTypeImage]];

       
        mediaUI.allowsEditing = YES;
        
        
        
        _popOverController=[[UIPopoverController alloc] initWithContentViewController:mediaUI];
        _popOverController.delegate=self;
        [_popOverController presentPopoverFromRect:CGRectMake(_rectCurrent.size.width/2, _rectCurrent.size.height/2, 1, 1) inView:self.view permittedArrowDirections:0 animated:YES];
        
        
        mediaUI.delegate = self;
    }else{
        UIImagePickerController *imagePicker =
        [[UIImagePickerController alloc] init];
        imagePicker.delegate = self;
        imagePicker.sourceType = UIImagePickerControllerSourceTypePhotoLibrary|
                                    UIImagePickerControllerSourceTypeSavedPhotosAlbum;
        
        [imagePicker setMediaTypes:@[(NSString *)kUTTypeImage]];
        
        imagePicker.allowsEditing = YES;
        
        imagePicker.edgesForExtendedLayout = UIRectEdgeAll;
        
        [self presentViewController:imagePicker animated:YES completion:nil];
    }
    return YES;
    
}

- (void)imagePickerControllerDidCancel:(UIImagePickerController *)picker{
    if(UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad){
        if(_popOverController==nil){
            [picker dismissViewControllerAnimated:YES completion:^{
                
            }];
        }else{        
            [_popOverController dismissPopoverAnimated:YES];
        }
        [[UIApplication sharedApplication] setStatusBarHidden:YES];
    }
    else{
        [picker dismissViewControllerAnimated:YES completion:^{
            
        }];
    }
}
- (void) imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo: (NSDictionary *) info
{
    NSString *mediaType = [info objectForKey: UIImagePickerControllerMediaType];
    
    UIImage *originalImage, *editedImage, *imageToUse;
    
    
    
    // Handle a still image picked from a photo album
    
    if (CFStringCompare ((CFStringRef) mediaType, kUTTypeImage, 0)
        
        == kCFCompareEqualTo)
    {
        
        editedImage = (UIImage *) [info objectForKey:
                                   
                                   UIImagePickerControllerEditedImage];
        
        originalImage = (UIImage *) [info objectForKey:
                                     
                                     UIImagePickerControllerOriginalImage];
        
        
        if (editedImage)
            imageToUse = editedImage;
        else
            imageToUse = originalImage;
        
        imageToUse = [imageToUse fixOrientation];
        NSURL *imagePath = [info objectForKey:@"UIImagePickerControllerReferenceURL"];
        
        NSString *imageName = [imagePath absoluteString];
      
        PDFScrollView* currentPage = nil;
        for (PDFScrollView* page in _arrVisiblePages)
        {
            if (page.index == _intCurrentPage) {
                currentPage = page;
            }
        }
        
        
        
        [currentPage importImage:imageToUse WithName:imageName];
        
    }
    if(UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad){
        [_popOverController dismissPopoverAnimated:YES];
        
        
        [[UIApplication sharedApplication] setStatusBarHidden:YES];
    }
    else{
        [picker dismissViewControllerAnimated:YES completion:^{
            
        }];
    }
    
    
}

#pragma mark UIAlertView
- (void)alertView:(UIAlertView *)alert clickedButtonAtIndex:(NSInteger)buttonIndex {
    [self resignFirstResponderWithView:self.view];
	if (alert == _alertViewSave) {
		if (buttonIndex == 1){
            _stringFormName = _textFieldSave.text;
            
            if ([[[_stringDocumentPath componentsSeparatedByString:@"."] lastObject] isEqualToString:@"pdf"]){
                [self saveFormWithName:_stringFormName];
            }
        }
	}else if(alert==_alertViewOverwrite){
        if(buttonIndex==1){
            [self saveForm];
        }
    } else if (alert.tag == FILE_NAME_ALERT_TAG) {
        if (buttonIndex == 1){
            if ([self validFileName:_alertTextField.text]) {
                _userFileName = _alertTextField.text;
                if (_boolIsForm) {
                    NSData * pdfData;
                    if(_shouldUploadFile){
                        if([super requireAllFields]&&![self allFieldsFilledIn]){
                            UIAlertView * alert  = [[UIAlertView alloc] initWithTitle:@"Required Fields" message:@"Please fill out all fields" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles: nil];
                            [alert show];
                            return;
                        }
                        pdfData =[self outputPDF];
                        [self uploadFileToServer:_stringFormName fileData:pdfData];
                    }else{
                        pdfData =[self outputPDF];
                    }
                    [self showEmailComposerAndAttachPDFData:pdfData];
                } else {
                    [self showEmailComposerAndAttachFile:_stringDocumentPath];
                }
            }
        }
    }
}
-(void) saveForm
{
    _stringFormName = _stringTempFormName;
    for (PDFScrollView *page in _arrVisiblePages)
        [page saveAllImportedImagesInfoForFormName:_stringFormName forTemp:FALSE];
    [self moveSignatures];
    NSMutableArray* components =  [[[[_stringDocumentPath componentsSeparatedByString:@"/"] lastObject] componentsSeparatedByString:@"."] mutableCopy];
    [components removeLastObject];
    NSString* docTitle = [components componentsJoinedByString:@"."];
    
    NSMutableDictionary* dictAllPDFsInfo = [[[NSUserDefaults standardUserDefaults] dictionaryForKey:@"PDF_Saved_Info"] mutableCopy];
    NSMutableArray* arrAllPDFFormsForDocument = [[dictAllPDFsInfo objectForKey:docTitle] mutableCopy];
    if(arrAllPDFFormsForDocument==nil){
        arrAllPDFFormsForDocument = [NSMutableArray new];
        [dictAllPDFsInfo setObject:arrAllPDFFormsForDocument forKey:docTitle];
    }
    [_dictSavedInfo setObject:_stringFormName forKey:@"Form_Name"];
    NSDictionary* formdictionary = [_dictSavedInfo copy];
    
    BOOL overwrite = FALSE;
    for (int i = 0; i < [arrAllPDFFormsForDocument count]; i++) {
        NSDictionary* savedDictionary = [arrAllPDFFormsForDocument objectAtIndex:i];
        if ([[savedDictionary objectForKey:@"Form_Name"] isEqualToString:_stringFormName]) {
            [arrAllPDFFormsForDocument replaceObjectAtIndex:i withObject:formdictionary];
            overwrite = TRUE;
            break;
        }
    }
    if (!overwrite)
        [arrAllPDFFormsForDocument insertObject:formdictionary atIndex:0];
    
    [dictAllPDFsInfo setObject:arrAllPDFFormsForDocument forKey:docTitle];
    [[NSUserDefaults standardUserDefaults] setObject:dictAllPDFsInfo forKey:@"PDF_Saved_Info"];
    [[NSUserDefaults standardUserDefaults] synchronize];
    
    [_icloudArchiver saveFormData];
    
}
-(void)moveSignatures{
    
    NSString* subFolder = [[_stringDocumentPath lastPathComponent]stringByDeletingPathExtension];
    NSString* fromPath = [self createFolderFromFullPath:[IMArcMediaViewer getTemporaryStorageLocationWithPDFFileName:subFolder]];
    NSString* toPath = [self createFolderFromFullPath:[IMArcMediaViewer getSavedFormDataLocationWithPDFFileName:subFolder savedFormName:_stringFormName]];
    NSFileManager *fm = [NSFileManager defaultManager];
    
    NSCharacterSet * NonNumbers = [[NSCharacterSet decimalDigitCharacterSet] invertedSet];
    
    NSArray *fileList = [fm contentsOfDirectoryAtPath:fromPath error:nil];
    for (NSString *s in fileList){
        if ([s rangeOfString:@"page"].location==0 && [[s stringByReplacingOccurrencesOfString:@"page" withString:@""] rangeOfCharacterFromSet:NonNumbers].location == NSNotFound) {
            NSString* fullFromPath = [NSString stringWithFormat:@"%@/%@",fromPath,s];
            NSString* fullToPath = [NSString stringWithFormat:@"%@/%@",toPath,s];
            NSError *err = nil;
            [fm removeItemAtPath:fullToPath error:&err];
            [fm copyItemAtPath:fullFromPath toPath:fullToPath error:&err];
        }
    }
}

-(void)clearSignatures{
    NSString* subFolder = [[_stringDocumentPath lastPathComponent]stringByDeletingPathExtension];
    NSString* fromPath = [self createFolderFromFullPath:[IMArcMediaViewer getTemporaryStorageLocationWithPDFFileName:subFolder]];
    NSFileManager *fm = [NSFileManager defaultManager];
    
    NSCharacterSet * NonNumbers = [[NSCharacterSet decimalDigitCharacterSet] invertedSet];
    
    NSArray *fileList = [fm contentsOfDirectoryAtPath:fromPath error:nil];
    for (NSString *s in fileList){
        if ([s rangeOfString:@"page"].location==0 && [[s stringByReplacingOccurrencesOfString:@"page" withString:@""] rangeOfCharacterFromSet:NonNumbers].location == NSNotFound) {
            NSString* fullFromPath = [NSString stringWithFormat:@"%@/%@",fromPath,s];
            NSError *err = nil;
            [fm removeItemAtPath:fullFromPath error:&err];
        }
    }
}

- (NSString* ) createFolder:(NSString*)folderName
{
    NSError* error;
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsDirectory = [paths objectAtIndex:0];
    NSString *dataPath = [documentsDirectory stringByAppendingPathComponent:folderName];
    
    if (![[NSFileManager defaultManager] fileExistsAtPath:dataPath])
        [[NSFileManager defaultManager] createDirectoryAtPath:dataPath withIntermediateDirectories:NO attributes:nil error:&error]; //Create folder
    return dataPath;
}
- (NSString* ) createFolderFromFullPath:(NSString*)folderPath
{
    NSError* error;
    if (![[NSFileManager defaultManager] fileExistsAtPath:folderPath])
        [[NSFileManager defaultManager] createDirectoryAtPath:folderPath withIntermediateDirectories:YES attributes:nil error:&error]; //Create folder
    return folderPath;
}
-(void) saveFormWithName:(NSString*)formName
{
    _stringTempFormName = [formName copy];
    
    NSMutableArray* components =  [[[[_stringDocumentPath componentsSeparatedByString:@"/"] lastObject] componentsSeparatedByString:@"."] mutableCopy];
    [components removeLastObject];
    NSString* docTitle = [components componentsJoinedByString:@"."];
    
    NSDictionary* dictAllPDFsInfo = [[NSUserDefaults standardUserDefaults] dictionaryForKey:@"PDF_Saved_Info"];
    NSArray* arrAllPDFFormsForDocument = [dictAllPDFsInfo objectForKey:docTitle];
    NSLog(@"%@", docTitle);
    if(![@"TEMPFORMFORROTATION" isEqualToString:formName]){
        for (int i = 0; i < [arrAllPDFFormsForDocument count]; i++) {
            NSDictionary* savedDictionary = [arrAllPDFFormsForDocument objectAtIndex:i];
            if ([[savedDictionary objectForKey:@"Form_Name"] isEqualToString:formName]) {
                
                _alertViewOverwrite = nil;
                _alertViewOverwrite= [[UIAlertView alloc] initWithTitle:@"Overwrite Existing?" message:[NSString stringWithFormat:@"Overwrite existing file named \"%@?\"", formName] delegate:self cancelButtonTitle:@"Cancel" otherButtonTitles:@"Save", nil];
                
                [_alertViewOverwrite show];
                return;
            }
        }
    }
    [self saveForm];
    
    if(![@"TEMPFORMFORROTATION" isEqualToString:formName]){
        [_icloudArchiver saveFormData];
    }
}

-(void) uploadFileToServer:(NSString*)fileName fileData:(NSData*)fileData{
    
    NSArray * allFormsArray = [_dictSavedInfo objectForKey:@"All_Forms_Array"];
    NSDictionary * thisForm = [allFormsArray objectAtIndex:0];
    NSArray * formFields = [thisForm objectForKey:@"SavedFormInfo"];
    
    NSString * timeStamp = [NSString stringWithFormat:@"%f",[[NSDate new] timeIntervalSince1970]];
    NSString * tempFilePath = [NSString stringWithFormat:@"%@/cms/toSend/%d-%@.pdf", [self documentsDirectory],arc4random()%10000000,timeStamp];
    [[NSFileManager defaultManager] createDirectoryAtPath:[NSString stringWithFormat:@"%@/cms/toSend", [self documentsDirectory]] withIntermediateDirectories:YES attributes:nil error:nil];
    [fileData writeToFile:tempFilePath atomically:YES];
    
    PDFUploadInfo * pdfInfo = [[PDFUploadInfo alloc] initWithFilePath:tempFilePath];
    
    if(self.setPDFUploadInfo!=nil){
        pdfInfo.uploadURL = [self.uploadURL absoluteString];
        self.setPDFUploadInfo(formFields,pdfInfo);
        [PDFUploader startUploadingPDFInfo:pdfInfo];
    }
    
}

- (NSString *)documentsDirectory
{
    return [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) objectAtIndex:0];
}

@end
