//
//  ICloudFormsImage.h
//  TestICloud
//
//  Created by Jeff Spaulding on 3/8/16.
//  Copyright © 2016 Jeff Spaulding. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface ICloudFormsImage : NSObject
@property (nonatomic, strong) NSString* path;
@property (nonatomic, strong) NSData * image;

-(id)initWithPath:(NSString*)path Image:(NSData*)image;
@end
