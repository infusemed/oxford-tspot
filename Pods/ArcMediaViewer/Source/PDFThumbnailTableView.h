//
//  ThumbnailTableView.h
//  TerumoTCVS
//
//  Created by Kevin Ray on 5/31/11.
//  Copyright 2011 Infuse Medical. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <QuartzCore/QuartzCore.h>

@protocol PDFThumbnailTableViewDelegate
-(void) jumpToPageFromThumbnail:(int)page;
@end

@interface PDFThumbnailTableView : UIView <UITableViewDelegate, UITableViewDataSource>
{
    // current pdf zoom scale
	CGFloat pdfScale;
	
	CGPDFPageRef page;
	CGPDFDocumentRef pdf;
	CGRect _pageRect;
	int _numberOfPages;
    UITableView* _table;
}
@property (nonatomic, strong) NSMutableArray* allThumbnails;
@property (assign) NSObject <PDFThumbnailTableViewDelegate>* delegate;

-(void) setRowHeight:(int)height;
-(void) addThumnail:(UIImage*)image;
-(void) changeTableSize:(CGSize)size;

@end
