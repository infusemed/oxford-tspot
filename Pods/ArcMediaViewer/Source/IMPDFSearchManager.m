//
//  IMPDFSearchManager.m
//  ARCMediaViewer
//
//  Created by Jeff Spaulding on 3/15/16.
//  Copyright © 2016 Infuse Medical. All rights reserved.
//

#import "IMPDFSearchManager.h"
#import "MFDocumentManager.h"

@interface IMPDFSearchOperation : NSOperation

@property (nonatomic, assign) MFDocumentManager *documentManagerToSearch;
@property (nonatomic, copy) NSString *searchString;
@property (nonatomic, strong) NSArray *searchResults;
@property (nonatomic) NSUInteger numberOfMatches;

@end

@implementation IMPDFSearchOperation

- (id) initWithDocumentManager: (MFDocumentManager*) documentManager searchString:(NSString *)searchString
{
	self = [super init];
	if (self)
	{
		self.documentManagerToSearch = documentManager;
		self.searchString = searchString;
		self.numberOfMatches = 0;
	}
	
	return self;
}

- (void) main
{
	if(![self isCancelled] && self.documentManagerToSearch) {
		NSString * cleanString = [_searchString copy];
		while([cleanString containsString:@"  "] && ![self isCancelled]){
			cleanString = [cleanString stringByReplacingOccurrencesOfString:@"  " withString:@" "];
		}
		
		if([cleanString isEqualToString:@""] || [cleanString isEqualToString:@" "] ){
			return;
		}
		
		NSComparisonResult(^searchComparator)(MFTextItem*, MFTextItem*) = ^NSComparisonResult(MFTextItem* obj1, MFTextItem* obj2) {
			if(![self isCancelled])
			{
				CGRect rect1 = CGPathGetBoundingBox(obj1.highlightPath);
				CGRect rect2 = CGPathGetBoundingBox(obj2.highlightPath);
				
				if(rect1.origin.y < rect2.origin.y){
					return NSOrderedAscending;
				}else if(rect1.origin.y > rect2.origin.y){
					return NSOrderedDescending;
				}else{
					if(rect1.origin.x < rect2.origin.x){
						return NSOrderedAscending;
					}else if(rect1.origin.x > rect2.origin.x){
						return NSOrderedDescending;
					}else{
						return NSOrderedSame;
					}
				}
			}
			return NSOrderedAscending;
		};
	
		NSMutableArray *allResults = [NSMutableArray array];
		for (int i = 0; i < [self.documentManagerToSearch numberOfPages]; i++) {
			NSArray *resultsForPage;
			if(![self isCancelled]) {
				resultsForPage = [self.documentManagerToSearch searchResultOnPage:i forSearchTerms: cleanString exactMatch: NO];
			}
			if(![self isCancelled]) {
				[allResults addObjectsFromArray:resultsForPage];
			}
			
			if(![self isCancelled] && i % 10 == 0) {
//				self.searchResults = [allResults sortedArrayUsingComparator:searchComparator];
				self.searchResults = [allResults sortedArrayUsingComparator:^NSComparisonResult(MFTextItem* obj1, MFTextItem* obj2) {
					if(![self isCancelled])
					{
						CGRect rect1 = CGPathGetBoundingBox(obj1.highlightPath);
						CGRect rect2 = CGPathGetBoundingBox(obj2.highlightPath);
						
						if(rect1.origin.y < rect2.origin.y || obj1.page < obj2.page){
							return NSOrderedAscending;
						}else if(rect1.origin.y > rect2.origin.y || obj1.page > obj2.page){
							return NSOrderedDescending;
						}else{
							if(rect1.origin.x < rect2.origin.x){
								return NSOrderedAscending;
							}else if(rect1.origin.x > rect2.origin.x){
								return NSOrderedDescending;
							}else{
								return NSOrderedSame;
							}
						}
					}
					return NSOrderedAscending;
				}];
			}
			self.numberOfMatches = allResults.count;
		}
		
		if(![self isCancelled])
		{
//			self.searchResults = [allResults sortedArrayUsingComparator:searchComparator];
			self.searchResults = [allResults sortedArrayUsingComparator:^NSComparisonResult(MFTextItem* obj1, MFTextItem* obj2) {
				if(![self isCancelled])
				{
					CGRect rect1 = CGPathGetBoundingBox(obj1.highlightPath);
					CGRect rect2 = CGPathGetBoundingBox(obj2.highlightPath);
					
					if(rect1.origin.y < rect2.origin.y || obj1.page < obj2.page){
						return NSOrderedAscending;
					}else if(rect1.origin.y > rect2.origin.y || obj1.page > obj2.page){
						return NSOrderedDescending;
					}else{
						if(rect1.origin.x < rect2.origin.x){
							return NSOrderedAscending;
						}else if(rect1.origin.x > rect2.origin.x){
							return NSOrderedDescending;
						}else{
							return NSOrderedSame;
						}
					}
				}
				return NSOrderedAscending;
			}];
		}
	}
}

@end

@interface IMPDFSearchManager(){
    NSMutableArray<MFTextItem*>* _searchResults;
    MFDocumentManager* _searchDocument;
	
	NSOperationQueue* _searchQueue;
	IMPDFSearchOperation *_currentSearch;
}
@end

@implementation IMPDFSearchManager

-(id)initWithFilePath:(NSString*)filePath{
    self = [super init];
    if(self){
        _searchResults = [NSMutableArray new];
        _searchDocument = [MFDocumentManager documentManagerWithFilePath:filePath];
		_searchQueue = [[NSOperationQueue alloc] init];
		_searchQueue.maxConcurrentOperationCount = 1;
    }
    return self;
}
-(MFTextItem*)currentItem{
	if(self.numberOfMatches == 0 || self.results.count <= 0){
		return nil;
	}
    return [self.results objectAtIndex:_currentIndex];
}

-(NSUInteger)numberOfMatches {
	return _currentSearch.numberOfMatches;
}


-(NSArray<MFTextItem*>*)results{
//    return _searchResults;
	return _currentSearch.searchResults;
}

-(NSArray<MFTextItem*>*)resultsForPage:(int)pageIndex{
    NSMutableArray* results = [NSMutableArray new];
    for (MFTextItem* item in self.results) {
        if(item.page == pageIndex){
            [results addObject:item];
        }
    }
    return results;
}


- (void) observeValueForKeyPath:(NSString *)keyPath ofObject:(id)object change:(NSDictionary<NSString *,id> *)change context:(void *)context
{
	if(object == _currentSearch) {
		[self willChangeValueForKey:@"numberOfMatches"];
		[self didChangeValueForKey:@"numberOfMatches"];
	}else{
		[super observeValueForKeyPath:keyPath ofObject:object change:change context:context];
	}
}

- (void) dealloc {
	if (_currentSearch) {
		[_currentSearch removeObserver:self forKeyPath:@"numberOfMatches"];
	}
}

-(BOOL)setSearchString:(NSString*)searchString{
	IMPDFSearchOperation *searchOp = [[IMPDFSearchOperation alloc] initWithDocumentManager:_searchDocument searchString:searchString];
	
	if(_currentSearch) {
		[_currentSearch removeObserver:self forKeyPath:@"numberOfMatches"];
		if (!_currentSearch.isFinished)
		{
			[_currentSearch cancel];
		}
	}
	
	[searchOp addObserver:self forKeyPath:@"numberOfMatches" options:NSKeyValueObservingOptionNew context:nil];
	_currentSearch = searchOp;
	[_searchQueue addOperation:searchOp];
	
	
	return YES;}
@end
