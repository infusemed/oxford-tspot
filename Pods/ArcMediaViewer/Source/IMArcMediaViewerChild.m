//
//  IMArcMediaViewerChild.m
//  ArcMediaViewer
//
//  Created by Infuse Medical on 2/14/14.
//  Copyright (c) 2014 Infuse Medical. All rights reserved.
//

#import "IMArcMediaViewerChild.h"

#define HEADER_HEIGHT 40

@interface IMArcMediaViewerChild ()

@end

@implementation IMArcMediaViewerChild


-(id)initWithFrame:(CGRect)frame Delegate:(id)delegate {
    self = [super init];
    if(self){
        _delegate = delegate;
        
        [self.view setBackgroundColor:[UIColor colorWithWhite:.9 alpha:1]];
        [self.view setClipsToBounds:YES];
        
        
        _buttonBack = [UIButton buttonWithType:UIButtonTypeSystem];
        [_buttonBack setFrame:CGRectMake(5, 5, 100, 30)];
        [_buttonBack setAccessibilityLabel:@"MediaViewerButtonBack"];
        [_buttonBack setTitle:@"Back" forState:UIControlStateNormal];
        [_buttonBack addTarget:self action:@selector(buttonBackPressed:) forControlEvents:UIControlEventTouchUpInside];
        //[_buttonBack.layer setShadowColor:[UIColor blackColor].CGColor];
        //[_buttonBack.layer setShadowOffset:CGSizeMake(2, 0)];
        //[_buttonBack.layer setShadowOpacity:1.0];
        //[_buttonBack.layer setShadowRadius:5];
        [self.view addSubview:_buttonBack];
        
        CGRect headerFrame = CGRectMake(0, -HEADER_HEIGHT, frame.size.width, HEADER_HEIGHT);
        _headerView =[[IMArcMediaHeaderView alloc] initWithFrame:headerFrame Delegate:self];
        [self.view insertSubview:_headerView belowSubview:_buttonBack];
        [self shouldShowMenu];
        [self setFrame:frame];
        [_headerView hideHeader:NO];
    }
    return self;
}


- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(BOOL)gestureRecognizer:(UIGestureRecognizer *)gestureRecognizer shouldRecognizeSimultaneouslyWithGestureRecognizer:(UIGestureRecognizer *)otherGestureRecognizer{
    return YES;
}
-(void)tapGestureRecognized:(UITapGestureRecognizer*)tap{
    if(_headerView.isHidden){
        [self showTitleBar];
    }else{
        [self hideTitleBar];
    }
}

-(void)setBlockEmailOption:(BOOL)blockEmailOption{
    _blockEmailOption = blockEmailOption;
    [self.headerView setFrame:self.headerView.frame];
}
-(void)setBlockMenu:(BOOL)blockMenu{
    _blockMenu = blockMenu;
    [self.headerView setFrame:self.headerView.frame];
}


-(BOOL)respondsToExtension:(NSString*)extension{
    return NO;
}
-(void)loadFileAtPath:(NSString*)filePath{
    
}
-(void)stop{
    
}

-(void)buttonBackPressed:(UIButton*)sender{
    if(_delegate!=nil){
        [_delegate closeMediaViewer];
    }
}

-(void)setFrame:(CGRect)frame{
    [self.view setFrame:frame];
    CGRect headerFrame = CGRectMake(0, 0, frame.size.width, HEADER_HEIGHT);
    [_headerView setFrame:headerFrame];
}


-(BOOL)shouldShowMenu{
    return NO;
}
-(NSArray*)optionsForMenu{
    return [NSArray new];
}
-(void)menuButtonPressed:(int)buttonIndex{
    
}

-(void)hideTitleBar{
    [_headerView hideHeader:YES];
}
-(void)showTitleBar{
    [_headerView showHeader:YES];
}

@end
