//
//  ICloudFormsImage.m
//  TestICloud
//
//  Created by Jeff Spaulding on 3/8/16.
//  Copyright © 2016 Jeff Spaulding. All rights reserved.
//

#import "ICloudFormsImage.h"

@implementation ICloudFormsImage

-(id)initWithPath:(NSString*)path Image:(NSData*)image{
    self= [super init];
    if(self){
        self.path = path;
        self.image = image;
    }
    return self;
}

- (void)encodeWithCoder:(NSCoder *)coder {
    [coder encodeObject:self.path forKey:@"path"];
    [coder encodeObject:self.image forKey:@"image"];
}

- (id)initWithCoder:(NSCoder *)coder  {
    self = [super init];
    
    if (self != nil) {
        self.path = [coder decodeObjectForKey:@"path"];
        self.image = [coder decodeObjectForKey:@"image"];
    }
    
    return self;
}
@end
