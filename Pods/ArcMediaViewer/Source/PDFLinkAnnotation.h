//
//  PDFLinkAnnotation.h
//  BalloonsPlus
//
//  Created by Kevin Ray on 7/14/11.
//  Copyright 2011 Infuse Medical. All rights reserved.
//

#import <Foundation/Foundation.h>

//id selfClass;
@interface PDFLinkAnnotation : NSObject {
    CGPDFDictionaryRef annotationDictionary;
    CGRect pdfRectangle;
}

@property (readonly, assign) CGRect pdfRectangle;
@property (nonatomic, retain) NSMutableDictionary *auxInfo;
@property (nonatomic, retain) NSMutableDictionary *tempDictionary;

- (id)initWithPDFDictionary:(CGPDFDictionaryRef)newAnnotationDictionary;
- (BOOL)hitTest:(CGPoint)point;
- (NSObject*)getLinkTarget:(CGPDFDocumentRef)document;
- (CGPDFArrayRef)findDestinationByName:(const char *)destinationName inDestsTree:(CGPDFDictionaryRef)node;
@end

