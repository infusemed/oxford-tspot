//
//  ICloudFormsDocument.m
//  TestICloud
//
//  Created by Jeff Spaulding on 3/8/16.
//  Copyright © 2016 Jeff Spaulding. All rights reserved.
//

#import "ICloudFormsDocument.h"
@interface ICloudFormsDocument(){
    NSString* documentsFolder;
}
@end

@implementation ICloudFormsDocument
-(id)initWithFileName:(NSString*)fileName{
    _fileName=fileName;
    NSURL *baseURL = [[NSFileManager defaultManager] URLForUbiquityContainerIdentifier:nil];
    
    if (baseURL) {
        NSURL *documentsURL = [baseURL URLByAppendingPathComponent:@"Documents"];
        NSURL *documentURL = [documentsURL URLByAppendingPathComponent:fileName];
        if (documentURL) {
            self = [super initWithFileURL:documentURL];
        }else{
            self = [super init];
        }
    }else{
        self = [super init];
    }
    if(self){
        
    }
    return self;
}

-(BOOL)loadFromContents:(id)contents ofType:(NSString *)typeName error:(NSError * _Nullable __autoreleasing *)outError{
    if ([contents length] > 0) {
        NSKeyedUnarchiver *unarchiver = [[NSKeyedUnarchiver alloc] initForReadingWithData:contents];
        self.formsDictionary = [unarchiver decodeObjectForKey:@"form_data"];
        self.formImages = [unarchiver decodeObjectForKey:@"form_images"];
        [unarchiver finishDecoding];
        
        [self correctImagePaths];
        
    } else {
        self.formsDictionary = [NSDictionary new];
        self.formImages = [NSMutableArray new];
    }
    return YES;
}
-(id)contentsForType:(NSString *)typeName error:(NSError * _Nullable __autoreleasing *)outError{
    NSMutableData *data = [[NSMutableData alloc] init];
    NSKeyedArchiver *archiver = [[NSKeyedArchiver alloc] initForWritingWithMutableData:data];
    [archiver encodeObject:self.formsDictionary forKey:@"form_data"];
    [archiver encodeObject:self.formImages forKey:@"form_images"];
    [archiver finishEncoding];
    
    return data;
}

-(void)updateModel{
    self.formsDictionary = [[NSUserDefaults standardUserDefaults] dictionaryForKey:@"PDF_Saved_Info"];
    
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    documentsFolder = [paths objectAtIndex:0];
    
    NSString* folderName = [documentsFolder stringByAppendingPathComponent:@"PDFSavedImages"];
    
    self.formImages =  [NSMutableArray new];
    
    // Enumerators are recursive
    NSDirectoryEnumerator *enumerator = [[NSFileManager defaultManager] enumeratorAtPath:folderName] ;
    
    NSString *filePath;
    
    while ((filePath = [enumerator nextObject]) != nil){
        NSString* fullFilePath = [folderName stringByAppendingPathComponent:filePath];
        NSData * data = [NSData dataWithContentsOfFile:fullFilePath];
        if(data !=nil){
            [self.formImages addObject:[[ICloudFormsImage alloc] initWithPath:filePath Image:data]];
        }
    }
}

-(void)restoreFormDataIfNeeded{
    if([[NSUserDefaults standardUserDefaults] objectForKey:@"ICLOUD_RESTORED_ONCE"] != nil){
        return;
    }
    [[NSUserDefaults standardUserDefaults] setBool:YES forKey:@"ICLOUD_RESTORED_ONCE"];
    if (self.formsDictionary) {
        [[NSUserDefaults standardUserDefaults] setObject:self.formsDictionary forKey:@"PDF_Saved_Info"];
    }
    
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    documentsFolder = [paths objectAtIndex:0];
    
    NSString* folderName = [documentsFolder stringByAppendingPathComponent:@"PDFSavedImages"];
    for (ICloudFormsImage * img in self.formImages) {
        NSString* fullFilePath = [folderName stringByAppendingPathComponent:img.path];
        
        [[NSFileManager defaultManager] createDirectoryAtPath:[fullFilePath stringByDeletingLastPathComponent]
                                 withIntermediateDirectories:YES
                                                  attributes:nil
                                                       error:nil];
        [img.image writeToFile:fullFilePath atomically:YES];
    }
}

-(void)saveWithCompletionHandler:(void (^)(BOOL success))completion{
    [self updateModel];
    
    //[self saveWithCompletionHandler:completion];
    
    NSURL *baseURL = [[NSFileManager defaultManager] URLForUbiquityContainerIdentifier:nil];
    NSURL *documentsURL = [baseURL URLByAppendingPathComponent:@"Documents"];
    NSURL *documentURL = [documentsURL URLByAppendingPathComponent:_fileName];
    [self saveToURL:documentURL forSaveOperation:UIDocumentSaveForOverwriting completionHandler:^(BOOL success) {
    }];
}
-(void)correctImagePaths{
    NSMutableDictionary * dict = [self.formsDictionary mutableCopy];
    [self correctImagePathsInDictionary:dict];
    self.formsDictionary = dict;
}
-(void)correctImagePathsInDictionary:(NSMutableDictionary*)dictionary{
    for (NSString* key in [dictionary allKeys]) {
        if ([dictionary[key] isKindOfClass:[NSArray class]]) {
            NSMutableArray* arr = [dictionary[key] mutableCopy];
            [self correctImagePathsInArray:arr];
            dictionary[key] = arr;
        }else if([dictionary[key] isKindOfClass:[NSDictionary class]]){
            NSMutableDictionary* dict = [dictionary[key] mutableCopy];
            [self correctImagePathsInDictionary:dict];
            dictionary[key] = dict;
        }else if([key isEqualToString:@"imageName"]){
            NSString* oldPath = dictionary[key];
            int index= (int)[oldPath rangeOfString:@"/Documents"].location;
            NSString * end = [oldPath substringFromIndex:index];
            
            NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
            documentsFolder = [paths objectAtIndex:0];
            
            NSString* newPath = [end stringByReplacingOccurrencesOfString:@"/Documents" withString:documentsFolder];
            dictionary[key] = newPath;
        }
    }
}
-(void)correctImagePathsInArray:(NSMutableArray*)array{
    for (int i = 0; i < array.count; i++) {
        if([array[i] isKindOfClass:[NSArray class]]){
            NSMutableArray * arr = [array[i] mutableCopy];
            [self correctImagePathsInArray:arr];
            array[i] = arr;
        }else if([array[i] isKindOfClass:[NSDictionary class]]){
            NSMutableDictionary * dict = [array[i] mutableCopy];
            [self correctImagePathsInDictionary:dict];
            array[i] = dict;
        }
    }
}
@end
