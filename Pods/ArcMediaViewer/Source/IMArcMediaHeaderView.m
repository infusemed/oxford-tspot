//
//  IMArcMediaHeaderViewController.m
//  ArcMediaViewer
//
//  Created by Infuse Medical on 2/14/14.
//  Copyright (c) 2014 Infuse Medical. All rights reserved.
//

#import "IMArcMediaHeaderView.h"

@interface IMArcMediaHeaderView (){
    int frameHeight;
    IMArcMediaHeaderMenuView * _menu;
}

@end

@implementation IMArcMediaHeaderView


-(id)initWithFrame:(CGRect)frame Delegate:(id)delegate{
    self = [super initWithFrame:frame];
    if(self){
        [self setBackgroundColor:[UIColor colorWithWhite:0.5 alpha:0.6]];
        if([[[UIDevice currentDevice] systemVersion] floatValue]>=7.f){
            self.opaque = NO;
            self.backgroundColor = [UIColor clearColor];
            
            UIToolbar *blurToolbar = [[UIToolbar alloc] initWithFrame:self.bounds];
            blurToolbar.autoresizingMask = self.autoresizingMask;
            [blurToolbar setAlpha:0.99];
            [self insertSubview:blurToolbar atIndex:0];
        }
        _delegate = delegate;
        _isHidden = YES;
        
        _labelTitle = [[UILabel alloc] init];
        [_labelTitle setTextAlignment:NSTextAlignmentCenter];
        [_labelTitle setBackgroundColor:[UIColor clearColor]];
        [_labelTitle setTextColor:[UIColor blackColor]];
        [self addSubview:_labelTitle];
        
        [self setFrame:frame];
    }
    return self;
}


-(void)buttonMenuPressed:(UIButton*)sender{
    if(UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad){
        UIActionSheet *popupQuery;
        popupQuery = [[UIActionSheet alloc] initWithTitle:@"Title" delegate:self cancelButtonTitle:nil destructiveButtonTitle:nil otherButtonTitles:nil];
        for (int i=0;i<[_delegate optionsForMenu].count;i++) {
            NSString *buttonName= [[_delegate optionsForMenu] objectAtIndex:i];
            [popupQuery addButtonWithTitle:buttonName];
        }
        [popupQuery addButtonWithTitle: @""];
        [popupQuery setCancelButtonIndex: popupQuery.numberOfButtons - 1];
        popupQuery.actionSheetStyle = UIActionSheetStyleBlackTranslucent;
        popupQuery.title = @"";
        [popupQuery showFromRect:sender.frame inView:self animated:YES];
    }else{
        _menu = [[IMArcMediaHeaderMenuView alloc] initWithFrame:self.superview.frame menuOptions:[_delegate optionsForMenu] fromPoint:sender.center delegate:self];
        [self.superview addSubview:_menu];
    }
}
-(void)setFrame:(CGRect)frame{
    [super setFrame:frame];
    frameHeight = frame.size.height;
    [_labelTitle setFrame:CGRectMake(100, 5, frame.size.width-210, frame.size.height-10)];
    int index = 0;
    if([[[UIDevice currentDevice] systemVersion] floatValue]>=7.f && self.subviews.count>0){
        [self.subviews[0] removeFromSuperview];
        UIToolbar *BlurToolbar = [[UIToolbar alloc] initWithFrame:self.bounds];
        BlurToolbar.autoresizingMask = self.autoresizingMask;
        [BlurToolbar setAlpha:0.99];
        [self insertSubview:BlurToolbar atIndex:0];
        index++;
    }
    
    if([_delegate shouldShowMenu]){
        if(_buttonMenu!=nil){
            [_buttonMenu removeFromSuperview];
            _buttonMenu = nil;
        }
        _buttonMenu = [UIButton buttonWithType:UIButtonTypeSystem];
        [_buttonMenu setTitle:@"Menu" forState:UIControlStateNormal];
        [_buttonMenu setAccessibilityLabel:@"MediaViewerButtonMenu"];
        [_buttonMenu addTarget:self action:@selector(buttonMenuPressed:) forControlEvents:UIControlEventTouchUpInside];
        [self insertSubview:_buttonMenu atIndex:index];
    }else{
        if(_buttonMenu!=nil){
            [_buttonMenu removeFromSuperview];
            _buttonMenu = nil;
        }
    }
    if(_buttonMenu!=nil){
        [_buttonMenu setFrame:CGRectMake(frame.size.width-105, 5, 100, frame.size.height-10)];
        [_delegate shouldShowMenu];
    }
}


-(void)setDocumentTitle:(NSString*)documentTitle{
    [_labelTitle setText:documentTitle];
}


-(void)showHeader:(BOOL)animated{
    CGRect frame = self.frame;
    frame.origin.y =0;
    if (animated) {
        [UIView animateWithDuration:0.4 animations:^{
            [super setFrame:frame];
        } completion:^(BOOL finished) {
            if(finished){
                _isHidden = NO;
            }
        }];
    }else{
        _isHidden = NO;
        [super setFrame:frame];
    }
}
-(void)hideHeader:(BOOL)animated{
    CGRect frame = self.frame;
    frame.origin.y =-frameHeight;
    if (animated) {
        [UIView animateWithDuration:0.4 animations:^{
            [super setFrame:frame];
        } completion:^(BOOL finished) {
            if(finished){
                _isHidden = YES;
            }
        }];
    }else{
        _isHidden = YES;
        [super setFrame:frame];
    }
}

-(void)hideMenu{
    if(_menu!=nil){
        [_menu removeFromSuperview];
        _menu  = nil;
    }
}

#pragma mark - IMArcMediaHeaderMenuDelegate methods
-(void)menuOptionSelectedAtIndex:(int)index{
    if(_menu!=nil){
        _menu  = nil;
    }
    if(_delegate!=nil){
        [_delegate menuButtonPressed:index];
    }
}

#pragma mark - UIActionSheetDelegate
-(void)actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex{
    if(buttonIndex==actionSheet.cancelButtonIndex){
        return;
    }
    if(_delegate!=nil){
        [_delegate menuButtonPressed:(int)buttonIndex];
    }
}

@end
