//
//  SignatureButton.h
//  BalloonsPlusTesting
//
//  Created by Kevin Ray on 2/27/13.
//  Copyright (c) 2013 Infuse Medical. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SignatureButton : UIButton

@end
