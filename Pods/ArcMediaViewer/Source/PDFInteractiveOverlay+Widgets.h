//
//  LowResView+Widgets.h
//  BalloonsPlus
//
//  Created by Kevin Ray on 8/8/11.
//  Copyright 2011 Infuse Medical. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "PDFInteractiveOverlay.h"


@interface PDFInteractiveOverlay (Widgets)

- (void)drawFormObjects;
- (void)drawFormObjectsOnPDF;
- (void)checkboxTouched:(UIButton *)btn;
- (void)radioButtonTouched:(UIButton *)btn;
- (void)loadPageWidgets:(CGPDFPageRef)page withIndexOffset:(int)indexOffset;
- (void)createPDFFile:(CGPDFPageRef)pageRef;
- (void)extractPDFDictionaryforPage:(CGPDFPageRef)pageRef;

+ (int)widgetCountForPage:(CGPDFPageRef)page;

@end
