//
//  IMArcVideoViewController.h
//  ArcMediaViewer
//
//  Created by Infuse Medical on 2/14/14.
//  Copyright (c) 2014 Infuse Medical. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "IMArcMediaViewerChild.h"
#import <MediaPlayer/MPMoviePlayerController.h>
@interface IMArcVideoViewController : IMArcMediaViewerChild
@property (nonatomic, strong) MPMoviePlayerController * video;

@end
