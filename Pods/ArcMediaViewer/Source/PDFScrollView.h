

#import <UIKit/UIKit.h>
//#import <libkern/OSMemoryNotification.h>
#import "PDFTiledView.h"
#import "CULSignatureView.h"
#import "PDFInteractiveOverlay.h"
#import "ImportedImageView.h"
#import "ImportedImageCloseBtn.h"

@class PDFTiledView;

@protocol PDFScrollViewDelegate
-(void) finishedLoading;
-(void) jumpToPage:(int)page;
-(void) keepTitleAlpha;
-(void) documentIsForm;
-(void) lockZoomAndScrolling;
-(void) unlockZoomAndScrolling;
@end

@interface PDFScrollView : UIView <UIGestureRecognizerDelegate, UIScrollViewDelegate, PDFInteractiveOverlayDelegate> {
	// The TiledPDFView that is currently front most
	PDFTiledView *pdfView;
	// The old TiledPDFView that we draw on top of when the zooming stops
	PDFTiledView *oldPDFView;
    
    __strong PDFInteractiveOverlay* interactiveOverlay;
	// A low res image of the PDF page that is displayed until the TiledPDFView
	// renders its content.
	UIImageView *_viewReallyLowRes;
    
	NSThread* _thread;
	
	// current pdf zoom scale
	CGFloat pdfScale;
    CGFloat oldPDFScale;
    CGPoint _pointPrev;
	float _maximumZoomScale;
    float _minimumZoomScale;
    float _lastScaleFactor;
    int _intImportedImageCount;
    int widgetOffset;
    BOOL _isLoadingForEmail;
    
	CGPDFPageRef page;
	CGPDFDocumentRef pdf;
	CGRect _pageRect;
    
    UIActivityIndicatorView* _spinningWheel;
    
    NSNotificationCenter* _nc;
    NSMutableArray* _arrImportedImagesInfo;
    NSMutableArray * _arrImportedImages;
    
    UIPinchGestureRecognizer *_pinchGestureImportedImage;
    UIPanGestureRecognizer *_panTranslateImportedImage;
    
    NSDictionary* _defaultValues;
    NSArray* _fieldNames;
    NSArray*_disabledFields;
}

//@property (assign) UIImage* lowResImage;
@property (assign) NSObject <PDFScrollViewDelegate>* delegate;
@property (nonatomic,strong) UIImageView* backgroundImageView;
@property (nonatomic, strong) IMPDFSearchManager *searchDocument;
@property (nonatomic,strong) UIScrollView* scrollView;
@property (nonatomic,strong) NSMutableDictionary* dictSavedDocumentInfo;
@property (nonatomic,strong) NSString* stringFileName;
@property (nonatomic,strong) NSString* stringFormName;
@property (nonatomic,strong) SignatureStyler styleSignature;
@property (nonatomic) BOOL formEnabled;
@property (nonatomic) CGFloat emailScale;

- (id)initWithFrame:(CGRect)frame PageNumber:(int)pageNumber Path:(NSString*)path ScrollLeftRight:(BOOL)scrollLeftRight formEnabled:(BOOL)formEnabled minZoomScale:(float)minZoomScale;

-(void) renderLowResForViewing;
-(void) renderLowResForEmailing;
-(void) removeHighlightsForSearchResults;
-(void) createAndSaveFormImage;
-(void) drawReallyLowResFirst:(UIImage*)thumbnail;
-(void) centerPDF;
-(void) stopThread;
-(void) saveFormData;
-(void) stopVideos;
-(void) clearFormDataWithDefaultValues:(NSDictionary*)defaultValues andFieldNames:(NSArray*)fieldNames;
-(void) loadDefaultValues:(NSDictionary*)defaultValues andFieldNames:(NSArray*)fieldNames;
-(void) loadDisabledFields:(NSArray*)disabledFields;
-(void) dismissKeyboard;
-(ImportedImageView*) importImage:(UIImage*)image withOrigin:(CGPoint)origin;
-(void)saveImportedImage:(ImportedImageView*)importedImage;
-(void)saveAllImportedImagesInfoForFormName:(NSString*)formName forTemp:(bool)forTemp;
-(void) importImage:(UIImage*)image WithName:(NSString*)imageName;
- (void) createFolder:(NSString*)folderName;

@property (assign) int index;
@property (assign) BOOL	pageLoaded;
@property (assign) BOOL	stopEverything;
@end
