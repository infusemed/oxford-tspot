//
//  ThumbnailTableView.m
//  TerumoTCVS
//
//  Created by Kevin Ray on 5/31/11.
//  Copyright 2011 Infuse Medical. All rights reserved.
//

#import "PDFThumbnailTableView.h"


@implementation PDFThumbnailTableView

@synthesize allThumbnails = _allThumnails;
@synthesize delegate;

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Open the PDF document
        //NSURL *pdfURL = [[NSBundle mainBundle] URLForResource:@"TestPage.pdf" withExtension:nil];
        
        _allThumnails = [[NSMutableArray alloc] init];
        
        _table=[[UITableView alloc] initWithFrame:CGRectMake(0, 0, frame.size.width, frame.size.height)];
        _table.dataSource=self;
        _table.delegate=self;
        [self addSubview:_table];
        _table.rowHeight=200;
        _table.backgroundColor = [UIColor colorWithRed:221.0/256.0 green:223.0/256.0 blue:228.0/256.0 alpha:1];
        
    }
    return self;
}

-(void) changeTableSize:(CGSize)size
{
    _table.frame = CGRectMake(_table.frame.origin.x, _table.frame.origin.y, size.width, size.height);
}
/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect
{
    // Drawing code
}
*/
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView 
{
    return 1;
}


- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section 
{
    return [_allThumnails count];
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath 
{
    

    UITableViewCell* cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:nil];
    UIImageView* image = [[UIImageView alloc] initWithImage:[_allThumnails objectAtIndex:[indexPath row]]];
    image.frame = CGRectMake(20, 20, image.frame.size.width, image.frame.size.height);
    
	image.layer.masksToBounds = NO;
    image.layer.cornerRadius = 2; // if you like rounded corners
    image.layer.shadowOffset = CGSizeMake(-5, 5);
    image.layer.shadowRadius = 5;
    image.layer.shadowOpacity = 0.5;
    
    UIBezierPath *shadowPath = [UIBezierPath bezierPathWithRect:image.bounds];
    image.layer.shadowPath = shadowPath.CGPath;
    
    
    [cell addSubview:image];
    
    cell.contentView.backgroundColor = [UIColor colorWithRed:221.0/256.0 green:223.0/256.0 blue:228.0/256.0 alpha:1];
    cell.backgroundColor = [UIColor colorWithRed:221.0/256.0 green:223.0/256.0 blue:228.0/256.0 alpha:1];
    
    //[cell sendSubviewToBack:backgroundImageView];
    UILabel* pageNum = [[UILabel alloc] initWithFrame:CGRectMake(0, image.frame.size.height + image.frame.origin.y + 5, 200, 15)];
	//pageNum.center = CGPointMake(150, 100);
	pageNum.backgroundColor = [UIColor clearColor];
	pageNum.textAlignment = NSTextAlignmentCenter;
	pageNum.textColor = [UIColor blackColor];
    pageNum.shadowOffset = CGSizeMake(1, 1);
	pageNum.font = [UIFont fontWithName:@"HelveticaNeue-Bold" size:15];
	pageNum.text = [NSString stringWithFormat:@"%i", (int)([indexPath row] + 1)];
	[cell addSubview:pageNum];
	
    return cell;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath;
{
    UIImageView* image = [[UIImageView alloc] initWithImage:[_allThumnails objectAtIndex:[indexPath row]]];
    return image.frame.size.height + 40;
}


#pragma mark -
#pragma mark Table view selection

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath 
{
	
    [[self delegate] jumpToPageFromThumbnail:(int)([indexPath row]+1)];
	[tableView deselectRowAtIndexPath:indexPath animated:TRUE];
}

-(void) addThumnail:(UIImage*)image
{
    [_allThumnails addObject:image];
    [_table reloadData];
}
-(void) setRowHeight:(int)height
{
    _table.rowHeight = height;
    [_table reloadData];
}

- (void)dealloc
{
    //for (
    [_allThumnails removeAllObjects];
    //CGPDFDocumentRelease(pdf), pdf = NULL;
}

@end
