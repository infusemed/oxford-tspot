//
//  SignatureButton.m
//  BalloonsPlusTesting
//
//  Created by Kevin Ray on 2/27/13.
//  Copyright (c) 2013 Infuse Medical. All rights reserved.
//

#import "SignatureButton.h"

@implementation SignatureButton

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
    }
    return self;
}

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect
{
    // Drawing code
}
*/

@end
