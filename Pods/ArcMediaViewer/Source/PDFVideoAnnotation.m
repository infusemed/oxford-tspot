//
//  PDFVideoAnnotation.m
//  BalloonsPlusTesting
//
//  Created by Kevin Ray on 4/17/13.
//  Copyright (c) 2013 Infuse Medical. All rights reserved.
//

#import "PDFVideoAnnotation.h"

@implementation PDFVideoAnnotation

@synthesize pdfRectangle;
@synthesize stringName;
@synthesize stringPath;
@synthesize stringThumbPath;

- (id)initWithPDFDictionary:(CGPDFDictionaryRef)newAnnotationDictionary videoName:(NSString*)videoName videoPath:(NSString*)videoPath {
    self = [super init];
    if (self) {
        
        
        //** For extracting what is in the pdf **//
        //auxInfo = [[NSMutableDictionary alloc] init];
        //tempDictionary = auxInfo;
        ////CFDictionaryRef newDictionary = (CFDictionaryRef)[self auxInfo];
        //[self extractPDFDictionary];
        
        self.stringName = videoName;
        self.stringPath = videoPath;
        
        self.stringThumbPath = [NSTemporaryDirectory() stringByAppendingPathComponent:[NSString stringWithFormat:@"thumb%@.jpg", videoName]];
        
        // Normalize and cache the annotation rect definition for faster hit testing.
        CGPDFArrayRef rectArray = NULL;
        CGPDFDictionaryGetArray(newAnnotationDictionary, "Rect", &rectArray);
        if (rectArray != NULL) {
            CGPDFReal llx = 0;
            CGPDFArrayGetNumber(rectArray, 0, &llx);
            CGPDFReal lly = 0;
            CGPDFArrayGetNumber(rectArray, 1, &lly);
            CGPDFReal urx = 0;
            CGPDFArrayGetNumber(rectArray, 2, &urx);
            CGPDFReal ury = 0;
            CGPDFArrayGetNumber(rectArray, 3, &ury);
            
            if (llx > urx) {
                CGPDFReal temp = llx;
                llx = urx;
                urx = temp;
            }
            if (lly > ury) {
                CGPDFReal temp = lly;
                lly = ury;
                ury = temp;
            }
            
            pdfRectangle = CGRectMake(llx, lly, urx - llx, ury - lly);
        }
        
        
    }
    
    return self;
}



@end
