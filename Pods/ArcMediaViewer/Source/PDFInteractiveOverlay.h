//
//  InteractiveOverlay.h
//  BalloonsPlus
//
//  Created by Kevin Ray on 8/12/11.
//  Copyright 2011 Infuse Medical. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CULSignatureView.h"
#import "UIPlaceHolderTextView.h"
#import <MediaPlayer/MPMoviePlayerController.h>
#import "IMPDFSearchManager.h"
@protocol PDFInteractiveOverlayDelegate
-(void) jumpToPage:(int)pageNumber;
-(void) isEditingSignature;
-(void) doneEditingSignature;
-(void) keepTitleAlpha;
-(void) documentIsForm;
@end


@interface PDFInteractiveOverlay : UIView <UITextViewDelegate, UITextFieldDelegate>{
    
	CGPDFPageRef pdfPage;
    CGRect pageRenderRect;
    NSMutableArray *pageLinks;
    NSMutableArray *pageWidgets;//forms text fields, checkboxes, etc.
    NSMutableArray *pageVideos;
    NSMutableArray* _arrAllFormObjects;
    NSMutableArray* _arrHighlights;
    NSMutableArray* _defaultValues;
    NSMutableArray* _radioButtons;
	NSArray *selections;
    //NSMutableDictionary *auxInfo;
    CULSignatureView* signature;
    
    UIButton* signatureBtn;
    MPMoviePlayerController* _videoPlayer;
}

typedef void (^SignatureStyler)(UIButton*buttonExit, CULSignatureView*signatureView, UIButton*buttonDone, UIButton*buttonClear);

@property (assign) NSObject <PDFInteractiveOverlayDelegate>* delegate;
@property (nonatomic, strong) NSMutableArray *arrSavedFormInfo;
@property (nonatomic, strong) NSArray *disabledFields;
@property (nonatomic, strong) IMPDFSearchManager *searchDocument;
@property (nonatomic, weak) UIView *viewParent;
@property (nonatomic, strong) NSMutableDictionary *tempPageDictionary;
@property (nonatomic, strong) NSMutableDictionary *pageDictionary;
@property (nonatomic, strong) NSMutableArray *radioButtonParents;
@property (nonatomic, strong) UIButton * buttonExitSignature;
@property (nonatomic, assign) float scale;
@property (nonatomic, assign) BOOL boolEmailing;
@property (nonatomic, copy) NSString* stringFileName;
@property (nonatomic, copy) NSString* stringFormName;
@property (nonatomic) BOOL formEnabled;
@property (nonatomic, strong) SignatureStyler styleSignature;
@property (strong, nonatomic) UIToolbar *textInputAccessoryView;
@property (strong, nonatomic) UIBarButtonItem *textInputNextBtn;
@property (strong, nonatomic) UIBarButtonItem *textInputPrevBtn;
@property (readonly, assign, nonatomic) int pageNumber;

-(void) removeHighlights;
- (void)setPage:(CGPDFPageRef)newPage pageNumber:(int)pageNumber widgetIndexOffset:(int)offset;
-(void) dismissKeyboard;
-(void) saveFormData;
-(void) clearFormDataWithDefaultValues:(NSDictionary*)defaultValues andFieldNames:(NSArray*)fieldNames;
-(void) loadDefaultValues:(NSDictionary*)defaultValues andFieldNames:(NSArray*)fieldNames;
-(void) disableFields:(NSArray*)disabledFields;
-(void) saveImagesWithFormName:(NSString*)formName forTemp:(BOOL)forTemp;
-(void) stopVideos;
@end
