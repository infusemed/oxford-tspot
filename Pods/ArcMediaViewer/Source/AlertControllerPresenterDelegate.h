//
//  AlertControllerPresenterDelegate.h
//  TestICloud
//
//  Created by Jeff Spaulding on 3/8/16.
//  Copyright © 2016 Jeff Spaulding. All rights reserved.
//

#ifndef AlertControllerPresenterDelegate_h
#define AlertControllerPresenterDelegate_h


@protocol AlertControllerPresenterDelegate
-(void)presentAlertController:(UIAlertController*)ac;
@end

#endif /* AlertControllerPresenterDelegate_h */
