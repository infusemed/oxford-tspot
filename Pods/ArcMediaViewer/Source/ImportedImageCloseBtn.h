//
//  ImportedImageCloseBtn.h
//  BalloonsPlus
//
//  Created by Kevin Ray on 7/3/12.
//  Copyright (c) 2012 Infuse Medical. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ImportedImageCloseBtn : UIButton

@property (assign) UIImageView* importedImage;
@end
