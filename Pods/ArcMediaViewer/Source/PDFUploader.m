//
//  PDFUploader.m
//  BostonScientific
//
//  Created by Infuse Medical on 3/6/14.
//  Copyright (c) 2014 Dave Stevenson. All rights reserved.
//

#import "PDFUploader.h"
#import "PDFUploadInfo.h"
#import <AFNetworking/AFNetworking.h>

@implementation PDFUploader{
    BOOL isRunning;
    NSMutableArray * _operationManagers;
}
static PDFUploader* sharedUploader = nil;
- (instancetype)init
{
    self = [super init];
    if (self) {
        _operationManagers = [NSMutableArray new];
        
    }
    return self;
}
+(void)startUploading{
    if(sharedUploader==nil){
        sharedUploader = [[PDFUploader alloc]init];
    }
    [sharedUploader runUploader];
}

+(void)startUploadingPDFInfo:(PDFUploadInfo*)pdfInfo{
    NSMutableArray * queue = [[[NSUserDefaults standardUserDefaults] objectForKey:@"pdfSendQueue"] mutableCopy];
    if(queue==nil){
        queue = [NSMutableArray new];
    }
    [queue addObject:[NSKeyedArchiver archivedDataWithRootObject:pdfInfo]];
    [[NSUserDefaults standardUserDefaults] setObject:queue forKey:@"pdfSendQueue"];
    if(sharedUploader==nil){
        sharedUploader = [[PDFUploader alloc]init];
        [sharedUploader runUploader];
    }else{
        [sharedUploader uploadFileWithPDFUploadInfo:pdfInfo];
    }
}
-(void)runUploader{
    if(isRunning){
        return;
    }
    isRunning= YES;
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
        NSMutableArray * queue = [[[NSUserDefaults standardUserDefaults] objectForKey:@"pdfSendQueue"] mutableCopy];
        if(queue!=nil){
            for (NSData * pdfData in queue) {
                PDFUploadInfo * pdfInfo= (PDFUploadInfo * )[NSKeyedUnarchiver unarchiveObjectWithData:pdfData];
                [self uploadFileWithPDFUploadInfo:pdfInfo];
            }
        }
        isRunning = NO;
    });
}

-(void)uploadFileWithPDFUploadInfo:(PDFUploadInfo *)pdfInfo{
    if (![[NSFileManager defaultManager] fileExistsAtPath:pdfInfo.file_path]) {
        return;
    }
    NSString * base64File = [[NSData dataWithContentsOfFile:pdfInfo.file_path] base64EncodedStringWithOptions:NSDataBase64EncodingEndLineWithLineFeed];
    
    NSDictionary * params = @{@"company": pdfInfo.company,
                              @"key": pdfInfo.key,
                              @"config_id": [NSNumber numberWithInt:pdfInfo.config_id],
                              @"folder_name": pdfInfo.folder_name,
                              @"file_name": pdfInfo.file_name,
                              @"file": base64File};
    
    
    AFHTTPRequestOperationManager * operationManager = [self operationManagerForURL:[NSURL URLWithString:pdfInfo.uploadURL]];
    AFSecurityPolicy *policy = [AFSecurityPolicy policyWithPinningMode:AFSSLPinningModeNone];
    [policy setAllowInvalidCertificates:YES];
    [policy setValidatesDomainName:NO];
    [operationManager setSecurityPolicy:policy];
    
    [operationManager PUT:pdfInfo.uploadURL parameters:params success:^(AFHTTPRequestOperation *operation, id responseObject) {
        NSLog(@"success");
        if([responseObject isKindOfClass:[NSDictionary class]]){
            if ([[((NSDictionary*)responseObject) objectForKey:@"success"] intValue]==1) {
                if (pdfInfo.showCompletionMessage) {
                    dispatch_async(dispatch_get_main_queue(), ^{
                        [[[UIAlertView alloc] initWithTitle:@"Upload Complete" message:[NSString stringWithFormat:@"'%@' has been uploaded successfully.",pdfInfo.file_name] delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil] show];
                    });
                }
                [self deletePDFInfoFromQueue:pdfInfo];
            }
        }
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        NSLog(@"FAIL");
        if (pdfInfo.showFailureMessage) {
            dispatch_async(dispatch_get_main_queue(), ^{
                [[[UIAlertView alloc] initWithTitle:@"Upload Failed" message:[NSString stringWithFormat:@"'%@' has been added to the queue for later.",pdfInfo.file_name] delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil] show];
            });
        }
    }];
}
-(AFHTTPRequestOperationManager*)operationManagerForURL:(NSURL*)url{
    NSString *baseURL =[NSString stringWithFormat:@"%@://%@", [url scheme], [url host]];
    for (AFHTTPRequestOperationManager * manager in _operationManagers) {
        if([[manager baseURL] isEqual:baseURL]){
            return manager;
        }
    }
    AFHTTPRequestOperationManager * operationManager = [[AFHTTPRequestOperationManager alloc] initWithBaseURL:[NSURL URLWithString:baseURL]];
    AFSecurityPolicy *policy = [AFSecurityPolicy policyWithPinningMode:AFSSLPinningModeNone];
    [policy setAllowInvalidCertificates:YES];
    [policy setValidatesDomainName:NO];
    [operationManager setSecurityPolicy:policy];
    [_operationManagers addObject:operationManager];
    [operationManager setRequestSerializer:[[AFJSONRequestSerializer alloc] init]];
    operationManager.securityPolicy.allowInvalidCertificates = YES;
    return operationManager;
}
-(void)deletePDFInfoFromQueue:(PDFUploadInfo*)pdfInfo{
    
    NSMutableArray * queue = [[[NSUserDefaults standardUserDefaults] objectForKey:@"pdfSendQueue"] mutableCopy];
    for (int i = 0; i<queue.count; i++) {
        NSData* pdfData = (NSData*)queue[i];
        PDFUploadInfo * pdfInfo2= (PDFUploadInfo * )[NSKeyedUnarchiver unarchiveObjectWithData:pdfData];
        if([pdfInfo2.file_path isEqual:pdfInfo.file_path]){
            [[NSFileManager defaultManager] removeItemAtPath:pdfInfo.file_path error:nil];
            [queue removeObjectAtIndex:i];
            [[NSUserDefaults standardUserDefaults] setObject:queue forKey:@"pdfSendQueue"];
        }
    }
}
@end
