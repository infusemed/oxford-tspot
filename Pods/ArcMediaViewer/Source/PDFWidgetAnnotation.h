//
//  PDFWidgetAnnotation.h
//  BalloonsPlus
//
//  Created by Kevin Ray on 8/8/11.
//  Copyright 2011 Infuse Medical. All rights reserved.
//

#import <Foundation/Foundation.h>

typedef enum {
    WidgetTypeNone = 0,
    WidgetTypeTextField =1,
    WidgetTypeTextView =2,
    WidgetTypeLinkBtn =3,
    WidgetTypeCheckbox =4,
    WidgetTypeRadioBtn =5,
    WidgetTypeSignature =6,
}WidgetType;
typedef enum {
    WidgetTextAlignmentLeft = 0,
    WidgetTextAlignmentCenter,
    WidgetTextAlignmentRight,
}WidgetTextAlignment;


@interface PDFWidgetAnnotation : NSObject {
    CGPDFDictionaryRef annotationDictionary;
    CGRect pdfRectangle;
    WidgetType _widgetType;
}

@property (assign) int pageNum;
@property (readonly, assign) CGRect pdfRectangle;
@property (readonly, assign) int fontSize;
@property (nonatomic, assign) int widgetIndex;
@property (readonly, retain) NSString* defaultText;
@property (readonly, assign) WidgetType widgetType;
@property (readonly, assign) WidgetTextAlignment widgetTextAlignment;
@property (nonatomic, retain) NSMutableDictionary *auxInfo;
@property (nonatomic, retain) NSMutableDictionary *tempDictionary;
@property (nonatomic, retain) NSString * radioButtonParentName;
@property (nonatomic, assign) BOOL checkedByDefault;
@property (nonatomic, assign) BOOL hasDefaultCheckValue;
@property (nonatomic, strong) NSString* fieldName;
@property (nonatomic, strong) NSString* fieldValue;
@property (nonatomic, assign) BOOL isRequired;
@property (nonatomic, retain) NSObject *linkTarget;

- (id)initWithPDFDictionary:(CGPDFDictionaryRef)newAnnotationDictionary PDFPageRef:(CGPDFPageRef)pageRef pageNum:(int)pageNumber;
- (BOOL)hitTest:(CGPoint)point;
- (CGPDFArrayRef)findDestinationByName:(const char *)destinationName inDestsTree:(CGPDFDictionaryRef)node;
- (void)getWidgetType:(CGPDFPageRef)pageRef;
@end
