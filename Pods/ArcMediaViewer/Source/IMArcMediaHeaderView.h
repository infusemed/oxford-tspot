//
//  IMArcMediaHeaderViewController.h
//  ArcMediaViewer
//
//  Created by Infuse Medical on 2/14/14.
//  Copyright (c) 2014 Infuse Medical. All rights reserved.
//

#import "IMArcMediaHeaderMenuView.h"

@protocol IMArcMediaHeaderViewDelegate <NSObject>
-(BOOL)shouldShowMenu;
-(NSArray*)optionsForMenu;
-(void)menuButtonPressed:(int)buttonIndex;
@end

@interface IMArcMediaHeaderView : UIView<IMArcMediaHeaderMenuDelegate,UIActionSheetDelegate>
@property (nonatomic, assign, readonly) BOOL isHidden;
@property (nonatomic, strong) UIButton* buttonMenu;
@property (nonatomic, strong) UILabel * labelTitle;
@property (nonatomic,assign) id<IMArcMediaHeaderViewDelegate>delegate;

-(id)initWithFrame:(CGRect)frame Delegate:(id)delegate;
-(void)setFrame:(CGRect)frame;
-(void)setDocumentTitle:(NSString*)documentTitle;
-(void)hideMenu;
-(void)showHeader:(BOOL)animated;
-(void)hideHeader:(BOOL)animated;
@end
