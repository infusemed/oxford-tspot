//
//  TiledPDFView+Widgets.m
//  WidgetsNavigation
//
//  Created by Sorin Nistor on 6/21/11.
//  Copyright 2011 iPDFdev.com. All rights reserved.
//

#import "PDFInteractiveOverlay+Links.h"
#import "PDFInteractiveOverlay+Widgets.h"
#import "PDFWidgetAnnotation.h"
#import "IMArcMediaViewer.h"
#import "SignatureButton.h"

id selfClass;

@implementation PDFInteractiveOverlay (Widgets)

- (void) drawFormObjects
{
    self.buttonExitSignature = nil;
	pageRenderRect = self.bounds;//[PDFPageRenderer renderPage: pdfPage inContext: context inRectangle: self.bounds];
    _radioButtons = [NSMutableArray new];
    self.radioButtonParents = [NSMutableArray new];
    
    BOOL isForm = FALSE;
    UIColor *widgetBackgroundColor = [UIColor colorWithRed:0.3 green:.4 blue:1.0 alpha:.2];
    
    if (self.formEnabled) {
        [self createInputAccessoryView];
        if ([pageWidgets count] > 0) {
            isForm = TRUE;
        }
        
        if ([pageWidgets count] != [self.arrSavedFormInfo count])
        {
            [self.arrSavedFormInfo removeAllObjects];
            for (int i = 0; i < [pageWidgets count]; i++)
                [self.arrSavedFormInfo addObject:@""];
        }
        for (int i = 0; i < [pageWidgets count]; i++)
        {
            PDFWidgetAnnotation *widgetAnnotation = [pageWidgets objectAtIndex: i];
            if (widgetAnnotation.widgetType == WidgetTypeNone) {
                [_arrAllFormObjects addObject:@"Test"];
                continue;
            }
            
            CGPoint pt1 = [self convertPDFPointToViewPoint: widgetAnnotation.pdfRectangle.origin];
            CGPoint pt2 = CGPointMake(
                                      widgetAnnotation.pdfRectangle.origin.x + widgetAnnotation.pdfRectangle.size.width,
                                      widgetAnnotation.pdfRectangle.origin.y + widgetAnnotation.pdfRectangle.size.height);
            pt2 = [self convertPDFPointToViewPoint: pt2];
            
            
            switch (widgetAnnotation.widgetType) {
                case WidgetTypeTextField://TextField
                {
                    int fontSize = widgetAnnotation.fontSize;
                    
                    UITextField* textfield = [[UITextField alloc] initWithFrame:CGRectMake((int)pt1.x+2, (int)pt1.y, (int)pt2.x - (int)pt1.x, (int)pt2.y - (int)pt1.y)];
                    
                    [textfield setAccessibilityLabel:widgetAnnotation.fieldName];
                    if (fontSize == 0) {
                        fontSize = textfield.frame.size.height * .8;
                    }
                    
                    if (widgetAnnotation.widgetTextAlignment == WidgetTextAlignmentCenter) {
                        textfield.textAlignment = NSTextAlignmentCenter;
                    }
                    else if (widgetAnnotation.widgetTextAlignment == WidgetTextAlignmentRight) {
                        textfield.textAlignment = NSTextAlignmentRight;
                    }
                    else {
                        textfield.textAlignment = NSTextAlignmentLeft;
                    }
                    textfield.borderStyle = UITextBorderStyleNone;
                    textfield.textColor = [UIColor blackColor]; //text color
                    textfield.font = [UIFont fontWithName:@"Helvetica" size:fontSize *self.scale];  //font size
                    if (!self.boolEmailing) {
                        textfield.backgroundColor = widgetBackgroundColor; //background color
                    }
                    textfield.autocorrectionType = UITextAutocorrectionTypeNo;	// no auto correction support
                    textfield.keyboardType = UIKeyboardTypeDefault;  // type of the keyboard
                    textfield.returnKeyType = UIReturnKeyDone;  // type of the return key
                    textfield.contentVerticalAlignment = UIControlContentVerticalAlignmentCenter;
                    textfield.text = [self.arrSavedFormInfo objectAtIndex:i];
                    if ([textfield.text isEqualToString:@""]) {
                        if(_defaultValues!=nil && _defaultValues.count > widgetAnnotation.widgetIndex && [_defaultValues objectAtIndex:widgetAnnotation.widgetIndex]!=nil){
                            textfield.text = [_defaultValues objectAtIndex:widgetAnnotation.widgetIndex];
                        }
                    }
                    if (self.disabledFields != nil && self.disabledFields.count>widgetAnnotation.widgetIndex) {
                        if ([[self.disabledFields objectAtIndex:widgetAnnotation.widgetIndex] boolValue]) {
                            textfield.backgroundColor = [UIColor clearColor];
                            [textfield setUserInteractionEnabled:false];
                        }
                    }
                    textfield.placeholder = widgetAnnotation.defaultText;
                    textfield.delegate = self;
                    textfield.tag = i;
                    [self addSubview:textfield];
                    [_arrAllFormObjects addObject:textfield];
                    
                    [self sizeToFitTextField:textfield newText:textfield.text];
                }
                    break;
                case WidgetTypeTextView:
                {
                    int fontSize = widgetAnnotation.fontSize*1.3;
                    
                    if ([[self.arrSavedFormInfo objectAtIndex:i] isKindOfClass:[NSDictionary class]]) {
                        NSDictionary *dict = [self.arrSavedFormInfo objectAtIndex:i];
                        fontSize = [[dict objectForKey:@"fontSize"] intValue];
                    } else if (![[self.arrSavedFormInfo objectAtIndex:i] isEqualToString:@""]) {
                        fontSize = [[[[self.arrSavedFormInfo objectAtIndex:i] componentsSeparatedByString:@"!)@(#*"] lastObject] intValue];
                    }
                    
                    UIPlaceHolderTextView* textView = [[UIPlaceHolderTextView alloc] initWithFrame:CGRectMake((int)pt1.x, (int)pt1.y, (int)pt2.x - (int)pt1.x, (int)pt2.y - (int)pt1.y)];
                    if (fontSize == 0) {
                        fontSize = ((textView.frame.size.height - (textView.textContainerInset.top + textView.textContainerInset.bottom)) * 0.9) / 2; // Make sure at least two lines fit without the text having to shrink
                    }
                    
                    [textView setAccessibilityLabel:widgetAnnotation.fieldName];
                    textView.fontSize = fontSize;
                    [textView setFont:[UIFont systemFontOfSize: fontSize * self.scale]];
                    [textView setBackgroundColor:[UIColor clearColor]];
                    if (!self.boolEmailing) {
                        textView.backgroundColor = widgetBackgroundColor; //background color
                    }
                    
                    textView.autocorrectionType = UITextAutocorrectionTypeNo;	// no auto correction support
                    [textView setTextColor:[UIColor blackColor]];
                    textView.delegate = self;
                    
                    if (widgetAnnotation.widgetTextAlignment == WidgetTextAlignmentCenter) {
                        textView.textAlignment = NSTextAlignmentCenter;
                    }
                    else if (widgetAnnotation.widgetTextAlignment == WidgetTextAlignmentRight) {
                        textView.textAlignment = NSTextAlignmentRight;
                    }
                    else {
                        textView.textAlignment = NSTextAlignmentLeft;
                    }
                    
                    textView.contentInset = UIEdgeInsetsMake(0,0,0,0);
                    textView.scrollEnabled = false;
                    [self addSubview:textView];
                    [_arrAllFormObjects addObject:textView];
                    if ([[self.arrSavedFormInfo objectAtIndex:i] isKindOfClass:[NSDictionary class]]) {
                        NSDictionary *dict = [self.arrSavedFormInfo objectAtIndex:i];
                        textView.text = [dict objectForKey:@"text"];
                    } else {
                        textView.text = [[[self.arrSavedFormInfo objectAtIndex:i] componentsSeparatedByString:@"!)@(#*"] objectAtIndex:0];
                    }
                    if ([textView.text isEqualToString:@""]) {
                        if(_defaultValues!=nil && _defaultValues.count > widgetAnnotation.widgetIndex &&[_defaultValues objectAtIndex:widgetAnnotation.widgetIndex]!=nil){
                            textView.text = [_defaultValues objectAtIndex:widgetAnnotation.widgetIndex];
                        }
                    }
                    if (self.disabledFields != nil && self.disabledFields.count>widgetAnnotation.widgetIndex) {
                        if ([[self.disabledFields objectAtIndex:widgetAnnotation.widgetIndex] boolValue]) {
                            textView.backgroundColor = [UIColor clearColor];
                            [textView setUserInteractionEnabled:false];
                        }
                    }
                    [self sizeToFitTextView:textView newText:textView.text];
                    textView.placeholder = widgetAnnotation.defaultText;
                    
                }
                    break;
                case WidgetTypeCheckbox://Checkbox Button
                {
                    
                    UIButton* checkBox = [UIButton buttonWithType:UIButtonTypeCustom];
                    [checkBox setAccessibilityLabel:widgetAnnotation.fieldName];
                    checkBox.frame = CGRectMake(pt1.x, pt1.y, pt2.x - pt1.x, pt2.y - pt1.y);
                    [checkBox addTarget:self action:@selector(checkboxTouched:) forControlEvents:UIControlEventTouchUpInside];
                    [[checkBox layer] setBorderWidth:2.0f];
                    [[checkBox layer] setBorderColor:[UIColor blackColor].CGColor];
                    checkBox.layer.shadowRadius = 3;
                    [checkBox.layer setCornerRadius:((checkBox.frame.size.width + checkBox.frame.size.height) / 2) / 8];
                    [checkBox setBackgroundColor:widgetBackgroundColor];
                    
                    [self addSubview:checkBox];
                    [_arrAllFormObjects addObject:checkBox];
                    if ([[self.arrSavedFormInfo objectAtIndex:i] isEqualToString:@"Selected"]) {
                        [self checkboxTouched:checkBox];
                    }
                }
                    break;
                case WidgetTypeLinkBtn://Checkbox Button
                {
                    UIButton* linkBtn = [UIButton buttonWithType:UIButtonTypeCustom];
                    [linkBtn setAccessibilityLabel:widgetAnnotation.fieldName];
                    linkBtn.frame = CGRectMake(pt1.x, pt1.y, pt2.x - pt1.x, pt2.y - pt1.y);
                    [linkBtn addTarget:self action:@selector(linkBtnPressed:) forControlEvents:UIControlEventTouchUpInside];
                    linkBtn.tag = i;
                    [self addSubview:linkBtn];
                    [_arrAllFormObjects addObject:linkBtn];
                    
                    isForm = FALSE;
                }
                    break;
                case WidgetTypeRadioBtn://Radio Button
                {
                    if ([self.radioButtonParents indexOfObject:widgetAnnotation.radioButtonParentName]==NSNotFound) {
                        [self.radioButtonParents addObject:widgetAnnotation.radioButtonParentName];
                    }
                    
                    int parentIndex = (int)[self.radioButtonParents indexOfObject:widgetAnnotation.radioButtonParentName];
                    
                    UIButton* radioButton = [UIButton buttonWithType:UIButtonTypeCustom];
                    [radioButton setAccessibilityLabel:widgetAnnotation.fieldName];
                    radioButton.frame = CGRectMake(pt1.x, pt1.y, pt2.x - pt1.x, pt2.y - pt1.y);
                    [radioButton addTarget:self action:@selector(radioButtonTouched:) forControlEvents:UIControlEventTouchUpInside];
                    [[radioButton layer] setBorderWidth:2.0f];
                    [[radioButton layer] setBorderColor:[UIColor blackColor].CGColor];
                    radioButton.layer.shadowRadius = 3;
                    [radioButton.layer setCornerRadius:((radioButton.frame.size.width + radioButton.frame.size.height) / 2) / 8];
                    [radioButton setBackgroundColor:widgetBackgroundColor];
                    radioButton.tag = parentIndex;
                    
                    [self addSubview:radioButton];
                    [_arrAllFormObjects addObject:radioButton];
                    [_radioButtons addObject:radioButton];
                    
                    if ([[self.arrSavedFormInfo objectAtIndex:i] isEqualToString:@"Selected"]) {
                        [self radioButtonTouched:radioButton];
                    }else if(widgetAnnotation.checkedByDefault){
                        BOOL alreadySelectedAnotherOption = false;
                        for(UIView * view in _arrAllFormObjects){
                            if (view.tag == radioButton.tag) {
                                UIButton * otherBtn = (UIButton*)view;
                                if(otherBtn.selected){
                                    alreadySelectedAnotherOption = true;
                                    break;
                                }
                            }
                        }
                        if(!alreadySelectedAnotherOption){
                            [self selectRadioButton:radioButton];
                        }
                    }
                }
                    break;
                case WidgetTypeSignature://Signiture
                {
                    SignatureButton* signatureBox = [SignatureButton buttonWithType:UIButtonTypeCustom];
                    [signatureBox setAccessibilityLabel:widgetAnnotation.fieldName];
                    signatureBox.frame = CGRectMake(pt1.x, pt1.y, pt2.x - pt1.x, pt2.y - pt1.y);
                    signatureBox.tag = i;
                    //signatureBox.stringInfo = [NSString stringWithFormat:@""];
                    //signatureBox.backgroundColor = [UIColor redColor];
                    [signatureBox addTarget:self action:@selector(signatureBoxTouched:) forControlEvents:UIControlEventTouchUpInside];
                    [self addSubview:signatureBox];
                    [_arrAllFormObjects addObject:signatureBox];
                    
                    if (!self.boolEmailing) {
                        signatureBox.backgroundColor = widgetBackgroundColor; //background color
                    }
                    
                    NSString* tempFilesPath = [IMArcMediaViewer getTemporaryStorageLocationWithPDFFileName:self.stringFileName];
                    NSString* savedFilesPath = [IMArcMediaViewer getSavedFormDataLocationWithPDFFileName:self.stringFileName savedFormName:self.stringFormName];
                    
                    NSString *fullPath = [NSString stringWithFormat:@"%@/page%li/signature%li.png",tempFilesPath, (long)self.tag, (long)signatureBox.tag];
                    
                    BOOL fileExists = [[NSFileManager defaultManager] fileExistsAtPath:fullPath];
                    if (!fileExists) {
                        fullPath = [NSString stringWithFormat:@"%@/page%li/signature%li.png",savedFilesPath, (long)self.tag, (long)signatureBox.tag];
                        fileExists = [[NSFileManager defaultManager] fileExistsAtPath:fullPath];
                        if(fileExists){
                            NSString *tempPath = [NSString stringWithFormat:@"%@/page%li/signature%li.png",tempFilesPath, (long)self.tag, (long)signatureBox.tag];
                            NSError * error= [[NSError alloc] init];
                            [[NSFileManager defaultManager] createDirectoryAtPath:[tempPath stringByDeletingLastPathComponent] withIntermediateDirectories:YES attributes:nil error:&error];
                            [[NSFileManager defaultManager] copyItemAtPath:fullPath toPath:tempPath error:&error];
                        }
                    }
                    if (fileExists){
                        [signatureBox setImage:[UIImage imageWithContentsOfFile:fullPath] forState:UIControlStateNormal];
                        //NSLog(@"YES: %@",fullPath);
                    }else{
                        //NSLog(@"NO: %@",fullPath);
                    }
                }
                    break;
                    
                default:
                    break;
            }
        }
    }
    
    for (MFTextItem* item in [self.searchDocument resultsForPage:self.pageNumber]) {
        
        CGRect rect = CGPathGetBoundingBox(item.highlightPath);
        CGPoint pt1 = [self convertPDFPointToViewPoint: rect.origin];
        CGPoint pt2 = CGPointMake(
                                  rect.origin.x + rect.size.width,
                                  rect.origin.y + rect.size.height);
        pt2 = [self convertPDFPointToViewPoint: pt2];
        
        UIView *highlightView = [[UIView alloc] initWithFrame:CGRectMake(pt1.x, pageRenderRect.size.height- pt1.y-(pt2.y - pt1.y), pt2.x - pt1.x, pt2.y - pt1.y)];
        [highlightView setBackgroundColor:(item == self.searchDocument.currentItem)?[UIColor redColor]:[UIColor yellowColor]];
        [highlightView setAlpha:0.3];
        [self addSubview:highlightView];
        
        [UIView animateWithDuration:0.4 delay:0.5 options:UIViewAnimationOptionCurveEaseInOut animations:^{
            [highlightView setBackgroundColor:[UIColor yellowColor]];
        } completion:^(BOOL finished) {
            
        }];
    }
    
    if (isForm) {
        [self.delegate documentIsForm];
    }
}

- (void)drawFormObjectsOnPDF{
    for (UIView * subView in _arrAllFormObjects) {
        if ([subView isKindOfClass:[UITextField class]]) {
            UITextField * l = (UITextField*)subView;
            NSMutableParagraphStyle *paragraphStyle = [[NSParagraphStyle defaultParagraphStyle] mutableCopy];
            paragraphStyle.lineBreakMode = NSLineBreakByTruncatingTail;
            paragraphStyle.alignment = l.textAlignment;
            NSString *s = l.text;
            [s drawInRect:l.frame
           withAttributes:@{
                            NSFontAttributeName : l.font,
                            NSForegroundColorAttributeName : l.textColor,
                            NSParagraphStyleAttributeName: paragraphStyle
                            }];
        }else if([subView isKindOfClass:[UITextView class]]){
            UITextView * l = (UITextView*)subView;
            NSMutableParagraphStyle *paragraphStyle = [[NSParagraphStyle defaultParagraphStyle] mutableCopy];
            paragraphStyle.lineBreakMode = NSLineBreakByTruncatingTail;
            paragraphStyle.alignment = l.textAlignment;
            NSString *s = l.text;
            [s drawInRect:l.frame
           withAttributes:@{
                            NSFontAttributeName : l.font,
                            NSForegroundColorAttributeName : l.textColor,
                            NSParagraphStyleAttributeName: paragraphStyle
                            }];
            
        }else if([subView isKindOfClass:[SignatureButton class]]){
            SignatureButton * b = (SignatureButton*)subView;
            [[b imageForState:UIControlStateNormal] drawInRect:b.frame];
        }else if([subView isKindOfClass:[UIButton class]]){
            UIButton * b = (UIButton*)subView;
            if(b.selected){
                [[self fillerImageFor:b.frame].image drawInRect:b.frame];
            }
        }else if([subView isKindOfClass:[UIImageView class]]){
            UIImageView* i = (UIImageView*)subView;
            [i.image drawInRect:i.frame];
        }
    }
}

#pragma mark - Button methods

- (void)linkBtnPressed:(UIButton *)btn
{
    PDFWidgetAnnotation *link = [pageWidgets objectAtIndex:btn.tag];
    NSObject *linkTarget = link.linkTarget;
    if (linkTarget != nil) 
    {
        if ([linkTarget isKindOfClass:[NSNumber class]]) {
            NSNumber *targetPageNumber = (NSNumber *)linkTarget;
            //CGPDFPageRef targetPage = CGPDFDocumentGetPage(document, [targetPageNumber intValue]);
            [self.delegate jumpToPage:[targetPageNumber intValue]];
        } else {
            if ([linkTarget isKindOfClass: [NSString class]]) {
                NSString *linkUri = (NSString *)linkTarget;
                NSURL *url = [NSURL URLWithString: linkUri];
                [[UIApplication sharedApplication] openURL: url];
            }
        }
        
    }
}

- (UIImageView *)fillerImageFor:(CGRect)frame
{
    UIImageView *line = [[UIImageView alloc] initWithFrame:frame];
    
    UIGraphicsBeginImageContext(frame.size);
    CGContextRef context = UIGraphicsGetCurrentContext();
    [line.image drawInRect:line.bounds];
    
    CGRect reducedBoxFrame = CGRectMake((frame.size.width * .3) / 2, (frame.size.height * .3) / 2, (frame.size.width * .7), (frame.size.height * .7));
    
    CGContextSetFillColorWithColor(context, [[UIColor blackColor] CGColor]);
    CGContextFillRect(context, reducedBoxFrame);
    
    line.image = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    
    return line;
}

- (void)checkboxTouched:(UIButton *)btn
{
    btn.selected = !btn.selected;
    
    if (btn.selected) {
        [btn addSubview:[self fillerImageFor:btn.bounds]];
    } else {
        for (UIView* subview in btn.subviews) {
            [subview removeFromSuperview];
        }
    }
    
    [self saveFormData];
    [self.delegate keepTitleAlpha];
}

- (void)selectRadioButton:(UIButton *)btn
{
    for (UIView * view in _radioButtons) {
        if (view.tag == btn.tag) {
            UIButton *otherBtn = (UIButton *)view;
            otherBtn.selected = NO;
            
            for (UIView* subview in otherBtn.subviews) {
                [subview removeFromSuperview];
            }
        }
    }
    btn.selected = YES;
    
    [btn addSubview:[self fillerImageFor:btn.bounds]];
    [self.delegate keepTitleAlpha];

}

- (void)radioButtonTouched:(UIButton *)btn
{
    [self selectRadioButton:btn];
    
    [self saveFormData];
    [self.delegate keepTitleAlpha];
}

#pragma mark - File handling

- (void)makeFolder:(NSString *)folderName
{
    NSError* error;
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsDirectory = [paths objectAtIndex:0];
    NSString *dataPath = [documentsDirectory stringByAppendingPathComponent:folderName];
    
    if (![[NSFileManager defaultManager] fileExistsAtPath:dataPath])
        [[NSFileManager defaultManager] createDirectoryAtPath:dataPath withIntermediateDirectories:NO attributes:nil error:&error]; //Create folder
}

- (void)makeFolderFromFullPath:(NSString *)folderPath
{
    NSError* error;
    
    if (![[NSFileManager defaultManager] fileExistsAtPath:folderPath])
        [[NSFileManager defaultManager] createDirectoryAtPath:folderPath withIntermediateDirectories:YES attributes:nil error:&error]; //Create folder
}

#pragma mark - Signature Methods

- (void)signatureBoxTouched:(UIButton*)btn
{
    if (self.buttonExitSignature) {
        [self.buttonExitSignature removeFromSuperview];
    }
    
    for (UIView * siblingview in self.subviews) {
        if([siblingview isFirstResponder]){
            [siblingview resignFirstResponder];
        }
    }
    
    CGFloat viewWidth = self.viewParent.frame.size.width;
    
    signatureBtn = btn;
    CGFloat signatureWidth = (viewWidth * .9f);
    [self.delegate isEditingSignature];
    int height = (signatureWidth/btn.frame.size.width)*btn.frame.size.height;
    if (height > self.viewParent.frame.size.height){
        height = self.viewParent.frame.size.height *0.6f;
        signatureWidth = height * btn.frame.size.width/btn.frame.size.height;
    }

    signature = [[CULSignatureView alloc] initWithFrame:CGRectMake(0, 0, signatureWidth, height)];
    signature.center = CGPointMake(self.viewParent.center.x, self.viewParent.center.y);
    if (signature.center.x > self.viewParent.frame.size.width) {
        signature.center = CGPointMake((int)self.viewParent.center.x % (int)self.viewParent.frame.size.width, signature.center.y);
    }
    CGRect sigFrame = signature.frame;
    if (self.styleSignature==nil) {
        signature.frame = btn.frame;
        
        [UIView beginAnimations:nil context:nil];
        signature.frame = sigFrame;
        [UIView commitAnimations];
    }else{
        signature.frame = sigFrame;
    }
    
    self.buttonExitSignature = [UIButton buttonWithType:UIButtonTypeCustom];
    self.buttonExitSignature.frame = CGRectMake(0,0, self.viewParent.frame.size.width,  self.viewParent.frame.size.height);
    [self.buttonExitSignature setBackgroundColor:[UIColor colorWithWhite:0.0 alpha:0.6]];
    [self.buttonExitSignature addTarget:self action:@selector(signatureDone) forControlEvents:UIControlEventTouchUpInside];
    [self.viewParent addSubview:self.buttonExitSignature];
    
    [self.buttonExitSignature addSubview:signature];
    
    UIButton* done = [UIButton buttonWithType:UIButtonTypeCustom];
    done.frame = CGRectMake(signature.frame.size.width-50, signature.frame.size.height-30, 50, 30);
    [done addTarget:self action:@selector(signatureDone) forControlEvents:UIControlEventTouchUpInside];
    [done setImage:[UIImage imageNamed:@"done_BTN.png"] forState:UIControlStateNormal];
    [signature addSubview:done];
    
    UIButton* clearBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    clearBtn.frame = CGRectMake(signature.frame.size.width-50, 0, 50, 30);
    [clearBtn addTarget:self action:@selector(signatureClear) forControlEvents:UIControlEventTouchUpInside];
    [clearBtn setImage:[UIImage imageNamed:@"clear_BTN.png"] forState:UIControlStateNormal];
    [signature addSubview:clearBtn];
    
    [self.delegate keepTitleAlpha];
    
    if (self.styleSignature!=nil) {
        self.styleSignature(self.buttonExitSignature,signature,done,clearBtn);
    }
}

- (void)signatureDone
{
    [signatureBtn setImage:[signature imageFromSignature] forState:UIControlStateNormal];
    //signatureBtn.stringInfo = [NSString stringWithFormat:@"%i, %i, %i, %i", (int)signatureBtn.frame.origin.x, (int)signature.frame.origin.y, self.tag, signatureBtn.tag];
    [signature removeFromSuperview];
    signature = nil;
    
    [self.buttonExitSignature removeFromSuperview];
    [self.delegate doneEditingSignature];
    
    [self.delegate keepTitleAlpha];
    NSString* tempFilesPath = [IMArcMediaViewer getTemporaryStorageLocationWithPDFFileName:self.stringFileName];
    NSString* pagePath = [tempFilesPath stringByAppendingFormat:@"/page%li",(long)self.tag];
    [self makeFolderFromFullPath:pagePath];
    
    NSData* imageData = UIImagePNGRepresentation(signatureBtn.currentImage);
    
    NSFileManager *fileManager = [NSFileManager defaultManager];
    NSString *fullPath = [NSString stringWithFormat:@"%@/signature%li.png",pagePath, (long)signatureBtn.tag];
    [fileManager createFileAtPath:fullPath contents:imageData attributes:nil];
    [self saveFormData];
    //[self saveTextfields];
}

- (void)signatureClear
{
    [signature clear];
}

- (void)loadPageWidgets:(CGPDFPageRef)page withIndexOffset:(int)indexOffset {
    CGPDFDictionaryRef pageDictionary = CGPDFPageGetDictionary(page);
    CGPDFArrayRef annotsArray = NULL;
    
    int linkOffset = 0;
    
    // PDF widgets are widget annotations stored in the page's Annots array.
    CGPDFDictionaryGetArray(pageDictionary, "Annots", &annotsArray);
    if (annotsArray != NULL) {
        size_t annotsCount = CGPDFArrayGetCount(annotsArray);
        
        for (int j = 0; j < annotsCount; j++) {
            CGPDFDictionaryRef annotationDictionary = NULL;            
            if (CGPDFArrayGetDictionary(annotsArray, j, &annotationDictionary)) {
                const char *annotationType;
                CGPDFDictionaryGetName(annotationDictionary, "Subtype", &annotationType);
                
                if (strcmp(annotationType, "Widget") == 0) {
                    PDFWidgetAnnotation *widgetAnnotation = [[PDFWidgetAnnotation alloc] initWithPDFDictionary:annotationDictionary PDFPageRef:pdfPage pageNum:(int)self.tag];
                    widgetAnnotation.widgetIndex = (indexOffset+ linkOffset + (int)pageWidgets.count);
                    [pageWidgets addObject: widgetAnnotation];
                }else if (strcmp(annotationType, "Link") == 0) {
                    linkOffset ++;
                }
            }
        }
    }
}

#pragma mark - Text shrinking methods

- (BOOL)sizeToFitTextView:(UITextView *)textView newText:(NSString *)newText
{
    CGSize boundingRectSize =CGSizeMake(textView.frame.size.width - (textView.textContainerInset.top + textView.textContainerInset.bottom), MAXFLOAT);
    CGFloat fontSize = ((textView.frame.size.height - (textView.textContainerInset.top + textView.textContainerInset.bottom)) * 0.9) / 2; // Make sure at least two lines fit without the text having to shrink
    [textView setFont:[UIFont systemFontOfSize:fontSize * self.scale]];
    CGSize size = [textView.text boundingRectWithSize:boundingRectSize
                                              options:NSStringDrawingUsesLineFragmentOrigin|NSStringDrawingUsesFontLeading
                                           attributes:@{NSFontAttributeName:[textView font]}
                                              context:nil].size;
    CGFloat constrainedHeight = textView.frame.size.height - (textView.textContainerInset.top + textView.textContainerInset.bottom);
    
    if (size.height > constrainedHeight) {
        CGFloat lowPoint = 0.1;
        CGFloat hiPoint = fontSize;
        while(hiPoint-lowPoint>0.1){
            CGFloat midPoint = lowPoint + (hiPoint-lowPoint)/2;
            fontSize = midPoint;
            size = [newText boundingRectWithSize:boundingRectSize
                                                     options:NSStringDrawingUsesLineFragmentOrigin|NSStringDrawingUsesFontLeading
                                                  attributes:@{NSFontAttributeName:[UIFont systemFontOfSize:fontSize]}
                                                     context:nil].size;
            if(size.height > constrainedHeight && fontSize > 0.1){
                hiPoint = midPoint;
            }else{
                lowPoint = midPoint;
            }
        }
        fontSize = lowPoint;
        [textView setFont:[UIFont systemFontOfSize:fontSize]];
    }
    
    return true;
}

- (BOOL)sizeToFitTextField:(UITextField *)textField newText:(NSString *)newText
{
    [textField setFont:[UIFont systemFontOfSize:(textField.frame.size.height * 0.8) * self.scale]];
    CGSize constraint = textField.frame.size;
    CGSize size = [newText sizeWithAttributes:@{NSFontAttributeName:[textField font]}];
    CGFloat fontSize = textField.font.pointSize;
    
    if(size.width > constraint.width || size.height > constraint.height){
        CGFloat lowPoint = 0.01;
        CGFloat hiPoint = fontSize;
        while(hiPoint-lowPoint>0.1){
            CGFloat midPoint = lowPoint + (hiPoint-lowPoint)/2;
            fontSize = midPoint;
            size = [newText sizeWithAttributes:@{NSFontAttributeName: [UIFont systemFontOfSize:fontSize]}];
            if(size.width > constraint.width || size.height > constraint.height){
                hiPoint = midPoint;
            }else{
                lowPoint = midPoint;
            }
        }
        fontSize = lowPoint;
    }
    [textField setFont:[UIFont systemFontOfSize:fontSize]];
    
    return YES;
}

#pragma mark - UITextView delegate methods

- (BOOL)textView:(UITextView *)textView shouldChangeTextInRange:(NSRange)range replacementText:(NSString *)text
{
    [self sizeToFitTextView:textView newText:[textView.text stringByReplacingCharactersInRange:range withString:text]];
    [self saveFormData];
    
    return YES;
}

- (BOOL)textViewShouldBeginEditing:(UITextView *)textView
{
    [self.delegate keepTitleAlpha];
    [textView setInputAccessoryView:self.textInputAccessoryView];
    int index = (int)[_arrAllFormObjects indexOfObject:textView];
    //Make sure index is not out of bounds
    if (index < 0) {
        [self.textInputPrevBtn setEnabled:NO];
    } else if (index >= [_arrAllFormObjects count] - 1) {
        [self.textInputNextBtn setEnabled:NO];
    } else {
        [self.textInputPrevBtn setEnabled:YES];
        [self.textInputNextBtn setEnabled:YES];
    }

    return TRUE;
}

- (void)textViewDidEndEditing:(UITextView *)textView
{
    [self saveFormData];
}

#pragma mark - UITextfield delegate methods

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
    NSString *whatWasThereBefore = textField.text;
    NSString *newText = [textField.text stringByReplacingCharactersInRange:range withString:string];
    if (![self sizeToFitTextField:textField newText:newText]) {
        textField.text = whatWasThereBefore;
        return NO;
    }
    [self saveFormData];
    
    return YES;
}

- (void)textFieldDidBeginEditing:(UITextField *)textField
{
    [self.delegate keepTitleAlpha];
    [textField setInputAccessoryView:self.textInputAccessoryView];
    int index = (int)[_arrAllFormObjects indexOfObject:textField];
    //Make sure index is not out of bounds
    if (index < 0) {
        [self.textInputPrevBtn setEnabled:NO];
        [textField setReturnKeyType:UIReturnKeyNext];
    } else if (index >= [_arrAllFormObjects count] - 1) {
        [self.textInputNextBtn setEnabled:NO];
        [textField setReturnKeyType:UIReturnKeyDone];
    } else {
        [self.textInputPrevBtn setEnabled:YES];
        [self.textInputNextBtn setEnabled:YES];
        [textField setReturnKeyType:UIReturnKeyNext];
    }
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    
    int index = (int)[_arrAllFormObjects indexOfObject:textField];
    //Make sure index is not out of bounds
    if(index >= [_arrAllFormObjects count] - 1){
        [textField resignFirstResponder];
    } else {
        [self gotoNextTextfield];
    }
	return YES;
}

- (void)textFieldDidEndEditing:(UITextField *)textField
{
    [self saveFormData];
    [textField resignFirstResponder];
}

-(void)createInputAccessoryView {
    
    self.textInputAccessoryView = [[UIToolbar alloc] initWithFrame:CGRectMake(0.0, 0.0, self.viewParent.frame.size.width, 44.0)];
    
    //Previous Button
    UIButton *prevCustomBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    UIImage *prevArrowActive = [UIImage imageNamed:@"back_arrow_active.png"];
    UIImage *prevArrow = [UIImage imageNamed:@"back_arrow.png"];
    [prevCustomBtn setFrame: CGRectMake(0.0, 0.0, prevArrowActive.size.width + 16, prevArrowActive.size.height)];
    [prevCustomBtn setImage:prevArrowActive forState:UIControlStateNormal];
    [prevCustomBtn setImage:prevArrow forState:UIControlStateDisabled];
    [prevCustomBtn addTarget: self action: @selector(gotoPrevTextfield) forControlEvents: UIControlEventTouchUpInside];
    self.textInputPrevBtn = [[UIBarButtonItem alloc] initWithCustomView:prevCustomBtn];
    
    // Next Button
    UIButton *nextCustomBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    UIImage *nextArrowActive = [UIImage imageNamed:@"forward_arrow_active.png"];
    UIImage *nextArrow = [UIImage imageNamed:@"forward_arrow.png"];
    [nextCustomBtn setFrame: CGRectMake(0.0, 0.0, nextArrowActive.size.width + 16, nextArrowActive.size.height)];
    [nextCustomBtn setImage:nextArrowActive forState:UIControlStateNormal];
    [nextCustomBtn setImage:nextArrow forState:UIControlStateDisabled];
    [nextCustomBtn addTarget:self action:@selector(gotoNextTextfield) forControlEvents:UIControlEventTouchUpInside];
    self.textInputNextBtn = [[UIBarButtonItem alloc] initWithCustomView:nextCustomBtn];
    
    //Done Button
    UIBarButtonItem *doneBtn = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemDone target:self action:@selector(doneEditingTextInputs)];
    
    // Add buttons to toolbar and set the inputAccessoryView
    NSMutableArray *toolBarItems = [[NSMutableArray alloc] init];
    UIBarButtonItem *fixedItem = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFixedSpace target:nil action:nil];
    fixedItem.width = 10.0f;
    UIBarButtonItem *flexibleItem = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:nil action:nil];
    
    [toolBarItems addObject:fixedItem];
    [toolBarItems addObject:self.textInputPrevBtn];
    [toolBarItems addObject:fixedItem];
    [toolBarItems addObject:self.textInputNextBtn];
    [toolBarItems addObject:flexibleItem];
    [toolBarItems addObject:doneBtn];
    [toolBarItems addObject:fixedItem];
    
    [self.textInputAccessoryView setItems:toolBarItems];
    
}

-(void)gotoPrevTextfield{
    int index = (int)[_arrAllFormObjects indexOfObject:[self findFirstResponderWithView:self.viewParent]];
    index--;
    //Make sure index is not out of bounds
    if (index < 0) {
        [self.textInputPrevBtn setEnabled:NO];
        return;
    }
    else{
        while (!([[_arrAllFormObjects objectAtIndex:index] isKindOfClass:[UITextField class]] || [[_arrAllFormObjects objectAtIndex:index] isKindOfClass:[UITextView class]])) {
            //Make sure index is not out of bounds if it is disable the button and return
            index--;
            if (index < 0) {
                [self.textInputPrevBtn setEnabled:NO];
                return;
            }
        }
        [[_arrAllFormObjects objectAtIndex:index] becomeFirstResponder];
    }
   
}

-(void)gotoNextTextfield{
    int index = (int)[_arrAllFormObjects indexOfObject:[self findFirstResponderWithView:self.viewParent]];
    index++;
    //Make sure index is not out of bounds
    if (index >= [_arrAllFormObjects count] - 1) {
        [self.textInputNextBtn setEnabled:NO];
        return;
    }
    else{
        while (!([[_arrAllFormObjects objectAtIndex:index] isKindOfClass:[UITextField class]] || [[_arrAllFormObjects objectAtIndex:index] isKindOfClass:[UITextView class]])) {
            //Make sure index is not out of bounds if it is disable the button and return
                index++;
                if (index >= [_arrAllFormObjects count] - 1) {
                    [self.textInputNextBtn setEnabled:NO];
                    return;
                }
        }
        [[_arrAllFormObjects objectAtIndex:index] becomeFirstResponder];
    }
    
}

-(void)doneEditingTextInputs{
    [[self findFirstResponderWithView:self.viewParent] resignFirstResponder];
}

-(UIView* )findFirstResponderWithView:(UIView*)v{
    if ([v isFirstResponder]) {
        return v;
    }
    for (UIView* subview in v.subviews) {
        UIView * fr = [self findFirstResponderWithView:subview];
        if(fr!=nil){
            return fr;
        }
    }
    return nil;
}

#pragma mark - PDF creation

-(void)addOverlayToPDFPage:(CGPDFPageRef)page{
    
}

- (void)createPDFFile:(CGPDFPageRef)pageRef
{
    CGContextRef pdfContext;
    
    CFStringRef path;
    
    CFURLRef url;
    
    CFMutableDictionaryRef myDictionary = NULL;
    
    

	NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES); 
	NSString *documentsDirectory = [paths objectAtIndex:0]; // Get documents folder
	const char *filepath = [[documentsDirectory stringByAppendingString:@"/fileTest.pdf"] UTF8String];
    
    path = CFStringCreateWithCString (NULL, filepath, // 2
                                      
                                      kCFStringEncodingUTF8);
    
    url = CFURLCreateWithFileSystemPath (NULL, path, // 3
                                         
                                         kCFURLPOSIXPathStyle, 0);
    
    CFRelease (path);
    
    myDictionary = CFDictionaryCreateMutable(NULL, 0,
                                             
                                             &kCFTypeDictionaryKeyCallBacks,
                                             
                                             &kCFTypeDictionaryValueCallBacks); 
    
    CFDictionarySetValue(myDictionary, kCGPDFContextTitle, CFSTR("My PDF File"));
    
    CFDictionarySetValue(myDictionary, kCGPDFContextCreator, CFSTR("My Name"));
    
    CGRect pageRect = self.bounds;
    pdfContext = CGPDFContextCreateWithURL (url, &pageRect, myDictionary); // 5
    
    CFRelease(myDictionary);
    
    CFRelease(url);
    
    CGRect mediaBox;
    mediaBox = CGRectMake (0, 0, pageRect.size.width, 100);
    CFStringRef myKeys[1];
    
    CFTypeRef myValues[1];
    
    myKeys[0] = CFSTR("MediaBox");
    
    myValues[0] = (CFTypeRef) CFDataCreate(NULL,(const UInt8 *)&mediaBox, sizeof (CGRect));
    
    CFDictionaryRef pageDictionary = CFDictionaryCreate(NULL, (const void **) myKeys,
                                                        (const void **) myValues, 1,
                                                        &kCFTypeDictionaryKeyCallBacks,
                                                        & kCFTypeDictionaryValueCallBacks);
    CFMutableDictionaryRef pageDictionaryMutable = CFDictionaryCreateMutableCopy(NULL, 0, pageDictionary);
    //CFDictionarySetValue(pageDictionaryMutable,CFSTR("Annots"), (CFMutableDictionaryRef)[self.pageDictionary objectForKey:@"Annots"]);
    
    CFMutableDictionaryRef  metaDataDictionary = CFDictionaryCreateMutable(NULL, 0, &kCFTypeDictionaryKeyCallBacks,&kCFTypeDictionaryValueCallBacks); 
    
    CFDictionarySetValue(metaDataDictionary, CFSTR("DA"), CFSTR("/Helvetica 10 Tf 0 g"));
    CFDictionarySetValue(metaDataDictionary, CFSTR("F"), CFSTR("4"));
    CFDictionarySetValue(metaDataDictionary, CFSTR("FT"), CFSTR("Tx"));
    CFDictionarySetValue(metaDataDictionary, CFSTR("Subtype"), CFSTR("Widget"));
    CFDictionarySetValue(metaDataDictionary, CFSTR("T"), CFSTR("undefined"));
    CFDictionarySetValue(metaDataDictionary, CFSTR("Type"), CFSTR("Annot"));
    CFDictionarySetValue(metaDataDictionary, CFSTR("V"), CFSTR(""));
    CFMutableArrayRef array = CFArrayCreateMutable(kCFAllocatorDefault,0, &kCFTypeArrayCallBacks);
    CFArrayInsertValueAtIndex(array, 0, metaDataDictionary);
    CFDictionarySetValue(pageDictionaryMutable, CFSTR("Annots"), array);
    
    
    CGPDFContextBeginPage (pdfContext, pageDictionaryMutable);
    
    
    
    CGContextSetTextDrawingMode (pdfContext, kCGTextFill);
    CGContextSetRGBFillColor (pdfContext, 0, 0, 0, 1);
    [@"Some text" drawAtPoint:CGPointMake(260, 390) withAttributes:@{NSFontAttributeName:[UIFont fontWithName:@"Helvetica" size:16]}];
    
    CGPDFContextEndPage (pdfContext);
    
    CGContextRelease (pdfContext);
    
    CFRelease(pageDictionaryMutable);
    CFRelease(pageDictionary);
}

void copyDictionaryValues (const char *key, CGPDFObjectRef object, void *info) {
    if (strcmp(key, "P") == 0 || strcmp(key, "Parent") == 0 ) 
        return;
    
    NSMutableDictionary* currentDict = [selfClass pageDictionary];
    //NSLog(@"key: %s", key);
    CGPDFObjectType type = CGPDFObjectGetType(object);
    switch (type) {
        case kCGPDFObjectTypeString: {
            CGPDFStringRef objectString;
            if (CGPDFObjectGetValue(object, kCGPDFObjectTypeString, &objectString)) {
                NSString *tempStr = (NSString *)CFBridgingRelease(CGPDFStringCopyTextString(objectString));
                [currentDict setObject:tempStr
                                forKey:[NSString stringWithCString:key encoding:NSUTF8StringEncoding]];
                //NSLog(@"set string value");
            }
        }
            break;
        case kCGPDFObjectTypeInteger: {
            CGPDFInteger objectInteger;
            if (CGPDFObjectGetValue(object, kCGPDFObjectTypeInteger, &objectInteger)) {
                [currentDict setObject:[NSNumber numberWithInteger:objectInteger]
                                forKey:[NSString stringWithCString:key encoding:NSUTF8StringEncoding]];
                //NSLog(@"set int value");
            }
        }
            break;
        case kCGPDFObjectTypeBoolean: {
            CGPDFBoolean objectBool;
            if (CGPDFObjectGetValue(object, kCGPDFObjectTypeBoolean, &objectBool)) {
                [currentDict setObject:[NSNumber numberWithBool:objectBool]
                                forKey:[NSString stringWithCString:key encoding:NSUTF8StringEncoding]];
                //NSLog(@"set boolean value");
            }
        }
            break;
        case kCGPDFObjectTypeName:
        {
            const char * name;
            if( CGPDFObjectGetValue( object, kCGPDFObjectTypeName, &name ) )
                
                [currentDict setObject:[NSString stringWithCString:name encoding:NSUTF8StringEncoding]
                                forKey:[NSString stringWithCString:key encoding:NSUTF8StringEncoding]];
            
            //NSLog(@"set name value");
        }
            break;
        case kCGPDFObjectTypeArray : {
            CGPDFArrayRef objectArray;
            if (CGPDFObjectGetValue(object, kCGPDFObjectTypeArray, &objectArray)) {
                NSArray *tempArr = [selfClass copyPDFArray:objectArray];
                [currentDict setObject:tempArr
                                forKey:[NSString stringWithCString:key encoding:NSUTF8StringEncoding]];
                //NSLog(@"set array value");
            }
        }
            break;
        case kCGPDFObjectTypeDictionary : {
            CGPDFDictionaryRef objectDictionary;
            if (CGPDFObjectGetValue(object, kCGPDFObjectTypeDictionary, &objectDictionary)) {
                
                //NSMutableDictionary* dictionary = [[NSMutableDictionary alloc] init];
                NSDictionary* dict = [selfClass copyPDFDictionary:objectDictionary];
                [currentDict setObject:dict
                                forKey:[NSString stringWithCString:key encoding:NSUTF8StringEncoding]];
                //[dict release];
                //NSMutableDictionary* temp = [selfClass tempDictionary];
                [selfClass resetDictionaryWithDictionary:currentDict];
                //NSLog(@"set Dictionary value");
            }
        }
            break;
            
        default:
            break;
    }
}

- (void)extractPDFDictionaryforPage:(CGPDFPageRef)pageRef
{
    NSLog(@"extractingPDFDictionary");
    //CGPDFDictionaryRef oldDict = CGPDFDocumentGetInfo(pdf);
    CGPDFDictionaryApplyFunction(CGPDFPageGetDictionary(pageRef), copyDictionaryValues, NULL);
}

- (NSArray *)copyPDFArray:(CGPDFArrayRef)arr
{
    int i = 0;
    NSMutableArray *temp = [[NSMutableArray alloc] init];
    for(i=0; i<CGPDFArrayGetCount(arr); i++){
        CGPDFObjectRef object;
        CGPDFArrayGetObject(arr, i, &object);
        CGPDFObjectType type = CGPDFObjectGetType(object);
        switch(type){
            case kCGPDFObjectTypeString: {
                CGPDFStringRef objectString;
                if (CGPDFObjectGetValue(object, kCGPDFObjectTypeString, &objectString)) {
                    NSString *tempStr = (NSString *)CFBridgingRelease(CGPDFStringCopyTextString(objectString));
                    [temp addObject:tempStr];
                }
            }
                break;
            case kCGPDFObjectTypeInteger: {
                CGPDFInteger objectInteger;
                if (CGPDFObjectGetValue(object, kCGPDFObjectTypeInteger, &objectInteger)) {
                    [temp addObject:[NSNumber numberWithInteger:objectInteger]];
                }
            }
                break;
            case kCGPDFObjectTypeBoolean: {
                CGPDFBoolean objectBool;
                if (CGPDFObjectGetValue(object, kCGPDFObjectTypeBoolean, &objectBool)) {
                    [temp addObject:[NSNumber numberWithBool:objectBool]];
                }
            }
                break;
            case kCGPDFObjectTypeArray : {
                CGPDFArrayRef objectArray;
                if (CGPDFObjectGetValue(object, kCGPDFObjectTypeArray, &objectArray)) {
                    NSArray *tempArr = [selfClass copyPDFArray:objectArray];
                    [temp addObject:tempArr];
                }
            }
                break;
            case kCGPDFObjectTypeDictionary : {
                CGPDFDictionaryRef objectDictionary;
                if (CGPDFObjectGetValue(object, kCGPDFObjectTypeDictionary, &objectDictionary)) {
                    
                    NSMutableDictionary* currentDict = [selfClass pageDictionary];
                    //NSMutableDictionary* dictionary = [[NSMutableDictionary alloc] init];
                     NSDictionary* dict = [selfClass copyPDFDictionary:objectDictionary];
                    [temp addObject:dict];
                    //NSMutableDictionary* temp = [selfClass pageDictionary];
                    [self resetDictionaryWithDictionary:currentDict];
                    //NSLog(@"set Dictionary value");
                }
            }
                break;
            default:
                break;
        }
    }
    
    return temp;
}

- (NSDictionary *)copyPDFDictionary:(CGPDFDictionaryRef)dictRef
{
    self.pageDictionary = [[NSMutableDictionary alloc] init];
    CGPDFDictionaryApplyFunction(dictRef, copyDictionaryValues, NULL);
    
    return self.pageDictionary;
}

- (void)resetDictionaryWithDictionary:(NSMutableDictionary *)oldDictionary
{
    self.pageDictionary = oldDictionary;
}

#pragma mark - Class method

+ (int)widgetCountForPage:(CGPDFPageRef) page
{
    CGPDFDictionaryRef pageDictionary = CGPDFPageGetDictionary(page);
    CGPDFArrayRef annotsArray = NULL;
    
    // PDF widgets are widget annotations stored in the page's Annots array.
    CGPDFDictionaryGetArray(pageDictionary, "Annots", &annotsArray);
    int count = 0;
    if (annotsArray != NULL) {
        size_t annotsCount = CGPDFArrayGetCount(annotsArray);
        
        for (int j = 0; j < annotsCount; j++) {
            CGPDFDictionaryRef annotationDictionary = NULL;
            if (CGPDFArrayGetDictionary(annotsArray, j, &annotationDictionary)) {
                const char *annotationType;
                CGPDFDictionaryGetName(annotationDictionary, "Subtype", &annotationType);
                
                if (strcmp(annotationType, "Widget") == 0) {
                    count ++;
                }else if (strcmp(annotationType, "Link") == 0) {
                    count ++;
                }
            }
        }
    }
    return count;
}

@end
