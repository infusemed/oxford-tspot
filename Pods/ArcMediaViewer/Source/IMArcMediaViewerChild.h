//
//  IMArcMediaViewerChild.h
//  ArcMediaViewer
//
//  Created by Infuse Medical on 2/14/14.
//  Copyright (c) 2014 Infuse Medical. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "IMArcMediaHeaderView.h"
#import "PDFUploadInfo.h"


@protocol IMArcMediaViewerChildDelegate <NSObject>
-(void)closeMediaViewer;
-(void) documentIsForm;
-(void) emailPDFForm;
@end

typedef void(^SetPDFUploadInfoBlock)(NSArray* formFields, PDFUploadInfo* pdfInfo);

@interface IMArcMediaViewerChild : UIViewController<IMArcMediaHeaderViewDelegate,UIGestureRecognizerDelegate>
@property (nonatomic, strong) UIButton* buttonBack;
@property (nonatomic, assign) id<IMArcMediaViewerChildDelegate>delegate;
@property (nonatomic, strong) NSURL * uploadURL;
@property (nonatomic, strong) NSString * uploadConfiguration;
@property (nonatomic, assign) BOOL requireAllFields;
/**
 * Only blocks the "Send Email" option from being added to the menu
 */
@property (nonatomic, assign) BOOL blockEmailOption;
/**
 * Blocks the menu from ever being shown for any document
 */
@property (nonatomic, assign) BOOL blockMenu;
/**
 * Only blocks the menu from being displayed on non-form documents
 */
@property (nonatomic, assign) BOOL nonFormsBlockMenu;
/**
 * Allows the 'Open In' to be added to the menu drop down.
 * 
 * Default is YES
 */
@property (nonatomic, assign) BOOL allowOpenInMenuOption;
@property (nonatomic, strong) NSString* emailToRecipients;
@property (nonatomic, strong) NSString* emailSubject;
@property (nonatomic, strong) NSString* ccRecipients;
@property (nonatomic, strong) NSString* bCCRecipients;
@property (nonatomic, assign) float minZoomScale;
@property (nonatomic, strong) NSString* emailBody;
@property (nonatomic, strong) IMArcMediaHeaderView * headerView;
@property (nonatomic, copy) SetPDFUploadInfoBlock setPDFUploadInfo;
@property (nonatomic, strong) NSDictionary* defaultValues;
@property (nonatomic, strong) NSArray* disabledFields;
/**
 * UIColor used to customize elements in the MediaViewer
 */
@property (nonatomic, strong) UIColor *customColor;
@property (nonatomic, strong) NSString *documentTitle;
@property (nonatomic, strong) NSDictionary *customParameters;
@property (nonatomic) CGFloat emailScale;

-(void)tapGestureRecognized:(UITapGestureRecognizer*)tap;
-(id)initWithFrame:(CGRect)frame Delegate:(id)delegate;
-(BOOL)respondsToExtension:(NSString*)extension;
-(void)loadFileAtPath:(NSString*)filePath;
-(void)stop;
-(void)setFrame:(CGRect)frame;
-(void)hideTitleBar;
-(void)showTitleBar;

-(void)buttonBackPressed:(UIButton*)sender;
@end
