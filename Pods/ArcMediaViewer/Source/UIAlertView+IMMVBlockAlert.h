//
//  UIAlertView+IMMVBlockAlert.h
//  ARCMediaViewer
//
//  Created by Jordan Helzer on 6/3/15.
//  Copyright (c) 2015 Infuse Medical. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIAlertView (IMMVBlockAlert)

- (id)initWithTitle:(NSString *)title message:(NSString *)message dismissBlock:(void ( ^ )( NSInteger buttonIndex))block cancelButtonTitle:(NSString *)cancelButtonTitle otherButtonTitles:(NSString *)otherButtonTitles, ... ;

@end
