//
//  PDFLinkAnnotation.m
//  LinksNavigation
//
//  Created by Sorin Nistor on 6/11/11.
//  Copyright 2011 iPDFdev.com. All rights reserved.
//

#import "PDFLinkAnnotation.h"


@implementation PDFLinkAnnotation

@synthesize pdfRectangle;
@synthesize auxInfo;
@synthesize tempDictionary;

- (id)initWithPDFDictionary:(CGPDFDictionaryRef)newAnnotationDictionary {
    self = [super init];
    if (self) {
        self->annotationDictionary = newAnnotationDictionary;
        
        //selfClass = self;
        
        //** For extracting what is in the pdf **//
        //auxInfo = [[NSMutableDictionary alloc] init];
        //tempDictionary = auxInfo;
        ////CFDictionaryRef newDictionary = (CFDictionaryRef)[self auxInfo];
        //[self extractPDFDictionary];
        
        
        // Normalize and cache the annotation rect definition for faster hit testing.
        CGPDFArrayRef rectArray = NULL;
        CGPDFDictionaryGetArray(annotationDictionary, "Rect", &rectArray);
        if (rectArray != NULL) {
            CGPDFReal llx = 0;
            CGPDFArrayGetNumber(rectArray, 0, &llx);
            CGPDFReal lly = 0;
            CGPDFArrayGetNumber(rectArray, 1, &lly);
            CGPDFReal urx = 0;
            CGPDFArrayGetNumber(rectArray, 2, &urx);
            CGPDFReal ury = 0;
            CGPDFArrayGetNumber(rectArray, 3, &ury);
            
            if (llx > urx) {
                CGPDFReal temp = llx;
                llx = urx;
                urx = temp;
            }
            if (lly > ury) {
                CGPDFReal temp = lly;
                lly = ury;
                ury = temp;
            }
            
            pdfRectangle = CGRectMake(llx, lly, urx - llx, ury - lly);
        }
        
        
    }
    
    return self;
}

- (void)dealloc {
    self->annotationDictionary = NULL;
}

- (BOOL)hitTest:(CGPoint)point {
    if ((pdfRectangle.origin.x <= point.x) &&
        (pdfRectangle.origin.y <= point.y) &&
        (point.x <= pdfRectangle.origin.x + pdfRectangle.size.width) &&
        (point.y <= pdfRectangle.origin.y + pdfRectangle.size.height)) {
        return YES;
    } else {
        return NO;
    }
}

- (NSObject*)getLinkTarget:(CGPDFDocumentRef)document {
    NSObject *linkTarget = nil;
    
    // Link target can be stored either in A entry or in Dest entry in annotation dictionary.
    // Dest entry is the destination array that we're looking for. It can be a direct array definition
    // or a name. If it is a name, we need to search recursively for the corresponding destination array 
    // in document's Dests tree.
    // A entry is an action dictionary. There are many action types, we're looking for GoTo and URI actions.
    // GoTo actions are used for links within the same document. The GoTo action has a D entry which is the
    // destination array, same format like Dest entry in annotation dictionary.
    // URI actions are used for links to web resources. The URI action has a 
    // URI entry which is the destination URL.
    // If both entries are present, A entry takes precedence.
    
    CGPDFArrayRef destArray = NULL;
    CGPDFDictionaryRef actionDictionary = NULL;
    if (CGPDFDictionaryGetDictionary(annotationDictionary, "A", &actionDictionary)) {
        const char* actionType;
        if (CGPDFDictionaryGetName(actionDictionary, "S", &actionType)) {
            if (strcmp(actionType, "GoTo") == 0) {
                CGPDFDictionaryGetArray(actionDictionary, "D", &destArray);
            }
            /*
            CGPDFStringRef nameRef = NULL;
            if (!destArray) {
                CGPDFDictionaryGetString(actionDictionary, "D", &nameRef);
                const char *name = (const char *)CGPDFStringGetBytePtr(nameRef);
                NSString* nameLink = [[NSString alloc] initWithCString: name encoding: NSASCIIStringEncoding];
                NSString* hello = nameLink;
            }*/
            CGPDFStringRef destName;
            if (CGPDFDictionaryGetString(actionDictionary, "D", &destName)) {
                // Traverse the Dests tree to locate the destination array.
                CGPDFDictionaryRef catalogDictionary = CGPDFDocumentGetCatalog(document);
                CGPDFDictionaryRef namesDictionary = NULL;
                if (CGPDFDictionaryGetDictionary(catalogDictionary, "Names", &namesDictionary)) {
                    CGPDFDictionaryRef destsDictionary = NULL;
                    if (CGPDFDictionaryGetDictionary(namesDictionary, "Dests", &destsDictionary)) {
                        const char *destinationName = (const char *)CGPDFStringGetBytePtr(destName);
                        destArray = [self findDestinationByName: destinationName inDestsTree: destsDictionary];
                    }
                }
            }
            if (strcmp(actionType, "URI") == 0) {
                CGPDFStringRef uriRef = NULL;
                if (CGPDFDictionaryGetString(actionDictionary, "URI", &uriRef)) {
                    const char *uri = (const char *)CGPDFStringGetBytePtr(uriRef);
                    linkTarget = [[NSString alloc] initWithCString: uri encoding: NSASCIIStringEncoding];
                }
            }
        }
    } else {
        // Dest entry can be either a string object or an array object.
        if (!CGPDFDictionaryGetArray(annotationDictionary, "Dest", &destArray)) {
            CGPDFStringRef destName;
            if (CGPDFDictionaryGetString(annotationDictionary, "Dest", &destName)) {
                // Traverse the Dests tree to locate the destination array.
                CGPDFDictionaryRef catalogDictionary = CGPDFDocumentGetCatalog(document);
                CGPDFDictionaryRef namesDictionary = NULL;
                if (CGPDFDictionaryGetDictionary(catalogDictionary, "Names", &namesDictionary)) {
                    CGPDFDictionaryRef destsDictionary = NULL;
                    if (CGPDFDictionaryGetDictionary(namesDictionary, "Dests", &destsDictionary)) {
                        const char *destinationName = (const char *)CGPDFStringGetBytePtr(destName);
                        destArray = [self findDestinationByName: destinationName inDestsTree: destsDictionary];
                    }
                }
            }
        }
    }
    
    if (destArray != NULL) {
        int targetPageNumber = 0;
        // First entry in the array is the page the links points to.
        CGPDFDictionaryRef pageDictionaryFromDestArray = NULL;
        if (CGPDFArrayGetDictionary(destArray, 0, &pageDictionaryFromDestArray)) {
            size_t documentPageCount = CGPDFDocumentGetNumberOfPages(document);
            for (int i = 1; i <= documentPageCount; i++) {
                CGPDFPageRef page = CGPDFDocumentGetPage(document, i);
                CGPDFDictionaryRef pageDictionaryFromPage = CGPDFPageGetDictionary(page);
                if (pageDictionaryFromPage == pageDictionaryFromDestArray) {
                    targetPageNumber = i;
                    break;
                }
            }
        } else {
            // Some PDF generators use incorrectly the page number as the first element of the array 
            // instead of a reference to the actual page.
            CGPDFInteger pageNumber = 0;
            if (CGPDFArrayGetInteger(destArray, 0, &pageNumber)) {
                targetPageNumber = (int)(pageNumber + 1);
            }
        }
        
        if (targetPageNumber > 0) {
            linkTarget = [[NSNumber alloc] initWithInt: targetPageNumber]; 
        }
    }
    
    return linkTarget;
}

- (CGPDFArrayRef)findDestinationByName:(const char *)destinationName inDestsTree:(CGPDFDictionaryRef)node {
    CGPDFArrayRef destinationArray = NULL;
    CGPDFArrayRef limitsArray = NULL;
    if (CGPDFDictionaryGetArray(node, "Limits", &limitsArray)) {
        CGPDFStringRef lowerLimit = NULL;
        CGPDFStringRef upperLimit = NULL;
        if (CGPDFArrayGetString(limitsArray, 0, &lowerLimit)) {
            if (CGPDFArrayGetString(limitsArray, 1, &upperLimit)) {
                const unsigned char *ll = CGPDFStringGetBytePtr(lowerLimit);
                const unsigned char *ul = CGPDFStringGetBytePtr(upperLimit);
                if ((strcmp(destinationName, (const char*)ll) < 0) ||
                    (strcmp(destinationName, (const char*)ul) > 0)) {
                    return NULL;
                }
            }
        }
    }
    
    CGPDFArrayRef namesArray = NULL;
    if (CGPDFDictionaryGetArray(node, "Names", &namesArray)) {
        size_t namesCount = CGPDFArrayGetCount(namesArray);
        for (int i = 0; i < namesCount; i = i + 2) {
            CGPDFStringRef destName;
            if (CGPDFArrayGetString(namesArray, i, &destName)) {
                const unsigned char *dn = CGPDFStringGetBytePtr(destName);
                if (strcmp((const char*)dn, destinationName) == 0) {
                    CGPDFDictionaryRef destinationDictionary = NULL;
                    if (CGPDFArrayGetDictionary(namesArray, i + 1, &destinationDictionary)) {
                        if (CGPDFDictionaryGetArray(destinationDictionary, "D", &destinationArray)) {
                            return destinationArray;
                        }
                    }
                    if (!destinationDictionary) {
                        if (CGPDFArrayGetArray(namesArray, i + 1, &destinationArray)) {
                            return destinationArray;
                        }
                    }
                   
                }
            }
        }
    }
    
    CGPDFArrayRef kidsArray = NULL;
    if (CGPDFDictionaryGetArray(node, "Kids", &kidsArray)) {
        size_t kidsCount = CGPDFArrayGetCount(kidsArray);
        for (int i = 0; i < kidsCount; i++) {
            CGPDFDictionaryRef kidNode = NULL;
            if (CGPDFArrayGetDictionary(kidsArray, i, &kidNode)) {
                destinationArray = [self findDestinationByName: destinationName inDestsTree: kidNode];
                if (destinationArray != NULL) {
                    return destinationArray;
                }
            }
        }
    }
    
    return NULL;
}


/*
void copyDictionaryValues (const char *key, CGPDFObjectRef object, void *info) {
//    if (strcmp(key, "RichMediaContent") == 0) {
//        return;
//    }
    NSMutableDictionary* currentDict = [selfClass tempDictionary];
    NSLog(@"key: %s", key);
    CGPDFObjectType type = CGPDFObjectGetType(object);
    switch (type) {
        case kCGPDFObjectTypeString: {
            CGPDFStringRef objectString;
            if (CGPDFObjectGetValue(object, kCGPDFObjectTypeString, &objectString)) {
                NSString *tempStr = (NSString *)CGPDFStringCopyTextString(objectString);
                [currentDict setObject:tempStr
                                        forKey:[NSString stringWithCString:key encoding:NSUTF8StringEncoding]];
                [tempStr release];
                NSLog(@"set string value");
            }
        }
            break;
        case kCGPDFObjectTypeInteger: {
            CGPDFInteger objectInteger;
            if (CGPDFObjectGetValue(object, kCGPDFObjectTypeInteger, &objectInteger)) {
                [currentDict setObject:[NSNumber numberWithInt:objectInteger]
                                        forKey:[NSString stringWithCString:key encoding:NSUTF8StringEncoding]];
                NSLog(@"set int value");
            }
        }
            break;
        case kCGPDFObjectTypeBoolean: {
            CGPDFBoolean objectBool;
            if (CGPDFObjectGetValue(object, kCGPDFObjectTypeBoolean, &objectBool)) {
                [currentDict setObject:[NSNumber numberWithBool:objectBool]
                                        forKey:[NSString stringWithCString:key encoding:NSUTF8StringEncoding]];
                NSLog(@"set boolean value");
            }
        }
            break;
        case kCGPDFObjectTypeName:
        {
            const char * name;
            if( CGPDFObjectGetValue( object, kCGPDFObjectTypeName, &name ) )
                
                [currentDict setObject:[NSString stringWithCString:name encoding:NSUTF8StringEncoding]
                                forKey:[NSString stringWithCString:key encoding:NSUTF8StringEncoding]];
            
            NSLog(@"set name value");
        }
            break;
        case kCGPDFObjectTypeArray : {
            CGPDFArrayRef objectArray;
            if (CGPDFObjectGetValue(object, kCGPDFObjectTypeArray, &objectArray)) {
                NSArray *tempArr = [selfClass copyPDFArray:objectArray];
                [currentDict setObject:tempArr
                                        forKey:[NSString stringWithCString:key encoding:NSUTF8StringEncoding]];
                [tempArr release];
                NSLog(@"set array value");
            }
        }
            break;
        case kCGPDFObjectTypeDictionary : {
            CGPDFDictionaryRef objectDictionary;
            if (CGPDFObjectGetValue(object, kCGPDFObjectTypeDictionary, &objectDictionary)) {
                
                //NSMutableDictionary* dictionary = [[NSMutableDictionary alloc] init];
                NSDictionary* dict = [selfClass copyPDFDictionary:objectDictionary];
                [currentDict setObject:dict
                                        forKey:[NSString stringWithCString:key encoding:NSUTF8StringEncoding]];
                //[dict release];
                //NSMutableDictionary* temp = [selfClass tempDictionary];
                [selfClass resetDictionaryWithDictionary:currentDict];
                NSLog(@"set Dictionary value");
            }
        }
            break;
            
        default:
            break;
    }
}

- (void)extractPDFDictionary{
    NSLog(@"extractingPDFDictionary");
    //CGPDFDictionaryRef oldDict = CGPDFDocumentGetInfo(pdf);
    CGPDFDictionaryApplyFunction(annotationDictionary, copyDictionaryValues, NULL);
}

- (NSArray *)copyPDFArray:(CGPDFArrayRef)arr{
    int i = 0;
    NSMutableArray *temp = [[NSMutableArray alloc] init];
    for(i=0; i<CGPDFArrayGetCount(arr); i++){
        CGPDFObjectRef object;
        CGPDFArrayGetObject(arr, i, &object);
        CGPDFObjectType type = CGPDFObjectGetType(object);
        switch(type){
            case kCGPDFObjectTypeString: {
                CGPDFStringRef objectString;
                if (CGPDFObjectGetValue(object, kCGPDFObjectTypeString, &objectString)) {
                    NSString *tempStr = (NSString *)CGPDFStringCopyTextString(objectString);
                    [temp addObject:tempStr];
                    [tempStr release];
                }
            }
                break;
            case kCGPDFObjectTypeInteger: {
                CGPDFInteger objectInteger;
                if (CGPDFObjectGetValue(object, kCGPDFObjectTypeInteger, &objectInteger)) {
                    [temp addObject:[NSNumber numberWithInt:objectInteger]];
                }
            }
                break;
            case kCGPDFObjectTypeBoolean: {
                CGPDFBoolean objectBool;
                if (CGPDFObjectGetValue(object, kCGPDFObjectTypeBoolean, &objectBool)) {
                    [temp addObject:[NSNumber numberWithBool:objectBool]];
                }
            }
                break;
            case kCGPDFObjectTypeArray : {
                CGPDFArrayRef objectArray;
                if (CGPDFObjectGetValue(object, kCGPDFObjectTypeArray, &objectArray)) {
                    NSArray *tempArr = [selfClass copyPDFArray:objectArray];
                    [temp addObject:tempArr];
                    [tempArr release];
                }
            }
                break;
            case kCGPDFObjectTypeDictionary : {
                CGPDFDictionaryRef objectDictionary;
                if (CGPDFObjectGetValue(object, kCGPDFObjectTypeDictionary, &objectDictionary)) {
                    
                    //NSMutableDictionary* currentDict = tempDictionary;
                    //NSMutableDictionary* dictionary = [[NSMutableDictionary alloc] init];
                   // NSDictionary* dict = [selfClass copyPDFDictionary:objectDictionary];
                    [temp addObject:@""];
                    //[dict release];
                    //NSMutableDictionary* temp = [selfClass tempDictionary];
                    //[self resetDictionaryWithDictionary:currentDict];
                    //NSLog(@"set Dictionary value");
                }
            }
                break;
            default:
                break;
        }
    }
    
    return temp;
}
- (NSDictionary *)copyPDFDictionary:(CGPDFDictionaryRef)dictRef{

    tempDictionary = [[NSMutableDictionary alloc] init];
    CGPDFDictionaryApplyFunction(dictRef, copyDictionaryValues, NULL);
    
    return tempDictionary;
}
- (void) resetDictionaryWithDictionary:(NSMutableDictionary*)oldDictionary
{
    tempDictionary = oldDictionary;
}
 */
@end
