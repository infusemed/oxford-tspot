

#import "PDFScrollView.h"
#import "PDFTiledView.h"
#import "PDFInteractiveOverlay+Widgets.h"
#import "SignatureButton.h"
#import <QuartzCore/QuartzCore.h>
#import "IMArcMediaViewer.h"

#define _UIKeyboardFrameEndUserInfoKey (&UIKeyboardFrameEndUserInfoKey != NULL ? UIKeyboardFrameEndUserInfoKey : @"UIKeyboardBoundsUserInfoKey")
@implementation PDFScrollView

//@synthesize lowResImage;
@synthesize index;
@synthesize pageLoaded;
@synthesize delegate;
@synthesize stopEverything;
@synthesize backgroundImageView;
@synthesize scrollView = _scrollView;
@synthesize dictSavedDocumentInfo;
@synthesize stringFileName;

- (id)initWithFrame:(CGRect)frame PageNumber:(int)pageNumber Path:(NSString*)path ScrollLeftRight:(BOOL)scrollLeftRight formEnabled:(BOOL)formEnabled minZoomScale:(float)minZoomScale
{
	self = [super initWithFrame:frame];
    if (self == nil)
        return nil;
	_lastScaleFactor = 1;
    self.formEnabled = formEnabled;
    self.emailScale = 1.25490201; // Jeff set through trial and error process of working with very large PDFs from vivint
    
    stringFileName = [[[[path componentsSeparatedByString:@"/"] lastObject] componentsSeparatedByString:@"."]objectAtIndex:0];
    
	_scrollView = [[UIScrollView alloc] initWithFrame:CGRectMake(0, 0, frame.size.width, frame.size.height)];
	[self addSubview:_scrollView];
	// Set up the UIScrollView
	_scrollView.showsVerticalScrollIndicator = NO;
	_scrollView.showsHorizontalScrollIndicator = NO;
	_scrollView.bouncesZoom = NO;
	_scrollView.bounces = NO;
	
	_scrollView.decelerationRate = UIScrollViewDecelerationRateFast;
	_scrollView.delegate = self;
	[_scrollView setBackgroundColor:[UIColor grayColor]];
	_scrollView.maximumZoomScale = 10.0;
	_scrollView.minimumZoomScale = minZoomScale;
    _arrImportedImages = [NSMutableArray new];
    _arrImportedImagesInfo = [NSMutableArray new];
    
    [self setAccessibilityLabel:[NSString stringWithFormat:@"MediaViewerPDFPage%d",pageNumber]];
    
	self.pageLoaded = FALSE;
	self.index = pageNumber;
	
	// Open the PDF document
	NSURL *pdfUrl = [NSURL fileURLWithPath:path];
	//NSURL *pdfURL = [[NSBundle mainBundle] URLForResource:@"TestPage.pdf" withExtension:nil];
	pdf = CGPDFDocumentCreateWithURL((CFURLRef)pdfUrl);
    
    widgetOffset =0;
    for (int pageIndex  = 1; pageIndex < pageNumber; pageIndex++) {
        widgetOffset += [PDFInteractiveOverlay widgetCountForPage:CGPDFDocumentGetPage(pdf, pageIndex)];
    }
	
	// Get the PDF Page that we will be drawing
	page = CGPDFDocumentGetPage(pdf, pageNumber);
	CGPDFPageRetain(page);
	
	
	self.stopEverything = FALSE;
	
    
	
	// determine the size of the PDF page
	CGRect pageRect = CGPDFPageGetBoxRect(page, kCGPDFCropBox);
    int rotation = CGPDFPageGetRotationAngle(page);
    if (90 == rotation || 270 == rotation)
        pageRect = CGRectMake(pageRect.origin.x, pageRect.origin.y, pageRect.size.height, pageRect.size.width);
    
    CGFloat widthScale = frame.size.width / pageRect.size.width;
    CGFloat heightScale = frame.size.height / pageRect.size.height;
    pdfScale = MIN(widthScale, heightScale);
    CGSize size = CGSizeMake(pageRect.size.width * pdfScale, pageRect.size.height * pdfScale);
    
    pageRect.size = size;
	_pageRect = pageRect;
	_scrollView.contentSize = pageRect.size;
	
	//add notifications so we know when the keyboard is about to show or when it is about to hide
	_nc = [NSNotificationCenter defaultCenter];
    [_nc addObserver:self selector:@selector(keyboardWillShow:) name: UIKeyboardWillShowNotification object:self.window];
    [_nc addObserver:self selector:@selector(keyboardWillChangeFrames:) name: UIKeyboardWillChangeFrameNotification object:self.window];
	[_nc addObserver:self selector:@selector(keyboardWillHide:) name: UIKeyboardWillHideNotification object:self.window];
    
    
    return self;
}
-(void)stopVideos{
    [interactiveOverlay stopVideos];
}
- (void)setEmailScale:(CGFloat)emailScale
{
    if (emailScale > 0.0) {
        _emailScale = emailScale;
    }
}

-(void) renderLowResForViewing
{
    
    _spinningWheel = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleWhiteLarge];
    _spinningWheel.frame = CGRectMake(0, 0, 35, 35);
    _spinningWheel.center = CGPointMake(self.frame.size.width/2, self.frame.size.height/2);
    [self addSubview:_spinningWheel];
    [_spinningWheel startAnimating];
    
	_thread = [[NSThread alloc] initWithTarget:self selector:@selector(drawLowResImage) object:nil];
    
    [self centerPDF];
}

-(void) renderLowResForEmailing
{
    oldPDFScale = pdfScale;
    pdfScale = self.emailScale;
    
    CGRect drawRect = CGRectMake(0, 0, _scrollView.contentSize.width/oldPDFScale*pdfScale, _scrollView.contentSize.height/oldPDFScale*pdfScale);
    
    UIGraphicsBeginImageContext(CGSizeMake(drawRect.size.width, drawRect.size.height));
    //UIGraphicsBeginImageContext(_scrollView.contentSize);
    
    CGContextRef context = UIGraphicsGetCurrentContext();
    
    
    CGContextSaveGState(context);
    // Flip the context so that the PDF page is rendered
    // right side up.
    int rotation = CGPDFPageGetRotationAngle(page);
    if (90 == rotation)
    {
        CGContextTranslateCTM(context, drawRect.size.width/2, drawRect.size.height/2);
        CGContextRotateCTM(context, 3.14/2);
        CGContextTranslateCTM(context, -drawRect.size.height/2, drawRect.size.width/2);
    }
    else if (270 == rotation)
    {
        CGContextTranslateCTM(context, drawRect.size.width/2, drawRect.size.height/2);
        CGContextRotateCTM(context, 3.14*1.5);
        CGContextTranslateCTM(context, -drawRect.size.height/2, drawRect.size.width/2);
    }
    else
        CGContextTranslateCTM(context, 0.0, drawRect.size.height);
    
    CGContextTranslateCTM(context, -drawRect.origin.x*pdfScale, drawRect.origin.y*pdfScale);
    //CGContextTranslateCTM(context, -_pageRect.origin.x*pdfScale, 0);
    
    //CGContextTranslateCTM(context, 0.0, _pageRect.size.height);
    CGContextScaleCTM(context, 1.0, -1.0);
    
    // Scale the context so that the PDF page is rendered
    // at the correct size for the zoom level.
    CGContextScaleCTM(context, pdfScale,pdfScale);
    
    // First fill the background with white.
    CGContextSetRGBFillColor(context, 1.0,1.0,1.0,1.0);
    CGContextFillRect(context, CGContextGetClipBoundingBox(context));
    
    //CGContextConcatCTM(context, CGPDFPageGetDrawingTransform(page, kCGPDFMediaBox, _pageRect, 0, true));
    CGContextDrawPDFPage(context, page);
    CGContextRestoreGState(context);
    
    UIImage *backgroundImage;
    
    backgroundImage= UIGraphicsGetImageFromCurrentImageContext();
    
    self.pageLoaded = TRUE;
    UIGraphicsEndImageContext();
    
    backgroundImageView = [[UIImageView alloc] initWithImage:backgroundImage];
    backgroundImageView.frame = drawRect;
    backgroundImageView.contentMode = UIViewContentModeScaleAspectFit;
    [_scrollView addSubview:backgroundImageView];
    [_scrollView sendSubviewToBack:backgroundImageView];
    //backgroundImageView.alpha = 0;
    _isLoadingForEmail = YES;
    [self performSelectorOnMainThread:@selector(finishedLoadingLowRes:) withObject:nil waitUntilDone:YES];
    
    //[self addInteractiveLayerForEmailing:TRUE];
    //[self centerPDF];
}
#pragma mark -
#pragma mark Search Results
-(void) removeHighlightsForSearchResults
{
    [interactiveOverlay removeHighlights];
}

#pragma mark -
#pragma mark Keyboard Notifications
-(void) keyboardWillShow:(NSNotification *) note
{
    //get the height of the keyboard so we can move the UITextView up if we need to
    CGRect r = [self convertRect:[[[note userInfo] objectForKey:_UIKeyboardFrameEndUserInfoKey] CGRectValue] fromView:nil];
    
	
    int newHeight = r.origin.y;
    
    if((r.origin.y+r.size.height < self.frame.size.height -20) || r.origin.y+r.size.height >self.frame.size.height){
        newHeight = self.frame.size.height;
    }
    
    [UIView beginAnimations:nil context:nil];
	_scrollView.frame = CGRectMake(_scrollView.frame.origin.x, _scrollView.frame.origin.y, 
                                   _scrollView.frame.size.width, newHeight);
    [UIView commitAnimations];
    
	for (UIView *subView in interactiveOverlay.subviews) {
        if (subView.isFirstResponder){
            [_scrollView scrollRectToVisible:subView.frame animated:NO];
            [subView.inputAccessoryView setHidden:(r.origin.y+r.size.height < self.frame.size.height -20) || r.origin.y+r.size.height >self.frame.size.height];
        }
    }
}
-(void) keyboardWillChangeFrames:(NSNotification *) note
{
    
    CGRect r = [self convertRect:[[[note userInfo] objectForKey:_UIKeyboardFrameEndUserInfoKey] CGRectValue] fromView:nil];
    if(r.origin.y < self.frame.size.height){
        [self keyboardWillShow:note];
    }
}

//When the user selects the hide keyboard button on the keyboard and scale the scrollView back to the height of the screen
-(void) keyboardWillHide:(NSNotification *) note
{
	
	_scrollView.frame = CGRectMake(_scrollView.frame.origin.x, _scrollView.frame.origin.y, 
                                   _scrollView.frame.size.width, self.frame.size.height);
}

#pragma mark -
- (UIImage*) addFormsOverlay:(UIImage*)formsImage toBackground:(UIImage*)backgroundImage
{
    UIGraphicsBeginImageContext(backgroundImage.size);
    [backgroundImage drawAtPoint:CGPointZero];
    
    CGPoint starredPoint = CGPointMake(0, 0);
    [formsImage drawAtPoint:starredPoint];
    
    UIImage* result = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    
    return result;
}

-(void) createAndSaveFormImage
{
    
    UIGraphicsBeginImageContext(backgroundImageView.bounds.size);
    CGContextTranslateCTM(UIGraphicsGetCurrentContext(), -_pageRect.origin.x*pdfScale, _pageRect.origin.y*pdfScale);
    [[backgroundImageView layer] renderInContext:UIGraphicsGetCurrentContext()];
    UIImage *backgroundImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    
    UIGraphicsBeginImageContext(interactiveOverlay.bounds.size);
    [[interactiveOverlay layer] renderInContext:UIGraphicsGetCurrentContext()];
    UIImage *formsImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    
    NSFileManager *fileManager = [NSFileManager defaultManager];
    NSString* tempFilesPath = [IMArcMediaViewer getTemporaryStorageLocationWithPDFFileName:self.stringFileName];
    
    for (UIView *subView in interactiveOverlay.subviews) {
        if([subView isKindOfClass:[SignatureButton class]]){
            SignatureButton * signatureBtn = (SignatureButton*)subView;
            UIGraphicsBeginImageContext(formsImage.size);
            [formsImage drawAtPoint:CGPointZero];
            
            
            NSString * filePath = [NSString stringWithFormat:@"%@/page%i/signature%li.png",tempFilesPath,index , (long)signatureBtn.tag];
            
            UIImage * temp = [UIImage imageWithContentsOfFile:filePath];
            
            [temp drawInRect:signatureBtn.frame];
            
            formsImage = UIGraphicsGetImageFromCurrentImageContext();
            UIGraphicsEndImageContext();
            
        }
    }
    NSData* imageData = UIImagePNGRepresentation([self addFormsOverlay:formsImage toBackground:backgroundImage]);
    [self createFolderFromFullPath:[NSString stringWithFormat:@"%@/PDFSnapshots",tempFilesPath]];
    NSString *fullPath = [NSString stringWithFormat:@"%@/PDFSnapshots/page%i.png",tempFilesPath, index];
    [fileManager createFileAtPath:fullPath contents:imageData attributes:nil];
}
-(void)lockZoom
{
    _maximumZoomScale = _scrollView.maximumZoomScale;
    _minimumZoomScale = _scrollView.minimumZoomScale;
    
    _scrollView.maximumZoomScale = 1.0;
    _scrollView.minimumZoomScale = 1.0;
    
    [self.delegate lockZoomAndScrolling];
}

-(void)unlockZoom
{    
    _scrollView.maximumZoomScale = _maximumZoomScale;
    _scrollView.minimumZoomScale = _minimumZoomScale;
    
    [self.delegate unlockZoomAndScrolling];
    
}


-(void) openLink:(UIButton*)btn
{
	[[UIApplication sharedApplication] openURL:[NSURL URLWithString:btn.currentTitle]];
}

-(void) stopThread
{
	[_thread cancel];
	_thread = nil;
    
    self.stopEverything = TRUE;
    
    if (pdfView) 
        [pdfView stopEverythingNow];
    if (oldPDFView) 
        [oldPDFView stopEverythingNow];
}
-(void) drawReallyLowResFirst:(UIImage*)thumbnail
{
    if (_viewReallyLowRes) {
        [_viewReallyLowRes removeFromSuperview];
    };
    
    _viewReallyLowRes = [[UIImageView alloc] initWithImage:thumbnail];
    _viewReallyLowRes.frame = CGRectMake(0, 0, _scrollView.contentSize.width, _scrollView.contentSize.height);
    [_scrollView addSubview:_viewReallyLowRes];
    
    CGSize boundsSize = _scrollView.bounds.size;
    CGRect frameToCenter = _viewReallyLowRes.frame;
    
    // center horizontally
    if (frameToCenter.size.width < boundsSize.width)
        frameToCenter.origin.x = (boundsSize.width - frameToCenter.size.width) / 2;
    else
        frameToCenter.origin.x = 0;
    
    // center vertically
    if (frameToCenter.size.height < boundsSize.height)
        frameToCenter.origin.y = (boundsSize.height - frameToCenter.size.height) / 2;
    else
        frameToCenter.origin.y = 0;
    
    _viewReallyLowRes.frame = frameToCenter;
    [_thread start]; //after low res has been added start drawing drawing pdf
}
// Create a low res image representation of the PDF page to display before the TiledPDFView
// renders its content.
- (void) drawLowResImage
{
	@synchronized(self)
	{
		
        oldPDFScale = pdfScale;
		UIGraphicsBeginImageContext(_scrollView.contentSize);
		
		CGContextRef context = UIGraphicsGetCurrentContext();
		
		
		CGContextSaveGState(context);
		// Flip the context so that the PDF page is rendered
		// right side up.
        int rotation = CGPDFPageGetRotationAngle(page);
        if (90 == rotation)
        {
            CGContextTranslateCTM(context, _pageRect.size.width/2, _pageRect.size.height/2);
            CGContextRotateCTM(context, 3.14/2);
            CGContextTranslateCTM(context, -_pageRect.size.height/2, _pageRect.size.width/2);
        }
        else if (270 == rotation)
        {
            CGContextTranslateCTM(context, _pageRect.size.width/2, _pageRect.size.height/2);
            CGContextRotateCTM(context, 3.14*1.5);
            CGContextTranslateCTM(context, -_pageRect.size.height/2, _pageRect.size.width/2);
        }
        else
            CGContextTranslateCTM(context, 0.0, _pageRect.size.height);
        
        CGContextTranslateCTM(context, -_pageRect.origin.x*pdfScale, _pageRect.origin.y*pdfScale);
        //CGContextTranslateCTM(context, -_pageRect.origin.x*pdfScale, 0);
        
		//CGContextTranslateCTM(context, 0.0, _pageRect.size.height);
		CGContextScaleCTM(context, 1.0, -1.0);
		
		// Scale the context so that the PDF page is rendered 
		// at the correct size for the zoom level.
		CGContextScaleCTM(context, pdfScale,pdfScale);
        
        // First fill the background with white.
        CGContextSetRGBFillColor(context, 1.0,1.0,1.0,1.0);
        CGContextFillRect(context, CGContextGetClipBoundingBox(context));
        
        //CGContextConcatCTM(context, CGPDFPageGetDrawingTransform(page, kCGPDFMediaBox, _pageRect, 0, true));
		CGContextDrawPDFPage(context, page);
		CGContextRestoreGState(context);
		
		UIImage *backgroundImage;
		
		backgroundImage= UIGraphicsGetImageFromCurrentImageContext();
		
		
		if (!_thread) {
			UIGraphicsEndImageContext();
		}
		else {
			self.pageLoaded = TRUE;
			UIGraphicsEndImageContext();
			//backgroundImageView.alpha = 0;
            [self performSelectorOnMainThread:@selector(finishedLoadingLowRes:) withObject:backgroundImage waitUntilDone:YES];
			
            _isLoadingForEmail = NO;
            [self performSelectorOnMainThread:@selector(removeReallyLowResOnMainThread) withObject:nil waitUntilDone:YES];
		}
	}
}
-(void)removeReallyLowResOnMainThread{
    if (_viewReallyLowRes) {
        [_viewReallyLowRes removeFromSuperview];
        _viewReallyLowRes = nil;
    }
}
-(void) finishedLoadingLowRes:(UIImage*)backgroundImage
{
    
    if(backgroundImage != nil){
        backgroundImageView = [[UIImageView alloc] initWithImage:backgroundImage];
        backgroundImageView.frame = _pageRect;
        backgroundImageView.contentMode = UIViewContentModeScaleAspectFit;
        [_scrollView addSubview:backgroundImageView];
        [_scrollView sendSubviewToBack:backgroundImageView];
        
    }
    
    if (self.stopEverything) {
        return;
    }
    [self addInteractiveLayerForEmailing:_isLoadingForEmail];
    [_spinningWheel removeFromSuperview];
    _spinningWheel = nil;
    
	if (self.stopEverything) {
		return;
	}
    
    [self centerPDF];
	[[self delegate] finishedLoading];
    
    [interactiveOverlay loadDefaultValues:_defaultValues andFieldNames:_fieldNames];
    [interactiveOverlay disableFields:_disabledFields];
    pdfScale = oldPDFScale;
    
    if (!_isLoadingForEmail) {
        [_scrollView setZoomScale:_scrollView.minimumZoomScale animated:YES];
    }
}
-(void) addInteractiveLayerForEmailing:(BOOL)forEmailing
{
    NSMutableArray* pdfFormInfo = [dictSavedDocumentInfo objectForKey:@"SavedFormInfo"];
    if (!pdfFormInfo) {
        pdfFormInfo = [[NSMutableArray alloc] init];
        [dictSavedDocumentInfo setObject:pdfFormInfo forKey:@"SavedFormInfo"];
    }
 
    
    interactiveOverlay = [[PDFInteractiveOverlay alloc] initWithFrame:backgroundImageView.frame];
    interactiveOverlay.searchDocument = self.searchDocument;
    interactiveOverlay.formEnabled = self.formEnabled;
    interactiveOverlay.stringFileName = stringFileName;
    interactiveOverlay.stringFormName = self.stringFormName;
    interactiveOverlay.boolEmailing = forEmailing;
    interactiveOverlay.viewParent = self;
    interactiveOverlay.arrSavedFormInfo = pdfFormInfo;
    interactiveOverlay.tag = self.index;
    interactiveOverlay.delegate = self;
    [interactiveOverlay setDisabledFields:_disabledFields];
    [interactiveOverlay loadDefaultValues:_defaultValues andFieldNames:_fieldNames];
    interactiveOverlay.scale = pdfScale;
    [interactiveOverlay setPage:page pageNumber:self.index widgetIndexOffset:widgetOffset];
    interactiveOverlay.styleSignature = self.styleSignature;
    //interactiveOverlay.exclusiveTouch = FALSE;
    [_scrollView addSubview:interactiveOverlay];
    
    _arrImportedImagesInfo = [dictSavedDocumentInfo objectForKey:@"ArrayOfImportedImages"];
    if (!_arrImportedImagesInfo) {
        _arrImportedImagesInfo = [[NSMutableArray alloc] init];
        [dictSavedDocumentInfo setObject:_arrImportedImagesInfo forKey:@"ArrayOfImportedImages"];
    }
    
    for (NSMutableDictionary* importedImageInfo in _arrImportedImagesInfo)
    {
        BOOL fileExists = [[NSFileManager defaultManager] fileExistsAtPath:[importedImageInfo objectForKey:@"imageName"]];
        if (fileExists){
            ImportedImageView * savedImage = [self importImage:[UIImage imageWithContentsOfFile:[importedImageInfo objectForKey:@"imageName"]] withOrigin:CGPointMake([[importedImageInfo objectForKey:@"originX"] intValue], [[importedImageInfo objectForKey:@"originY"] intValue])];
            float scaler = pdfScale;
            savedImage.frame = CGRectMake(savedImage.frame.origin.x*scaler, savedImage.frame.origin.y*scaler,[[importedImageInfo objectForKey:@"width"] intValue]*scaler, [[importedImageInfo objectForKey:@"height"] intValue]*scaler);
            savedImage.imageName =[importedImageInfo objectForKey:@"imageName"];
            savedImage.imageID = [importedImageInfo objectForKey:@"imageID"];
            
            savedImage.closeBtn.frame = CGRectMake(savedImage.frame.origin.x, savedImage.frame.origin.y, savedImage.closeBtn.frame.size.width, savedImage.closeBtn.frame.size.height);
            [self reEnable:savedImage];
            if (forEmailing){
                [savedImage.closeBtn removeFromSuperview];
                [savedImage.closeBtnImage removeFromSuperview];
            }
        }
    }
    
}
-(void) isEditingSignature;
{
    [self lockZoom];
    _scrollView.scrollEnabled = FALSE;
}

-(void) doneEditingSignature;
{
    [self unlockZoom];
    _scrollView.scrollEnabled = TRUE;
}

#pragma mark -
#pragma mark Import Image delegate methods
NSString *letters = @"abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";

-(NSString *) genRandStringLength: (int) len {
    
    NSMutableString *randomString = [NSMutableString stringWithCapacity: len];
    
    for (int i=0; i<len; i++) {
        [randomString appendFormat: @"%C", [letters characterAtIndex: arc4random() % [letters length]]];
    }
    
    return randomString;
}

-(ImportedImageView*) importImage:(UIImage*)image withOrigin:(CGPoint)origin
{
    ImportedImageView* importedImage = [[ImportedImageView alloc] initWithImage:image];
    importedImage.frame = CGRectMake(origin.x, origin.y, image.size.width, image.size.height);
    importedImage.userInteractionEnabled = TRUE;
    importedImage.imageID =[self genRandStringLength:10];
    [interactiveOverlay addSubview:importedImage];
    [_arrImportedImages addObject:importedImage];
    
    
    ImportedImageCloseBtn* btnClose = [ImportedImageCloseBtn buttonWithType:UIButtonTypeCustom];
    btnClose.frame = CGRectMake(importedImage.frame.origin.x, importedImage.frame.origin.y, 50, 50);
    [btnClose addTarget:self action:@selector(closeImportedImage:) forControlEvents:UIControlEventTouchUpInside];
    [btnClose setImage:[UIImage imageNamed:@"pdf_imported_image_close.png"] forState:UIControlStateNormal];
    [interactiveOverlay addSubview:btnClose];
    btnClose.autoresizingMask = UIViewAutoresizingNone;
    
    btnClose.importedImage = importedImage;
    importedImage.closeBtn = btnClose;
    
    _pinchGestureImportedImage = [[UIPinchGestureRecognizer alloc] initWithTarget:self action:@selector(handlePinchGesture:)];
    _pinchGestureImportedImage.delegate = self;
    [_pinchGestureImportedImage requireGestureRecognizerToFail:_scrollView.pinchGestureRecognizer];
    [importedImage addGestureRecognizer:_pinchGestureImportedImage];
    
    _panTranslateImportedImage = [[UIPanGestureRecognizer alloc] initWithTarget:self action:@selector(handlePanGesture:)];
    [importedImage addGestureRecognizer:_panTranslateImportedImage];
    //[_panTranslateImportedImage requireGestureRecognizerToFail:_scrollView.panGestureRecognizer];
    [_panTranslateImportedImage setMaximumNumberOfTouches:1];
    [_panTranslateImportedImage setMinimumNumberOfTouches:1];
    
    UITapGestureRecognizer* tapRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(handleTapGesture:)];
    [importedImage addGestureRecognizer:tapRecognizer];
    //[_panTranslateImportedImage requireGestureRecognizerToFail:_scrollView.panGestureRecognizer];
    [tapRecognizer setNumberOfTapsRequired:1];
    
    UILongPressGestureRecognizer* longRecognizer = [[UILongPressGestureRecognizer alloc] initWithTarget:self action:@selector(longPressGesture:)];
    [importedImage addGestureRecognizer:longRecognizer];
    
    [_scrollView.panGestureRecognizer setEnabled:NO];
    [_scrollView.pinchGestureRecognizer setEnabled:NO];
    
    importedImage.layer.shadowRadius = 10.0f;
    importedImage.layer.shadowOffset = CGSizeMake(3,3);
    importedImage.layer.borderColor = [[UIColor blueColor] CGColor];
    importedImage.layer.borderWidth = 10.0f;
    
    
    [NSObject cancelPreviousPerformRequestsWithTarget:self];
    [self performSelector:@selector(reEnable:) withObject:importedImage afterDelay:2.0f];
    
    return importedImage;
}
-(void) importImage:(UIImage*)image WithName:(NSString*)imageName
{
    
    NSString* tempFilesPath = [IMArcMediaViewer getTemporaryStorageLocationWithPDFFileName:self.stringFileName];
    
    NSString * importedPath = [tempFilesPath stringByAppendingString:@"/Imported"];
    [self createFolderFromFullPath:importedPath];
    
    NSString *filePath = [NSString stringWithFormat:@"%@/%@.png",importedPath, [self genRandStringLength:10]];
    while([[NSFileManager defaultManager] fileExistsAtPath:filePath]){
        filePath = [NSString stringWithFormat:@"%@/%@.png",importedPath
                    , [self genRandStringLength:10]];
    }
    [[NSFileManager defaultManager] createFileAtPath:filePath contents:UIImagePNGRepresentation(image) attributes:nil];
    
    
    ImportedImageView* importedImage = [self importImage:image withOrigin:CGPointZero];
    float widthRatio = MIN(interactiveOverlay.frame.size.width, importedImage.image.size.width)/importedImage.image.size.width;
    [importedImage setFrame:CGRectMake(0, 0, importedImage.image.size.width*widthRatio, importedImage.image.size.height*widthRatio)];
    [importedImage setCenter:CGPointMake(interactiveOverlay.frame.size.width/2.f, interactiveOverlay.frame.size.height/2.f)];
    [importedImage.closeBtn setFrame:CGRectMake(importedImage.frame.origin.x, importedImage.frame.origin.y, importedImage.closeBtn.frame.size.width, importedImage.closeBtn.frame.size.height)];
    
    importedImage.imageName = filePath;
    
    [self saveImportedImage:importedImage];
    
}
- (void) closeImportedImage:(ImportedImageCloseBtn*)btn
{
    
    [_scrollView.panGestureRecognizer setEnabled:YES];
    [_scrollView.pinchGestureRecognizer setEnabled:YES];
    ImportedImageView* importedImage = (ImportedImageView*) btn.importedImage;
    for (NSMutableDictionary* dictionary in _arrImportedImagesInfo)
    {
        if ([[dictionary objectForKey:@"imageName"] isEqualToString:importedImage.imageName]) {
            [_arrImportedImagesInfo removeObject:dictionary];
            [dictSavedDocumentInfo setObject:_arrImportedImagesInfo forKey:@"ArrayOfImportedImages"];
            [self saveFormData];
            break;
        }
    }
    [NSObject cancelPreviousPerformRequestsWithTarget:self];
    importedImage.imageReleased = TRUE;
    [importedImage removeFromSuperview];
    [btn removeFromSuperview];
    
    importedImage = nil;
    btn = nil;
    
}
-(void)reEnable:(ImportedImageView *)view{
    [NSObject cancelPreviousPerformRequestsWithTarget:self];
    [_scrollView.panGestureRecognizer setEnabled:YES];
    [_scrollView.pinchGestureRecognizer setEnabled:YES];
    
    for (UIGestureRecognizer * recognizer in view.gestureRecognizers) {
        if(![recognizer isKindOfClass:[UILongPressGestureRecognizer class]]){
            [recognizer setEnabled:NO];
        }
    }
    
    view.closeBtn.alpha = 0.25f;
    
    view.layer.shadowRadius = 0.0f;
    view.layer.shadowOffset = CGSizeMake(0,0);
    view.layer.borderColor = [[UIColor clearColor] CGColor];
    view.layer.borderWidth = 0.0f;
}
-(void)longPressGesture:(UILongPressGestureRecognizer*)sender{
    [_scrollView.panGestureRecognizer setEnabled:NO];
    [_scrollView.pinchGestureRecognizer setEnabled:NO];
    
    ImportedImageView *view = (ImportedImageView*)sender.view;
    view.closeBtn.alpha = 1;
    for (UIGestureRecognizer * recognizer in view.gestureRecognizers) {
        [recognizer setEnabled:YES];
    }
    
    view.layer.shadowRadius = 10.0f;
    view.layer.shadowOffset = CGSizeMake(3,3);
    view.layer.borderColor = [[UIColor blueColor] CGColor];
    view.layer.borderWidth = 10.0f;

    
}
- (BOOL)gestureRecognizer:(UIGestureRecognizer *)gestureRecognizer shouldRecognizeSimultaneouslyWithGestureRecognizer:(UIGestureRecognizer *)otherGestureRecognizer
{
    if ((gestureRecognizer == _pinchGestureImportedImage && otherGestureRecognizer == _panTranslateImportedImage) ||
        (gestureRecognizer == _panTranslateImportedImage && otherGestureRecognizer == _pinchGestureImportedImage)) {
        return TRUE;
    }
    return FALSE;
}
- (void)handlePanGesture:(UIGestureRecognizer *)sender 
{
   
   
    ImportedImageView* pannedView = (ImportedImageView*)sender.view;
    pannedView.closeBtn.alpha = 1;
    
    
    [NSObject cancelPreviousPerformRequestsWithTarget:self];
    [self performSelector:@selector(reEnable:) withObject:pannedView afterDelay:2.0f];
    
    [interactiveOverlay bringSubviewToFront:pannedView];
    [interactiveOverlay bringSubviewToFront:pannedView.closeBtn];
    
    CGPoint point = [sender locationInView:interactiveOverlay];
    if ([sender state] == UIGestureRecognizerStateBegan) {
        _pointPrev = point;
    }
    else if ([sender state] == UIGestureRecognizerStateChanged)
    {
        CGPoint translation = CGPointMake( point.x-_pointPrev.x, point.y - _pointPrev.y);
        //helps prevent the jumping if one finger is lifted off before the other
        if (fabs(translation.x) > 40 || fabs(translation.y) > 40 ) {
            return;
        }
        pannedView.center = CGPointMake(pannedView.center.x+translation.x, pannedView.center.y+translation.y);
        _pointPrev = point;
        
        pannedView.closeBtn.frame = CGRectMake(pannedView.frame.origin.x, pannedView.frame.origin.y, pannedView.closeBtn.frame.size.width, pannedView.closeBtn.frame.size.height);
    }
    else if ([sender state] == UIGestureRecognizerStateEnded)
    {
        [self saveImportedImage:pannedView];
    }
    [interactiveOverlay bringSubviewToFront:pannedView.closeBtn];
}
- (void)handlePinchGesture:(UIPinchGestureRecognizer *)sender {
    
    
    ImportedImageView *view = (ImportedImageView*)sender.view;
    view.closeBtn.alpha = 1;
    
    CGPoint parentPoint = [sender locationInView:interactiveOverlay];
    
    [NSObject cancelPreviousPerformRequestsWithTarget:self];
    [self performSelector:@selector(reEnable:) withObject:view afterDelay:2.0f];
    
    [interactiveOverlay bringSubviewToFront:view];
    [interactiveOverlay bringSubviewToFront:view.closeBtn];
    
    if (sender.state == UIGestureRecognizerStateBegan){
        _lastScaleFactor = view.contentScaleFactor;
    }
    else if (sender.state == UIGestureRecognizerStateChanged){
        view.transform = CGAffineTransformScale([view transform], [sender scale], [sender scale]);
        [view setCenter:parentPoint];
        [sender setScale:1];
        
        view.closeBtn.frame = CGRectMake(view.frame.origin.x, view.frame.origin.y, view.closeBtn.frame.size.width, view.closeBtn.frame.size.height);
    }
    else if ([sender state] == UIGestureRecognizerStateEnded)
    {
        [self saveImportedImage:view];
    }
    return;
}

- (void)handleTapGesture:(UIGestureRecognizer *)sender
{
    
    [_scrollView.panGestureRecognizer setEnabled:NO];
    [_scrollView.pinchGestureRecognizer setEnabled:NO];
    
    ImportedImageView *view = (ImportedImageView*)sender.view;
    view.closeBtn.alpha = 1;
    for (UIGestureRecognizer * recognizer in view.gestureRecognizers) {
        [recognizer setEnabled:YES];
    }
    
    view.layer.shadowRadius = 10.0f;
    view.layer.shadowOffset = CGSizeMake(3,3);
    view.layer.borderColor = [[UIColor blackColor] CGColor];
    view.layer.borderWidth = 10.0f;
    
    [self keepTitleAlpha];
}
-(void)saveImportedImage:(ImportedImageView*)importedImage
{
    
    
    NSMutableDictionary* imageInfo = nil;
    for (NSMutableDictionary* dictionary in _arrImportedImagesInfo)
    {
        if ([[dictionary objectForKey:@"imageName"] isEqualToString:importedImage.imageName]) {
            imageInfo = dictionary;
            break;
        }
    }
    if (imageInfo)
    {
        [imageInfo setObject:[NSString stringWithFormat:@"%i", (int)(importedImage.frame.origin.x/pdfScale)] forKey:@"originX"];
        [imageInfo setObject:[NSString stringWithFormat:@"%i", (int)(importedImage.frame.origin.y/pdfScale)] forKey:@"originY"];
        [imageInfo setObject:[NSString stringWithFormat:@"%i", (int)(importedImage.center.x/pdfScale)] forKey:@"centerX"];
        [imageInfo setObject:[NSString stringWithFormat:@"%i", (int)(importedImage.center.y/pdfScale)] forKey:@"centerY"];
        [imageInfo setObject:[NSString stringWithFormat:@"%i", (int)(importedImage.frame.size.width/pdfScale)] forKey:@"width"];
        [imageInfo setObject:[NSString stringWithFormat:@"%i", (int)(importedImage.frame.size.height/pdfScale)] forKey:@"height"];
        [imageInfo setObject:importedImage.imageID forKey:@"imageID"];
    }
    else
    {
        imageInfo = [[NSMutableDictionary alloc] init];
        [imageInfo setObject:importedImage.imageID forKey:@"imageID"];
        [imageInfo setObject:importedImage.imageName forKey:@"imageName"];
        [imageInfo setObject:[NSString stringWithFormat:@"%i", (int)(importedImage.center.x/pdfScale)] forKey:@"centerX"];
        [imageInfo setObject:[NSString stringWithFormat:@"%i", (int)(importedImage.center.y/pdfScale)] forKey:@"centerY"];
        [imageInfo setObject:[NSString stringWithFormat:@"%i", (int)(importedImage.frame.origin.x/pdfScale)] forKey:@"originX"];
        [imageInfo setObject:[NSString stringWithFormat:@"%i", (int)(importedImage.frame.origin.y/pdfScale)] forKey:@"originY"];
        [imageInfo setObject:[NSString stringWithFormat:@"%i", (int)(importedImage.frame.size.width/pdfScale)] forKey:@"width"];
        [imageInfo setObject:[NSString stringWithFormat:@"%i", (int)(importedImage.frame.size.height/pdfScale)] forKey:@"height"];
        
        [_arrImportedImagesInfo addObject:imageInfo];
        
    }
}
#pragma mark -
#pragma mark UIScrollView delegate methods

// A UIScrollView delegate callback, called when the user starts zooming. 
- (UIView *)viewForZoomingInScrollView:(UIScrollView *)scrollView
{
    return backgroundImageView;
}

// A UIScrollView delegate callback, called when the user stops zooming.  When the user stops zooming
// we create a new TiledPDFView based on the new zoom level and draw it on top of the old TiledPDFView.
- (void)scrollViewDidEndZooming:(UIScrollView *)scrollView withView:(UIView *)view atScale:(CGFloat)scale
{
	// set the new scale factor for the TiledPDFView
	pdfScale = scale;
    
    interactiveOverlay.hidden = FALSE;
    interactiveOverlay.transform = CGAffineTransformMakeScale(scale, scale);
    interactiveOverlay.scale = scale;
    
	// Calculate the new frame for the new TiledPDFView
	CGRect pageRect = CGPDFPageGetBoxRect(page, kCGPDFCropBox);
    
    int rotation = CGPDFPageGetRotationAngle(page);
    if (90 == rotation)
        pageRect = CGRectMake(pageRect.origin.x, pageRect.origin.y, pageRect.size.height, pageRect.size.width);
    
    pdfScale = backgroundImageView.frame.size.width/pageRect.size.width;
	//pageRect.size = CGSizeMake(pageRect.size.width*pdfScale, pageRect.size.height*pdfScale);
    
    
    //backgroundImageView.frame = pageRect;
	
	// Create a new TiledPDFView based on new frame and scaling.
	pdfView = [[PDFTiledView alloc] initWithFrame:backgroundImageView.frame andScale:pdfScale];
    pdfView.index = self.index;
	[pdfView setPage:page];
    pdfView.pdfDcoument = pdf;
	// Add the new TiledPDFView to the PDFScrollView.
	[_scrollView addSubview:pdfView];
    
	[self centerPDF];
    
    [_scrollView bringSubviewToFront:interactiveOverlay];
    
    [self zoomToEdges];
    
    [interactiveOverlay removeFromSuperview];
    [interactiveOverlay stopVideos];
    interactiveOverlay = nil;
    
    [NSObject cancelPreviousPerformRequestsWithTarget:self];
    [self addInteractiveLayerForEmailing:NO];
}



-(void) zoomToEdges
{
    CGSize boundsSize = _scrollView.bounds.size;
    
    
    BOOL widthSmallerThanScreen = FALSE;
    // center horizontally
    if (backgroundImageView.frame.size.width < boundsSize.width)
    {
        widthSmallerThanScreen = TRUE;
    }
    
    // center vertically
    if (backgroundImageView.frame.size.height < boundsSize.height*_scrollView.minimumZoomScale)
    {
        if (widthSmallerThanScreen) {
            float scrollViewRatio = _scrollView.frame.size.width / _scrollView.frame.size.height;
            float backgroundViewRatio = backgroundImageView.frame.size.width / backgroundImageView.frame.size.height;
            float zoom;
            if(scrollViewRatio > backgroundViewRatio)
                zoom = _scrollView.frame.size.height*_scrollView.minimumZoomScale/_pageRect.size.height;
            else
                zoom = _scrollView.frame.size.width/_pageRect.size.width;
            
            [_scrollView setZoomScale:zoom animated:TRUE];
        }
    }
}
// A UIScrollView delegate callback, called when the user begins zooming.  When the user begins zooming
// we remove the old TiledPDFView and set the current TiledPDFView to be the old view so we can create a
// a new TiledPDFView when the zooming ends.

- (void)scrollViewWillBeginZooming:(UIScrollView *)scrollView withView:(UIView *)view
{
    interactiveOverlay.hidden = TRUE;
    [pdfView removeFromSuperview];
}
- (void)scrollViewDidZoom:(UIScrollView *)scrollView 
{
	[self centerPDF];
}
-(void) centerPDF
{
	CGSize boundsSize = _scrollView.bounds.size;
    CGRect frameToCenter = backgroundImageView.frame;//pdfView.frame;
    
    //BOOL widthSmallerThanScreen = FALSE;
    // center horizontally
    if (frameToCenter.size.width < boundsSize.width){
        //widthSmallerThanScreen = TRUE;
        frameToCenter.origin.x = (boundsSize.width - frameToCenter.size.width) / 2;
    }else{
        frameToCenter.origin.x = 0;
    }
    
    // center vertically
    if (frameToCenter.size.height < boundsSize.height){
        frameToCenter.origin.y = (boundsSize.height - frameToCenter.size.height) / 2;
    }else{
        frameToCenter.origin.y = 0;
    }
	//oldPDFView.frame = frameToCenter;
    //pdfView.frame = frameToCenter;
	backgroundImageView.frame = frameToCenter;
    interactiveOverlay.frame = frameToCenter;
	// to handle the interaction between CATiledLayer and high resolution screens, we need to manually set the
	// tiling view's contentScaleFactor to 1.0. (If we omitted this, it would be 2.0 on high resolution screens,
	// which would cause the CATiledLayer to ask us for tiles of the wrong scales.)
	
    //pdfView.contentScaleFactor = 1.0;
}
-(void) dismissKeyboard
{
    [interactiveOverlay dismissKeyboard];
}
-(void) clearFormDataWithDefaultValues:(NSDictionary*)defaultValues andFieldNames:(NSArray*)fieldNames
{
    /*
    NSMutableArray* pdfFormInfo = [dictSavedDocumentInfo objectForKey:@"SavedFormInfo"];
    if (pdfFormInfo)
        [pdfFormInfo removeAllObjects];*/
    
    [interactiveOverlay clearFormDataWithDefaultValues:defaultValues andFieldNames:fieldNames];
}
-(void) loadDefaultValues:(NSDictionary*)defaultValues andFieldNames:(NSArray*)fieldNames{
    _defaultValues = defaultValues;
    _fieldNames = fieldNames;
}
-(void) loadDisabledFields:(NSArray*)disabledFields{
    _disabledFields = disabledFields;
}
-(void) documentIsForm
{
    [self.delegate documentIsForm];
}
-(void) keepTitleAlpha
{
    [self.delegate keepTitleAlpha];
}
-(void) jumpToPage:(int)pageNumber
{
    [self.delegate jumpToPage:pageNumber];
}
-(void) saveFormData
{
    [interactiveOverlay saveFormData];
}
- (void) createFolder:(NSString*)folderName
{
	NSError* error;
	NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
	NSString *documentsDirectory = [paths objectAtIndex:0];
	NSString *dataPath = [documentsDirectory stringByAppendingPathComponent:folderName];
	
	if (![[NSFileManager defaultManager] fileExistsAtPath:dataPath])
		[[NSFileManager defaultManager] createDirectoryAtPath:dataPath withIntermediateDirectories:NO attributes:nil error:&error]; //Create folder
}
- (void) createFolderFromFullPath:(NSString*)folderPath
{
    NSError* error;
    
    if (![[NSFileManager defaultManager] fileExistsAtPath:folderPath])
        [[NSFileManager defaultManager] createDirectoryAtPath:folderPath withIntermediateDirectories:YES attributes:nil error:&error]; //Create folder
}
- (void)dealloc
{
    [_nc removeObserver:self];
    if (_spinningWheel) {
        [_spinningWheel removeFromSuperview];
        _spinningWheel = nil;
    }
	if (_thread) {
		if (!self.stopEverything) {
			[[self delegate] finishedLoading];
		}
		
		[_thread cancel];
		_thread = nil;
	}
	[pdfView removeTileLayer];
	[pdfView removeFromSuperview];
	//oldPDFView = nil;
	pdfView = nil;
    
	CGPDFPageRelease(page);
	CGPDFDocumentRelease(pdf);
}
-(void)setStringFormName:(NSString *)stringFormName{
    _stringFormName = stringFormName;
    interactiveOverlay.stringFormName  = _stringFormName;
}

-(void)saveImportedImageToFile:(ImportedImageView*)importedImage
{
    NSString* tempImagesPath = [IMArcMediaViewer getTemporaryStorageLocationWithPDFFileName:self.stringFileName];
    [self createFolderFromFullPath:tempImagesPath];
    
    NSData* imageData = UIImagePNGRepresentation(importedImage.image);
    
    NSFileManager *fileManager = [NSFileManager defaultManager];
    NSString *fullPath = [NSString stringWithFormat:@"%@/%@.png",tempImagesPath, importedImage.imageName];
    
    if(![fileManager fileExistsAtPath:fullPath])
        [fileManager createFileAtPath:fullPath contents:imageData attributes:nil];
    
    
    NSMutableDictionary* imageInfo = [[NSMutableDictionary alloc] init];
    [imageInfo setObject:importedImage.imageName forKey:@"imageName"];
    [imageInfo setObject:importedImage.imageID forKey:@"imageID"];
    [imageInfo setObject:fullPath forKey:@"imagePath"];
    [imageInfo setObject:[NSString stringWithFormat:@"%i", (int)(importedImage.center.x/pdfScale)] forKey:@"centerX"];
    [imageInfo setObject:[NSString stringWithFormat:@"%i", (int)(importedImage.center.y/pdfScale)] forKey:@"centerY"];
    [imageInfo setObject:[NSString stringWithFormat:@"%i", (int)(importedImage.frame.origin.x/pdfScale)] forKey:@"originX"];
    [imageInfo setObject:[NSString stringWithFormat:@"%i", (int)(importedImage.frame.origin.y/pdfScale)] forKey:@"originY"];
    [imageInfo setObject:[NSString stringWithFormat:@"%i", (int)(importedImage.frame.size.width/pdfScale)] forKey:@"width"];
    [imageInfo setObject:[NSString stringWithFormat:@"%i", (int)(importedImage.frame.size.height/pdfScale)] forKey:@"height"];
    
    [_arrImportedImagesInfo addObject:imageInfo];
}
-(void)saveAllImportedImagesInfoForFormName:(NSString*)formName forTemp:(bool)forTemp
{
    for (int i = 0; i < [_arrImportedImagesInfo count]; i++) {
        NSMutableDictionary* dictionary = [_arrImportedImagesInfo objectAtIndex:i];
        BOOL imageStillExists = FALSE;
        for (ImportedImageView* importedImage in _arrImportedImages){
            if ([dictionary objectForKey:@"imageID"]) {
                if ([[dictionary objectForKey:@"imageID"] isEqualToString:importedImage.imageID]) {
                    imageStillExists = TRUE;
                    [dictionary setObject:[NSString stringWithFormat:@"%i", (int)(importedImage.center.x/pdfScale)] forKey:@"centerX"];
                    [dictionary setObject:[NSString stringWithFormat:@"%i", (int)(importedImage.center.y/pdfScale)] forKey:@"centerY"];
                    [dictionary setObject:[NSString stringWithFormat:@"%i", (int)(importedImage.frame.origin.x/pdfScale)] forKey:@"originX"];
                    [dictionary setObject:[NSString stringWithFormat:@"%i", (int)(importedImage.frame.origin.y/pdfScale)] forKey:@"originY"];
                    [dictionary setObject:[NSString stringWithFormat:@"%i", (int)(importedImage.frame.size.width/pdfScale)] forKey:@"width"];
                    [dictionary setObject:[NSString stringWithFormat:@"%i", (int)(importedImage.frame.size.height/pdfScale)] forKey:@"height"];
                    
                    break;
                }
            }
            else{
                if ([[dictionary objectForKey:@"imageName"] isEqualToString:importedImage.imageName]) {
                    [dictionary setObject:[NSString stringWithFormat:@"%i", (int)importedImage.center.x] forKey:@"centerX"];
                    [dictionary setObject:[NSString stringWithFormat:@"%i", (int)importedImage.center.y] forKey:@"centerY"];
                    [dictionary setObject:[NSString stringWithFormat:@"%i", (int)importedImage.frame.origin.x] forKey:@"originX"];
                    [dictionary setObject:[NSString stringWithFormat:@"%i", (int)importedImage.frame.origin.y] forKey:@"originY"];
                    [dictionary setObject:[NSString stringWithFormat:@"%i", (int)importedImage.frame.size.width] forKey:@"width"];
                    [dictionary setObject:[NSString stringWithFormat:@"%i", (int)importedImage.frame.size.height] forKey:@"height"];
                    break;
                }
            }
            
        }
        if (!imageStillExists) {
            [_arrImportedImagesInfo removeObjectAtIndex:i];
            i--;
        }
    }
    [interactiveOverlay saveImagesWithFormName:formName forTemp:forTemp];
    if (!forTemp)
        [dictSavedDocumentInfo setObject:_arrImportedImagesInfo forKey:@"ArrayOfImportedImages"];
    
}

@end
