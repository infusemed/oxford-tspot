//
//  IMArcWebViewController.h
//  ArcMediaViewer
//
//  Created by Infuse Medical on 2/14/14.
//  Copyright (c) 2014 Infuse Medical. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "IMArcMediaViewerChild.h"

@interface IMArcWebViewController : IMArcMediaViewerChild <UIWebViewDelegate>

@property (nonatomic, strong) UIWebView *webView;
@property (nonatomic, strong) UIActivityIndicatorView *spinningWheel;

@end
