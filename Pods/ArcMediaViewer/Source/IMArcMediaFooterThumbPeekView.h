//
//  IMArcMediaFooterThumbPeekView.h
//  iPhoneTest
//
//  Created by Infuse Medical on 5/13/14.
//  Copyright (c) 2014 Infuse Medical. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface IMArcMediaFooterThumbPeekView : UIView
-(id)initWithFrame:(CGRect)frame Image:(UIImage*)image Label:(NSString*)label OverPoint:(CGPoint)point;
@end
