//
//  IMArcMediaViewer.m
//  ArcMediaViewer
//
//  Created by Infuse Medical on 2/14/14.
//  Copyright (c) 2014 Infuse Medical. All rights reserved.
//

#import "IMArcMediaViewer.h"
#import "IMArcPDFViewController.h"
#import "IMArcVideoViewController.h"
#import "IMArcWebViewController.h"

@interface IMArcMediaViewer (){
    CGRect _startingFrame;
}

@end

@implementation IMArcMediaViewer

-(id)initWithFrame:(CGRect)frame Delegate:(id<IMArcMediaViewerDelegate>)delegate {
    self = [super init];
    if(self){
        _allowOpenInMenuOption = YES;
        _startingFrame = frame;
        [self.view setFrame:frame];
        CGRect childFrame = frame;
        childFrame.origin = CGPointMake(0, 0);
        _mediaViewers = [NSMutableArray new];
        _overRiddenMediaViewers = [NSMutableArray new];
        self.formEnabled = YES;
        [_mediaViewers addObject:[[IMArcPDFViewController alloc] initWithFrame:childFrame Delegate:self]];
        [_mediaViewers addObject:[[IMArcVideoViewController alloc] initWithFrame:childFrame Delegate:self]];
        [_mediaViewers addObject:[[IMArcWebViewController alloc] initWithFrame:childFrame Delegate:self]];
        _delegate = delegate;
        
    }
    return self;
}

- (id)initWithRect:(CGRect)rect file:(id)fileToOpen delegate:(id<IMArcMediaViewerDelegate>)delegate
{
    NSString * docTitle = @"";
    _startingFrame = rect;
    if ([fileToOpen isKindOfClass:[NSClassFromString(@"DCPFile") class]]) {
        /* Solution for performing an unknown selector without complile warnings
         * as described in the below stack over flow post
         * http://stackoverflow.com/questions/7017281/performselector-may-cause-a-leak-because-its-selector-is-unknown
         */
        if([fileToOpen respondsToSelector:@selector(name)]){
            docTitle = [fileToOpen name];
        }
        SEL selector = NSSelectorFromString(@"isLink");
        IMP imp = [fileToOpen methodForSelector:selector];
        BOOL (*func)(id, SEL) = (void *)imp;
        BOOL islink = func(fileToOpen, selector);
        if (islink) {
            NSURL *url = [NSURL URLWithString:[fileToOpen performSelector:@selector(URL) withObject:nil]];
            [[UIApplication sharedApplication] openURL:url];
            return nil;
        }
    }
    self = [self initWithFrame:rect Delegate:delegate];
    if (self) {
        NSString *path = [fileToOpen performSelector:@selector(filePath) withObject:nil];
        [self performSelector:@selector(loadDocument:) withObject:path afterDelay:0.10f];
        [self performSelector:@selector(setDocumentTitle:) withObject:docTitle afterDelay:0.11f];
        [self performSelector:@selector(showTitleBar) withObject:nil afterDelay:0.11f];
    }
    return self;
}

- (id)initWithRect:(CGRect)rect file:(id)fileToOpen delegate:(id<IMArcMediaViewerDelegate>)delegate overRiddenMediaViewerClass:(Class)overRiddenMediaViewer
{
    self = [self initWithRect:rect file:fileToOpen delegate:delegate];
    if(self){
        if([overRiddenMediaViewer isSubclassOfClass:[IMArcMediaViewerChild class]]){
            CGRect childFrame = rect;
            childFrame.origin = CGPointMake(0, 0);
            [_overRiddenMediaViewers addObject:[[overRiddenMediaViewer alloc] initWithFrame:childFrame Delegate:self]];
        }
    }
    return self;
}

-(void)viewDidDisappear:(BOOL)animated{
    for (IMArcMediaViewerChild * child in _overRiddenMediaViewers) {
        [child stop];
        [child removeFromParentViewController];
        [child.view removeFromSuperview];
    }
    for (IMArcMediaViewerChild * child in _mediaViewers) {
        [child stop];
        [child removeFromParentViewController];
        [child.view removeFromSuperview];
    }
    [_overRiddenMediaViewers removeAllObjects];
    [_mediaViewers removeAllObjects];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)addOverRiddenMediaViewer:(IMArcMediaViewerChild*)overRiddenMediaViewer{
    [_overRiddenMediaViewers addObject:overRiddenMediaViewer];
}

- (void)addOverRiddenMediaViewerClass:(Class)overRiddenMediaViewer{
    if([overRiddenMediaViewer isSubclassOfClass:[IMArcMediaViewerChild class]]){
        CGRect childFrame = _startingFrame;
        childFrame.origin = CGPointMake(0, 0);
        [_overRiddenMediaViewers addObject:[[overRiddenMediaViewer alloc] initWithFrame:childFrame Delegate:self]];
    }
}

- (void)setSetPDFUploadInfo:(SetPDFUploadInfoBlock)setPDFUploadInfo{
    for (IMArcMediaViewerChild * child in _overRiddenMediaViewers) {
        [child setSetPDFUploadInfo:setPDFUploadInfo];
    }
    for (IMArcMediaViewerChild * child in _mediaViewers) {
        [child setSetPDFUploadInfo:setPDFUploadInfo];
    }
}


- (void)setMinZoomScale:(float)minZoomScale{
    for (IMArcMediaViewerChild * child in _overRiddenMediaViewers) {
        [child setMinZoomScale:minZoomScale];
    }
    for (IMArcMediaViewerChild * child in _mediaViewers) {
        [child setMinZoomScale:minZoomScale];
    }
}

- (void)setDefaultValuesDict:(NSDictionary*)defaultValues{
    for (IMArcMediaViewerChild * child in _overRiddenMediaViewers) {
        [child setDefaultValues:defaultValues];
    }
    for (IMArcMediaViewerChild * child in _mediaViewers) {
        [child setDefaultValues:defaultValues];
    }
}

- (void)setDisabledFields:(NSArray*)disabledFields{
    for (IMArcMediaViewerChild * child in _overRiddenMediaViewers) {
        [child setDisabledFields:disabledFields];
    }
    for (IMArcMediaViewerChild * child in _mediaViewers) {
        [child setDisabledFields:disabledFields];
    }
}

-(void)loadDocument:(NSString*)filePath {
    if (filePath && filePath.length > 0) {
        _filePath = filePath;
        if(_currentViewer!=nil){
            [_currentViewer.view removeFromSuperview];
            [_currentViewer removeFromParentViewController];
            _currentViewer = nil;
        }
        for (IMArcMediaViewerChild * mediaViewer in _overRiddenMediaViewers) {
            if([mediaViewer respondsToExtension:[filePath pathExtension]]){
                _currentViewer  = mediaViewer;
                break;
            }
        }
        if (_currentViewer==nil){
            for (IMArcMediaViewerChild * mediaViewer in _mediaViewers) {
                if([mediaViewer respondsToExtension:[filePath pathExtension]]){
                    _currentViewer  = mediaViewer;
                    break;
                }
            }
        }
        
        if ([_currentViewer respondsToSelector:@selector(formEnabled)]) {
            [(IMArcPDFViewController *)_currentViewer setFormEnabled:self.formEnabled];
        }
        
        if(_currentViewer!=nil){
            if (self.customColor) {
                [_currentViewer setCustomColor:self.customColor];
            }
            if (self.documentTitle) {
                [_currentViewer.headerView setDocumentTitle:self.documentTitle];
                [_currentViewer setDocumentTitle:self.documentTitle];
            }
            [_currentViewer loadFileAtPath:_filePath];
            [self addChildViewController:_currentViewer];
            [self.view addSubview:_currentViewer.view];
            [self performSelector:@selector(hideTitleBar) withObject:self afterDelay:3];
        }
    }
}

- (void)loadPDFFormWithName:(NSString*)formName{
    
    IMArcPDFViewController* pdfViewer = nil;
    for (IMArcMediaViewerChild * mediaViewer in _overRiddenMediaViewers) {
        if([mediaViewer isKindOfClass:[IMArcPDFViewController class]]){
            pdfViewer = (IMArcPDFViewController * )mediaViewer;
            [pdfViewer loadPDFFormWithName:formName];
            return;
        }
    }
    for (IMArcMediaViewerChild * mediaViewer in _mediaViewers) {
        if([mediaViewer isKindOfClass:[IMArcPDFViewController class]]){
            pdfViewer = (IMArcPDFViewController * )mediaViewer;
            [pdfViewer loadPDFFormWithName:formName];
            return;
        }
    }
}

-(void)hideTitleBar{
    [_currentViewer hideTitleBar];
}
-(void)showTitleBar{
    [_currentViewer showTitleBar];
}
-(void)setDocumentTitle:(NSString*)documentTitle{
    _documentTitle = documentTitle;
    if(_currentViewer!=nil){
        [[_currentViewer headerView] setDocumentTitle:documentTitle];
        [_currentViewer setDocumentTitle:documentTitle];
    }
}

-(void)setCcRecipients:(NSString *)ccRecipients{
    _ccRecipients = ccRecipients;
    for (IMArcMediaViewerChild * viewer in _overRiddenMediaViewers) {
        [viewer setCcRecipients:_ccRecipients];
    }
    for (IMArcMediaViewerChild * viewer in _mediaViewers) {
        [viewer setCcRecipients:_ccRecipients];
    }
}
-(void)setBCCRecipients:(NSString *)bCCRecipients{
    _bCCRecipients = bCCRecipients;
    for (IMArcMediaViewerChild * viewer in _overRiddenMediaViewers) {
        [viewer setBCCRecipients:_bCCRecipients];
    }
    for (IMArcMediaViewerChild * viewer in _mediaViewers) {
        [viewer setBCCRecipients:_bCCRecipients];
    }
}
-(void)setEmailToRecipients:(NSString *)emailToRecipients{
    _emailToRecipients = emailToRecipients;
    for (IMArcMediaViewerChild * viewer in _overRiddenMediaViewers) {
        [viewer setEmailToRecipients:_emailToRecipients];
    }
    for (IMArcMediaViewerChild * viewer in _mediaViewers) {
        [viewer setEmailToRecipients:_emailToRecipients];
    }
}
-(void)setEmailSubject:(NSString *)emailSubject{
    _emailSubject = emailSubject;
    for (IMArcMediaViewerChild * viewer in _overRiddenMediaViewers) {
        [viewer setEmailSubject:_emailSubject];
    }
    for (IMArcMediaViewerChild * viewer in _mediaViewers) {
        [viewer setEmailSubject:_emailSubject];
    }
}

-(void)setEmailBody:(NSString *)emailBody{
    _emailBody = emailBody;
    for (IMArcMediaViewerChild * viewer in _overRiddenMediaViewers) {
        [viewer setEmailBody:_emailBody];
    }
    for (IMArcMediaViewerChild * viewer in _mediaViewers) {
        [viewer setEmailBody:_emailBody];
    }
}

-(void)setUploadURL:(NSURL *)uploadURL{
    _uploadURL = uploadURL;
    for (IMArcMediaViewerChild * viewer in _overRiddenMediaViewers) {
        [viewer setUploadURL:_uploadURL];
    }
    for (IMArcMediaViewerChild * viewer in _mediaViewers) {
        [viewer setUploadURL:_uploadURL];
    }
}
-(void)setUploadConfiguration:(NSString *)uploadConfiguration{
    _uploadConfiguration = uploadConfiguration;
    for (IMArcMediaViewerChild * viewer in _overRiddenMediaViewers) {
        [viewer setUploadConfiguration:_uploadConfiguration];
    }
    for (IMArcMediaViewerChild * viewer in _mediaViewers) {
        [viewer setUploadConfiguration:_uploadConfiguration];
    }
}
-(void)setRequireAllFields:(BOOL)requireAllFields{
    _requireAllFields = requireAllFields;
    for (IMArcMediaViewerChild * viewer in _overRiddenMediaViewers) {
        [viewer setRequireAllFields:_requireAllFields];
    }
    for (IMArcMediaViewerChild * viewer in _mediaViewers) {
        [viewer setRequireAllFields:_requireAllFields];
    }
}
-(void)setBlockEmailOption:(BOOL)blockEmailOption{
    _blockEmailOption = blockEmailOption;
    for (IMArcMediaViewerChild * viewer in _overRiddenMediaViewers) {
        [viewer setBlockEmailOption:_blockEmailOption];
    }
    for (IMArcMediaViewerChild * viewer in _mediaViewers) {
        [viewer setBlockEmailOption:_blockEmailOption];
    }
}
-(void)setBlockMenu:(BOOL)blockMenu{
    _blockMenu = blockMenu;
    for (IMArcMediaViewerChild * viewer in _overRiddenMediaViewers) {
        [viewer setBlockMenu:blockMenu];
    }
    for (IMArcMediaViewerChild * viewer in _mediaViewers) {
        [viewer setBlockMenu:blockMenu];
    }
}
- (void)setNonFormsBlockMenu:(BOOL)nonFormsBlockMenu
{
    _nonFormsBlockMenu = nonFormsBlockMenu;
    for (IMArcMediaViewerChild * viewer in _overRiddenMediaViewers) {
        [viewer setNonFormsBlockMenu:nonFormsBlockMenu];
    }
    for (IMArcMediaViewerChild * viewer in _mediaViewers) {
        [viewer setNonFormsBlockMenu:nonFormsBlockMenu];
    }
}
- (void)setAllowOpenInMenuOption:(BOOL)allowOpenInMenuOption
{
    _allowOpenInMenuOption = allowOpenInMenuOption;
    for (IMArcMediaViewerChild * viewer in _overRiddenMediaViewers) {
        [viewer setAllowOpenInMenuOption:allowOpenInMenuOption];
    }
    for (IMArcMediaViewerChild * viewer in _mediaViewers) {
        [viewer setAllowOpenInMenuOption:allowOpenInMenuOption];
    }
}

- (void)setEmailScale:(CGFloat)emailScale
{
    _emailScale = emailScale;
    for (IMArcMediaViewerChild * viewer in _overRiddenMediaViewers) {
        [viewer setEmailScale:_emailScale];
    }
    for (IMArcMediaViewerChild * viewer in _mediaViewers) {
        [viewer setEmailScale:_emailScale];
    }
}

- (void)setCustomParameters:(NSDictionary *)customParameters
{
    _customParameters = customParameters;
    for (IMArcMediaViewerChild * viewer in _overRiddenMediaViewers) {
        [viewer setCustomParameters:_customParameters];
    }
    for (IMArcMediaViewerChild * viewer in _mediaViewers) {
        [viewer setCustomParameters:_customParameters];
    }
}

-(void)didRotateFromInterfaceOrientation:(UIInterfaceOrientation)fromInterfaceOrientation{
    UIInterfaceOrientation orientation = [[UIApplication sharedApplication] statusBarOrientation];
    CGRect frame = self.view.frame;
    frame.origin=CGPointMake(0, 0);
    if(UIInterfaceOrientationIsLandscape(orientation)){
        if(frame.size.height>MIN(_startingFrame.size.width,_startingFrame.size.height)){
            frame.size = CGSizeMake(frame.size.height, frame.size.width);
        }
    }else{
        if(frame.size.width>MIN(_startingFrame.size.width,_startingFrame.size.height)){
            frame.size = CGSizeMake(frame.size.height, frame.size.width);
        }
    }
    [_currentViewer setFrame:frame];
    [_currentViewer.headerView hideMenu];
}

-(BOOL)prefersStatusBarHidden{
    return YES;
}

+(NSString*)documentsDirectory{
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsDirectory = [paths objectAtIndex:0];

    return documentsDirectory;
}


static TempFolderLocationBlock temporaryStorageLocation = nil;
+ (void)setTemporaryStorageLocationBlock:(TempFolderLocationBlock)tempFolderLocationBlock;{
    temporaryStorageLocation  =tempFolderLocationBlock;
}
+ (NSString*)getTemporaryStorageLocationWithPDFFileName:(NSString *)fileName{
    if(temporaryStorageLocation!=nil){
        return temporaryStorageLocation([IMArcMediaViewer documentsDirectory],fileName);
    }
    return [NSString stringWithFormat:@"%@/PDFSavedImages/%@",[IMArcMediaViewer documentsDirectory],fileName];
}

static SaveFolderLocationBlock savedFormDataLocation = nil;
+ (void)setSavedFormDataLocationBlock:(SaveFolderLocationBlock)saveFolderLocationBlock;{
    savedFormDataLocation = saveFolderLocationBlock;
}
+ (NSString*)getSavedFormDataLocationWithPDFFileName:(NSString *)fileName savedFormName:(NSString *)savedFormName{
    if(savedFormDataLocation!=nil){
        return savedFormDataLocation([IMArcMediaViewer documentsDirectory],fileName,savedFormName);
    }
    return [NSString stringWithFormat:@"%@/PDFSavedImages/%@/%@",[IMArcMediaViewer documentsDirectory],fileName,savedFormName];
}



#pragma mark - MediaViewerChildDelegate method

-(void) documentIsForm
{
    
}

-(void) emailPDFForm
{
    
}


#pragma mark - IMArchMediaViewerChildDelegate Methods
-(void)closeMediaViewer{
    if(_delegate!=nil){
        [_delegate mediaViewerDidClose:self];
    }
}




@end
