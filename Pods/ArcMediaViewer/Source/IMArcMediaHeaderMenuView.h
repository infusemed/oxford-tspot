//
//  IMArcMediaHeaderMenuView.h
//  iPhoneTest
//
//  Created by Infuse Medical on 5/12/14.
//  Copyright (c) 2014 Infuse Medical. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol IMArcMediaHeaderMenuDelegate <NSObject>
-(void)menuOptionSelectedAtIndex:(int)index;
@end

@interface IMArcMediaHeaderMenuView : UIView<UITableViewDataSource,UITableViewDelegate>{
    UIButton *_buttonExit;
    UITableView * _tableViewOptions;
    NSArray * _menuOptions;
    id<IMArcMediaHeaderMenuDelegate> _delegate;
    CGPoint _point;
}

-(id)initWithFrame:(CGRect)frame
       menuOptions:(NSArray*)menuOptions
         fromPoint:(CGPoint)point
          delegate:(id)delegate;

@end
