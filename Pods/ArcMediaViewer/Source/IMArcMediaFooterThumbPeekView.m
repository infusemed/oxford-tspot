//
//  IMArcMediaFooterThumbPeekView.m
//  iPhoneTest
//
//  Created by Infuse Medical on 5/13/14.
//  Copyright (c) 2014 Infuse Medical. All rights reserved.
//

#import "IMArcMediaFooterThumbPeekView.h"

@implementation IMArcMediaFooterThumbPeekView


-(id)initWithFrame:(CGRect)frame Image:(UIImage*)image Label:(NSString*)label OverPoint:(CGPoint)point{
    self = [super initWithFrame:frame];
    if(self){
        [self setBackgroundColor:[UIColor colorWithWhite:0.0 alpha:0.3]];
        
        UIImageView * imgView  = [[UIImageView alloc] init];
        [imgView setBackgroundColor:[UIColor whiteColor]];
        [imgView setImage:image];
        [self addSubview:imgView];
        
        if(frame.size.width>frame.size.height){
            int width = image.size.width*(point.y-40)/image.size.height;
            int left = MAX(0, point.x-width/2);
            left = MIN(left,self.frame.size.width-width);
            [imgView setFrame:CGRectMake(left,5, width, (point.y-40))];
            
        }else{
            int height = image.size.height*200/image.size.width;
            [imgView setFrame:CGRectMake(frame.size.width-260, point.y-40-height, 200, height)];
        }
        float r = imgView.frame.size.height/200;
        
        UILabel * labelView = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, 80*r, 30*r)];
        [labelView setBackgroundColor:[UIColor colorWithWhite:0.0 alpha:0.4]];
        [labelView setFont:[UIFont systemFontOfSize:10*r]];
        [labelView setTextAlignment:NSTextAlignmentCenter];
        [labelView setText:label];
        [imgView addSubview:labelView];
        [labelView setCenter:CGPointMake(imgView.frame.size.width/2, imgView.frame.size.height/2)];
        
        [imgView.layer setCornerRadius:10];
        [imgView.layer setMasksToBounds:YES];
        [labelView.layer setCornerRadius:5];
        [labelView.layer setMasksToBounds:YES];
    }
    return self;
}

@end
