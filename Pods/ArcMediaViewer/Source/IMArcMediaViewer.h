//
//  IMArcMediaViewer.h
//  ArcMediaViewer
//
//  Created by Infuse Medical on 2/14/14.
//  Copyright (c) 2014 Infuse Medical. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "IMArcMediaViewerChild.h"

@class IMArcMediaViewer;

@protocol IMArcMediaViewerDelegate
-(void)mediaViewerDidClose:(IMArcMediaViewer*)mediaViewer;
@end

typedef NSString* (^TempFolderLocationBlock)(NSString*documentsFolder, NSString*formName);
typedef NSString* (^SaveFolderLocationBlock)(NSString*documentsFolder, NSString*formName, NSString* saveAsName);

@interface IMArcMediaViewer : UIViewController <IMArcMediaViewerChildDelegate>
@property (nonatomic, assign) id <IMArcMediaViewerDelegate> delegate;
@property (nonatomic, strong) NSString * filePath;
@property (nonatomic, strong) NSMutableArray* overRiddenMediaViewers;
@property (nonatomic, strong) NSMutableArray* mediaViewers;
@property (nonatomic, strong) NSURL * uploadURL;
@property (nonatomic, strong) NSString * uploadConfiguration;
@property (nonatomic, assign) BOOL requireAllFields;
/**
 * Blocks the menu from ever being shown for any document
 */
@property (nonatomic, assign) BOOL blockMenu;
/**
 * Only blocks the "Send Email" option from being added to the menu
 */
@property (nonatomic, assign) BOOL blockEmailOption;
/**
 * Only blocks the menu from being displayed on non-form documents
 */
@property (nonatomic, assign) BOOL nonFormsBlockMenu;
/**
 * Allows the 'Open In' to be added to the menu drop down.
 *
 * Default is YES
 */
@property (nonatomic, assign) BOOL allowOpenInMenuOption;
@property (nonatomic, assign) float minZoomScale;
@property (nonatomic, assign) NSString* emailToRecipients;
@property (nonatomic, assign) NSString* emailSubject;
@property (nonatomic, assign) NSString* ccRecipients;
@property (nonatomic, assign) NSString* bCCRecipients;
@property (nonatomic, assign) NSString* emailBody;
@property (nonatomic, strong) IMArcMediaViewerChild * currentViewer;
@property (nonatomic, strong) SetPDFUploadInfoBlock setPDFUploadInfo;
/**
 * BOOL indicating if Form Data should be rendered on PDFs
 *
 * Set prior to calling loadDocument
 * Default Value is set to YES
 */
@property (nonatomic) BOOL formEnabled;
/**
 * UIColor used to customize elements in the MediaViewer
 *
 * Set prior to calling loadDocument
 * Default Value is set to nil
 */
@property (nonatomic, strong) UIColor *customColor;
@property (nonatomic, strong) NSString *documentTitle;
/**
 * Allows the developer to override the default scale at which PDF forms are generated for emailing
 */
@property (nonatomic) CGFloat emailScale;
/**
 * Container that holds key value pairs for URL parameters to be passed into zip files containing an index.html file at the root
 */
@property (nonatomic, strong) NSDictionary *customParameters;


- (id)initWithRect:(CGRect)rect file:(id)fileToOpen delegate:(id<IMArcMediaViewerDelegate>)delegate overRiddenMediaViewerClass:(Class)overRiddenMediaViewer;
// Only works with DCPFile

///---------------------
/// @name Initialization
///---------------------

/**
 Creates and returns a MediaViewer for a DCPFile without knowing or having to import DCPFile into the mediaviewr
 
 @param CGRect identifying size and placement of the MediaViewer
 @param DCPFile object created in the Ether iOS InfuseAPI
 @param `IMArcMediaViewerDelegate` delegate
 
 @return A newly created IMArcMediaViewer `UIViewController`
 */
- (id)initWithRect:(CGRect)rect file:(id)fileToOpen delegate:(id<IMArcMediaViewerDelegate>)delegate;
- (id)initWithFrame:(CGRect)frame Delegate:(id<IMArcMediaViewerDelegate>)delegate;

- (void)addOverRiddenMediaViewer:(IMArcMediaViewerChild*)overRiddenMediaViewer;
- (void)addOverRiddenMediaViewerClass:(Class)overRiddenMediaViewer;
- (void)loadDocument:(NSString *)filePath;
- (void)setDocumentTitle:(NSString *)documentTitle;
- (void)loadPDFFormWithName:(NSString*)formName;
- (void)setDefaultValuesDict:(NSDictionary*)defaultValues;
- (void)setDisabledFields:(NSArray*)disabledFields;

+ (void)setTemporaryStorageLocationBlock:(TempFolderLocationBlock)tempFolderLocationBlock;
+ (NSString*)getTemporaryStorageLocationWithPDFFileName:(NSString*)fileName;

+ (void)setSavedFormDataLocationBlock:(SaveFolderLocationBlock)saveFolderLocationBlock;
+ (NSString*)getSavedFormDataLocationWithPDFFileName:(NSString*)fileName savedFormName:(NSString*)savedFormName;


@end
