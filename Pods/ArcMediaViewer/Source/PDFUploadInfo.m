//
//  PDFUploadInfo.m
//  ArcMediaViewer
//
//  Created by Infuse Medical on 7/10/14.
//  Copyright (c) 2014 Infuse Medical. All rights reserved.
//

#import "PDFUploadInfo.h"

@implementation PDFUploadInfo

-(id)initWithFilePath:(NSString*)filePath{
    self = [super init];
    if(self){
        _file_path = filePath;
        _uploadURL = @"";
        _company = @"";
        _key = @"";
        _folder_name = @"";
        _config_id = 0;
    }
    return self;
}

- (instancetype)initWithCoder:(NSCoder *)coder
{
    self = [super init];
    if (self) {
        _uploadURL = [coder decodeObjectForKey:@"uploadURL"];
        _company = [coder decodeObjectForKey:@"company"];
        _key = [coder decodeObjectForKey:@"key"];
        _config_id = [[coder decodeObjectForKey:@"config_id"] intValue];
        _folder_name = [coder decodeObjectForKey:@"folder_name"];
        _file_name = [coder decodeObjectForKey:@"file_name"];
        _file_path = [coder decodeObjectForKey:@"file_path"];
        _showCompletionMessage = [[coder decodeObjectForKey:@"showCompletionMessage"] boolValue];
        _showFailureMessage = [[coder decodeObjectForKey:@"showFailureMessage"] boolValue];
    }
    return self;
}

- (void)encodeWithCoder:(NSCoder *)coder
{
    [coder encodeObject:_uploadURL forKey:@"uploadURL"];
    [coder encodeObject:_company forKey:@"company"];
    [coder encodeObject:_key forKey:@"key"];
    [coder encodeObject:[NSNumber numberWithInt:_config_id] forKey:@"config_id"];
    [coder encodeObject:_folder_name forKey:@"folder_name"];
    [coder encodeObject:_file_name forKey:@"file_name"];
    [coder encodeObject:_file_path forKey:@"file_path"];
    [coder encodeObject:[NSNumber numberWithBool:_showCompletionMessage] forKey:@"showCompletionMessage"];
    [coder encodeObject:[NSNumber numberWithBool:_showFailureMessage] forKey:@"showFailureMessage"];
    
}

@end
