//
//  CULSignatureView.m
//  Bard
//
//  Created by Brook Harker on 7/19/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import "CULSignatureView.h"

#define MEMORY_CHUNK_SIZE 10

@implementation CULSignatureView
@synthesize enabled = _enabled;
@synthesize strokeWidth = _strokeWidth;
@synthesize strokeColor = _strokeColor;

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        _chunks = 1;
        _paths = malloc(MEMORY_CHUNK_SIZE * sizeof(CGMutablePathRef) * _chunks);
        _enabled = YES;
        _strokeWidth = 5;
        _strokeColor = [UIColor blackColor];
        self.backgroundColor = [UIColor whiteColor];
        imageView = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, frame.size.width, frame.size.height)];
        [self addSubview:imageView];
        
        self.layer.masksToBounds = NO;
        self.layer.cornerRadius = 2; // if you like rounded corners
        self.layer.shadowOffset = CGSizeMake(-3, 3);
        self.layer.shadowRadius = 5;
        self.layer.shadowOpacity = 0.5;
        
        UIBezierPath *shadowPath = [UIBezierPath bezierPathWithRect:self.bounds];
        self.layer.shadowPath = shadowPath.CGPath;
        
    }
    return self;
}


- (void)dealloc
{
	[self clear];
}


- (void) clear 
{
    [imageView removeFromSuperview];
    
    imageView = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, self.frame.size.width, self.frame.size.height)];
    [self addSubview:imageView];
}
CGPoint culMidPoint(CGPoint p1, CGPoint p2)
{
    
    return CGPointMake((p1.x + p2.x) * 0.5, (p1.y + p2.y) * 0.5);
    
}

- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event 
{
    
    if (!_enabled)
    {
        return;
    }
    
    UITouch *touch = [touches anyObject];
    
    previousPoint1 = [touch previousLocationInView:self];
    previousPoint2 = [touch previousLocationInView:self];
    currentPoint = [touch locationInView:self];
    
    
    
}

- (void)touchesMoved:(NSSet *)touches withEvent:(UIEvent *)event
{
    
    if (!_enabled)
    {
        return;
    }
    UITouch *touch = [touches anyObject];
    
    previousPoint2 = previousPoint1;
    previousPoint1 = currentPoint;
    currentPoint = [touch locationInView:self];
    
    
    // calculate mid point
    CGPoint mid1 = culMidPoint(previousPoint1, previousPoint2); 
    CGPoint mid2 = culMidPoint(currentPoint, previousPoint1);
    
    UIGraphicsBeginImageContext(imageView.frame.size);
    CGContextRef context = UIGraphicsGetCurrentContext();
    [imageView.image drawInRect:CGRectIntegral(CGRectMake(0, 0, imageView.frame.size.width, imageView.frame.size.height))];
    
    CGContextMoveToPoint(context, mid1.x, mid1.y);
    // Use QuadCurve is the key
    CGContextAddQuadCurveToPoint(context, previousPoint1.x, previousPoint1.y, mid2.x, mid2.y); 
    
    CGContextSetLineCap(context, kCGLineCapRound);
    CGContextSetLineWidth(context, _strokeWidth);
    CGContextSetStrokeColorWithColor(context, [_strokeColor CGColor]);
    CGContextStrokePath(context);
    
    imageView.image = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    
	
}
- (void)touchesEnded:(NSSet *)touches withEvent:(UIEvent *)event {
    if (!_enabled)
    {
        return;
    }
    UITouch *touch = [touches anyObject];
    
    previousPoint2 = previousPoint1;
    previousPoint1 = currentPoint;
    currentPoint = [touch locationInView:self];
    
    
    // calculate mid point
    CGPoint mid1 = culMidPoint(previousPoint1, previousPoint2); 
    CGPoint mid2 = culMidPoint(currentPoint, previousPoint1);
    
    UIGraphicsBeginImageContext(imageView.frame.size);
    CGContextRef context = UIGraphicsGetCurrentContext();
    [imageView.image drawInRect:CGRectIntegral(CGRectMake(0, 0, imageView.frame.size.width, imageView.frame.size.height))];
    
    CGContextMoveToPoint(context, mid1.x, mid1.y);
    // Use QuadCurve is the key
    CGContextAddQuadCurveToPoint(context, previousPoint1.x, previousPoint1.y, mid2.x, mid2.y); 
    
    CGContextSetLineCap(context, kCGLineCapRound);
    CGContextSetLineWidth(context, _strokeWidth);
    CGContextSetStrokeColorWithColor(context, [_strokeColor CGColor]);
    //CGContextSetRGBStrokeColor(context, 1.0, 0.0, 0.0, 1.0);
    CGContextStrokePath(context);
    
    imageView.image = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
}

- (UIImage*) imageFromSignature
{
	return imageView.image;
}

@end
