//
//  IMArcWebViewController.m
//  ArcMediaViewer
//
//  Created by Infuse Medical on 2/14/14.
//  Copyright (c) 2014 Infuse Medical. All rights reserved.
//

#import "IMArcWebViewController.h"

@interface IMArcWebViewController () {
    NSString *_filePath;
    NSURL *_url;
    NSURLRequest *_request;
    NSMutableDictionary *_presetJSONDictionary;
    NSString *_presetJSONFilePath;
}

@end

@implementation IMArcWebViewController

- (id)initWithFrame:(CGRect)frame Delegate:(id<IMArcMediaViewerChildDelegate>)delegate
{
    self = [super initWithFrame:frame Delegate:delegate];
    if(self){
        _webView = [[UIWebView alloc] initWithFrame:CGRectMake(0, 0, frame.size.width, frame.size.height)];
        [_webView setDelegate:self];
        [self.view addSubview:_webView];
        
        [self.view bringSubviewToFront:[super headerView]];
        [self.view bringSubviewToFront:[super buttonBack]];
        
        
        UITapGestureRecognizer *tapRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(tapGestureRecognized:)];
        [tapRecognizer setDelegate:self];
        [_webView addGestureRecognizer:tapRecognizer];
    }
    return self;
}

- (BOOL)respondsToExtension:(NSString*)extension
{
    return YES;
}

- (void)loadFileAtPath:(NSString *)filePath
{
    _filePath = filePath;
    if ([[_filePath pathExtension] isEqualToString:@"zip"]) {
        NSString *presetID;
        if ([[NSFileManager defaultManager] fileExistsAtPath:[[_filePath stringByDeletingPathExtension] stringByAppendingPathComponent:@"presets.json"]]) {
            _presetJSONFilePath = [[_filePath stringByDeletingPathExtension] stringByAppendingPathComponent:@"presets.json"];
            NSData *data = [NSData dataWithContentsOfFile:_presetJSONFilePath];
            NSError *error;
            NSDictionary *presetsJson = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingAllowFragments error:&error];
            if (error) {
                NSLog(@"%@", [error localizedDescription]);
            } else {
                _presetJSONDictionary = [NSMutableDictionary dictionaryWithDictionary:presetsJson];
            }
            presetID = _presetJSONDictionary[@"preset_id"];
        }
        _filePath = [NSString stringWithFormat:@"%@/index.html",[_filePath stringByDeletingPathExtension]];
        
        if(![[NSFileManager defaultManager] fileExistsAtPath:_filePath]){
            NSError * error;
            NSURL* folderURL =[[NSURL alloc] initFileURLWithPath:[NSString stringWithFormat:@"%@/",[_filePath stringByDeletingLastPathComponent]]];
            NSArray *dirContents = [[NSFileManager defaultManager]
                                    contentsOfDirectoryAtURL:folderURL
                                    includingPropertiesForKeys:nil
                                    options:NSDirectoryEnumerationSkipsHiddenFiles | NSDirectoryEnumerationSkipsSubdirectoryDescendants
                                    error:&error];
            for (NSURL * url in dirContents) {
                NSString* filePath = [NSString stringWithFormat:@"%@/index.html",[url path]];
                if([[NSFileManager defaultManager] fileExistsAtPath:filePath]){
                    _filePath = filePath;
                    break;
                }
            }
        }
        
        
        [_webView removeGestureRecognizer:_webView.gestureRecognizers[0]];
        
        if (self.buttonBack) {
            [self.buttonBack removeFromSuperview];
            self.buttonBack = nil;
        }
        
        UIImage *closeButtonImage = [UIImage imageNamed:@"Close-Button"];
        self.buttonBack = [UIButton buttonWithType:UIButtonTypeCustom];
        [self.buttonBack setImage:closeButtonImage forState:UIControlStateNormal];
        [self.buttonBack addTarget:self action:@selector(buttonBackPressed:) forControlEvents:UIControlEventTouchUpInside];
        [self.view addSubview:self.buttonBack];
        
        UIImage *homeButtonImage = [UIImage imageNamed:@"Home-Button"];
        UIButton *homeButton = [UIButton buttonWithType:UIButtonTypeCustom];
        [homeButton setImage:homeButtonImage forState:UIControlStateNormal];
        [homeButton addTarget:self action:@selector(homeButtonPressed:) forControlEvents:UIControlEventTouchUpInside];
        [self.view addSubview:homeButton];
        
//------------------------------Call method to setup constraints-------------------------------------------------------
        [self setupConstraintsForButtons:self.buttonBack homeButton:homeButton];
    
        NSString *additionalParameters = @"";
        for (NSString *key in self.customParameters) {
            NSString *value = self.customParameters[key];
            additionalParameters = [additionalParameters stringByAppendingFormat:@"&%@=%@", key, value];
        }
        
        if (self.customColor) {
            NSString *hexString = [self hexStringForColor:self.customColor];
            NSString *queryString = [NSString stringWithFormat:@"?title=%@&color1=%@&presets=1", self.documentTitle, hexString];
            if (presetID && presetID.length > 0) {
                queryString = [queryString stringByAppendingFormat:@"&init_id=%@", presetID];
            }
            
            if (additionalParameters.length > 0) {
                queryString = [queryString stringByAppendingString:additionalParameters];
            }
            
            queryString = [queryString stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
            NSURL *fileURL = [NSURL fileURLWithPath:_filePath];
            NSString *absoluteFileURL = [fileURL absoluteString];
            NSString *absoluteFileURLWithQueryString = [absoluteFileURL stringByAppendingString:queryString];
            _url = [NSURL URLWithString:absoluteFileURLWithQueryString];
        } else {
            NSString *queryString = [NSString stringWithFormat:@"?title=%@&presets=1", self.documentTitle];
            queryString = [queryString stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
            if (presetID && presetID.length > 0) {
                queryString = [queryString stringByAppendingFormat:@"&init_id=%@", presetID];
            }
            
            if (additionalParameters.length > 0) {
                queryString = [queryString stringByAppendingString:additionalParameters];
            }
            
            NSURL *fileURL = [NSURL fileURLWithPath:_filePath];
            NSString *absoluteFileURL = [fileURL absoluteString];
            NSString *absoluteFileURLWithQueryString = [absoluteFileURL stringByAppendingString:queryString];
            _url = [NSURL URLWithString:absoluteFileURLWithQueryString];
        }
    }
    
    _webView.exclusiveTouch = FALSE;
    _webView.scalesPageToFit = YES;
    
    if (!_url) {
        _url = [NSURL fileURLWithPath:_filePath];
    }
    
    _request = [NSURLRequest requestWithURL:_url];
    
    [_webView loadRequest:_request];
    
    if (![self isAudioFile]) {
        _spinningWheel = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleWhiteLarge];
        _spinningWheel.frame = CGRectMake(0, 0, 35, 35);
        _spinningWheel.center = CGPointMake(_webView.frame.size.width/2, _webView.frame.size.height/2);
        [self.view addSubview:_spinningWheel];
        [_spinningWheel startAnimating];
    }
}

- (BOOL)isAudioFile
{
    NSString *extension = [_filePath pathExtension];
    if ([extension isEqualToString:@"mp3"] ||
        [extension isEqualToString:@"m4p"] ||
        [extension isEqualToString:@"m4a"] ||
        [extension isEqualToString:@"m4b"] ||
        [extension isEqualToString:@"aac"]) {
        return YES;
    }
    return NO;
}

#pragma mark Setup Button Constraints

-(void)setupConstraintsForButtons:(UIButton *)backButton homeButton:(UIButton *) homeButton{
   
    
    NSDictionary *viewsDictionary = @{@"buttonBack":backButton, @"buttonHome":homeButton};
    
//-----------------------------------Back button contraints setup -------------------------------------------------------------------------------
    backButton.translatesAutoresizingMaskIntoConstraints = NO;
    NSArray *backButtonHeight = [NSLayoutConstraint constraintsWithVisualFormat:@"V:[buttonBack(30)]"
                                                                        options:0
                                                                        metrics:nil
                                                                        views:viewsDictionary];
    
    NSArray *backButtonWidth = [NSLayoutConstraint constraintsWithVisualFormat:@"H:[buttonBack(30)]"
                                                                       options:0
                                                                       metrics:nil
                                                                       views:viewsDictionary];
    [backButton addConstraints:backButtonHeight];
    [backButton addConstraints:backButtonWidth];
    
//-----------------------------------Home button contraints setup -------------------------------------------------------------------------------
    homeButton.translatesAutoresizingMaskIntoConstraints = NO;
    NSArray *homeButtonHeight = [NSLayoutConstraint constraintsWithVisualFormat:@"V:[buttonHome(30)]"
                                                                        options:0
                                                                        metrics:nil
                                                                        views:viewsDictionary];
    
    NSArray *homeButtonWidth = [NSLayoutConstraint constraintsWithVisualFormat:@"H:[buttonHome(30)]"
                                                                       options:0
                                                                       metrics:nil
                                                                       views:viewsDictionary];
    
    [homeButton addConstraints:homeButtonHeight];
    [homeButton addConstraints:homeButtonWidth];

//-----------------------------------Button contraints within superview setup -------------------------------------------------------------------
    
    NSArray *constraint_v = [NSLayoutConstraint constraintsWithVisualFormat:@"H:[buttonHome]-10-[buttonBack]-10-|"
                                                                    options:0
                                                                    metrics:nil
                                                                    views:viewsDictionary];
    
    NSArray *closeBtnContraint_h = [NSLayoutConstraint constraintsWithVisualFormat:@"V:|-10-[buttonBack]"
                                                                           options:0
                                                                           metrics:nil
                                                                           views:viewsDictionary];
    
    NSArray *homeBtnContraint_h = [NSLayoutConstraint constraintsWithVisualFormat:@"V:|-10-[buttonHome]"
                                                                          options:0
                                                                          metrics:nil
                                                                          views:viewsDictionary];
    
    [self.view addConstraints:constraint_v];
    [self.view addConstraints:closeBtnContraint_h];
    [self.view addConstraints:homeBtnContraint_h];

}


- (void)setFrame:(CGRect)frame
{
    [super setFrame:frame];
    [_webView setFrame:CGRectMake(0, 0, frame.size.width, frame.size.height)];
}

- (void)stop
{
    
}

- (void)saveJSONPresets
{
    NSError *error = nil;
    NSOutputStream *outputStream = [NSOutputStream outputStreamToFileAtPath:_presetJSONFilePath append:NO];
    [outputStream open];
    
    [NSJSONSerialization writeJSONObject:_presetJSONDictionary
                                toStream:outputStream
                                 options:NSJSONWritingPrettyPrinted
                                   error:&error];
    [outputStream close];
}

- (NSString *)hexStringForColor:(UIColor *)color {
    const CGFloat *components = CGColorGetComponents(color.CGColor);
    CGFloat r = components[0];
    CGFloat g = components[1];
    CGFloat b = components[2];
    NSString *hexString=[NSString stringWithFormat:@"%02X%02X%02X", (int)(r * 255), (int)(g * 255), (int)(b * 255)];
    return hexString;
}

- (void)savePreset:(NSDictionary *)presetDictionary
{
    NSMutableArray *presets = [NSMutableArray arrayWithArray:_presetJSONDictionary[@"presets"]];
    NSMutableDictionary *newPreset = [NSMutableDictionary dictionaryWithCapacity:presetDictionary.count];
    for (NSString *key in presetDictionary.allKeys) {
        if ([key isEqualToString:@"type"]) {
            continue;
        } else if ([key isEqualToString:@"order"]) {
            NSArray *slides = [presetDictionary[key] componentsSeparatedByString:@","];
            [newPreset setObject:slides forKey:@"slides"];
        } else if ([key isEqualToString:@"id"]) {
            NSNumber *idAsNumber = [NSNumber numberWithInteger:[presetDictionary[key] integerValue]];
            [newPreset setObject:idAsNumber forKey:key];
        } else {
            [newPreset setObject:presetDictionary[key] forKey:key];
        }
    }
    
    NSInteger index = 0;
    BOOL found = NO;
    for (NSDictionary *dict in presets) {
        if ([dict[@"id"] integerValue] == [newPreset[@"id"] integerValue]) {
            found = YES;
            break;
        }
        index++;
    }
    
    if (found) {
        [presets replaceObjectAtIndex:index withObject:newPreset];
    } else {
        [presets addObject:newPreset];
    }
    
    [_presetJSONDictionary setObject:presets forKey:@"presets"];
    [self saveJSONPresets];
}

- (void)savePresetID:(NSString *)presetID
{
    [_presetJSONDictionary setObject:presetID forKey:@"preset_id"];
    [self saveJSONPresets];
}

- (void)deletePreset:(NSDictionary *)presetDictionary
{
    NSMutableArray *presets = [NSMutableArray arrayWithArray:_presetJSONDictionary[@"presets"]];
    NSInteger index = 0;
    BOOL found = NO;
    for (NSDictionary *dict in presets) {
        if ([dict[@"id"] integerValue] == [presetDictionary[@"id"] integerValue]) {
            found = YES;
            break;
        }
        index++;
    }
    
    if (found) {
        [presets removeObjectAtIndex:index];
        [_presetJSONDictionary setObject:presets forKey:@"presets"];
        [self saveJSONPresets];
    }
}

- (NSMutableDictionary *)getParameters:(NSURLRequest *)request
{
    NSURLComponents *urlComponents = [NSURLComponents componentsWithURL:request.URL resolvingAgainstBaseURL:NO];
    NSArray *queryItems;
    NSMutableDictionary *parameters;
    if ([urlComponents respondsToSelector:@selector(queryItems)]) {
        queryItems = urlComponents.queryItems;
        parameters = [NSMutableDictionary dictionaryWithCapacity:queryItems.count];
        for (NSURLQueryItem *queryItme in queryItems) {
            [parameters setObject:queryItme.value forKey:queryItme.name];
        }
    } else {
        NSString *queryString = [urlComponents query];
        NSArray *unprocessedQueryItems = [queryString componentsSeparatedByString:@"&"];
        parameters = [NSMutableDictionary dictionaryWithCapacity:unprocessedQueryItems.count];
        for (NSString *queryItemString in unprocessedQueryItems) {
            NSArray *queryArray = [queryItemString componentsSeparatedByString:@"="];
            if (queryArray.count == 2) {
                NSString *key = queryArray[0];
                NSString *value = queryArray[1];
                [parameters setObject:value forKey:key];
            }
        }
    }
    return parameters;
}

#pragma mark - button method

- (void)homeButtonPressed:(UIButton *)sender
{
    _request = [NSURLRequest requestWithURL:_url];
    [_webView loadRequest:_request];
}

-(void)buttonBackPressed:(UIButton*)sender{
    [super buttonBackPressed:sender];
    [[NSURLCache sharedURLCache] removeCachedResponseForRequest:_request];
}

#pragma mark - UIWebViewDelegate

- (void)webViewDidFinishLoad:(UIWebView *)webView
{
    [[NSUserDefaults standardUserDefaults] setInteger:0 forKey:@"WebKitCacheModelPreferenceKey"];
    [_spinningWheel removeFromSuperview];
    _spinningWheel = nil;
}

- (void)webViewDidStartLoad:(UIWebView *)webView
{
    [self.view.window makeKeyWindow];
}

- (BOOL)webView:(UIWebView *)webView shouldStartLoadWithRequest:(NSURLRequest *)request navigationType:(UIWebViewNavigationType)navigationType
{
    NSLog(@"Reqest: %@", request.URL.absoluteString);
    if (request) {
        NSRange range = [request.URL.absoluteString rangeOfString:@"http://etherdcp.com/?type="];
        if (range.location != NSNotFound) {

            NSMutableDictionary *parameters = [self getParameters:request];
            if ([parameters[@"type"] isEqualToString:@"save"]) {
                [self savePreset:[NSDictionary dictionaryWithDictionary:parameters]];
            } else if ([parameters[@"type"] isEqualToString:@"delete"]) {
                [self deletePreset:[NSDictionary dictionaryWithDictionary:parameters]];
            }
            
            return NO;
        }
        
        range = [request.URL.absoluteString rangeOfString:@"preset_id"];
        if (range.location != NSNotFound) {
            NSMutableDictionary *parameters = [self getParameters:request];
            NSString *presetID = parameters[@"preset_id"];
            [self savePresetID:presetID];
        }
    }
    return YES;
}

@end
