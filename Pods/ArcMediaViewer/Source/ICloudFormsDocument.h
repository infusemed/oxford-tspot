//
//  ICloudFormsDocument.h
//  TestICloud
//
//  Created by Jeff Spaulding on 3/8/16.
//  Copyright © 2016 Jeff Spaulding. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ICloudFormsImage.h"

@interface ICloudFormsDocument : UIDocument
@property (nonatomic, strong) NSString* fileName;
@property (nonatomic, strong) NSDictionary* formsDictionary;
@property (nonatomic, strong) NSMutableArray<ICloudFormsImage *>* formImages;
-(id)initWithFileName:(NSString*)fileName;
-(void)saveWithCompletionHandler:(void (^)(BOOL success))completion;

-(void)restoreFormDataIfNeeded;
@end
