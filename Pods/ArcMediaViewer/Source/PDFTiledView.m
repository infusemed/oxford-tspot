
#import "PDFTiledView.h"
//#import <QuartzCore/QuartzCore.h>

@implementation PDFTiledView


// Create a new TiledPDFView with the desired frame and scale.
- (id)initWithFrame:(CGRect)frame andScale:(CGFloat)scale{
    if ((self = [super initWithFrame:frame])) {
            tiledLayer = (CATiledLayer *)[self layer];
            // levelsOfDetail and levelsOfDetailBias determine how
            // the layer is rendered at different zoom levels.  This
            // only matters while the view is zooming, since once the 
            // the view is done zooming a new TiledPDFView is created
            // at the correct size and scale.
            tiledLayer.levelsOfDetail = 4;
            tiledLayer.levelsOfDetailBias = 4;

        NSUInteger processorCount = [[NSProcessInfo processInfo] processorCount];
        if (processorCount == 2) 
            tiledLayer.tileSize = CGSizeMake(1024.0f, 1024.0f);
        else
            tiledLayer.tileSize = CGSizeMake(512.0f, 512.0f);
            
            //[tiledLayer setNeedsDisplayInRect:tiledLayer.bounds];
            
        myScale = scale;
        
        self.stopEverything = FALSE;
        
        self.exclusiveTouch = NO;
    }
    return self;
}
@synthesize pdfDcoument;
@synthesize index;
@synthesize stopEverything;

// Set the layer's class to be CATiledLayer.
+ (Class)layerClass {
	return [CATiledLayer class];
}

// Set the CGPDFPageRef for the view.
- (void)setPage:(CGPDFPageRef)newPage
{
    CGPDFPageRelease(self->pdfPage);
    self->pdfPage = CGPDFPageRetain(newPage);
    
}

-(void)drawRect:(CGRect)r
{
    // UIView uses the existence of -drawRect: to determine if it should allow its CALayer
    // to be invalidated, which would then lead to the layer creating a backing store and
    // -drawLayer:inContext: being called.
    // By implementing an empty -drawRect: method, we allow UIKit to continue to implement
    // this logic, while doing our real drawing work inside of -drawLayer:inContext:
}


// Draw the CGPDFPageRef into the layer at the correct scale.
-(void)drawLayer:(CALayer*)layer inContext:(CGContextRef)context
{
    
    
    // First fill the background with white.
    CGContextSetRGBFillColor(context, 1.0,1.0,1.0,1.0);
    CGContextFillRect(context,self.bounds);
    
    CGContextSaveGState(context);
    
    int rotation = CGPDFPageGetRotationAngle(pdfPage);
    if (90 == rotation)
    {
        CGContextTranslateCTM(context, self.bounds.size.width/2, self.bounds.size.height/2);
    
    
        CGContextRotateCTM(context, 3.14/2);
    
    // Flip the context so that the PDF page is rendered
    // right side up.
        CGContextTranslateCTM(context, -self.bounds.size.height/2, self.bounds.size.width/2);
    }
    else if (270 == rotation)
    {
        CGContextTranslateCTM(context, self.bounds.size.width/2, self.bounds.size.height/2);
        CGContextRotateCTM(context, 3.14*1.5);
        CGContextTranslateCTM(context, -self.bounds.size.height/2, self.bounds.size.width/2);
    }
    else{
        CGContextTranslateCTM(context, 0.0, self.bounds.size.height);
    }
    
	CGRect pageRect = CGPDFPageGetBoxRect(pdfPage, kCGPDFCropBox);
    
    //CGContextTranslateCTM(context, -pageRect.origin.x*myScale, pageRect.origin.y*myScale);
    //CGContextTranslateCTM(context, -pageRect.origin.x*myScale, 0);
    CGContextTranslateCTM(context, -pageRect.origin.x*myScale, pageRect.origin.y*myScale);
    
    CGContextScaleCTM(context, 1.0, -1.0);
    // Scale the context so that the PDF page is rendered 
    // at the correct size for the zoom level.
    
    
    //CGContextConcatCTM(context, CGPDFPageGetDrawingTransform(pdfPage, kCGPDFMediaBox, self.bounds, 0, true));
    CGContextScaleCTM(context, myScale,myScale);
    
    CGContextDrawPDFPage(context, pdfPage);
    
    CGContextRestoreGState(context);
    
    
     
}

- (BOOL)pointInside:(CGPoint)point withEvent:(UIEvent *)event
{

    return NO;
}

-(void) stopEverythingNow
{
    self.stopEverything = TRUE;
}

-(void) removeTileLayer
{
	
	//[tiledLayer release];
}

// Clean up.
- (void)dealloc {
        
    CGPDFPageRelease(pdfPage);
}


@end
