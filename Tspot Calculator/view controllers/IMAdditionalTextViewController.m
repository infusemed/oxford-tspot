//
//  IMAdditionalTextViewController.m
//  Tspot Calculator
//
//  Created by Anthony Long on 6/29/14.
//  Copyright (c) 2014 Infuse Medical. All rights reserved.
//

#import "IMAdditionalTextViewController.h"

@interface IMAdditionalTextViewController ()

@property (weak, nonatomic) IBOutlet UIButton *buttonCancel;
@property (weak, nonatomic) IBOutlet UIButton *buttonSubmit;

@end

@implementation IMAdditionalTextViewController

- (IBAction)handleCancelTouch:(UIButton *)button
{
	[self dismissViewControllerAnimated:YES
							 completion:NULL];
}

- (IBAction)handleSubmitTouch:(UIButton *)button
{
	[self.delegate additionalTextViewController:self willDismissWithText:self.textView.text];
	[self dismissViewControllerAnimated:YES
							 completion:NULL];
}

#pragma mark - UIViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
		self.isAdditionalContent = YES;
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	[self.textView becomeFirstResponder];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
