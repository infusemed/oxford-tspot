//
//  IMSaveSessionViewController.h
//  Tspot Calculator
//
//  Created by Anthony Long on 6/23/14.
//  Copyright (c) 2014 Infuse Medical. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "IMSpreadSheet.h"

@class IMSaveSessionViewController;

@protocol IMSaveSessionViewControllerDelegate <NSObject>

- (void) saveSessionViewController:(IMSaveSessionViewController *)controller shouldSaveWithName:(NSString *)fileName;

@end

@interface IMSaveSessionViewController : UIViewController

@property (nonatomic, strong) IMWorkbook *workbook;
@property (nonatomic, unsafe_unretained) id <IMSaveSessionViewControllerDelegate> delegate;

@end