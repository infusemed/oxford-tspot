//
//  IMPDFViewController.m
//  Tspot Calculator
//
//  Created by Anthony Long on 7/27/14.
//  Copyright (c) 2014 Infuse Medical. All rights reserved.
//

#import "IMPDFViewController.h"
#import "IMGraphView.h"
#import "IMPieChartView.h"
#import "UIView+IMFrame.h"
#import "IMUKTSPOTvTST.h"

@interface IMPDFViewController ()

@property (nonatomic, strong) NSArray *pages;

@end

@implementation IMPDFViewController

- (void) parseLabels
{
	[self.pages enumerateObjectsUsingBlock:^(UIView *pageView, NSUInteger idx, BOOL *stop) {
        [self parseView:pageView];
    }];
}

-(void)parseView:(UIView*)pageView{
    [pageView.subviews enumerateObjectsUsingBlock:^(UIView *subView, NSUInteger idx, BOOL *stop) {
        if([subView isKindOfClass:[UILabel class]])
        {
            UILabel *l = (UILabel *)subView;
            NSError *error = nil;
            NSMutableAttributedString *attString = [l.attributedText mutableCopy];
            NSString *string = attString.string;
            NSRegularExpression *superscriptRegex = [NSRegularExpression regularExpressionWithPattern:@"®" options:NSRegularExpressionCaseInsensitive error:&error];
            [superscriptRegex enumerateMatchesInString:string
                                               options:0
                                                 range:NSMakeRange(0, [string length])
                                            usingBlock:^(NSTextCheckingResult *result, NSMatchingFlags flags, BOOL *stop) {
                                                NSMutableDictionary *atts = [[attString attributesAtIndex:result.range.location effectiveRange:NULL] mutableCopy];
                                                UIFont *f = atts[NSFontAttributeName];
                                                atts[NSFontAttributeName] = [UIFont fontWithName:f.fontName size:f.pointSize-3];
                                                atts[NSBaselineOffsetAttributeName] = @3.0;
                                                [attString setAttributes:atts range:result.range];
                                            }];
            
            superscriptRegex = [NSRegularExpression regularExpressionWithPattern:@"[A-Za-z%](\\d+?)\\." options:NSRegularExpressionCaseInsensitive error:&error];
            [superscriptRegex enumerateMatchesInString:string
                                               options:0
                                                 range:NSMakeRange(0, [string length])
                                            usingBlock:^(NSTextCheckingResult *result, NSMatchingFlags flags, BOOL *stop) {
                                                NSMutableDictionary *atts = [[attString attributesAtIndex:[result rangeAtIndex:1].location effectiveRange:NULL] mutableCopy];
                                                UIFont *f = atts[NSFontAttributeName];
                                                atts[NSFontAttributeName] = [UIFont fontWithName:f.fontName size:f.pointSize-3];
                                                atts[NSBaselineOffsetAttributeName] = @3.0;
                                                [attString setAttributes:atts range:[result rangeAtIndex:1]];
                                            }];
            
            NSRegularExpression *regex = [NSRegularExpression regularExpressionWithPattern:@"<.*?>" options:NSRegularExpressionCaseInsensitive error:&error];
            
            NSRange rangeOfFirstMatch = [regex rangeOfFirstMatchInString:string options:0 range:NSMakeRange(0, [string length])];
            while(!NSEqualRanges(rangeOfFirstMatch, NSMakeRange(NSNotFound, 0))) {
                NSRange r = NSMakeRange(rangeOfFirstMatch.location + 1, rangeOfFirstMatch.length - 2);
                NSString *theMatch = [string substringWithRange:r];
                NSString *spreadSheetValue = @"";
                
                if([theMatch isEqualToString:@"customer name"])
                {
					spreadSheetValue = [self.spreadSheet.workbook customerName];
					if(!spreadSheetValue) {
						spreadSheetValue = @"";
					}
                    [attString replaceCharactersInRange:rangeOfFirstMatch withString:spreadSheetValue];
                }else if([theMatch isEqualToString:@"info"]){
                    if(self.additionalInfo)
                    {
                        spreadSheetValue = self.additionalInfo;
                    }else{
//                        l.hidden = YES;
                    }
					if(!spreadSheetValue) {
						spreadSheetValue = @"";
					}
					if([self.spreadSheet.workbook isKindOfClass:[IMUKTSPOTvTST class]]) {
						NSAttributedString * refs = [[NSAttributedString alloc] initWithString:@"\n\nReferences\n1. Public Health England. Tuberculosis in England: 2015 report. January 2015. https://www.gov.uk/government/publications/tuberculosis-in-england-annual-report\n2. De Keyser E, De Keyser F, De Baets F. Tuberculin skin test versus interferon-gamma release assays for the diagnosis of tuberculosis infection. Acta Clin Belg. 2014;69(5):358-366. doi:10.1179/2295333714Y.0000000043.\n3. Oxford Immunotec Ltd. T-SPOT.TB Package Insert 2013." attributes:nil];
						[attString appendAttributedString:refs];
					}
					
                    [attString replaceCharactersInRange:rangeOfFirstMatch withString:spreadSheetValue];
                    CGRect rect = [attString boundingRectWithSize:CGSizeMake(l.frame.size.width, 600.0) options:NSStringDrawingUsesLineFragmentOrigin context:nil];
                    rect.origin = l.frame.origin;
                    l.frame = rect;
                }else if([theMatch isEqualToString:@"dislaimer"]){
					spreadSheetValue = self.disclaimerText;
					if(!spreadSheetValue) {
						spreadSheetValue = @"";
					}
                    [attString replaceCharactersInRange:rangeOfFirstMatch withString:spreadSheetValue];
                    CGRect rect = [attString boundingRectWithSize:CGSizeMake(l.frame.size.width, 400.0) options:NSStringDrawingUsesLineFragmentOrigin context:nil];
                    rect.origin.y = 1000.0f - rect.size.height;
                    rect.origin.x = l.frame.origin.x;
                    l.frame = rect;
                }else{
                    NSString *firstChar = [theMatch substringToIndex:1];
                    if([firstChar isEqualToString:@"$"])
                    {
                        theMatch = [theMatch substringFromIndex:1];
						spreadSheetValue = [self.spreadSheet valueForIdentifier:theMatch].intCurrencyValue;
						if(!spreadSheetValue) {
							spreadSheetValue = @"";
						}
						if(spreadSheetValue.length == 0) {
							spreadSheetValue = @"$0";
						}
                    }else if([firstChar isEqualToString:@"%"]){
                        theMatch = [theMatch substringFromIndex:1];
                        spreadSheetValue = [self.spreadSheet valueForIdentifier:theMatch].percentStringValue;
                    }else{
                        spreadSheetValue = [self.spreadSheet valueForIdentifier:theMatch].intStringValue;
                    }
                    if(!spreadSheetValue)
                    {
                        spreadSheetValue = @"";
                    }
                    [attString replaceCharactersInRange:rangeOfFirstMatch withString:spreadSheetValue];
                }
				
                string = attString.string;
                rangeOfFirstMatch = [regex rangeOfFirstMatchInString:string options:0 range:NSMakeRange(0, [string length])];
            }
            
            
            l.attributedText = attString;
        }else if([subView isKindOfClass:[UITextView class]])
        {
            UITextView *l = (UITextView *)subView;
			
			if([l.text isEqualToString:@"<initialContent>"]){
				//					spreadSheetValue = self.disclaimerText;
				//					[attString replaceCharactersInRange:rangeOfFirstMatch withString:spreadSheetValue];
				l.attributedText = self.spreadSheet.workbook.initialContent;
				
			}else{
			
            NSError *error = nil;
            NSMutableAttributedString *attString = [l.attributedText mutableCopy];
            NSString *string = attString.string;
            NSRegularExpression *superscriptRegex = [NSRegularExpression regularExpressionWithPattern:@"®" options:NSRegularExpressionCaseInsensitive error:&error];
            [superscriptRegex enumerateMatchesInString:string
                                               options:0
                                                 range:NSMakeRange(0, [string length])
                                            usingBlock:^(NSTextCheckingResult *result, NSMatchingFlags flags, BOOL *stop) {
                                                NSMutableDictionary *atts = [[attString attributesAtIndex:result.range.location effectiveRange:NULL] mutableCopy];
                                                UIFont *f = atts[NSFontAttributeName];
                                                atts[NSFontAttributeName] = [UIFont fontWithName:f.fontName size:f.pointSize-3];
                                                atts[NSBaselineOffsetAttributeName] = @3.0;
                                                [attString setAttributes:atts range:result.range];
                                            }];
            
            superscriptRegex = [NSRegularExpression regularExpressionWithPattern:@"[A-Za-z%](\\d+?)\\." options:NSRegularExpressionCaseInsensitive error:&error];
            [superscriptRegex enumerateMatchesInString:string
                                               options:0
                                                 range:NSMakeRange(0, [string length])
                                            usingBlock:^(NSTextCheckingResult *result, NSMatchingFlags flags, BOOL *stop) {
                                                NSMutableDictionary *atts = [[attString attributesAtIndex:[result rangeAtIndex:1].location effectiveRange:NULL] mutableCopy];
                                                UIFont *f = atts[NSFontAttributeName];
                                                atts[NSFontAttributeName] = [UIFont fontWithName:f.fontName size:f.pointSize-3];
                                                atts[NSBaselineOffsetAttributeName] = @3.0;
                                                [attString setAttributes:atts range:[result rangeAtIndex:1]];
                                            }];
            
            NSRegularExpression *regex = [NSRegularExpression regularExpressionWithPattern:@"<.*?>" options:NSRegularExpressionCaseInsensitive error:&error];
            
            NSRange rangeOfFirstMatch = [regex rangeOfFirstMatchInString:string options:0 range:NSMakeRange(0, [string length])];
            while(!NSEqualRanges(rangeOfFirstMatch, NSMakeRange(NSNotFound, 0))) {
                NSRange r = NSMakeRange(rangeOfFirstMatch.location + 1, rangeOfFirstMatch.length - 2);
                NSString *theMatch = [string substringWithRange:r];
                NSString *spreadSheetValue = @"";
                
                if([theMatch isEqualToString:@"customer name"])
                {
                    spreadSheetValue = [self.spreadSheet.workbook customerName];
                    [attString replaceCharactersInRange:rangeOfFirstMatch withString:spreadSheetValue];
                }else if([theMatch isEqualToString:@"info"]){
                    if(self.additionalInfo)
                    {
                        spreadSheetValue = self.additionalInfo;
                    }else{
                        l.hidden = YES;
                    }
                    [attString replaceCharactersInRange:rangeOfFirstMatch withString:spreadSheetValue];
                    CGRect rect = [attString boundingRectWithSize:CGSizeMake(l.frame.size.width, 500.0) options:NSStringDrawingUsesLineFragmentOrigin context:nil];
                    rect.origin = l.frame.origin;
                    l.frame = rect;
                }else if([theMatch isEqualToString:@"dislaimer"]){
                    spreadSheetValue = self.disclaimerText;
                    [attString replaceCharactersInRange:rangeOfFirstMatch withString:spreadSheetValue];
                    CGRect rect = [attString boundingRectWithSize:CGSizeMake(l.frame.size.width, 400.0) options:NSStringDrawingUsesLineFragmentOrigin context:nil];
                    rect.origin.y = 1000.0f - rect.size.height;
                    rect.origin.x = l.frame.origin.x;
                    l.frame = rect;
				}else{
                    NSString *firstChar = [theMatch substringToIndex:1];
                    if([firstChar isEqualToString:@"$"])
                    {
                        theMatch = [theMatch substringFromIndex:1];
                        spreadSheetValue = [self.spreadSheet valueForIdentifier:theMatch].intCurrencyValue;
						if(spreadSheetValue.length == 0) {
							spreadSheetValue = @"$0";
						}
                    }else if([firstChar isEqualToString:@"%"]){
                        theMatch = [theMatch substringFromIndex:1];
                        spreadSheetValue = [self.spreadSheet valueForIdentifier:theMatch].percentStringValue;
                    }else{
                        spreadSheetValue = [self.spreadSheet valueForIdentifier:theMatch].intStringValue;
                    }
                    if(!spreadSheetValue)
                    {
                        spreadSheetValue = @"";
                    }
                    [attString replaceCharactersInRange:rangeOfFirstMatch withString:spreadSheetValue];
                }
                
                string = attString.string;
                rangeOfFirstMatch = [regex rangeOfFirstMatchInString:string options:0 range:NSMakeRange(0, [string length])];
            }
            
            
            l.attributedText = attString;
			}
        }else if([subView isKindOfClass:[IMGraphView class]]){
            IMGraphView *gv = (IMGraphView *)subView;
            NSArray *keys = [gv.cellIdsForLeftBar componentsSeparatedByString:@","];
            NSMutableArray *values = [NSMutableArray array];
            [keys enumerateObjectsUsingBlock:^(NSString  *obj, NSUInteger idx, BOOL *stop) {
				if([self.spreadSheet valueForIdentifier:obj]){
					[values addObject:[self.spreadSheet valueForIdentifier:obj]];
				}
            }];
            gv.valuesForLeftBar = values;
            
            keys = [gv.cellIdsForRightBar componentsSeparatedByString:@","];
            values = [NSMutableArray array];
            [keys enumerateObjectsUsingBlock:^(NSString  *obj, NSUInteger idx, BOOL *stop) {
				if([self.spreadSheet valueForIdentifier:obj]){
					[values addObject:[self.spreadSheet valueForIdentifier:obj]];
				}
            }];
            gv.valuesForRightBar = values;
            //				gv.labelLeft.text = @"Current Practice";
            //				gv.labelRight.text = @"T-SPOT®.TB";
		}else if([subView isKindOfClass:[IMPieChartView class]]){
			IMPieChartView *gv = (IMPieChartView *)subView;
			gv.backgroundColor = [UIColor clearColor];
			NSArray *a = [gv.cellIds componentsSeparatedByString:@","];
			if(a.count <= 0)
			{
				return;
			}
			
			[a enumerateObjectsUsingBlock:^(NSString *cellId, NSUInteger idx, BOOL *stop) {
				NSNumber *val = [self.spreadSheet valueForIdentifier:cellId];
				if(!val)
				{
					val = [NSNumber numberWithDouble:0.0];
				}
				[gv setValue:val forKey:[NSString stringWithFormat:@"value%lu", (unsigned long)idx+1]];
			}];
			[gv setNeedsDisplay];
			
		}else if(subView.subviews.count > 0) {
			[self parseView:subView];
        }
    }];

}


- (NSData *) pdfVersion
{
	if(self.pages.count == 0)
	{
		return nil;
	}
	[self parseLabels];
	NSMutableData *pdfData = [NSMutableData data];
	
	UIGraphicsBeginPDFContextToData(pdfData, CGRectMake(0.0f, 0.0f, 612.0f, 792.0f), nil);
//	UIGraphicsBeginPDFPage();
	
	CGContextRef pdfContext = UIGraphicsGetCurrentContext();
	
	for(UIView *v in self.pages)
	{
		
//		v.transform = CGAffineTransformMakeScale(792.0f/1024.0f, 792.0f/1024.0f);
		
		UIGraphicsBeginImageContext(CGSizeMake(768.0f, 1024.0f));
		
		CGContextRef imgContext = UIGraphicsGetCurrentContext();
		[v.layer renderInContext:imgContext];
//		CGContextScaleCTM(imgContext, 792.0f/1024.0f, 792.0f/1024.0f);
		UIImage *image = UIGraphicsGetImageFromCurrentImageContext();
//		NSLog(@"image size - %f : %f", image.size.width, image.size.height);
		UIGraphicsEndImageContext();
		
		UIGraphicsBeginImageContext(CGSizeMake(594.0, 792.0f));
		imgContext = UIGraphicsGetCurrentContext();
		CGContextTranslateCTM(imgContext, 0, 792.0);
		CGContextScaleCTM(imgContext, 1, -1);
		[image drawInRect:CGRectMake(0.0f, 0.0f, 594.0, 792.0f)];
		UIImage *newImage = UIGraphicsGetImageFromCurrentImageContext();
		UIGraphicsEndImageContext();
		
		UIGraphicsBeginPDFPage();
		CGContextDrawImage(pdfContext, CGRectMake(9.0f, 0.0f, 594.0, 792.0f), newImage.CGImage);
		
//		[v.layer renderInContext:pdfContext];
	}
	
	UIGraphicsEndPDFContext();
	
	return pdfData;
}

#pragma mark - UIViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nil bundle:nil];
    if (self) {
		self.disclaimerText = @"";
		self.pages = [[NSBundle mainBundle] loadNibNamed:nibNameOrNil owner:nil options:nil];
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
