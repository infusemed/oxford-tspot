//
//  IMMainViewController.m
//  Tspot Calculator
//
//  Created by Anthony Long on 6/9/14.
//  Copyright (c) 2014 Infuse Medical. All rights reserved.
//

#import "IMMainViewController.h"
#import "IMSessiontypeViewController.h"
#import "IMCalculatorViewController.h"
#import "IMCalculatorSelectionTableViewCell.h"
#import "UIView+IMFrame.h"
#import "IMCalcListCollectionViewCell.h"
#import "IMCalcListCollectionReusableView.h"
#import "IMCalcListTabButton.h"

@interface IMMainViewController () <UICollectionViewDataSource, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout>

@property (weak, nonatomic) IBOutlet UIView *viewList;
@property (weak, nonatomic) IBOutlet UILabel *labelVersion;
@property (weak, nonatomic) IBOutlet UICollectionView *collectionView;

@end

@implementation IMMainViewController
{
	NSArray *_config;
	NSIndexPath *_selectedIndexPath;
	NSInteger _selectedTab;
	NSArray *_tabs;
}

- (void)sessionTypeViewController:(IMSessiontypeViewController *)controller didSelectCalculatorURL:(NSURL *)url
{
	IMCalculatorViewController *vc = [[IMCalculatorViewController alloc] initWithNibName:@"IMCalculatorViewController" bundle:nil];
	NSDictionary *d = _config[_selectedTab][@"sections"][_selectedIndexPath.section][@"items"][_selectedIndexPath.item];
	Class workbookClass = NSClassFromString(d[@"model"]);
	if(!url)
	{
		vc.workbook = [[workbookClass alloc] init];
	}else{
		vc.workbook = [[workbookClass alloc] initWithFile:url.path];
	}
	vc.config = d;
	
	[self presentViewController:vc
					   animated:YES
					 completion:NULL];
}

- (void) animateListIn
{
	__weak IMMainViewController *weakself = self;
	[UIView animateWithDuration:0.4f
						  delay:0.0f
		 usingSpringWithDamping:0.5f
		  initialSpringVelocity:0.2f
						options:UIViewAnimationOptionCurveEaseOut
					 animations:^{
						 CGFloat newX = 1024.0f - weakself.viewList.frameWidth - 116.0f;
						 [_tabs enumerateObjectsUsingBlock:^(IMCalcListTabButton *tab, NSUInteger idx, BOOL *stop) {
							 tab.frameX = newX;
						 }];
						 weakself.viewList.frameX = weakself.view.frameWidth - weakself.viewList.frameWidth;
					 } completion:^(BOOL finished) {
						 
					 }];
}

- (void) animateListOutWithCompletion:(void (^)(BOOL finished))completion
{
	__weak IMMainViewController *weakself = self;
	[UIView animateWithDuration:0.4
						  delay:0.0f
		 usingSpringWithDamping:0.5f
		  initialSpringVelocity:0.2
						options:UIViewAnimationOptionCurveEaseIn
					 animations:^{
						 [_tabs enumerateObjectsUsingBlock:^(IMCalcListTabButton *tab, NSUInteger idx, BOOL *stop) {
							 tab.selected = NO;
							 tab.frameX = 909.0f;
						 }];
						 weakself.viewList.frameX = weakself.view.frameWidth;
					 } completion:completion];
}

- (void)showFilePopover
{
	IMSessiontypeViewController *vc = [[IMSessiontypeViewController alloc] initWithNibName:@"IMSessiontypeViewController" bundle:nil];
	vc.delegate = self;
	vc.directoryName = _config[_selectedTab][@"sections"][_selectedIndexPath.section][@"items"][_selectedIndexPath.item][@"saveDirectory"];
	[self presentViewController:vc animated:YES completion:NULL];
}

- (IBAction)handleSelectionTouch:(UIButton *)button
{
	if(button.selected)
	{
		[self animateListOutWithCompletion:NULL];
		button.selected = NO;
	}else{
		[_tabs enumerateObjectsUsingBlock:^(IMCalcListTabButton *tab, NSUInteger idx, BOOL *stop) {
			tab.selected = button == tab;
		}];
		_selectedTab = button.tag;
		[self.collectionView reloadData];
		[self animateListIn];
	}
}

#pragma mark - UICollectionViewDataSource

- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView
{
	return ((NSArray *)_config[_selectedTab][@"sections"]).count;
}

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
	return ((NSArray *)_config[_selectedTab][@"sections"][section][@"items"]).count;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
	IMCalcListCollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"ListCell" forIndexPath:indexPath];
	cell.text = _config[_selectedTab][@"sections"][indexPath.section][@"items"][indexPath.item][@"title"];
	cell.isTop = indexPath.item == 0;
	cell.isBottom = indexPath.item + 1 == [collectionView numberOfItemsInSection:indexPath.section];
	
	return cell;
}

- (UICollectionReusableView *)collectionView:(UICollectionView *)collectionView viewForSupplementaryElementOfKind:(NSString *)kind atIndexPath:(NSIndexPath *)indexPath
{
	IMCalcListCollectionReusableView *v = [collectionView dequeueReusableSupplementaryViewOfKind:kind withReuseIdentifier:@"ListHeader" forIndexPath:indexPath];
	v.textLabel.text = _config[_selectedTab][@"sections"][indexPath.section][@"title"];
	return v;
}

#pragma mark - UICollectionViewDelegate

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
	_selectedIndexPath = indexPath;
	__weak IMMainViewController *weakself = self;
	[self animateListOutWithCompletion:^(BOOL finished) {
		[weakself showFilePopover];
	}];
}

#pragma mark - UICollectionViewDelegateFlowLayout

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout referenceSizeForHeaderInSection:(NSInteger)section
{
	NSString *s = _config[_selectedTab][@"sections"][section][@"title"];
	if(!s || s.length < 1)
	{
		return CGSizeMake(0.0f, 0.0f);
	}else{
		return CGSizeMake(collectionView.frame.size.width, 40.0f);
	}
}

#pragma mark - UIViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	
	_selectedTab = 0;
	
	self.viewList.frameX = self.view.frameWidth;
	
	_config = [NSArray arrayWithContentsOfFile:[[NSBundle mainBundle] pathForResource:@"calculators" ofType:@"plist"]];
	[self.collectionView registerClass:[IMCalcListCollectionViewCell class] forCellWithReuseIdentifier:@"ListCell"];
	[self.collectionView registerClass:[IMCalcListCollectionReusableView class] forSupplementaryViewOfKind:UICollectionElementKindSectionHeader
				   withReuseIdentifier:@"ListHeader"];
	
	NSMutableArray *ma = [NSMutableArray array];
	[_config enumerateObjectsUsingBlock:^(NSDictionary *obj, NSUInteger idx, BOOL *stop) {
		IMCalcListTabButton *b = [[IMCalcListTabButton alloc] initWithFrame:CGRectMake(909.0f, 602.0f - ((_config.count - idx - 1) * 110), 116.0f, 96.0f)];
		[b setTitle:obj[@"title"] forState:UIControlStateNormal];
		[b setImage:[UIImage imageNamed:@"drawer-tab-icon"] forState:UIControlStateNormal];
		[b setBackgroundImage:[UIImage imageNamed:@"drawer-tab-off"] forState:UIControlStateNormal];
		[b setBackgroundImage:[UIImage imageNamed:@"drawer-tab"] forState:UIControlStateSelected];
		[b addTarget:self action:@selector(handleSelectionTouch:) forControlEvents:UIControlEventTouchUpInside];
		b.tag = idx;
		[self.view addSubview:b];
		[ma addObject:b];
	}];
	_tabs = [NSArray arrayWithArray:ma];
	self.labelVersion.text = [NSString stringWithFormat:@"v %@", [[[NSBundle mainBundle] infoDictionary] objectForKey:@"CFBundleShortVersionString"]];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
