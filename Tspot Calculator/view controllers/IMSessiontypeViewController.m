//
//  IMSessiontypeViewController.m
//  Tspot Calculator
//
//  Created by Anthony Long on 6/10/14.
//  Copyright (c) 2014 Infuse Medical. All rights reserved.
//

#import "IMSessiontypeViewController.h"
#import "IMSessionTableViewCell.h"


@interface IMSessiontypeViewController ()

@property (weak, nonatomic) IBOutlet UIView *viewPopup;
@property (weak, nonatomic) IBOutlet UITableView *tvSessionList;
@property (weak, nonatomic) IBOutlet UIButton * buttonClose;
@property (weak, nonatomic) IBOutlet UIButton *buttonLoad;
@property (strong, nonatomic) UIVisualEffectView *viewBlur;

@end

@implementation IMSessiontypeViewController
{
	NSMutableArray *_directoryContents;
}

static NSString *sessionCellIdentifier = @"SessionCell";

#pragma mark - IBActions

- (IBAction)handleCloseTouch:(UIButton *)button
{
	[self dismissViewControllerAnimated:YES completion:NULL];
}

- (IBAction)handleNewTouch:(UITapGestureRecognizer *)gesture
{
	[self dismissViewControllerAnimated:YES
							 completion:^ {
								 [self.delegate sessionTypeViewController:self didSelectCalculatorURL:nil];
							 }];
}

- (IBAction)handleLoadTouch:(UIButton *)button
{
	[self dismissViewControllerAnimated:YES
							 completion:^ {
								 [self.delegate sessionTypeViewController:self didSelectCalculatorURL:_directoryContents[self.tvSessionList.indexPathForSelectedRow.row]];
							 }];
}

#pragma mark - UIViewControllerContextTransitioning

- (id<UIViewControllerAnimatedTransitioning>)animationControllerForPresentedController:(UIViewController *)presented
                                                                  presentingController:(UIViewController *)presenting
                                                                      sourceController:(UIViewController *)source
{
	return self;
}


- (id <UIViewControllerAnimatedTransitioning>)animationControllerForDismissedController:(UIViewController *)dismissed
{
	return self;
}

- (NSTimeInterval)transitionDuration:(id<UIViewControllerContextTransitioning>)transitionContext
{
	return 0.3;
}


#pragma mark - UIViewControllerAnimatedTransitioning

-(void)animateTransition:(id<UIViewControllerContextTransitioning>)transitionContext
{
    if(!self.viewBlur){
        [self animateInWithContext:transitionContext];
    }
    else{
        [self animateOutWithContext:transitionContext];
    }
	
}

#pragma mark - Modal presentation

- (void) animateInWithContext:(id<UIViewControllerContextTransitioning>)transitionContext
{
	UIView *containerView = [transitionContext containerView];
	UIViewController *fromViewController = [transitionContext viewControllerForKey:UITransitionContextFromViewControllerKey];
	
	UIVisualEffectView *vev = [[UIVisualEffectView alloc] initWithEffect:[UIBlurEffect effectWithStyle:UIBlurEffectStyleLight]];
	vev.frame = fromViewController.view.frame;
	
	[containerView addSubview:vev];
	containerView.backgroundColor = [UIColor clearColor];
	[containerView addSubview:self.view];
	self.view.frame = ({
		CGRect rect = self.view.frame;
		rect.origin.y = 768.0f;
		rect;
	});
	
	__weak IMSessiontypeViewController *weakself = self;
	[UIView animateWithDuration:0.3
						  delay:0.0f
						options:UIViewAnimationOptionCurveEaseOut
					 animations:^{
						 weakself.view.frame = ({
							 CGRect rect = weakself.view.frame;
							 rect.origin.y = 0.0;
							 rect;
						 });
					 } completion:^(BOOL finished) {
						 [self.view insertSubview:vev atIndex:0];
						 [transitionContext completeTransition:YES];
					 }];
	
	self.viewBlur = vev;
}

- (void) animateOutWithContext:(id<UIViewControllerContextTransitioning>)transitionContext
{
	UIView *containerView = [transitionContext containerView];
	containerView.backgroundColor = [UIColor clearColor];
	[containerView insertSubview:self.viewBlur atIndex:0];
	[containerView addSubview:self.view];
	
	__weak IMSessiontypeViewController *weakself = self;
	[UIView animateWithDuration:0.5
						  delay:0.0f
						options:UIViewAnimationOptionCurveEaseIn
					 animations:^{
						 weakself.view.frame = ({
							 CGRect rect = weakself.view.frame;
							 rect.origin.y = 768.0f;
							 rect;
						 });
					 } completion:^(BOOL finished) {
						 [transitionContext completeTransition:YES];
					 }];
}

#pragma mark - UITableViewDelegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
	self.buttonLoad.enabled = YES;
	self.buttonLoad.alpha = 1.0f;
}


#pragma mark - UITableViewDataSource

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
	return _directoryContents.count;
}


- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
	return 1;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
	IMSessionTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:sessionCellIdentifier];
	if(!cell)
	{
		cell = [[IMSessionTableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:sessionCellIdentifier];
		[cell.trashButton addTarget:self action:@selector(handleTrashTouch:) forControlEvents:UIControlEventTouchUpInside];
	}
	
	cell.trashButton.tag = indexPath.row;
	cell.textLabel.text = [[_directoryContents[indexPath.row] lastPathComponent] stringByDeletingPathExtension];
	
	return cell;
}

- (void) handleTrashTouch:(UIButton *)button
{
	NSInteger row = button.tag;
	NSURL *url = [_directoryContents objectAtIndex:row];
	[[NSFileManager defaultManager] removeItemAtURL:url error:NULL];
	[_directoryContents removeObjectAtIndex:row];
	[self.tvSessionList deleteRowsAtIndexPaths:@[[NSIndexPath indexPathForRow:row inSection:0]] withRowAnimation:UITableViewRowAnimationRight];
}

#pragma mark - UIViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        self.transitioningDelegate = self;
		self.modalPresentationStyle = UIModalPresentationCustom;
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    self.view.backgroundColor = [UIColor clearColor];
	
	self.viewPopup.layer.borderWidth = 3.0f;
	self.viewPopup.layer.borderColor = [UIColor colorWithRed:154.0f/255.0f
												green:159.0f/255.0f
												 blue:207.0f/255.0f
												alpha:1.0f].CGColor;
	self.viewPopup.layer.cornerRadius = 10.0f;
	self.viewPopup.layer.masksToBounds = YES;
	self.viewPopup.layer.shadowColor = [UIColor colorWithRed:135.0f/255.0f
													   green:129.0f/255.0f
														blue:189.0f/255.0f
													   alpha:1.0f].CGColor;
	self.viewPopup.layer.shadowOpacity = 0.5f;
	self.viewPopup.layer.shadowRadius = 22.0f;
	self.viewPopup.layer.shadowOffset = CGSizeMake(0.0f, -0.5f);
	
	_directoryContents = [NSMutableArray array];
	self.buttonLoad.alpha = 0.4;
	self.buttonLoad.enabled = NO;
	NSFileManager *localFileManager=[[NSFileManager alloc] init];
	
	NSURL *directoryURL = [[[[NSFileManager defaultManager] URLsForDirectory:NSDocumentDirectory inDomains:NSUserDomainMask] lastObject] URLByAppendingPathComponent:self.directoryName];
	
	NSDirectoryEnumerator *dirEnumerator = [localFileManager enumeratorAtURL:directoryURL
												  includingPropertiesForKeys:[NSArray arrayWithObjects:NSURLNameKey,
																			  NSURLIsDirectoryKey,nil]
																	 options:NSDirectoryEnumerationSkipsHiddenFiles
																errorHandler:nil];
	
	for (NSURL *theURL in dirEnumerator) {
		NSString *fileName;
		[theURL getResourceValue:&fileName forKey:NSURLNameKey error:NULL];
		NSNumber *isDirectory;
		[theURL getResourceValue:&isDirectory forKey:NSURLIsDirectoryKey error:NULL];
		if ([isDirectory boolValue]==NO)
		{
			[_directoryContents addObject:theURL];
		}
	}
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
