//
//  IMBaseViewController.h
//  Practice Productivity Analysis
//
//  Created by Anthony Long on 9/4/13.
//  Copyright (c) 2013 Infuse Medical. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "IMSpreadSheet.h"

@interface IMBaseViewController : UIViewController <UITextFieldDelegate>

@property (nonatomic, readonly, strong) NSMutableDictionary * textFieldMappings, * labelMappings;
@property (nonatomic, strong) IMWorkbook * workbook;
@property (nonatomic, strong) NSArray * textFieldKeys, * labelKeys;
@property (nonatomic, strong) NSMutableArray * labels;
@property (nonatomic, strong) IMSpreadSheet * spreadsheet;
- (void) mapFields;
- (void) clearMappings;

@end