//
//  IMBaseViewController.m
//  Practice Productivity Analysis
//
//  Created by Anthony Long on 9/4/13.
//  Copyright (c) 2013 Infuse Medical. All rights reserved.
//

#import "IMBaseViewController.h"
#import "IMSpreadSheetTextField.h"
#import "IMSpreadSheetLabel.h"
#import "IMSpreadSheetCell.h"
//#import "IMSpreadSheetCPTButton.h"
//#import "IMDescriptionView.h"

@interface IMBaseViewController ()

@end

@implementation IMBaseViewController
{
	// for keyboard animations
	// ***************************
	
    BOOL _keyboardVisible;
	UITextField * _currentTextField;
	CGRect _keyboardFrame;
    NSNumber * _keyboardAnimationDuration;
	NSString * _previousTextValue;
	
	// ***************************	
}

@synthesize textFieldMappings=_textFieldMappings, labelMappings=_labelMappings;

- (NSMutableDictionary *) textFieldMappings
{
	if(!_textFieldMappings)
	{
		_textFieldMappings = [NSMutableDictionary dictionary];
	}
	
	return _textFieldMappings;
}

- (NSMutableDictionary *) labelMappings
{
	if(!_labelMappings)
	{
		_labelMappings = [NSMutableDictionary dictionary];
	}
	
	return _labelMappings;
}

//- (void) handleCPTTouch:(IMSpreadSheetCPTButton *)button
//{
//	
//}

- (void) mapFields
{
	self.labels = [NSMutableArray array];
	UIView * subview;
	for(int i=0;i<self.view.subviews.count;i++)
	{
		if(subview && [subview isKindOfClass:[IMSpreadSheetTextField class]])
		{
			((IMSpreadSheetTextField *)subview).returnKeyType = UIReturnKeyNext;
		}
		subview = [self.view.subviews objectAtIndex:i];
		if([subview isKindOfClass:[IMSpreadSheetTextField class]])
		{
			NSString * s = [subview valueForKey:@"cellId"];
			if(!s)
			{
				continue;
			}
			IMSpreadSheetTextField * tf = (IMSpreadSheetTextField *) subview;
			tf.adjustsFontSizeToFitWidth = YES;
			tf.minimumFontSize = 8.0f;
			tf.delegate = self;
			tf.keyboardType = UIKeyboardTypeDecimalPad;
			tf.returnKeyType = UIReturnKeyDone;
			IMSpreadSheetCell * cell = [self.spreadsheet spreadSheetCellForIdentifier:s];
			[cell addObserver:self forKeyPath:@"value" options:NSKeyValueObservingOptionNew context:nil];
			tf.text = [self textForValue:cell.value usingType:tf.formatType];
			[self.textFieldMappings setObject:tf forKey:s];
		}else if([subview isKindOfClass:[IMSpreadSheetLabel class]])
		{
			NSString * s = [subview valueForKey:@"cellId"];
			if(!s)
			{
				continue;
			}
			IMSpreadSheetLabel * tf = (IMSpreadSheetLabel *) subview;
			tf.adjustsFontSizeToFitWidth = YES;
			IMSpreadSheetCell * cell = [self.spreadsheet spreadSheetCellForIdentifier:s];
			[cell addObserver:self forKeyPath:@"value" options:NSKeyValueObservingOptionNew context:nil];
			if(tf.formatType.intValue == 5)
			{
				if(cell.value.boolValue)
				{
					tf.text = @"Day";
				}else{
					tf.text = @"Month";
				}
			}else{
				tf.text = [self textForValue:cell.value usingType:tf.formatType];
			}
			[self.labelMappings setObject:tf forKey:s];
		}
		else if ([subview isKindOfClass:[UILabel class]])
		{
			UILabel * l = (UILabel *)subview;
			if([l.text rangeOfString:@"DAY"].location != NSNotFound || [l.text rangeOfString:@"MONTH"].location != NSNotFound)
			{
				[self.labels addObject:l];
			}
		}
	}
}

- (void) clearMappings
{
	IMSpreadSheetTextField * tf;
	for(NSString * k in _textFieldMappings.allKeys)
	{
		tf = [self.textFieldMappings objectForKey:k];
		tf.delegate = nil;
		NSString * s = [tf valueForKey:@"cellId"];
		IMSpreadSheetCell * cell = [self.spreadsheet spreadSheetCellForIdentifier:s];
		[cell removeObserver:self forKeyPath:@"value"];
	}
	
	IMSpreadSheetLabel * l;
	for(NSString * k in _labelMappings.allKeys)
	{
		l = [self.labelMappings objectForKey:k];
		NSString * s = [l valueForKey:@"cellId"];
		IMSpreadSheetCell * cell = [self.spreadsheet spreadSheetCellForIdentifier:s];
		[cell removeObserver:self forKeyPath:@"value"];
	}
	
	_textFieldMappings = nil;
	_labelMappings = nil;
}

#pragma mark - KVO

- (void) observeValueForKeyPath:(NSString *)keyPath ofObject:(id)object change:(NSDictionary *)change context:(void *)context
{
	IMSpreadSheetCell * cell = (IMSpreadSheetCell * )object;
	NSIndexPath * indexpath = cell.indexPath;
	NSString * cellId = [NSString stringWithFormat:@"%@%d", indexpath.spreadsheetColumn, indexpath.spreadsheetRow];
	
	if([[self.labelMappings allKeys] containsObject:cellId])
	{
		IMSpreadSheetLabel * label = (IMSpreadSheetLabel *)[self.labelMappings objectForKey:cellId];
		if(label.formatType.intValue == 5)
		{
			if(cell.value.boolValue)
			{
				label.text = @"Day";
			}else{
				label.text = @"Month";
			}
		}else{
			label.text = [self textForValue:cell.value usingType:label.formatType];
		}
	}else if([[self.textFieldMappings allKeys] containsObject:cellId])
	{
		IMSpreadSheetTextField * tf = (IMSpreadSheetTextField *)[self.textFieldMappings objectForKey:cellId];
		tf.text = [self textForValue:cell.value usingType:tf.formatType];
	}
}

- (NSString *) textForValue:(NSNumber *)value usingType:(NSNumber *)formatType
{
	int i = formatType.intValue;
	if(i == 0)
	{
		return value.numericValue;
	}else if(i==1)
	{
		return value.intStringValue;
	}else if(i==2)
	{
		return value.percentStringValue;
	}else if(i==3)
	{
		return value.intCurrencyValue;
	}else if(i==4)
	{
		return value.currencyValue;
	}
	return @"";
}


#pragma mark - UITextFieldDelegate

- (BOOL) textFieldShouldBeginEditing:(UITextField *)textField
{
	if(_currentTextField && [_currentTextField.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]].length == 0)
	{
		_currentTextField.text = _previousTextValue;
	}
    _currentTextField = textField;
	_previousTextValue = textField.text;
	return YES;
}

- (BOOL) textFieldShouldEndEditing:(UITextField *)textField
{
	return YES;
}

- (void) textFieldDidEndEditing:(IMSpreadSheetTextField *)textField
{
	IMSpreadSheetCell * cell = [self.spreadsheet spreadSheetCellForIdentifier:textField.cellId];
	[cell removeObserver:self forKeyPath:@"value"];
	NSString * s = [textField.text stringByReplacingOccurrencesOfString:@"%" withString:@""];
	s = [s stringByReplacingOccurrencesOfString:@"$" withString:@""];
	s = [s stringByReplacingOccurrencesOfString:@"£" withString:@""];
	s = [s stringByReplacingOccurrencesOfString:@"," withString:@""];
	if(textField.formatType.intValue == 2)
	{
		cell.value = [NSNumber numberWithDouble:s.doubleValue/100];
	}else{
		cell.value = [NSNumber numberWithDouble:s.doubleValue];
	}
	textField.text = [self textForValue:cell.value usingType:textField.formatType];
	[cell addObserver:self forKeyPath:@"value" options:NSKeyValueObservingOptionNew context:nil];
}

- (BOOL) textFieldShouldReturn:(UITextField *)textField
{
	if(_currentTextField && [_currentTextField.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]].length == 0)
	{
		_currentTextField.text = _previousTextValue;
	}

	
	NSInteger i = [textField.superview.subviews indexOfObject:textField]+1;
	
	for(int ii = i;ii<textField.superview.subviews.count;ii++)
	{
		if([textField.superview.subviews[ii] isKindOfClass:[UITextField class]])
		{
			UITextField *tf = textField.superview.subviews[ii];
//			_previousTextValue = tf.text;
			[tf becomeFirstResponder];
			return YES;
		}
	}
	
	[textField resignFirstResponder];
    return YES;
}

#pragma mark - UIViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
}

- (void) viewWillAppear:(BOOL)animated
{
	[super viewWillAppear:animated];
	[self mapFields];
}

- (void) viewDidDisappear:(BOOL)animated
{
	[super viewDidDisappear:animated];
	[self clearMappings];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (BOOL) shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)toInterfaceOrientation
{
	return UIInterfaceOrientationIsLandscape(toInterfaceOrientation);
}

- (UIInterfaceOrientationMask)supportedInterfaceOrientations
{
	return UIInterfaceOrientationMaskLandscape;
}

- (BOOL) shouldAutorotate
{
	return YES;
}

@end
