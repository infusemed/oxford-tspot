//
//  IMPDFViewController.h
//  Tspot Calculator
//
//  Created by Anthony Long on 7/27/14.
//  Copyright (c) 2014 Infuse Medical. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "IMSpreadSheet.h"

@interface IMPDFViewController : UIViewController

@property (nonatomic, assign) IMSpreadSheet *spreadSheet;
@property (nonatomic, copy) NSString *additionalInfo;
@property (nonatomic, copy) NSString *disclaimerText;

- (NSData *) pdfVersion;

@end