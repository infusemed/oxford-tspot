//
//  IMSessiontypeViewController.h
//  Tspot Calculator
//
//  Created by Anthony Long on 6/10/14.
//  Copyright (c) 2014 Infuse Medical. All rights reserved.
//

#import <UIKit/UIKit.h>

@class IMSessiontypeViewController;

@protocol IMSessiontypeViewControllerDelegate <NSObject>

- (void)sessionTypeViewController:(IMSessiontypeViewController *)controller didSelectCalculatorURL:(NSURL *)url;

@end

@interface IMSessiontypeViewController : UIViewController <UIViewControllerAnimatedTransitioning, UIViewControllerTransitioningDelegate, UITableViewDataSource, UITableViewDelegate>

@property (unsafe_unretained, nonatomic) id <IMSessiontypeViewControllerDelegate> delegate;
@property (nonatomic, copy) NSString *directoryName;

@end