//
//  PDFEditorViewController.swift
//  Tspot Calculator
//
//  Created by Anthony Long on 2/2/16.
//  Copyright © 2016 Infuse Medical. All rights reserved.
//
// 612 x 792

import UIKit
import MessageUI
fileprivate func < <T : Comparable>(lhs: T?, rhs: T?) -> Bool {
  switch (lhs, rhs) {
  case let (l?, r?):
    return l < r
  case (nil, _?):
    return true
  default:
    return false
  }
}


protocol PDFEditorDrawableView: NSObjectProtocol {
//	func drawRectInPDFContext(rect: CGRect, context: CGContextRef)
	var keyValues: NSObject? { get set }
	
}

class PDFEditorViewController: UIViewController, UIScrollViewDelegate, UICollectionViewDataSource, UICollectionViewDelegate, MFMailComposeViewControllerDelegate {
	
	var keyValues: NSObject?
	
	@IBOutlet var scrollView: TPKeyboardAvoidingScrollView!
	@IBOutlet var thumbScrollView: TPKeyboardAvoidingCollectionView!
	@IBOutlet weak var labelPageNumber: UILabel!
	var mailController: MFMailComposeViewController?
	
	var showPhased = false
	
	let contentView: UIView
	
	var pages: [UIView]
	var pageComponents: [UIView]
	var thumbs: [UIImage] = []
	var pageWidth = 0.0
	var currentPage = 0
	var scrollBuffer = 0.0
	var initialPageHeight = 3168
	var pageGap = 100.0
	
	var pdfData: Data {
		get {
			let data = NSMutableData()
//			UIGraphicsBeginPDFContextToData(data, CGRect(x: 0.0, y: 0.0, width: 2550.0, height: 3300.0), nil)
			UIGraphicsBeginPDFContextToData(data, CGRect.zero, nil)
			let context = UIGraphicsGetCurrentContext()
			for page in self.pages {
				let transform = page.transform
				page.transform = CGAffineTransform.identity
				UIGraphicsBeginPDFPage()
				context?.saveGState();
				context?.concatenate(CGAffineTransform(scaleX: 0.25, y: 0.25));
				page.layer.borderWidth = 0.0
				page.layer.render(in: context!)
				context?.restoreGState();
				page.transform = transform
				page.layer.borderWidth = 4.0
			}
			
			UIGraphicsEndPDFContext()
			
			return data as Data
		}
	}
	
	// MARK: - IBActions
	
	@IBAction func handleCancelTouch(_ sender: UIButton) {
		self.dismiss(animated: true, completion: nil)
	}
	
	
	@IBAction func handleShareTouch(_ sender: UIButton) {
		if !MFMailComposeViewController.canSendMail() {
			return
		}
//		let hud = MBProgressHUD.showHUDAddedTo(self.view, animated: true)
//		hud.labelText = "Generating PDF"
		
		self.mailController = MFMailComposeViewController()
		self.mailController?.addAttachmentData(self.pdfData, mimeType: "application/pdf", fileName: "Executive Summary.pdf")
		self.mailController?.setSubject("TB Screening Program Comparison Executive Summary")
//		self.mailController?.setMessageBody("", isHTML: false)
		self.mailController?.mailComposeDelegate = self
		self.present(self.mailController!, animated: true) {
//			MBProgressHUD.hideHUDForView(self.view, animated: true)
		}
	}
	
	func mailComposeController(_ controller: MFMailComposeViewController, didFinishWith result: MFMailComposeResult, error: Error?) {
		self.dismiss(animated: true, completion: nil)
		self.mailController = nil
		self.dismiss(animated: true, completion: nil)
	}
	
	
	// MARK: - 
	
	func updatePageNumber () {
		self.labelPageNumber.text = "Page \(self.currentPage + 1) of \(self.pages.count)"
	}
	
	func parseString(_ text: NSAttributedString) -> NSAttributedString {
		
		let regex: NSRegularExpression
		do {
			regex = try NSRegularExpression(pattern: "<.*?>", options: .caseInsensitive)
		}catch _ {
			print("error creating regex for variable replacement")
			return text
		}
		
		let mutableText = text.mutableCopy() as! NSMutableAttributedString
		var stringToSearch = mutableText.string
		var rangeOfFirstMatch = regex.rangeOfFirstMatch(in: stringToSearch, options: .reportCompletion, range: NSMakeRange(0, stringToSearch.characters.count))
		
		while rangeOfFirstMatch.location != NSNotFound {
//			let r = NSMakeRange(rangeOfFirstMatch.location + 1, rangeOfFirstMatch.length - 1)
			let range = (stringToSearch.characters.index(stringToSearch.startIndex, offsetBy: rangeOfFirstMatch.location + 1) ..< stringToSearch.characters.index(stringToSearch.startIndex, offsetBy: rangeOfFirstMatch.location + rangeOfFirstMatch.length - 1))
			var varToReplace = stringToSearch.substring(with: range)
			var isCurrency = false
			if varToReplace.characters.first == "$" {
				varToReplace.remove(at: varToReplace.startIndex)
				isCurrency = true
			}
			if let value: String = self.keyValues?.value(forKey: varToReplace) as? String {
				mutableText.replaceCharacters(in: rangeOfFirstMatch, with: value)
			}else if let value = self.keyValues?.value(forKey: varToReplace) as? NSAttributedString {
				mutableText.replaceCharacters(in: rangeOfFirstMatch, with: value)
			}else if let num = self.keyValues?.value(forKey: varToReplace) as? NSNumber {
				if isCurrency {
					var currencyValue = num.intCurrencyValue()
					if currencyValue?.characters.count == 0 {
						currencyValue = "$0"
					}
					mutableText.replaceCharacters(in: rangeOfFirstMatch, with: currencyValue!)
				}else{
					mutableText.replaceCharacters(in: rangeOfFirstMatch, with: num.intStringValue())
				}
			}else{
				mutableText.replaceCharacters(in: rangeOfFirstMatch, with: "")
			}
			stringToSearch = mutableText.string
			rangeOfFirstMatch = regex.rangeOfFirstMatch(in: stringToSearch, options: .reportCompletion, range: NSMakeRange(0, stringToSearch.characters.count))
		}
		
		let s = mutableText.string as NSString
		let tbRange = s.range(of: "®")
		if tbRange.location != NSNotFound {
			if let currentFont = mutableText.attribute(NSFontAttributeName, at: tbRange.location, effectiveRange: nil) as? UIFont {
				let superscriptFont = UIFont(name: currentFont.fontName, size: currentFont.pointSize - 14)!
				mutableText.setAttributes([NSFontAttributeName: superscriptFont], range: tbRange)
				mutableText.addAttribute(NSBaselineOffsetAttributeName, value: 10, range: tbRange)
			}
		}
		
		return mutableText
	}
	
	func parseView(_ pageView: UIView) {
		if let dv = pageView as? PDFEditorDrawableView {
			dv.keyValues = self.keyValues
		}
		for v in pageView.subviews {
			if self.keyValues != nil {
				if let l = v as? UILabel {
					if let attstr = l.attributedText {
						l.attributedText = self.parseString(attstr)
					}else if let str = l.text {
						l.attributedText = self.parseString(NSAttributedString(string: str, attributes: [ NSFontAttributeName : l.font ]))
					}
					if l.numberOfLines == 0 {
						var r = l.attributedText?.boundingRect(with: CGSize(width: l.frame.width, height: 2000), options: .usesLineFragmentOrigin, context: nil)
						r!.size.height += 50.0
						var f = l.frame
						f.size.height = (r?.size.height)!
						l.frame = f
					}
				}else if let tv = v as? UITextView {
					if let attstr = tv.attributedText {
						tv.attributedText = self.parseString(attstr)
					}else if let str = tv.text {
						tv.attributedText = self.parseString(NSAttributedString(string: str, attributes: [ NSFontAttributeName : tv.font! ]))
					}
					var r = tv.attributedText?.boundingRect(with: CGSize(width: tv.frame.width, height: 2000), options: .usesLineFragmentOrigin, context: nil)
					r!.size.height += 50.0
					var f = tv.frame
					let heightDif = r!.size.height - f.size.height
					f.size.height = (r?.size.height)!
					tv.frame = f
					f = tv.superview!.frame
					f.size.height += heightDif
					tv.superview!.frame = f
				}else if let gv = v as? IMGraphView {
					var a: [NSNumber] = []
					for s in gv.cellIdsForLeftBar.components(separatedBy: ",") {
						if let numberValue = self.keyValues?.value(forKey: s) as? NSNumber {
							a.append(numberValue)
						}
					}
					gv.valuesForLeftBar = a
					a = []
					for s in gv.cellIdsForRightBar.components(separatedBy: ",") {
						if let numberValue = self.keyValues?.value(forKey: s) as? NSNumber {
							a.append(numberValue)
						}
					}
					gv.valuesForRightBar = a
				}
			}
			if v.subviews.count > 0 {
				self.parseView(v)
			}
		}
	}
	
	
	// MARK: -
	
	func scrollPositionForPageIndex(_ index:Int) -> Double {
		return ((self.pageWidth + pageGap) * Double(index)) + self.scrollBuffer + (self.pageWidth * 0.5) - (Double(self.scrollView.frame.width) / 2.0)
	}
	
	func pageIndexForScrollPosition(_ position:Double) -> Int {
		let val = round((position - (self.scrollBuffer - (pageGap/2.0))) / (self.pageWidth + pageGap))
		return Int(val)
	}
	
	// MARK: - UIScrollViewDelegate
	
	func scrollViewDidEndZooming(_ scrollView: UIScrollView, with view: UIView?, atScale scale: CGFloat) {
		if scale == 1.0 {
			var offset = scrollView.contentOffset
			self.currentPage = self.pageIndexForScrollPosition(Double(offset.x))
			offset.x = CGFloat(self.scrollPositionForPageIndex(self.currentPage))
			scrollView.setContentOffset(offset, animated: true)
			self.updatePageNumber()
		}
	}
	
	func viewForZooming(in scrollView: UIScrollView) -> UIView? {
		return self.contentView
	}
	
	func scrollViewWillEndDragging(_ scrollView: UIScrollView, withVelocity velocity: CGPoint, targetContentOffset: UnsafeMutablePointer<CGPoint>) {
		if scrollView.zoomScale > 1.0 {
			return;
		}
		let currentScrollPosition = self.scrollPositionForPageIndex(self.currentPage)
		if abs(Double(targetContentOffset.pointee.x) - currentScrollPosition) < (self.pageWidth + pageGap) / 2.0 {
			targetContentOffset.pointee.x = CGFloat(currentScrollPosition)
		}else{
			if velocity.x > 0 {
				if Int(self.currentPage) < pages.count - 1 {
					currentPage += 1
				}
			}else {
				if self.currentPage > 0 {
					self.currentPage -= 1
				}
			}
			targetContentOffset.pointee.x = CGFloat(self.scrollPositionForPageIndex(self.currentPage))
			self.updatePageNumber()
		}
		self.thumbScrollView.selectItem(at: IndexPath(item: self.currentPage, section: 0), animated: false, scrollPosition: .top)
	}
	
	// MARK: - UICollectionViewDataSource
	
	func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
		return self.pages.count
	}
	
	func numberOfSections(in collectionView: UICollectionView) -> Int {
		return 1
	}
	
	func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
		let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "ThumbCell", for: indexPath)
		if cell.backgroundView == nil {
			cell.backgroundView = UIImageView(image: self.thumbs[(indexPath as NSIndexPath).item])
			cell.backgroundView?.backgroundColor = UIColor.clear
			cell.selectedBackgroundView = UIImageView(image: self.thumbs[(indexPath as NSIndexPath).item])
			cell.selectedBackgroundView?.backgroundColor = UIColor(white: 84.0/255.0, alpha: 1.0)
		}
		
		if let iv = cell.backgroundView as? UIImageView {
			iv.contentMode = .scaleAspectFit
			iv.image = self.thumbs[(indexPath as NSIndexPath).item]
		}else{
			print("UICollectionViewCell is not a UIImageView")
		}
		
		if let siv = cell.selectedBackgroundView as? UIImageView {
			siv.contentMode = .scaleAspectFit
			siv.image = self.thumbs[(indexPath as NSIndexPath).item]
		}
		
		
		return cell
	}
	
	func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
		self.currentPage = (indexPath as NSIndexPath).item
		var rect = self.pages[(indexPath as NSIndexPath).item].frame
		rect.origin.x = CGFloat(self.scrollPositionForPageIndex(self.currentPage))
		self.scrollView.setZoomScale(1.0, animated: true)
		self.scrollView.setContentOffset(CGPoint(x: rect.origin.x, y: 0), animated: true)
		self.updatePageNumber()
	}
	
	// MARK: -
	
	func generatePages () {
		let pdfPageHeight = 3168.0
		var contentHeight = 0.0
		var generatedPages: [UIView] = []
		var page = UIView(frame: CGRect(x: 0.0, y: 0.0, width: 2448.0, height: pdfPageHeight))
		for v in self.pageComponents {
			if v.tag >= 5000 && (self.keyValues?.value(forKey: "N39") as AnyObject).intValue < 4 {
				continue
			}
			if v.tag >= 4567 && !self.showPhased {
				continue
			}
			let componentHeight = Double(v.frame.size.height)
			if page.subviews.count == 0 {
				page.addSubview(v)
			}else{
				if contentHeight + componentHeight < pdfPageHeight && v.tag != 1001
				{
					var r = v.frame
					r.origin.y = CGFloat(contentHeight)
					v.frame = r
					page.addSubview(v)
				}else{
					generatedPages.append(page)
					page = UIView(frame: CGRect(x: 0.0, y: 0.0, width: 2448.0, height: pdfPageHeight))
					contentHeight = 0.0
					page.addSubview(v)
				}
			}
			contentHeight += componentHeight
		}
		generatedPages.append(page)
		for v in self.pages {
			v.removeFromSuperview()
		}
		self.pages = generatedPages
		self.thumbs = []
		for v in self.pages {
			v.clipsToBounds = true
			
			v.layer.borderColor = UIColor(white: 0.6, alpha: 1.0).cgColor
			v.layer.borderWidth = 4.0
			v.backgroundColor = UIColor.white
			
			UIGraphicsBeginImageContext(v.frame.size)
			let context = UIGraphicsGetCurrentContext()!
			v.layer.render(in: context)
			let scale = 155.0 / CGFloat(initialPageHeight)
			context.scaleBy(x: scale, y: scale)
			let image = UIGraphicsGetImageFromCurrentImageContext()
			UIGraphicsEndImageContext()
			
			UIGraphicsBeginImageContext(CGSize(width: 120.0, height: 155.0));
			image?.draw(in: CGRect(x: 0.0, y: 0.0, width: 120.0, height: 155.0))
			let thumbImage = UIGraphicsGetImageFromCurrentImageContext();
			UIGraphicsEndImageContext();
			
			self.thumbs.append(thumbImage!)
			
			self.contentView.addSubview(v)
		}
		self.view.setNeedsLayout()
		self.thumbScrollView.reloadData()
	}
	
	// MARK: - UIViewController

    override func viewDidLoad() {
        super.viewDidLoad()
		self.view.backgroundColor = UIColor(red: 244.0/255.0, green: 244.0/255.0, blue: 244.0/255.0, alpha: 1.0)
		self.thumbScrollView.register(UICollectionViewCell.self, forCellWithReuseIdentifier: "ThumbCell")
		self.contentView.backgroundColor = self.view.backgroundColor
		self.contentView.autoresizesSubviews = false
		for v in self.pageComponents {
			self.parseView(v)
		}
		self.generatePages()
		self.scrollView.addSubview(self.contentView)
		self.thumbScrollView.selectItem(at: IndexPath(item: 0, section: 0), animated: false, scrollPosition: .top)
    }
	
	override func viewDidLayoutSubviews() {
		super.viewDidLayoutSubviews()
		var nextPageX = 0.0
		for v in self.pages {
			v.transform = CGAffineTransform.identity
			let scale = (self.scrollView.frame.height - 40.0) / CGFloat(self.initialPageHeight)
			v.transform = CGAffineTransform(scaleX: scale, y: scale)
			var r = v.frame
			if nextPageX == 0.0 {
				nextPageX = Double(self.scrollView.frame.width - r.width) / 2.0
				self.scrollBuffer = nextPageX
			}
			r.origin.x = CGFloat(nextPageX)
			r.origin.y = 20.0
			self.pageWidth = Double(r.width)
			v.frame = r
			nextPageX += Double(r.width) + pageGap
		}
		self.contentView.frame = CGRect(x: 0.0, y: 0.0, width: CGFloat(nextPageX - pageGap + self.scrollBuffer), height: self.scrollView.frame.height)
		self.scrollView.contentSize = self.contentView.frame.size
		self.scrollView.minimumZoomScale = 1.0
		self.scrollView.maximumZoomScale = 2.0
		self.scrollView.zoomScale = 1.0
		self.updatePageNumber()
	}

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
	
	override init(nibName nibNameOrNil: String?, bundle nibBundleOrNil: Bundle?) {
		self.pageComponents = Bundle.main.loadNibNamed(nibNameOrNil!, owner: nil, options: nil) as! [UIView]
		self.pages = []
		self.contentView = UIView()
		super.init(nibName: nil, bundle: nil)
	}

	required init?(coder aDecoder: NSCoder) {
	    fatalError("init(coder:) has not been implemented")
	}
}
