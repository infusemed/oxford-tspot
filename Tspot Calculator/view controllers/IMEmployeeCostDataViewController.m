//
//  IMEmployeeCostDataViewController.m
//  Tspot Calculator
//
//  Created by Anthony Long on 7/22/14.
//  Copyright (c) 2014 Infuse Medical. All rights reserved.
//

#import "IMEmployeeCostDataViewController.h"
#import "IMSpotlightEconWorkbook.h"

@implementation IMEmployeeCostDataViewController
{
	__weak IBOutlet UILabel *_labelCustomer;
}

- (void) viewWillDisappear:(BOOL)animated
{
	[super viewWillDisappear:animated];
	[self.workbook removeObserver:self forKeyPath:@"customerName"];
}

- (BOOL) isValid
{
	if([self.spreadsheet valueForIdentifier:@"C18"].doubleValue != 1.0)
	{
		UIAlertView *av = [[UIAlertView alloc] initWithTitle:@"Data Validation" message:@"'Total Workforce' must be 100%.  Please adjust '% of Workforce' fields to add up to 100%" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
		[av show];
		return NO;
	}
	return YES;
}

- (void) observeValueForKeyPath:(NSString *)keyPath ofObject:(id)object change:(NSDictionary *)change context:(void *)context
{
	if([object isKindOfClass:[IMSpreadSheetCell class]])
	{
		[super observeValueForKeyPath:keyPath ofObject:object change:change context:context];
		return;
	}
	_labelCustomer.text = self.workbook.customerName;
}

- (void) viewWillAppear:(BOOL)animated
{
	[super viewWillAppear:animated];
	_labelCustomer.text = self.workbook.customerName;
	[self.workbook addObserver:self forKeyPath:@"customerName" options:NSKeyValueObservingOptionNew context:nil];
}

@end