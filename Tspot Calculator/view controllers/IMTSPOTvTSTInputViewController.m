//
//  IMTSPOTvTSTInputViewController.m
//  Tspot Calculator
//
//  Created by Anthony Long on 9/25/14.
//  Copyright (c) 2014 Infuse Medical. All rights reserved.
//

#import "IMTSPOTvTSTInputViewController.h"

@interface IMTSPOTvTSTInputViewController ()

@property (weak, nonatomic) IBOutlet UIButton *buttonStep1;
@property (weak, nonatomic) IBOutlet UIButton *buttonStep2;


@end

@implementation IMTSPOTvTSTInputViewController

#pragma mark - IBActions

- (IBAction)handleButtonStepTouch:(UIButton *)sender
{
	sender.selected = !sender.selected;
	[self.spreadsheet setValue:[NSNumber numberWithBool:self.buttonStep1.selected]
				 forIdentifier:@"B13"];
	[self.spreadsheet setValue:[NSNumber numberWithBool:self.buttonStep2.selected]
				 forIdentifier:@"B14"];
}

#pragma mark - UIViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
	self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
	if (self) {
		// Custom initialization
	}
	return self;
}

- (void)viewDidLoad
{
	[super viewDidLoad];
	self.buttonStep1.selected = [self.spreadsheet valueForIdentifier:@"B13"].boolValue;
	self.buttonStep2.selected = [self.spreadsheet valueForIdentifier:@"B14"].boolValue;
}

- (void)didReceiveMemoryWarning
{
	[super didReceiveMemoryWarning];
	// Dispose of any resources that can be recreated.
}

@end