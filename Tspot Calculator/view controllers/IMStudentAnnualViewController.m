//
//  IMStudentAnnualViewController.m
//  Tspot Calculator
//
//  Created by Anthony Long on 2/6/15.
//  Copyright (c) 2015 Infuse Medical. All rights reserved.
//

#import "IMStudentAnnualViewController.h"

@interface IMStudentAnnualViewController ()

@property (weak, nonatomic) IBOutlet UIButton *buttonSteps;

@end

@implementation IMStudentAnnualViewController

- (IBAction)handleStepTouch:(UIButton *)sender
{
	sender.selected = !sender.selected;
	[self.spreadsheet spreadSheetCellForIdentifier:@"B24"].value = sender.selected ? @1.0 : @2.0;
}


- (void)viewDidLoad {
    [super viewDidLoad];
	self.buttonSteps.selected = [self.spreadsheet valueForIdentifier:@"B24"].integerValue == 1;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
