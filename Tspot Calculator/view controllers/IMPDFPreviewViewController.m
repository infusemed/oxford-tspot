//
//  IMPDFPreviewViewController.m
//  EUS
//
//  Created by Jeff Spaulding on 5/20/15.
//  Copyright (c) 2015 Olympus. All rights reserved.
//

#import "IMPDFPreviewViewController.h"
#import <MessageUI/MessageUI.h>

@interface IMPDFPreviewViewController ()<MFMailComposeViewControllerDelegate>{
    NSString* _filePath;
}

@end

@implementation IMPDFPreviewViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    _toEmailAddress = @"";
}


- (BOOL)shouldShowMenu {
    [self performSelector:@selector(delayedStyleMenuButton) withObject:nil afterDelay:0.01];
    return YES;
}
-(void)delayedStyleMenuButton{
    [self.headerView.buttonMenu setTitle:@"Send" forState:UIControlStateNormal];
    [self.headerView.buttonMenu removeTarget:self.headerView action:@selector(buttonMenuPressed:) forControlEvents:UIControlEventTouchUpInside];
    [self.headerView.buttonMenu addTarget:self action:@selector(buttonMenuPressed:) forControlEvents:UIControlEventTouchUpInside];
}

-(void)buttonMenuPressed:(UIButton*)sender{
    
    NSData * pdfData =[[NSData alloc] initWithContentsOfFile:_filePath];
    
    MFMailComposeViewController *picker = [[MFMailComposeViewController alloc] init];
    picker.mailComposeDelegate = self;
    
    [picker setToRecipients:@[_toEmailAddress]];

    [picker addAttachmentData:pdfData mimeType:@"application/pdf" fileName:[_filePath lastPathComponent]];
    [picker setSubject:[self emailSubject]];

    [self presentViewController:picker animated:YES completion:NULL];
}

-(void)menuButtonPressed:(int)buttonIndex{
    return;
}


- (void)mailComposeController:(MFMailComposeViewController*)controller didFinishWithResult:(MFMailComposeResult)result error:(NSError*)error
{
    [self dismissViewControllerAnimated:YES completion:NULL];
}


-(void)loadFileAtPath:(NSString*)filePath{
    [super loadFileAtPath:filePath];
    _filePath = filePath;
}

@end
