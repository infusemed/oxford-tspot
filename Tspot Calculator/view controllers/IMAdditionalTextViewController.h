//
//  IMAdditionalTextViewController.h
//  Tspot Calculator
//
//  Created by Anthony Long on 6/29/14.
//  Copyright (c) 2014 Infuse Medical. All rights reserved.
//

#import <UIKit/UIKit.h>

@class IMAdditionalTextViewController;

@protocol IMAdditionalTextViewControllerDelegate

- (void) additionalTextViewController:(IMAdditionalTextViewController *)viewController willDismissWithText:(NSString *)text;

@end

@interface IMAdditionalTextViewController : UIViewController

@property (weak, nonatomic) IBOutlet UITextView *textView;
@property (nonatomic) BOOL isAdditionalContent;

@property (nonatomic, unsafe_unretained) id <IMAdditionalTextViewControllerDelegate> delegate;

@end
