//
//  IMPDFPreviewViewController.h
//  EUS
//
//  Created by Jeff Spaulding on 5/20/15.
//  Copyright (c) 2015 Olympus. All rights reserved.
//

#import "IMArcPDFViewController.h"

@interface IMPDFPreviewViewController : IMArcPDFViewController
@property (nonatomic, strong) NSString * toEmailAddress;
@end
