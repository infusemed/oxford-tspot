//
//  IMCalculatorViewController.m
//  Tspot Calculator
//
//  Created by Anthony Long on 6/13/14.
//  Copyright (c) 2014 Infuse Medical. All rights reserved.
//

#import "IMCalculatorViewController.h"
#import "IMBaseViewController.h"
#import "IMSpotlightEconWorkbook.h"
#import "IMAssignNewSessionViewController.h"
#import "IMPopoverBackgroundView.h"
#import "IMGraphViewController.h"
#import "IMSettingsPopoverViewController.h"
#import "IMSpotlightEconPDFViewController.h"
//#import "IMPDFViewController.h"
#import "IMReferenceInfoViewController.h"
#import "IMTSPOTvTSTWorkbook.h"
#import "UIView+IMFrame.h"
#import "IMPDFPreviewViewController.h"
#import "TPKeyboardAvoidingScrollView.h"
#import "Econ_Analysis-Swift.h"

@interface IMCalculatorViewController ()//<IMArcMediaViewerDelegate>

@property (weak, nonatomic) IBOutlet UIView *viewTabs;
@property (weak, nonatomic) IBOutlet UIView *viewHeader;
@property (weak, nonatomic) IBOutlet UIScrollView *scrollView;
@property (weak, nonatomic) IBOutlet UIImageView *ivLogo;
@property (weak, nonatomic) IBOutlet UIButton *buttonEmail;
@property (weak, nonatomic) IBOutlet UIButton *buttonSave;
@property (strong, nonatomic) NSMutableArray *currentControllers;
@property (weak, nonatomic) IMSpreadSheet *resultsSpreadsheet;
@end


@implementation IMCalculatorViewController
{
	BOOL _animatingIn;
	UIPopoverController *_popover;
	NSMutableArray *_tabs;
	NSMutableArray *_forms;
	NSString *_additionalInfo;
}

#pragma mark - show email

-(void)mediaViewerDidClose:(IMArcMediaViewer*)mediaViewer {
	[self dismissViewControllerAnimated:YES completion:nil];
}

- (void) doEmail
{
	PDFEditorViewController *vcPdf;
	if(self.config[@"pdfSettingsKey"] != nil)
	{
		if([self.config[@"settings"][self.config[@"pdfSettingsKey"]] boolValue])
		{
			vcPdf = [[PDFEditorViewController alloc] initWithNibName:self.config[@"pdfView"] bundle:nil];
		}else{
			vcPdf = [[PDFEditorViewController alloc] initWithNibName:self.config[@"pdfViewAlternate"] bundle:nil];
		}
	}else{
		vcPdf = [[PDFEditorViewController alloc] initWithNibName:self.config[@"pdfView"] bundle:nil];
	}
	vcPdf.showPhased = [self.config[@"settings"][@"Phased Implementation"] boolValue];
	self.workbook.additionalInfo = _additionalInfo;
	self.workbook.disclaimer = self.config[@"pdfDisclaimer"];

	vcPdf.keyValues = [self.workbook spreadSheetWithName:self.config[@"pdfSpreadsheet"]];

	[self presentViewController:vcPdf animated:YES completion:nil];
	
}

- (void)mailComposeController:(MFMailComposeViewController*)controller didFinishWithResult:(MFMailComposeResult)result error:(NSError*)error
{
    [self dismissViewControllerAnimated:YES completion:NULL];
}

- (NSAttributedString *) parseStringVars:(NSAttributedString *)stringToParse {
		self.resultsSpreadsheet = [self.workbook spreadSheetWithName:self.config[@"pdfSpreadsheet"]];
		NSError *error = nil;
		NSMutableAttributedString *attString = [stringToParse mutableCopy];
		NSString *string = attString.string;
		NSRegularExpression *superscriptRegex = [NSRegularExpression regularExpressionWithPattern:@"®" options:NSRegularExpressionCaseInsensitive error:&error];
		[superscriptRegex enumerateMatchesInString:string
										   options:0
											 range:NSMakeRange(0, [string length])
										usingBlock:^(NSTextCheckingResult *result, NSMatchingFlags flags, BOOL *stop) {
											NSMutableDictionary *atts = [[attString attributesAtIndex:result.range.location effectiveRange:NULL] mutableCopy];
											UIFont *f = atts[NSFontAttributeName];
											atts[NSFontAttributeName] = [UIFont fontWithName:f.fontName size:f.pointSize-3];
											atts[NSBaselineOffsetAttributeName] = @3.0;
											[attString setAttributes:atts range:result.range];
										}];
		
		superscriptRegex = [NSRegularExpression regularExpressionWithPattern:@"[A-Za-z%](\\d+?)\\." options:NSRegularExpressionCaseInsensitive error:&error];
		[superscriptRegex enumerateMatchesInString:string
										   options:0
											 range:NSMakeRange(0, [string length])
										usingBlock:^(NSTextCheckingResult *result, NSMatchingFlags flags, BOOL *stop) {
											NSMutableDictionary *atts = [[attString attributesAtIndex:[result rangeAtIndex:1].location effectiveRange:NULL] mutableCopy];
											UIFont *f = atts[NSFontAttributeName];
											atts[NSFontAttributeName] = [UIFont fontWithName:f.fontName size:f.pointSize-3];
											atts[NSBaselineOffsetAttributeName] = @3.0;
											[attString setAttributes:atts range:[result rangeAtIndex:1]];
										}];
		
		NSRegularExpression *regex = [NSRegularExpression regularExpressionWithPattern:@"<.*?>" options:NSRegularExpressionCaseInsensitive error:&error];
		
		NSRange rangeOfFirstMatch = [regex rangeOfFirstMatchInString:string options:0 range:NSMakeRange(0, [string length])];
		while(!NSEqualRanges(rangeOfFirstMatch, NSMakeRange(NSNotFound, 0))) {
			NSRange r = NSMakeRange(rangeOfFirstMatch.location + 1, rangeOfFirstMatch.length - 2);
			NSString *theMatch = [string substringWithRange:r];
			NSString *spreadSheetValue = @"";
			
			if([theMatch isEqualToString:@"customer name"])
			{
				spreadSheetValue = [self.resultsSpreadsheet.workbook customerName];
				[attString replaceCharactersInRange:rangeOfFirstMatch withString:spreadSheetValue];
			}else{
				NSString *firstChar = [theMatch substringToIndex:1];
				if([firstChar isEqualToString:@"$"])
				{
					theMatch = [theMatch substringFromIndex:1];
					spreadSheetValue = [self.resultsSpreadsheet valueForIdentifier:theMatch].intCurrencyValue;
				}else if([firstChar isEqualToString:@"%"]){
					theMatch = [theMatch substringFromIndex:1];
					spreadSheetValue = [self.resultsSpreadsheet valueForIdentifier:theMatch].percentStringValue;
				}else{
					spreadSheetValue = [self.resultsSpreadsheet valueForIdentifier:theMatch].intStringValue;
				}
				if(!spreadSheetValue)
				{
					spreadSheetValue = @"";
				}
				[attString replaceCharactersInRange:rangeOfFirstMatch withString:spreadSheetValue];
			}
			
			string = attString.string;
			rangeOfFirstMatch = [regex rangeOfFirstMatchInString:string options:0 range:NSMakeRange(0, [string length])];
		}
		
		
		return attString;
}

#pragma mark - IBActions

- (IBAction) handleEmailButtonTouch:(UIButton *)button
{
	UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Add Additional Text?" message:@"Add additional text to be included in the Executive Summary?" delegate:self cancelButtonTitle:@"No" otherButtonTitles:@"Yes", nil];
	[alert show];
}

- (void) showEditAlert
{
	UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Edit Content?" message:@"Would you like to edit the initial content of the Executive Summary?" delegate:self cancelButtonTitle:@"No" otherButtonTitles:@"Yes", nil];
	[alert show];
}

- (void) alertView:(UIAlertView *)alertView didDismissWithButtonIndex:(NSInteger)buttonIndex
{
	if([alertView.title isEqualToString:@"Save"])
	{
		if(buttonIndex == 0)
		{
			[self dismissViewControllerAnimated:YES completion:NULL];
		}else{
			[self handleSaveSessionTouch:self.buttonSave];
		}
	}else{
		if(buttonIndex == 0)
		{
			if([alertView.title isEqualToString:@"Edit Content?"]){
				NSString *contentName = self.config[@"pdfInitialContent"];
				
				NSMutableAttributedString *mas = [[NSMutableAttributedString alloc] initWithAttributedString:[self contentFormRtf:contentName]];
				
				[mas enumerateAttribute:NSFontAttributeName
								inRange:NSMakeRange(0, mas.length)
								options:NSAttributedStringEnumerationLongestEffectiveRangeNotRequired
							 usingBlock:^(id  _Nullable value, NSRange range, BOOL * _Nonnull stop) {
								 UIFont *smallFont = (UIFont *)value;
								 UIFont *font = [UIFont fontWithName:smallFont.fontName size:smallFont.pointSize * 4.0];
								 [mas addAttribute:NSFontAttributeName value:font range:range];
							 }];
				
				self.workbook.initialContent = mas;
				
				mas = [[NSMutableAttributedString alloc] initWithAttributedString:[self contentFormRtf:@"phased-approach-content-for-pdf"]];
				
				[mas enumerateAttribute:NSFontAttributeName
								inRange:NSMakeRange(0, mas.length)
								options:NSAttributedStringEnumerationLongestEffectiveRangeNotRequired
							 usingBlock:^(id  _Nullable value, NSRange range, BOOL * _Nonnull stop) {
								 UIFont *smallFont = (UIFont *)value;
								 UIFont *font = [UIFont fontWithName:smallFont.fontName size:smallFont.pointSize * 4.0];
								 [mas addAttribute:NSFontAttributeName value:font range:range];
							 }];
				self.workbook.phasedContent = mas;
				
				[self doEmail];
			}else{
				
#ifdef OXFORD_ISUK
				[self doEmail];
#else
				NSString *contentName = self.config[@"pdfInitialContent"];
				if(!contentName || [contentName isEqualToString:@""]) {
					[self doEmail];
				}else{
					[self showEditAlert];
				}
#endif
				
			}
		}else{
			IMAdditionalTextViewController *vc = [[IMAdditionalTextViewController alloc] initWithNibName:@"IMAdditionalTextViewController" bundle:nil];
			vc.delegate = self;
			[self presentViewController:vc animated:YES completion:NULL];
			if([alertView.title isEqualToString:@"Edit Content?"]){
				vc.isAdditionalContent = NO;
				//				let attstr = try NSAttributedString(URL: NSBundle.mainBundle().URLForResource("university", withExtension: "rtf")!, options: [:], documentAttributes: nil)
//				if(self.workbook.initialContent == nil) {
					NSString *contentName = self.config[@"pdfInitialContent"];
					self.workbook.initialContent = [self contentFormRtf:contentName];
//				}
//				if([self.config[@"settings"][@"Phased Implementation"] boolValue]){
//					vc.textView.attributedText = self.workbook.initialContent;
//				}else{
					vc.textView.attributedText = self.workbook.initialContent;
//				}
			}else if(self.workbook.additionalInfo != nil) {
				vc.textView.text = self.workbook.additionalInfo;
			}
		}
	}
}

- (NSAttributedString *) contentFormRtf:(NSString *)fileName
{
	NSAttributedString *attstr = [[NSAttributedString alloc] initWithURL:[[NSBundle mainBundle] URLForResource:fileName withExtension:@"rtf"]
																 options:@{}
													  documentAttributes:nil
																   error:nil];
	return [self parseStringVars:attstr];
}

- (void) additionalTextViewController:(IMAdditionalTextViewController *)viewController willDismissWithText:(NSString *)text
{
	if(viewController.isAdditionalContent) {
		_additionalInfo = text;
		self.workbook.additionalInfo = text;
//		UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Additional Text Has Been Added to the Executive Summary" message:nil delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil];
//		[alert show];
#ifdef OXFORD_ISUK
		[self performSelector:@selector(doEmail) withObject:nil afterDelay:0.5];
#else
		[self showEditAlert];
#endif
		
	}else{
		// add to email
		
		NSMutableAttributedString *mas = [[NSMutableAttributedString alloc] initWithAttributedString:viewController.textView.attributedText];
		
		[mas enumerateAttribute:NSFontAttributeName
						inRange:NSMakeRange(0, mas.length)
						options:NSAttributedStringEnumerationLongestEffectiveRangeNotRequired
					 usingBlock:^(id  _Nullable value, NSRange range, BOOL * _Nonnull stop) {
						 UIFont *smallFont = (UIFont *)value;
						 UIFont *font = [UIFont fontWithName:smallFont.fontName size:smallFont.pointSize * 4.0];
						 [mas addAttribute:NSFontAttributeName value:font range:range];
					 }];
		
		self.workbook.initialContent = mas;
//		[self doEmail];
		[self performSelector:@selector(doEmail) withObject:nil afterDelay:0.5];
	}
}

- (IBAction)handleHomeTouch:(UIButton *)sender
{
	if(!self.workbook.fileName)
	{
		UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Save" message:@"This calculator has not been saved, would you like to save now?" delegate:self cancelButtonTitle:@"No" otherButtonTitles:@"Yes", nil];
		[alert show];
		return;
	}
	[self dismissViewControllerAnimated:YES completion:NULL];
}

- (IBAction)handleRefreshTouch:(UIButton *)sender
{
	Class workbookClass = [self.workbook class];
	self.workbook = [[workbookClass alloc] init];
	[self createTabs];
	[self.scrollView setContentOffset:CGPointZero animated:YES];
}

- (IBAction) handleSettingsTouch:(UIButton *)button
{
	IMSettingsPopoverViewController *vc = [[IMSettingsPopoverViewController alloc]  initWithNibName:@"IMSettingsPopoverViewController" bundle:nil];
	_popover = [[UIPopoverController alloc] initWithContentViewController:vc];
	_popover.delegate = self;
	CGSize size = vc.view.frame.size;
	int i=0;
	if(self.config[@"settings"])
	{
		for(NSString *s in [self.config[@"settings"] allKeys])
		{
			BOOL isSet = [self.config[@"settings"][s] boolValue];
			size.height += 60.0f;
			UIButton *b = [UIButton buttonWithType:UIButtonTypeCustom];
			[b setImage:[UIImage imageNamed:@"toggle-on.png"] forState:UIControlStateSelected];
			[b setImage:[UIImage imageNamed:@"toggle-off.png"] forState:UIControlStateNormal];
			[b setContentHorizontalAlignment:UIControlContentHorizontalAlignmentLeft];
			[b setTitleEdgeInsets:UIEdgeInsetsMake(0.0f, 16.0f, 0.0f, 0.0f)];
			b.selected = isSet;
			b.tag = i;
			[b setTitle:s forState:UIControlStateNormal];
			b.titleLabel.font = [UIFont fontWithName:@"Arial-BoldMT" size:13];
			[b setTitleColor:[UIColor colorWithRed:82.0f/255.0f
											 green:88.0f/255.0f
											  blue:134.0f/255.0f
											 alpha:1.0f] forState:UIControlStateNormal];
			b.frame = CGRectMake(20.0f, 70.0f + (i * 60.0f), 305.0f, 60.0f);
			[b addTarget:self action:@selector(reloadTabs:) forControlEvents:UIControlEventTouchUpInside];
			[vc.view addSubview:b];
			i++;
		}
	}
	
	_popover.popoverContentSize = size;
	_popover.popoverBackgroundViewClass = [IMPopoverBackgroundView class];
	[vc.buttonClear addTarget:self action:@selector(clearSavedSessions:) forControlEvents:UIControlEventTouchUpInside];
	[_popover presentPopoverFromRect:button.frame
							  inView:self.view
			permittedArrowDirections:UIPopoverArrowDirectionUp
							animated:YES];
}

- (IBAction)handleSaveSessionTouch:(UIButton *)sender
{
	IMSaveSessionViewController *svc = [[IMSaveSessionViewController alloc] initWithNibName:@"IMSaveSessionViewController" bundle:nil];
	svc.workbook = self.workbook;
	svc.delegate = self;
	_popover = [[UIPopoverController alloc] initWithContentViewController:svc];
	_popover.delegate = self;
	_popover.popoverContentSize = CGSizeMake(348.0f, 200.0f);
	_popover.popoverBackgroundViewClass = [IMPopoverBackgroundView class];
	[_popover presentPopoverFromRect:sender.frame
							  inView:self.view
			permittedArrowDirections:UIPopoverArrowDirectionUp
							animated:YES];
}

- (void) saveSessionViewController:(IMSaveSessionViewController *)controller shouldSaveWithName:(NSString *)fileName
{
	if(fileName && fileName.length > 0 && self.config[@"saveDirectory"])
	{
		fileName = [fileName stringByAppendingPathExtension:@"tspot"];
		NSURL *directoryURL = [[[[[NSFileManager defaultManager] URLsForDirectory:NSDocumentDirectory inDomains:NSUserDomainMask] lastObject] URLByAppendingPathComponent:self.config[@"saveDirectory"]] URLByAppendingPathComponent:fileName];
		[self.workbook writeToFile:directoryURL.path];
	}
	[_popover dismissPopoverAnimated:YES];
}

- (void) clearSavedSessions:(UIButton *)sender
{
	NSURL *directoryURL = [[[[NSFileManager defaultManager] URLsForDirectory:NSDocumentDirectory inDomains:NSUserDomainMask] lastObject] URLByAppendingPathComponent:self.config[@"saveDirectory"]];
	NSDirectoryEnumerator *dirEnumerator = [[NSFileManager defaultManager] enumeratorAtURL:directoryURL
												  includingPropertiesForKeys:[NSArray arrayWithObjects:NSURLNameKey,
																			  NSURLIsDirectoryKey,nil]
																	 options:NSDirectoryEnumerationSkipsHiddenFiles
																errorHandler:nil];
	
	for (NSURL *theURL in dirEnumerator) {
		NSString *fileName;
		[theURL getResourceValue:&fileName forKey:NSURLNameKey error:NULL];
		NSNumber *isDirectory;
		[theURL getResourceValue:&isDirectory forKey:NSURLIsDirectoryKey error:NULL];
		if ([isDirectory boolValue]==NO)
		{
			[[NSFileManager defaultManager] removeItemAtURL:theURL error:NULL];
		}
	}
	
	[_popover dismissPopoverAnimated:YES];
}

#pragma mark UIPopoverControllerDelegate

- (void)popoverControllerDidDismissPopover:(UIPopoverController *)popoverController
{
	_popover = nil;
}

#pragma mark - UIScrollViewDelegate

- (void) scrollViewDidEndDecelerating:(UIScrollView *)scrollView
{
	for(UIImageView *iv in _tabs)
	{
		iv.highlighted = NO;
	}
	
	NSInteger index = (scrollView.contentOffset.x/1024)-1;
	NSArray *a = self.currentControllers;
	if(a.count == 1)
	{
		index ++;
	}
	index = index>=_tabs.count && index >= 0?_tabs.count-1:index;
	if(index >= 0 && index < _tabs.count)
	{
		UIImageView *iv = (UIImageView *)_tabs[index];
		iv.highlighted = YES;
	}
	if(scrollView.contentOffset.x + 1024.0 == scrollView.contentSize.width && _config[@"pdfView"])
//	if(scrollView.contentOffset.x + 1024.0 == scrollView.contentSize.width && [MFMailComposeViewController canSendMail] && _config[@"pdfView"])
	{
		self.buttonEmail.hidden = NO;
		[UIView animateWithDuration:0.3
						 animations:^{
							 _buttonEmail.frameY = 768.0f - _buttonEmail.frameHeight;
						 }];
	}else{
		if(!self.buttonEmail.hidden)
		{
			[UIView animateWithDuration:0.3
							 animations:^{
								 _buttonEmail.frameY = 768.0f;
							 } completion:^(BOOL finished) {
								 _buttonEmail.hidden = YES;
							 }];
		}
	}
}

- (void) scrollViewWillBeginDragging:(UIScrollView *)scrollView
{
	int currentIndex = (int)(scrollView.contentOffset.x/1024);
	UIViewController *vc = _forms[currentIndex];
	if([vc respondsToSelector:@selector(isValid)])
	{
		BOOL isValid = (BOOL)[vc performSelector:@selector(isValid) withObject:nil];
		if(!isValid)
		{
			scrollView.scrollEnabled = NO;
			scrollView.contentOffset = CGPointMake(currentIndex*1024, 0.0);
			scrollView.scrollEnabled = YES;
		}
	}
}

- (void) reloadTabs:(UIButton *)sender
{
	[_popover dismissPopoverAnimated:YES];
	sender.selected = !sender.selected;
	NSMutableDictionary *d = [self.config mutableCopy];
	NSMutableDictionary *dSettings = [d[@"settings"] mutableCopy];
	NSString *settingsTitle = [sender titleForState:UIControlStateNormal];
	dSettings[settingsTitle] = [NSNumber numberWithBool:sender.selected];
	//for case such as TSPOT v TST where either Annuals or New Hires must be toggled to yes.
	if(!sender.selected && [d.allKeys containsObject:@"settingsInverse"] && [[d[@"settingsInverse"] allKeys] containsObject:settingsTitle])
	{
		dSettings[d[@"settingsInverse"][settingsTitle]] = [NSNumber numberWithBool:YES];
	}
	d[@"settings"] = dSettings;
	
	self.config = d;
	[self createTabs];
}

- (void) createTabs
{
	CGPoint currentOffset = self.scrollView.contentOffset;
	if(_tabs)
	{
		for (UIImageView *iv in _tabs){
			[iv removeFromSuperview];
		}
	}
	if(_forms)
	{
		for(UIViewController *vc in _forms)
		{
			[vc.view removeFromSuperview];
			[vc removeFromParentViewController];
		}
	}
	
	self.currentControllers = [NSMutableArray array];
	NSArray *a = self.config[@"controllers"];
	
	for(NSDictionary *d in a)
	{
		if(!d[@"settingsKey"] || (d[@"settingsKey"] && [self.config[@"settings"][d[@"settingsKey"]] boolValue]))
		{
			[self.currentControllers addObject:d];
		}else{
			//stupid one off
			if([d[@"settingsKey"] isEqualToString:@"Annuals"])
			{
				[[self.workbook spreadSheetWithName:kIMTvTAnnualScreening] setValue:@0.0 forIdentifier:@"B5"];
			}
		}
	}
	
	a = self.currentControllers;
	
	_tabs = [NSMutableArray array];
	_forms = [NSMutableArray array];
	CGFloat widthOfTab = 0.0f;
	if(a.count == 1)
	{
		widthOfTab = 532.0f;
	}else{
		widthOfTab = (self.view.bounds.size.width / a.count)+20.0f;
	}
	int i = 0;
	IMAssignNewSessionViewController *bvc = [[IMAssignNewSessionViewController alloc] initWithNibName:@"IMAssignNewSessionViewController" bundle:nil];
	bvc.workbook = self.workbook;
	bvc.spreadsheet = [self.workbook spreadSheetWithName:kIMAnnualTBScreeningProgramSheet];
	[self addChildViewController:bvc];
	[self.scrollView addSubview:bvc.view];
	[_forms addObject:bvc];
	if(currentOffset.x == 0 && (!self.workbook.customerName || self.workbook.customerName.length == 0))
	{
		[bvc.tfCustomerName becomeFirstResponder];
	}
	
	if(a.count == 1)
	{
		UIImageView *iv = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"chevron.png"] highlightedImage:[UIImage imageNamed:@"chevron-red.png"]];
		iv.frame = ({
			CGRect rect = iv.frame;
			rect.size.width = widthOfTab;
			rect.origin.x = (i * (widthOfTab-20.0)) - 2.0f;
			rect.origin.y = -3.0f;
			rect;
		});
		iv.highlighted = YES;
		[self.viewTabs insertSubview:iv atIndex:0];
		
		UILabel *l = [[UILabel alloc] initWithFrame:CGRectInset(iv.bounds, 20.0f, 00.0f)];
		l.font = [UIFont fontWithName:@"Arial" size:13.0];
		l.textColor = [UIColor whiteColor];
		l.numberOfLines = 2;
		l.textAlignment = NSTextAlignmentCenter;
		l.text = @"Assign New\nSession";
		[iv addSubview:l];
		[_tabs addObject:iv];
	}
	
	for(NSDictionary *d in a)
	{
		UIImageView *iv = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"chevron.png"] highlightedImage:[UIImage imageNamed:@"chevron-red.png"]];
		iv.frame = ({
			CGRect rect = iv.frame;
			rect.size.width = widthOfTab;
			if(a.count == 1)
			{
				rect.origin.x = ((i+1) * (widthOfTab-20.0)) - 2.0f;
			}else{
				rect.origin.x = (i * (widthOfTab-20.0)) - 2.0f;
			}
			rect.origin.y = -3.0f;
			rect;
		});
		[self.viewTabs insertSubview:iv atIndex:0];
		
		UILabel *l = [[UILabel alloc] initWithFrame:CGRectInset(iv.bounds, 20.0f, 00.0f)];
		l.font = [UIFont fontWithName:@"Arial" size:13.0];
		l.textColor = [UIColor whiteColor];
		l.numberOfLines = 2;
		l.textAlignment = NSTextAlignmentCenter;
		l.text = d[@"title"];
		NSRange tbRange = [l.text rangeOfString:@".TB"];
		if(tbRange.location != NSNotFound)
		{
			NSMutableAttributedString *atrString = [[NSMutableAttributedString alloc] initWithString:l.text];
			[atrString setAttributes:@{NSFontAttributeName: [UIFont fontWithName:@"Arial-ItalicMT" size:13.0]} range:tbRange];
			l.attributedText = atrString;
		}
		[iv addSubview:l];
		
		if(d[@"controller"])
		{
			NSString *controllerClassName = d[@"controller"];
			IMBaseViewController *vc;
			if([controllerClassName isEqualToString:@"ImmediateApproachResultsViewController"]) {
				vc = [[ImmediateApproachResultsViewController alloc] initWithNibName:d[@"nib"] bundle:nil];
			}else if([controllerClassName isEqualToString:@"PhasedApproachResultsViewController"]) {
				vc = [[PhasedApproachResultsViewController alloc] initWithNibName:d[@"nib"] bundle:nil];
			}else{
				Class class = NSClassFromString(controllerClassName);
				vc = [[class alloc] initWithNibName:d[@"nib"] bundle:nil];
			}
			vc.workbook = self.workbook;
			vc.spreadsheet = [self.workbook spreadSheetWithName:d[@"spreadsheet"]];
			vc.view.frame = ({
				CGRect rect = vc.view.frame;
				rect.origin.x = (i+1) * 1024.0;
				rect;
			});
			
			[self addChildViewController:vc];
			[self.scrollView addSubview:vc.view];
			[_forms addObject:vc];
			
			if(d[@"info"] && [d[@"info"] length] > 0)
			{
				UIButton *infoButton = [UIButton buttonWithType:UIButtonTypeCustom];
				[infoButton setBackgroundImage:[UIImage imageNamed:@"button-info.png"] forState:UIControlStateNormal];
				infoButton.frame = CGRectMake(25.0f, 535.0f, 43.0f, 43.0f);
				infoButton.tag = i;
				[infoButton addTarget:self action:@selector(handleInfoTouch:) forControlEvents:UIControlEventTouchUpInside];
				[vc.view addSubview:infoButton];
			}
			
			i++;
		}else if(d[@"subcontrollers"]){
			for(NSDictionary *subd in d[@"subcontrollers"])
			{
				if(!subd[@"settingsKey"] || (subd[@"settingsKey"] && [self.config[@"settings"][subd[@"settingsKey"]] boolValue]))
				{
					if([subd[@"controller"] isEqualToString:@"IMBarGraphViewController"]){
						IMBarGraphViewController *vc = [[IMBarGraphViewController alloc] initWithNibName:@"IMBarGraphViewController" bundle:nil];
						vc.workbook = self.workbook;
						vc.spreadsheet = [self.workbook spreadSheetWithName:d[@"spreadsheet"]];
						vc.view.frame = ({
							CGRect rect = vc.view.frame;
							rect.origin.x = (i+1) * 1024.0;
							rect;
						});
						[self addChildViewController:vc];
						[self.scrollView addSubview:vc.view];
						[_forms addObject:vc];
						i++;
					}else{
						Class class = NSClassFromString(subd[@"controller"]);
						IMGraphViewController *vc = [[class alloc] initWithNibName:subd[@"nib"] bundle:nil];
						vc.workbook = self.workbook;
						vc.spreadsheet = [self.workbook spreadSheetWithName:d[@"spreadsheet"]];
						vc.cellIds = subd[@"cells"];
						vc.rightBarText = subd[@"rightChartLabel"];
						if(subd[@"leftChartLabel"])
						{
							vc.leftBarText = subd[@"leftChartLabel"];
						}
						if(subd[@"popupleft"])
						{
							vc.itemsForLeftPopup = subd[@"popupleft"];
						}
						if(subd[@"popupright"])
						{
							vc.itemsForRightPopup = subd[@"popupright"];
						}
						if([subd valueForKey:@"currency"])
						{
							vc.isCurrency = YES;
						}else{
							vc.isCurrency = NO;
						}
						vc.view.frame = ({
							CGRect rect = vc.view.frame;
							rect.origin.x = (i+1) * 1024.0;
							rect;
						});
						
						if(subd[@"legendKeyRed"])
						{
							vc.graphView.legendRed = subd[@"legendKeyRed"];
						}
						
						if(subd[@"legendKeyBlue"])
						{
							vc.graphView.legendBlue = subd[@"legendKeyBlue"];
						}
						
						if(subd[@"legendKeyPurple"])
						{
							vc.graphView.legendPurple = subd[@"legendKeyPurple"];
						}
						
						if([subd valueForKey:@"title"])
						{
							vc.titleLabel.text = [subd valueForKey:@"title"];
						}
						if(subd[@"legends"])
						{
							vc.legends = subd[@"legends"];
						}
						[self addChildViewController:vc];
						[self.scrollView addSubview:vc.view];
						[_forms addObject:vc];
						i++;
					}
				}
			}
		}
		[_tabs addObject:iv];
	}
	
	self.scrollView.contentSize = CGSizeMake((i+1)*1024.0f, 598.0f);
	CGFloat maxOffset = self.scrollView.contentSize.width - 1024.0f;
	if(currentOffset.x > maxOffset)
	{
		currentOffset.x = maxOffset;
	}
	[self.scrollView setContentOffset:currentOffset animated:NO];
	[self scrollViewDidEndDecelerating:self.scrollView];
}


- (void) handleInfoTouch:(UIButton *)sender
{
	NSString *infoText = self.config[@"controllers"][sender.tag][@"info"];
	IMReferenceInfoViewController *vc = [[IMReferenceInfoViewController alloc] initWithNibName:@"IMReferenceInfoViewController" bundle:nil info:infoText];
	[self presentViewController:vc animated:YES completion:NULL];
}


#pragma mark - UIViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        _animatingIn = YES;
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    [self createTabs];
	self.buttonEmail.hidden = YES;
	self.buttonEmail.frameY = 768.0f;
//	_buttonEmail.frameY = 768.0f - _buttonEmail.frameHeight;
	NSString *logoName = [self.config objectForKey:@"logo"];
	if(logoName)
	{
		self.ivLogo.image = [UIImage imageNamed:logoName];
	}
	self.viewHeader.layer.shadowColor = [UIColor colorWithRed:64.0f/255.0f
														green:60.0f/255.0f
														 blue:97.0f/255.0f
														alpha:0.7f].CGColor;
	self.viewHeader.layer.shadowOpacity = 0.6f;
	self.viewHeader.layer.shadowRadius = 4.0f;
	self.viewHeader.layer.shadowOffset = CGSizeMake(4.0f, 4.0f);
	self.scrollView.keyboardDismissMode = UIScrollViewKeyboardDismissModeOnDrag;
}


//#pragma mark - IMArcMediaViewerDelegate
//
//-(void)mediaViewerDidClose:(IMArcMediaViewer*)mediaViewer{
//    [mediaViewer dismissViewControllerAnimated:YES completion:^{
//        
//    }];
//}

@end
