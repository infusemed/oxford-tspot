//
//  IMPhasedApproachResultsViewController.swift
//  Tspot Calculator
//
//  Created by Anthony Long on 6/14/16.
//  Copyright © 2016 Infuse Medical. All rights reserved.
//

import UIKit

protocol OxfordText {
	
}

extension OxfordText where Self: UILabel {
	
	var oxfordText: String? {
		get {
			return self.text
//			return ""
		}
		set (newText) {
			guard let newText = newText else {
				self.text = ""
				return
			}
			let s = newText as NSString
			let tbRange = s.range(of: ".TB")
			
			if tbRange.location != NSNotFound {
				let mutAttStr = NSMutableAttributedString(string: newText, attributes: [NSFontAttributeName : self.font!])
				var currentPointSize = self.font!.pointSize
				if let italicFont = UIFont(name: "Arial-ItalicMT", size: currentPointSize) {
					mutAttStr.addAttribute(NSFontAttributeName, value: italicFont, range: tbRange)
					while currentPointSize > 8 && mutAttStr.boundingRect(with: CGSize(width: 1000, height: 40), options: .usesLineFragmentOrigin, context: nil).size.width > self.frame.size.width {
						currentPointSize -= 1
						mutAttStr.addAttribute(NSFontAttributeName, value: UIFont(name: "ArialMT", size: currentPointSize)!, range: NSMakeRange(0, s.length))
						mutAttStr.addAttribute(NSFontAttributeName, value: UIFont(name: "Arial-ItalicMT", size: currentPointSize)!, range: tbRange)
					}
					
					self.attributedText = mutAttStr
				}else{
					self.text = newText
				}
			}else{
				self.text = newText
			}
		}
	}
}

extension OxfordText where Self: UITextField {
	
	var oxfordText: String? {
		get {
			return self.text
			//			return ""
		}
		set (newText) {
			guard let newText = newText else {
				self.text = ""
				return
			}
			let s = newText as NSString
			let tbRange = s.range(of: ".TB")
			
			if tbRange.location != NSNotFound {
				let mutAttStr = NSMutableAttributedString(string: newText, attributes: [NSFontAttributeName : self.font!])
				var currentPointSize = self.font!.pointSize
				if let italicFont = UIFont(name: "Arial-ItalicMT", size: currentPointSize) {
					mutAttStr.addAttribute(NSFontAttributeName, value: italicFont, range: tbRange)
					while currentPointSize > 8 && mutAttStr.boundingRect(with: CGSize(width: 1000, height: 40), options: .usesLineFragmentOrigin, context: nil).size.width > self.frame.size.width {
						currentPointSize -= 1
						mutAttStr.addAttribute(NSFontAttributeName, value: UIFont(name: "ArialMT", size: currentPointSize)!, range: NSMakeRange(0, s.length))
						mutAttStr.addAttribute(NSFontAttributeName, value: UIFont(name: "Arial-ItalicMT", size: currentPointSize)!, range: tbRange)
					}
					
					self.attributedText = mutAttStr
				}else{
					self.text = newText
				}
			}else{
				self.text = newText
			}
		}
	}
}

extension UITextField: OxfordText {}
extension UILabel: OxfordText {}


class PhasedApproachResultsViewController: IMBaseViewController, UITableViewDelegate, UITableViewDataSource {

	@IBOutlet weak var tableView: UITableView!
	@IBOutlet weak var buttonAddYear: UIButton!
//	@IBOutlet weak var buttonAddPopulation: UIButton!
	@IBOutlet weak var buttonEdit: UIButton!
	@IBOutlet var tfYearLables: [UILabel]!
	@IBOutlet var tfTestsLabels: [UILabel]!
	@IBOutlet var tfPriceLabels: [UITextField]!
	@IBOutlet var tfBudgetLabels: [UILabel]!
	@IBOutlet var deleteButtons: [UIButton]!
	@IBOutlet weak var viewTableFooter: UIView!
	
	
	@IBOutlet weak var buttonLeft: UIButton!
	@IBOutlet weak var buttonRight: UIButton!
	
	
	let yearColumns = ["P", "Q", "R", "S", "T", "U", "V", "W", "X", "Y"]
	var currentFirstColumn = -1 {
		didSet {
			if currentFirstColumn < -1 {
				currentFirstColumn = -1
			}
		}
	}
	let cellId = "PopulationCell"
	var numOfRows = 0 {
		didSet {
			if oldValue != numOfRows {
				self.spreadsheet.spreadSheetCell(forIdentifier: "M39").value = NSNumber(value: numOfRows as Int)
			}
		}
	}
	
	var numOfColumns = 0 {
		didSet {
			self.spreadsheet.spreadSheetCell(forIdentifier: "N39").value = NSNumber(value: numOfColumns as Int)
		}
	}
	
	func setLabelText () {
		for i in 0..<self.tfYearLables.count {
			self.tfPriceLabels[i].tag = currentFirstColumn + 1 + i
			self.tfYearLables[i].isHidden = i >= self.numOfColumns
			self.tfPriceLabels[i].isHidden = self.tfYearLables[i].isHidden
			self.tfBudgetLabels[i].isHidden = self.tfYearLables[i].isHidden
			self.tfTestsLabels[i].isHidden = self.tfYearLables[i].isHidden
			self.tfYearLables[i].text = "Year \(currentFirstColumn+2+i)"
			let column = yearColumns[currentFirstColumn + 1 + i]
			let val = self.spreadsheet.spreadSheetCell(forIdentifier: "\(column)55").value
			self.tfTestsLabels[i].text = val?.intStringValue()
			if let testVal = self.spreadsheet.spreadSheetCell(forIdentifier: "\(column)56").value {
				self.tfPriceLabels[i].text = testVal.currencyValue()
				if let budgetVal = self.spreadsheet.spreadSheetCell(forIdentifier: "\(column)57").value {
					if budgetVal.doubleValue <= 0.0 {
						self.tfBudgetLabels[i].text = "$0"
					}else{
						self.tfBudgetLabels[i].text = budgetVal.currencyValue()
					}
				}
			}else{
				self.tfPriceLabels[i].text = ""
				self.tfBudgetLabels[i].text = ""
			}
			self.deleteButtons[i].isHidden = i >= self.numOfColumns || self.buttonAddYear.isHidden
		}
	}
	
	func moveFields (_ direction: PhasedApproachMultiYearCellMoveDirection) {
		self.buttonLeft.isHidden = self.currentFirstColumn == -1
//		self.buttonRight.hidden = self.currentFirstColumn >= numOfColumns - 4
		for cell in (self.tableView.visibleCells as! [PhasedApproachMultiYearTableViewCell]) {
			cell.moveFields(direction)
		}
		self.setLabelText()
		self.tableView.perform(#selector(UITableView.reloadData), with: nil, afterDelay: 0.5)
	}
	
	@IBAction func handleYearMoveTouch (_ btn:UIButton) {
		if btn == self.buttonRight && self.currentFirstColumn >= numOfColumns - 4 {
			return
		}
		self.currentFirstColumn += btn == self.buttonLeft ? -1 : 1
		self.moveFields(btn == self.buttonLeft ? .right : .left)
	}
	
	@IBAction func togleEdit (_ btn:UIButton?) {
		self.buttonEdit.isSelected = !self.buttonEdit.isSelected
		self.buttonAddYear.isHidden = !self.buttonEdit.isSelected
		self.setLabelText()
	}
	
	@IBAction func addRow (_ btn:UIButton?) {
		numOfRows += 1
		let indexPath = IndexPath(row: numOfRows-1, section: 0)
		self.tableView.insertRows(at: [indexPath], with: .bottom)
		self.tableView.scrollToRow(at: indexPath, at: .bottom, animated: true)
	}
	
	@IBAction func handleDeleteTouch (_ btn:UIButton) {
		self.deleteYearAtIndex(btn.tag + self.currentFirstColumn + 1)
	}
	
	func deleteRowAtIndex (_ index: Int) {
		numOfRows -= 1
		self.spreadsheet.spreadSheetCell(forIdentifier: "P\(self.numOfRows)").value = NSNumber(value: 0 as Int)
		self.spreadsheet.spreadSheetCell(forIdentifier: "O\(self.numOfRows)").textValue = ""
		let indexPath = IndexPath(row: numOfRows-2, section: 0)
		self.tableView.deleteRows(at: [indexPath], with: .bottom)
		self.tableView.scrollToRow(at: indexPath, at: .bottom, animated: true)
	}
	
	@IBAction func addYear (_ btn:UIButton?) {
		if numOfColumns >= 6 {
			return
		}
		self.numOfColumns += 1
		self.currentFirstColumn = self.numOfColumns - 4
		if self.numOfColumns > 3 {
			self.tableView.reloadData()
			self.moveFields(.left)
		}else{
			self.setLabelText()
			self.tableView.reloadData()
		}
	}
	
	func deleteYearAtIndex (_ index: Int) {
		numOfColumns -= 1
		if self.currentFirstColumn > -1 {
			self.currentFirstColumn -= 1
		}
		for i in index..<yearColumns.count {
			let col = yearColumns[i]
			for row in 40..<55 {
				if col == yearColumns.last {
					if row != 44 && row != 40 {
						self.spreadsheet.spreadSheetCell(forIdentifier: "\(col)\(row)").value = NSNumber(value: 0.0 as Double)
					}
					continue
				}
				self.spreadsheet.spreadSheetCell(forIdentifier: "\(col)\(row)").value = self.spreadsheet.spreadSheetCell(forIdentifier: "\(yearColumns[i+1])\(row)").value
			}
		}
		self.tableView.reloadData()
		self.setLabelText()
	}
	
	override func observeValue(forKeyPath keyPath: String?, of object: Any?, change: [NSKeyValueChangeKey : Any]?, context: UnsafeMutableRawPointer?) {
		self.numOfRows = self.spreadsheet.spreadSheetCell(forIdentifier: "M39").value.intValue
		self.tableView.reloadData()
		self.setLabelText()
		super.observeValue(forKeyPath: keyPath, of: object, change: change, context: context)
	}
	
	override func textFieldDidEndEditing(_ textField: UITextField) {
		if textField.superview!.tag == 10 { // is tfPopulationCounts
			if let intVal = Int(textField.text!) {
				self.spreadsheet.spreadSheetCell(forIdentifier: "\(yearColumns[textField.tag])\(textField.superview!.superview!.tag+40)").value = NSNumber(value: intVal as Int)
			}
		}else if self.tfPriceLabels.contains(textField) {
			let s = textField.text!.replacingOccurrences(of: "$", with: "")
			self.spreadsheet.spreadSheetCell(forIdentifier: "\(self.yearColumns[textField.tag])56").value = NSNumber(value: Double(s)! as Double)
		}else{
			self.spreadsheet.spreadSheetCell(forIdentifier: "O\(textField.tag+40)").textValue = textField.text
		}
		self.setLabelText()
	}
	
	override func textFieldShouldReturn(_ textField: UITextField) -> Bool {
		textField.resignFirstResponder()
		return true
	}
	
	func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
		return numOfRows
	}
	
	func numberOfSections(in tableView: UITableView) -> Int {
		return 1
	}
	
	func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
		let cell = tableView.dequeueReusableCell(withIdentifier: cellId, for: indexPath) as! PhasedApproachMultiYearTableViewCell
		let rowToUse = (indexPath as NSIndexPath).row
		let s = self.spreadsheet.spreadSheetCell(forIdentifier: "M\(rowToUse+40)").textValue
		
		cell.tfPopulationName.oxfordText = s
		
		for i in 0..<5 {
			let tf = cell.tfPopulationCounts[i]
			let colIndex = i + currentFirstColumn
			if colIndex < 0 {
				tf.text = ""
			}else{
				tf.isHidden = colIndex >= self.numOfColumns && self.numOfColumns <= 3
				if let value = self.spreadsheet.spreadSheetCell(forIdentifier: "\(yearColumns[colIndex])\(rowToUse+40)").value {
					tf.text = value.intStringValue()
				}else{
					tf.text = ""
				}
			}
			tf.tag = colIndex
			tf.delegate = self
			tf.superview!.tag = 10
		}
//		cell.tfPopulationName.enabled = indexPath.row > 5
		cell.tfPopulationName.tag = rowToUse
		cell.tfPopulationName.superview!.tag = rowToUse
		cell.tfPopulationName.delegate = self
		return cell
	}
	
	func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
		cell.backgroundColor = UIColor.clear
	}
	
	override func mapFields() {
		self.tfYearLables.sort { (l, l2) -> Bool in
			return l.frame.origin.x < l2.frame.origin.x
		}
		self.tfBudgetLabels.sort { (l, l2) -> Bool in
			return l.frame.origin.x < l2.frame.origin.x
		}
		self.tfTestsLabels.sort { (l, l2) -> Bool in
			return l.frame.origin.x < l2.frame.origin.x
		}
		self.tfPriceLabels.sort { (l, l2) -> Bool in
			return l.frame.origin.x < l2.frame.origin.x
		}
		for col in yearColumns {
			self.spreadsheet.spreadSheetCell(forIdentifier: "\(col)55").addObserver(self, forKeyPath: "value", options: .new, context: nil)
		}
		self.spreadsheet.spreadSheetCell(forIdentifier: "M39").addObserver(self, forKeyPath: "value", options: .new, context: nil)
		for i in 40..<57 {
			self.spreadsheet.spreadSheetCell(forIdentifier: "N\(i)").addObserver(self, forKeyPath: "value", options: .new, context: nil)
		}
		super.mapFields()
	}
	
	override func clearMappings () {
		for col in yearColumns {
			self.spreadsheet.spreadSheetCell(forIdentifier: "\(col)55").removeObserver(self, forKeyPath: "value")
		}
		self.spreadsheet.spreadSheetCell(forIdentifier: "M39").removeObserver(self, forKeyPath: "value")
		for i in 40..<57 {
			self.spreadsheet.spreadSheetCell(forIdentifier: "N\(i)").removeObserver(self, forKeyPath: "value")

		}
		super.clearMappings()
	}
	
	override func viewDidLoad () {
		super.viewDidLoad()
		self.numOfRows = self.spreadsheet.spreadSheetCell(forIdentifier: "M39").value!.intValue
		self.numOfColumns = self.spreadsheet.spreadSheetCell(forIdentifier: "N39").value!.intValue
		self.buttonLeft.isHidden = self.currentFirstColumn == -1
		self.tableView.register(UINib(nibName: "PhasedApproachMultiYearTableViewCell", bundle: nil), forCellReuseIdentifier: cellId)
		self.buttonAddYear.isHidden = true
		self.tableView.tableFooterView = self.viewTableFooter

//		self.buttonAddPopulation.hidden = true
		self.setLabelText()
	}

}
