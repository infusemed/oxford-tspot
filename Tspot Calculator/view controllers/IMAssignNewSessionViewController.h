//
//  IMAssignNewSessionViewController.h
//  Tspot Calculator
//
//  Created by Anthony Long on 6/21/14.
//  Copyright (c) 2014 Infuse Medical. All rights reserved.
//

#import "IMBaseViewController.h"

@interface IMAssignNewSessionViewController : IMBaseViewController <UITextFieldDelegate>

@property (weak, nonatomic) IBOutlet UITextField *tfCustomerName;

@end