//
//  IMBarGraphViewController.swift
//  Tspot Calculator
//
//  Created by Anthony Long on 9/19/16.
//  Copyright © 2016 Infuse Medical. All rights reserved.
//

import UIKit

class IMBarGraphViewController: UIViewController {
	
	weak var spreadsheet: IMSpreadSheet?
	weak var workbook: IMWorkbook?
	
	//711, 455
	
	@IBOutlet var barViews: [UIView]!
	@IBOutlet var barBlueViews: [UIView]!
	@IBOutlet var barValueLabels: [UILabel]!
	
	override func observeValue(forKeyPath keyPath: String?, of object: Any?, change: [NSKeyValueChangeKey : Any]?, context: UnsafeMutableRawPointer?) {
		self.layoutGraph()
	}
	
	func layoutGraph () {
		let cellNames = ["B3", "B4", "C4"]
		let blueCellNames = ["H6", "H19"]
		var maxValue = 0.0
		if let spreadsheet = self.spreadsheet {
			for cellName in cellNames {
				if let value = spreadsheet.value(forIdentifier: cellName) {
					maxValue = Double.maximum(maxValue, value.doubleValue)
				}
			}
			
			for (index, cellName) in cellNames.enumerated() {
				if let value = spreadsheet.value(forIdentifier: cellName) {
					barValueLabels[index].text = value.intStringValue()
					//184, 344 -- 144
					let barHeight = 344.0 * value.doubleValue / maxValue
					if barHeight.isNaN {
						barValueLabels[index].text = ""
						barViews[index].frame.size.height = 0.0
					}else{
						barValueLabels[index].frame.origin.y = CGFloat(110 + (344.0 - barHeight))
						barViews[index].frame.origin.y = barValueLabels[index].frame.origin.y + 36.0
						barViews[index].frame.size.height = CGFloat(barHeight)
					}
				}else{
					barValueLabels[index].text = ""
					barViews[index].frame.size.height = 0.0
				}
			}
			
			for (index, cellName) in blueCellNames.enumerated() {
				if let value = spreadsheet.value(forIdentifier: cellName) {
					let barHeight = 344.0 * value.doubleValue / maxValue
					if barHeight.isNaN {
						barBlueViews[index].frame.size.height = 0.0
					}else{
						barBlueViews[index].frame.origin.y = CGFloat(490.0 - barHeight)
						barBlueViews[index].frame.size.height = CGFloat(barHeight)
					}
				}else{
					barBlueViews[index].frame.size.height = 0.0
				}
			}
		}
	}

    override func viewDidLoad() {
        super.viewDidLoad()
		let cellNames = ["B3", "B4", "C4"]
		if let spreadsheet = self.spreadsheet {
			for cellName in cellNames {
				let cell:IMSpreadSheetCell = spreadsheet.spreadSheetCell(forIdentifier: cellName)
				cell.addObserver(self, forKeyPath: "value", options: .new, context: nil)
			}
		}
		self.layoutGraph()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
