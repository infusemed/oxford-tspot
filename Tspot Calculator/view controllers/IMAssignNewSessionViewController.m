//
//  IMAssignNewSessionViewController.m
//  Tspot Calculator
//
//  Created by Anthony Long on 6/21/14.
//  Copyright (c) 2014 Infuse Medical. All rights reserved.
//

#import "IMAssignNewSessionViewController.h"

@interface IMAssignNewSessionViewController ()



@end

@implementation IMAssignNewSessionViewController

- (void) textFieldDidEndEditing:(UITextField *)textField
{
	self.workbook.customerName = textField.text;
}

-(BOOL) textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
	NSString *s = textField.text;
	s = [s stringByReplacingCharactersInRange:range withString:string];
	self.workbook.customerName = textField.text;
	return YES;
}

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    self.tfCustomerName.text = self.workbook.customerName;
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
