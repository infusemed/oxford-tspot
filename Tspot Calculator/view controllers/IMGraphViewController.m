//
//  IMGraphViewController.m
//  Tspot Calculator
//
//  Created by Anthony Long on 6/24/14.
//  Copyright (c) 2014 Infuse Medical. All rights reserved.
//

#import "IMGraphViewController.h"
#import "UIView+IMFrame.h"
#import "IMPopoverBackgroundView.h"

@interface IMGraphViewController () <UITableViewDataSource, UITableViewDelegate>

@end

@implementation IMGraphViewController
{
	BOOL _cellsWatched;
	BOOL _isPieChart;
	NSArray *_pieColors;
	UIImageView *_infoLeft, *_infoRight;
}


- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
	return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
	NSArray *a = [self.legends componentsSeparatedByString:@","];
	return a.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
	UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"LegendListCell" forIndexPath:indexPath];
	
	UILabel *l = (UILabel *)[cell.contentView viewWithTag:11];
	l.text = [self.legends componentsSeparatedByString:@","][indexPath.row];
	
	UIView *v = [cell.contentView viewWithTag:10];
	v.backgroundColor = _pieColors[indexPath.row];
	cell.backgroundColor = [UIColor clearColor];
	cell.contentView.backgroundColor = [UIColor clearColor];
	
	return cell;
}

- (void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath
{
	cell.backgroundColor = [UIColor clearColor];
	cell.contentView.backgroundColor = [UIColor clearColor];
}

- (UIImage *)graphImage
{
	UIGraphicsBeginImageContext(self.graphView.bounds.size);
	[self.graphView drawViewHierarchyInRect:self.graphView.bounds afterScreenUpdates:NO];
	UIImage *img = UIGraphicsGetImageFromCurrentImageContext();
	UIGraphicsEndImageContext();
	
	return img;
}

- (void) setValuesForPieChart
{
	NSArray *a = [self.cellIds componentsSeparatedByString:@","];
	if(a.count <= 0)
	{
		return;
	}
	
	[a enumerateObjectsUsingBlock:^(NSString *cellId, NSUInteger idx, BOOL *stop) {
		NSNumber *val = [self.spreadsheet valueForIdentifier:cellId];
		if(!val)
		{
			val = [NSNumber numberWithDouble:0.0];
		}
		[self.pieChart setValue:val forKey:[NSString stringWithFormat:@"value%lu", (unsigned long)idx+1]];
	}];
	[self.pieChart setNeedsDisplay];
}

- (void) setValues
{
	if(_isPieChart)
	{
		[self setValuesForPieChart];
		return;
	}
	NSArray *a = [self.cellIds componentsSeparatedByString:@","];
	if(a.count <= 0)
	{
		return;
	}
	NSMutableArray *_leftValues = [NSMutableArray array];
	for(int i=0;i<a.count/2;i++)
	{
		NSNumber *val = [self.spreadsheet valueForIdentifier:a[i]];
		if(!val)
		{
			val = [NSNumber numberWithDouble:0.0];
		}
		[_leftValues addObject:val];
	}
	NSMutableArray *_rightValues = [NSMutableArray array];
	for(NSInteger i=a.count/2;i<a.count;i++)
	{
		NSNumber *val = [self.spreadsheet valueForIdentifier:a[i]];
		if(!val)
		{
			val = [NSNumber numberWithDouble:0.0];
		}
		[_rightValues addObject:val];
	}
	
	self.graphView.valuesForLeftBar = _leftValues;
	self.graphView.valuesForRightBar = _rightValues;
	self.graphView.isCurrencyFormat = self.isCurrency;
	[self performSelector:@selector(adjustInfoButtons) withObject:nil afterDelay:0.5];
}

- (void) adjustInfoButtons {
	if (self.itemsForLeftPopup) {
		if (!_infoLeft) {
			UIImage *infoImage = [UIImage imageNamed:@"button-info"];
			[self.graphView.leftBar addGestureRecognizer:[[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(handleBarTouch:)]];
			UIImageView *iv = [[UIImageView alloc] initWithImage:infoImage];
			iv.frameX = 107.0;
			[self.graphView.leftBar addSubview:iv];
			self.graphView.leftBar.userInteractionEnabled = YES;
			_infoLeft = iv;
		}
		CGRect r = self.graphView.labelMaxValLeft.frame;
		_infoLeft.frameY = r.origin.y + 25;
	}
	if (self.itemsForRightPopup) {
		if (!_infoRight) {
			UIImage *infoImage = [UIImage imageNamed:@"button-info"];
			[self.graphView.rightBar addGestureRecognizer:[[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(handleBarTouch:)]];
			UIImageView *iv = [[UIImageView alloc] initWithImage:infoImage];
			iv.frameX = 107.0;
			[self.graphView.rightBar addSubview:iv];
			self.graphView.rightBar.userInteractionEnabled = YES;
			_infoRight = iv;
		}
		CGRect r = self.graphView.labelMaxValRight.frame;
		_infoRight.frameY = r.origin.y + 25;
	}
}

//1422 805

- (void) handleBarTouch:(UITapGestureRecognizer *)gesture
{
	BOOL isLeft = gesture.view == _infoLeft.superview;
	NSArray *a =  isLeft? self.itemsForLeftPopup : self.itemsForRightPopup;
	UIPopoverArrowDirection ad = isLeft ? UIPopoverArrowDirectionLeft : UIPopoverArrowDirectionRight;
	UIViewController *vc = [[UIViewController alloc] init];
	vc.modalPresentationStyle = UIModalPresentationPopover;
	UIImageView *iv = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"popover_header"]];
	iv.frameWidth = 320.0;
	[vc.view addSubview:iv];
	UILabel *headerLabel = [[UILabel alloc] initWithFrame:CGRectMake(32.0f, 8.0f, 300.0f, 37.0f)];
	headerLabel.font = [UIFont fontWithName:@"ArialMT" size:25.0];
	headerLabel.textColor = [UIColor whiteColor];
	headerLabel.text = @"Calculation Info";
	[vc.view addSubview:headerLabel];
	UIFont *font = [UIFont fontWithName:@"ArialMT" size:16.0];
	UIFont *boldFont = [UIFont fontWithName:@"Arial-BoldMT" size:16.0];
	CGFloat labelY = 63.0f;
	for (NSString *s in a) {
		UILabel *l = [[UILabel alloc] init];
		l.lineBreakMode = NSLineBreakByWordWrapping;
		l.numberOfLines = 0;
		l.font = font;
		l.textColor = [UIColor blackColor];
		CGRect r = [s boundingRectWithSize:CGSizeMake(300.0, 500.0)
								   options:NSStringDrawingUsesLineFragmentOrigin
								attributes:@{NSFontAttributeName : font}
								   context:nil];
		l.frame = CGRectMake(10.0f, labelY, 300.0f, r.size.height);
		NSMutableAttributedString *mas = [[NSMutableAttributedString alloc] initWithString:s attributes:@{NSFontAttributeName : font }];
		NSInteger index = [s rangeOfString:@"\n"].location;
		if (index != NSNotFound){
			[mas setAttributes:@{ NSFontAttributeName : boldFont } range:NSMakeRange(0, index)];
		}
		l.attributedText = mas;
		labelY += 20.0 + r.size.height;
		[vc.view addSubview:l];
	}
	[self presentViewController:vc animated:YES completion:nil];
	vc.popoverPresentationController.permittedArrowDirections = ad;
	vc.popoverPresentationController.sourceView = self.view;
	vc.popoverPresentationController.popoverBackgroundViewClass = [IMPopoverBackgroundView class];
	vc.preferredContentSize = CGSizeMake(320.0f, labelY);
	UIView *v = isLeft ? _infoLeft : _infoRight;
	vc.popoverPresentationController.sourceRect = [self.view convertRect:v.frame fromView:v.superview];
}

- (void) observeValueForKeyPath:(NSString *)keyPath ofObject:(id)object change:(NSDictionary *)change context:(void *)context
{
	[self setValues];
}

- (void) breakPoint:(UITapGestureRecognizer *) gesture
{
	NSLog(@"break");
}

- (void) viewDidLoad
{
	NSArray *a = [self.cellIds componentsSeparatedByString:@","];
	
	if(!_cellsWatched)
	{
		for(NSString *s in a)
		{
			if(s.length > 1)
			{
				IMSpreadSheetCell *cell = [self.spreadsheet spreadSheetCellForIdentifier:s];
				[cell addObserver:self forKeyPath:@"value" options:NSKeyValueObservingOptionNew context:nil];
			}
		}
		_cellsWatched = YES;
	}
	
	if(!_isPieChart)
	{
		self.graphView = [[IMGraphView alloc] initWithFrame:CGRectMake(155.0f, 103.0f, 711.0f, 455.0f)];
		[self setValues];
		[self.view addSubview:self.graphView];
		self.graphView.labelLeft.text = self.leftBarText;
		NSRange tbRange = [self.rightBarText rangeOfString:@".TB"];
		if(tbRange.location == NSNotFound)
		{
			self.graphView.labelRight.text = self.rightBarText;
		}else{
			NSMutableAttributedString *atrString = [[NSMutableAttributedString alloc] initWithString:self.rightBarText];
			UIFont *italicFont = [UIFont fontWithName:@"Arial-ItalicMT" size:18.0];
			NSDictionary *attr = @{NSFontAttributeName:italicFont};
			[atrString setAttributes:attr range:tbRange];
			self.graphView.labelRight.attributedText = atrString;
		}
		
		[self.graphView addGestureRecognizer:[[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(breakPoint:)]];
	}else{
		_pieColors = @[[UIColor colorWithRed:213.0f/255.0f
										green:31.0f/255.0f
										 blue:42.0f/255.0f
										alpha:1.0f]
						,[UIColor colorWithRed:37.0f/255.0f
										green:144.0f/255.0f
										 blue:252.0f/255.0f
										alpha:1.0f]
						  ,[UIColor colorWithRed:184.0f/255.0f
										green:185.0f/255.0f
										 blue:211.0f/255.0f
										alpha:1.0f]
							,[UIColor colorWithRed:89.0f/255.0f
										green:89.0f/255.0f
										 blue:160.0f/255.0f
										alpha:1.0f]
							  ,[UIColor colorWithRed:122.0f/255.0f
										green:122.0f/255.0f
										 blue:179.0f/255.0f
												alpha:1.0f]];
		[self setValues];
	}
	
	if(self.tableView)
	{
		[self.tableView registerNib:[UINib nibWithNibName:@"IMLegendListCell" bundle:[NSBundle mainBundle]] forCellReuseIdentifier:@"LegendListCell"];
	}
}

- (instancetype)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
	self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
	if(self)
	{
		self.leftBarText = @"Current Practice";
		_isPieChart = [nibNameOrNil isEqualToString:@"IMPieChartViewController"];
	}
	
	return self;
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void) dealloc
{
	NSArray *a = [self.cellIds componentsSeparatedByString:@","];
	for(NSString *s in a)
	{
		if(s.length > 1)
		{
			IMSpreadSheetCell *cell = [self.spreadsheet spreadSheetCellForIdentifier:s];
			[cell removeObserver:self forKeyPath:@"value"];
		}
	}
}

@end
