//
//  IMMainViewController.h
//  Tspot Calculator
//
//  Created by Anthony Long on 6/9/14.
//  Copyright (c) 2014 Infuse Medical. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "IMSessiontypeViewController.h"

@interface IMMainViewController : UIViewController <IMSessiontypeViewControllerDelegate>

@end