//
//  ImmediateApproachResultsViewController.swift
//  Tspot Calculator
//
//  Created by Anthony Long on 6/7/16.
//  Copyright © 2016 Infuse Medical. All rights reserved.
//

import UIKit

class ImmediateApproachResultsViewController: IMBaseViewController, UITableViewDataSource, UITableViewDelegate {
	
	@IBOutlet weak var tableView: UITableView!
	@IBOutlet weak var labelTotalTests: UILabel!
	@IBOutlet weak var labelBudget: UILabel!
	@IBOutlet weak var tfPrice: UITextField!
	@IBOutlet weak var buttonEdit: UIButton!
	@IBOutlet weak var buttonPopulation: UIButton!
	@IBOutlet weak var viewTableFooter: UIView!
	
	
	let yearColumns = ["N", "P", "Q", "R", "S", "T", "U", "V", "W", "X", "Y"]
	let cellId = "PopulationCell"
	var numOfRows = 0 {
		didSet {
			self.spreadsheet.spreadSheetCell(forIdentifier: "M39").value = NSNumber(value: numOfRows as Int)
		}
	}
	
	@IBAction func addRow () {
		numOfRows = numOfRows + 1
		for c in yearColumns {
			self.spreadsheet.spreadSheetCell(forIdentifier: "\(c)\(numOfRows - 1)").value = NSNumber(value: 0.0 as Double)
		}
		let indexPath = IndexPath(row: numOfRows-1, section: 0)
		self.tableView.insertRows(at: [indexPath], with: .bottom)
		self.tableView.scrollToRow(at: indexPath, at: .bottom, animated: true)
	}
	
	@IBAction func handleEditTouch () {
		self.buttonEdit.isSelected = !self.buttonEdit.isSelected
		self.buttonPopulation.isHidden = !self.buttonEdit.isSelected
		self.tableView.reloadData()
	}
	
	func handleDeleteButtonTouch (_ sender: UIButton) {
		self.deleteRowAtIndex(sender.tag)
	}
	
	func deleteRowAtIndex (_ index: Int) {
		for i in index+40..<54 {
			self.spreadsheet.spreadSheetCell(forIdentifier: "M\(i)").textValue = self.spreadsheet.spreadSheetCell(forIdentifier: "M\(i+1)").textValue
			for c in yearColumns {
				if i == 53 {
					self.spreadsheet.spreadSheetCell(forIdentifier: "\(c)\(i)").value = NSNumber(value: 0.0 as Double)
				}else{
					self.spreadsheet.spreadSheetCell(forIdentifier: "\(c)\(i)").value = self.spreadsheet.spreadSheetCell(forIdentifier: "\(c)\(i+1)").value
				}
			}
		}
		numOfRows -= 1
		let indexPath = IndexPath(row: index, section: 0)
		self.tableView.deleteRows(at: [indexPath], with: .fade)
	}
	
	func setLabelText () {
		let totalTests = self.spreadsheet.value(forIdentifier: "N55")
		self.labelTotalTests.text = totalTests?.intStringValue()
		if let pricePer = self.spreadsheet.value(forIdentifier: "N56") {
			let budget = NSNumber(value: (totalTests?.doubleValue)! * pricePer.doubleValue as Double)
			self.tfPrice.text = pricePer.currencyValue()
			self.labelBudget.text = budget.currencyValue()
		}
	}
	
	override func textFieldDidEndEditing(_ textField: UITextField) {
		if textField.superview!.tag == 10 {
			if let intVal = Int(textField.text!) {
				self.spreadsheet.spreadSheetCell(forIdentifier: "N\(textField.tag+40)").value = NSNumber(value: intVal as Int)
				self.spreadsheet.spreadSheetCell(forIdentifier: "P\(textField.tag+40)").value = NSNumber(value: intVal as Int)
			}
		}else{
			self.spreadsheet.spreadSheetCell(forIdentifier: "M\(textField.tag+40)").textValue = textField.text
		}
	}
	
	func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
		return numOfRows
	}
	
	func numberOfSections(in tableView: UITableView) -> Int {
		return 1
	}
	
	override func mapFields() {
		self.spreadsheet.spreadSheetCell(forIdentifier: "N40").addObserver(self, forKeyPath: "value", options: .new, context: nil)
		self.spreadsheet.spreadSheetCell(forIdentifier: "N56").addObserver(self, forKeyPath: "value", options: .new, context: nil)
		self.spreadsheet.spreadSheetCell(forIdentifier: "N44").addObserver(self, forKeyPath: "value", options: .new, context: nil)
		self.spreadsheet.spreadSheetCell(forIdentifier: "N55").addObserver(self, forKeyPath: "value", options: .new, context: nil)
		super.mapFields()
	}
	
	override func clearMappings () {
		self.spreadsheet.spreadSheetCell(forIdentifier: "N40").removeObserver(self, forKeyPath: "value")
		self.spreadsheet.spreadSheetCell(forIdentifier: "N56").removeObserver(self, forKeyPath: "value")
		self.spreadsheet.spreadSheetCell(forIdentifier: "N44").removeObserver(self, forKeyPath: "value")
		self.spreadsheet.spreadSheetCell(forIdentifier: "N55").removeObserver(self, forKeyPath: "value")
		super.clearMappings()
	}
	
	override func observeValue(forKeyPath keyPath: String?, of object: Any?, change: [NSKeyValueChangeKey : Any]?, context: UnsafeMutableRawPointer?) {
		self.tableView.reloadData()
		self.setLabelText()
		super.observeValue(forKeyPath: keyPath, of: object, change: change, context: context)
	}
	
	func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
		let cell = tableView.dequeueReusableCell(withIdentifier: cellId, for: indexPath) as! PhasedApproachTableViewCell
		cell.tfPopulationName.text = self.spreadsheet.spreadSheetCell(forIdentifier: "M\((indexPath as NSIndexPath).row+40)").textValue
		let disabled = cell.tfPopulationName.text == "Annual employees who received the T-SPOT.TB test the previous year upon new hire"
		if let value = self.spreadsheet.spreadSheetCell(forIdentifier: "N\((indexPath as NSIndexPath).row+40)").value {
			cell.tfPopulationCount.text = value.intStringValue()
		}else{
			cell.tfPopulationCount.text = ""
		}
		cell.tfPopulationCount.isHidden = disabled;
		cell.tfPopulationName.alpha = disabled ? 0.8 : 1.0
		cell.tfPopulationName.tag = (indexPath as NSIndexPath).row
		cell.tfPopulationCount.tag = (indexPath as NSIndexPath).row
		cell.tfPopulationCount.delegate = self
		cell.tfPopulationName.delegate = self
//		cell.tfPopulationName.enabled = indexPath.row > 4
		cell.buttonDelete.tag = (indexPath as NSIndexPath).row
		cell.buttonDelete.isHidden = self.buttonPopulation.isHidden
		cell.buttonDelete.addTarget(self, action: #selector(ImmediateApproachResultsViewController.handleDeleteButtonTouch(_:)), for: .touchUpInside)
		
		return cell
	}
	
	func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
		cell.backgroundColor = UIColor.clear
	}
	
	override func viewDidLoad () {
		super.viewDidLoad()
		self.numOfRows = self.spreadsheet.spreadSheetCell(forIdentifier: "M39").value!.intValue
		self.buttonPopulation.isHidden = true
		self.tableView.register(UINib(nibName: "PhasedApproachTableViewCell", bundle: nil), forCellReuseIdentifier: cellId)
		self.tableView.tableFooterView = self.viewTableFooter
	}
	
}
