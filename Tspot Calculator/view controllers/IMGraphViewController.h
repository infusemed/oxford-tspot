//
//  IMGraphViewController.h
//  Tspot Calculator
//
//  Created by Anthony Long on 6/24/14.
//  Copyright (c) 2014 Infuse Medical. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "IMSpreadSheet.h"
#import "IMGraphView.h"
#import "IMPieChartView.h"

@interface IMGraphViewController : UIViewController

@property (nonatomic, copy) NSString *cellIds;
@property (nonatomic, weak) IMSpreadSheet *spreadsheet;
@property (nonatomic, weak) IMWorkbook *workbook;
@property (nonatomic, assign) BOOL isCurrency;
@property (nonatomic, strong) IMGraphView *graphView;
@property (nonatomic, copy) NSString *rightBarText;
@property (nonatomic, copy) NSString *leftBarText;
@property (nonatomic, strong) NSArray *itemsForLeftPopup;
@property (nonatomic, strong) NSArray *itemsForRightPopup;
@property (nonatomic, strong) IBOutlet UILabel *titleLabel;
@property (nonatomic, strong) IBOutlet IMPieChartView *pieChart;
@property (nonatomic, strong) NSString *legends;
@property (nonatomic, strong) IBOutlet UITableView *tableView;

- (UIImage *)graphImage;

@end