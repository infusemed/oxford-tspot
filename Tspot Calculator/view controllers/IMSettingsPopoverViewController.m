//
//  IMSettingsPopoverViewController.m
//  Tspot Calculator
//
//  Created by Anthony Long on 6/25/14.
//  Copyright (c) 2014 Infuse Medical. All rights reserved.
//

#import "IMSettingsPopoverViewController.h"

@interface IMSettingsPopoverViewController ()

@end

@implementation IMSettingsPopoverViewController


- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void) viewWillAppear:(BOOL)animated
{
	[super viewWillAppear:animated];
	
	UIView *view = self.view;
	while (view != nil) {
		if (view.layer.cornerRadius > 0) {
			view.layer.cornerRadius = 10.0;
		}
		view = view.superview;
	}
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end