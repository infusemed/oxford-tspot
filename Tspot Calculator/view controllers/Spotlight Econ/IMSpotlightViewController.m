//
//  IMSpotlightViewController.m
//  Tspot Calculator
//
//  Created by Anthony Long on 6/21/14.
//  Copyright (c) 2014 Infuse Medical. All rights reserved.
//

#import "IMSpotlightViewController.h"

@interface IMSpotlightViewController ()

@property (weak, nonatomic) IBOutlet UISlider *slider;

@end

@implementation IMSpotlightViewController

- (IBAction)handleSliderChange:(UISlider *)sender
{
//	NSLog(@"%f : %f : %f", sender.value, (sender.value/0.33), roundf(sender.value/0.33))
	sender.value = roundf(sender.value/0.33) * 0.33;
	[self.spreadsheet setValue:[NSNumber numberWithInt:(sender.value/0.33)+1]
				 forIdentifier:@"B6"];
}


- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    self.slider.value = 0.33 * ([self.spreadsheet valueForIdentifier:@"B6"].intValue - 1);
	[self.slider setThumbImage:[UIImage imageNamed:@"track-slider.png"] forState:UIControlStateNormal];
	self.slider.minimumTrackTintColor = [UIColor colorWithWhite:1.0f alpha:0.1f];
	self.slider.maximumTrackTintColor = [UIColor colorWithWhite:1.0f alpha:0.1f];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end