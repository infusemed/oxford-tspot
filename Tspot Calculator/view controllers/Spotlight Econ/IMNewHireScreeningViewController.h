//
//  IMNewHireScreeningViewController.h
//  Tspot Calculator
//
//  Created by Anthony Long on 6/21/14.
//  Copyright (c) 2014 Infuse Medical. All rights reserved.
//

#import "IMBaseViewController.h"

@interface IMNewHireScreeningViewController : IMBaseViewController

@end
