//
//  IMNewHireScreeningViewController.m
//  Tspot Calculator
//
//  Created by Anthony Long on 6/21/14.
//  Copyright (c) 2014 Infuse Medical. All rights reserved.
//

#import "IMNewHireScreeningViewController.h"

@interface IMNewHireScreeningViewController ()

@property (weak, nonatomic) IBOutlet UIButton *buttonStep1;
@property (weak, nonatomic) IBOutlet UIButton *buttonStep2;


@end

@implementation IMNewHireScreeningViewController

#pragma mark - IBActions

- (IBAction)handleButtonStepTouch:(UIButton *)sender
{
	sender.selected = !sender.selected;
	[self.spreadsheet setValue:[NSNumber numberWithBool:self.buttonStep1.selected]
				 forIdentifier:@"B16"];
	[self.spreadsheet setValue:[NSNumber numberWithBool:self.buttonStep2.selected]
				 forIdentifier:@"B17"];
}

#pragma mark - UIViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    self.buttonStep1.selected = [self.spreadsheet valueForIdentifier:@"B16"].boolValue;
	self.buttonStep2.selected = [self.spreadsheet valueForIdentifier:@"B17"].boolValue;
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
