//
//  IMReferenceInfoViewController.m
//  Tspot Calculator
//
//  Created by Anthony Long on 8/1/14.
//  Copyright (c) 2014 Infuse Medical. All rights reserved.
//

#import "IMReferenceInfoViewController.h"

@interface IMReferenceInfoViewController ()


@property (weak, nonatomic) IBOutlet UIView *viewPopup;
@property (weak, nonatomic) IBOutlet UIButton * buttonClose;
@property (strong, nonatomic) UIVisualEffectView *viewBlur;
@property (weak, nonatomic) IBOutlet UILabel *labelInfo;

@end

@implementation IMReferenceInfoViewController
{
	NSString *_info;
}


#pragma mark - IBActions

- (IBAction)handleCloseTouch:(UIButton *)button
{
	[self dismissViewControllerAnimated:YES completion:NULL];
}

#pragma mark - UIViewControllerContextTransitioning

- (id<UIViewControllerAnimatedTransitioning>)animationControllerForPresentedController:(UIViewController *)presented
                                                                  presentingController:(UIViewController *)presenting
                                                                      sourceController:(UIViewController *)source
{
	return self;
}

- (id <UIViewControllerAnimatedTransitioning>)animationControllerForDismissedController:(UIViewController *)dismissed
{
	return self;
}

- (NSTimeInterval)transitionDuration:(id<UIViewControllerContextTransitioning>)transitionContext
{
	return 0.3;
}


#pragma mark - UIViewControllerAnimatedTransitioning

-(void)animateTransition:(id<UIViewControllerContextTransitioning>)transitionContext
{
    if(!self.viewBlur){
        [self animateInWithContext:transitionContext];
    }
    else{
        [self animateOutWithContext:transitionContext];
    }
	
}

#pragma mark - Modal presentation

- (void) animateInWithContext:(id<UIViewControllerContextTransitioning>)transitionContext
{
	UIView *containerView = [transitionContext containerView];
	UIViewController *fromViewController = [transitionContext viewControllerForKey:UITransitionContextFromViewControllerKey];
	
	UIVisualEffectView *vev = [[UIVisualEffectView alloc] initWithEffect:[UIBlurEffect effectWithStyle:UIBlurEffectStyleLight]];
	vev.frame = fromViewController.view.frame;
	
	[containerView addSubview:vev];
	containerView.backgroundColor = [UIColor clearColor];
	[containerView addSubview:self.view];
	self.view.frame = ({
		CGRect rect = self.view.frame;
		rect.origin.y = 768.0f;
		rect;
	});
	
	__weak IMReferenceInfoViewController *weakself = self;
	[UIView animateWithDuration:0.3
						  delay:0.0f
						options:UIViewAnimationOptionCurveEaseOut
					 animations:^{
						 weakself.view.frame = ({
							 CGRect rect = weakself.view.frame;
							 rect.origin.y = 0.0;
							 rect;
						 });
					 } completion:^(BOOL finished) {
						 [self.view insertSubview:vev atIndex:0];
						 [transitionContext completeTransition:YES];
					 }];
	
	self.viewBlur = vev;
}

- (void) animateOutWithContext:(id<UIViewControllerContextTransitioning>)transitionContext
{
	UIView *containerView = [transitionContext containerView];
	containerView.backgroundColor = [UIColor clearColor];
	[containerView insertSubview:self.viewBlur atIndex:0];
	[containerView addSubview:self.view];
	
	__weak IMReferenceInfoViewController *weakself = self;
	[UIView animateWithDuration:0.3
						  delay:0.0f
						options:UIViewAnimationOptionCurveEaseIn
					 animations:^{
						 weakself.view.frame = ({
							 CGRect rect = weakself.view.frame;
							 rect.origin.y = 768.0f;
							 rect;
						 });
					 } completion:^(BOOL finished) {
						 [transitionContext completeTransition:YES];
					 }];
}


#pragma mark - UIViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil info:(NSString *)info
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
		_info = info;
        self.transitioningDelegate = self;
		self.modalPresentationStyle = UIModalPresentationCustom;
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    self.view.backgroundColor = [UIColor clearColor];
	
	self.viewPopup.layer.borderWidth = 3.0f;
	self.viewPopup.layer.borderColor = [UIColor colorWithRed:154.0f/255.0f
													   green:159.0f/255.0f
														blue:207.0f/255.0f
													   alpha:1.0f].CGColor;
	self.viewPopup.layer.cornerRadius = 10.0f;
	self.viewPopup.layer.masksToBounds = YES;
	self.viewPopup.layer.shadowColor = [UIColor colorWithRed:135.0f/255.0f
													   green:129.0f/255.0f
														blue:189.0f/255.0f
													   alpha:1.0f].CGColor;
	self.viewPopup.layer.shadowOpacity = 0.5f;
	self.viewPopup.layer.shadowRadius = 22.0f;
	self.viewPopup.layer.shadowOffset = CGSizeMake(0.0f, -0.5f);
	
	self.labelInfo.text = _info;
	CGRect rect = [_info boundingRectWithSize:CGSizeMake(self.labelInfo.frame.size.width, 500.0)
													options:NSStringDrawingUsesLineFragmentOrigin
												 attributes:@{NSFontAttributeName:self.labelInfo.font}
													context:nil];
	self.labelInfo.frame = ({
		CGRect r = self.labelInfo.frame;
		r.size.height = rect.size.height;
		r;
	});
	
	self.viewPopup.frame = ({
		CGRect r = self.viewPopup.frame;
		r.size.height = CGRectGetMaxY(self.labelInfo.frame) + 30.0f;
		r;
	});
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
