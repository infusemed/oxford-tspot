//
//  IMSaveSessionViewController.m
//  Tspot Calculator
//
//  Created by Anthony Long on 6/23/14.
//  Copyright (c) 2014 Infuse Medical. All rights reserved.
//

#import "IMSaveSessionViewController.h"

@interface IMSaveSessionViewController ()

@property (nonatomic, weak) IBOutlet UITextField *tfFileName;

@end

@implementation IMSaveSessionViewController


#pragma mark - IBActions

- (IBAction)handleSaveTouch:(UIButton *)sender
{
	[self.delegate saveSessionViewController:self shouldSaveWithName:self.tfFileName.text];
}


#pragma mark - UIViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
		
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    CGColorRef borderColor = self.tfFileName.backgroundColor.CGColor;
	self.tfFileName.backgroundColor = [UIColor whiteColor];
	self.tfFileName.layer.borderWidth = 2.0f;
	self.tfFileName.layer.borderColor = borderColor;
	self.tfFileName.layer.cornerRadius = 7.0f;
	self.tfFileName.layer.masksToBounds = YES;
}

- (void) viewWillAppear:(BOOL)animated
{
	[super viewWillAppear:animated];
	
	UIView *view = self.view;
	while (view != nil) {
		if (view.layer.cornerRadius > 0) {
			view.layer.cornerRadius = 10.0;
		}
		view = view.superview;
	}
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
