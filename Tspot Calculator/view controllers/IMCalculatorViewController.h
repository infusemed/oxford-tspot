//
//  IMCalculatorViewController.h
//  Tspot Calculator
//
//  Created by Anthony Long on 6/13/14.
//  Copyright (c) 2014 Infuse Medical. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "IMSpreadSheet.h"
#import "IMSaveSessionViewController.h"
#import "IMAdditionalTextViewController.h"
#import "IMArcMediaViewer.h"
#import <MessageUI/MessageUI.h>

@interface IMCalculatorViewController : UIViewController <UIPopoverControllerDelegate, IMSaveSessionViewControllerDelegate, UIScrollViewDelegate, UIAlertViewDelegate, MFMailComposeViewControllerDelegate, IMAdditionalTextViewControllerDelegate, IMArcMediaViewerDelegate>

@property (nonatomic, strong) NSDictionary *config;
@property (nonatomic, strong) IMWorkbook *workbook;

@end
