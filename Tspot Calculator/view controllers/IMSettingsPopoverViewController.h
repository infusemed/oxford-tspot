//
//  IMSettingsPopoverViewController.h
//  Tspot Calculator
//
//  Created by Anthony Long on 6/25/14.
//  Copyright (c) 2014 Infuse Medical. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface IMSettingsPopoverViewController : UIViewController

@property (weak, nonatomic) IBOutlet UIButton *buttonClear;

@end
