//
//  IMTSPOTvQFTCostDataViewController.m
//  Tspot Calculator
//
//  Created by Anthony Long on 7/25/14.
//  Copyright (c) 2014 Infuse Medical. All rights reserved.
//

#import "IMTSPOTvQFTCostDataViewController.h"

@implementation IMTSPOTvQFTCostDataViewController
{
	__weak IBOutlet UIButton *_buttonPay;
}

- (IBAction)handleButtonTouch:(UIButton *)sender
{
	sender.selected = !sender.selected;
	[self.spreadsheet spreadSheetCellForIdentifier:@"B22"].value = [NSNumber numberWithBool:sender.selected];
}

- (void) viewDidLoad
{
	[super viewDidLoad];
	_buttonPay.selected = [self.spreadsheet valueForIdentifier:@"B22"].boolValue;
}

@end
