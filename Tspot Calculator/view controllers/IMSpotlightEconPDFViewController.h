//
//  IMSpotlightEconPDFViewController.h
//  Tspot Calculator
//
//  Created by Anthony Long on 6/27/14.
//  Copyright (c) 2014 Infuse Medical. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "IMSpreadSheet.h"

@interface IMSpotlightEconPDFViewController : UIViewController

@property (nonatomic, strong) IMWorkbook *workbook;

@property (weak, nonatomic) IBOutlet UIImageView *ivChart1;
@property (weak, nonatomic) IBOutlet UIImageView *ivChart2;
@property (weak, nonatomic) IBOutlet UIImageView *ivChart3;
@property (weak, nonatomic) IBOutlet UIImageView *ivChart4;

@property (nonatomic, copy) NSString *additionalInfo;

- (NSData *) pdfVersion;

@end
