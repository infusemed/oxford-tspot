//
//  IMReferenceInfoViewController.h
//  Tspot Calculator
//
//  Created by Anthony Long on 8/1/14.
//  Copyright (c) 2014 Infuse Medical. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface IMReferenceInfoViewController : UIViewController <UIViewControllerAnimatedTransitioning, UIViewControllerTransitioningDelegate>

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil info:(NSString *)info;

@end
