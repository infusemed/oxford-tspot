//
//  IMSpotlightEconPDFViewController.m
//  Tspot Calculator
//
//  Created by Anthony Long on 6/27/14.
//  Copyright (c) 2014 Infuse Medical. All rights reserved.
//

#import "IMSpotlightEconPDFViewController.h"
#import "NSNumber+IMSpreadSheet.h"
#import "IMSpotlightEconWorkbook.h"

@interface NSMutableAttributedString (keyed)

- (NSMutableAttributedString *)stringByReplacingKey:(NSString *)key withValue:(NSString *)value;

@end

@implementation NSMutableAttributedString (keyed)

- (NSMutableAttributedString *)stringByReplacingKey:(NSString *)key withValue:(NSString *)value
{
	NSString *s = [self string];
	NSRange range = [s rangeOfString:key];
	if(range.location == NSNotFound)
	{
		NSLog(@"couldn't find key : %@", key);
		return self;
	}
	[self replaceCharactersInRange:range withString:value];
	return self;
}

@end


@interface IMSpotlightEconPDFViewController ()

@end

@implementation IMSpotlightEconPDFViewController
{
	__weak IBOutlet UILabel *_tf1;
	__weak IBOutlet UILabel *_tf2;
	__weak IBOutlet UILabel *_tf3;
	__weak IBOutlet UILabel *_tf4;
	__weak IBOutlet UILabel *_tf5;
	__weak IBOutlet UILabel *_tf6;
	
	
	IBOutlet UIView *_viewPage1;
	IBOutlet UIView *_viewPage2;
	IBOutlet UIView *_viewPage3;
}

- (NSData *) pdfVersion
{
	NSMutableData *pdfData = [NSMutableData data];
	
	UIGraphicsBeginPDFContextToData(pdfData, self.view.bounds, nil);
	UIGraphicsBeginPDFPage();
	
	CGContextRef pdfContext = UIGraphicsGetCurrentContext();
	[_viewPage1.layer renderInContext:pdfContext];
	
	UIGraphicsBeginPDFPage();
	
	[_viewPage2.layer renderInContext:pdfContext];
	
	UIGraphicsBeginPDFPage();
	
	[_viewPage3.layer renderInContext:pdfContext];
	
	UIGraphicsEndPDFContext();
	
	return pdfData;
}

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) { 
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	
	IMSpreadSheet *ssCharts = [self.workbook spreadSheetWithName:kIMChartsAndTotals];
	if(self.workbook.customerName)
	{
		_tf1.attributedText = [[_tf1.attributedText mutableCopy] stringByReplacingKey:@"<customer name>" withValue:self.workbook.customerName];
		_tf1.attributedText = [[_tf1.attributedText mutableCopy] stringByReplacingKey:@"<customer name>" withValue:self.workbook.customerName];
		_tf4.attributedText = [[_tf4.attributedText mutableCopy] stringByReplacingKey:@"<customer name>" withValue:self.workbook.customerName];
	}
	//B16,C16
	_tf1.attributedText = [[_tf1.attributedText mutableCopy] stringByReplacingKey:@"X" withValue:[ssCharts sumOfCells:@[@"B10", @"B11"]].intStringValue];
	_tf1.attributedText = [[_tf1.attributedText mutableCopy] stringByReplacingKey:@"Y" withValue:[ssCharts sumOfCells:@[@"C10", @"C11"]].intStringValue];
	_tf1.attributedText = [[_tf1.attributedText mutableCopy] stringByReplacingKey:@"X" withValue:[ssCharts valueForIdentifier:@"B16"].intStringValue];
	_tf1.attributedText = [[_tf1.attributedText mutableCopy] stringByReplacingKey:@"Y" withValue:[ssCharts valueForIdentifier:@"C16"].intStringValue];
	_tf1.attributedText = [[_tf1.attributedText mutableCopy] stringByReplacingKey:@"<val1>" withValue:[ssCharts valueForIdentifier:@"B6"].intCurrencyValue];
	_tf1.attributedText = [[_tf1.attributedText mutableCopy] stringByReplacingKey:@"<val2>" withValue:[ssCharts valueForIdentifier:@"C6"].intCurrencyValue];
	NSNumber *n = [NSNumber numberWithDouble:[ssCharts valueForIdentifier:@"B12"].doubleValue - [ssCharts valueForIdentifier:@"C12"].doubleValue];
	_tf1.attributedText = [[_tf1.attributedText mutableCopy] stringByReplacingKey:@"<val3>" withValue:n.intStringValue];
	n = [NSNumber numberWithDouble:[ssCharts valueForIdentifier:@"B16"].doubleValue - [ssCharts valueForIdentifier:@"C16"].doubleValue];
	_tf2.attributedText = [[_tf2.attributedText mutableCopy] stringByReplacingKey:@"<val4>" withValue:n.intStringValue];
	n = [NSNumber numberWithDouble:[ssCharts sumOfCells:@[@"B2", @"B3", @"B4"]].doubleValue - [ssCharts sumOfCells:@[@"C2", @"C3", @"C4"]].doubleValue];
	_tf3.attributedText = [[_tf3.attributedText mutableCopy] stringByReplacingKey:@"<val5>" withValue:n.intCurrencyValue];
	_tf4.attributedText = [[_tf4.attributedText mutableCopy] stringByReplacingKey:@"<val6>" withValue:[ssCharts valueForIdentifier:@"B20"].intStringValue];
	_tf4.attributedText = [[_tf4.attributedText mutableCopy] stringByReplacingKey:@"<val7>" withValue:[ssCharts valueForIdentifier:@"C20"].intStringValue];
	n = [NSNumber numberWithDouble:[ssCharts valueForIdentifier:@"B20"].doubleValue - [ssCharts valueForIdentifier:@"C20"].doubleValue];
	_tf5.attributedText = [[_tf5.attributedText mutableCopy] stringByReplacingKey:@"<val8>" withValue:n.intStringValue];
	
	if(self.additionalInfo)
	{
		_tf6.attributedText = [[_tf6.attributedText mutableCopy] stringByReplacingKey:@"<info>" withValue:self.additionalInfo];
	}else{
		_tf6.hidden = YES;
	}
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
