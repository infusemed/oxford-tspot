//
//  IMAppDelegate.h
//  Tspot Calculator
//
//  Created by Anthony Long on 6/3/14.
//  Copyright (c) 2014 Infuse Medical. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface IMAppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@end
