//
//  main.m
//  Tspot Calculator
//
//  Created by Anthony Long on 6/3/14.
//  Copyright (c) 2014 Infuse Medical. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "IMAppDelegate.h"

int main(int argc, char * argv[])
{
	@autoreleasepool {
	    return UIApplicationMain(argc, argv, nil, NSStringFromClass([IMAppDelegate class]));
	}
}	