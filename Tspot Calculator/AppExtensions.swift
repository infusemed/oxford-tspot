//
//  AppExtensions.swift
//  tomosynthesis
//
//  Created by Anthony Long on 3/17/16.
//  Copyright © 2016 Infuse Medical. All rights reserved.
//

import UIKit

extension UIView {
	
	@IBInspectable var cornerRadius: CGFloat {
		get {
			return layer.cornerRadius
		}
		set {
			layer.cornerRadius = newValue
			layer.masksToBounds = newValue > 0
		}
	}
	
	@IBInspectable var borderSize: CGFloat {
		get {
			return layer.borderWidth
		}
		set {
			layer.borderWidth = newValue
		}
	}
	
	@IBInspectable var borderColor: UIColor?{
		get {
			if((layer.borderColor) != nil){
				return UIColor(cgColor: layer.borderColor!)
			}
			return nil;
		}
		set {
			layer.borderColor = newValue?.cgColor
		}
	}
	
}
