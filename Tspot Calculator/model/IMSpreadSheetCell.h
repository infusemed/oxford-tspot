//
//  IMSpreadSheetCell.h
//  IMSpreadSheet
//
//  Created by Anthony Long on 4/18/13.
//  Copyright (c) 2013 Infuse Medical. All rights reserved.
//

#import <Foundation/Foundation.h>

@class IMSpreadSheet, IMSpreadSheetCell;

typedef NSNumber * (^IMSpreadSheetCellFunc_t)(IMSpreadSheet* calculator, IMSpreadSheetCell *cell);

@interface IMSpreadSheetCellCalculationData : NSObject

@property (nonatomic) SEL selectorToPerform;
@property (nonatomic, strong) NSArray * valuesForOperation;  // these may be NSNumber or other IMSpreadSheetCellCalculationData instances

@end


@interface IMSpreadSheetCell : NSObject

@property (nonatomic, strong) NSIndexPath * indexPath;
@property (nonatomic, weak) IMSpreadSheet * spreadSheet;
@property (nonatomic, strong) NSNumber * value;
@property (nonatomic, strong) NSNumber * maxValue;
@property (nonatomic, strong) NSNumber * minValue;
@property (nonatomic, copy) NSString * textValue;
@property (nonatomic, strong) NSString * formula;
@property (nonatomic, strong) IMSpreadSheetCellCalculationData * rootCalculationData;
@property (nonatomic, strong) NSArray * watchList;
@property (nonatomic) BOOL watchListEnabled;
@property (nonatomic) BOOL hasFunction;

- (void)calculate;

- (void)setFunction:(IMSpreadSheetCellFunc_t)cellFunc;
- (void)setSetterFunction:(void(^)(IMSpreadSheetCell *cell))cellFunc;
- (void)mapToCell:(IMSpreadSheetCell *)cell;
//- (void)setWatchList:(NSArray *)list;  // array of cells to watch for value changes

@end