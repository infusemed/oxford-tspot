//
//  IMTSPOTvTSTWorkbook.m
//  Tspot Calculator
//
//  Created by Anthony Long on 7/25/14.
//  Copyright (c) 2014 Infuse Medical. All rights reserved.
//

#import "IMTSPOTvTSTWorkbook.h"

@implementation IMTSPOTvTSTWorkbook

NSString * const kIMTvTInput = @"Input Tab";
NSString * const kIMTvTAnnualScreening = @"Annual TB Screening Program Tab";
NSString * const kIMTvTNewHireScreening = @"New Hire TB Screening Program Tab";
NSString * const kIMTvTTSPOT = @"T-SPOT";
NSString * const kIMTvTEmpCostData = @"Employee Cost Data";
NSString * const kIMTvTCharts = @"Charts and Totals";
NSString * const kIMTvTAnalysis = @"Analysis";
NSString * const kIMTvTEmpTimeData = @"Employee Time Reference Data";


//- (id) init
//{
//	self = [super init];
//	if(self)
//	{
//		[self initializeWorkbook];
//	}
//	
//	return self;
//}
//
//- (id) initWithFile:(NSString *)filePath
//{
//	self = [super init];
//	if(self)
//	{
//		self.fileName = filePath.lastPathComponent;
//		[self initializeWorkbook];
//		NSDictionary * d = [NSDictionary dictionaryWithContentsOfFile:filePath];
//		[d enumerateKeysAndObjectsUsingBlock:^(id key, id obj, BOOL *stop) {
//			NSDictionary * d2 = (NSDictionary *)obj;
//			IMSpreadSheet * spreadsheet = [self spreadSheetWithName:(NSString *)key];
//			[d2 enumerateKeysAndObjectsUsingBlock:^(id key, id obj, BOOL *stop) {
//				NSArray * a = [(NSString *)key componentsSeparatedByString:@"-"];
//				NSUInteger indexes[] = {[[a objectAtIndex:0] intValue], [[a objectAtIndex:1] intValue]};
//				if([obj isKindOfClass:[NSString class]])
//				{
//					[spreadsheet setTextValue:obj forIndexPath:[NSIndexPath indexPathWithIndexes:indexes length:2]];
//				}
//				else
//				{
//					[spreadsheet setValue:obj forIndexPath:[NSIndexPath indexPathWithIndexes:indexes length:2]];
//				}
//			}];
//		}];
//	}
//	
//	return self;
//}

- (void) setCustomerName:(NSString *)customerName
{
	[super setCustomerName:customerName];
	[[self spreadSheetWithName:kIMTvTAnnualScreening] spreadSheetCellForIdentifier:@"B3"].textValue = customerName;
}

- (NSString *) customerName
{
	return [[self spreadSheetWithName:kIMTvTAnnualScreening] spreadSheetCellForIdentifier:@"B3"].textValue;
}

- (void) initializeWorkbook
{
	[super initializeWorkbook];
	[self setSpreadSheet:[[IMSpreadSheet alloc] init]
				WithName:kIMTvTInput];
	[self setSpreadSheet:[[IMSpreadSheet alloc] init]
				WithName:kIMTvTAnnualScreening];
	[self setSpreadSheet:[[IMSpreadSheet alloc] init]
				WithName:kIMTvTNewHireScreening];
	[self setSpreadSheet:[[IMSpreadSheet alloc] init]
				WithName:kIMTvTTSPOT];
	[self setSpreadSheet:[[IMSpreadSheet alloc] init]
				WithName:kIMTvTEmpCostData];
	[self setSpreadSheet:[[IMSpreadSheet alloc] init]
				WithName:kIMTvTCharts];
	[self setSpreadSheet:[[IMSpreadSheet alloc] init]
				WithName:kIMTvTAnalysis];
	[self setSpreadSheet:[[IMSpreadSheet alloc] init]
				WithName:kIMTvTEmpTimeData];
	
	[self initInputTab];
	[self initScreeningTab];
	[self initNewHireScreening];
	[self initEmployeeCostData];
	[self initTSPOT];
	[self initEmployeeTimeReferenceData];
	[self initChartsAndTotals];
	[self initAnalysis];
}

- (void) initInputTab
{
	IMSpreadSheet *spreadSheet = [self spreadSheetWithName:kIMTvTInput];
	IMSpreadSheet *ssScreening = [self spreadSheetWithName:kIMTvTAnnualScreening];
	IMSpreadSheet *ssNewHire = [self spreadSheetWithName:kIMTvTNewHireScreening];
	IMSpreadSheet *ssTspot = [self spreadSheetWithName:kIMTvTTSPOT];
	
	IMSpreadSheetCell * cell = [spreadSheet spreadSheetCellForIdentifier:@"B3"];
	[cell setSetterFunction:^(IMSpreadSheetCell *cell) {
		[ssScreening spreadSheetCellForIdentifier:@"B5"].value = cell.value;
	}];
	
	cell = [spreadSheet spreadSheetCellForIdentifier:@"B5"];
	[cell setSetterFunction:^(IMSpreadSheetCell *cell) {
		[ssNewHire spreadSheetCellForIdentifier:@"B4"].value = cell.value;
	}];
	
	cell = [spreadSheet spreadSheetCellForIdentifier:@"B8"];
	cell.value = @2.83;
	[cell setSetterFunction:^(IMSpreadSheetCell *cell) {
		[ssScreening spreadSheetCellForIdentifier:@"B10"].value = cell.value;
	}];
	cell = [spreadSheet spreadSheetCellForIdentifier:@"D8"];
	[cell setSetterFunction:^(IMSpreadSheetCell *cell) {
		[ssTspot spreadSheetCellForIdentifier:@"B2"].value = cell.value;
	}];
	
	cell = [spreadSheet spreadSheetCellForIdentifier:@"B13"];
	[cell setSetterFunction:^(IMSpreadSheetCell *cell) {
		[ssNewHire spreadSheetCellForIdentifier:@"B16"].value = cell.value;
	}];
	cell.value = [NSNumber numberWithBool:NO];
	
	cell = [spreadSheet spreadSheetCellForIdentifier:@"B14"];
	[cell setSetterFunction:^(IMSpreadSheetCell *cell) {
		[ssNewHire spreadSheetCellForIdentifier:@"B17"].value = cell.value;
	}];
	cell.value = [NSNumber numberWithBool:YES];
}

- (void) initScreeningTab
{
	IMSpreadSheet *spreadSheet = [self spreadSheetWithName:kIMTvTAnnualScreening];
	IMSpreadSheet *ssNewHire = [self spreadSheetWithName:kIMTvTNewHireScreening];
	IMSpreadSheet *ssCharts = [self spreadSheetWithName:kIMTvTCharts];
	[spreadSheet spreadSheetCellForIdentifier:@"B3"];
	IMSpreadSheetCell *cell = [spreadSheet spreadSheetCellForIdentifier:@"B5"];
	[cell setSetterFunction:^(IMSpreadSheetCell *cell) {
//		[ssCharts spreadSheetCellForIdentifier:@"N44"].value = cell.value;
		for(int i=40;i<54;i++){
			NSString *location = [NSString stringWithFormat:@"M%d", i];
			NSString *s2 = [ssCharts spreadSheetCellForIdentifier:location].textValue;
			if([s2 isEqualToString:@"Annual Employees"]){
				NSArray *a = @[@"N", @"Q", @"R", @"S", @"T", @"U", @"V", @"W", @"X", @"Y"];
				[a enumerateObjectsUsingBlock:^(NSString *  _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
					NSString *cellId = [NSString stringWithFormat:@"%@%d", obj, i];
					[ssCharts spreadSheetCellForIdentifier:cellId].value = cell.value;
				}];
				break;
			}
		}
	}];
	[spreadSheet spreadSheetCellForIdentifier:@"B8"];
	cell = [spreadSheet spreadSheetCellForIdentifier:@"B10"];
	cell.value = @2.83;
	[cell setSetterFunction:^(IMSpreadSheetCell *cell) {
		[ssNewHire spreadSheetCellForIdentifier:@"B8"].value = cell.value;
	}];
	[spreadSheet spreadSheetCellForIdentifier:@"B13"];
	[spreadSheet spreadSheetCellForIdentifier:@"B16"].value = @0.2;
	cell = [spreadSheet spreadSheetCellForIdentifier:@"B20"];
	[cell setSetterFunction:^(IMSpreadSheetCell *cell) {
		[ssNewHire spreadSheetCellForIdentifier:@"B21"].value = cell.value;
	}];
	[spreadSheet spreadSheetCellForIdentifier:@"B21"];
}

- (void) initNewHireScreening
{
	IMSpreadSheet *spreadSheet = [self spreadSheetWithName:kIMTvTNewHireScreening];
	IMSpreadSheet *ssCharts = [self spreadSheetWithName:kIMTvTCharts];
	IMSpreadSheetCell * cell = [spreadSheet spreadSheetCellForIdentifier:@"B4"];
	[cell setSetterFunction:^(IMSpreadSheetCell *cell) {
		NSString *s = [ssCharts spreadSheetCellForIdentifier:@"M40"].textValue;
		if([s isEqualToString:@"New Hires"]){
			[ssCharts spreadSheetCellForIdentifier:@"N40"].value = cell.value;
			[ssCharts spreadSheetCellForIdentifier:@"P40"].value = cell.value;
			for(int i=41;i<54;i++){
				NSString *location = [NSString stringWithFormat:@"M%d", i];
				NSString *s2 = [ssCharts spreadSheetCellForIdentifier:location].textValue;
				if([s2 containsString:@"Annual employees who received the"]){
					
					location = [NSString stringWithFormat:@"Q%d", i];
					[ssCharts spreadSheetCellForIdentifier:location].value = cell.value;
					break;
				}
			}
		}
	}];
	[spreadSheet spreadSheetCellForIdentifier:@"B8"].value = @2.83;
	[spreadSheet spreadSheetCellForIdentifier:@"B11"].value = @0.0;
	[spreadSheet spreadSheetCellForIdentifier:@"B12"].value = @0.5;
	[spreadSheet spreadSheetCellForIdentifier:@"B16"].value = @0.0;
	[spreadSheet spreadSheetCellForIdentifier:@"B17"].value = @1.0;
	
	cell = [spreadSheet spreadSheetCellForIdentifier:@"B21"];
}

- (void) initTSPOT
{
	IMSpreadSheet *spreadSheet = [self spreadSheetWithName:kIMTvTTSPOT];
	IMSpreadSheet *ssCharts = [self spreadSheetWithName:kIMTvTCharts];
	IMSpreadSheetCell *cell = [spreadSheet spreadSheetCellForIdentifier:@"B2"];
	[cell setSetterFunction:^(IMSpreadSheetCell *cell) {
		[ssCharts spreadSheetCellForIdentifier:@"N56"].value = cell.value;
	}];
	[spreadSheet spreadSheetCellForIdentifier:@"B5"].value = @0.0;
}

- (void) initEmployeeCostData
{
	IMSpreadSheet * spreadSheet = [self spreadSheetWithName:kIMTvTEmpCostData];
	
	IMSpreadSheetCell * cell = [spreadSheet spreadSheetCellForIdentifier:@"B8"];
	cell.value = @54.44;
	cell = [spreadSheet spreadSheetCellForIdentifier:@"F8"];
	cell.value = @54.44;
	cell = [spreadSheet spreadSheetCellForIdentifier:@"B12"];
	cell.value = @110.52;
	cell = [spreadSheet spreadSheetCellForIdentifier:@"C12"];
	cell.value = @0.159;
	cell = [spreadSheet spreadSheetCellForIdentifier:@"D12"];
	cell.watchList = [spreadSheet arrayOfCells:@[@"C12", @"B12"]];
	[cell setFunction:^NSNumber *(IMSpreadSheet *calculator, IMSpreadSheetCell *cell) {
		return [calculator multiplyCells:@[@"C12", @"B12"]];
	}];
	
	cell = [spreadSheet spreadSheetCellForIdentifier:@"B13"];
	cell.value = @34.65;
	cell = [spreadSheet spreadSheetCellForIdentifier:@"C13"];
	cell.value = @0.363;
	cell = [spreadSheet spreadSheetCellForIdentifier:@"D13"];
	cell.watchList = [spreadSheet arrayOfCells:@[@"B13", @"C13"]];
	[cell setFunction:^NSNumber *(IMSpreadSheet *calculator, IMSpreadSheetCell *cell) {
		return [spreadSheet multiplyCells:@[@"B13", @"C13"]];
	}];
	
	cell = [spreadSheet spreadSheetCellForIdentifier:@"B14"];
	cell.value = @139.04;
	cell = [spreadSheet spreadSheetCellForIdentifier:@"C14"];
	cell.value = @0.063;
	cell = [spreadSheet spreadSheetCellForIdentifier:@"D14"];
	cell.watchList = [spreadSheet arrayOfCells:@[@"B14", @"C14"]];
	[cell setFunction:^NSNumber *(IMSpreadSheet *calculator, IMSpreadSheetCell *cell) {
		return [calculator multiplyCells:@[@"B14", @"C14"]];
	}];
	
	cell = [spreadSheet spreadSheetCellForIdentifier:@"B15"];
	cell.value = @26.9;
	cell = [spreadSheet spreadSheetCellForIdentifier:@"C15"];
	cell.value = @0.225;
	cell = [spreadSheet spreadSheetCellForIdentifier:@"D15"];
	cell.watchList = [spreadSheet arrayOfCells:@[@"B15", @"C15"]];
	[cell setFunction:^NSNumber *(IMSpreadSheet *calculator, IMSpreadSheetCell *cell) {
		return [calculator multiplyCells:@[@"B15", @"C15"]];
	}];
	
	cell = [spreadSheet spreadSheetCellForIdentifier:@"B16"];
	cell.value = @23.27;
	cell = [spreadSheet spreadSheetCellForIdentifier:@"C16"];
	cell.value = @0.19;
	cell = [spreadSheet spreadSheetCellForIdentifier:@"D16"];
	cell.watchList = [spreadSheet arrayOfCells:@[@"B16", @"C16"]];
	[cell setFunction:^NSNumber *(IMSpreadSheet *calculator, IMSpreadSheetCell *cell) {
		return [calculator multiplyCells:@[@"B16", @"C16"]];
	}];
	
	cell = [spreadSheet spreadSheetCellForIdentifier:@"F12"];
	cell.value = @110.52;
	cell = [spreadSheet spreadSheetCellForIdentifier:@"G12"];
	cell.value = @0.159;
	cell = [spreadSheet spreadSheetCellForIdentifier:@"H12"];
	cell.watchList = [spreadSheet arrayOfCells:@[@"G12", @"F12"]];
	[cell setFunction:^NSNumber *(IMSpreadSheet *calculator, IMSpreadSheetCell *cell) {
		return [calculator multiplyCells:@[@"G12", @"F12"]];
	}];
	
	cell = [spreadSheet spreadSheetCellForIdentifier:@"F13"];
	cell.value = @34.65;
	cell = [spreadSheet spreadSheetCellForIdentifier:@"G13"];
	cell.value = @0.363;
	cell = [spreadSheet spreadSheetCellForIdentifier:@"H13"];
	cell.watchList = [spreadSheet arrayOfCells:@[@"F13", @"G13"]];
	[cell setFunction:^NSNumber *(IMSpreadSheet *calculator, IMSpreadSheetCell *cell) {
		return [spreadSheet multiplyCells:@[@"F13", @"G13"]];
	}];
	
	cell = [spreadSheet spreadSheetCellForIdentifier:@"F14"];
	cell.value = @139.04;
	cell = [spreadSheet spreadSheetCellForIdentifier:@"G14"];
	cell.value = @0.063;
	cell = [spreadSheet spreadSheetCellForIdentifier:@"H14"];
	cell.watchList = [spreadSheet arrayOfCells:@[@"F14", @"G14"]];
	[cell setFunction:^NSNumber *(IMSpreadSheet *calculator, IMSpreadSheetCell *cell) {
		return [calculator multiplyCells:@[@"F14", @"G14"]];
	}];
	
	cell = [spreadSheet spreadSheetCellForIdentifier:@"F15"];
	cell.value = @26.9;
	cell = [spreadSheet spreadSheetCellForIdentifier:@"G15"];
	cell.value = @0.225;
	cell = [spreadSheet spreadSheetCellForIdentifier:@"H15"];
	cell.watchList = [spreadSheet arrayOfCells:@[@"F15", @"G15"]];
	[cell setFunction:^NSNumber *(IMSpreadSheet *calculator, IMSpreadSheetCell *cell) {
		return [calculator multiplyCells:@[@"F15", @"G15"]];
	}];
	
	cell = [spreadSheet spreadSheetCellForIdentifier:@"F16"];
	cell.value = @23.27;
	cell = [spreadSheet spreadSheetCellForIdentifier:@"G16"];
	cell.value = @0.19;
	cell = [spreadSheet spreadSheetCellForIdentifier:@"H16"];
	cell.watchList = [spreadSheet arrayOfCells:@[@"F16", @"G16"]];
	[cell setFunction:^NSNumber *(IMSpreadSheet *calculator, IMSpreadSheetCell *cell) {
		return [calculator multiplyCells:@[@"F16", @"G16"]];
	}];
	
	cell = [spreadSheet spreadSheetCellForIdentifier:@"C18"];
	cell.watchList = [spreadSheet arrayOfCells:@[@"C12", @"C13", @"C14", @"C15", @"C16"]];
	[cell setFunction:^NSNumber *(IMSpreadSheet *calculator, IMSpreadSheetCell *cell) {
		return [calculator sumOfCellRange:@"C12:C16"];
	}];
	
	cell = [spreadSheet spreadSheetCellForIdentifier:@"C19"];
	cell.watchList = [spreadSheet arrayOfCells:@[@"D12", @"D13", @"D14", @"D15", @"D16"]];
	[cell setFunction:^NSNumber *(IMSpreadSheet *calculator, IMSpreadSheetCell *cell) {
		return [calculator sumOfCellRange:@"D12:D16"];
	}];
	
	
	cell = [spreadSheet spreadSheetCellForIdentifier:@"G18"];
	cell.watchList = [spreadSheet arrayOfCells:@[@"G12", @"G13", @"G14", @"G15", @"G16"]];
	[cell setFunction:^NSNumber *(IMSpreadSheet *calculator, IMSpreadSheetCell *cell) {
		return [calculator sumOfCellRange:@"G12:G16"];
	}];
	
	cell = [spreadSheet spreadSheetCellForIdentifier:@"G19"];
	cell.watchList = [spreadSheet arrayOfCells:@[@"H12", @"H13", @"H14", @"H15", @"H16"]];
	[cell setFunction:^NSNumber *(IMSpreadSheet *calculator, IMSpreadSheetCell *cell) {
		return [calculator sumOfCellRange:@"H12:H16"];
	}];
}

- (int)rowForAnnualEmployeesWhoReceivedTSpot {
	IMSpreadSheet *ssCharts = [self spreadSheetWithName:kIMTvTCharts];
	for(int i=41;i<54;i++){
		NSString *location = [NSString stringWithFormat:@"M%d", i];
		NSString *s2 = [ssCharts spreadSheetCellForIdentifier:location].textValue;
		if([s2 containsString:@"Annual employees who received the"]){
			return i;
		}
	}
	return 0;
}

- (void) initChartsAndTotals
{
	IMSpreadSheet *spreadSheet = [self spreadSheetWithName:kIMTvTCharts];
	IMSpreadSheet *ssScreening = [self spreadSheetWithName:kIMTvTAnnualScreening];
	IMSpreadSheet *ssNewHire = [self spreadSheetWithName:kIMTvTNewHireScreening];
	IMSpreadSheet *ssAnalysis = [self spreadSheetWithName:kIMTvTAnalysis];
	IMSpreadSheet *ssEmpCost = [self spreadSheetWithName:kIMTvTEmpCostData];
	IMSpreadSheet *ssEmpData = [self spreadSheetWithName:kIMTvTEmpTimeData];
	IMSpreadSheet *ssTspot = [self spreadSheetWithName:kIMTvTTSPOT];
	
	IMSpreadSheetCell *cell = [spreadSheet spreadSheetCellForIdentifier:@"B2"];
	cell.watchList = @[[ssScreening spreadSheetCellForIdentifier:@"B5"]
					   ,[ssScreening spreadSheetCellForIdentifier:@"B10"]
					   ,[ssNewHire spreadSheetCellForIdentifier:@"B8"]
					   ,[ssNewHire spreadSheetCellForIdentifier:@"B4"]
					   ,[ssAnalysis spreadSheetCellForIdentifier:@"B45"]
					   ,[ssAnalysis spreadSheetCellForIdentifier:@"B49"]];
	[cell setFunction:^NSNumber *(IMSpreadSheet *calculator, IMSpreadSheetCell *cell) {
		double val1 = [ssScreening valueForIdentifier:@"B5"].doubleValue * [ssScreening valueForIdentifier:@"B10"].doubleValue;
		double val2 = [ssNewHire valueForIdentifier:@"B8"].doubleValue * [ssNewHire valueForIdentifier:@"B4"].doubleValue * 2.0;
		double val3 = val1 + val2 + [ssAnalysis valueForIdentifier:@"B45"].doubleValue + [ssAnalysis valueForIdentifier:@"B49"].doubleValue;
		return [NSNumber numberWithDouble:val3];
	}];
	
	cell = [spreadSheet spreadSheetCellForIdentifier:@"C2"];
	cell.watchList = @[[ssTspot spreadSheetCellForIdentifier:@"B2"]
					   ,[ssScreening spreadSheetCellForIdentifier:@"B5"]
					   ,[ssNewHire spreadSheetCellForIdentifier:@"C8"]
					   ,[ssNewHire spreadSheetCellForIdentifier:@"B4"]
					   ,[ssAnalysis spreadSheetCellForIdentifier:@"C45"]];
	[cell setFunction:^NSNumber *(IMSpreadSheet *calculator, IMSpreadSheetCell *cell) {
		double val1 = [ssTspot valueForIdentifier:@"B2"].doubleValue * [ssScreening valueForIdentifier:@"B5"].doubleValue;
		double val2 = [ssTspot valueForIdentifier:@"B2"].doubleValue * [ssNewHire valueForIdentifier:@"B4"].doubleValue;
		double val3 = val1 + val2 + [ssAnalysis valueForIdentifier:@"C45"].doubleValue;
		return [NSNumber numberWithDouble:val3];
	}];
	
	cell = [spreadSheet spreadSheetCellForIdentifier:@"B3"];
	cell.watchList = @[[spreadSheet spreadSheetCellForIdentifier:@"B10"]
					   ,[ssEmpCost spreadSheetCellForIdentifier:@"B8"]
					   ,[ssAnalysis spreadSheetCellForIdentifier:@"B12"]];
	[cell setFunction:^NSNumber *(IMSpreadSheet *calculator, IMSpreadSheetCell *cell) {
		double val1 = [spreadSheet valueForIdentifier:@"B10"].doubleValue * [ssEmpCost valueForIdentifier:@"B8"].doubleValue;
		return [NSNumber numberWithDouble:val1 + [ssAnalysis valueForIdentifier:@"B12"].doubleValue];
	}];
	
	cell = [spreadSheet spreadSheetCellForIdentifier:@"C3"];
	cell.watchList = @[[spreadSheet spreadSheetCellForIdentifier:@"C10"]
					   ,[ssEmpCost spreadSheetCellForIdentifier:@"B8"]];
	[cell setFunction:^NSNumber *(IMSpreadSheet *calculator, IMSpreadSheetCell *cell) {
		double val1 = [spreadSheet valueForIdentifier:@"C10"].doubleValue * [ssEmpCost valueForIdentifier:@"B8"].doubleValue;
		return [NSNumber numberWithDouble:val1];
	}];
	
	cell = [spreadSheet spreadSheetCellForIdentifier:@"B4"];
	cell.watchList = @[[ssAnalysis spreadSheetCellForIdentifier:@"B11"]
					   ,[ssScreening spreadSheetCellForIdentifier:@"B5"]
					   ,[ssAnalysis spreadSheetCellForIdentifier:@"B29"]
					   ,[ssNewHire spreadSheetCellForIdentifier:@"B4"]
					   ,[ssAnalysis spreadSheetCellForIdentifier:@"B30"]];
	[cell setFunction:^NSNumber *(IMSpreadSheet *calculator, IMSpreadSheetCell *cell) {
		double val1 = [ssAnalysis valueForIdentifier:@"B11"].doubleValue * [ssScreening valueForIdentifier:@"B5"].doubleValue;
		double val2 = [ssAnalysis valueForIdentifier:@"B29"].doubleValue * [ssNewHire valueForIdentifier:@"B4"].doubleValue;
		double val3 = [ssAnalysis valueForIdentifier:@"B30"].doubleValue * [ssNewHire valueForIdentifier:@"B4"].doubleValue;
		return [NSNumber numberWithDouble:val1 + val2 + val3];
	}];
	
	cell = [spreadSheet spreadSheetCellForIdentifier:@"C4"];
	cell.watchList = @[[ssAnalysis spreadSheetCellForIdentifier:@"C11"]
					   ,[ssScreening spreadSheetCellForIdentifier:@"B5"]
					   ,[ssAnalysis spreadSheetCellForIdentifier:@"C29"]
					   ,[ssNewHire spreadSheetCellForIdentifier:@"B4"]
					   ,[ssAnalysis spreadSheetCellForIdentifier:@"C30"]];
	[cell setFunction:^NSNumber *(IMSpreadSheet *calculator, IMSpreadSheetCell *cell) {
		double val1 = [ssAnalysis valueForIdentifier:@"C11"].doubleValue * [ssScreening valueForIdentifier:@"B5"].doubleValue;
		double val2 = [ssAnalysis valueForIdentifier:@"C29"].doubleValue * [ssNewHire valueForIdentifier:@"B4"].doubleValue;
		double val3 = [ssAnalysis valueForIdentifier:@"C30"].doubleValue * [ssNewHire valueForIdentifier:@"B4"].doubleValue;
		return [NSNumber numberWithDouble:val1+val2+val3];
	}];
	
	cell = [spreadSheet spreadSheetCellForIdentifier:@"B6"];
	cell.watchList = [spreadSheet arrayOfCells:@[@"B2", @"B3", @"B4"]];
	[cell setFunction:^NSNumber *(IMSpreadSheet *calculator, IMSpreadSheetCell *cell) {
		return [spreadSheet sumOfCellRange:@"B2:B4"];
	}];
	
	cell = [spreadSheet spreadSheetCellForIdentifier:@"C6"];
	cell.watchList = [spreadSheet arrayOfCells:@[@"C2", @"C3", @"C4"]];
	[cell setFunction:^NSNumber *(IMSpreadSheet *calculator, IMSpreadSheetCell *cell) {
		return [spreadSheet sumOfCellRange:@"C2:C4"];
	}];
	
	cell = [spreadSheet spreadSheetCellForIdentifier:@"D6"];
	cell.watchList = [spreadSheet arrayOfCells:@[@"B6", @"C6"]];
	[cell setFunction:^NSNumber *(IMSpreadSheet *calculator, IMSpreadSheetCell *cell) {
		double val1 = [spreadSheet valueForIdentifier:@"B6"].doubleValue - [spreadSheet valueForIdentifier:@"C6"].doubleValue;
		return [NSNumber numberWithDouble:floor(val1)];
	}];
	
	cell = [spreadSheet spreadSheetCellForIdentifier:@"E6"];
	cell.watchList = [spreadSheet arrayOfCells:@[@"B6", @"C6"]];
	[cell setFunction:^NSNumber *(IMSpreadSheet *calculator, IMSpreadSheetCell *cell) {
		double val1 = 1.0 - ([spreadSheet valueForIdentifier:@"C6"].doubleValue / [spreadSheet valueForIdentifier:@"B6"].doubleValue);
		return [NSNumber numberWithDouble:val1];
	}];
	
	cell = [spreadSheet spreadSheetCellForIdentifier:@"B10"];
	cell.watchList = @[[ssAnalysis spreadSheetCellForIdentifier:@"B3"]
					   ,[ssScreening spreadSheetCellForIdentifier:@"B5"]
					   ,[ssAnalysis spreadSheetCellForIdentifier:@"B4"]
					   ,[ssAnalysis spreadSheetCellForIdentifier:@"B15"]
					   ,[ssAnalysis spreadSheetCellForIdentifier:@"B20"]
					   ,[ssAnalysis spreadSheetCellForIdentifier:@"B21"]
					   ,[ssNewHire spreadSheetCellForIdentifier:@"B4"]
					   ,[ssAnalysis spreadSheetCellForIdentifier:@"B34"]
					   ,[ssAnalysis spreadSheetCellForIdentifier:@"B35"]
					   ,[ssAnalysis spreadSheetCellForIdentifier:@"B41"]];
	[cell setFunction:^NSNumber *(IMSpreadSheet *calculator, IMSpreadSheetCell *cell) {
		double val1 = [ssAnalysis valueForIdentifier:@"B3"].doubleValue * [ssScreening valueForIdentifier:@"B5"].doubleValue / 60.0;
		double val2 = [ssAnalysis valueForIdentifier:@"B4"].doubleValue * [ssAnalysis valueForIdentifier:@"B15"].doubleValue / 60.0;
		double val3 = [ssAnalysis valueForIdentifier:@"B20"].doubleValue * [ssNewHire valueForIdentifier:@"B4"].doubleValue;
		double val4 = [ssAnalysis valueForIdentifier:@"B21"].doubleValue * [ssAnalysis valueForIdentifier:@"B34"].doubleValue;
		double val5 = [ssAnalysis valueForIdentifier:@"B21"].doubleValue * [ssAnalysis valueForIdentifier:@"B35"].doubleValue;
		double val6 = [ssAnalysis valueForIdentifier:@"B41"].doubleValue;
		double val7 = val1 + val2 + ((val3 + val4 + val5) / 60.0) + val6;
		return [NSNumber numberWithDouble:val7];
	}];
	
	cell = [spreadSheet spreadSheetCellForIdentifier:@"C10"];
	cell.watchList = @[[ssAnalysis spreadSheetCellForIdentifier:@"C3"]
					   ,[ssAnalysis spreadSheetCellForIdentifier:@"C4"]
					   ,[ssScreening spreadSheetCellForIdentifier:@"B5"]
					   ,[ssAnalysis spreadSheetCellForIdentifier:@"C20"]
					   ,[ssAnalysis spreadSheetCellForIdentifier:@"C21"]
					   ,[ssNewHire spreadSheetCellForIdentifier:@"B4"]
					   ,[ssAnalysis spreadSheetCellForIdentifier:@"C41"]];
	[cell setFunction:^NSNumber *(IMSpreadSheet *calculator, IMSpreadSheetCell *cell) {
		double val1 = [ssAnalysis valueForIdentifier:@"C3"].doubleValue + [ssAnalysis valueForIdentifier:@"C4"].doubleValue;
		double val2 = (val1 * [ssScreening valueForIdentifier:@"B5"].doubleValue) / 60.0;
		// getting stuck in a loop here for some reason
//		if(val2 == 0)
//		{
//			return @0.0;
//		}
		double val3 = (([ssAnalysis valueForIdentifier:@"C20"].doubleValue + [ssAnalysis valueForIdentifier:@"C21"].doubleValue) * [ssNewHire valueForIdentifier:@"B4"].doubleValue) / 60.0;
		double val4 = val2 + val3 + [ssAnalysis valueForIdentifier:@"C41"].doubleValue;
		return [NSNumber numberWithDouble:val4];
	}];
	
	cell = [spreadSheet spreadSheetCellForIdentifier:@"B11"];
	cell.watchList = @[[ssAnalysis spreadSheetCellForIdentifier:@"B5"]
					   ,[ssScreening spreadSheetCellForIdentifier:@"B5"]
					   ,[ssAnalysis spreadSheetCellForIdentifier:@"B22"]
					   ,[ssAnalysis spreadSheetCellForIdentifier:@"B23"]
					   ,[ssNewHire spreadSheetCellForIdentifier:@"B4"]
					   ,[ssAnalysis spreadSheetCellForIdentifier:@"B42"]];
	[cell setFunction:^NSNumber *(IMSpreadSheet *calculator, IMSpreadSheetCell *cell) {
		double val1 = [ssAnalysis valueForIdentifier:@"B5"].doubleValue / 60.0;
		double val2 = val1 * [ssScreening valueForIdentifier:@"B5"].doubleValue;
		double val3 = [ssAnalysis valueForIdentifier:@"B22"].doubleValue + [ssAnalysis valueForIdentifier:@"B23"].doubleValue;
		double val4 = (val3 * [ssNewHire valueForIdentifier:@"B4"].doubleValue) / 60.0;
		double val5 = val2 + val4 + [ssAnalysis valueForIdentifier:@"B42"].doubleValue;
		return [NSNumber numberWithDouble:val5];
	}];
	
	cell = [spreadSheet spreadSheetCellForIdentifier:@"C11"];
	cell.watchList = @[[ssAnalysis spreadSheetCellForIdentifier:@"C5"]
					   ,[ssScreening spreadSheetCellForIdentifier:@"B5"]
					   ,[ssAnalysis spreadSheetCellForIdentifier:@"C22"]
					   ,[ssNewHire spreadSheetCellForIdentifier:@"B4"]
					   ,[ssAnalysis spreadSheetCellForIdentifier:@"C42"]];
	[cell setFunction:^NSNumber *(IMSpreadSheet *calculator, IMSpreadSheetCell *cell) {
		double val1 = [ssAnalysis valueForIdentifier:@"C5"].doubleValue * [ssScreening valueForIdentifier:@"B5"].doubleValue / 60.0;
		double val2 = [ssAnalysis valueForIdentifier:@"C22"].doubleValue * [ssNewHire valueForIdentifier:@"B4"].doubleValue / 60.0;
		double val3 = val1 + val2 + [ssAnalysis valueForIdentifier:@"C42"].doubleValue;
		return [NSNumber numberWithDouble:val3];
	}];
	
	cell = [spreadSheet spreadSheetCellForIdentifier:@"B12"];
	cell.watchList = [spreadSheet arrayOfCells:@[@"B10", @"B11"]];
	[cell setFunction:^NSNumber *(IMSpreadSheet *calculator, IMSpreadSheetCell *cell) {
		return [spreadSheet sumOfCellRange:@"B10:B11"];
	}];
	
	cell = [spreadSheet spreadSheetCellForIdentifier:@"C12"];
	cell.watchList = [spreadSheet arrayOfCells:@[@"C10", @"C11"]];
	[cell setFunction:^NSNumber *(IMSpreadSheet *calculator, IMSpreadSheetCell *cell) {
		return [spreadSheet sumOfCellRange:@"C10:C11"];
	}];
	
	cell = [spreadSheet spreadSheetCellForIdentifier:@"D12"];
	cell.watchList = [spreadSheet arrayOfCells:@[@"B12", @"C12"]];
	[cell setFunction:^NSNumber *(IMSpreadSheet *calculator, IMSpreadSheetCell *cell) {
		double val1 = round([spreadSheet valueForIdentifier:@"B12"].doubleValue) - round([spreadSheet valueForIdentifier:@"C12"].doubleValue);
		return [NSNumber numberWithDouble:floor(val1)];
	}];
	
	cell = [spreadSheet spreadSheetCellForIdentifier:@"E12"];
	cell.watchList = [spreadSheet arrayOfCells:@[@"B12", @"C12"]];
	[cell setFunction:^NSNumber *(IMSpreadSheet *calculator, IMSpreadSheetCell *cell) {
		double val1 = 1.0 - ([spreadSheet valueForIdentifier:@"C12"].doubleValue / [spreadSheet valueForIdentifier:@"B12"].doubleValue);
		return [NSNumber numberWithDouble:val1];
	}];
	
	cell = [spreadSheet spreadSheetCellForIdentifier:@"B16"];
	cell.watchList = @[[ssScreening spreadSheetCellForIdentifier:@"B5"]];
	[cell setFunction:^NSNumber *(IMSpreadSheet *calculator, IMSpreadSheetCell *cell) {
		double val1 = [ssScreening valueForIdentifier:@"B5"].doubleValue * 2.0;
		return [NSNumber numberWithDouble:val1];
	}];
	
	cell = [spreadSheet spreadSheetCellForIdentifier:@"C16"];
	cell.watchList = @[[ssScreening spreadSheetCellForIdentifier:@"B5"]];
	[cell setFunction:^NSNumber *(IMSpreadSheet *calculator, IMSpreadSheetCell *cell) {
		return [ssScreening valueForIdentifier:@"B5"];
	}];
	
	cell = [spreadSheet spreadSheetCellForIdentifier:@"B17"];
	cell.watchList = @[[ssNewHire spreadSheetCellForIdentifier:@"B4"]
					   ,[ssNewHire spreadSheetCellForIdentifier:@"B11"]
					   ,[ssNewHire spreadSheetCellForIdentifier:@"B12"]
					   ,[ssEmpData spreadSheetCellForIdentifier:@"B35"]];
	[cell setFunction:^NSNumber *(IMSpreadSheet *calculator, IMSpreadSheetCell *cell) {
		double val1 = ([ssNewHire valueForIdentifier:@"B4"].doubleValue * [ssEmpData valueForIdentifier:@"B35"].doubleValue) + ([ssNewHire valueForIdentifier:@"B11"].doubleValue * [ssNewHire valueForIdentifier:@"B4"].doubleValue) + ([ssNewHire valueForIdentifier:@"B4"].doubleValue * [ssNewHire valueForIdentifier:@"B12"].doubleValue);
		return [NSNumber numberWithDouble:val1];
	}];
	
	cell = [spreadSheet spreadSheetCellForIdentifier:@"C17"];
	cell.watchList = @[[ssNewHire spreadSheetCellForIdentifier:@"B4"]
					   ,[ssTspot spreadSheetCellForIdentifier:@"B7"]];
	[cell setFunction:^NSNumber *(IMSpreadSheet *calculator, IMSpreadSheetCell *cell) {
		double val1 = [ssNewHire valueForIdentifier:@"B4"].doubleValue + ([ssTspot valueForIdentifier:@"B7"].doubleValue * [ssNewHire valueForIdentifier:@"B4"].doubleValue);
		return [NSNumber numberWithDouble:val1];
	}];
	
	cell = [spreadSheet spreadSheetCellForIdentifier:@"D17"];
	cell.watchList = [spreadSheet arrayOfCells:@[@"B17", @"C17"]];
	[cell setFunction:^NSNumber *(IMSpreadSheet *calculator, IMSpreadSheetCell *cell) {
		double val1 = [spreadSheet valueForIdentifier:@"B17"].doubleValue - [spreadSheet valueForIdentifier:@"C17"].doubleValue;
		return [NSNumber numberWithDouble:floor(val1)];
	}];
	
	cell = [spreadSheet spreadSheetCellForIdentifier:@"B18"];
	cell.watchList = [spreadSheet arrayOfCells:@[@"B16", @"B17"]];
	[cell setFunction:^NSNumber *(IMSpreadSheet *calculator, IMSpreadSheetCell *cell) {
		return [spreadSheet sumOfCellRange:@"B16:B17"];
	}];
	
	cell = [spreadSheet spreadSheetCellForIdentifier:@"C18"];
	cell.watchList = [spreadSheet arrayOfCells:@[@"C16", @"C17"]];
	[cell setFunction:^NSNumber *(IMSpreadSheet *calculator, IMSpreadSheetCell *cell) {
		return [spreadSheet sumOfCellRange:@"C16:C17"];
	}];
	
	cell = [spreadSheet spreadSheetCellForIdentifier:@"D18"];
	cell.watchList = [spreadSheet arrayOfCells:@[@"B18", @"C18"]];
	[cell setFunction:^NSNumber *(IMSpreadSheet *calculator, IMSpreadSheetCell *cell) {
		double val1 = [spreadSheet valueForIdentifier:@"B18"].doubleValue - [spreadSheet valueForIdentifier:@"C18"].doubleValue;
		return [NSNumber numberWithDouble:floor(val1)];
	}];
	
	cell = [spreadSheet spreadSheetCellForIdentifier:@"E18"];
	cell.watchList = [spreadSheet arrayOfCells:@[@"B18", @"C18"]];
	[cell setFunction:^NSNumber *(IMSpreadSheet *calculator, IMSpreadSheetCell *cell) {
		double val1 = 1.0 - ([spreadSheet valueForIdentifier:@"C18"].doubleValue / [spreadSheet valueForIdentifier:@"B18"].doubleValue);
		return [NSNumber numberWithDouble:val1];
	}];
	
	// Phased approach col M+ and rows 39+
	
	// annual B5, new hires B4, cost per test B2
	
	cell = [spreadSheet spreadSheetCellForIdentifier:@"M39"]; // number of rows
	cell.value = @6;
	
	[spreadSheet spreadSheetCellForIdentifier:@"N39"].value = @3; // number of years
	
	[spreadSheet spreadSheetCellForIdentifier:@"M40"].textValue = @"New Hires";
	[spreadSheet spreadSheetCellForIdentifier:@"M41"].textValue = @"Volunteers";
	[spreadSheet spreadSheetCellForIdentifier:@"M42"].textValue = @"Contact Exposures";
	[spreadSheet spreadSheetCellForIdentifier:@"M43"].textValue = @"*Employees removed from TB testing pool (symptom screen only)";
	[spreadSheet spreadSheetCellForIdentifier:@"M44"].textValue = @"Annual employees who received the T-SPOT.TB test the previous year upon new hire";
	[spreadSheet spreadSheetCellForIdentifier:@"M45"].textValue = @"Annual Employees";
	
	
	cell = [spreadSheet spreadSheetCellForIdentifier:@"N55"];
	cell.watchList = [spreadSheet arrayOfCells:@[@"N40", @"N41", @"N42", @"N43", @"N44", @"N45", @"N46", @"N47", @"N48", @"N49", @"N50", @"N51", @"N52", @"N53", @"N54"]];
	[cell setFunction:^NSNumber *(IMSpreadSheet *calculator, IMSpreadSheetCell *cell) {
		return [calculator sumOfCellRange:@"N40:N54"];
	}];
	
	cell = [spreadSheet spreadSheetCellForIdentifier:@"N56"];
	[cell setSetterFunction:^(IMSpreadSheetCell *cell) {
		NSArray *a = @[@"P", @"Q", @"R", @"S", @"T", @"U", @"V", @"W", @"X", @"Y"];
		[a enumerateObjectsUsingBlock:^(NSString *  _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
			NSString *cellId = [NSString stringWithFormat:@"%@56", obj];
			[spreadSheet spreadSheetCellForIdentifier:cellId].value = cell.value;
			cellId = [NSString stringWithFormat:@"%@56", obj];
			[spreadSheet spreadSheetCellForIdentifier:cellId].value = cell.value;
		}];
	}];
	
	static NSString* newHires = @"New Hires";
	
	cell = [spreadSheet spreadSheetCellForIdentifier:@"P40"];
	[cell setSetterFunction:^(IMSpreadSheetCell *cell) {
		if([[spreadSheet spreadSheetCellForIdentifier:@"M40"].textValue isEqualToString:newHires]){
			int i = [self rowForAnnualEmployeesWhoReceivedTSpot];
			NSString *location = [NSString stringWithFormat:@"Q%d", i];
			[spreadSheet spreadSheetCellForIdentifier:location].value = cell.value;
		}
	}];
	
	cell = [spreadSheet spreadSheetCellForIdentifier:@"Q40"];
	[cell setSetterFunction:^(IMSpreadSheetCell *cell) {
		if([[spreadSheet spreadSheetCellForIdentifier:@"M40"].textValue isEqualToString:newHires]){
			int i = [self rowForAnnualEmployeesWhoReceivedTSpot];
			NSString *location = [NSString stringWithFormat:@"R%d", i];
			[spreadSheet spreadSheetCellForIdentifier:location].value = cell.value;
		}
	}];
	
	cell = [spreadSheet spreadSheetCellForIdentifier:@"R40"];
	[cell setSetterFunction:^(IMSpreadSheetCell *cell) {
		if([[spreadSheet spreadSheetCellForIdentifier:@"M40"].textValue isEqualToString:newHires]){
			int i = [self rowForAnnualEmployeesWhoReceivedTSpot];
			NSString *location = [NSString stringWithFormat:@"S%d", i];
			[spreadSheet spreadSheetCellForIdentifier:location].value = cell.value;
		}
	}];
	
	cell = [spreadSheet spreadSheetCellForIdentifier:@"S40"];
	[cell setSetterFunction:^(IMSpreadSheetCell *cell) {
		if([[spreadSheet spreadSheetCellForIdentifier:@"M40"].textValue isEqualToString:newHires]){
			int i = [self rowForAnnualEmployeesWhoReceivedTSpot];
			NSString *location = [NSString stringWithFormat:@"T%d", i];
			[spreadSheet spreadSheetCellForIdentifier:location].value = cell.value;
		}
	}];
	
	cell = [spreadSheet spreadSheetCellForIdentifier:@"T40"];
	[cell setSetterFunction:^(IMSpreadSheetCell *cell) {
		if([[spreadSheet spreadSheetCellForIdentifier:@"M40"].textValue isEqualToString:newHires]){
			int i = [self rowForAnnualEmployeesWhoReceivedTSpot];
			NSString *location = [NSString stringWithFormat:@"U%d", i];
			[spreadSheet spreadSheetCellForIdentifier:location].value = cell.value;
		}
	}];
	
	cell = [spreadSheet spreadSheetCellForIdentifier:@"U40"];
	[cell setSetterFunction:^(IMSpreadSheetCell *cell) {
		if([[spreadSheet spreadSheetCellForIdentifier:@"M40"].textValue isEqualToString:newHires]){
			int i = [self rowForAnnualEmployeesWhoReceivedTSpot];
			NSString *location = [NSString stringWithFormat:@"V%d", i];
			[spreadSheet spreadSheetCellForIdentifier:location].value = cell.value;
		}
	}];
	
	cell = [spreadSheet spreadSheetCellForIdentifier:@"V40"];
	[cell setSetterFunction:^(IMSpreadSheetCell *cell) {
		if([[spreadSheet spreadSheetCellForIdentifier:@"M40"].textValue isEqualToString:newHires]){
			int i = [self rowForAnnualEmployeesWhoReceivedTSpot];
			NSString *location = [NSString stringWithFormat:@"W%d", i];
			[spreadSheet spreadSheetCellForIdentifier:location].value = cell.value;
		}
	}];
	
	cell = [spreadSheet spreadSheetCellForIdentifier:@"W40"];
	[cell setSetterFunction:^(IMSpreadSheetCell *cell) {
		if([[spreadSheet spreadSheetCellForIdentifier:@"M40"].textValue isEqualToString:newHires]){
			int i = [self rowForAnnualEmployeesWhoReceivedTSpot];
			NSString *location = [NSString stringWithFormat:@"X%d", i];
			[spreadSheet spreadSheetCellForIdentifier:location].value = cell.value;
		}
	}];
	
	cell = [spreadSheet spreadSheetCellForIdentifier:@"X40"];
	[cell setSetterFunction:^(IMSpreadSheetCell *cell) {
		if([[spreadSheet spreadSheetCellForIdentifier:@"M40"].textValue isEqualToString:newHires]){
			int i = [self rowForAnnualEmployeesWhoReceivedTSpot];
			NSString *location = [NSString stringWithFormat:@"Y%d", i];
			[spreadSheet spreadSheetCellForIdentifier:location].value = cell.value;
		}
	}];
	
	cell = [spreadSheet spreadSheetCellForIdentifier:@"P55"];
	cell.watchList = [spreadSheet arrayOfCells:@[@"P40", @"P41", @"P42", @"P43", @"P44", @"P45", @"P46", @"P47", @"P48", @"P49", @"P50", @"P51", @"P52", @"P53", @"P54"]];
	[cell setFunction:^NSNumber *(IMSpreadSheet *calculator, IMSpreadSheetCell *cell) {
//		return [NSNumber numberWithInt:[calculator sumOfCellRange:@"P45:P54"].intValue + [calculator sumOfCellRange:@"P40:P43"].intValue - [calculator valueForIdentifier:@"P44"].intValue];
		return [calculator sumOfCellRange:@"P40:P54"];
	}];
	
	cell = [spreadSheet spreadSheetCellForIdentifier:@"Q55"];
	cell.watchList = [spreadSheet arrayOfCells:@[@"Q40", @"Q41", @"Q42", @"Q43", @"Q44", @"Q45", @"Q46", @"Q47", @"Q48", @"Q49", @"Q50", @"Q51", @"Q52", @"Q53", @"Q54"]];
	[cell setFunction:^NSNumber *(IMSpreadSheet *calculator, IMSpreadSheetCell *cell) {
//		return [NSNumber numberWithInt:[calculator sumOfCellRange:@"Q45:Q54"].intValue + [calculator sumOfCellRange:@"Q40:Q43"].intValue - [calculator valueForIdentifier:@"Q44"].intValue];
		return [calculator sumOfCellRange:@"Q40:Q54"];
	}];
	
	cell = [spreadSheet spreadSheetCellForIdentifier:@"R55"];
	cell.watchList = [spreadSheet arrayOfCells:@[@"R40", @"R41", @"R42", @"R43", @"R44", @"R45", @"R46", @"R47", @"R48", @"R49", @"R50", @"R51", @"R52", @"R53", @"R54"]];
	[cell setFunction:^NSNumber *(IMSpreadSheet *calculator, IMSpreadSheetCell *cell) {
//		return [NSNumber numberWithInt:[calculator sumOfCellRange:@"R45:R54"].intValue + [calculator sumOfCellRange:@"R40:R43"].intValue - [calculator valueForIdentifier:@"R44"].intValue];
		return [calculator sumOfCellRange:@"R40:R54"];
	}];
	
	cell = [spreadSheet spreadSheetCellForIdentifier:@"S55"];
	cell.watchList = [spreadSheet arrayOfCells:@[@"S40", @"S41", @"S42", @"S43", @"S44", @"S45", @"S46", @"S47", @"S48", @"S49", @"S50", @"S51", @"S52", @"S53", @"S54"]];
	[cell setFunction:^NSNumber *(IMSpreadSheet *calculator, IMSpreadSheetCell *cell) {
//		return [NSNumber numberWithInt:[calculator sumOfCellRange:@"S45:S54"].intValue + [calculator sumOfCellRange:@"S40:S43"].intValue - [calculator valueForIdentifier:@"S44"].intValue];
		return [calculator sumOfCellRange:@"S40:S54"];
	}];
	
	cell = [spreadSheet spreadSheetCellForIdentifier:@"T55"];
	cell.watchList = [spreadSheet arrayOfCells:@[@"T40", @"T41", @"T42", @"T43", @"T44", @"T45", @"T46", @"T47", @"T48", @"T49", @"T50", @"T51", @"T52", @"T53", @"T54"]];
	[cell setFunction:^NSNumber *(IMSpreadSheet *calculator, IMSpreadSheetCell *cell) {
//		return [NSNumber numberWithInt:[calculator sumOfCellRange:@"T45:T54"].intValue + [calculator sumOfCellRange:@"T40:T43"].intValue - [calculator valueForIdentifier:@"T44"].intValue];
		return [calculator sumOfCellRange:@"T40:T54"];
	}];
	
	cell = [spreadSheet spreadSheetCellForIdentifier:@"U55"];
	cell.watchList = [spreadSheet arrayOfCells:@[@"U40", @"U41", @"U42", @"U43", @"U44", @"U45", @"U46", @"U47", @"U48", @"U49", @"U50", @"U51", @"U52", @"U53", @"U54"]];
	[cell setFunction:^NSNumber *(IMSpreadSheet *calculator, IMSpreadSheetCell *cell) {
//		return [NSNumber numberWithInt:[calculator sumOfCellRange:@"U45:U54"].intValue + [calculator sumOfCellRange:@"U40:U43"].intValue - [calculator valueForIdentifier:@"U44"].intValue];
		return [calculator sumOfCellRange:@"U40:U54"];
	}];
	
	cell = [spreadSheet spreadSheetCellForIdentifier:@"V55"];
	cell.watchList = [spreadSheet arrayOfCells:@[@"V40", @"V41", @"V42", @"V43", @"V44", @"V45", @"V46", @"V47", @"V48", @"V49", @"V50", @"V51", @"V52", @"V53", @"V54"]];
	[cell setFunction:^NSNumber *(IMSpreadSheet *calculator, IMSpreadSheetCell *cell) {
//		return [NSNumber numberWithInt:[calculator sumOfCellRange:@"V45:V54"].intValue + [calculator sumOfCellRange:@"V40:V43"].intValue - [calculator valueForIdentifier:@"V44"].intValue];
		return [calculator sumOfCellRange:@"V40:V54"];
	}];
	
	cell = [spreadSheet spreadSheetCellForIdentifier:@"W55"];
	cell.watchList = [spreadSheet arrayOfCells:@[@"W40", @"W41", @"W42", @"W43", @"W44", @"W45", @"W46", @"W47", @"W48", @"W49", @"W50", @"W51", @"W52", @"W53", @"W54"]];
	[cell setFunction:^NSNumber *(IMSpreadSheet *calculator, IMSpreadSheetCell *cell) {
//		return [NSNumber numberWithInt:[calculator sumOfCellRange:@"W45:W54"].intValue + [calculator sumOfCellRange:@"W40:W43"].intValue - [calculator valueForIdentifier:@"W44"].intValue];
		return [calculator sumOfCellRange:@"W40:W54"];
	}];
	
	cell = [spreadSheet spreadSheetCellForIdentifier:@"X55"];
	cell.watchList = [spreadSheet arrayOfCells:@[@"X40", @"X41", @"X42", @"X43", @"X44", @"X45", @"X46", @"X47", @"X48", @"X49", @"X50", @"X51", @"X52", @"X53", @"X54"]];
	[cell setFunction:^NSNumber *(IMSpreadSheet *calculator, IMSpreadSheetCell *cell) {
//		return [NSNumber numberWithInt:[calculator sumOfCellRange:@"X45:X54"].intValue + [calculator sumOfCellRange:@"X40:X43"].intValue - [calculator valueForIdentifier:@"X44"].intValue];
		return [calculator sumOfCellRange:@"X40:X54"];
	}];
	
	cell = [spreadSheet spreadSheetCellForIdentifier:@"Y55"];
	cell.watchList = [spreadSheet arrayOfCells:@[@"Y40", @"Y41", @"Y42", @"Y43", @"Y44", @"Y45", @"Y46", @"Y47", @"Y48", @"Y49", @"Y50", @"Y51", @"Y52", @"Y53", @"Y54"]];
	[cell setFunction:^NSNumber *(IMSpreadSheet *calculator, IMSpreadSheetCell *cell) {
//		return [NSNumber numberWithInt:[calculator sumOfCellRange:@"Y45:Y54"].intValue + [calculator sumOfCellRange:@"Y40:Y43"].intValue - [calculator valueForIdentifier:@"Y44"].intValue];
		return [calculator sumOfCellRange:@"Y40:Y54"];
	}];
	
	cell = [spreadSheet spreadSheetCellForIdentifier:@"N57"];
	cell.watchList = [spreadSheet arrayOfCells:@[@"N55", @"N56"]];
	[cell setFunction:^NSNumber *(IMSpreadSheet *calculator, IMSpreadSheetCell *cell) {
		double val = [calculator valueForIdentifier:@"N55"].doubleValue * [calculator valueForIdentifier:@"N56"].doubleValue;
		return [NSNumber numberWithDouble:fmax(0.0, val)];
	}];
	
	cell = [spreadSheet spreadSheetCellForIdentifier:@"P57"];
	cell.watchList = [spreadSheet arrayOfCells:@[@"P55", @"P56"]];
	[cell setFunction:^NSNumber *(IMSpreadSheet *calculator, IMSpreadSheetCell *cell) {
		double val = [calculator valueForIdentifier:@"P55"].doubleValue * [calculator valueForIdentifier:@"P56"].doubleValue;
		return [NSNumber numberWithDouble:fmax(0.0, val)];
	}];
	
	cell = [spreadSheet spreadSheetCellForIdentifier:@"Q57"];
	cell.watchList = [spreadSheet arrayOfCells:@[@"Q55", @"Q56", @"P57"]];
	[cell setFunction:^NSNumber *(IMSpreadSheet *calculator, IMSpreadSheetCell *cell) {
		double val = [calculator valueForIdentifier:@"Q55"].doubleValue * [calculator valueForIdentifier:@"Q56"].doubleValue;
		val -= [self maxBudgetIncreaseBeforeColumn:@"Q"];
		return [NSNumber numberWithDouble:fmax(0.0, val)];
	}];
	
	cell = [spreadSheet spreadSheetCellForIdentifier:@"R57"];
	cell.watchList = [spreadSheet arrayOfCells:@[@"R55", @"R56", @"Q57"]];
	[cell setFunction:^NSNumber *(IMSpreadSheet *calculator, IMSpreadSheetCell *cell) {
		double val = [calculator valueForIdentifier:@"R55"].doubleValue * [calculator valueForIdentifier:@"R56"].doubleValue;
		val -= [self maxBudgetIncreaseBeforeColumn:@"R"];
		return [NSNumber numberWithDouble:fmax(0.0, val)];
	}];
	
	cell = [spreadSheet spreadSheetCellForIdentifier:@"S57"];
	cell.watchList = [spreadSheet arrayOfCells:@[@"S55", @"S56", @"R57"]];
	[cell setFunction:^NSNumber *(IMSpreadSheet *calculator, IMSpreadSheetCell *cell) {
		double val = [calculator valueForIdentifier:@"S55"].doubleValue * [calculator valueForIdentifier:@"S56"].doubleValue;
		val -= [self maxBudgetIncreaseBeforeColumn:@"S"];
		return [NSNumber numberWithDouble:fmax(0.0, val)];
	}];
	
	cell = [spreadSheet spreadSheetCellForIdentifier:@"T57"];
	cell.watchList = [spreadSheet arrayOfCells:@[@"T55", @"T56", @"S57"]];
	[cell setFunction:^NSNumber *(IMSpreadSheet *calculator, IMSpreadSheetCell *cell) {
		double val = [calculator valueForIdentifier:@"T55"].doubleValue * [calculator valueForIdentifier:@"T56"].doubleValue;
		val -= [self maxBudgetIncreaseBeforeColumn:@"T"];
		return [NSNumber numberWithDouble:fmax(0.0, val)];
	}];
	
	cell = [spreadSheet spreadSheetCellForIdentifier:@"U57"];
	cell.watchList = [spreadSheet arrayOfCells:@[@"U55", @"U56", @"T57"]];
	[cell setFunction:^NSNumber *(IMSpreadSheet *calculator, IMSpreadSheetCell *cell) {
		double val = [calculator valueForIdentifier:@"U55"].doubleValue * [calculator valueForIdentifier:@"U56"].doubleValue;
		val -= [self maxBudgetIncreaseBeforeColumn:@"U"];
		return [NSNumber numberWithDouble:fmax(0.0, val)];
	}];
	
	cell = [spreadSheet spreadSheetCellForIdentifier:@"V57"];
	cell.watchList = [spreadSheet arrayOfCells:@[@"V55", @"V56", @"U57"]];
	[cell setFunction:^NSNumber *(IMSpreadSheet *calculator, IMSpreadSheetCell *cell) {
		double val = [calculator valueForIdentifier:@"V55"].doubleValue * [calculator valueForIdentifier:@"V56"].doubleValue;
		val -= [self maxBudgetIncreaseBeforeColumn:@"V"];
		return [NSNumber numberWithDouble:fmax(0.0, val)];
	}];
	
	cell = [spreadSheet spreadSheetCellForIdentifier:@"W57"];
	cell.watchList = [spreadSheet arrayOfCells:@[@"W55", @"W56", @"V57"]];
	[cell setFunction:^NSNumber *(IMSpreadSheet *calculator, IMSpreadSheetCell *cell) {
		double val = [calculator valueForIdentifier:@"W55"].doubleValue * [calculator valueForIdentifier:@"W56"].doubleValue;
		val -= [self maxBudgetIncreaseBeforeColumn:@"W"];
		return [NSNumber numberWithDouble:fmax(0.0, val)];
	}];
	
	cell = [spreadSheet spreadSheetCellForIdentifier:@"X57"];
	cell.watchList = [spreadSheet arrayOfCells:@[@"X55", @"X56", @"W57"]];
	[cell setFunction:^NSNumber *(IMSpreadSheet *calculator, IMSpreadSheetCell *cell) {
		double val = [calculator valueForIdentifier:@"X55"].doubleValue * [calculator valueForIdentifier:@"X56"].doubleValue;
		val -= [self maxBudgetIncreaseBeforeColumn:@"X"];
		return [NSNumber numberWithDouble:fmax(0.0, val)];
	}];
	
	cell = [spreadSheet spreadSheetCellForIdentifier:@"Y57"];
	cell.watchList = [spreadSheet arrayOfCells:@[@"Y55", @"Y56", @"X57"]];
	[cell setFunction:^NSNumber *(IMSpreadSheet *calculator, IMSpreadSheetCell *cell) {
		double val =[calculator valueForIdentifier:@"Y55"].doubleValue * [calculator valueForIdentifier:@"Y56"].doubleValue;
		val -= [self maxBudgetIncreaseBeforeColumn:@"Y"];
		return [NSNumber numberWithDouble:fmax(0.0, val)];
	}];
	//
}

- (double) maxBudgetIncreaseBeforeColumn:(NSString *)column
{
	__block double currentVal = 0.0;
	IMSpreadSheet *spreadSheet = [self spreadSheetWithName:kIMTvTCharts];
	NSArray *a = @[@"P", @"Q", @"R", @"S", @"T", @"U", @"V", @"W", @"X", @"Y"];
	
	[a enumerateObjectsUsingBlock:^(NSString *  _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
		if([obj isEqualToString:column]) {
			*stop = YES;
		}else{
//			NSNumber *val = [spreadSheet valueForIdentifier:[NSString stringWithFormat:@"%@57", obj]];
			double val = [spreadSheet valueForIdentifier:[NSString stringWithFormat:@"%@55", obj]].doubleValue * [spreadSheet valueForIdentifier:[NSString stringWithFormat:@"%@56", obj]].doubleValue;
			currentVal = fmax(val, currentVal);
			
		}
	}];
	
	 return currentVal;
}


- (void)initAnalysis
{
	IMSpreadSheet *spreadSheet = [self spreadSheetWithName:kIMTvTAnalysis];
	IMSpreadSheet *ssScreening = [self spreadSheetWithName:kIMTvTAnnualScreening];
	IMSpreadSheet *ssNewHire = [self spreadSheetWithName:kIMTvTNewHireScreening];
	IMSpreadSheet *ssEmpCost = [self spreadSheetWithName:kIMTvTEmpCostData];
	IMSpreadSheet *ssEmpData = [self spreadSheetWithName:kIMTvTEmpTimeData];
	IMSpreadSheet *ssTspot = [self spreadSheetWithName:kIMTvTTSPOT];
	
	IMSpreadSheetCell *cell = [spreadSheet spreadSheetCellForIdentifier:@"B3"];
	cell.watchList = @[[ssEmpData spreadSheetCellForIdentifier:@"B9"]
					   ,[ssEmpData spreadSheetCellForIdentifier:@"B10"]
					   ,[ssEmpData spreadSheetCellForIdentifier:@"B11"]
					   ,[ssEmpData spreadSheetCellForIdentifier:@"B14"]];
	[cell setFunction:^NSNumber *(IMSpreadSheet *calculator, IMSpreadSheetCell *cell) {
		return [ssEmpData sumOfCells:@[@"B9", @"B10", @"B11", @"B14"]];
	}];
	
	cell = [spreadSheet spreadSheetCellForIdentifier:@"C3"];
	cell.watchList = @[[ssEmpData spreadSheetCellForIdentifier:@"B12"]];
	[cell setFunction:^NSNumber *(IMSpreadSheet *calculator, IMSpreadSheetCell *cell) {
		return [ssEmpData valueForIdentifier:@"B12"];
	}];
	
	cell = [spreadSheet spreadSheetCellForIdentifier:@"B4"];
	cell.watchList = @[[ssEmpData spreadSheetCellForIdentifier:@"B13"]];
	[cell setFunction:^NSNumber *(IMSpreadSheet *calculator, IMSpreadSheetCell *cell) {
		return [ssEmpData valueForIdentifier:@"B13"];
	}];
	
	[spreadSheet spreadSheetCellForIdentifier:@"C4"].value = @0.0;
	
	cell = [spreadSheet spreadSheetCellForIdentifier:@"B5"];
	cell.watchList = @[[ssEmpData spreadSheetCellForIdentifier:@"B21"]
					   ,[ssEmpData spreadSheetCellForIdentifier:@"B22"]];
	[cell setFunction:^NSNumber *(IMSpreadSheet *calculator, IMSpreadSheetCell *cell) {
		double val1 = [ssEmpData valueForIdentifier:@"B21"].doubleValue + [ssEmpData valueForIdentifier:@"B22"].doubleValue;
		return [NSNumber numberWithDouble:val1];
	}];
	
	cell = [spreadSheet spreadSheetCellForIdentifier:@"C5"];
	cell.watchList = @[[ssEmpData spreadSheetCellForIdentifier:@"B23"]];
	[cell setFunction:^NSNumber *(IMSpreadSheet *calculator, IMSpreadSheetCell *cell) {
		return [ssEmpData valueForIdentifier:@"B23"];
	}];
	
	cell = [spreadSheet spreadSheetCellForIdentifier:@"B6"];
	cell.watchList = @[[ssScreening spreadSheetCellForIdentifier:@"B13"]
					   ,[ssEmpData spreadSheetCellForIdentifier:@"B39"]];
	[cell setFunction:^NSNumber *(IMSpreadSheet *calculator, IMSpreadSheetCell *cell) {
		double val1 = [ssScreening valueForIdentifier:@"B13"].doubleValue * [ssEmpData valueForIdentifier:@"B39"].doubleValue;
		return [NSNumber numberWithDouble:val1];
	}];
	
	[spreadSheet spreadSheetCellForIdentifier:@"C6"].value = @0.0;
	
	cell = [spreadSheet spreadSheetCellForIdentifier:@"B9"];
	cell.watchList = @[[spreadSheet spreadSheetCellForIdentifier:@"B3"]
					   ,[ssEmpCost spreadSheetCellForIdentifier:@"B8"]];
	[cell setFunction:^NSNumber *(IMSpreadSheet *calculator, IMSpreadSheetCell *cell) {
		double val1 = [spreadSheet valueForIdentifier:@"B3"].doubleValue * [ssEmpCost valueForIdentifier:@"B8"].doubleValue / 60.0;
		return [NSNumber numberWithDouble:val1];
	}];
	
	cell = [spreadSheet spreadSheetCellForIdentifier:@"C9"];
	cell.watchList = @[[spreadSheet spreadSheetCellForIdentifier:@"C3"]
					   ,[ssEmpCost spreadSheetCellForIdentifier:@"B8"]];
	[cell setFunction:^NSNumber *(IMSpreadSheet *calculator, IMSpreadSheetCell *cell) {
		double val1 = [spreadSheet valueForIdentifier:@"C3"].doubleValue * [ssEmpCost valueForIdentifier:@"B8"].doubleValue / 60.0;
		return [NSNumber numberWithDouble:val1];
	}];
	
	cell = [spreadSheet spreadSheetCellForIdentifier:@"B10"];
	cell.watchList = @[[spreadSheet spreadSheetCellForIdentifier:@"B4"]
					   ,[ssEmpCost spreadSheetCellForIdentifier:@"B8"]];
	[cell setFunction:^NSNumber *(IMSpreadSheet *calculator, IMSpreadSheetCell *cell) {
		double val1 = [spreadSheet valueForIdentifier:@"B4"].doubleValue * [ssEmpCost valueForIdentifier:@"B8"].doubleValue / 60.0;
		return [NSNumber numberWithDouble:val1];
	}];
	
	[spreadSheet spreadSheetCellForIdentifier:@"C10"].value = @0.0;
	
	cell = [spreadSheet spreadSheetCellForIdentifier:@"B11"];
	cell.watchList = @[[spreadSheet spreadSheetCellForIdentifier:@"B5"]
					   ,[ssEmpCost spreadSheetCellForIdentifier:@"C19"]];
	[cell setFunction:^NSNumber *(IMSpreadSheet *calculator, IMSpreadSheetCell *cell) {
		double val1 = [spreadSheet valueForIdentifier:@"B5"].doubleValue * [ssEmpCost valueForIdentifier:@"C19"].doubleValue / 60.0;
		return [NSNumber numberWithDouble:val1];
	}];
	
	cell = [spreadSheet spreadSheetCellForIdentifier:@"C11"];
	cell.watchList = @[[ssEmpCost spreadSheetCellForIdentifier:@"C19"]];
	[cell setFunction:^NSNumber *(IMSpreadSheet *calculator, IMSpreadSheetCell *cell) {
		double val1 = [ssEmpData valueForIdentifier:@"B23"].doubleValue * [ssEmpCost valueForIdentifier:@"C19"].doubleValue / 60.0;
		return [NSNumber numberWithDouble:val1];
	}];
	
	cell = [spreadSheet spreadSheetCellForIdentifier:@"B12"];
	cell.watchList = @[[spreadSheet spreadSheetCellForIdentifier:@"B6"]
					   ,[ssEmpCost spreadSheetCellForIdentifier:@"B13"]];
	[cell setFunction:^NSNumber *(IMSpreadSheet *calculator, IMSpreadSheetCell *cell) {
		double val1 = [spreadSheet valueForIdentifier:@"B6"].doubleValue * [ssEmpCost valueForIdentifier:@"B13"].doubleValue;
		return [NSNumber numberWithDouble:val1];
	}];
	
	[spreadSheet spreadSheetCellForIdentifier:@"C12"].value = @0.0;
	
	cell = [spreadSheet spreadSheetCellForIdentifier:@"B15"];
	cell.watchList = @[[ssScreening spreadSheetCellForIdentifier:@"B5"]
					   ,[ssScreening spreadSheetCellForIdentifier:@"B16"]];
	[cell setFunction:^NSNumber *(IMSpreadSheet *calculator, IMSpreadSheetCell *cell) {
		double val1 = [ssScreening valueForIdentifier:@"B5"].doubleValue * [ssScreening valueForIdentifier:@"B16"].doubleValue;
		return [NSNumber numberWithDouble:val1];
	}];
	
	cell = [spreadSheet spreadSheetCellForIdentifier:@"B20"];
	cell.watchList = @[[ssEmpData spreadSheetCellForIdentifier:@"B9"]
					   ,[ssEmpData spreadSheetCellForIdentifier:@"B10"]
					   ,[ssEmpData spreadSheetCellForIdentifier:@"B11"]
					   ,[ssEmpData spreadSheetCellForIdentifier:@"B14"]];
	[cell setFunction:^NSNumber *(IMSpreadSheet *calculator, IMSpreadSheetCell *cell) {
		double val1 = [ssEmpData sumOfCells:@[@"B9", @"B10", @"B11", @"B14"]].doubleValue * 2.0;
		return [NSNumber numberWithDouble:val1];
	}];
	
	cell = [spreadSheet spreadSheetCellForIdentifier:@"C20"];
	cell.watchList = @[[ssEmpData spreadSheetCellForIdentifier:@"B12"]
					   ,[ssEmpData spreadSheetCellForIdentifier:@"B9"]];
	[cell setFunction:^NSNumber *(IMSpreadSheet *calculator, IMSpreadSheetCell *cell) {
		return [ssEmpData sumOfCells:@[@"B12", @"B9"]];
	}];
	
	cell = [spreadSheet spreadSheetCellForIdentifier:@"B21"];
	cell.watchList = @[[ssEmpData spreadSheetCellForIdentifier:@"B13"]];
	[cell setFunction:^NSNumber *(IMSpreadSheet *calculator, IMSpreadSheetCell *cell) {
		return [ssEmpData valueForIdentifier:@"B13"];
	}];
	
	[spreadSheet spreadSheetCellForIdentifier:@"C21"].value = @0.0;
	
	cell = [spreadSheet spreadSheetCellForIdentifier:@"B22"];
	cell.watchList = @[[ssEmpData spreadSheetCellForIdentifier:@"B21"]
					   ,[ssEmpData spreadSheetCellForIdentifier:@"B22"]];
	[cell setFunction:^NSNumber *(IMSpreadSheet *calculator, IMSpreadSheetCell *cell) {
		return [ssEmpData sumOfCells:@[@"B21", @"B22"]];
	}];
	
	cell = [spreadSheet spreadSheetCellForIdentifier:@"C22"];
	cell.watchList = @[[ssEmpData spreadSheetCellForIdentifier:@"B23"]];
	[cell setFunction:^NSNumber *(IMSpreadSheet *calculator, IMSpreadSheetCell *cell) {
		return [ssEmpData valueForIdentifier:@"B23"];
	}];
	
	cell = [spreadSheet spreadSheetCellForIdentifier:@"B23"];
	cell.watchList = @[[ssEmpData spreadSheetCellForIdentifier:@"B21"]
					   ,[ssEmpData spreadSheetCellForIdentifier:@"B22"]];
	[cell setFunction:^NSNumber *(IMSpreadSheet *calculator, IMSpreadSheetCell *cell) {
		return [ssEmpData sumOfCells:@[@"B21", @"B22"]];
	}];
	
	[spreadSheet spreadSheetCellForIdentifier:@"C23"].value = @0.0;
	
	cell = [spreadSheet spreadSheetCellForIdentifier:@"B27"];
	cell.watchList = @[[spreadSheet spreadSheetCellForIdentifier:@"B20"]
					   ,[ssEmpCost spreadSheetCellForIdentifier:@"B8"]];
	[cell setFunction:^NSNumber *(IMSpreadSheet *calculator, IMSpreadSheetCell *cell) {
		double val1  = [spreadSheet valueForIdentifier:@"B20"].doubleValue * [ssEmpCost valueForIdentifier:@"B8"].doubleValue / 60.0;
		return [NSNumber numberWithDouble:val1];
	}];
	
	cell = [spreadSheet spreadSheetCellForIdentifier:@"C27"];
	cell.watchList = @[[spreadSheet spreadSheetCellForIdentifier:@"C20"]
					   ,[ssEmpCost spreadSheetCellForIdentifier:@"B8"]];
	[cell setFunction:^NSNumber *(IMSpreadSheet *calculator, IMSpreadSheetCell *cell) {
		double val1 = [spreadSheet valueForIdentifier:@"C20"].doubleValue * [ssEmpCost valueForIdentifier:@"B8"].doubleValue / 60.0;
		return [NSNumber numberWithDouble:val1];
	}];
	
	cell = [spreadSheet spreadSheetCellForIdentifier:@"B28"];
	cell.watchList = @[[spreadSheet spreadSheetCellForIdentifier:@"B21"]
					   ,[ssEmpCost spreadSheetCellForIdentifier:@"B8"]];
	[cell setFunction:^NSNumber *(IMSpreadSheet *calculator, IMSpreadSheetCell *cell) {
		double val1  = [spreadSheet valueForIdentifier:@"B21"].doubleValue * [ssEmpCost valueForIdentifier:@"B8"].doubleValue / 60.0;
		return [NSNumber numberWithDouble:val1];
	}];
	
	[spreadSheet spreadSheetCellForIdentifier:@"C28"].value = @0.0;
	
	cell = [spreadSheet spreadSheetCellForIdentifier:@"B29"];
	cell.watchList = @[[spreadSheet spreadSheetCellForIdentifier:@"B22"]
					   ,[ssEmpCost spreadSheetCellForIdentifier:@"C19"]
					   ,[ssNewHire spreadSheetCellForIdentifier:@"B16"]];
	[cell setFunction:^NSNumber *(IMSpreadSheet *calculator, IMSpreadSheetCell *cell) {
		double val1 = [spreadSheet valueForIdentifier:@"B22"].doubleValue * [ssEmpCost valueForIdentifier:@"C19"].doubleValue * [ssNewHire valueForIdentifier:@"B16"].doubleValue / 60.0;
		return [NSNumber numberWithDouble:val1];
	}];
	
	cell = [spreadSheet spreadSheetCellForIdentifier:@"C29"];
	cell.watchList = @[[spreadSheet spreadSheetCellForIdentifier:@"C22"]
					   ,[ssEmpCost spreadSheetCellForIdentifier:@"C19"]
					   ,[ssNewHire spreadSheetCellForIdentifier:@"B16"]];
	[cell setFunction:^NSNumber *(IMSpreadSheet *calculator, IMSpreadSheetCell *cell) {
		double val1 = [spreadSheet valueForIdentifier:@"C22"].doubleValue * [ssEmpCost valueForIdentifier:@"C19"].doubleValue * [ssNewHire valueForIdentifier:@"B16"].doubleValue / 60.0;
		return [NSNumber numberWithDouble:val1];
	}];
	
	cell = [spreadSheet spreadSheetCellForIdentifier:@"B30"];
	cell.watchList = @[[spreadSheet spreadSheetCellForIdentifier:@"B22"]
					   ,[ssEmpCost spreadSheetCellForIdentifier:@"C19"]
					   ,[ssNewHire spreadSheetCellForIdentifier:@"B17"]];
	[cell setFunction:^NSNumber *(IMSpreadSheet *calculator, IMSpreadSheetCell *cell) {
		double val1 = [spreadSheet valueForIdentifier:@"B22"].doubleValue * [ssEmpCost valueForIdentifier:@"C19"].doubleValue * [ssNewHire valueForIdentifier:@"B17"].doubleValue / 60.0;
		return [NSNumber numberWithDouble:val1];
	}];
	
	[spreadSheet spreadSheetCellForIdentifier:@"C30"].value = @0.0;
	
	cell = [spreadSheet spreadSheetCellForIdentifier:@"B34"];
	cell.watchList = @[[ssNewHire spreadSheetCellForIdentifier:@"B4"]
					   ,[ssNewHire spreadSheetCellForIdentifier:@"B11"]];
	[cell setFunction:^NSNumber *(IMSpreadSheet *calculator, IMSpreadSheetCell *cell) {
		double val1 = [ssNewHire valueForIdentifier:@"B4"].doubleValue * [ssNewHire valueForIdentifier:@"B11"].doubleValue;
		return [NSNumber numberWithDouble:val1];
	}];
	
	cell = [spreadSheet spreadSheetCellForIdentifier:@"B35"];
	cell.watchList = @[[ssNewHire spreadSheetCellForIdentifier:@"B4"]
					   ,[ssNewHire spreadSheetCellForIdentifier:@"B12"]];
	[cell setFunction:^NSNumber *(IMSpreadSheet *calculator, IMSpreadSheetCell *cell) {
		double val1 = [ssNewHire valueForIdentifier:@"B4"].doubleValue * [ssNewHire valueForIdentifier:@"B12"].doubleValue;
		return [NSNumber numberWithDouble:val1];
	}];
	
	cell = [spreadSheet spreadSheetCellForIdentifier:@"B36"];
	cell.watchList = @[[ssScreening spreadSheetCellForIdentifier:@"B5"]
					   ,[ssScreening spreadSheetCellForIdentifier:@"B20"]
					   ,[ssNewHire spreadSheetCellForIdentifier:@"B4"]
					   ,[ssNewHire spreadSheetCellForIdentifier:@"B21"]];
	[cell setFunction:^NSNumber *(IMSpreadSheet *calculator, IMSpreadSheetCell *cell) {
		double val1 = [ssScreening valueForIdentifier:@"B5"].doubleValue * [ssScreening valueForIdentifier:@"B20"].doubleValue;
		double val2 = [ssNewHire valueForIdentifier:@"B4"].doubleValue * [ssNewHire valueForIdentifier:@"B21"].doubleValue;
		double val3 = val1 + val2;
		return [NSNumber numberWithDouble:val3];
	}];
	
	cell = [spreadSheet spreadSheetCellForIdentifier:@"B37"];
	cell.watchList = @[[ssScreening spreadSheetCellForIdentifier:@"B5"]
					   ,[ssTspot spreadSheetCellForIdentifier:@"B5"]
					   ,[ssNewHire spreadSheetCellForIdentifier:@"B4"]
					   ,[ssNewHire spreadSheetCellForIdentifier:@"C21"]];
	[cell setFunction:^NSNumber *(IMSpreadSheet *calculator, IMSpreadSheetCell *cell) {
		double val1 = [ssScreening valueForIdentifier:@"B5"].doubleValue * [ssTspot valueForIdentifier:@"B5"].doubleValue;
		double val2 = [ssNewHire valueForIdentifier:@"B4"].doubleValue * [ssNewHire valueForIdentifier:@"C21"].doubleValue;
		double val3 = val1 + val2;
		return [NSNumber numberWithDouble:val3];
	}];
	
	cell = [spreadSheet spreadSheetCellForIdentifier:@"B41"];
	cell.watchList = @[[ssScreening spreadSheetCellForIdentifier:@"B5"]
					   ,[ssScreening spreadSheetCellForIdentifier:@"B20"]
					   ,[ssNewHire spreadSheetCellForIdentifier:@"B4"]
					   ,[ssNewHire spreadSheetCellForIdentifier:@"B21"]
					   ,[ssEmpData spreadSheetCellForIdentifier:@"B17"]
					   ,[ssEmpData spreadSheetCellForIdentifier:@"B16"]
					   ,[ssEmpData spreadSheetCellForIdentifier:@"B15"]];
	[cell setFunction:^NSNumber *(IMSpreadSheet *calculator, IMSpreadSheetCell *cell) {
		double val1 = ([ssEmpData valueForIdentifier:@"B15"].doubleValue + [ssEmpData valueForIdentifier:@"B16"].doubleValue + [ssEmpData valueForIdentifier:@"B17"].doubleValue) / 60.0;
		double val2 = [ssScreening valueForIdentifier:@"B5"].doubleValue * [ssScreening valueForIdentifier:@"B20"].doubleValue;
		double val3 = [ssNewHire valueForIdentifier:@"B4"].doubleValue * [ssNewHire valueForIdentifier:@"B21"].doubleValue;
		double val4 = val1 * (val2 + val3);
		return [NSNumber numberWithDouble:val4];
	}];
	
	cell = [spreadSheet spreadSheetCellForIdentifier:@"C41"];
	cell.watchList = @[[ssScreening spreadSheetCellForIdentifier:@"B5"]
						,[ssTspot spreadSheetCellForIdentifier:@"B5"]
						,[ssNewHire spreadSheetCellForIdentifier:@"B4"]
					   ,[ssNewHire spreadSheetCellForIdentifier:@"C21"]
					   ,[ssEmpData spreadSheetCellForIdentifier:@"B17"]
					   ,[ssEmpData spreadSheetCellForIdentifier:@"B16"]
					   ,[ssEmpData spreadSheetCellForIdentifier:@"B15"]];
	[cell setFunction:^NSNumber *(IMSpreadSheet *calculator, IMSpreadSheetCell *cell) {
		double val1 = ([ssEmpData valueForIdentifier:@"B15"].doubleValue + [ssEmpData valueForIdentifier:@"B16"].doubleValue + [ssEmpData valueForIdentifier:@"B17"].doubleValue) / 60.0;
		double val2 = [ssScreening valueForIdentifier:@"B5"].doubleValue * [ssTspot valueForIdentifier:@"B5"].doubleValue;
		double val3 = [ssNewHire valueForIdentifier:@"B4"].doubleValue * [ssNewHire valueForIdentifier:@"C21"].doubleValue;
		double val4 = val1 * (val2 + val3);
		return [NSNumber numberWithDouble:val4];
	}];
	
	cell = [spreadSheet spreadSheetCellForIdentifier:@"B42"];
	cell.watchList = @[[ssScreening spreadSheetCellForIdentifier:@"B5"]
					   ,[ssScreening spreadSheetCellForIdentifier:@"B20"]
					   ,[ssNewHire spreadSheetCellForIdentifier:@"B4"]
					   ,[ssNewHire spreadSheetCellForIdentifier:@"B21"]
					   ,[ssEmpData spreadSheetCellForIdentifier:@"B25"]
					   ,[ssEmpData spreadSheetCellForIdentifier:@"B26"]];
	[cell setFunction:^NSNumber *(IMSpreadSheet *calculator, IMSpreadSheetCell *cell) {
		double val1 = ([ssEmpData valueForIdentifier:@"B25"].doubleValue + [ssEmpData valueForIdentifier:@"B26"].doubleValue) / 60.0;
		double val2 = [ssScreening valueForIdentifier:@"B5"].doubleValue * [ssScreening valueForIdentifier:@"B20"].doubleValue;
		double val3 = [ssNewHire valueForIdentifier:@"B4"].doubleValue * [ssNewHire valueForIdentifier:@"B21"].doubleValue;
		double val4 = val1 * (val2 + val3);
		return [NSNumber numberWithDouble:val4];
	}];
	
	cell = [spreadSheet spreadSheetCellForIdentifier:@"C42"];
	cell.watchList = @[[ssScreening spreadSheetCellForIdentifier:@"B5"]
					   ,[ssTspot spreadSheetCellForIdentifier:@"B5"]
					   ,[ssNewHire spreadSheetCellForIdentifier:@"B4"]
					   ,[ssNewHire spreadSheetCellForIdentifier:@"B21"]
					   ,[ssEmpData spreadSheetCellForIdentifier:@"B25"]
					   ,[ssEmpData spreadSheetCellForIdentifier:@"B26"]];
	[cell setFunction:^NSNumber *(IMSpreadSheet *calculator, IMSpreadSheetCell *cell) {
		double val1 = ([ssEmpData valueForIdentifier:@"B25"].doubleValue + [ssEmpData valueForIdentifier:@"B26"].doubleValue) / 60.0;
		double val2 = [ssScreening valueForIdentifier:@"B5"].doubleValue * [ssTspot valueForIdentifier:@"B5"].doubleValue;
		double val3 = [ssNewHire valueForIdentifier:@"B4"].doubleValue * [ssNewHire valueForIdentifier:@"B21"].doubleValue;
		double val4 = val1 * (val2 + val3);
		return [NSNumber numberWithDouble:val4];
	}];
	
	cell = [spreadSheet spreadSheetCellForIdentifier:@"B45"];
	cell.watchList = @[[ssScreening spreadSheetCellForIdentifier:@"B21"]
					   ,[ssScreening spreadSheetCellForIdentifier:@"B5"]
					   ,[ssScreening spreadSheetCellForIdentifier:@"B20"]
					   ,[ssNewHire spreadSheetCellForIdentifier:@"B4"]
					   ,[ssNewHire spreadSheetCellForIdentifier:@"B21"]];
	[cell setFunction:^NSNumber *(IMSpreadSheet *calculator, IMSpreadSheetCell *cell) {
		double val1 = [ssScreening valueForIdentifier:@"B21"].doubleValue;
		double val2 = [ssScreening valueForIdentifier:@"B5"].doubleValue * [ssScreening valueForIdentifier:@"B20"].doubleValue;
		double val3 = [ssNewHire valueForIdentifier:@"B4"].doubleValue * [ssNewHire valueForIdentifier:@"B21"].doubleValue;
		double val4 = val1 * (val2 + val3);
		return [NSNumber numberWithDouble:val4];
	}];
	
	cell = [spreadSheet spreadSheetCellForIdentifier:@"C45"];
	cell.watchList = @[[ssScreening spreadSheetCellForIdentifier:@"B21"]
					   ,[ssScreening spreadSheetCellForIdentifier:@"B5"]
					   ,[ssTspot spreadSheetCellForIdentifier:@"B5"]
					   ,[ssNewHire spreadSheetCellForIdentifier:@"B4"]
					   ,[ssNewHire spreadSheetCellForIdentifier:@"C21"]];
	[cell setFunction:^NSNumber *(IMSpreadSheet *calculator, IMSpreadSheetCell *cell) {
		double val1 = [ssScreening valueForIdentifier:@"B21"].doubleValue;
		double val2 = [ssScreening valueForIdentifier:@"B5"].doubleValue * [ssTspot valueForIdentifier:@"B5"].doubleValue;
		double val3 = [ssNewHire valueForIdentifier:@"B4"].doubleValue * [ssNewHire valueForIdentifier:@"C21"].doubleValue;
		double val4 = val1 * (val2 + val3);
		return [NSNumber numberWithDouble:val4];
	}];
	
	cell = [spreadSheet spreadSheetCellForIdentifier:@"B48"];
	cell.watchList = @[[ssScreening spreadSheetCellForIdentifier:@"B13"]
					   ,[ssEmpData spreadSheetCellForIdentifier:@"B39"]];
	[cell setFunction:^NSNumber *(IMSpreadSheet *calculator, IMSpreadSheetCell *cell) {
		double val1 = [ssScreening valueForIdentifier:@"B13"].doubleValue * [ssEmpData valueForIdentifier:@"B39"].doubleValue;
		return [NSNumber numberWithDouble:val1];
	}];
	
	cell = [spreadSheet spreadSheetCellForIdentifier:@"B49"];
	cell.watchList = @[[ssScreening spreadSheetCellForIdentifier:@"B13"]
					   ,[ssEmpCost spreadSheetCellForIdentifier:@"B13"]
					   ,[ssEmpData spreadSheetCellForIdentifier:@"B39"]];
	[cell setFunction:^NSNumber *(IMSpreadSheet *calculator, IMSpreadSheetCell *cell) {
		double val1 = [ssScreening valueForIdentifier:@"B13"].doubleValue * [ssEmpCost valueForIdentifier:@"B13"].doubleValue * [ssEmpData valueForIdentifier:@"B39"].doubleValue;
		return [NSNumber numberWithDouble:val1];
	}];
	
}


- (void) initEmployeeTimeReferenceData
{
	IMSpreadSheet *ssEmployeeTime = [self spreadSheetWithName:kIMTvTEmpTimeData];
	IMSpreadSheetCell *cell;
	NSArray *values = @[@5.0, @7.5, @6.7, @7.4, @10.0, @6.2, @13.7, @5.0, @25.0];
	for(int i=9;i<18;i++)
	{
		cell = [ssEmployeeTime spreadSheetCellForIdentifier:[NSString stringWithFormat:@"B%d", i]];
		cell.value = values[i-9];
	}
	
	values = @[@22.9, @22.3, @23.6, @15.0, @33.2, @45.0];
	for(int i=21;i<27;i++)
	{
		cell = [ssEmployeeTime spreadSheetCellForIdentifier:[NSString stringWithFormat:@"B%d", i]];
		cell.value = values[i-21];
	}
	
	cell = [ssEmployeeTime spreadSheetCellForIdentifier:@"B35"];
	cell.value = @4.0;
	cell = [ssEmployeeTime spreadSheetCellForIdentifier:@"B36"];
	cell.value = @1.0;
	cell = [ssEmployeeTime spreadSheetCellForIdentifier:@"B39"];
	cell.value = @8.0;
}

@end