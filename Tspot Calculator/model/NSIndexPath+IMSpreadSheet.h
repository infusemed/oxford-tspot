//
//  NSIndexPath+IMSpreadSheet.h
//  IMSpreadSheet
//
//  Created by Anthony Long on 4/18/13.
//  Copyright (c) 2013 Infuse Medical. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSIndexPath (IMSpreadSheet)

@property(nonatomic, readonly) NSInteger spreadsheetRow;
@property(nonatomic, readonly) NSString * spreadsheetColumn;

+ (NSIndexPath *) indexPathForRow:(NSInteger)row column:(NSString *)column;

@end