//
//  IMSpreadSheet.h
//  IMSpreadSheet
//
//  Created by Anthony Long on 4/18/13.
//  Copyright (c) 2013 Infuse Medical. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "IMSpreadSheetCell.h"

@class IMWorkbook, IMSpreadSheetCell;

@interface IMSpreadSheet : NSObject

@property (nonatomic, weak) IMWorkbook * workbook;
@property (nonatomic, strong) NSMutableDictionary * cells;

- (IMSpreadSheetCell *) spreadSheetCellForIndexPath:(NSIndexPath *)indexPath;
- (IMSpreadSheetCell *) spreadSheetCellForIndexPath:(NSIndexPath *)indexPath createIfNil:(BOOL)shouldCreate;
- (IMSpreadSheetCell *) spreadSheetCellForIdentifier:(NSString *)identifier;
- (void) setSpreadSheetCell:(IMSpreadSheetCell *)cell forIndexPath:(NSIndexPath *)indexPath;
- (void) setValue:(NSNumber *)value forIndexPath:(NSIndexPath *)indexPath;
- (void) setTextValue:(NSString *)value forIndexPath:(NSIndexPath *)indexPath;
- (void) setFormula:(NSString *)formula forIndexPath:(NSIndexPath *)indexPath;
- (NSString *) formulaForIndexPath:(NSIndexPath *)indexPath;
- (void) removeIndexPath:(NSIndexPath *)indexPath;
- (NSNumber *) valueForIndexPath:(NSIndexPath *)indexPath;
- (NSNumber *) valueForIdentifier:(NSString *)identifier;
- (NSString *) textValueForIndexPath:(NSIndexPath *)indexPath;
- (NSNumber *) multiplyCells:(NSArray *)cellIdentifiers;
- (NSArray *) arrayOfCells:(NSArray *) arrayIdentifiers;
- (NSNumber *) divideCells:(NSArray *)cellIdentifiers;
- (NSNumber *) sumOfCells:(NSArray *)cellIdentifiers;
- (NSNumber *) sumOfCellsInArray:(NSArray *)cells;
- (NSNumber *) sumOfCellRange:(NSString *)cellRange;
- (NSNumber *) subtractCell:(NSString *)cellIdentifier1 fromCell:(NSString *)cellIdentifier2;
- (void) setValue:(NSNumber *)value forIdentifier:(NSString *)identifier;

@end

@interface IMWorkbook : NSObject

@property (nonatomic, strong) NSString * fileName;
@property (nonatomic, strong) NSString * customerName;
@property (nonatomic, strong) NSString * additionalInfo;
@property (nonatomic, strong) NSString * disclaimer;

@property (nonatomic, copy) NSAttributedString *phasedContent;
@property (nonatomic, strong) NSAttributedString * initialContent;

- (id) initWithFile:(NSString *)filePath;
- (void)writeToFile:(NSString *)filePath;
- (IMSpreadSheet *) spreadSheetWithName:(NSString *)name;
- (void) setSpreadSheet:(IMSpreadSheet *)spreadSheet WithName:(NSString *)name;
- (void) initializeWorkbook;

@end
