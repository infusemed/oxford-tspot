//
//  IMStudentSpotlightEconWorkbook.m
//  Tspot Calculator
//
//  Created by Anthony Long on 1/2/15.
//  Copyright (c) 2015 Infuse Medical. All rights reserved.
//

#import "IMStudentSpotlightEconWorkbook.h"

@implementation IMStudentSpotlightEconWorkbook

 NSString * const kIMStudentTBScreening = @"Student TB Screening Program";
 NSString * const kIMStudentSPOTLIGHT = @"SPOTLIGHT";
 NSString * const kIMStudentAnalysis = @"Analysis";
 NSString * const kIMStudentChartsAndTotals = @"Charts and Totals";
 NSString * const kIMStudenEmpTimeReference = @"Employee Time Reference Data";

//- (id) init
//{
//	self = [super init];
//	if(self)
//	{
//		[self initializeWorkbook];
//	}
//	
//	return self;
//}
//
//- (id) initWithFile:(NSString *)filePath
//{
//	self = [super init];
//	if(self)
//	{
//		self.fileName = filePath.lastPathComponent;
//		[self initializeWorkbook];
//		NSDictionary * d = [NSDictionary dictionaryWithContentsOfFile:filePath];
//		[d enumerateKeysAndObjectsUsingBlock:^(id key, id obj, BOOL *stop) {
//			NSDictionary * d2 = (NSDictionary *)obj;
//			IMSpreadSheet * spreadsheet = [self spreadSheetWithName:(NSString *)key];
//			[d2 enumerateKeysAndObjectsUsingBlock:^(id key, id obj, BOOL *stop) {
//				NSArray * a = [(NSString *)key componentsSeparatedByString:@"-"];
//				NSUInteger indexes[] = {[[a objectAtIndex:0] intValue], [[a objectAtIndex:1] intValue]};
//				if([obj isKindOfClass:[NSString class]])
//				{
//					[spreadsheet setTextValue:obj forIndexPath:[NSIndexPath indexPathWithIndexes:indexes length:2]];
//				}
//				else
//				{
//					[spreadsheet setValue:obj forIndexPath:[NSIndexPath indexPathWithIndexes:indexes length:2]];
//				}
//			}];
//		}];
//	}
//	
//	return self;
//}

- (void) setCustomerName:(NSString *)customerName
{
	[super setCustomerName:customerName];
	[[self spreadSheetWithName:kIMStudentTBScreening] spreadSheetCellForIdentifier:@"B3"].textValue = customerName;
}

- (NSString *) customerName
{
	return [[self spreadSheetWithName:kIMStudentTBScreening] spreadSheetCellForIdentifier:@"B3"].textValue;
}

- (void) initializeWorkbook
{
	[super initializeWorkbook];
	[self setSpreadSheet:[[IMSpreadSheet alloc] init]
				WithName:kIMStudentTBScreening];
	
	[self setSpreadSheet:[[IMSpreadSheet alloc] init]
				WithName:kIMStudentSPOTLIGHT];
	
	[self setSpreadSheet:[[IMSpreadSheet alloc] init]
				WithName:kIMStudentChartsAndTotals];
	
	[self setSpreadSheet:[[IMSpreadSheet alloc] init]
				WithName:kIMStudentAnalysis];
	
	[self setSpreadSheet:[[IMSpreadSheet alloc] init]
				WithName:kIMStudenEmpTimeReference];
	
	[self initAnnualTBScreening];
	[self initSpotlight];
	[self initEmployeeTimeReferenceData];
	[self initAnalysis];
	[self initChartsAndTotals];
}

- (void) initAnnualTBScreening
{
	IMSpreadSheet * spreadSheet = [self spreadSheetWithName:kIMStudentTBScreening];
	
	[spreadSheet spreadSheetCellForIdentifier:@"B3"];
	[spreadSheet spreadSheetCellForIdentifier:@"B5"];
	[spreadSheet spreadSheetCellForIdentifier:@"B7"];
	[spreadSheet spreadSheetCellForIdentifier:@"B10"].value = @2.83;
	[spreadSheet spreadSheetCellForIdentifier:@"B13"].value = @54.44;
	[spreadSheet spreadSheetCellForIdentifier:@"B16"].value = @0.2;
	[spreadSheet spreadSheetCellForIdentifier:@"B20"].value = @0.438;
	[spreadSheet spreadSheetCellForIdentifier:@"B21"];
	[spreadSheet spreadSheetCellForIdentifier:@"B24"].value = @2.0;
}

- (void) initSpotlight
{
	IMSpreadSheet * spreadSheet = [self spreadSheetWithName:kIMStudentSPOTLIGHT];
	[spreadSheet spreadSheetCellForIdentifier:@"B2"];
	[spreadSheet spreadSheetCellForIdentifier:@"B4"].value = @20.0;
	[spreadSheet spreadSheetCellForIdentifier:@"B6"].value = @1.0;
	[spreadSheet spreadSheetCellForIdentifier:@"B9"].value = @0.056;
}

- (void) initAnalysis
{
	IMSpreadSheet * ssScreening = [self spreadSheetWithName:kIMStudentTBScreening];
	IMSpreadSheet * ssSpotlight = [self spreadSheetWithName:kIMStudentSPOTLIGHT];
	IMSpreadSheet * ssAnalysis = [self spreadSheetWithName:kIMStudentAnalysis];
	IMSpreadSheet * ssEmployeeTime = [self spreadSheetWithName:kIMStudenEmpTimeReference];
	IMSpreadSheetCell * cell;
	
	cell = [ssAnalysis spreadSheetCellForIdentifier:@"B3"];
	cell.watchList = [ssEmployeeTime arrayOfCells:@[@"B9", @"B10", @"B11", @"B14"]];
	[cell setFunction:^NSNumber *(IMSpreadSheet *calculator, IMSpreadSheetCell *cell) {
		return [ssEmployeeTime sumOfCells:@[@"B9", @"B10", @"B11", @"B14"]];
	}];
	
	cell = [ssAnalysis spreadSheetCellForIdentifier:@"C3"];
	cell.watchList = @[[ssSpotlight spreadSheetCellForIdentifier:@"B4"]
					   ,[ssScreening spreadSheetCellForIdentifier:@"B13"]
					   ,[ssSpotlight spreadSheetCellForIdentifier:@"B6"]
					   ,[ssScreening spreadSheetCellForIdentifier:@"B5"]];
	[cell setFunction:^NSNumber *(IMSpreadSheet *calculator, IMSpreadSheetCell *cell) {
		NSNumber *val1 = [ssSpotlight valueForIdentifier:@"B4"];
		NSNumber *val2 = [ssScreening valueForIdentifier:@"B13"];
		NSNumber *val3 = [ssSpotlight valueForIdentifier:@"B6"];
		NSNumber *val4 = [ssScreening valueForIdentifier:@"B5"];
		return [NSNumber numberWithDouble:(val1.doubleValue * val2.doubleValue * val3.doubleValue)/val4.doubleValue];
	}];
	cell = [ssAnalysis spreadSheetCellForIdentifier:@"B5"];
	cell.watchList = [ssEmployeeTime arrayOfCells:@[@"B21", @"B22"]];
	[cell setFunction:^NSNumber *(IMSpreadSheet *calculator, IMSpreadSheetCell *cell) {
		return [calculator sumOfCellsInArray:cell.watchList];
	}];
	
	cell = [ssAnalysis spreadSheetCellForIdentifier:@"C5"];
	cell.watchList = [ssEmployeeTime arrayOfCells:@[@"B24"]];
	[cell setFunction:^NSNumber *(IMSpreadSheet *calculator, IMSpreadSheetCell *cell) {
		return [ssEmployeeTime valueForIdentifier:@"B24"];
	}];
	
	cell = [ssAnalysis spreadSheetCellForIdentifier:@"B6"];
	cell.watchList = @[[ssScreening spreadSheetCellForIdentifier:@"B7"]];
	[cell setFunction:^NSNumber *(IMSpreadSheet *calculator, IMSpreadSheetCell *cell) {
		return [ssScreening valueForIdentifier:@"B7"];
	}];
	
	cell = [ssAnalysis spreadSheetCellForIdentifier:@"C6"];
	cell.watchList = @[[ssAnalysis spreadSheetCellForIdentifier:@"B20"]];
	[cell setFunction:^NSNumber *(IMSpreadSheet *calculator, IMSpreadSheetCell *cell) {
		return [ssAnalysis valueForIdentifier:@"B20"];
	}];
	
	cell = [ssAnalysis spreadSheetCellForIdentifier:@"B19"];
	cell.watchList = @[[ssScreening spreadSheetCellForIdentifier:@"B5"]
					   ,[ssSpotlight spreadSheetCellForIdentifier:@"B6"]];
	[cell setFunction:^NSNumber *(IMSpreadSheet *calculator, IMSpreadSheetCell *cell) {
		double val1 = [[(IMSpreadSheetCell *)cell.watchList[0] value] doubleValue];
		double val2 = [[(IMSpreadSheetCell *)cell.watchList[1] value] doubleValue];
		return [NSNumber numberWithDouble:val1/val2];
	}];
	
	cell = [ssAnalysis spreadSheetCellForIdentifier:@"B20"];
	cell.watchList = @[[ssAnalysis spreadSheetCellForIdentifier:@"B19"]
					   ,[ssSpotlight spreadSheetCellForIdentifier:@"B6"]];
	[cell setFunction:^NSNumber *(IMSpreadSheet *calculator, IMSpreadSheetCell *cell) {
		double val1 = [ssAnalysis valueForIdentifier:@"B19"].doubleValue;
		double val2 = 0.0;
		if(val1 >= 5000)
		{
			val2 = 8.0;
		}else if(val1 >= 4000){
			val2 = 7.0;
		}else if(val1 >= 3000){
			val2 = 6.0;
		}else if(val1 >= 2500){
			val2 = 5.0;
		}else if(val1 >= 2000){
			val2 = 4.0;
		}else if(val1 >= 1000){
			val2 = 3.0;
		}else if(val1 >= 500){
			val2 = 2.0;
		}else if(val1 >= 250){
			val2 = 1.0;
		}
		
		return [NSNumber numberWithDouble:val2*[ssSpotlight valueForIdentifier:@"B6"].doubleValue];
	}];
	
	cell = [ssAnalysis spreadSheetCellForIdentifier:@"B46"];
	cell.watchList = @[[ssEmployeeTime spreadSheetCellForIdentifier:@"B15"]
					   ,[ssEmployeeTime spreadSheetCellForIdentifier:@"B16"]
					   ,[ssEmployeeTime spreadSheetCellForIdentifier:@"B17"]
					   ,[ssScreening spreadSheetCellForIdentifier:@"B5"]
					   ,[ssScreening spreadSheetCellForIdentifier:@"B20"]];
	[cell setFunction:^NSNumber *(IMSpreadSheet *calculator, IMSpreadSheetCell *cell) {
		double val1 = [ssEmployeeTime sumOfCells:@[@"B15", @"B16", @"B17"]].doubleValue;
		double val2 = [ssScreening multiplyCells:@[@"B5", @"B20"]].doubleValue;
		return [NSNumber numberWithDouble:(val1/60) * val2];
	}];
	cell = [ssAnalysis spreadSheetCellForIdentifier:@"C46"];
	cell.watchList = @[[ssEmployeeTime spreadSheetCellForIdentifier:@"B15"]
					   ,[ssEmployeeTime spreadSheetCellForIdentifier:@"B16"]
					   ,[ssEmployeeTime spreadSheetCellForIdentifier:@"B17"]
					   ,[ssScreening spreadSheetCellForIdentifier:@"B5"]
					   ,[ssSpotlight spreadSheetCellForIdentifier:@"B9"]];
	[cell setFunction:^NSNumber *(IMSpreadSheet *calculator, IMSpreadSheetCell *cell) {
		double val1 = [ssEmployeeTime sumOfCells:@[@"B15", @"B16", @"B17"]].doubleValue;
		double val2 = [ssScreening valueForIdentifier:@"B5"].doubleValue * [ssSpotlight valueForIdentifier:@"B9"].doubleValue;
		return [NSNumber numberWithDouble:(val1/60) * val2 ];
	}];
	
	cell = [ssAnalysis spreadSheetCellForIdentifier:@"B47"];
	cell.watchList = @[[ssEmployeeTime spreadSheetCellForIdentifier:@"B25"]
					   ,[ssEmployeeTime spreadSheetCellForIdentifier:@"B26"]
					   ,[ssScreening spreadSheetCellForIdentifier:@"B5"]
					   ,[ssScreening spreadSheetCellForIdentifier:@"B20"]];
	[cell setFunction:^NSNumber *(IMSpreadSheet *calculator, IMSpreadSheetCell *cell) {
		double val1 = [ssEmployeeTime sumOfCells:@[@"B25", @"B26"]].doubleValue;
		double val2 = [ssScreening multiplyCells:@[@"B5", @"B20"]].doubleValue;
		return [NSNumber numberWithDouble:(val1/60) * val2];
	}];
	
	cell = [ssAnalysis spreadSheetCellForIdentifier:@"C47"];
	cell.watchList = @[[ssEmployeeTime spreadSheetCellForIdentifier:@"B25"]
					   ,[ssEmployeeTime spreadSheetCellForIdentifier:@"B26"]
					   ,[ssScreening spreadSheetCellForIdentifier:@"B5"]
					   ,[ssSpotlight spreadSheetCellForIdentifier:@"B9"]];
	[cell setFunction:^NSNumber *(IMSpreadSheet *calculator, IMSpreadSheetCell *cell) {
		double val1 = [ssEmployeeTime sumOfCells:@[@"B25", @"B26"]].doubleValue;
		double val2 = [ssScreening valueForIdentifier:@"B5"].doubleValue * [ssSpotlight valueForIdentifier:@"B9"].doubleValue;
		return [NSNumber numberWithDouble:(val1/60) * val2];
	}];
}

- (void) initEmployeeTimeReferenceData
{
	IMSpreadSheet *ssEmployeeTime = [self spreadSheetWithName:kIMStudenEmpTimeReference];
	IMSpreadSheetCell *cell;
	NSArray *values = @[@5.0, @7.5, @6.7, @7.4, @10.0, @6.2, @13.7, @5.0, @25.0];
	for(int i=9;i<18;i++)
	{
		cell = [ssEmployeeTime spreadSheetCellForIdentifier:[NSString stringWithFormat:@"B%d", i]];
		cell.value = values[i-9];
	}
	
	values = @[@22.9, @22.3, @23.6, @15.0, @33.2, @45.0];
	for(int i=21;i<27;i++)
	{
		cell = [ssEmployeeTime spreadSheetCellForIdentifier:[NSString stringWithFormat:@"B%d", i]];
		cell.value = values[i-21];
	}
	
	cell = [ssEmployeeTime spreadSheetCellForIdentifier:@"B35"];
	cell.value = @4.0;
	cell = [ssEmployeeTime spreadSheetCellForIdentifier:@"B36"];
	cell.value = @1.0;
	cell = [ssEmployeeTime spreadSheetCellForIdentifier:@"B39"];
	cell.value = @8.0;
}

- (void) initChartsAndTotals
{
	IMSpreadSheet * ssScreening = [self spreadSheetWithName:kIMStudentTBScreening];
	IMSpreadSheet * ssSpotlight = [self spreadSheetWithName:kIMStudentSPOTLIGHT];
	IMSpreadSheet * ssAnalysis = [self spreadSheetWithName:kIMStudentAnalysis];
	IMSpreadSheet * ssTotals = [self spreadSheetWithName:kIMStudentChartsAndTotals];
	IMSpreadSheetCell * cell = [ssTotals spreadSheetCellForIdentifier:@"B2"];
	cell.watchList = [ssScreening arrayOfCells:@[@"B5", @"B10"]];
	[cell setFunction:^NSNumber *(IMSpreadSheet *calculator, IMSpreadSheetCell *cell) {
		return [ssScreening multiplyCells:@[@"B5", @"B10"]];
	}];
	cell = [ssTotals spreadSheetCellForIdentifier:@"C2"];
	cell.watchList = @[[ssSpotlight spreadSheetCellForIdentifier:@"B2"]
					   ,[ssScreening spreadSheetCellForIdentifier:@"B5"]];
	[cell setFunction:^NSNumber *(IMSpreadSheet *calculator, IMSpreadSheetCell *cell) {
		double val1 = [ssSpotlight valueForIdentifier:@"B2"].doubleValue;
		double val2 = [ssScreening valueForIdentifier:@"B5"].doubleValue;
		return [NSNumber numberWithDouble:val1 * val2];
	}];
	cell = [ssTotals spreadSheetCellForIdentifier:@"B3"];
	cell.watchList = @[[ssTotals spreadSheetCellForIdentifier:@"B9"]
					   ,[ssScreening spreadSheetCellForIdentifier:@"B13"]];
	[cell setFunction:^NSNumber *(IMSpreadSheet *calculator, IMSpreadSheetCell *cell) {
		double val1 = [ssTotals valueForIdentifier:@"B9"].doubleValue;
		double val2 = [ssScreening valueForIdentifier:@"B13"].doubleValue;
		return [NSNumber numberWithDouble:val1 * val2];
	}];
	cell = [ssTotals spreadSheetCellForIdentifier:@"C3"];
	cell.watchList = @[[ssTotals spreadSheetCellForIdentifier:@"C9"]
					   ,[ssScreening spreadSheetCellForIdentifier:@"B13"]];
	[cell setFunction:^NSNumber *(IMSpreadSheet *calculator, IMSpreadSheetCell *cell) {
		double val1 = [ssTotals valueForIdentifier:@"C9"].doubleValue;
		double val2 = [ssScreening valueForIdentifier:@"B13"].doubleValue;
		return [NSNumber numberWithDouble:val1 * val2];
	}];
	
	cell = [ssTotals spreadSheetCellForIdentifier:@"B4"];
	cell.watchList = [ssScreening arrayOfCells:@[@"B21", @"B5", @"B20"]];
	[cell setFunction:^NSNumber *(IMSpreadSheet *calculator, IMSpreadSheetCell *cell) {
		return [ssScreening multiplyCells:@[@"B21", @"B5", @"B20"]];
	}];
	
	cell = [ssTotals spreadSheetCellForIdentifier:@"C4"];
	cell.watchList = @[[ssScreening spreadSheetCellForIdentifier:@"B21"]
					   ,[ssScreening spreadSheetCellForIdentifier:@"B5"]
					   ,[ssSpotlight spreadSheetCellForIdentifier:@"B9"]];
	[cell setFunction:^NSNumber *(IMSpreadSheet *calculator, IMSpreadSheetCell *cell) {
		double val1 = [ssScreening valueForIdentifier:@"B21"].doubleValue * [ssScreening valueForIdentifier:@"B5"].doubleValue * [ssSpotlight valueForIdentifier:@"B9"].doubleValue;
		return [NSNumber numberWithDouble:val1];
	}];
	
	cell = [ssTotals spreadSheetCellForIdentifier:@"B5"];
	cell.watchList = [ssTotals arrayOfCells:@[@"B2", @"B3", @"B4"]];
	[cell setFunction:^NSNumber *(IMSpreadSheet *calculator, IMSpreadSheetCell *cell) {
		return [ssTotals sumOfCells:@[@"B2", @"B3", @"B4"]];
	}];
	
	cell = [ssTotals spreadSheetCellForIdentifier:@"C5"];
	cell.watchList = [ssTotals arrayOfCells:@[@"C2", @"C3", @"C4"]];
	[cell setFunction:^NSNumber *(IMSpreadSheet *calculator, IMSpreadSheetCell *cell) {
		return [ssTotals sumOfCells:@[@"C2", @"C3", @"C4"]];
	}];
	
	cell = [ssTotals spreadSheetCellForIdentifier:@"D5"];
	cell.watchList = [ssTotals arrayOfCells:@[@"B5", @"C5"]];
	[cell setFunction:^NSNumber *(IMSpreadSheet *calculator, IMSpreadSheetCell *cell) {
		double val1 = 1.0 - ([ssTotals valueForIdentifier:@"C5"].doubleValue / [ssTotals valueForIdentifier:@"B5"].doubleValue);
		return [NSNumber numberWithDouble:val1];
	}];
	
	cell = [ssTotals spreadSheetCellForIdentifier:@"E5"];
	cell.watchList = [ssTotals arrayOfCells:@[@"B5", @"C5"]];
	[cell setFunction:^NSNumber *(IMSpreadSheet *calculator, IMSpreadSheetCell *cell) {
		double val1 = [ssTotals valueForIdentifier:@"B5"].doubleValue - [ssTotals valueForIdentifier:@"C5"].doubleValue;
		return [NSNumber numberWithDouble:val1];
	}];
	
	cell = [ssTotals spreadSheetCellForIdentifier:@"B9"];
	cell.watchList = @[[ssAnalysis spreadSheetCellForIdentifier:@"B3"]
					   ,[ssScreening spreadSheetCellForIdentifier:@"B5"]
					   ,[ssScreening spreadSheetCellForIdentifier:@"B24"]
					   ,[ssAnalysis spreadSheetCellForIdentifier:@"B46"]];
	[cell setFunction:^NSNumber *(IMSpreadSheet *calculator, IMSpreadSheetCell *cell) {
		double val1 = [ssAnalysis valueForIdentifier:@"B3"].doubleValue * [ssScreening valueForIdentifier:@"B5"].doubleValue / 60.0;
		double val2 = [ssScreening valueForIdentifier:@"B24"].doubleValue;
		double val3 = [ssAnalysis valueForIdentifier:@"B46"].doubleValue;
		return [NSNumber numberWithDouble: (val1 * val2) + val3];
	}];
	
	cell = [ssTotals spreadSheetCellForIdentifier:@"C9"];
	cell.watchList = @[[ssAnalysis spreadSheetCellForIdentifier:@"C3"]
					   ,[ssScreening spreadSheetCellForIdentifier:@"B5"]
					   ,[ssAnalysis spreadSheetCellForIdentifier:@"C46"]];
	[cell setFunction:^NSNumber *(IMSpreadSheet *calculator, IMSpreadSheetCell *cell) {
		double val1 = [ssAnalysis valueForIdentifier:@"C3"].doubleValue * [ssScreening valueForIdentifier:@"B5"].doubleValue / 60.0;
		double val2 = [ssAnalysis valueForIdentifier:@"C46"].doubleValue;
		return [NSNumber numberWithDouble: val1 + val2];
	}];
	
	cell = [ssTotals spreadSheetCellForIdentifier:@"B10"];
	cell.watchList = @[[ssAnalysis spreadSheetCellForIdentifier:@"B5"]
					   ,[ssScreening spreadSheetCellForIdentifier:@"B5"]
					   ,[ssScreening spreadSheetCellForIdentifier:@"B24"]
					   ,[ssAnalysis spreadSheetCellForIdentifier:@"B47"]];
	[cell setFunction:^NSNumber *(IMSpreadSheet *calculator, IMSpreadSheetCell *cell) {
		double val1 = [ssAnalysis valueForIdentifier:@"B5"].doubleValue / 60.0;
		double val2 = val1 * [ssScreening valueForIdentifier:@"B5"].doubleValue * [ssScreening valueForIdentifier:@"B24"].doubleValue;
		double val3 = [ssAnalysis valueForIdentifier:@"B47"].doubleValue;
		return [NSNumber numberWithDouble:val2 + val3];
	}];
	
	cell = [ssTotals spreadSheetCellForIdentifier:@"C10"];
	cell.watchList = @[[ssAnalysis spreadSheetCellForIdentifier:@"C5"]
					   ,[ssScreening spreadSheetCellForIdentifier:@"B5"]
					   ,[ssAnalysis spreadSheetCellForIdentifier:@"C47"]];
	[cell setFunction:^NSNumber *(IMSpreadSheet *calculator, IMSpreadSheetCell *cell) {
		double val1 = [ssAnalysis valueForIdentifier:@"C5"].doubleValue / 60.0 * [ssScreening valueForIdentifier:@"B5"].doubleValue;
		double val2 = [ssAnalysis valueForIdentifier:@"C47"].doubleValue;
		return [NSNumber numberWithDouble:val1 + val2];
	}];
	
	cell = [ssTotals spreadSheetCellForIdentifier:@"B11"];
	cell.watchList = [ssTotals arrayOfCells:@[@"B9", @"B10"]];
	[cell setFunction:^NSNumber *(IMSpreadSheet *calculator, IMSpreadSheetCell *cell) {
		return [ssTotals sumOfCells:@[@"B9", @"B10"]];
	}];
	
	cell = [ssTotals spreadSheetCellForIdentifier:@"C11"];
	cell.watchList = [ssTotals arrayOfCells:@[@"C9", @"C10"]];
	[cell setFunction:^NSNumber *(IMSpreadSheet *calculator, IMSpreadSheetCell *cell) {
		return [ssTotals sumOfCells:@[@"C9", @"C10"]];
	}];
	
	cell = [ssTotals spreadSheetCellForIdentifier:@"D11"];
	cell.watchList = [ssTotals arrayOfCells:@[@"B11", @"C11"]];
	[cell setFunction:^NSNumber *(IMSpreadSheet *calculator, IMSpreadSheetCell *cell) {
		double val1 = 1.0 - ([ssTotals valueForIdentifier:@"C11"].doubleValue / [ssTotals valueForIdentifier:@"B11"].doubleValue);
		return [NSNumber numberWithDouble:val1];
	}];
	
	cell = [ssTotals spreadSheetCellForIdentifier:@"E11"];
	cell.watchList = [ssTotals arrayOfCells:@[@"B11", @"C11"]];
	[cell setFunction:^NSNumber *(IMSpreadSheet *calculator, IMSpreadSheetCell *cell) {
		double val1 = [ssTotals valueForIdentifier:@"B11"].doubleValue - [ssTotals valueForIdentifier:@"C11"].doubleValue;
		return [NSNumber numberWithDouble:val1];
	}];
	
	cell = [ssTotals spreadSheetCellForIdentifier:@"B15"];
	cell.watchList = @[[ssAnalysis spreadSheetCellForIdentifier:@"B6"]];
	[cell setFunction:^NSNumber *(IMSpreadSheet *calculator, IMSpreadSheetCell *cell) {
		return [ssAnalysis valueForIdentifier:@"B6"];
	}];
	
	cell = [ssTotals spreadSheetCellForIdentifier:@"C15"];
	cell.watchList = @[[ssAnalysis spreadSheetCellForIdentifier:@"C6"]];
	[cell setFunction:^NSNumber *(IMSpreadSheet *calculator, IMSpreadSheetCell *cell) {
		return [ssAnalysis valueForIdentifier:@"C6"];
	}];
	
	cell = [ssTotals spreadSheetCellForIdentifier:@"D15"];
	cell.watchList = [ssTotals arrayOfCells:@[@"B15", @"C15"]];
	[cell setFunction:^NSNumber *(IMSpreadSheet *calculator, IMSpreadSheetCell *cell) {
		double val1 = 1.0 - ([ssTotals valueForIdentifier:@"C15"].doubleValue / [ssTotals valueForIdentifier:@"B15"].doubleValue);
		return [NSNumber numberWithDouble:val1];
	}];
	
	cell = [ssTotals spreadSheetCellForIdentifier:@"E15"];
	cell.watchList = [ssTotals arrayOfCells:@[@"B15", @"C15"]];
	[cell setFunction:^NSNumber *(IMSpreadSheet *calculator, IMSpreadSheetCell *cell) {
		double val1 = [ssTotals valueForIdentifier:@"B15"].doubleValue - [ssTotals valueForIdentifier:@"C15"].doubleValue;
		return [NSNumber numberWithDouble:val1];
	}];
	
	cell = [ssTotals spreadSheetCellForIdentifier:@"B19"];
	cell.watchList = @[[ssScreening spreadSheetCellForIdentifier:@"B5"]
					   ,[ssScreening spreadSheetCellForIdentifier:@"B24"]];
	[cell setFunction:^NSNumber *(IMSpreadSheet *calculator, IMSpreadSheetCell *cell) {
		double val1 = [ssScreening valueForIdentifier:@"B5"].doubleValue * [ssScreening valueForIdentifier:@"B24"].doubleValue * 2.0;
		return [NSNumber numberWithDouble: val1];
	}];
	
	cell = [ssTotals spreadSheetCellForIdentifier:@"C19"];
	cell.watchList = @[[ssScreening spreadSheetCellForIdentifier:@"B5"]];
	[cell setFunction:^NSNumber *(IMSpreadSheet *calculator, IMSpreadSheetCell *cell) {
		return [ssScreening valueForIdentifier:@"B5"];
	}];
	
	cell = [ssTotals spreadSheetCellForIdentifier:@"D19"];
	cell.watchList = [ssTotals arrayOfCells:@[@"B19", @"C19"]];
	[cell setFunction:^NSNumber *(IMSpreadSheet *calculator, IMSpreadSheetCell *cell) {
		double val1 = 1.0 - ([ssTotals valueForIdentifier:@"C19"].doubleValue / [ssTotals valueForIdentifier:@"B19"].doubleValue);
		return [NSNumber numberWithDouble:val1];
	}];
	
	cell = [ssTotals spreadSheetCellForIdentifier:@"E19"];
	cell.watchList = [ssTotals arrayOfCells:@[@"B19", @"C19"]];
	[cell setFunction:^NSNumber *(IMSpreadSheet *calculator, IMSpreadSheetCell *cell) {
		double val1 = [ssTotals valueForIdentifier:@"B19"].doubleValue - [ssTotals valueForIdentifier:@"C19"].doubleValue;
		return [NSNumber numberWithDouble:val1];
	}];
	
	cell = [ssTotals spreadSheetCellForIdentifier:@"B22"];
	cell.watchList = @[[ssScreening spreadSheetCellForIdentifier:@"B5"]
					   ,[ssScreening spreadSheetCellForIdentifier:@"B20"]];
	[cell setFunction:^NSNumber *(IMSpreadSheet *calculator, IMSpreadSheetCell *cell) {
		return [ssScreening multiplyCells:@[@"B5", @"B20"]];
	}];
	
	cell = [ssTotals spreadSheetCellForIdentifier:@"C22"];
	cell.watchList = @[[ssScreening spreadSheetCellForIdentifier:@"B5"]
					   ,[ssSpotlight spreadSheetCellForIdentifier:@"B9"]];
	[cell setFunction:^NSNumber *(IMSpreadSheet *calculator, IMSpreadSheetCell *cell) {
		double val1 = [ssScreening valueForIdentifier:@"B5"].doubleValue * [ssSpotlight valueForIdentifier:@"B9"].doubleValue;
		return [NSNumber numberWithDouble:val1];
	}];
	
	cell = [ssTotals spreadSheetCellForIdentifier:@"D22"];
	cell.watchList = [ssTotals arrayOfCells:@[@"B22", @"C22"]];
	[cell setFunction:^NSNumber *(IMSpreadSheet *calculator, IMSpreadSheetCell *cell) {
		double val1 = 1.0 - ([ssTotals valueForIdentifier:@"C22"].doubleValue / [ssTotals valueForIdentifier:@"B22"].doubleValue);
		return [NSNumber numberWithDouble:val1];
	}];
	
	cell = [ssTotals spreadSheetCellForIdentifier:@"E22"];
	cell.watchList = [ssTotals arrayOfCells:@[@"B22", @"C22"]];
	[cell setFunction:^NSNumber *(IMSpreadSheet *calculator, IMSpreadSheetCell *cell) {
		double val1 = [ssTotals valueForIdentifier:@"B22"].doubleValue - [ssTotals valueForIdentifier:@"C22"].doubleValue;
		return [NSNumber numberWithDouble:val1];
	}];
}

@end