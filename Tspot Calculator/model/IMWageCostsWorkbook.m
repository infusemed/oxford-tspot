//
//  IMWageCostsWorkbook.m
//  Tspot Calculator
//
//  Created by Anthony Long on 4/1/15.
//  Copyright (c) 2015 Infuse Medical. All rights reserved.
//

#import "IMWageCostsWorkbook.h"

@implementation IMWageCostsWorkbook

NSString * const kIMWageCostsSpreadsheet = @"IMWageCostsSpreadsheet";

- (void) setCustomerName:(NSString *)customerName
{
	[super setCustomerName:customerName];
	[[self spreadSheetWithName:kIMWageCostsSpreadsheet] spreadSheetCellForIdentifier:@"B3"].textValue = customerName;
}

- (NSString *) customerName
{
	return [[self spreadSheetWithName:kIMWageCostsSpreadsheet] spreadSheetCellForIdentifier:@"B3"].textValue;
}

- (void) initializeWorkbook
{
	[super initializeWorkbook];
	IMSpreadSheet *ss = [[IMSpreadSheet alloc] init];
	[self setSpreadSheet:ss WithName:kIMWageCostsSpreadsheet];
	IMSpreadSheetCell *cell;
	
	[ss spreadSheetCellForIdentifier:@"C4"];
	[ss spreadSheetCellForIdentifier:@"C6"].maxValue = @1.0;
	[ss spreadSheetCellForIdentifier:@"C8"];
	[ss spreadSheetCellForIdentifier:@"C10"].value = @3.0;
	[ss spreadSheetCellForIdentifier:@"C15"];
	[ss spreadSheetCellForIdentifier:@"C17"];
	[ss spreadSheetCellForIdentifier:@"C19"];
	[ss spreadSheetCellForIdentifier:@"C23"];
	[ss spreadSheetCellForIdentifier:@"C25"].value = @1.5;
	[ss spreadSheetCellForIdentifier:@"C27"].value = @2.0;
	[ss spreadSheetCellForIdentifier:@"C29"];
	
	cell = [ss spreadSheetCellForIdentifier:@"C33"];
//	cell.value = @0.6;
	cell.maxValue = @1.0;
	[cell setSetterFunction:^(IMSpreadSheetCell *cell) {
		if(![ss spreadSheetCellForIdentifier:@"C34"].value || [ss spreadSheetCellForIdentifier:@"C34"].value.doubleValue + cell.value.doubleValue != 1.0)
		{
			[ss spreadSheetCellForIdentifier:@"C34"].value = [NSNumber numberWithDouble:1.0 - cell.value.doubleValue];
		}
	}];
	cell = [ss spreadSheetCellForIdentifier:@"C34"];
//	cell.value = @0.4;
	cell.maxValue = @1.0;
	[cell setSetterFunction:^(IMSpreadSheetCell *cell) {
		if(![ss spreadSheetCellForIdentifier:@"C33"].value || [ss spreadSheetCellForIdentifier:@"C33"].value.doubleValue + cell.value.doubleValue != 1.0)
		{
			[ss spreadSheetCellForIdentifier:@"C33"].value = [NSNumber numberWithDouble:1.0 - cell.value.doubleValue];
		}
	}];
	
	[ss spreadSheetCellForIdentifier:@"C38"].value = @2.83;
	[ss spreadSheetCellForIdentifier:@"C40"];
	
	cell = [ss spreadSheetCellForIdentifier:@"H4"];
	cell.watchList = [ss arrayOfCells:@[@"C4", @"C6", @"C8", @"C29", @"C15", @"C17", @"C19"]];
	[cell setFunction:^NSNumber *(IMSpreadSheet *calculator, IMSpreadSheetCell *cell) {
		double val1 = [ss multiplyCells:@[@"C4", @"C6", @"C8", @"C29"]].doubleValue;
		double val2 = [ss multiplyCells:@[@"C15", @"C17", @"C19", @"C29"]].doubleValue;
		return [NSNumber numberWithDouble:val1 + val2];
	}];
	
	cell = [ss spreadSheetCellForIdentifier:@"I4"];
	cell.watchList = [ss arrayOfCells:@[@"C4", @"C6", @"C10", @"C29", @"C15", @"C17", @"C19"]];
	[cell setFunction:^NSNumber *(IMSpreadSheet *calculator, IMSpreadSheetCell *cell) {
		double val1 = [ss multiplyCells:@[@"C4", @"C6", @"C10", @"C29"]].doubleValue;
		double val2 = [ss multiplyCells:@[@"C15", @"C17", @"C19", @"C29"]].doubleValue;
		return [NSNumber numberWithDouble:val1 + val2];
	}];
	
	cell = [ss spreadSheetCellForIdentifier:@"K4"];
	cell.watchList = [ss arrayOfCells:@[@"I4", @"H4"]];
	[cell setFunction:^NSNumber *(IMSpreadSheet *calculator, IMSpreadSheetCell *cell) {
		return [ss subtractCell:@"I4" fromCell:@"H4"];
	}];
	
	
	
	cell = [ss spreadSheetCellForIdentifier:@"H6"];
	cell.watchList = [ss arrayOfCells:@[@"C4", @"C6", @"C8", @"C29", @"C23", @"C25", @"C33", @"C15", @"C17", @"C19"]];
	[cell setFunction:^NSNumber *(IMSpreadSheet *calculator, IMSpreadSheetCell *cell) {
		double val1 = [ss multiplyCells:@[@"C4", @"C6", @"C8", @"C29", @"C23", @"C25", @"C33"]].doubleValue;
		double val2 = [ss multiplyCells:@[@"C15", @"C17", @"C19", @"C29", @"C23", @"C25", @"C33"]].doubleValue;
		return [NSNumber numberWithDouble:val1 + val2];
	}];
	
	cell = [ss spreadSheetCellForIdentifier:@"I6"];
	cell.watchList = [ss arrayOfCells:@[@"C4", @"C6", @"C10", @"C29", @"C23", @"C33", @"C15", @"C17", @"C19"]];
	[cell setFunction:^NSNumber *(IMSpreadSheet *calculator, IMSpreadSheetCell *cell) {
		double val1 = [ss multiplyCells:@[@"C4", @"C6", @"C10", @"C29", @"C23", @"C33"]].doubleValue;
		double val2 = [ss multiplyCells:@[@"C15", @"C17", @"C19", @"C29", @"C23", @"C33"]].doubleValue;
		return [NSNumber numberWithDouble:val1 + val2];
	}];
	
	cell = [ss spreadSheetCellForIdentifier:@"K6"];
	cell.watchList = [ss arrayOfCells:@[@"I6", @"H6"]];
	[cell setFunction:^NSNumber *(IMSpreadSheet *calculator, IMSpreadSheetCell *cell) {
		return [ss subtractCell:@"I6" fromCell:@"H6"];
	}];
	
//	((C4*C6*C8*C29*(C23*C27))*C34)+((C15*C17*C19*C29*C23*C27*C34))
	
	cell = [ss spreadSheetCellForIdentifier:@"H7"];
	cell.watchList = [ss arrayOfCells:@[@"C4", @"C6", @"C8", @"C29", @"C23", @"C27", @"C34", @"C15", @"C17", @"C19"]];
	[cell setFunction:^NSNumber *(IMSpreadSheet *calculator, IMSpreadSheetCell *cell) {
		double val1 = [ss multiplyCells:@[@"C4", @"C6", @"C8", @"C29", @"C23", @"C27", @"C34"]].doubleValue;
		double val2 = [ss multiplyCells:@[@"C15", @"C17", @"C19", @"C29", @"C23", @"C27", @"C34"]].doubleValue;
		return [NSNumber numberWithDouble:val1 + val2];
	}];
	
//	(C4*C6*C10*C29*C23*C34)+(C15*C17*C19*C29*C23*C34)
	cell = [ss spreadSheetCellForIdentifier:@"I7"];
	cell.watchList = [ss arrayOfCells:@[@"C4", @"C6", @"C10", @"C29", @"C23", @"C34", @"C15", @"C17", @"C19"]];
	[cell setFunction:^NSNumber *(IMSpreadSheet *calculator, IMSpreadSheetCell *cell) {
		double val1 = [ss multiplyCells:@[@"C4", @"C6", @"C10", @"C29", @"C23", @"C34"]].doubleValue;
		double val2 = [ss multiplyCells:@[@"C15", @"C17", @"C19", @"C29", @"C23", @"C34"]].doubleValue;
		return [NSNumber numberWithDouble:val1 + val2];
	}];
	
	cell = [ss spreadSheetCellForIdentifier:@"K7"];
	cell.watchList = [ss arrayOfCells:@[@"I7", @"H7"]];
	[cell setFunction:^NSNumber *(IMSpreadSheet *calculator, IMSpreadSheetCell *cell) {
		return [ss subtractCell:@"I7" fromCell:@"H7"];
	}];
	
	cell = [ss spreadSheetCellForIdentifier:@"H9"];
	cell.watchList = [ss arrayOfCells:@[@"C4", @"C6", @"C15", @"C38"]];
	[cell setFunction:^NSNumber *(IMSpreadSheet *calculator, IMSpreadSheetCell *cell) {
		double val1 = [ss multiplyCells:@[@"C4", @"C6"]].doubleValue;
		double val2 = val1 + [ss valueForIdentifier:@"C15"].doubleValue;
		double val3 = val2 * [ss valueForIdentifier:@"C38"].doubleValue;
		return [NSNumber numberWithDouble:val3];
	}];
	
	cell = [ss spreadSheetCellForIdentifier:@"I9"];
	cell.watchList = [ss arrayOfCells:@[@"C4", @"C6", @"C15", @"C40"]];
	[cell setFunction:^NSNumber *(IMSpreadSheet *calculator, IMSpreadSheetCell *cell) {
		double val1 = [ss multiplyCells:@[@"C4", @"C6"]].doubleValue;
		double val2 = val1 + [ss valueForIdentifier:@"C15"].doubleValue;
		double val3 = val2 * [ss valueForIdentifier:@"C40"].doubleValue;
		return [NSNumber numberWithDouble:val3];
	}];
	
	cell = [ss spreadSheetCellForIdentifier:@"K9"];
	cell.watchList = [ss arrayOfCells:@[@"I9", @"H9"]];
	[cell setFunction:^NSNumber *(IMSpreadSheet *calculator, IMSpreadSheetCell *cell) {
		return [ss subtractCell:@"I9" fromCell:@"H9"];
	}];
	
	cell = [ss spreadSheetCellForIdentifier:@"H11"];
	cell.watchList = [ss arrayOfCells:@[@"H6", @"H7", @"H9"]];
	[cell setFunction:^NSNumber *(IMSpreadSheet *calculator, IMSpreadSheetCell *cell) {
		return [ss sumOfCells:@[@"H6", @"H7", @"H9"]];
	}];
	
	cell = [ss spreadSheetCellForIdentifier:@"I11"];
	cell.watchList = [ss arrayOfCells:@[@"I6", @"I7", @"I9"]];
	[cell setFunction:^NSNumber *(IMSpreadSheet *calculator, IMSpreadSheetCell *cell) {
		return [ss sumOfCells:@[@"I6", @"I7", @"I9"]];
	}];
	
	cell = [ss spreadSheetCellForIdentifier:@"K11"];
	cell.watchList = [ss arrayOfCells:@[@"I11", @"H11"]];
	[cell setFunction:^NSNumber *(IMSpreadSheet *calculator, IMSpreadSheetCell *cell) {
		return [ss subtractCell:@"I11" fromCell:@"H11"];
	}];
	
	cell = [ss spreadSheetCellForIdentifier:@"H14"];
	cell.watchList = [ss arrayOfCells:@[@"H6", @"H7"]];
	[cell setFunction:^NSNumber *(IMSpreadSheet *calculator, IMSpreadSheetCell *cell) {
		double val1 = [ss sumOfCells:@[@"H6", @"H7"]].doubleValue;
		return [NSNumber numberWithDouble:val1 * 12.0];
	}];
	
	cell = [ss spreadSheetCellForIdentifier:@"I14"];
	cell.watchList = [ss arrayOfCells:@[@"I6", @"I7"]];
	[cell setFunction:^NSNumber *(IMSpreadSheet *calculator, IMSpreadSheetCell *cell) {
		double val1 = [ss sumOfCells:@[@"I6", @"I7"]].doubleValue;
		return [NSNumber numberWithDouble:val1 * 12.0];
	}];
	
	cell = [ss spreadSheetCellForIdentifier:@"K14"];
	cell.watchList = [ss arrayOfCells:@[@"I14", @"H14"]];
	[cell setFunction:^NSNumber *(IMSpreadSheet *calculator, IMSpreadSheetCell *cell) {
		return [ss subtractCell:@"I14" fromCell:@"H14"];
	}];
	
	cell = [ss spreadSheetCellForIdentifier:@"H16"];
	cell.watchList = [ss arrayOfCells:@[@"K14", @"C40"]];
	[cell setFunction:^NSNumber *(IMSpreadSheet *calculator, IMSpreadSheetCell *cell) {
		double val1 = [ss valueForIdentifier:@"K14"].doubleValue / [ss valueForIdentifier:@"C40"].doubleValue;
		return [NSNumber numberWithDouble:val1];
	}];
}

@end