//
//  IMAnalysisWorkbook.m
//  Practice Productivity Analysis
//
//  Created by Anthony Long on 5/1/13.
//  Copyright (c) 2013 Infuse Medical. All rights reserved.
//

#import "IMSpotlightEconWorkbook.h"

@implementation IMSpotlightEconWorkbook

NSString * const kIMAnnualTBScreeningProgramSheet = @"Annual TB Screening Program";
NSString * const kIMSPOTLIGHTSheet = @"SPOTLIGHT";
NSString * const kIMNewHireScreeningSheet = @"New Hire TB Screening Program";
NSString * const kIMEmployeeCostSheet = @"Employee Cost Data";
NSString * const kIMAnalysisSheet = @"Analysis";
NSString * const kIMEmployeeTimeReferenceDataSheet = @"Employee Time Reference Data";
NSString * const kIMSpotlightReferenceDataSheet = @"SPOTLIGHT Reference Data";
NSString * const kIMChartsAndTotals = @"Charts And Totals";

//- (id) init
//{
//	self = [super init];
//	if(self)
//	{
//		[self initializeWorkbook];
//	}
//	
//	return self;
//}
//
//- (id) initWithFile:(NSString *)filePath
//{
//	self = [super init];
//	if(self)
//	{
//		self.fileName = filePath.lastPathComponent;
//		[self initializeWorkbook];
//		NSDictionary * d = [NSDictionary dictionaryWithContentsOfFile:filePath];
//		[d enumerateKeysAndObjectsUsingBlock:^(id key, id obj, BOOL *stop) {
//			NSDictionary * d2 = (NSDictionary *)obj;
//			IMSpreadSheet * spreadsheet = [self spreadSheetWithName:(NSString *)key];
//			[d2 enumerateKeysAndObjectsUsingBlock:^(id key, id obj, BOOL *stop) {
//				NSArray * a = [(NSString *)key componentsSeparatedByString:@"-"];
//				NSUInteger indexes[] = {[[a objectAtIndex:0] intValue], [[a objectAtIndex:1] intValue]};
//				if([obj isKindOfClass:[NSString class]])
//				{
//					[spreadsheet setTextValue:obj forIndexPath:[NSIndexPath indexPathWithIndexes:indexes length:2]];
//				}
//				else
//				{
//					[spreadsheet setValue:obj forIndexPath:[NSIndexPath indexPathWithIndexes:indexes length:2]];
//				}
//			}];
//		}];
//	}
//	
//	return self;
//}

- (void) setCustomerName:(NSString *)customerName
{
	[super setCustomerName:customerName];
	[[self spreadSheetWithName:kIMAnnualTBScreeningProgramSheet] spreadSheetCellForIdentifier:@"B3"].textValue = customerName;
}

- (NSString *) customerName
{
	return [[self spreadSheetWithName:kIMAnnualTBScreeningProgramSheet] spreadSheetCellForIdentifier:@"B3"].textValue;
}

- (void) initializeWorkbook
{
	[super initializeWorkbook];
	[self setSpreadSheet:[[IMSpreadSheet alloc] init]
				WithName:kIMAnnualTBScreeningProgramSheet];
	[self setSpreadSheet:[[IMSpreadSheet alloc] init]
				WithName:kIMSPOTLIGHTSheet];
	[self setSpreadSheet:[[IMSpreadSheet alloc] init]
				WithName:kIMNewHireScreeningSheet];
	[self setSpreadSheet:[[IMSpreadSheet alloc] init]
				WithName:kIMEmployeeCostSheet];
	[self setSpreadSheet:[[IMSpreadSheet alloc] init]
				WithName:kIMAnalysisSheet];
	[self setSpreadSheet:[[IMSpreadSheet alloc] init]
				WithName:kIMEmployeeTimeReferenceDataSheet];
	[self setSpreadSheet:[[IMSpreadSheet alloc] init]
				WithName:kIMSpotlightReferenceDataSheet];
	[self setSpreadSheet:[[IMSpreadSheet alloc] init]
				WithName:kIMChartsAndTotals];
	
	[self initAnnualTBScreening];
	[self initSpotlight];
	[self initNewHireScreening];
	[self initEmployeeCostData];
	[self initAnalysis];
	[self initEmployeeTimeReferenceData];
	[self initCharts];
}

- (void) initAnnualTBScreening
{
	IMSpreadSheet * spreadSheet = [self spreadSheetWithName:kIMAnnualTBScreeningProgramSheet];
	
	IMSpreadSheetCell * cell = [spreadSheet spreadSheetCellForIdentifier:@"B3"];
	cell = [spreadSheet spreadSheetCellForIdentifier:@"B5"];
	cell = [spreadSheet spreadSheetCellForIdentifier:@"B7"];
	cell.value = @208;
	cell = [spreadSheet spreadSheetCellForIdentifier:@"B10"];
	cell.value = @2.83;
	cell = [spreadSheet spreadSheetCellForIdentifier:@"B13"];
	cell = [spreadSheet spreadSheetCellForIdentifier:@"B16"];
	cell.value = @0.2;
	cell = [spreadSheet spreadSheetCellForIdentifier:@"B20"];
	cell = [spreadSheet spreadSheetCellForIdentifier:@"B21"];
}

- (void) initSpotlight
{
	IMSpreadSheet * spreadSheet = [self spreadSheetWithName:kIMSPOTLIGHTSheet];
	IMSpreadSheet *ssNewHire = [self spreadSheetWithName:kIMNewHireScreeningSheet];
	IMSpreadSheetCell * cell = [spreadSheet spreadSheetCellForIdentifier:@"B2"];
	cell = [spreadSheet spreadSheetCellForIdentifier:@"B4"];
	cell.value = @20.0;
	cell = [spreadSheet spreadSheetCellForIdentifier:@"B6"];
	cell.value = @4.0;
	cell = [spreadSheet spreadSheetCellForIdentifier:@"B9"];
	cell.value = @0.011;
	[cell setSetterFunction:^(IMSpreadSheetCell *cell) {
		[ssNewHire spreadSheetCellForIdentifier:@"C21"].value = cell.value;
	}];
}

- (void) initNewHireScreening
{
	
	IMSpreadSheet * spreadSheet = [self spreadSheetWithName:kIMNewHireScreeningSheet];
	
	IMSpreadSheetCell * cell = [spreadSheet spreadSheetCellForIdentifier:@"B4"];
	cell = [spreadSheet spreadSheetCellForIdentifier:@"B8"];
	cell.watchList = @[[[self spreadSheetWithName:kIMAnnualTBScreeningProgramSheet] spreadSheetCellForIdentifier:@"B10"]];
	[cell setFunction:^NSNumber *(IMSpreadSheet *calculator, IMSpreadSheetCell *cell) {
		return [[calculator.workbook spreadSheetWithName:kIMAnnualTBScreeningProgramSheet] valueForIdentifier:@"B10"];
	}];
	cell = [spreadSheet spreadSheetCellForIdentifier:@"C8"];
	cell = [spreadSheet spreadSheetCellForIdentifier:@"B11"];
	cell.value = @0.0;
	cell = [spreadSheet spreadSheetCellForIdentifier:@"B12"];
	cell.value = @0.5;
	cell = [spreadSheet spreadSheetCellForIdentifier:@"B16"];
	cell.value = @0.0;
	cell = [spreadSheet spreadSheetCellForIdentifier:@"B17"];
	cell.value = @1.0;
	cell = [spreadSheet spreadSheetCellForIdentifier:@"B21"];
	cell.value = @0.0;
	cell = [spreadSheet spreadSheetCellForIdentifier:@"B22"];
	cell.value = @0.011;
	[spreadSheet spreadSheetCellForIdentifier:@"C21"].value = @0.011;
	cell = [spreadSheet spreadSheetCellForIdentifier:@"C155"];
	cell.watchList = [spreadSheet arrayOfCells:@[@"C154", @"C153"]];
	[cell setFunction:^NSNumber *(IMSpreadSheet *calculator, IMSpreadSheetCell *cell) {
		return [calculator divideCells:@[@"C154", @"C153"]];
	}];
}

- (void) initEmployeeCostData
{
	IMSpreadSheet * spreadSheet = [self spreadSheetWithName:kIMEmployeeCostSheet];
	
	IMSpreadSheetCell * cell = [spreadSheet spreadSheetCellForIdentifier:@"B8"];
	cell.value = @54.44;
	cell = [spreadSheet spreadSheetCellForIdentifier:@"F8"];
	cell.value = @54.44;
	cell = [spreadSheet spreadSheetCellForIdentifier:@"B12"];
	cell.value = @110.52;
	cell = [spreadSheet spreadSheetCellForIdentifier:@"C12"];
	cell.value = @0.159;
	cell = [spreadSheet spreadSheetCellForIdentifier:@"D12"];
	cell.watchList = [spreadSheet arrayOfCells:@[@"C12", @"B12"]];
	[cell setFunction:^NSNumber *(IMSpreadSheet *calculator, IMSpreadSheetCell *cell) {
		return [calculator multiplyCells:@[@"C12", @"B12"]];
	}];
	
	cell = [spreadSheet spreadSheetCellForIdentifier:@"B13"];
	cell.value = @34.65;
	cell = [spreadSheet spreadSheetCellForIdentifier:@"C13"];
	cell.value = @0.363;
	cell = [spreadSheet spreadSheetCellForIdentifier:@"D13"];
	cell.watchList = [spreadSheet arrayOfCells:@[@"B13", @"C13"]];
	[cell setFunction:^NSNumber *(IMSpreadSheet *calculator, IMSpreadSheetCell *cell) {
		return [spreadSheet multiplyCells:@[@"B13", @"C13"]];
	}];
	
	cell = [spreadSheet spreadSheetCellForIdentifier:@"B14"];
	cell.value = @139.04;
	cell = [spreadSheet spreadSheetCellForIdentifier:@"C14"];
	cell.value = @0.063;
	cell = [spreadSheet spreadSheetCellForIdentifier:@"D14"];
	cell.watchList = [spreadSheet arrayOfCells:@[@"B14", @"C14"]];
	[cell setFunction:^NSNumber *(IMSpreadSheet *calculator, IMSpreadSheetCell *cell) {
		return [calculator multiplyCells:@[@"B14", @"C14"]];
	}];
	
	cell = [spreadSheet spreadSheetCellForIdentifier:@"B15"];
	cell.value = @26.9;
	cell = [spreadSheet spreadSheetCellForIdentifier:@"C15"];
	cell.value = @0.225;
	cell = [spreadSheet spreadSheetCellForIdentifier:@"D15"];
	cell.watchList = [spreadSheet arrayOfCells:@[@"B15", @"C15"]];
	[cell setFunction:^NSNumber *(IMSpreadSheet *calculator, IMSpreadSheetCell *cell) {
		return [calculator multiplyCells:@[@"B15", @"C15"]];
	}];
	
	cell = [spreadSheet spreadSheetCellForIdentifier:@"B16"];
	cell.value = @23.27;
	cell = [spreadSheet spreadSheetCellForIdentifier:@"C16"];
	cell.value = @0.19;
	cell = [spreadSheet spreadSheetCellForIdentifier:@"D16"];
	cell.watchList = [spreadSheet arrayOfCells:@[@"B16", @"C16"]];
	[cell setFunction:^NSNumber *(IMSpreadSheet *calculator, IMSpreadSheetCell *cell) {
		return [calculator multiplyCells:@[@"B16", @"C16"]];
	}];
	
	cell = [spreadSheet spreadSheetCellForIdentifier:@"F12"];
	cell.value = @110.52;
	cell = [spreadSheet spreadSheetCellForIdentifier:@"G12"];
	cell.value = @0.159;
	cell = [spreadSheet spreadSheetCellForIdentifier:@"H12"];
	cell.watchList = [spreadSheet arrayOfCells:@[@"G12", @"F12"]];
	[cell setFunction:^NSNumber *(IMSpreadSheet *calculator, IMSpreadSheetCell *cell) {
		return [calculator multiplyCells:@[@"G12", @"F12"]];
	}];
	
	cell = [spreadSheet spreadSheetCellForIdentifier:@"F13"];
	cell.value = @34.65;
	cell = [spreadSheet spreadSheetCellForIdentifier:@"G13"];
	cell.value = @0.363;
	cell = [spreadSheet spreadSheetCellForIdentifier:@"H13"];
	cell.watchList = [spreadSheet arrayOfCells:@[@"F13", @"G13"]];
	[cell setFunction:^NSNumber *(IMSpreadSheet *calculator, IMSpreadSheetCell *cell) {
		return [spreadSheet multiplyCells:@[@"F13", @"G13"]];
	}];
	
	cell = [spreadSheet spreadSheetCellForIdentifier:@"F14"];
	cell.value = @139.04;
	cell = [spreadSheet spreadSheetCellForIdentifier:@"G14"];
	cell.value = @0.063;
	cell = [spreadSheet spreadSheetCellForIdentifier:@"H14"];
	cell.watchList = [spreadSheet arrayOfCells:@[@"F14", @"G14"]];
	[cell setFunction:^NSNumber *(IMSpreadSheet *calculator, IMSpreadSheetCell *cell) {
		return [calculator multiplyCells:@[@"F14", @"G14"]];
	}];
	
	cell = [spreadSheet spreadSheetCellForIdentifier:@"F15"];
	cell.value = @26.9;
	cell = [spreadSheet spreadSheetCellForIdentifier:@"G15"];
	cell.value = @0.225;
	cell = [spreadSheet spreadSheetCellForIdentifier:@"H15"];
	cell.watchList = [spreadSheet arrayOfCells:@[@"F15", @"G15"]];
	[cell setFunction:^NSNumber *(IMSpreadSheet *calculator, IMSpreadSheetCell *cell) {
		return [calculator multiplyCells:@[@"F15", @"G15"]];
	}];
	
	cell = [spreadSheet spreadSheetCellForIdentifier:@"F16"];
	cell.value = @23.27;
	cell = [spreadSheet spreadSheetCellForIdentifier:@"G16"];
	cell.value = @0.19;
	cell = [spreadSheet spreadSheetCellForIdentifier:@"H16"];
	cell.watchList = [spreadSheet arrayOfCells:@[@"F16", @"G16"]];
	[cell setFunction:^NSNumber *(IMSpreadSheet *calculator, IMSpreadSheetCell *cell) {
		return [calculator multiplyCells:@[@"F16", @"G16"]];
	}];
	
	cell = [spreadSheet spreadSheetCellForIdentifier:@"C18"];
	cell.watchList = [spreadSheet arrayOfCells:@[@"C12", @"C13", @"C14", @"C15", @"C16"]];
	[cell setFunction:^NSNumber *(IMSpreadSheet *calculator, IMSpreadSheetCell *cell) {
		return [calculator sumOfCellRange:@"C12:C16"];
	}];
	
	cell = [spreadSheet spreadSheetCellForIdentifier:@"C19"];
	cell.watchList = [spreadSheet arrayOfCells:@[@"D12", @"D13", @"D14", @"D15", @"D16"]];
	[cell setFunction:^NSNumber *(IMSpreadSheet *calculator, IMSpreadSheetCell *cell) {
		return [calculator sumOfCellRange:@"D12:D16"];
	}];
	
	
	cell = [spreadSheet spreadSheetCellForIdentifier:@"G18"];
	cell.watchList = [spreadSheet arrayOfCells:@[@"G12", @"G13", @"G14", @"G15", @"G16"]];
	[cell setFunction:^NSNumber *(IMSpreadSheet *calculator, IMSpreadSheetCell *cell) {
		return [calculator sumOfCellRange:@"G12:G16"];
	}];
	
	cell = [spreadSheet spreadSheetCellForIdentifier:@"G19"];
	cell.watchList = [spreadSheet arrayOfCells:@[@"H12", @"H13", @"H14", @"H15", @"H16"]];
	[cell setFunction:^NSNumber *(IMSpreadSheet *calculator, IMSpreadSheetCell *cell) {
		return [calculator sumOfCellRange:@"H12:H16"];
	}];
}

- (void) initAnalysis
{
	IMSpreadSheet * spreadSheet = [self spreadSheetWithName:kIMAnalysisSheet];
	IMSpreadSheet * ssEmployeeTime = [self spreadSheetWithName:kIMEmployeeTimeReferenceDataSheet];
	IMSpreadSheet * ssSpotlight = [self spreadSheetWithName:kIMSPOTLIGHTSheet];
	IMSpreadSheet * ssEmpCostData = [self spreadSheetWithName:kIMEmployeeCostSheet];
	IMSpreadSheet * ssScreening = [self spreadSheetWithName:kIMAnnualTBScreeningProgramSheet];
	IMSpreadSheet * ssNewHire = [self spreadSheetWithName:kIMNewHireScreeningSheet];
	
	IMSpreadSheetCell * cell = [spreadSheet spreadSheetCellForIdentifier:@"B3"];
	cell.watchList = [ssEmployeeTime arrayOfCells:@[@"B9", @"B10", @"B11", @"B14"]];
	[cell setFunction:^NSNumber *(IMSpreadSheet *calculator, IMSpreadSheetCell *cell) {
		IMSpreadSheet *ss = [calculator.workbook spreadSheetWithName:kIMEmployeeTimeReferenceDataSheet];
		return [ss sumOfCells:@[@"B9", @"B10", @"B11", @"B14"]];
	}];
	
	cell = [spreadSheet spreadSheetCellForIdentifier:@"C3"];
	cell.watchList = @[[ssSpotlight spreadSheetCellForIdentifier:@"B4"]
					   ,[ssEmpCostData spreadSheetCellForIdentifier:@"B8"]
					   ,[ssSpotlight spreadSheetCellForIdentifier:@"B6"]
					   ,[ssScreening spreadSheetCellForIdentifier:@"B5"]];
	[cell setFunction:^NSNumber *(IMSpreadSheet *calculator, IMSpreadSheetCell *cell) {
		NSNumber *val1 = [[calculator.workbook spreadSheetWithName:kIMSPOTLIGHTSheet] valueForIdentifier:@"B4"];
		NSNumber *val2 = [[calculator.workbook spreadSheetWithName:kIMEmployeeCostSheet] valueForIdentifier:@"B8"];
		NSNumber *val3 = [[calculator.workbook spreadSheetWithName:kIMSPOTLIGHTSheet] valueForIdentifier:@"B6"];
		NSNumber *val4 = [[calculator.workbook spreadSheetWithName:kIMAnnualTBScreeningProgramSheet] valueForIdentifier:@"B5"];
		return [NSNumber numberWithDouble:(val1.doubleValue * val2.doubleValue * val3.doubleValue)/val4.doubleValue];
	}];
	
	cell = [spreadSheet spreadSheetCellForIdentifier:@"B4"];
	cell.watchList = @[[ssEmployeeTime spreadSheetCellForIdentifier:@"B13"]];
	[cell setFunction:^NSNumber *(IMSpreadSheet *calculator, IMSpreadSheetCell *cell) {
		return [ssEmployeeTime valueForIdentifier:@"B13"];
	}];
	
	cell = [spreadSheet spreadSheetCellForIdentifier:@"C4"];
	cell.value = @0.0;
	
	cell = [spreadSheet spreadSheetCellForIdentifier:@"B5"];
	cell.watchList = [ssEmployeeTime arrayOfCells:@[@"B21", @"B22"]];
	[cell setFunction:^NSNumber *(IMSpreadSheet *calculator, IMSpreadSheetCell *cell) {
		return [calculator sumOfCellsInArray:cell.watchList];
	}];
	
	cell = [spreadSheet spreadSheetCellForIdentifier:@"C5"];
	cell.watchList = [ssEmployeeTime arrayOfCells:@[@"B24"]];
	[cell setFunction:^NSNumber *(IMSpreadSheet *calculator, IMSpreadSheetCell *cell) {
		return [ssEmployeeTime valueForIdentifier:@"B24"];
	}];
	
	cell = [spreadSheet spreadSheetCellForIdentifier:@"B6"];
	cell.watchList = @[[ssScreening spreadSheetCellForIdentifier:@"B7"]];
	[cell setFunction:^NSNumber *(IMSpreadSheet *calculator, IMSpreadSheetCell *cell) {
		return [ssScreening valueForIdentifier:@"B7"];
	}];
	
	cell = [spreadSheet spreadSheetCellForIdentifier:@"C6"];
	cell.watchList = [spreadSheet arrayOfCells:@[@"B20"]];
	[cell setFunction:^NSNumber *(IMSpreadSheet *calculator, IMSpreadSheetCell *cell) {
		return [calculator valueForIdentifier:@"B20"];
	}];
	
	cell = [spreadSheet spreadSheetCellForIdentifier:@"B7"];
	cell.watchList = @[[ssScreening spreadSheetCellForIdentifier:@"B13"], [ssEmployeeTime spreadSheetCellForIdentifier:@"B39"]];
	[cell setFunction:^NSNumber *(IMSpreadSheet *calculator, IMSpreadSheetCell *cell) {
		return [NSNumber numberWithDouble:[[ssScreening valueForIdentifier:@"B13"] doubleValue] * [[ssEmployeeTime valueForIdentifier:@"B39"] doubleValue]];
	}];
	
	cell = [spreadSheet spreadSheetCellForIdentifier:@"C7"];
	cell.value = @0.0;
	
	cell = [spreadSheet spreadSheetCellForIdentifier:@"B10"];
	cell.watchList = @[[spreadSheet spreadSheetCellForIdentifier:@"B3"], [ssEmpCostData spreadSheetCellForIdentifier:@"B8"]];
	[cell setFunction:^NSNumber *(IMSpreadSheet *calculator, IMSpreadSheetCell *cell) {
		double val1 = [[calculator valueForIdentifier:@"B3"] doubleValue];
		double val2 = [[ssEmpCostData valueForIdentifier:@"B8"] doubleValue];
		return [NSNumber numberWithDouble:(val1*val2)/60];
	}];
	
	cell = [spreadSheet spreadSheetCellForIdentifier:@"B11"];
	cell.watchList = @[[spreadSheet spreadSheetCellForIdentifier:@"B4"], [ssEmpCostData spreadSheetCellForIdentifier:@"B8"]];
	[cell setFunction:^NSNumber *(IMSpreadSheet *calculator, IMSpreadSheetCell *cell) {
		double val1 = [[calculator valueForIdentifier:@"B4"] doubleValue];
		double val2 = [[ssEmpCostData valueForIdentifier:@"B8"] doubleValue];
		return [NSNumber numberWithDouble:(val1*val2)/60];
	}];
	
	cell = [spreadSheet spreadSheetCellForIdentifier:@"C11"];
	cell.value = @0.0;
	
	cell = [spreadSheet spreadSheetCellForIdentifier:@"B13"];
	cell.watchList = @[[spreadSheet spreadSheetCellForIdentifier:@"B5"], [ssEmpCostData spreadSheetCellForIdentifier:@"C19"]];
	[cell setFunction:^NSNumber *(IMSpreadSheet *calculator, IMSpreadSheetCell *cell) {
		double val1 = [[spreadSheet valueForIdentifier:@"B5"] doubleValue];
		double val2 = [[ssEmpCostData valueForIdentifier:@"C19"] doubleValue];
		return [NSNumber numberWithDouble:(val1*val2)/60];
	}];
	
	cell = [spreadSheet spreadSheetCellForIdentifier:@"C13"];
	cell.watchList = @[[ssEmployeeTime spreadSheetCellForIdentifier:@"B24"], [ssEmpCostData spreadSheetCellForIdentifier:@"C19"]];
	[cell setFunction:^NSNumber *(IMSpreadSheet *calculator, IMSpreadSheetCell *cell) {
		double val1 = [[ssEmployeeTime valueForIdentifier:@"B24"] doubleValue];
		double val2 = [[ssEmpCostData valueForIdentifier:@"C19"] doubleValue];
		return [NSNumber numberWithDouble:(val1*val2)/60];
	}];
	
	cell = [spreadSheet spreadSheetCellForIdentifier:@"B15"];
	cell.watchList = @[[spreadSheet spreadSheetCellForIdentifier:@"B7"], [ssEmpCostData spreadSheetCellForIdentifier:@"B13"]];
	[cell setFunction:^NSNumber *(IMSpreadSheet *calculator, IMSpreadSheetCell *cell) {
		double val1 = [[spreadSheet valueForIdentifier:@"B7"] doubleValue];
		double val2 = [[ssEmpCostData valueForIdentifier:@"B13"] doubleValue];
		return [NSNumber numberWithDouble:val1*val2];
	}];
	
	cell = [spreadSheet spreadSheetCellForIdentifier:@"C15"];
	cell.value = @0.0;
	
	cell = [spreadSheet spreadSheetCellForIdentifier:@"B18"];
	cell.watchList = [ssScreening arrayOfCells:@[@"B5", @"B16"]];
	[cell setFunction:^NSNumber *(IMSpreadSheet *calculator, IMSpreadSheetCell *cell) {
		return [[calculator.workbook spreadSheetWithName:kIMAnnualTBScreeningProgramSheet] multiplyCells:@[@"B5", @"B16"]];
	}];
	
	cell = [spreadSheet spreadSheetCellForIdentifier:@"B19"];
	cell.watchList = @[[ssScreening spreadSheetCellForIdentifier:@"B5"]
					   ,[ssSpotlight spreadSheetCellForIdentifier:@"B6"]];
	[cell setFunction:^NSNumber *(IMSpreadSheet *calculator, IMSpreadSheetCell *cell) {
		double val1 = [[(IMSpreadSheetCell *)cell.watchList[0] value] doubleValue];
		double val2 = [[(IMSpreadSheetCell *)cell.watchList[1] value] doubleValue];
		return [NSNumber numberWithDouble:val1/val2];
	}];
	
	cell = [spreadSheet spreadSheetCellForIdentifier:@"B20"];
	cell.watchList = @[[spreadSheet spreadSheetCellForIdentifier:@"B19"]
					   ,[ssSpotlight spreadSheetCellForIdentifier:@"B6"]];
	[cell setFunction:^NSNumber *(IMSpreadSheet *calculator, IMSpreadSheetCell *cell) {
		double val1 = [calculator valueForIdentifier:@"B19"].doubleValue;
		double val2 = 0.0;
		if(val1 >= 5000)
		{
			val2 = 8.0;
		}else if(val1 >= 4000){
			val2 = 7.0;
		}else if(val1 >= 3000){
			val2 = 6.0;
		}else if(val1 >= 2500){
			val2 = 5.0;
		}else if(val1 >= 2000){
			val2 = 4.0;
		}else if(val1 >= 1000){
			val2 = 3.0;
		}else if(val1 >= 500){
			val2 = 2.0;
		}else if(val1 >= 250){
			val2 = 1.0;
		}
		
		return [NSNumber numberWithDouble:val2*[ssSpotlight valueForIdentifier:@"B6"].doubleValue];
	}];
	
	cell = [spreadSheet spreadSheetCellForIdentifier:@"B25"];
	cell.watchList = [ssEmployeeTime arrayOfCells:@[@"B9", @"B10", @"B11", @"B14"]];
	[cell setFunction:^NSNumber *(IMSpreadSheet *calculator, IMSpreadSheetCell *cell) {
		return [NSNumber numberWithDouble:[[[calculator.workbook spreadSheetWithName:kIMEmployeeTimeReferenceDataSheet] sumOfCells:@[@"B9", @"B10", @"B11", @"B14"]] doubleValue]*2.0];
	}];
	
	cell = [spreadSheet spreadSheetCellForIdentifier:@"C25"];
	cell.watchList = [ssEmployeeTime arrayOfCells:@[@"B12", @"B9"]];
	[cell setFunction:^NSNumber *(IMSpreadSheet *calculator, IMSpreadSheetCell *cell) {
		return [[calculator.workbook spreadSheetWithName:kIMEmployeeTimeReferenceDataSheet] sumOfCells:@[@"B12", @"B9"]];
	}];
	
	cell = [spreadSheet spreadSheetCellForIdentifier:@"B26"];
	cell.watchList = [ssEmployeeTime arrayOfCells:@[@"B13"]];
	[cell setFunction:^NSNumber *(IMSpreadSheet *calculator, IMSpreadSheetCell *cell) {
		return [(IMSpreadSheetCell *)cell.watchList[0] value];
	}];
	
	cell = [spreadSheet spreadSheetCellForIdentifier:@"C26"];
	cell.value = @0.0;
	
	cell = [spreadSheet spreadSheetCellForIdentifier:@"B27"];
	cell.watchList = [ssEmployeeTime arrayOfCells:@[@"B21", @"B22"]];
	[cell setFunction:^NSNumber *(IMSpreadSheet *calculator, IMSpreadSheetCell *cell) {
		return [calculator sumOfCellsInArray:cell.watchList];
	}];
	
	cell = [spreadSheet spreadSheetCellForIdentifier:@"C27"];
	cell.watchList = [ssEmployeeTime arrayOfCells:@[@"B23"]];
	[cell setFunction:^NSNumber *(IMSpreadSheet *calculator, IMSpreadSheetCell *cell) {
		return [(IMSpreadSheetCell *)cell.watchList[0] value];
	}];
	
	cell = [spreadSheet spreadSheetCellForIdentifier:@"B28"];
	cell.watchList = [ssEmployeeTime arrayOfCells:@[@"B21", @"B22"]];
	[cell setFunction:^NSNumber *(IMSpreadSheet *calculator, IMSpreadSheetCell *cell) {
		return [calculator sumOfCellsInArray:cell.watchList];
	}];
	
	cell = [spreadSheet spreadSheetCellForIdentifier:@"C28"];
	cell.value = @0.0;
	
	cell = [spreadSheet spreadSheetCellForIdentifier:@"B32"];
	cell.watchList = @[[spreadSheet spreadSheetCellForIdentifier:@"B25"], [ssEmpCostData spreadSheetCellForIdentifier:@"B8"]];
	[cell setFunction:^NSNumber *(IMSpreadSheet *calculator, IMSpreadSheetCell *cell) {
		double val1 = [[(IMSpreadSheetCell *)cell.watchList[0] value] doubleValue];
		double val2 = [[(IMSpreadSheetCell *)cell.watchList[1] value] doubleValue];
		return [NSNumber numberWithDouble:(val1*val2)/60];
	}];
	
	cell = [spreadSheet spreadSheetCellForIdentifier:@"C32"];
	cell.watchList = @[[spreadSheet spreadSheetCellForIdentifier:@"C25"], [ssEmpCostData spreadSheetCellForIdentifier:@"B8"]];
	[cell setFunction:^NSNumber *(IMSpreadSheet *calculator, IMSpreadSheetCell *cell) {
		double val1 = [[(IMSpreadSheetCell *)cell.watchList[0] value] doubleValue];
		double val2 = [[(IMSpreadSheetCell *)cell.watchList[1] value] doubleValue];
		return [NSNumber numberWithDouble:(val1*val2)/60];
	}];
	
	cell = [spreadSheet spreadSheetCellForIdentifier:@"B33"];
	cell.watchList = @[[spreadSheet spreadSheetCellForIdentifier:@"B26"], [ssEmpCostData spreadSheetCellForIdentifier:@"B8"]];
	[cell setFunction:^NSNumber *(IMSpreadSheet *calculator, IMSpreadSheetCell *cell) {
		double val1 = [[(IMSpreadSheetCell *)cell.watchList[0] value] doubleValue];
		double val2 = [[(IMSpreadSheetCell *)cell.watchList[1] value] doubleValue];
		return [NSNumber numberWithDouble:(val1*val2)/60];
	}];
	
	cell = [spreadSheet spreadSheetCellForIdentifier:@"C33"];
	cell.value = @0.0;
	
	cell = [spreadSheet spreadSheetCellForIdentifier:@"B34"];
	cell.watchList = @[[spreadSheet spreadSheetCellForIdentifier:@"B27"]
					   ,[ssEmpCostData spreadSheetCellForIdentifier:@"C19"]
					   ,[ssNewHire spreadSheetCellForIdentifier:@"B16"]];
	[cell setFunction:^NSNumber *(IMSpreadSheet *calculator, IMSpreadSheetCell *cell) {
		double val1 = [[(IMSpreadSheetCell *)cell.watchList[0] value] doubleValue];
		double val2 = [[(IMSpreadSheetCell *)cell.watchList[1] value] doubleValue];
		double val3 = [[(IMSpreadSheetCell *)cell.watchList[2] value] doubleValue];
		return [NSNumber numberWithDouble:(val1*val2*val3)/60];
	}];
	
	cell = [spreadSheet spreadSheetCellForIdentifier:@"C34"];
	cell.watchList = @[[spreadSheet spreadSheetCellForIdentifier:@"C27"]
					   ,[ssEmpCostData spreadSheetCellForIdentifier:@"C19"]
					   ,[ssScreening spreadSheetCellForIdentifier:@"B16"]];
	[cell setFunction:^NSNumber *(IMSpreadSheet *calculator, IMSpreadSheetCell *cell) {
		double val1 = [[(IMSpreadSheetCell *)cell.watchList[0] value] doubleValue];
		double val2 = [[(IMSpreadSheetCell *)cell.watchList[1] value] doubleValue];
		double val3 = [[(IMSpreadSheetCell *)cell.watchList[2] value] doubleValue];
		return [NSNumber numberWithDouble:(val1*val2*val3)/60];
	}];
	
	cell = [spreadSheet spreadSheetCellForIdentifier:@"B35"];
	cell.watchList = @[[spreadSheet spreadSheetCellForIdentifier:@"B27"]
					   ,[ssEmpCostData spreadSheetCellForIdentifier:@"C19"]
					   ,[ssNewHire spreadSheetCellForIdentifier:@"B17"]];
	[cell setFunction:^NSNumber *(IMSpreadSheet *calculator, IMSpreadSheetCell *cell) {
		double val1 = [[(IMSpreadSheetCell *)cell.watchList[0] value] doubleValue];
		double val2 = [[(IMSpreadSheetCell *)cell.watchList[1] value] doubleValue];
		double val3 = [[(IMSpreadSheetCell *)cell.watchList[2] value] doubleValue];
		return [NSNumber numberWithDouble:(val1*val2*val3)/60];
	}];
	
	cell = [spreadSheet spreadSheetCellForIdentifier:@"C35"];
	cell.value = @0.0;
	
	cell = [spreadSheet spreadSheetCellForIdentifier:@"B39"];
	cell.watchList = [ssNewHire arrayOfCells:@[@"B4", @"B11"]];
	[cell setFunction:^NSNumber *(IMSpreadSheet *calculator, IMSpreadSheetCell *cell) {
		return [ssNewHire multiplyCells:@[@"B4", @"B11"]];
	}];
	
	cell = [spreadSheet spreadSheetCellForIdentifier:@"B40"];
	cell.watchList = [ssNewHire arrayOfCells:@[@"B4", @"B12"]];
	[cell setFunction:^NSNumber *(IMSpreadSheet *calculator, IMSpreadSheetCell *cell) {
		return [ssNewHire multiplyCells:@[@"B4", @"B12"]];
	}];
	
	cell = [spreadSheet spreadSheetCellForIdentifier:@"B41"];
	cell.watchList = @[[ssScreening spreadSheetCellForIdentifier:@"B5"]
					   ,[ssScreening spreadSheetCellForIdentifier:@"B20"]
					   ,[ssNewHire spreadSheetCellForIdentifier:@"B4"]
					   ,[ssNewHire spreadSheetCellForIdentifier:@"B21"]];
	[cell setFunction:^NSNumber *(IMSpreadSheet *calculator, IMSpreadSheetCell *cell) {
		double val1 = [[(IMSpreadSheetCell *)cell.watchList[0] value] doubleValue];
		double val2 = [[(IMSpreadSheetCell *)cell.watchList[1] value] doubleValue];
		double val3 = [[(IMSpreadSheetCell *)cell.watchList[2] value] doubleValue];
		double val4 = [[(IMSpreadSheetCell *)cell.watchList[3] value] doubleValue];
		return [NSNumber numberWithDouble:(val1 * val2) + (val3 * val4)];
	}];
	
	cell = [spreadSheet spreadSheetCellForIdentifier:@"B42"];
	cell.watchList = @[[ssScreening spreadSheetCellForIdentifier:@"B5"]
					   ,[ssSpotlight spreadSheetCellForIdentifier:@"B9"]
					   ,[ssNewHire spreadSheetCellForIdentifier:@"B4"]
					   ,[ssNewHire spreadSheetCellForIdentifier:@"C21"]];
	[cell setFunction:^NSNumber *(IMSpreadSheet *calculator, IMSpreadSheetCell *cell) {
		double val1 = [[(IMSpreadSheetCell *)cell.watchList[0] value] doubleValue];
		double val2 = [[(IMSpreadSheetCell *)cell.watchList[1] value] doubleValue];
		double val3 = [[(IMSpreadSheetCell *)cell.watchList[2] value] doubleValue];
		double val4 = [[(IMSpreadSheetCell *)cell.watchList[3] value] doubleValue];
		return [NSNumber numberWithDouble:(val1 * val2) + (val3 * val4)];
	}];
	
	cell = [spreadSheet spreadSheetCellForIdentifier:@"B46"];
	cell.watchList = @[[ssEmployeeTime spreadSheetCellForIdentifier:@"B15"]
					   ,[ssEmployeeTime spreadSheetCellForIdentifier:@"B16"]
					   ,[ssEmployeeTime spreadSheetCellForIdentifier:@"B17"]
					   ,[ssScreening spreadSheetCellForIdentifier:@"B5"]
					   ,[ssScreening spreadSheetCellForIdentifier:@"B20"]
					   ,[ssNewHire spreadSheetCellForIdentifier:@"B4"]
					   ,[ssNewHire spreadSheetCellForIdentifier:@"B21"]];
	[cell setFunction:^NSNumber *(IMSpreadSheet *calculator, IMSpreadSheetCell *cell) {
		double val1 = [ssEmployeeTime sumOfCells:@[@"B15", @"B16", @"B17"]].doubleValue;
		double val2 = [ssScreening multiplyCells:@[@"B5", @"B20"]].doubleValue;
		double val3 = [ssNewHire multiplyCells:@[@"B4", @"B21"]].doubleValue;
		return [NSNumber numberWithDouble:(val1/60) * (val2 + val3)];
	}];
	
	cell = [spreadSheet spreadSheetCellForIdentifier:@"C46"];
	cell.watchList = @[[ssEmployeeTime spreadSheetCellForIdentifier:@"B15"]
					   ,[ssEmployeeTime spreadSheetCellForIdentifier:@"B16"]
					   ,[ssEmployeeTime spreadSheetCellForIdentifier:@"B17"]
					   ,[ssScreening spreadSheetCellForIdentifier:@"B5"]
					   ,[ssSpotlight spreadSheetCellForIdentifier:@"B9"]
					   ,[ssNewHire spreadSheetCellForIdentifier:@"B4"]
					   ,[ssScreening spreadSheetCellForIdentifier:@"C21"]];
	[cell setFunction:^NSNumber *(IMSpreadSheet *calculator, IMSpreadSheetCell *cell) {
		double val1 = [ssEmployeeTime sumOfCells:@[@"B15", @"B16", @"B17"]].doubleValue;
		double val2 = [ssScreening valueForIdentifier:@"B5"].doubleValue * [ssSpotlight valueForIdentifier:@"B9"].doubleValue;
		double val3 = [ssNewHire multiplyCells:@[@"B4", @"C21"]].doubleValue;
		return [NSNumber numberWithDouble:(val1/60) * (val2 + val3)];
	}];
	
	cell = [spreadSheet spreadSheetCellForIdentifier:@"B47"];
	cell.watchList = @[[ssEmployeeTime spreadSheetCellForIdentifier:@"B25"]
					   ,[ssEmployeeTime spreadSheetCellForIdentifier:@"B26"]
					   ,[ssScreening spreadSheetCellForIdentifier:@"B5"]
					   ,[ssScreening spreadSheetCellForIdentifier:@"B20"]
					   ,[ssNewHire spreadSheetCellForIdentifier:@"B4"]
					   ,[ssNewHire spreadSheetCellForIdentifier:@"B21"]];
	[cell setFunction:^NSNumber *(IMSpreadSheet *calculator, IMSpreadSheetCell *cell) {
		double val1 = [ssEmployeeTime sumOfCells:@[@"B25", @"B26"]].doubleValue;
		double val2 = [ssScreening multiplyCells:@[@"B5", @"B20"]].doubleValue;
		double val3 = [ssNewHire multiplyCells:@[@"B4", @"B21"]].doubleValue;
		return [NSNumber numberWithDouble:(val1/60) * (val2 + val3)];
	}];
	
	cell = [spreadSheet spreadSheetCellForIdentifier:@"C47"];
	cell.watchList = @[[ssEmployeeTime spreadSheetCellForIdentifier:@"B25"]
					   ,[ssEmployeeTime spreadSheetCellForIdentifier:@"B26"]
					   ,[ssScreening spreadSheetCellForIdentifier:@"B5"]
					   ,[ssSpotlight spreadSheetCellForIdentifier:@"B9"]
					   ,[ssNewHire spreadSheetCellForIdentifier:@"B4"]
					   ,[ssNewHire spreadSheetCellForIdentifier:@"C21"]];
	[cell setFunction:^NSNumber *(IMSpreadSheet *calculator, IMSpreadSheetCell *cell) {
		double val1 = [ssEmployeeTime sumOfCells:@[@"B25", @"B26"]].doubleValue;
		double val2 = [ssScreening valueForIdentifier:@"B5"].doubleValue * [ssSpotlight valueForIdentifier:@"B9"].doubleValue;
		double val3 = [ssNewHire multiplyCells:@[@"B4", @"C21"]].doubleValue;
		return [NSNumber numberWithDouble:(val1/60) * (val2 + val3)];
	}];
	
	cell = [spreadSheet spreadSheetCellForIdentifier:@"B50"];
	cell.watchList = @[[ssScreening spreadSheetCellForIdentifier:@"B21"]
					   ,[ssScreening spreadSheetCellForIdentifier:@"B5"]
					   ,[ssScreening spreadSheetCellForIdentifier:@"B20"]
					   ,[ssNewHire spreadSheetCellForIdentifier:@"B4"]
					   ,[ssNewHire spreadSheetCellForIdentifier:@"B21"]];
	[cell setFunction:^NSNumber *(IMSpreadSheet *calculator, IMSpreadSheetCell *cell) {
		double val1 = [ssScreening valueForIdentifier:@"B21"].doubleValue;
		double val2 = [ssScreening multiplyCells:@[@"B5", @"B20"]].doubleValue;
		double val3 = [ssNewHire multiplyCells:@[@"B4", @"B21"]].doubleValue;
		return [NSNumber numberWithDouble:val1 * (val2 + val3)];
	}];
	
	cell = [spreadSheet spreadSheetCellForIdentifier:@"C50"];
	cell.watchList = @[[ssScreening spreadSheetCellForIdentifier:@"B21"]
					   ,[ssScreening spreadSheetCellForIdentifier:@"B5"]
					   ,[ssSpotlight spreadSheetCellForIdentifier:@"B9"]
					   ,[ssNewHire spreadSheetCellForIdentifier:@"B4"]];
	[cell setFunction:^NSNumber *(IMSpreadSheet *calculator, IMSpreadSheetCell *cell) {
		double val1 = [ssScreening valueForIdentifier:@"B21"].doubleValue;
		double val2 = [ssScreening valueForIdentifier:@"B5"].doubleValue * [ssSpotlight valueForIdentifier:@"B9"].doubleValue;
		double val3 = [ssNewHire valueForIdentifier:@"B4"].doubleValue * [ssSpotlight valueForIdentifier:@"B9"].doubleValue;
		return [NSNumber numberWithDouble:val1 * (val2 + val3)];
	}];
	
	cell = [spreadSheet spreadSheetCellForIdentifier:@"B53"];
	cell.watchList = @[[ssScreening spreadSheetCellForIdentifier:@"B13"]
					   ,[ssEmployeeTime spreadSheetCellForIdentifier:@"B39"]];
	[cell setFunction:^NSNumber *(IMSpreadSheet *calculator, IMSpreadSheetCell *cell) {
		return [NSNumber numberWithDouble:[ssScreening valueForIdentifier:@"B13"].doubleValue * [ssEmployeeTime valueForIdentifier:@"B39"].doubleValue];
	}];
	
	cell = [spreadSheet spreadSheetCellForIdentifier:@"B54"];
	cell.watchList = @[[ssScreening spreadSheetCellForIdentifier:@"B13"]
					   ,[ssEmpCostData spreadSheetCellForIdentifier:@"B13"]
					   ,[ssEmployeeTime spreadSheetCellForIdentifier:@"B39"]];
	[cell setFunction:^NSNumber *(IMSpreadSheet *calculator, IMSpreadSheetCell *cell) {
		return [NSNumber numberWithDouble:[ssScreening valueForIdentifier:@"B13"].doubleValue * [ssEmpCostData valueForIdentifier:@"B13"].doubleValue * [ssEmployeeTime valueForIdentifier:@"B39"].doubleValue];
	}];
}

- (void) initEmployeeTimeReferenceData
{
	IMSpreadSheet *ssEmployeeTime = [self spreadSheetWithName:kIMEmployeeTimeReferenceDataSheet];
	IMSpreadSheetCell *cell;
	NSArray *values = @[@5.0, @7.5, @6.7, @7.4, @10.0, @6.2, @13.7, @5.0, @25.0];
	for(int i=9;i<18;i++)
	{
		cell = [ssEmployeeTime spreadSheetCellForIdentifier:[NSString stringWithFormat:@"B%d", i]];
		cell.value = values[i-9];
	}
	
	values = @[@22.9, @22.3, @23.6, @15.0, @33.2, @45.0];
	for(int i=21;i<27;i++)
	{
		cell = [ssEmployeeTime spreadSheetCellForIdentifier:[NSString stringWithFormat:@"B%d", i]];
		cell.value = values[i-21];
	}
	
	cell = [ssEmployeeTime spreadSheetCellForIdentifier:@"B35"];
	cell.value = @4.0;
	cell = [ssEmployeeTime spreadSheetCellForIdentifier:@"B36"];
	cell.value = @1.0;
	cell = [ssEmployeeTime spreadSheetCellForIdentifier:@"B39"];
	cell.value = @8.0;
}

- (void) initCharts
{
	IMSpreadSheet * ssCharts = [self spreadSheetWithName:kIMChartsAndTotals];
	IMSpreadSheet * ssAnalysis = [self spreadSheetWithName:kIMAnalysisSheet];
	IMSpreadSheet * ssEmployeeTime = [self spreadSheetWithName:kIMEmployeeTimeReferenceDataSheet];
	IMSpreadSheet * ssSpotlight = [self spreadSheetWithName:kIMSPOTLIGHTSheet];
	IMSpreadSheet * ssEmpCostData = [self spreadSheetWithName:kIMEmployeeCostSheet];
	IMSpreadSheet * ssScreening = [self spreadSheetWithName:kIMAnnualTBScreeningProgramSheet];
	IMSpreadSheet * ssNewHire = [self spreadSheetWithName:kIMNewHireScreeningSheet];
	
	IMSpreadSheetCell *cell = [ssCharts spreadSheetCellForIdentifier:@"B2"];
	cell.watchList = @[[ssScreening spreadSheetCellForIdentifier:@"B5"]
					   ,[ssScreening spreadSheetCellForIdentifier:@"B10"]
					   ,[ssNewHire spreadSheetCellForIdentifier:@"B8"]
					   ,[ssNewHire spreadSheetCellForIdentifier:@"B4"]
					   ,[ssAnalysis spreadSheetCellForIdentifier:@"B50"]
					   ,[ssAnalysis spreadSheetCellForIdentifier:@"B54"]];
	[cell setFunction:^NSNumber *(IMSpreadSheet *calculator, IMSpreadSheetCell *cell) {
		double val1 = [ssScreening valueForIdentifier:@"B5"].doubleValue * [ssScreening valueForIdentifier:@"B10"].doubleValue;
		double val2 = [ssNewHire valueForIdentifier:@"B8"].doubleValue * [ssNewHire valueForIdentifier:@"B4"].doubleValue;
		double val3 = [ssAnalysis valueForIdentifier:@"B50"].doubleValue;
		double val4 = [ssAnalysis valueForIdentifier:@"B54"].doubleValue;
		return [NSNumber numberWithDouble:val1+val2+val3+val4];
	}];
	
	cell = [ssCharts spreadSheetCellForIdentifier:@"C2"];
	cell.watchList = @[[ssSpotlight spreadSheetCellForIdentifier:@"B2"]
					   ,[ssScreening spreadSheetCellForIdentifier:@"B5"]
					   ,[ssNewHire spreadSheetCellForIdentifier:@"C8"]
					   ,[ssNewHire spreadSheetCellForIdentifier:@"B4"]
					   ,[ssAnalysis spreadSheetCellForIdentifier:@"C50"]];
	
	[cell setFunction:^NSNumber *(IMSpreadSheet *calculator, IMSpreadSheetCell *cell) {
		double val1 = [ssSpotlight valueForIdentifier:@"B2"].doubleValue * [ssScreening valueForIdentifier:@"B5"].doubleValue;
		double val2 = [ssNewHire valueForIdentifier:@"C8"].doubleValue * [ssNewHire valueForIdentifier:@"B4"].doubleValue;
		double val3 = [ssAnalysis valueForIdentifier:@"C50"].doubleValue;
		return [NSNumber numberWithDouble:val1+val2+val3];
	}];
	
	cell = [ssCharts spreadSheetCellForIdentifier:@"B3"];
	cell.watchList = @[[ssCharts spreadSheetCellForIdentifier:@"B10"]
					   ,[ssEmpCostData spreadSheetCellForIdentifier:@"B8"]];
	[cell setFunction:^NSNumber *(IMSpreadSheet *calculator, IMSpreadSheetCell *cell) {
		return [NSNumber numberWithDouble:[ssCharts valueForIdentifier:@"B10"].doubleValue * [ssEmpCostData valueForIdentifier:@"B8"].doubleValue];
	}];
	
	cell = [ssCharts spreadSheetCellForIdentifier:@"C3"];
	cell.watchList = @[[ssCharts spreadSheetCellForIdentifier:@"C10"]
					   ,[ssEmpCostData spreadSheetCellForIdentifier:@"B8"]];
	[cell setFunction:^NSNumber *(IMSpreadSheet *calculator, IMSpreadSheetCell *cell) {
		return [NSNumber numberWithDouble:[ssCharts valueForIdentifier:@"C10"].doubleValue * [ssEmpCostData valueForIdentifier:@"B8"].doubleValue];
	}];
	
	cell = [ssCharts spreadSheetCellForIdentifier:@"B4"];
	cell.watchList = @[[ssScreening spreadSheetCellForIdentifier:@"B5"]
					   ,[ssAnalysis spreadSheetCellForIdentifier:@"B13"]
					   ,[ssAnalysis spreadSheetCellForIdentifier:@"B34"]
					   ,[ssNewHire spreadSheetCellForIdentifier:@"B4"]
					   ,[ssAnalysis spreadSheetCellForIdentifier:@"B35"]];
	[cell setFunction:^NSNumber *(IMSpreadSheet *calculator, IMSpreadSheetCell *cell) {
		double val1 = [ssScreening valueForIdentifier:@"B5"].doubleValue * [ssAnalysis valueForIdentifier:@"B13"].doubleValue;
		double val2 = [ssAnalysis valueForIdentifier:@"B34"].doubleValue * [ssNewHire valueForIdentifier:@"B4"].doubleValue;
		double val3 = [ssAnalysis valueForIdentifier:@"B35"].doubleValue * [ssNewHire valueForIdentifier:@"B4"].doubleValue;
		return [NSNumber numberWithDouble:val1 + val2 + val3];
	}];
	
//	cell = [ssCharts spreadSheetCellForIdentifier:@"C4"];
//	cell.watchList = @[[ssCharts spreadSheetCellForIdentifier:@"C11"]
//					   ,[ssEmpCostData spreadSheetCellForIdentifier:@"C19"]];
//	[cell setFunction:^NSNumber *(IMSpreadSheet *calculator, IMSpreadSheetCell *cell) {
//		return [NSNumber numberWithDouble:[ssCharts valueForIdentifier:@"C11"].doubleValue * [ssEmpCostData valueForIdentifier:@"C19"].doubleValue];
//	}];
	
	cell = [ssCharts spreadSheetCellForIdentifier:@"C4"];
	cell.watchList = @[[ssAnalysis spreadSheetCellForIdentifier:@"C13"]
					   ,[ssScreening spreadSheetCellForIdentifier:@"B5"]
					   ,[ssAnalysis spreadSheetCellForIdentifier:@"C27"]
					   ,[ssNewHire spreadSheetCellForIdentifier:@"B4"]
					   ,[ssAnalysis spreadSheetCellForIdentifier:@"C28"]];
	[cell setFunction:^NSNumber *(IMSpreadSheet *calculator, IMSpreadSheetCell *cell) {
		double val1 = [ssAnalysis valueForIdentifier:@"C13"].doubleValue * [ssScreening valueForIdentifier:@"B5"].doubleValue;
		double val2 = [ssAnalysis valueForIdentifier:@"C27"].doubleValue * [ssNewHire valueForIdentifier:@"B4"].doubleValue;
		double val3 = [ssAnalysis valueForIdentifier:@"C28"].doubleValue * [ssNewHire valueForIdentifier:@"B4"].doubleValue;
		return [NSNumber numberWithDouble:val1+val2+val3];
	}];
	
	cell = [ssCharts spreadSheetCellForIdentifier:@"B6"];
	cell.watchList = [ssCharts arrayOfCells:@[@"B2", @"B3", @"B4"]];
	[cell setFunction:^NSNumber *(IMSpreadSheet *calculator, IMSpreadSheetCell *cell) {
		return [calculator sumOfCellRange:@"B2:B4"];
	}];
	
	cell = [ssCharts spreadSheetCellForIdentifier:@"C6"];
	cell.watchList = [ssCharts arrayOfCells:@[@"C2", @"C3", @"C4"]];
	[cell setFunction:^NSNumber *(IMSpreadSheet *calculator, IMSpreadSheetCell *cell) {
		return [calculator sumOfCellRange:@"C2:C4"];
	}];
	
	cell = [ssCharts spreadSheetCellForIdentifier:@"D6"];
	cell.watchList = [ssCharts arrayOfCells:@[@"B6", @"C6"]];
	[cell setFunction:^NSNumber *(IMSpreadSheet *calculator, IMSpreadSheetCell *cell) {
		double val1 = [ssCharts valueForIdentifier:@"B6"].doubleValue - [ssCharts valueForIdentifier:@"C6"].doubleValue;
		return [NSNumber numberWithDouble:floor(val1)];
	}];
	
	cell = [ssCharts spreadSheetCellForIdentifier:@"B10"];
	cell.watchList = @[[ssAnalysis spreadSheetCellForIdentifier:@"B3"]
					   ,[ssScreening spreadSheetCellForIdentifier:@"B5"]
					   ,[ssAnalysis spreadSheetCellForIdentifier:@"B4"]
					   ,[ssAnalysis spreadSheetCellForIdentifier:@"B18"]
					   ,[ssAnalysis spreadSheetCellForIdentifier:@"B25"]
					   ,[ssNewHire spreadSheetCellForIdentifier:@"B4"]
					   ,[ssAnalysis spreadSheetCellForIdentifier:@"B26"]
					   ,[ssAnalysis spreadSheetCellForIdentifier:@"B39"]
					   ,[ssAnalysis spreadSheetCellForIdentifier:@"B26"]
					   ,[ssAnalysis spreadSheetCellForIdentifier:@"B40"]
					   ,[ssAnalysis spreadSheetCellForIdentifier:@"B46"]];
	[cell setFunction:^NSNumber *(IMSpreadSheet *calculator, IMSpreadSheetCell *cell) {
		double val1 = [ssAnalysis valueForIdentifier:@"B3"].doubleValue * [ssScreening valueForIdentifier:@"B5"].doubleValue / 60;
		double val2 = [ssAnalysis valueForIdentifier:@"B4"].doubleValue * [ssAnalysis valueForIdentifier:@"B18"].doubleValue / 60;
		double val3 = [ssAnalysis valueForIdentifier:@"B25"].doubleValue * [ssNewHire valueForIdentifier:@"B4"].doubleValue;
		double val4 = [ssAnalysis valueForIdentifier:@"B26"].doubleValue * [ssAnalysis valueForIdentifier:@"B39"].doubleValue;
		double val5 = [ssAnalysis valueForIdentifier:@"B26"].doubleValue * [ssAnalysis valueForIdentifier:@"B40"].doubleValue;
		return [NSNumber numberWithDouble:val1+val2+((val3 + val4 + val5) / 60) + [ssAnalysis valueForIdentifier:@"B46"].doubleValue];
	}];
	
	cell = [ssCharts spreadSheetCellForIdentifier:@"C10"];
	cell.watchList = @[[ssSpotlight spreadSheetCellForIdentifier:@"B4"]
					   ,[ssSpotlight spreadSheetCellForIdentifier:@"B6"]
					   ,[ssAnalysis spreadSheetCellForIdentifier:@"C25"]
					   ,[ssNewHire spreadSheetCellForIdentifier:@"B4"]
					   ,[ssAnalysis spreadSheetCellForIdentifier:@"C46"]];
	[cell setFunction:^NSNumber *(IMSpreadSheet *calculator, IMSpreadSheetCell *cell) {
		double val1 = [ssSpotlight multiplyCells:@[@"B4", @"B6"]].doubleValue;
		double val2 = [ssAnalysis valueForIdentifier:@"C25"].doubleValue * [ssNewHire valueForIdentifier:@"B4"].doubleValue / 60.0;
		double val3 = [ssAnalysis valueForIdentifier:@"C46"].doubleValue;
		return [NSNumber numberWithDouble:val1 + val2 + val3];
	}];
	
	cell = [ssCharts spreadSheetCellForIdentifier:@"B11"];
	cell.watchList = @[[ssAnalysis spreadSheetCellForIdentifier:@"B5"]
					   ,[ssScreening spreadSheetCellForIdentifier:@"B5"]
					   ,[ssAnalysis spreadSheetCellForIdentifier:@"B27"]
					   ,[ssAnalysis spreadSheetCellForIdentifier:@"B28"]
					   ,[ssNewHire spreadSheetCellForIdentifier:@"B4"]
					   ,[ssAnalysis spreadSheetCellForIdentifier:@"B47"]];
	[cell setFunction:^NSNumber *(IMSpreadSheet *calculator, IMSpreadSheetCell *cell) {
		double val1 = ([ssAnalysis valueForIdentifier:@"B5"].doubleValue / 60.0) * [ssScreening valueForIdentifier:@"B5"].doubleValue;
		double val2 = [ssAnalysis sumOfCells:@[@"B27", @"B28"]].doubleValue * [ssNewHire valueForIdentifier:@"B4"].doubleValue / 60.0;
		double val3 = [ssAnalysis valueForIdentifier:@"B47"].doubleValue;
		return [NSNumber numberWithDouble:val1 + val2 + val3];
	}];
	
	cell = [ssCharts spreadSheetCellForIdentifier:@"C11"];
	cell.watchList = @[[ssAnalysis spreadSheetCellForIdentifier:@"C5"]
					   ,[ssScreening spreadSheetCellForIdentifier:@"B5"]
					   ,[ssAnalysis spreadSheetCellForIdentifier:@"C27"]
					   ,[ssNewHire spreadSheetCellForIdentifier:@"B4"]
					   ,[ssAnalysis spreadSheetCellForIdentifier:@"C47"]];
	[cell setFunction:^NSNumber *(IMSpreadSheet *calculator, IMSpreadSheetCell *cell) {
		double val1 = ([ssAnalysis valueForIdentifier:@"C5"].doubleValue / 60.0) * [ssScreening valueForIdentifier:@"B5"].doubleValue;
		double val2 = [ssAnalysis valueForIdentifier:@"C27"].doubleValue * [ssNewHire valueForIdentifier:@"B4"].doubleValue / 60.0;
		double val3 = [ssAnalysis valueForIdentifier:@"C47"].doubleValue;
		return [NSNumber numberWithDouble:val1 + val2 + val3];
	}];
	
	cell = [ssCharts spreadSheetCellForIdentifier:@"B12"];
	cell.watchList = [ssCharts arrayOfCells:@[@"B10", @"B11"]];
	[cell setFunction:^NSNumber *(IMSpreadSheet *calculator, IMSpreadSheetCell *cell) {
		return [ssCharts sumOfCells:@[@"B10", @"B11"]];
	}];
	
	cell = [ssCharts spreadSheetCellForIdentifier:@"C12"];
	cell.watchList = [ssCharts arrayOfCells:@[@"C10", @"C11"]];
	[cell setFunction:^NSNumber *(IMSpreadSheet *calculator, IMSpreadSheetCell *cell) {
		return [ssCharts sumOfCells:@[@"C10", @"C11"]];
	}];
	
	cell = [ssCharts spreadSheetCellForIdentifier:@"D12"];
	cell.watchList = [ssCharts arrayOfCells:@[@"B12", @"C12"]];
	[cell setFunction:^NSNumber *(IMSpreadSheet *calculator, IMSpreadSheetCell *cell) {
		double val1 = round([ssCharts valueForIdentifier:@"B12"].doubleValue) - round([ssCharts valueForIdentifier:@"C12"].doubleValue);
		return [NSNumber numberWithDouble:floor(val1)];
	}];
	
	cell = [ssCharts spreadSheetCellForIdentifier:@"B16"];
	cell.watchList = @[[ssAnalysis spreadSheetCellForIdentifier:@"B6"]];
	[cell setFunction:^NSNumber *(IMSpreadSheet *calculator, IMSpreadSheetCell *cell) {
		return [ssAnalysis valueForIdentifier:@"B6"];
	}];
	
	cell = [ssCharts spreadSheetCellForIdentifier:@"C16"];
	cell.watchList = @[[ssAnalysis spreadSheetCellForIdentifier:@"C6"]];
	[cell setFunction:^NSNumber *(IMSpreadSheet *calculator, IMSpreadSheetCell *cell) {
		return [ssAnalysis valueForIdentifier:@"C6"];
	}];
	
	cell = [ssCharts spreadSheetCellForIdentifier:@"D16"];
	cell.watchList = [ssCharts arrayOfCells:@[@"B16", @"C16"]];
	[cell setFunction:^NSNumber *(IMSpreadSheet *calculator, IMSpreadSheetCell *cell) {
		double val1 = [ssCharts valueForIdentifier:@"B16"].doubleValue - [ssCharts valueForIdentifier:@"C16"].doubleValue;
		return [NSNumber numberWithDouble:floor(val1)];
	}];
	
	cell = [ssCharts spreadSheetCellForIdentifier:@"B20"];
	cell.watchList = @[[ssNewHire spreadSheetCellForIdentifier:@"B4"]
					   ,[ssEmployeeTime spreadSheetCellForIdentifier:@"B35"]];
	[cell setFunction:^NSNumber *(IMSpreadSheet *calculator, IMSpreadSheetCell *cell) {
		double val1 = [ssNewHire valueForIdentifier:@"B4"].doubleValue * [ssEmployeeTime valueForIdentifier:@"B35"].doubleValue;
		return [NSNumber numberWithDouble:val1];
	}];
	
	cell = [ssCharts spreadSheetCellForIdentifier:@"C20"];
	cell.watchList = @[[ssNewHire spreadSheetCellForIdentifier:@"B4"]
					   ,[ssEmployeeTime spreadSheetCellForIdentifier:@"B36"]];
	[cell setFunction:^NSNumber *(IMSpreadSheet *calculator, IMSpreadSheetCell *cell) {
		double val1 = [ssNewHire valueForIdentifier:@"B4"].doubleValue * [ssEmployeeTime valueForIdentifier:@"B36"].doubleValue;
		return [NSNumber numberWithDouble:val1];
	}];
	
	cell = [ssCharts spreadSheetCellForIdentifier:@"D20"];
	cell.watchList = [ssCharts arrayOfCells:@[@"B20", @"C20"]];
	[cell setFunction:^NSNumber *(IMSpreadSheet *calculator, IMSpreadSheetCell *cell) {
		double val1 = [ssCharts valueForIdentifier:@"B20"].doubleValue - [ssCharts valueForIdentifier:@"C20"].doubleValue;
		return [NSNumber numberWithDouble:floor(val1)];
	}];
	
	cell = [ssCharts spreadSheetCellForIdentifier:@"E6"];
	cell.watchList = [ssCharts arrayOfCells:@[@"C6", @"B6"]];
	[cell setFunction:^NSNumber *(IMSpreadSheet *calculator, IMSpreadSheetCell *cell) {
		double val1 = ([calculator valueForIdentifier:@"C6"].doubleValue / [calculator valueForIdentifier:@"B6"].doubleValue) - 1.0;
		return [NSNumber numberWithDouble:val1];
	}];
	
	cell = [ssCharts spreadSheetCellForIdentifier:@"E12"];
	cell.watchList = [ssCharts arrayOfCells:@[@"C12", @"B12"]];
	[cell setFunction:^NSNumber *(IMSpreadSheet *calculator, IMSpreadSheetCell *cell) {
		double val1 = ([calculator valueForIdentifier:@"C12"].doubleValue / [calculator valueForIdentifier:@"B12"].doubleValue) - 1.0;
		return [NSNumber numberWithDouble:val1];
	}];
	
	cell = [ssCharts spreadSheetCellForIdentifier:@"E16"];
	cell.watchList = [ssCharts arrayOfCells:@[@"C16", @"B16"]];
	[cell setFunction:^NSNumber *(IMSpreadSheet *calculator, IMSpreadSheetCell *cell) {
		double val1 = ([calculator valueForIdentifier:@"C16"].doubleValue / [calculator valueForIdentifier:@"B16"].doubleValue) - 1.0;
		return [NSNumber numberWithDouble:val1];
	}];
	
	cell = [ssCharts spreadSheetCellForIdentifier:@"E20"];
	cell.watchList = [ssCharts arrayOfCells:@[@"C20", @"B20"]];
	[cell setFunction:^NSNumber *(IMSpreadSheet *calculator, IMSpreadSheetCell *cell) {
		double val1 = ([calculator valueForIdentifier:@"C20"].doubleValue / [calculator valueForIdentifier:@"B20"].doubleValue) - 1.0;
		return [NSNumber numberWithDouble:val1];
	}];
	
	
}

@end
