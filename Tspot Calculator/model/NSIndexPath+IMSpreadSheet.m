//
//  NSIndexPath+IMSpreadSheet.m
//  IMSpreadSheet
//
//  Created by Anthony Long on 4/18/13.
//  Copyright (c) 2013 Infuse Medical. All rights reserved.
//

#import "NSIndexPath+IMSpreadSheet.h"

@implementation NSIndexPath (IMSpreadSheet)

+ (NSIndexPath *) indexPathForRow:(NSInteger)row column:(NSString *)column
{
	column = [column uppercaseString];
	if(column.length > 0)
	{
		int value = [column characterAtIndex:0];
		for(int i=1;i<column.length;i++)
		{
			value += [column characterAtIndex:i] + 26;
		}
		
		NSUInteger indexes[] = {row, value};
		
		return [NSIndexPath indexPathWithIndexes:indexes length:2];
	}
	else
	{
		return nil;
	}
}

- (NSInteger)spreadsheetRow
{
	return [self indexAtPosition:0];
}

- (NSString *)spreadsheetColumn
{
	NSUInteger c = [self indexAtPosition:1];
	return [NSString stringWithFormat:@"%c", c];
}

@end