//
//  NSNumber+IMSpreadSheet.m
//  IMSpreadSheet
//
//  Created by Anthony Long on 4/18/13.
//  Copyright (c) 2013 Infuse Medical. All rights reserved.
//

#import "NSNumber+IMSpreadSheet.h"

@implementation NSNumber (IMSpreadSheet)

+ (NSNumber *) numberWithCurrencyString:(NSString *)currencyString
{
	return [[self currencyFormatter] numberFromString:currencyString];
}

+ (NSNumber *) numberWithPercentString:(NSString *)percentString
{
	return [[self percentFormatter] numberFromString:percentString];
}

+ (NSNumber *) numberWithNumericString:(NSString *) numberString
{
	return [[self numericFormatter] numberFromString:numberString];
}

#pragma mark - getting formatted values

- (NSString *) percentStringValue
{
	if(isnan(self.floatValue) || isinf(self.floatValue))
	{
		return @"0%";
	}
	return [[self percentFormatter] stringFromNumber:self];
}

- (NSString *) numericValue
{
	if(isnan(self.floatValue) || isinf(self.floatValue) || self.floatValue == 0)
	{
		return @"";
	}
	return [[self numericFormatter] stringFromNumber:self];
}

- (NSString *) currencyValue
{
	if(isnan(self.floatValue) || isinf(self.floatValue) || self.floatValue == 0)
	{
		return @"";
	}
//	return [NSString stringWithFormat:@"$%@", [[self currencyFormatter] stringFromNumber:self]];
	return [[self currencyFormatter] stringFromNumber:self];
}

- (NSString *) intCurrencyValue
{
	if(isnan(self.floatValue) || isinf(self.floatValue) || self.floatValue == 0)
	{
		return @"";
	}
//	return [NSString stringWithFormat:@"$%@", [[self intCurrencyFormatter] stringFromNumber:self]];
	return [[self intCurrencyFormatter] stringFromNumber:self];
}

- (NSString *) intStringValue
{
	if(isnan(self.floatValue) || isinf(self.floatValue) || self.floatValue == 0)
	{
		return @"";
	}
//	return [NSString stringWithFormat:@"%d", self.intValue];
	NSUInteger max = [self numericFormatter].maximumFractionDigits;
	[self numericFormatter].maximumFractionDigits = 0;
	NSString *s = [[self numericFormatter] stringFromNumber:self];
	[self numericFormatter].maximumFractionDigits = max;
	return s;
}

#pragma mark - NSNumberFormatters

static NSNumberFormatter * _percentFormatter = nil;
static NSNumberFormatter * _currencyFormatter = nil;
static NSNumberFormatter * _numericFormatter = nil;
static NSNumberFormatter * _intCurrencyFormatter = nil;

- (NSNumberFormatter *) percentFormatter
{
	return [self.class percentFormatter];
}

+ (NSNumberFormatter *) percentFormatter
{
	if(!_percentFormatter)
	{
		_percentFormatter = [[NSNumberFormatter alloc] init];
		_percentFormatter.numberStyle = NSNumberFormatterPercentStyle;
		_percentFormatter.maximumFractionDigits = 1;
		_percentFormatter.minimumFractionDigits = 0;
	}
	return _percentFormatter;
}

+ (NSNumberFormatter *) currencyFormatter
{
	if(!_currencyFormatter)
	{
		_currencyFormatter = [[NSNumberFormatter alloc] init];
		_currencyFormatter.minimumFractionDigits = 2;
		_currencyFormatter.maximumFractionDigits = 2;
		_currencyFormatter.minimumIntegerDigits = 1;
#ifdef OXFORD_ISUK
		_currencyFormatter.currencyCode = @"GBP";
#else
		_currencyFormatter.currencyCode = @"USD";
#endif
		NSString * groupingSeparator = [[NSLocale currentLocale] objectForKey:NSLocaleGroupingSeparator];
		[_currencyFormatter setGroupingSeparator:groupingSeparator];
		[_currencyFormatter setGroupingSize:3];
		[_currencyFormatter setAlwaysShowsDecimalSeparator:YES];
		[_currencyFormatter setUsesGroupingSeparator:YES];
		_currencyFormatter.numberStyle = NSNumberFormatterCurrencyStyle;
	}
	return _currencyFormatter;
}

+ (NSNumberFormatter *) intCurrencyFormatter
{
	if(!_intCurrencyFormatter)
	{
		_intCurrencyFormatter = [[NSNumberFormatter alloc] init];
		_intCurrencyFormatter.minimumFractionDigits = 0;
		_intCurrencyFormatter.maximumFractionDigits = 0;
		_intCurrencyFormatter.minimumIntegerDigits = 1;
#ifdef OXFORD_ISUK
		_intCurrencyFormatter.currencyCode = @"GBP";
#else
		_intCurrencyFormatter.currencyCode = @"USD";
#endif
		NSString * groupingSeparator = [[NSLocale currentLocale] objectForKey:NSLocaleGroupingSeparator];
		[_intCurrencyFormatter setGroupingSeparator:groupingSeparator];
		[_intCurrencyFormatter setGroupingSize:3];
		[_intCurrencyFormatter setAlwaysShowsDecimalSeparator:NO];
		[_intCurrencyFormatter setUsesGroupingSeparator:YES];
		_intCurrencyFormatter.numberStyle = NSNumberFormatterCurrencyStyle;
	}
	return _intCurrencyFormatter;
}

- (NSNumberFormatter *) currencyFormatter
{
	return [self.class currencyFormatter];
}

- (NSNumberFormatter *) intCurrencyFormatter
{
	return [self.class intCurrencyFormatter];
}

+ (NSNumberFormatter *) numericFormatter
{
	if(!_numericFormatter)
	{
		_numericFormatter = [[NSNumberFormatter alloc] init];
		_numericFormatter.numberStyle = NSNumberFormatterDecimalStyle;
		_numericFormatter.maximumFractionDigits = 1;
		_numericFormatter.minimumFractionDigits = 0;
		NSString * groupingSeparator = [[NSLocale currentLocale] objectForKey:NSLocaleGroupingSeparator];
		[_currencyFormatter setGroupingSeparator:groupingSeparator];
		[_currencyFormatter setGroupingSize:3];
		[_currencyFormatter setUsesGroupingSeparator:YES];
	}
	
	return _numericFormatter;
}

- (NSNumberFormatter *) numericFormatter
{
	return [self.class numericFormatter];
}

@end
