//
//  IMTSPOTVsDual.h
//  Tspot Calculator
//
//  Created by Anthony Long on 4/6/15.
//  Copyright (c) 2015 Infuse Medical. All rights reserved.
//

#import "IMSpreadSheet.h"

@interface IMTSPOTVsDual : IMWorkbook

extern NSString *const kIMUKTSPOTVsDualCurrentPractice;
extern NSString *const kIMUKTSPOTVsDualTSPOT;
extern NSString *const kIMUKTSPOTChartsAndTotals;
extern NSString *const kIMUKTSPOTSensSpec;

@end
