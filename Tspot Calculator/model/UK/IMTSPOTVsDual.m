//
//  IMTSPOTVsDual.m
//  Tspot Calculator
//
//  Created by Anthony Long on 4/6/15.
//  Copyright (c) 2015 Infuse Medical. All rights reserved.
//

#import "IMTSPOTVsDual.h"

@implementation IMTSPOTVsDual

NSString *const kIMUKTSPOTVsDualCurrentPractice = @"CurrentPractice";
NSString *const kIMUKTSPOTVsDualTSPOT = @"TSPOT";
NSString *const kIMUKTSPOTChartsAndTotals = @"ChartsAndTotals";
NSString *const kIMUKTSPOTVsDualSensSpec = @"SensSpec";


- (void)initializeWorkbook
{
	[super initializeWorkbook];
	
	[self setSpreadSheet:[[IMSpreadSheet alloc] init]
				WithName:kIMUKTSPOTVsDualCurrentPractice];
	[self setSpreadSheet:[[IMSpreadSheet alloc] init]
				WithName:kIMUKTSPOTVsDualTSPOT];
	[self setSpreadSheet:[[IMSpreadSheet alloc] init]
				WithName:kIMUKTSPOTChartsAndTotals];
	[self setSpreadSheet:[[IMSpreadSheet alloc] init]
				WithName:kIMUKTSPOTVsDualSensSpec];
	
	[self initializeCurrentPractice];
	[self initializeTSPOT];
	[self initializeCharts];
	[self initializeSensSpec];
}

- (void) setCustomerName:(NSString *)customerName
{
    [super setCustomerName:customerName];
    [[self spreadSheetWithName:kIMUKTSPOTVsDualCurrentPractice] spreadSheetCellForIdentifier:@"B3"].textValue = customerName;
}

- (NSString *) customerName
{
    return [[self spreadSheetWithName:kIMUKTSPOTVsDualCurrentPractice] spreadSheetCellForIdentifier:@"B3"].textValue;
}

- (void) initializeCurrentPractice
{
	IMSpreadSheet *ss = [self spreadSheetWithName:kIMUKTSPOTVsDualCurrentPractice];
	
	[ss spreadSheetCellForIdentifier:@"B2"];
	[ss spreadSheetCellForIdentifier:@"B5"].value = @1.22;
	[ss spreadSheetCellForIdentifier:@"B6"].value = @7.0;
	[ss spreadSheetCellForIdentifier:@"B7"].value = @7.0;
	[ss spreadSheetCellForIdentifier:@"B8"].value = @0.1;
	[ss spreadSheetCellForIdentifier:@"B10"].maxValue = @1.0;
	[ss spreadSheetCellForIdentifier:@"B11"].value = @0.8;
	[ss spreadSheetCellForIdentifier:@"B11"].maxValue = @1.0;
	[ss spreadSheetCellForIdentifier:@"B14"].value = @0.37;
	[ss spreadSheetCellForIdentifier:@"B14"].maxValue = @1.0;
	[ss spreadSheetCellForIdentifier:@"B15"].maxValue = @1.0;
	[ss spreadSheetCellForIdentifier:@"B16"].value = @16.0;
	[ss spreadSheetCellForIdentifier:@"B17"].value = @398.0;
	[ss spreadSheetCellForIdentifier:@"B18"].value = @0.1;
	[ss spreadSheetCellForIdentifier:@"B18"].maxValue = @1.0;
	[ss spreadSheetCellForIdentifier:@"B19"].value= @6389.0;
}

- (void) initializeTSPOT
{
	IMSpreadSheet *ss = [self spreadSheetWithName:kIMUKTSPOTVsDualTSPOT];
	
	[ss spreadSheetCellForIdentifier:@"B2"];
	[ss spreadSheetCellForIdentifier:@"B3"].value = @1.5;
	[ss spreadSheetCellForIdentifier:@"B4"].value = @0.956;
	[ss spreadSheetCellForIdentifier:@"B4"].maxValue = @1.0;
	[ss spreadSheetCellForIdentifier:@"B5"].value = @0.989;
	[ss spreadSheetCellForIdentifier:@"B5"].maxValue = @1.0;
}

- (void) initializeCharts
{
	IMSpreadSheet *ssCharts = [self spreadSheetWithName:kIMUKTSPOTChartsAndTotals];
	IMSpreadSheet *ssSens = [self spreadSheetWithName:kIMUKTSPOTVsDualSensSpec];
	IMSpreadSheet *ssCurrent = [self spreadSheetWithName:kIMUKTSPOTVsDualCurrentPractice];
	IMSpreadSheet *ssTSPOT = [self spreadSheetWithName:kIMUKTSPOTVsDualTSPOT];
	
	IMSpreadSheetCell *cell;
	
	[[ssCharts spreadSheetCellForIdentifier:@"B2"] mapToCell:[ssCurrent spreadSheetCellForIdentifier:@"B2"]];
	[[ssCharts spreadSheetCellForIdentifier:@"C2"] mapToCell:[ssCharts spreadSheetCellForIdentifier:@"B2"]];
	cell = [ssCharts spreadSheetCellForIdentifier:@"B3"];
	cell.watchList = [ssCurrent arrayOfCells:@[@"B2", @"B14"]];
	[cell setFunction:^NSNumber *(IMSpreadSheet *calculator, IMSpreadSheetCell *cell) {
		return [ssCurrent multiplyCells:@[@"B2", @"B14"]];
	}];
	[[ssCharts spreadSheetCellForIdentifier:@"C3"] mapToCell:[ssCharts spreadSheetCellForIdentifier:@"B3"]];
	cell = [ssCharts spreadSheetCellForIdentifier:@"E3"];
	cell.watchList = [ssCharts arrayOfCells:@[@"B3", @"C3"]];
	[cell setFunction:^NSNumber *(IMSpreadSheet *calculator, IMSpreadSheetCell *cell) {
		return [ssCharts subtractCell:@"C3" fromCell:@"B3"];
	}];
	[[ssCharts spreadSheetCellForIdentifier:@"B4"] mapToCell:[ssSens spreadSheetCellForIdentifier:@"B5"]];
	[[ssCharts spreadSheetCellForIdentifier:@"C4"] mapToCell:[ssSens spreadSheetCellForIdentifier:@"D15"]];
	cell = [ssCharts spreadSheetCellForIdentifier:@"E4"];
	cell.watchList = [ssCharts arrayOfCells:@[@"B4", @"C4"]];
	[cell setFunction:^NSNumber *(IMSpreadSheet *calculator, IMSpreadSheetCell *cell) {
		return [ssCharts subtractCell:@"C4" fromCell:@"B4"];
	}];
	[[ssCharts spreadSheetCellForIdentifier:@"B5"] mapToCell:[ssSens spreadSheetCellForIdentifier:@"E7"]];
	[[ssCharts spreadSheetCellForIdentifier:@"C5"] mapToCell:[ssSens spreadSheetCellForIdentifier:@"E17"]];
	cell = [ssCharts spreadSheetCellForIdentifier:@"E5"];
	cell.watchList = [ssCharts arrayOfCells:@[@"B5", @"C5"]];
	[cell setFunction:^NSNumber *(IMSpreadSheet *calculator, IMSpreadSheetCell *cell) {
		return [ssCharts subtractCell:@"C5" fromCell:@"B5"];
	}];
	[[ssCharts spreadSheetCellForIdentifier:@"B6"] mapToCell:[ssSens spreadSheetCellForIdentifier:@"D26"]];
	[ssCharts spreadSheetCellForIdentifier:@"C6"].value = @0.0;
	cell = [ssCharts spreadSheetCellForIdentifier:@"E6"];
	cell.watchList = [ssCharts arrayOfCells:@[@"B6", @"C6"]];
	[cell setFunction:^NSNumber *(IMSpreadSheet *calculator, IMSpreadSheetCell *cell) {
		return [ssCharts subtractCell:@"C6" fromCell:@"B6"];
	}];
	[[ssCharts spreadSheetCellForIdentifier:@"B7"] mapToCell:[ssSens spreadSheetCellForIdentifier:@"B28"]];
	[ssCharts spreadSheetCellForIdentifier:@"C7"].value = @0.0;
	cell = [ssCharts spreadSheetCellForIdentifier:@"E7"];
	cell.watchList = [ssCharts arrayOfCells:@[@"B7", @"C7"]];
	[cell setFunction:^NSNumber *(IMSpreadSheet *calculator, IMSpreadSheetCell *cell) {
		return [ssCharts subtractCell:@"C7" fromCell:@"B7"];
	}];
	cell = [ssCharts spreadSheetCellForIdentifier:@"B8"];
	cell.watchList = @[[ssCharts spreadSheetCellForIdentifier:@"B7"]
					   ,[ssCurrent spreadSheetCellForIdentifier:@"B15"]];
	[cell setFunction:^NSNumber *(IMSpreadSheet *calculator, IMSpreadSheetCell *cell) {
		double val1 = [ssCharts valueForIdentifier:@"B7"].doubleValue * [ssCurrent valueForIdentifier:@"B15"].doubleValue;
		return [NSNumber numberWithDouble:val1];
	}];
	cell = [ssCharts spreadSheetCellForIdentifier:@"C8"];
	cell.watchList = @[[ssCharts spreadSheetCellForIdentifier:@"C4"]
					   ,[ssCurrent spreadSheetCellForIdentifier:@"B15"]];
	[cell setFunction:^NSNumber *(IMSpreadSheet *calculator, IMSpreadSheetCell *cell) {
		double val1 = [ssCharts valueForIdentifier:@"C4"].doubleValue * [ssCurrent valueForIdentifier:@"B15"].doubleValue;
		return [NSNumber numberWithDouble:val1];
	}];
	cell =[ssCharts spreadSheetCellForIdentifier:@"E8"];
	cell.watchList = [ssCharts arrayOfCells:@[@"B8", @"C8"]];
	[cell setFunction:^NSNumber *(IMSpreadSheet *calculator, IMSpreadSheetCell *cell) {
		return [ssCharts subtractCell:@"C8" fromCell:@"C8"];
	}];
	cell = [ssCharts spreadSheetCellForIdentifier:@"B9"];
	cell.watchList = @[[ssCharts spreadSheetCellForIdentifier:@"B5"]
					   ,[ssCurrent spreadSheetCellForIdentifier:@"B18"]
					   ,[ssCharts spreadSheetCellForIdentifier:@"B6"]
					   ,[ssCharts spreadSheetCellForIdentifier:@"B7"]
					   ,[ssCharts spreadSheetCellForIdentifier:@"B3"]];
	[cell setFunction:^NSNumber *(IMSpreadSheet *calculator, IMSpreadSheetCell *cell) {
		double val1 = [ssCharts valueForIdentifier:@"B5"].doubleValue * [ssCurrent valueForIdentifier:@"B18"].doubleValue;
		double val2 = [ssCharts subtractCell:@"B7" fromCell:@"B6"].doubleValue * [ssCurrent valueForIdentifier:@"B18"].doubleValue;
		double val3 = [ssCharts subtractCell:@"B7" fromCell:@"B3"].doubleValue * [ssCurrent valueForIdentifier:@"B18"].doubleValue;
		return [NSNumber numberWithDouble:val1 + val2 + val3];
	}];
	cell = [ssCharts spreadSheetCellForIdentifier:@"C9"];
	cell.watchList = @[[ssCharts spreadSheetCellForIdentifier:@"C5"]
					   ,[ssCurrent spreadSheetCellForIdentifier:@"B18"]
					   ,[ssCharts spreadSheetCellForIdentifier:@"C3"]
					   ,[ssCharts spreadSheetCellForIdentifier:@"C7"]];
	[cell setFunction:^NSNumber *(IMSpreadSheet *calculator, IMSpreadSheetCell *cell) {
		double val1 = [ssCharts valueForIdentifier:@"C5"].doubleValue * [ssCurrent valueForIdentifier:@"B18"].doubleValue;
//		double val2 = [ssCharts subtractCell:@"C7" fromCell:@"C3"].doubleValue * [ssCurrent valueForIdentifier:@"B18"].doubleValue;
		return [NSNumber numberWithDouble:val1];
	}];
	cell = [ssCharts spreadSheetCellForIdentifier:@"E9"];
	cell.watchList = [ssCharts arrayOfCells:@[@"B9", @"C9"]];
	[cell setFunction:^NSNumber *(IMSpreadSheet *calculator, IMSpreadSheetCell *cell) {
		return [ssCharts subtractCell:@"C9" fromCell:@"B9"];
	}];
	cell = [ssCharts spreadSheetCellForIdentifier:@"B12"];
	cell.watchList = [ssCurrent arrayOfCells:@[@"B2", @"B5", @"B6", @"B7", @"B8"]];
	[cell setFunction:^NSNumber *(IMSpreadSheet *calculator, IMSpreadSheetCell *cell) {
		double val1 = [ssCurrent valueForIdentifier:@"B2"].doubleValue * [ssCurrent sumOfCells:@[@"B5", @"B6", @"B7"]].doubleValue * (1.0 - [ssCurrent valueForIdentifier:@"B8"].doubleValue);
		return [NSNumber numberWithDouble:val1];
	}];
	cell = [ssCharts spreadSheetCellForIdentifier:@"C12"];
	cell.watchList = @[[ssCurrent spreadSheetCellForIdentifier:@"B2"]
					   ,[ssTSPOT spreadSheetCellForIdentifier:@"B2"]
					   ,[ssTSPOT spreadSheetCellForIdentifier:@"B3"]];
	[cell setFunction:^NSNumber *(IMSpreadSheet *calculator, IMSpreadSheetCell *cell) {
		double val1 = [ssCurrent valueForIdentifier:@"B2"].doubleValue * [ssTSPOT sumOfCells:@[@"B2", @"B3"]].doubleValue;
		return [NSNumber numberWithDouble:val1];
	}];
	cell = [ssCharts spreadSheetCellForIdentifier:@"E12"];
	cell.watchList = [ssCharts arrayOfCells:@[@"B12", @"C12"]];
	[cell setFunction:^NSNumber *(IMSpreadSheet *calculator, IMSpreadSheetCell *cell) {
		return [ssCharts subtractCell:@"C12" fromCell:@"B12"];
	}];
	cell = [ssCharts spreadSheetCellForIdentifier:@"B13"];
	cell.watchList = @[[ssCharts spreadSheetCellForIdentifier:@"B4"]
					   ,[ssTSPOT spreadSheetCellForIdentifier:@"B2"]
					   ,[ssTSPOT spreadSheetCellForIdentifier:@"B3"]];
	[cell setFunction:^NSNumber *(IMSpreadSheet *calculator, IMSpreadSheetCell *cell) {
		double val1 = [ssCharts valueForIdentifier:@"B4"].doubleValue * [ssTSPOT sumOfCells:@[@"B2", @"B3"]].doubleValue;
		return [NSNumber numberWithDouble:val1];
	}];
	[ssCharts spreadSheetCellForIdentifier:@"C13"].value = @0.0;
	cell = [ssCharts spreadSheetCellForIdentifier:@"B14"];
	cell.watchList = @[[ssCharts spreadSheetCellForIdentifier:@"B8"]
					   ,[ssCurrent spreadSheetCellForIdentifier:@"B17"]];
	[cell setFunction:^NSNumber *(IMSpreadSheet *calculator, IMSpreadSheetCell *cell) {
		double val1 = [ssCharts valueForIdentifier:@"B8"].doubleValue * [ssCurrent valueForIdentifier:@"B17"].doubleValue;
		return [NSNumber numberWithDouble:val1];
	}];
	cell = [ssCharts spreadSheetCellForIdentifier:@"C14"];
	cell.watchList = @[[ssCharts spreadSheetCellForIdentifier:@"C8"]
					   ,[ssCurrent spreadSheetCellForIdentifier:@"B17"]];
	[cell setFunction:^NSNumber *(IMSpreadSheet *calculator, IMSpreadSheetCell *cell) {
		double val1 = [ssCharts valueForIdentifier:@"C8"].doubleValue * [ssCurrent valueForIdentifier:@"B17"].doubleValue;
		return [NSNumber numberWithDouble:val1];
	}];
	cell = [ssCharts spreadSheetCellForIdentifier:@"E14"];
	cell.watchList = [ssCharts arrayOfCells:@[@"B14", @"C14"]];
	[cell setFunction:^NSNumber *(IMSpreadSheet *calculator, IMSpreadSheetCell *cell) {
		return [ssCharts subtractCell:@"C14" fromCell:@"B14"];
	}];
	cell = [ssCharts spreadSheetCellForIdentifier:@"B15"];
	cell.watchList = @[[ssCharts spreadSheetCellForIdentifier:@"B9"]
					   ,[ssCurrent spreadSheetCellForIdentifier:@"B19"]];
	[cell setFunction:^NSNumber *(IMSpreadSheet *calculator, IMSpreadSheetCell *cell) {
		double val1 = [ssCharts valueForIdentifier:@"B9"].doubleValue * [ssCurrent valueForIdentifier:@"B19"].doubleValue;
		return [NSNumber numberWithDouble:val1];
	}];
	cell = [ssCharts spreadSheetCellForIdentifier:@"C15"];
	cell.watchList = @[[ssCharts spreadSheetCellForIdentifier:@"C9"]
					   ,[ssCurrent spreadSheetCellForIdentifier:@"B19"]];
	[cell setFunction:^NSNumber *(IMSpreadSheet *calculator, IMSpreadSheetCell *cell) {
		double val1 = [ssCharts valueForIdentifier:@"C9"].doubleValue * [ssCurrent valueForIdentifier:@"B19"].doubleValue;
		return [NSNumber numberWithDouble:val1];
	}];
	
	cell = [ssCharts spreadSheetCellForIdentifier:@"E15"];
	cell.watchList = [ssCharts arrayOfCells:@[@"B15", @"C15"]];
	[cell setFunction:^NSNumber *(IMSpreadSheet *calculator, IMSpreadSheetCell *cell) {
		return [ssCharts subtractCell:@"C15" fromCell:@"B15"];
	}];
	cell = [ssCharts spreadSheetCellForIdentifier:@"B16"];
	cell.watchList = [ssCharts arrayOfCells:@[@"B12", @"B13", @"B14", @"B15"]];
	[cell setFunction:^NSNumber *(IMSpreadSheet *calculator, IMSpreadSheetCell *cell) {
		return [ssCharts sumOfCells:@[@"B12", @"B13", @"B14", @"B15"]];
	}];
	cell = [ssCharts spreadSheetCellForIdentifier:@"C16"];
	cell.watchList = [ssCharts arrayOfCells:@[@"C12", @"C13", @"C14", @"C15"]];
	[cell setFunction:^NSNumber *(IMSpreadSheet *calculator, IMSpreadSheetCell *cell) {
		return [ssCharts sumOfCells:@[@"C12", @"C13", @"C14", @"C15"]];
	}];
	cell = [ssCharts spreadSheetCellForIdentifier:@"E16"];
	cell.watchList = [ssCharts arrayOfCells:@[@"B16", @"C16"]];
	[cell setFunction:^NSNumber *(IMSpreadSheet *calculator, IMSpreadSheetCell *cell) {
		return [ssCharts subtractCell:@"C16" fromCell:@"B16"];
	}];
	cell = [ssCharts spreadSheetCellForIdentifier:@"B19"];
	cell.watchList = @[[ssCharts spreadSheetCellForIdentifier:@"B2"]];
	[cell setFunction:^NSNumber *(IMSpreadSheet *calculator, IMSpreadSheetCell *cell) {
		double val1 = [ssCharts valueForIdentifier:@"B2"].doubleValue * 2.0;
		return [NSNumber numberWithDouble:val1];
	}];
	[[ssCharts spreadSheetCellForIdentifier:@"C19"] mapToCell:[ssCurrent spreadSheetCellForIdentifier:@"B2"]];
	cell = [ssCharts spreadSheetCellForIdentifier:@"E19"];
	cell.watchList = [ssCharts arrayOfCells:@[@"B19", @"C19"]];
	[cell setFunction:^NSNumber *(IMSpreadSheet *calculator, IMSpreadSheetCell *cell) {
		return [ssCharts subtractCell:@"C19" fromCell:@"B19"];
	}];
	[[ssCharts spreadSheetCellForIdentifier:@"B20"] mapToCell:[ssCharts spreadSheetCellForIdentifier:@"B4"]];
	[ssCharts spreadSheetCellForIdentifier:@"C20"].value = @0.0;
	cell = [ssCharts spreadSheetCellForIdentifier:@"E20"];
	cell.watchList = [ssCharts arrayOfCells:@[@"B20", @"C20"]];
	[cell setFunction:^NSNumber *(IMSpreadSheet *calculator, IMSpreadSheetCell *cell) {
		return [ssCharts subtractCell:@"C20" fromCell:@"B20"];
	}];
	cell = [ssCharts spreadSheetCellForIdentifier:@"B21"];
	cell.watchList = @[[ssCurrent spreadSheetCellForIdentifier:@"B2"]
					   ,[ssCharts spreadSheetCellForIdentifier:@"B4"]];
	[cell setFunction:^NSNumber *(IMSpreadSheet *calculator, IMSpreadSheetCell *cell) {
		double val1 = [ssCurrent valueForIdentifier:@"B2"].doubleValue * 2.0 + [ssCharts valueForIdentifier:@"B4"].doubleValue;
		return [NSNumber numberWithDouble:val1];
	}];
	[[ssCharts spreadSheetCellForIdentifier:@"C21"] mapToCell:[ssCurrent spreadSheetCellForIdentifier:@"B2"]];
    cell = [ssCharts spreadSheetCellForIdentifier:@"E21"];
    cell.watchList = [ssCharts arrayOfCells:@[@"B21", @"C21"]];
    [cell setFunction:^NSNumber *(IMSpreadSheet *calculator, IMSpreadSheetCell *cell) {
        return [ssCharts subtractCell:@"C21" fromCell:@"B21"];
    }];
    
    //Sensitivity and specificity from the sens_spec worksheet
    [[ssCharts spreadSheetCellForIdentifier:@"D23"] mapToCell:[ssSens spreadSheetCellForIdentifier:@"D20"]];// T-Spot.TB sensitivity
    [[ssCharts spreadSheetCellForIdentifier:@"D24"] mapToCell:[ssSens spreadSheetCellForIdentifier:@"D21"]];// T-Spot.TB Specificity
    [[ssCharts spreadSheetCellForIdentifier:@"D25"] mapToCell:[ssSens spreadSheetCellForIdentifier:@"D34"]];// T-Spot.TB + Mantoux+ sensitivity
    [[ssCharts spreadSheetCellForIdentifier:@"D26"] mapToCell:[ssSens spreadSheetCellForIdentifier:@"D35"]];// T-Spot.TB + Mantoux+ specificity
    
    
	
	//**********
	
	cell = [ssCharts spreadSheetCellForIdentifier:@"H2"];
	cell.watchList = [ssCharts arrayOfCells:@[@"B2", @"H3"]];
	[cell setFunction:^NSNumber *(IMSpreadSheet *calculator, IMSpreadSheetCell *cell) {
		return [ssCharts subtractCell:@"H3" fromCell:@"B2"];
	}];
	
	[[ssCharts spreadSheetCellForIdentifier:@"H3"] mapToCell:[ssCharts spreadSheetCellForIdentifier:@"B3"]];
	[[ssCharts spreadSheetCellForIdentifier:@"H6"] mapToCell:[ssSens spreadSheetCellForIdentifier:@"D5"]];
	[[ssCharts spreadSheetCellForIdentifier:@"H7"] mapToCell:[ssSens spreadSheetCellForIdentifier:@"E5"]];
	[[ssCharts spreadSheetCellForIdentifier:@"H8"] mapToCell:[ssSens spreadSheetCellForIdentifier:@"D7"]];
	[[ssCharts spreadSheetCellForIdentifier:@"H9"] mapToCell:[ssSens spreadSheetCellForIdentifier:@"E7"]];
	[[ssCharts spreadSheetCellForIdentifier:@"H13"] mapToCell:[ssSens spreadSheetCellForIdentifier:@"D28"]];
	[[ssCharts spreadSheetCellForIdentifier:@"H14"] mapToCell:[ssSens spreadSheetCellForIdentifier:@"E28"]];
	[[ssCharts spreadSheetCellForIdentifier:@"H15"] mapToCell:[ssSens spreadSheetCellForIdentifier:@"D30"]];
	[[ssCharts spreadSheetCellForIdentifier:@"H16"] mapToCell:[ssSens spreadSheetCellForIdentifier:@"E30"]];
	[[ssCharts spreadSheetCellForIdentifier:@"H19"] mapToCell:[ssSens spreadSheetCellForIdentifier:@"D15"]];
	[[ssCharts spreadSheetCellForIdentifier:@"H20"] mapToCell:[ssSens spreadSheetCellForIdentifier:@"E15"]];
	[[ssCharts spreadSheetCellForIdentifier:@"H21"] mapToCell:[ssSens spreadSheetCellForIdentifier:@"D17"]];
	[[ssCharts spreadSheetCellForIdentifier:@"H22"] mapToCell:[ssSens spreadSheetCellForIdentifier:@"E17"]];
}

- (void) initializeSensSpec
{
	IMSpreadSheet *ssCurrent = [self spreadSheetWithName:kIMUKTSPOTVsDualCurrentPractice];
	IMSpreadSheet *ssTSPOT = [self spreadSheetWithName:kIMUKTSPOTVsDualTSPOT];
	IMSpreadSheet *ssSens = [self spreadSheetWithName:kIMUKTSPOTVsDualSensSpec];
	IMSpreadSheetCell *cell;
	
	[[ssSens spreadSheetCellForIdentifier:@"B2"] mapToCell:[ssCurrent spreadSheetCellForIdentifier:@"B2"]];
	cell = [ssSens spreadSheetCellForIdentifier:@"D2"];
	cell.watchList = [ssCurrent arrayOfCells:@[@"B2", @"B14"]];
	[cell setFunction:^NSNumber *(IMSpreadSheet *calculator, IMSpreadSheetCell *cell) {
		return [ssCurrent multiplyCells:@[@"B2", @"B14"]];
	}];
	
	cell = [ssSens spreadSheetCellForIdentifier:@"E2"];
	cell.watchList = [ssSens arrayOfCells:@[@"B2", @"D2"]];
	[cell setFunction:^NSNumber *(IMSpreadSheet *calculator, IMSpreadSheetCell *cell) {
		return [ssSens subtractCell:@"D2"
						   fromCell:@"B2"];
	}];
	
	cell = [ssSens spreadSheetCellForIdentifier:@"G2"];
	cell.watchList = [ssSens arrayOfCells:@[@"B2", @"D2"]];
	[cell setFunction:^NSNumber *(IMSpreadSheet *calculator, IMSpreadSheetCell *cell) {
		return [ssSens divideCells:@[@"D2", @"B2"]];
	}];
	
	cell = [ssSens spreadSheetCellForIdentifier:@"B5"];
	cell.watchList = [ssSens arrayOfCells:@[@"D5", @"E5"]];
	[cell setFunction:^NSNumber *(IMSpreadSheet *calculator, IMSpreadSheetCell *cell) {
		return [ssSens sumOfCells:@[@"D5", @"E5"]];
	}];
	
	cell = [ssSens spreadSheetCellForIdentifier:@"D5"];
	cell.watchList = @[[ssSens spreadSheetCellForIdentifier:@"D2"]
					   ,[ssCurrent spreadSheetCellForIdentifier:@"B10"]];
	[cell setFunction:^NSNumber *(IMSpreadSheet *calculator, IMSpreadSheetCell *cell) {
		double val1 = [ssSens valueForIdentifier:@"D2"].doubleValue * [ssCurrent valueForIdentifier:@"B10"].doubleValue;
		return [NSNumber numberWithDouble:val1];
	}];
	
	cell = [ssSens spreadSheetCellForIdentifier:@"E5"];
	cell.watchList = [ssSens arrayOfCells:@[@"E2", @"D7"]];
	[cell setFunction:^NSNumber *(IMSpreadSheet *calculator, IMSpreadSheetCell *cell) {
		return [ssSens subtractCell:@"D7" fromCell:@"E2"];
	}];
	
	cell = [ssSens spreadSheetCellForIdentifier:@"G5"];
	cell.watchList = [ssSens arrayOfCells:@[@"D5", @"B5"]];
	[cell setFunction:^NSNumber *(IMSpreadSheet *calculator, IMSpreadSheetCell *cell) {
		return [ssSens divideCells:@[@"D5", @"B5"]];
	}];
	
	cell = [ssSens spreadSheetCellForIdentifier:@"B7"];
	cell.watchList = [ssSens arrayOfCells:@[@"D7", @"E7"]];
	[cell setFunction:^NSNumber *(IMSpreadSheet *calculator, IMSpreadSheetCell *cell) {
		return [ssSens sumOfCells:@[@"D7", @"E7"]];
	}];
	
	cell = [ssSens spreadSheetCellForIdentifier:@"D7"];
	cell.watchList = @[[ssSens spreadSheetCellForIdentifier:@"E2"]
					   ,[ssCurrent spreadSheetCellForIdentifier:@"B11"]];
	[cell setFunction:^NSNumber *(IMSpreadSheet *calculator, IMSpreadSheetCell *cell) {
		double val1 = [ssSens valueForIdentifier:@"E2"].doubleValue * [ssCurrent valueForIdentifier:@"B11"].doubleValue;
		return [NSNumber numberWithDouble:val1];
	}];
	
	cell = [ssSens spreadSheetCellForIdentifier:@"E7"];
	cell.watchList = [ssSens arrayOfCells:@[@"D2", @"D5"]];
	[cell setFunction:^NSNumber *(IMSpreadSheet *calculator, IMSpreadSheetCell *cell) {
		return [ssSens subtractCell:@"D5" fromCell:@"D2"];
	}];
	
	cell = [ssSens spreadSheetCellForIdentifier:@"G7"];
	cell.watchList = [ssSens arrayOfCells:@[@"D7", @"B7"]];
	[cell setFunction:^NSNumber *(IMSpreadSheet *calculator, IMSpreadSheetCell *cell) {
		return [ssSens divideCells:@[@"D7", @"B7"]];
	}];
	
	cell = [ssSens spreadSheetCellForIdentifier:@"D10"];
	cell.watchList = [ssSens arrayOfCells:@[@"D5", @"D2"]];
	[cell setFunction:^NSNumber *(IMSpreadSheet *calculator, IMSpreadSheetCell *cell) {
		return [ssSens divideCells:@[@"D5", @"D2"]];
	}];
	
	cell = [ssSens spreadSheetCellForIdentifier:@"D11"];
	cell.watchList = [ssSens arrayOfCells:@[@"D7", @"E2"]];
	[cell setFunction:^NSNumber *(IMSpreadSheet *calculator, IMSpreadSheetCell *cell) {
		return [ssSens divideCells:@[@"D7", @"E2"]];
	}];
	
	cell = [ssSens spreadSheetCellForIdentifier:@"B15"];
	cell.watchList = [ssSens arrayOfCells:@[@"D15", @"E15"]];
	[cell setFunction:^NSNumber *(IMSpreadSheet *calculator, IMSpreadSheetCell *cell) {
		return [ssSens sumOfCells:@[@"D15", @"E15"]];
	}];
	
	cell = [ssSens spreadSheetCellForIdentifier:@"D15"];
	cell.watchList = @[[ssSens spreadSheetCellForIdentifier:@"D2"]
					   ,[ssTSPOT spreadSheetCellForIdentifier:@"B4"]];
	[cell setFunction:^NSNumber *(IMSpreadSheet *calculator, IMSpreadSheetCell *cell) {
		double val1 = [ssSens valueForIdentifier:@"D2"].doubleValue * [ssTSPOT valueForIdentifier:@"B4"].doubleValue;
		return [NSNumber numberWithDouble:val1];
	}];
	
	cell = [ssSens spreadSheetCellForIdentifier:@"E15"];
	cell.watchList = [ssSens arrayOfCells:@[@"E2", @"D17"]];
	[cell setFunction:^NSNumber *(IMSpreadSheet *calculator, IMSpreadSheetCell *cell) {
		return [ssSens subtractCell:@"D17" fromCell:@"E2"];
	}];
	
	cell = [ssSens spreadSheetCellForIdentifier:@"G15"];
	cell.watchList = [ssSens arrayOfCells:@[@"D15", @"B15"]];
	[cell setFunction:^NSNumber *(IMSpreadSheet *calculator, IMSpreadSheetCell *cell) {
		return [ssSens divideCells:@[@"D15", @"B15"]];
	}];
	
	cell = [ssSens spreadSheetCellForIdentifier:@"B17"];
	cell.watchList = [ssSens arrayOfCells:@[@"D17", @"E17"]];
	[cell setFunction:^NSNumber *(IMSpreadSheet *calculator, IMSpreadSheetCell *cell) {
		return [ssSens sumOfCells:@[@"D17", @"E17"]];
	}];
	
	cell = [ssSens spreadSheetCellForIdentifier:@"D17"];
	cell.watchList = @[[ssSens spreadSheetCellForIdentifier:@"E2"]
					   ,[ssTSPOT spreadSheetCellForIdentifier:@"B5"]];
	[cell setFunction:^NSNumber *(IMSpreadSheet *calculator, IMSpreadSheetCell *cell) {
		double val1 = [ssSens valueForIdentifier:@"E2"].doubleValue * [ssTSPOT valueForIdentifier:@"B5"].doubleValue;
		return [NSNumber numberWithDouble:val1];
	}];
	
	cell = [ssSens spreadSheetCellForIdentifier:@"E17"];
	cell.watchList = [ssSens arrayOfCells:@[@"D2", @"D15"]];
	[cell setFunction:^NSNumber *(IMSpreadSheet *calculator, IMSpreadSheetCell *cell) {
		return [ssSens subtractCell:@"D15" fromCell:@"D2"];
	}];
	
	cell = [ssSens spreadSheetCellForIdentifier:@"G17"];
	cell.watchList = [ssSens arrayOfCells:@[@"D17", @"B17"]];
	[cell setFunction:^NSNumber *(IMSpreadSheet *calculator, IMSpreadSheetCell *cell) {
		return [ssSens divideCells:@[@"D17", @"B17"]];
	}];
	
	cell = [ssSens spreadSheetCellForIdentifier:@"D20"];
	cell.watchList = [ssSens arrayOfCells:@[@"D15", @"D2"]];
	[cell setFunction:^NSNumber *(IMSpreadSheet *calculator, IMSpreadSheetCell *cell) {
		return [ssSens divideCells:@[@"D15", @"D2"]];
	}];
	
	cell = [ssSens spreadSheetCellForIdentifier:@"D21"];
	cell.watchList = [ssSens arrayOfCells:@[@"D17", @"E2"]];
	[cell setFunction:^NSNumber *(IMSpreadSheet *calculator, IMSpreadSheetCell *cell) {
		return [ssSens divideCells:@[@"D17", @"E2"]];
	}];
	
	[[ssSens spreadSheetCellForIdentifier:@"B26"] mapToCell:[ssSens spreadSheetCellForIdentifier:@"B5"]];
	[[ssSens spreadSheetCellForIdentifier:@"D26"] mapToCell:[ssSens spreadSheetCellForIdentifier:@"D5"]];
	cell = [ssSens spreadSheetCellForIdentifier:@"E26"];
	cell.watchList = [ssSens arrayOfCells:@[@"B26", @"D26"]];
	[cell setFunction:^NSNumber *(IMSpreadSheet *calculator, IMSpreadSheetCell *cell) {
		return [ssSens subtractCell:@"D26" fromCell:@"B26"];
	}];
	
	cell = [ssSens spreadSheetCellForIdentifier:@"G26"];
	cell.watchList = [ssSens arrayOfCells:@[@"D26", @"B26"]];
	[cell setFunction:^NSNumber *(IMSpreadSheet *calculator, IMSpreadSheetCell *cell) {
		return [ssSens divideCells:@[@"D26", @"B26"]];
	}];
	
	cell = [ssSens spreadSheetCellForIdentifier:@"B28"];
	cell.watchList = [ssSens arrayOfCells:@[@"D28", @"E28"]];
	[cell setFunction:^NSNumber *(IMSpreadSheet *calculator, IMSpreadSheetCell *cell) {
		return [ssSens sumOfCells:@[@"D28", @"E28"]];
	}];
	
	cell = [ssSens spreadSheetCellForIdentifier:@"D28"];
	cell.watchList = @[[ssSens spreadSheetCellForIdentifier:@"D26"]
					   ,[ssTSPOT spreadSheetCellForIdentifier:@"B4"]];
	[cell setFunction:^NSNumber *(IMSpreadSheet *calculator, IMSpreadSheetCell *cell) {
		double val1 = [ssSens valueForIdentifier:@"D26"].doubleValue * [ssTSPOT valueForIdentifier:@"B4"].doubleValue;
		return [NSNumber numberWithDouble:val1];
	}];
	
	cell = [ssSens spreadSheetCellForIdentifier:@"E28"];
	cell.watchList = [ssSens arrayOfCells:@[@"E26", @"D30"]];
	[cell setFunction:^NSNumber *(IMSpreadSheet *calculator, IMSpreadSheetCell *cell) {
		return [ssSens subtractCell:@"D30" fromCell:@"E26"];
	}];
	
	cell = [ssSens spreadSheetCellForIdentifier:@"G28"];
	cell.watchList = [ssSens arrayOfCells:@[@"D28", @"B28"]];
	[cell setFunction:^NSNumber *(IMSpreadSheet *calculator, IMSpreadSheetCell *cell) {
		return [ssSens divideCells:@[@"D28", @"B28"]];
	}];
	
	cell = [ssSens spreadSheetCellForIdentifier:@"B30"];
	cell.watchList = [ssSens arrayOfCells:@[@"D30", @"E30"]];
	[cell setFunction:^NSNumber *(IMSpreadSheet *calculator, IMSpreadSheetCell *cell) {
		return [ssSens sumOfCells:@[@"D30", @"E30"]];
	}];
	
	cell = [ssSens spreadSheetCellForIdentifier:@"D30"];
	cell.watchList = @[[ssSens spreadSheetCellForIdentifier:@"E26"]
					   ,[ssTSPOT spreadSheetCellForIdentifier:@"B5"]];
	[cell setFunction:^NSNumber *(IMSpreadSheet *calculator, IMSpreadSheetCell *cell) {
		double val1 = [ssSens valueForIdentifier:@"E26"].doubleValue * [ssTSPOT valueForIdentifier:@"B5"].doubleValue;
		return [NSNumber numberWithDouble:val1];
	}];
	
	cell = [ssSens spreadSheetCellForIdentifier:@"E30"];
	cell.watchList = [ssSens arrayOfCells:@[@"D26", @"D28"]];
	[cell setFunction:^NSNumber *(IMSpreadSheet *calculator, IMSpreadSheetCell *cell) {
		return  [ssSens subtractCell:@"D28" fromCell:@"D26"];
	}];
	
	cell = [ssSens spreadSheetCellForIdentifier:@"G30"];
	cell.watchList = [ssSens arrayOfCells:@[@"D30", @"B30"]];
	[cell setFunction:^NSNumber *(IMSpreadSheet *calculator, IMSpreadSheetCell *cell) {
		return [ssSens divideCells:@[@"D30", @"B30"]];
	}];
	
	cell = [ssSens spreadSheetCellForIdentifier:@"E32"];
	cell.watchList = [ssSens arrayOfCells:@[@"E30", @"E7"]];
	[cell setFunction:^NSNumber *(IMSpreadSheet *calculator, IMSpreadSheetCell *cell) {
		return [ssSens sumOfCells:@[@"E30", @"E7"]];
	}];
	
	cell = [ssSens spreadSheetCellForIdentifier:@"D34"];
	cell.watchList = [ssSens arrayOfCells:@[@"D28", @"D2"]];
	[cell setFunction:^NSNumber *(IMSpreadSheet *calculator, IMSpreadSheetCell *cell) {
		return [ssSens divideCells:@[@"D28", @"D2"]];
	}];
	
	cell = [ssSens spreadSheetCellForIdentifier:@"D35"];
	cell.watchList = [ssSens arrayOfCells:@[@"E28", @"E2"]];
	[cell setFunction:^NSNumber *(IMSpreadSheet *calculator, IMSpreadSheetCell *cell) {
		double val1 = 1.0 - [ssSens divideCells:@[@"E28", @"E2"]].doubleValue;
		return [NSNumber numberWithDouble:val1];
	}];
}

@end
