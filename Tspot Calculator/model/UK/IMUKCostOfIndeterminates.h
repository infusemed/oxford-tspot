//
//  IMUKCostOfIndeterminates.h
//  Tspot Calculator
//
//  Created by Anthony Long on 4/6/15.
//  Copyright (c) 2015 Infuse Medical. All rights reserved.
//

#import "IMSpreadSheet.h"

@interface IMUKCostOfIndeterminates : IMWorkbook

extern NSString *const kIMUKCostOfIndeterminates;

@end
