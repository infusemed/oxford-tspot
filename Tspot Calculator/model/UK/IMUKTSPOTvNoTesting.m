//
//  IMUKTSPOTvNoTesting.m
//  Tspot Calculator
//
//  Created by Anthony Long on 4/17/15.
//  Copyright (c) 2015 Infuse Medical. All rights reserved.
//

#import "IMUKTSPOTvNoTesting.h"

@implementation IMUKTSPOTvNoTesting

NSString *const kIMUKTSPOTvNoTestingCurrentPractice = @"Current Practice";
NSString *const kIMUKTSPOTvNoTestingTSPOT = @"TSPOT";
NSString *const kIMUKTSPOTvNoTestingCharts = @"Charts";
NSString *const kIMUKTSPOTSensSpec = @"SensSpec";

- (void) initializeWorkbook
{
	[super initializeWorkbook];
	[self setSpreadSheet:[[IMSpreadSheet alloc] init] WithName:kIMUKTSPOTvNoTestingCurrentPractice];
	[self setSpreadSheet:[[IMSpreadSheet alloc] init] WithName:kIMUKTSPOTvNoTestingTSPOT];
	[self setSpreadSheet:[[IMSpreadSheet alloc] init] WithName:kIMUKTSPOTvNoTestingCharts];
	[self initializeCurrentPractice];
	[self initializeTSPOT];
	[self initializeCharts];
	[self initializeSensSpec];
}

- (void) initializeCurrentPractice
{
	IMSpreadSheet *ssCurrent = [self spreadSheetWithName:kIMUKTSPOTvNoTestingCurrentPractice];
	
	[ssCurrent spreadSheetCellForIdentifier:@"B2"];
	[ssCurrent spreadSheetCellForIdentifier:@"B6"].value = @0.37;
	[ssCurrent spreadSheetCellForIdentifier:@"B6"].maxValue = @1.0;
	[ssCurrent spreadSheetCellForIdentifier:@"B7"].maxValue = @1.0;
	[ssCurrent spreadSheetCellForIdentifier:@"B8"].value = @16.0;
	[ssCurrent spreadSheetCellForIdentifier:@"B9"].value = @398.0;
	[ssCurrent spreadSheetCellForIdentifier:@"B10"].value = @0.1;
	[ssCurrent spreadSheetCellForIdentifier:@"B10"].maxValue = @1.0;
	[ssCurrent spreadSheetCellForIdentifier:@"B11"].value = @6389.0;
}

- (void) initializeTSPOT
{
	IMSpreadSheet *ssTSPOT = [self spreadSheetWithName:kIMUKTSPOTvNoTestingTSPOT];
	
	[ssTSPOT spreadSheetCellForIdentifier:@"B2"];
	[ssTSPOT spreadSheetCellForIdentifier:@"B3"].value = @1.5;
	[ssTSPOT spreadSheetCellForIdentifier:@"B4"].value = @0.956;
	[ssTSPOT spreadSheetCellForIdentifier:@"B4"].maxValue = @1.0;
	[ssTSPOT spreadSheetCellForIdentifier:@"B5"].value = @0.989;
	[ssTSPOT spreadSheetCellForIdentifier:@"B5"].maxValue = @1.0;
}

- (void) initializeCharts
{
	IMSpreadSheet *ssCharts = [self spreadSheetWithName:kIMUKTSPOTvNoTestingCharts];
	IMSpreadSheet *ssCurrent = [self spreadSheetWithName:kIMUKTSPOTvNoTestingCurrentPractice];
	IMSpreadSheet *ssTSPOT = [self spreadSheetWithName:kIMUKTSPOTvNoTestingTSPOT];
	IMSpreadSheetCell *cell = [ssCharts spreadSheetCellForIdentifier:@"B2"];
	cell.watchList = [ssCurrent arrayOfCells:@[@"B6", @"B2"]];
	[cell setFunction:^NSNumber *(IMSpreadSheet *calculator, IMSpreadSheetCell *cell) {
		return [ssCurrent multiplyCells:@[@"B2", @"B6"]];
	}];
	
	[[ssCharts spreadSheetCellForIdentifier:@"C2"] mapToCell:[ssCharts spreadSheetCellForIdentifier:@"B2"]];
	[ssCharts spreadSheetCellForIdentifier:@"E2"].value = @0.0;
	[ssCharts spreadSheetCellForIdentifier:@"B3"].value = @0.0;
	cell = [ssCharts spreadSheetCellForIdentifier:@"C3"];
	cell.watchList = @[[ssCurrent spreadSheetCellForIdentifier:@"B2"]
					   ,[ssCurrent spreadSheetCellForIdentifier:@"B6"]
					   ,[ssTSPOT spreadSheetCellForIdentifier:@"B4"]];
	[cell setFunction:^NSNumber *(IMSpreadSheet *calculator, IMSpreadSheetCell *cell) {
		double val1 = [ssCurrent valueForIdentifier:@"B2"].doubleValue * [ssCurrent valueForIdentifier:@"B6"].doubleValue * [ssTSPOT valueForIdentifier:@"B4"].doubleValue;
		return [NSNumber numberWithDouble:val1];
	}];
	
	cell = [ssCharts spreadSheetCellForIdentifier:@"B4"];
	cell.watchList = @[[ssCurrent spreadSheetCellForIdentifier:@"B2"]
					   ,[ssCurrent spreadSheetCellForIdentifier:@"B6"]
					   ,[ssCurrent spreadSheetCellForIdentifier:@"B10"]];
	[cell setFunction:^NSNumber *(IMSpreadSheet *calculator, IMSpreadSheetCell *cell) {
		return [ssCurrent multiplyCells:@[@"B2", @"B6", @"B10"]];
	}];
	
	cell = [ssCharts spreadSheetCellForIdentifier:@"C4"];
	cell.watchList = @[[ssTSPOT spreadSheetCellForIdentifier:@"B4"]
					   ,[ssCurrent spreadSheetCellForIdentifier:@"B2"]
					   ,[ssCurrent spreadSheetCellForIdentifier:@"B6"]
					   ,[ssCurrent spreadSheetCellForIdentifier:@"B10"]];
	[cell setFunction:^NSNumber *(IMSpreadSheet *calculator, IMSpreadSheetCell *cell) {
		double val1 = (1.0 - [ssTSPOT valueForIdentifier:@"B4"].doubleValue) * [ssCurrent multiplyCells:@[@"B2", @"B6", @"B10"]].doubleValue;
		return [NSNumber numberWithDouble:val1];
	}];
	
	cell = [ssCharts spreadSheetCellForIdentifier:@"E4"];
	cell.watchList = [ssCharts arrayOfCells:@[@"B4", @"C4"]];
	[cell setFunction:^NSNumber *(IMSpreadSheet *calculator, IMSpreadSheetCell *cell) {
		return [ssCharts subtractCell:@"C4" fromCell:@"B4"];
	}];
	
	[ssCharts spreadSheetCellForIdentifier:@"B7"].value = @0.0;
	cell = [ssCharts spreadSheetCellForIdentifier:@"C7"];
	cell.watchList = @[[ssCurrent spreadSheetCellForIdentifier:@"B2"]
					   ,[ssTSPOT spreadSheetCellForIdentifier:@"B2"]
					   ,[ssTSPOT spreadSheetCellForIdentifier:@"B3"]];
	[cell setFunction:^NSNumber *(IMSpreadSheet *calculator, IMSpreadSheetCell *cell) {
		double val1 = [ssCurrent valueForIdentifier:@"B2"].doubleValue * ([ssTSPOT valueForIdentifier:@"B2"].doubleValue + [ssTSPOT valueForIdentifier:@"B3"].doubleValue);
		return [NSNumber numberWithDouble:val1];
	}];
	
	cell = [ssCharts spreadSheetCellForIdentifier:@"E7"];
	cell.watchList = [ssCharts arrayOfCells:@[@"B7", @"C7"]];
	[cell setFunction:^NSNumber *(IMSpreadSheet *calculator, IMSpreadSheetCell *cell) {
		return [ssCharts subtractCell:@"C7" fromCell:@"B7"];
	}];
	
	[ssCharts spreadSheetCellForIdentifier:@"B8"].value = @0.0;
	cell = [ssCharts spreadSheetCellForIdentifier:@"C8"];
	cell.watchList = @[[ssCharts spreadSheetCellForIdentifier:@"C2"]
					   ,[ssTSPOT spreadSheetCellForIdentifier:@"B4"]
					   ,[ssCurrent spreadSheetCellForIdentifier:@"B2"]
					   ,[ssTSPOT spreadSheetCellForIdentifier:@"B5"]
					   ,[ssCurrent spreadSheetCellForIdentifier:@"B7"]
					   ,[ssCurrent spreadSheetCellForIdentifier:@"B8"]
					   ,[ssCurrent spreadSheetCellForIdentifier:@"B9"]];
	[cell setFunction:^NSNumber *(IMSpreadSheet *calculator, IMSpreadSheetCell *cell) {
		double val1 = [ssCharts valueForIdentifier:@"C2"].doubleValue * [ssTSPOT valueForIdentifier:@"B4"].doubleValue;
		double val2 = [ssCurrent valueForIdentifier:@"B2"].doubleValue * (1.0 - [ssTSPOT valueForIdentifier:@"B5"].doubleValue);
		double val3 = [ssCurrent valueForIdentifier:@"B7"].doubleValue * ([ssCurrent valueForIdentifier:@"B8"].doubleValue + [ssCurrent valueForIdentifier:@"B9"].doubleValue);
		return [NSNumber numberWithDouble:(val1 + val2)*val3];
	}];
	
	cell = [ssCharts spreadSheetCellForIdentifier:@"E8"];
	cell.watchList = [ssCharts arrayOfCells:@[@"B8", @"C8"]];
	[cell setFunction:^NSNumber *(IMSpreadSheet *calculator, IMSpreadSheetCell *cell) {
		return [ssCharts subtractCell:@"C8" fromCell:@"B8"];
	}];
	
	cell = [ssCharts spreadSheetCellForIdentifier:@"B9"];
	cell.watchList = @[[ssCharts spreadSheetCellForIdentifier:@"B4"]
					   ,[ssCurrent spreadSheetCellForIdentifier:@"B11"]];
	[cell setFunction:^NSNumber *(IMSpreadSheet *calculator, IMSpreadSheetCell *cell) {
		double val1 = [ssCharts valueForIdentifier:@"B4"].doubleValue * [ssCurrent valueForIdentifier:@"B11"].doubleValue;
		return [NSNumber numberWithDouble:val1];
	}];
	
	cell = [ssCharts spreadSheetCellForIdentifier:@"C9"];
	cell.watchList = @[[ssCharts spreadSheetCellForIdentifier:@"C4"]
					   ,[ssCurrent spreadSheetCellForIdentifier:@"B11"]];
	[cell setFunction:^NSNumber *(IMSpreadSheet *calculator, IMSpreadSheetCell *cell) {
		double val1 = [ssCharts valueForIdentifier:@"C4"].doubleValue * [ssCurrent valueForIdentifier:@"B11"].doubleValue;
		return [NSNumber numberWithDouble:val1];
	}];
	
	cell = [ssCharts spreadSheetCellForIdentifier:@"E9"];
	cell.watchList = [ssCharts arrayOfCells:@[@"B9", @"C9"]];
	[cell setFunction:^NSNumber *(IMSpreadSheet *calculator, IMSpreadSheetCell *cell) {
		return [ssCharts subtractCell:@"C9" fromCell:@"B9"];
	}];
	
	cell = [ssCharts spreadSheetCellForIdentifier:@"B10"];
	cell.watchList = [ssCharts arrayOfCells:@[@"B7", @"B8", @"B9"]];
	[cell setFunction:^NSNumber *(IMSpreadSheet *calculator, IMSpreadSheetCell *cell) {
		return [ssCharts sumOfCells:@[@"B7", @"B8", @"B9"]];
	}];
	
	cell = [ssCharts spreadSheetCellForIdentifier:@"C10"];
	cell.watchList = [ssCharts arrayOfCells:@[@"C7", @"C8", @"C9"]];
	[cell setFunction:^NSNumber *(IMSpreadSheet *calculator, IMSpreadSheetCell *cell) {
		return [ssCharts sumOfCells:@[@"C7", @"C8", @"C9"]];
	}];
	
	cell = [ssCharts spreadSheetCellForIdentifier:@"E10"];
	cell.watchList = [ssCharts arrayOfCells:@[@"B10", @"C10"]];
	[cell setFunction:^NSNumber *(IMSpreadSheet *calculator, IMSpreadSheetCell *cell) {
		return [ssCharts subtractCell:@"C10" fromCell:@"B10"];
	}];
}

- (void) initializeSensSpec
{
	IMSpreadSheet *ssCurrent = [self spreadSheetWithName:kIMUKTSPOTvNoTestingCurrentPractice];
	IMSpreadSheet *ssTSPOT = [self spreadSheetWithName:kIMUKTSPOTvNoTestingTSPOT];
	IMSpreadSheet *ssSens = [self spreadSheetWithName:kIMUKTSPOTSensSpec];
	IMSpreadSheetCell *cell;
	
	[[ssSens spreadSheetCellForIdentifier:@"B2"] mapToCell:[ssCurrent spreadSheetCellForIdentifier:@"B2"]];
	cell = [ssSens spreadSheetCellForIdentifier:@"D2"];
	cell.watchList = [ssCurrent arrayOfCells:@[@"B2", @"B14"]];
	[cell setFunction:^NSNumber *(IMSpreadSheet *calculator, IMSpreadSheetCell *cell) {
		return [ssCurrent multiplyCells:@[@"B2", @"B14"]];
	}];
	
	cell = [ssSens spreadSheetCellForIdentifier:@"E2"];
	cell.watchList = [ssSens arrayOfCells:@[@"B2", @"D2"]];
	[cell setFunction:^NSNumber *(IMSpreadSheet *calculator, IMSpreadSheetCell *cell) {
		return [ssSens subtractCell:@"D2"
						   fromCell:@"B2"];
	}];
	
	cell = [ssSens spreadSheetCellForIdentifier:@"G2"];
	cell.watchList = [ssSens arrayOfCells:@[@"B2", @"D2"]];
	[cell setFunction:^NSNumber *(IMSpreadSheet *calculator, IMSpreadSheetCell *cell) {
		return [ssSens divideCells:@[@"D2", @"B2"]];
	}];
	
	cell = [ssSens spreadSheetCellForIdentifier:@"B5"];
	cell.watchList = [ssSens arrayOfCells:@[@"D5", @"E5"]];
	[cell setFunction:^NSNumber *(IMSpreadSheet *calculator, IMSpreadSheetCell *cell) {
		return [ssSens sumOfCells:@[@"D5", @"E5"]];
	}];
	
	cell = [ssSens spreadSheetCellForIdentifier:@"D5"];
	cell.watchList = @[[ssSens spreadSheetCellForIdentifier:@"D2"]
					   ,[ssCurrent spreadSheetCellForIdentifier:@"B10"]];
	[cell setFunction:^NSNumber *(IMSpreadSheet *calculator, IMSpreadSheetCell *cell) {
		double val1 = [ssSens valueForIdentifier:@"D2"].doubleValue * [ssCurrent valueForIdentifier:@"B10"].doubleValue;
		return [NSNumber numberWithDouble:val1];
	}];
	
	cell = [ssSens spreadSheetCellForIdentifier:@"E5"];
	cell.watchList = [ssSens arrayOfCells:@[@"E2", @"D7"]];
	[cell setFunction:^NSNumber *(IMSpreadSheet *calculator, IMSpreadSheetCell *cell) {
		return [ssSens subtractCell:@"D7" fromCell:@"E2"];
	}];
	
	cell = [ssSens spreadSheetCellForIdentifier:@"G5"];
	cell.watchList = [ssSens arrayOfCells:@[@"D5", @"B5"]];
	[cell setFunction:^NSNumber *(IMSpreadSheet *calculator, IMSpreadSheetCell *cell) {
		return [ssSens divideCells:@[@"D5", @"B5"]];
	}];
	
	cell = [ssSens spreadSheetCellForIdentifier:@"B7"];
	cell.watchList = [ssSens arrayOfCells:@[@"D7", @"E7"]];
	[cell setFunction:^NSNumber *(IMSpreadSheet *calculator, IMSpreadSheetCell *cell) {
		return [ssSens sumOfCells:@[@"D7", @"E7"]];
	}];
	
	cell = [ssSens spreadSheetCellForIdentifier:@"D7"];
	cell.watchList = @[[ssSens spreadSheetCellForIdentifier:@"E2"]
					   ,[ssCurrent spreadSheetCellForIdentifier:@"B11"]];
	[cell setFunction:^NSNumber *(IMSpreadSheet *calculator, IMSpreadSheetCell *cell) {
		double val1 = [ssSens valueForIdentifier:@"E2"].doubleValue * [ssCurrent valueForIdentifier:@"B11"].doubleValue;
		return [NSNumber numberWithDouble:val1];
	}];
	
	cell = [ssSens spreadSheetCellForIdentifier:@"E7"];
	cell.watchList = [ssSens arrayOfCells:@[@"D2", @"D5"]];
	[cell setFunction:^NSNumber *(IMSpreadSheet *calculator, IMSpreadSheetCell *cell) {
		return [ssSens subtractCell:@"D5" fromCell:@"D2"];
	}];
	
	cell = [ssSens spreadSheetCellForIdentifier:@"G7"];
	cell.watchList = [ssSens arrayOfCells:@[@"D7", @"B7"]];
	[cell setFunction:^NSNumber *(IMSpreadSheet *calculator, IMSpreadSheetCell *cell) {
		return [ssSens divideCells:@[@"D7", @"B7"]];
	}];
	
	cell = [ssSens spreadSheetCellForIdentifier:@"D10"];
	cell.watchList = [ssSens arrayOfCells:@[@"D5", @"D2"]];
	[cell setFunction:^NSNumber *(IMSpreadSheet *calculator, IMSpreadSheetCell *cell) {
		return [ssSens divideCells:@[@"D5", @"D2"]];
	}];
	
	cell = [ssSens spreadSheetCellForIdentifier:@"D11"];
	cell.watchList = [ssSens arrayOfCells:@[@"D7", @"E2"]];
	[cell setFunction:^NSNumber *(IMSpreadSheet *calculator, IMSpreadSheetCell *cell) {
		return [ssSens divideCells:@[@"D7", @"E2"]];
	}];
	
	cell = [ssSens spreadSheetCellForIdentifier:@"B15"];
	cell.watchList = [ssSens arrayOfCells:@[@"D15", @"E15"]];
	[cell setFunction:^NSNumber *(IMSpreadSheet *calculator, IMSpreadSheetCell *cell) {
		return [ssSens sumOfCells:@[@"D15", @"E15"]];
	}];
	
	cell = [ssSens spreadSheetCellForIdentifier:@"D15"];
	cell.watchList = @[[ssSens spreadSheetCellForIdentifier:@"D2"]
					   ,[ssTSPOT spreadSheetCellForIdentifier:@"B4"]];
	[cell setFunction:^NSNumber *(IMSpreadSheet *calculator, IMSpreadSheetCell *cell) {
		double val1 = [ssSens valueForIdentifier:@"D2"].doubleValue * [ssTSPOT valueForIdentifier:@"B4"].doubleValue;
		return [NSNumber numberWithDouble:val1];
	}];
	
	cell = [ssSens spreadSheetCellForIdentifier:@"E15"];
	cell.watchList = [ssSens arrayOfCells:@[@"E2", @"D17"]];
	[cell setFunction:^NSNumber *(IMSpreadSheet *calculator, IMSpreadSheetCell *cell) {
		return [ssSens subtractCell:@"D17" fromCell:@"E2"];
	}];
	
	cell = [ssSens spreadSheetCellForIdentifier:@"G15"];
	cell.watchList = [ssSens arrayOfCells:@[@"D15", @"B15"]];
	[cell setFunction:^NSNumber *(IMSpreadSheet *calculator, IMSpreadSheetCell *cell) {
		return [ssSens divideCells:@[@"D15", @"B15"]];
	}];
	
	cell = [ssSens spreadSheetCellForIdentifier:@"B17"];
	cell.watchList = [ssSens arrayOfCells:@[@"D17", @"E17"]];
	[cell setFunction:^NSNumber *(IMSpreadSheet *calculator, IMSpreadSheetCell *cell) {
		return [ssSens sumOfCells:@[@"D17", @"E17"]];
	}];
	
	cell = [ssSens spreadSheetCellForIdentifier:@"D17"];
	cell.watchList = @[[ssSens spreadSheetCellForIdentifier:@"E2"]
					   ,[ssTSPOT spreadSheetCellForIdentifier:@"B5"]];
	[cell setFunction:^NSNumber *(IMSpreadSheet *calculator, IMSpreadSheetCell *cell) {
		double val1 = [ssSens valueForIdentifier:@"E2"].doubleValue * [ssTSPOT valueForIdentifier:@"B5"].doubleValue;
		return [NSNumber numberWithDouble:val1];
	}];
	
	cell = [ssSens spreadSheetCellForIdentifier:@"E17"];
	cell.watchList = [ssSens arrayOfCells:@[@"D2", @"D15"]];
	[cell setFunction:^NSNumber *(IMSpreadSheet *calculator, IMSpreadSheetCell *cell) {
		return [ssSens subtractCell:@"D15" fromCell:@"D2"];
	}];
	
	cell = [ssSens spreadSheetCellForIdentifier:@"G17"];
	cell.watchList = [ssSens arrayOfCells:@[@"D17", @"B17"]];
	[cell setFunction:^NSNumber *(IMSpreadSheet *calculator, IMSpreadSheetCell *cell) {
		return [ssSens divideCells:@[@"D17", @"B17"]];
	}];
	
	cell = [ssSens spreadSheetCellForIdentifier:@"D20"];
	cell.watchList = [ssSens arrayOfCells:@[@"D15", @"D2"]];
	[cell setFunction:^NSNumber *(IMSpreadSheet *calculator, IMSpreadSheetCell *cell) {
		return [ssSens divideCells:@[@"D15", @"D2"]];
	}];
	
	cell = [ssSens spreadSheetCellForIdentifier:@"D21"];
	cell.watchList = [ssSens arrayOfCells:@[@"D17", @"E2"]];
	[cell setFunction:^NSNumber *(IMSpreadSheet *calculator, IMSpreadSheetCell *cell) {
		return [ssSens divideCells:@[@"D17", @"E2"]];
	}];
	
	[[ssSens spreadSheetCellForIdentifier:@"B26"] mapToCell:[ssSens spreadSheetCellForIdentifier:@"B5"]];
	[[ssSens spreadSheetCellForIdentifier:@"D26"] mapToCell:[ssSens spreadSheetCellForIdentifier:@"D5"]];
	cell = [ssSens spreadSheetCellForIdentifier:@"E26"];
	cell.watchList = [ssSens arrayOfCells:@[@"B26", @"D26"]];
	[cell setFunction:^NSNumber *(IMSpreadSheet *calculator, IMSpreadSheetCell *cell) {
		return [ssSens subtractCell:@"D26" fromCell:@"B26"];
	}];
	
	cell = [ssSens spreadSheetCellForIdentifier:@"G26"];
	cell.watchList = [ssSens arrayOfCells:@[@"D26", @"B26"]];
	[cell setFunction:^NSNumber *(IMSpreadSheet *calculator, IMSpreadSheetCell *cell) {
		return [ssSens divideCells:@[@"D26", @"B26"]];
	}];
	
	cell = [ssSens spreadSheetCellForIdentifier:@"B28"];
	cell.watchList = [ssSens arrayOfCells:@[@"D28", @"E28"]];
	[cell setFunction:^NSNumber *(IMSpreadSheet *calculator, IMSpreadSheetCell *cell) {
		return [ssSens sumOfCells:@[@"D28", @"E28"]];
	}];
	
	cell = [ssSens spreadSheetCellForIdentifier:@"D28"];
	cell.watchList = @[[ssSens spreadSheetCellForIdentifier:@"D26"]
					   ,[ssTSPOT spreadSheetCellForIdentifier:@"B4"]];
	[cell setFunction:^NSNumber *(IMSpreadSheet *calculator, IMSpreadSheetCell *cell) {
		double val1 = [ssSens valueForIdentifier:@"D26"].doubleValue * [ssTSPOT valueForIdentifier:@"B4"].doubleValue;
		return [NSNumber numberWithDouble:val1];
	}];
	
	cell = [ssSens spreadSheetCellForIdentifier:@"E28"];
	cell.watchList = [ssSens arrayOfCells:@[@"E26", @"D30"]];
	[cell setFunction:^NSNumber *(IMSpreadSheet *calculator, IMSpreadSheetCell *cell) {
		return [ssSens subtractCell:@"D30" fromCell:@"E26"];
	}];
	
	cell = [ssSens spreadSheetCellForIdentifier:@"G28"];
	cell.watchList = [ssSens arrayOfCells:@[@"D28", @"B28"]];
	[cell setFunction:^NSNumber *(IMSpreadSheet *calculator, IMSpreadSheetCell *cell) {
		return [ssSens divideCells:@[@"D28", @"B28"]];
	}];
	
	cell = [ssSens spreadSheetCellForIdentifier:@"B30"];
	cell.watchList = [ssSens arrayOfCells:@[@"D30", @"E30"]];
	[cell setFunction:^NSNumber *(IMSpreadSheet *calculator, IMSpreadSheetCell *cell) {
		return [ssSens sumOfCells:@[@"D30", @"E30"]];
	}];
	
	cell = [ssSens spreadSheetCellForIdentifier:@"D30"];
	cell.watchList = @[[ssSens spreadSheetCellForIdentifier:@"E26"]
					   ,[ssTSPOT spreadSheetCellForIdentifier:@"B5"]];
	[cell setFunction:^NSNumber *(IMSpreadSheet *calculator, IMSpreadSheetCell *cell) {
		double val1 = [ssSens valueForIdentifier:@"E26"].doubleValue * [ssTSPOT valueForIdentifier:@"B5"].doubleValue;
		return [NSNumber numberWithDouble:val1];
	}];
	
	cell = [ssSens spreadSheetCellForIdentifier:@"E30"];
	cell.watchList = [ssSens arrayOfCells:@[@"D26", @"D28"]];
	[cell setFunction:^NSNumber *(IMSpreadSheet *calculator, IMSpreadSheetCell *cell) {
		return  [ssSens subtractCell:@"D28" fromCell:@"D26"];
	}];
	
	cell = [ssSens spreadSheetCellForIdentifier:@"G30"];
	cell.watchList = [ssSens arrayOfCells:@[@"D30", @"B30"]];
	[cell setFunction:^NSNumber *(IMSpreadSheet *calculator, IMSpreadSheetCell *cell) {
		return [ssSens divideCells:@[@"D30", @"B30"]];
	}];
	
	cell = [ssSens spreadSheetCellForIdentifier:@"E32"];
	cell.watchList = [ssSens arrayOfCells:@[@"E30", @"E7"]];
	[cell setFunction:^NSNumber *(IMSpreadSheet *calculator, IMSpreadSheetCell *cell) {
		return [ssSens sumOfCells:@[@"E30", @"E7"]];
	}];
	
	cell = [ssSens spreadSheetCellForIdentifier:@"D34"];
	cell.watchList = [ssSens arrayOfCells:@[@"D28", @"D2"]];
	[cell setFunction:^NSNumber *(IMSpreadSheet *calculator, IMSpreadSheetCell *cell) {
		return [ssSens divideCells:@[@"D28", @"D2"]];
	}];
	
	cell = [ssSens spreadSheetCellForIdentifier:@"D35"];
	cell.watchList = [ssSens arrayOfCells:@[@"E28", @"E2"]];
	[cell setFunction:^NSNumber *(IMSpreadSheet *calculator, IMSpreadSheetCell *cell) {
		double val1 = 1.0 - [ssSens divideCells:@[@"E28", @"E2"]].doubleValue;
		return [NSNumber numberWithDouble:val1];
	}];
}

@end
