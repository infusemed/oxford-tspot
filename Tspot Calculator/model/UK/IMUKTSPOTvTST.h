//
//  IMUKOccupationalTSPOTvTST.h
//  Tspot Calculator
//
//  Created by Anthony Long on 4/7/15.
//  Copyright (c) 2015 Infuse Medical. All rights reserved.
//

#import "IMSpreadSheet.h"

@interface IMUKTSPOTvTST : IMWorkbook

extern NSString *const kIMUKOccupationalTSPOTvTSTCurrentPractice;
extern NSString *const kIMUKOccupationalTSPOTvTSTPageTSPOT;
extern NSString *const kIMUKOccupationalTSPOTvTSTCharts;

@end
