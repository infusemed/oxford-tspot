//
//  IMUKCostOfIndeterminates.m
//  Tspot Calculator
//
//  Created by Anthony Long on 4/6/15.
//  Copyright (c) 2015 Infuse Medical. All rights reserved.
//

#import "IMUKCostOfIndeterminates.h"

@implementation IMUKCostOfIndeterminates

NSString *const kIMUKCostOfIndeterminates = @"Default";

- (void) initializeWorkbook
{
	[super initializeWorkbook];
	
	IMSpreadSheet *ss = [[IMSpreadSheet alloc] init];
	[self setSpreadSheet:ss WithName:kIMUKCostOfIndeterminates];
	
	IMSpreadSheetCell *cell;
	[ss spreadSheetCellForIdentifier:@"B6"];
	[[ss spreadSheetCellForIdentifier:@"D6"] mapToCell:[ss spreadSheetCellForIdentifier:@"B6"]];
	[ss spreadSheetCellForIdentifier:@"B8"];
	[ss spreadSheetCellForIdentifier:@"D8"].value = @0.014;
	[ss spreadSheetCellForIdentifier:@"D8"].maxValue = @1.0;
	[ss spreadSheetCellForIdentifier:@"B10"];
	[ss spreadSheetCellForIdentifier:@"D10"];
	
	cell = [ss spreadSheetCellForIdentifier:@"B18"];
	cell.watchList = [ss arrayOfCells:@[@"B6", @"B8"]];
	[cell setFunction:^NSNumber *(IMSpreadSheet *calculator, IMSpreadSheetCell *cell) {
		return [ss multiplyCells:@[@"B6", @"B8"]];
	}];
	
	cell = [ss spreadSheetCellForIdentifier:@"D18"];
	cell.watchList = [ss arrayOfCells:@[@"B6"]];
	[cell setFunction:^NSNumber *(IMSpreadSheet *calculator, IMSpreadSheetCell *cell) {
		double val1 = [ss valueForIdentifier:@"B6"].doubleValue * 0.008;
		return [NSNumber numberWithDouble:val1];
	}];
	
	cell = [ss spreadSheetCellForIdentifier:@"E18"];
	cell.watchList = [ss arrayOfCells:@[@"B18", @"D18"]];
	[cell setFunction:^NSNumber *(IMSpreadSheet *calculator, IMSpreadSheetCell *cell) {
		double val1 = 1.0 - ([ss valueForIdentifier:@"D18"].doubleValue / [ss valueForIdentifier:@"B18"].doubleValue);
		return [NSNumber numberWithDouble:val1];
	}];
	
	cell = [ss spreadSheetCellForIdentifier:@"F18"];
	cell.watchList = [ss arrayOfCells:@[@"B18", @"D18"]];
	[cell setFunction:^NSNumber *(IMSpreadSheet *calculator, IMSpreadSheetCell *cell) {
		double val1 = [ss valueForIdentifier:@"B18"].doubleValue - [ss valueForIdentifier:@"D18"].doubleValue;
		return [NSNumber numberWithDouble:val1];
	}];
	
	cell = [ss spreadSheetCellForIdentifier:@"B20"];
	cell.watchList = [ss arrayOfCells:@[@"B18", @"B10"]];
	[cell setFunction:^NSNumber *(IMSpreadSheet *calculator, IMSpreadSheetCell *cell) {
		return [ss multiplyCells:@[@"B18", @"B10"]];
	}];
	
	[ss spreadSheetCellForIdentifier:@"D20"].value = @0.0;
	
	cell = [ss spreadSheetCellForIdentifier:@"B22"];
	cell.watchList = [ss arrayOfCells:@[@"B6", @"B10", @"B20"]];
	[cell setFunction:^NSNumber *(IMSpreadSheet *calculator, IMSpreadSheetCell *cell) {
		double val1 = ([ss multiplyCells:@[@"B6", @"B10"]].doubleValue + [ss valueForIdentifier:@"B20"].doubleValue) / [ss valueForIdentifier:@"B6"].doubleValue;
		return [NSNumber numberWithDouble:val1];
	}];
	
	cell = [ss spreadSheetCellForIdentifier:@"D22"];
	cell.watchList = [ss arrayOfCells:@[@"D6", @"D10", @"D20", @"D6"]];
	[cell setFunction:^NSNumber *(IMSpreadSheet *calculator, IMSpreadSheetCell *cell) {
		double val1 = ([ss multiplyCells:@[@"D6", @"D10"]].doubleValue + [ss valueForIdentifier:@"D20"].doubleValue) / [ss valueForIdentifier:@"D6"].doubleValue;
		return [NSNumber numberWithDouble:val1];
	}];
}

- (void) setCustomerName:(NSString *)customerName
{
	[super setCustomerName:customerName];
	[[self spreadSheetWithName:kIMUKCostOfIndeterminates] spreadSheetCellForIdentifier:@"B3"].textValue = customerName;
}

- (NSString *) customerName
{
	return [[self spreadSheetWithName:kIMUKCostOfIndeterminates] spreadSheetCellForIdentifier:@"B3"].textValue;
}

@end
