//
//  IMUKNewEntrantTSPOTvTST.m
//  Tspot Calculator
//
//  Created by Anthony Long on 4/18/15.
//  Copyright (c) 2015 Infuse Medical. All rights reserved.
//

#import "IMUKNewEntrantTSPOTvTST.h"

@implementation IMUKNewEntrantTSPOTvTST

NSString *const kIMUKNewEntrantTSPOTvTSTCurrentPractice = @"CurrentPractice";
NSString *const kIMUKNewEntrantTSPOTvTSTPageTSPOT = @"TSPOT";
NSString *const kIMUKNewEntrantTSPOTvTSTCharts = @"ChartsAndTotals";
NSString *const kIMUKNewEntrantTSPOTSensSpec = @"SensSpec";

- (void) initializeWorkbook
{
	[super initializeWorkbook];
	[self setSpreadSheet:[[IMSpreadSheet alloc] init] WithName:kIMUKNewEntrantTSPOTvTSTCurrentPractice];
	[self setSpreadSheet:[[IMSpreadSheet alloc] init] WithName:kIMUKNewEntrantTSPOTvTSTPageTSPOT];
	[self setSpreadSheet:[[IMSpreadSheet alloc] init] WithName:kIMUKNewEntrantTSPOTvTSTCharts];
	[self setSpreadSheet:[[IMSpreadSheet alloc] init] WithName:kIMUKNewEntrantTSPOTSensSpec];
	
	[self initCurrentPractice];
	[self initTSPOT];
	[self initCharts];
	[self initializeSensSpec];
}

- (void) initCurrentPractice
{
	IMSpreadSheet *ssCurrent = [self spreadSheetWithName:kIMUKNewEntrantTSPOTvTSTCurrentPractice];
	[ssCurrent spreadSheetCellForIdentifier:@"B2"];
	[ssCurrent spreadSheetCellForIdentifier:@"B5"].value = @17.00;
	[ssCurrent spreadSheetCellForIdentifier:@"B6"].value = @0.00;
	[ssCurrent spreadSheetCellForIdentifier:@"B7"].value = @0.00;
	[ssCurrent spreadSheetCellForIdentifier:@"B8"].value = @0.1;
	[ssCurrent spreadSheetCellForIdentifier:@"B8"].maxValue = @1.0;
	[ssCurrent spreadSheetCellForIdentifier:@"B10"].value = @0.64;
	[ssCurrent spreadSheetCellForIdentifier:@"B10"].maxValue = @1.0;
	[ssCurrent spreadSheetCellForIdentifier:@"B11"].value = @0.57;
	[ssCurrent spreadSheetCellForIdentifier:@"B11"].maxValue = @1.0;
	[ssCurrent spreadSheetCellForIdentifier:@"B14"].value = @0.2;
	[ssCurrent spreadSheetCellForIdentifier:@"B14"].maxValue = @1.0;
	[ssCurrent spreadSheetCellForIdentifier:@"B15"].value = @0.5;
	[ssCurrent spreadSheetCellForIdentifier:@"B15"].maxValue = @1.0;
	[ssCurrent spreadSheetCellForIdentifier:@"B16"].value = @35.0;
	[ssCurrent spreadSheetCellForIdentifier:@"B17"].value = @943.0;
	[ssCurrent spreadSheetCellForIdentifier:@"B18"].value = @0.1;
	[ssCurrent spreadSheetCellForIdentifier:@"B18"].maxValue = @1.0;
	[ssCurrent spreadSheetCellForIdentifier:@"B19"].value = @1399.0;

}

- (void) initTSPOT
{
	IMSpreadSheet *ssTSPOT = [self spreadSheetWithName:kIMUKNewEntrantTSPOTvTSTPageTSPOT];
	[ssTSPOT spreadSheetCellForIdentifier:@"B2"];
	[ssTSPOT spreadSheetCellForIdentifier:@"B3"].value = @1.5;
	[ssTSPOT spreadSheetCellForIdentifier:@"B4"].value = @0.90;
	[ssTSPOT spreadSheetCellForIdentifier:@"B4"].maxValue = @1.0;
	[ssTSPOT spreadSheetCellForIdentifier:@"B5"].value = @0.77;
	[ssTSPOT spreadSheetCellForIdentifier:@"B5"].maxValue = @1.0;
}

- (void) initCharts
{
	IMSpreadSheet *ssCurrent = [self spreadSheetWithName:kIMUKNewEntrantTSPOTvTSTCurrentPractice];
	IMSpreadSheet *ssTSPOT = [self spreadSheetWithName:kIMUKNewEntrantTSPOTvTSTPageTSPOT];
	IMSpreadSheet *ssCharts = [self spreadSheetWithName:kIMUKNewEntrantTSPOTvTSTCharts];
	IMSpreadSheetCell *cell;
	
	cell = [ssCharts spreadSheetCellForIdentifier:@"B2"];
	cell.watchList = [ssCurrent arrayOfCells:@[@"B2", @"B14"]];
	[cell setFunction:^NSNumber *(IMSpreadSheet *calculator, IMSpreadSheetCell *cell) {
		return [ssCurrent multiplyCells:@[@"B2", @"B14"]];
	}];
	[[ssCharts spreadSheetCellForIdentifier:@"C2"] mapToCell:[ssCharts spreadSheetCellForIdentifier:@"B2"]];
	cell = [ssCharts spreadSheetCellForIdentifier:@"E2"];
	cell.watchList = [ssCharts arrayOfCells:@[@"B2", @"C2"]];
	[cell setFunction:^NSNumber *(IMSpreadSheet *calculator, IMSpreadSheetCell *cell) {
		return [ssCharts subtractCell:@"C2" fromCell:@"B2"];
	}];
	
	cell = [ssCharts spreadSheetCellForIdentifier:@"B3"];
	cell.watchList = @[[ssCharts spreadSheetCellForIdentifier:@"B2"]
					   ,[ssCurrent spreadSheetCellForIdentifier:@"B2"]
					   ,[ssCurrent spreadSheetCellForIdentifier:@"B11"]];
	[cell setFunction:^NSNumber *(IMSpreadSheet *calculator, IMSpreadSheetCell *cell) {
		double val1 = [ssCharts valueForIdentifier:@"B2"].doubleValue + (([ssCurrent valueForIdentifier:@"B2"].doubleValue - [ssCharts valueForIdentifier:@"B2"].doubleValue) * (1.0 - [ssCurrent valueForIdentifier:@"B11"].doubleValue));
		return [NSNumber numberWithDouble:val1];
	}];
	cell = [ssCharts spreadSheetCellForIdentifier:@"C3"];
	cell.watchList = @[[ssCurrent spreadSheetCellForIdentifier:@"B2"]
					   ,[ssCurrent spreadSheetCellForIdentifier:@"B14"]
					   ,[ssTSPOT spreadSheetCellForIdentifier:@"B4"]];
	[cell setFunction:^NSNumber *(IMSpreadSheet *calculator, IMSpreadSheetCell *cell) {
		double val1 = [ssCurrent valueForIdentifier:@"B2"].doubleValue * [ssCurrent valueForIdentifier:@"B14"].doubleValue * [ssTSPOT valueForIdentifier:@"B4"].doubleValue;
		return [NSNumber numberWithDouble:val1];
	}];
	
	cell = [ssCharts spreadSheetCellForIdentifier:@"E3"];
	cell.watchList = [ssCharts arrayOfCells:@[@"B3", @"C3"]];
	[cell setFunction:^NSNumber *(IMSpreadSheet *calculator, IMSpreadSheetCell *cell) {
		return [ssCharts subtractCell:@"C3" fromCell:@"B3"];
	}];
	
	cell = [ssCharts spreadSheetCellForIdentifier:@"B4"];
	cell.watchList = @[[ssCharts spreadSheetCellForIdentifier:@"B2"]
					   ,[ssCurrent spreadSheetCellForIdentifier:@"B10"]];
	[cell setFunction:^NSNumber *(IMSpreadSheet *calculator, IMSpreadSheetCell *cell) {
		return [NSNumber numberWithDouble:[ssCharts valueForIdentifier:@"B2"].doubleValue * (1.0 - [ssCurrent valueForIdentifier:@"B10"].doubleValue)];
	}];
	
	cell = [ssCharts spreadSheetCellForIdentifier:@"C4"];
	cell.watchList = @[[ssCharts spreadSheetCellForIdentifier:@"C2"]
					   ,[ssTSPOT spreadSheetCellForIdentifier:@"B4"]];
	[cell setFunction:^NSNumber *(IMSpreadSheet *calculator, IMSpreadSheetCell *cell) {
		return [NSNumber numberWithDouble:[ssCharts valueForIdentifier:@"C2"].doubleValue * (1.0 -[ssTSPOT valueForIdentifier:@"B4"].doubleValue)];
	}];
	
	cell = [ssCharts spreadSheetCellForIdentifier:@"E4"];
	cell.watchList = [ssCharts arrayOfCells:@[@"B4", @"C4"]];
	[cell setFunction:^NSNumber *(IMSpreadSheet *calculator, IMSpreadSheetCell *cell) {
		return [ssCharts subtractCell:@"C4" fromCell:@"B4"];
	}];
	
	cell = [ssCharts spreadSheetCellForIdentifier:@"B5"];
	cell.watchList = [ssCurrent arrayOfCells:@[@"B10", @"B2", @"B14", @"B18"]];
	[cell setFunction:^NSNumber *(IMSpreadSheet *calculator, IMSpreadSheetCell *cell) {
		double val1 = (1.0 - [ssCurrent valueForIdentifier:@"B10"].doubleValue) * [ssCurrent multiplyCells:@[@"B2", @"B14", @"B18"]].doubleValue;
		return [NSNumber numberWithDouble:val1];
	}];
	
	cell = [ssCharts spreadSheetCellForIdentifier:@"C5"];
	cell.watchList = @[[ssTSPOT spreadSheetCellForIdentifier:@"B4"]
					   ,[ssCurrent spreadSheetCellForIdentifier:@"B2"]
					   ,[ssCurrent spreadSheetCellForIdentifier:@"B14"]
					   ,[ssCurrent spreadSheetCellForIdentifier:@"b18"]];
	[cell setFunction:^NSNumber *(IMSpreadSheet *calculator, IMSpreadSheetCell *cell) {
		double val1 = (1.0 - [ssTSPOT valueForIdentifier:@"B4"].doubleValue) * [ssCurrent multiplyCells:@[@"B2", @"B14", @"B18"]].doubleValue;
		return [NSNumber numberWithDouble:val1];
	}];
	
	cell = [ssCharts spreadSheetCellForIdentifier:@"E5"];
	cell.watchList = [ssCharts arrayOfCells:@[@"B5", @"C5"]];
	[cell setFunction:^NSNumber *(IMSpreadSheet *calculator, IMSpreadSheetCell *cell) {
		return [ssCharts subtractCell:@"C5" fromCell:@"B5"];
	}];
	
	cell = [ssCharts spreadSheetCellForIdentifier:@"B8"];
	cell.watchList = [ssCurrent arrayOfCells:@[@"B2", @"B5", @"B6", @"B7", @"B8"]];
	[cell setFunction:^NSNumber *(IMSpreadSheet *calculator, IMSpreadSheetCell *cell) {
		double val1 = [ssCurrent valueForIdentifier:@"B2"].doubleValue * [ssCurrent sumOfCells:@[@"B5", @"B6", @"B7"]].doubleValue * (1.0 - [ssCurrent valueForIdentifier:@"B8"].doubleValue);
		return [NSNumber numberWithDouble:val1];
	}];
	
	cell = [ssCharts spreadSheetCellForIdentifier:@"C8"];
	cell.watchList = @[[ssCurrent spreadSheetCellForIdentifier:@"B2"]
					   ,[ssTSPOT spreadSheetCellForIdentifier:@"B2"]
					   ,[ssTSPOT spreadSheetCellForIdentifier:@"B3"]];
	[cell setFunction:^NSNumber *(IMSpreadSheet *calculator, IMSpreadSheetCell *cell) {
		double val1 = [ssCurrent valueForIdentifier:@"B2"].doubleValue * ([ssTSPOT valueForIdentifier:@"B2"].doubleValue + [ssTSPOT valueForIdentifier:@"B3"].doubleValue);
		return [NSNumber numberWithDouble:val1];
	}];
	
	cell = [ssCharts spreadSheetCellForIdentifier:@"E8"];
	cell.watchList = [ssCharts arrayOfCells:@[@"B8", @"C8"]];
	[cell setFunction:^NSNumber *(IMSpreadSheet *calculator, IMSpreadSheetCell *cell) {
		return [ssCharts subtractCell:@"C8" fromCell:@"B8"];
	}];
	
	cell = [ssCharts spreadSheetCellForIdentifier:@"B9"];
	cell.watchList = @[[ssCharts spreadSheetCellForIdentifier:@"B3"]
					   ,[ssCurrent spreadSheetCellForIdentifier:@"B15"]
					   ,[ssCurrent spreadSheetCellForIdentifier:@"B17"]];
	[cell setFunction:^NSNumber *(IMSpreadSheet *calculator, IMSpreadSheetCell *cell) {
		double val1 = [ssCharts valueForIdentifier:@"B3"].doubleValue * [ssCurrent valueForIdentifier:@"B15"].doubleValue * [ssCurrent valueForIdentifier:@"B17"].doubleValue;
		return [NSNumber numberWithDouble:val1];
	}];
	
	cell = [ssCharts spreadSheetCellForIdentifier:@"C9"];
	cell.watchList = @[[ssCurrent spreadSheetCellForIdentifier:@"B2"]
					   ,[ssCurrent spreadSheetCellForIdentifier:@"B14"]
					   ,[ssTSPOT spreadSheetCellForIdentifier:@"B4"]
					   ,[ssTSPOT spreadSheetCellForIdentifier:@"B5"]
					   ,[ssCurrent spreadSheetCellForIdentifier:@"B15"]
					   ,[ssCurrent spreadSheetCellForIdentifier:@"B16"]
					   ,[ssCurrent spreadSheetCellForIdentifier:@"B17"]];
	[cell setFunction:^NSNumber *(IMSpreadSheet *calculator, IMSpreadSheetCell *cell) {
		double val1 = [ssCurrent valueForIdentifier:@"B2"].doubleValue * [ssCurrent valueForIdentifier:@"B14"].doubleValue * [ssTSPOT valueForIdentifier:@"B4"].doubleValue;
		double val2 = [ssCurrent valueForIdentifier:@"B2"].doubleValue * (1.0 - [ssTSPOT valueForIdentifier:@"B5"].doubleValue);
		double val3 = [ssCurrent valueForIdentifier:@"B15"].doubleValue * ([ssCurrent valueForIdentifier:@"B16"].doubleValue + [ssCurrent valueForIdentifier:@"B17"].doubleValue);
		return [NSNumber numberWithDouble:(val1 + val2) * val3];
	}];
	
	cell = [ssCharts spreadSheetCellForIdentifier:@"E9"];
	cell.watchList = [ssCharts arrayOfCells:@[@"B9", @"C9"]];
	[cell setFunction:^NSNumber *(IMSpreadSheet *calculator, IMSpreadSheetCell *cell) {
		return [ssCharts subtractCell:@"C9" fromCell:@"B9"];
	}];
	
	cell = [ssCharts spreadSheetCellForIdentifier:@"B10"];
	cell.watchList = @[[ssCharts spreadSheetCellForIdentifier:@"B5"]
					   ,[ssCurrent spreadSheetCellForIdentifier:@"B19"]];
	[cell setFunction:^NSNumber *(IMSpreadSheet *calculator, IMSpreadSheetCell *cell) {
		double val1 = [ssCharts valueForIdentifier:@"B5"].doubleValue * [ssCurrent valueForIdentifier:@"B19"].doubleValue;
		return [NSNumber numberWithDouble:val1];
	}];
	
	cell = [ssCharts spreadSheetCellForIdentifier:@"C10"];
	cell.watchList = @[[ssCharts spreadSheetCellForIdentifier:@"C5"]
					   ,[ssCurrent spreadSheetCellForIdentifier:@"B19"]];
	[cell setFunction:^NSNumber *(IMSpreadSheet *calculator, IMSpreadSheetCell *cell) {
		double val1 = [ssCharts valueForIdentifier:@"C5"].doubleValue * [ssCurrent valueForIdentifier:@"B19"].doubleValue;
		return [NSNumber numberWithDouble:val1];
	}];
	
	cell = [ssCharts spreadSheetCellForIdentifier:@"E10"];
	cell.watchList = [ssCharts arrayOfCells:@[@"B10", @"C10"]];
	[cell setFunction:^NSNumber *(IMSpreadSheet *calculator, IMSpreadSheetCell *cell) {
		return [ssCharts subtractCell:@"C10" fromCell:@"B10"];
	}];
	
	cell = [ssCharts spreadSheetCellForIdentifier:@"B11"];
	cell.watchList = [ssCharts arrayOfCells:@[@"B8", @"B9", @"B10"]];
	[cell setFunction:^NSNumber *(IMSpreadSheet *calculator, IMSpreadSheetCell *cell) {
		return [ssCharts sumOfCells:@[@"B8", @"B9", @"B10"]];
	}];
	
	cell = [ssCharts spreadSheetCellForIdentifier:@"C11"];
	cell.watchList = [ssCharts arrayOfCells:@[@"C8", @"C9", @"C10"]];
	[cell setFunction:^NSNumber *(IMSpreadSheet *calculator, IMSpreadSheetCell *cell) {
		return [ssCharts sumOfCells:@[@"C8", @"C9", @"C10"]];
	}];
	
	cell = [ssCharts spreadSheetCellForIdentifier:@"E11"];
	cell.watchList = [ssCharts arrayOfCells:@[@"E8", @"E9", @"E10"]];
	[cell setFunction:^NSNumber *(IMSpreadSheet *calculator, IMSpreadSheetCell *cell) {
		return [ssCharts sumOfCells:@[@"E8", @"E9", @"E10"]];
	}];
	
	cell = [ssCharts spreadSheetCellForIdentifier:@"B14"];
	cell.watchList = @[[ssCurrent spreadSheetCellForIdentifier:@"B2"]];
	[cell setFunction:^NSNumber *(IMSpreadSheet *calculator, IMSpreadSheetCell *cell) {
		return [NSNumber numberWithDouble:[ssCurrent valueForIdentifier:@"B2"].doubleValue * 2.0];
	}];
	
	[[ssCharts spreadSheetCellForIdentifier:@"C14"] mapToCell:[ssCurrent spreadSheetCellForIdentifier:@"B2"]];
	
	cell = [ssCharts spreadSheetCellForIdentifier:@"E14"];
	cell.watchList = [ssCharts arrayOfCells:@[@"B14", @"C14"]];
	[cell setFunction:^NSNumber *(IMSpreadSheet *calculator, IMSpreadSheetCell *cell) {
		return [ssCharts subtractCell:@"C14" fromCell:@"B14"];
	}];
}

- (void) initializeSensSpec
{
	IMSpreadSheet *ssCurrent = [self spreadSheetWithName:kIMUKNewEntrantTSPOTvTSTCurrentPractice];
	IMSpreadSheet *ssTSPOT = [self spreadSheetWithName:kIMUKNewEntrantTSPOTvTSTPageTSPOT];
	IMSpreadSheet *ssSens = [self spreadSheetWithName:kIMUKNewEntrantTSPOTSensSpec];
	IMSpreadSheetCell *cell;
	
	[[ssSens spreadSheetCellForIdentifier:@"B2"] mapToCell:[ssCurrent spreadSheetCellForIdentifier:@"B2"]];
	cell = [ssSens spreadSheetCellForIdentifier:@"D2"];
	cell.watchList = [ssCurrent arrayOfCells:@[@"B2", @"B14"]];
	[cell setFunction:^NSNumber *(IMSpreadSheet *calculator, IMSpreadSheetCell *cell) {
		return [ssCurrent multiplyCells:@[@"B2", @"B14"]];
	}];
	
	cell = [ssSens spreadSheetCellForIdentifier:@"E2"];
	cell.watchList = [ssSens arrayOfCells:@[@"B2", @"D2"]];
	[cell setFunction:^NSNumber *(IMSpreadSheet *calculator, IMSpreadSheetCell *cell) {
		return [ssSens subtractCell:@"D2"
						   fromCell:@"B2"];
	}];
	
	cell = [ssSens spreadSheetCellForIdentifier:@"G2"];
	cell.watchList = [ssSens arrayOfCells:@[@"B2", @"D2"]];
	[cell setFunction:^NSNumber *(IMSpreadSheet *calculator, IMSpreadSheetCell *cell) {
		return [ssSens divideCells:@[@"D2", @"B2"]];
	}];
	
	cell = [ssSens spreadSheetCellForIdentifier:@"B5"];
	cell.watchList = [ssSens arrayOfCells:@[@"D5", @"E5"]];
	[cell setFunction:^NSNumber *(IMSpreadSheet *calculator, IMSpreadSheetCell *cell) {
		return [ssSens sumOfCells:@[@"D5", @"E5"]];
	}];
	
	cell = [ssSens spreadSheetCellForIdentifier:@"D5"];
	cell.watchList = @[[ssSens spreadSheetCellForIdentifier:@"D2"]
					   ,[ssCurrent spreadSheetCellForIdentifier:@"B10"]];
	[cell setFunction:^NSNumber *(IMSpreadSheet *calculator, IMSpreadSheetCell *cell) {
		double val1 = [ssSens valueForIdentifier:@"D2"].doubleValue * [ssCurrent valueForIdentifier:@"B10"].doubleValue;
		return [NSNumber numberWithDouble:val1];
	}];
	
	cell = [ssSens spreadSheetCellForIdentifier:@"E5"];
	cell.watchList = [ssSens arrayOfCells:@[@"E2", @"D7"]];
	[cell setFunction:^NSNumber *(IMSpreadSheet *calculator, IMSpreadSheetCell *cell) {
		return [ssSens subtractCell:@"D7" fromCell:@"E2"];
	}];
	
	cell = [ssSens spreadSheetCellForIdentifier:@"G5"];
	cell.watchList = [ssSens arrayOfCells:@[@"D5", @"B5"]];
	[cell setFunction:^NSNumber *(IMSpreadSheet *calculator, IMSpreadSheetCell *cell) {
		return [ssSens divideCells:@[@"D5", @"B5"]];
	}];
	
	cell = [ssSens spreadSheetCellForIdentifier:@"B7"];
	cell.watchList = [ssSens arrayOfCells:@[@"D7", @"E7"]];
	[cell setFunction:^NSNumber *(IMSpreadSheet *calculator, IMSpreadSheetCell *cell) {
		return [ssSens sumOfCells:@[@"D7", @"E7"]];
	}];
	
	cell = [ssSens spreadSheetCellForIdentifier:@"D7"];
	cell.watchList = @[[ssSens spreadSheetCellForIdentifier:@"E2"]
					   ,[ssCurrent spreadSheetCellForIdentifier:@"B11"]];
	[cell setFunction:^NSNumber *(IMSpreadSheet *calculator, IMSpreadSheetCell *cell) {
		double val1 = [ssSens valueForIdentifier:@"E2"].doubleValue * [ssCurrent valueForIdentifier:@"B11"].doubleValue;
		return [NSNumber numberWithDouble:val1];
	}];
	
	cell = [ssSens spreadSheetCellForIdentifier:@"E7"];
	cell.watchList = [ssSens arrayOfCells:@[@"D2", @"D5"]];
	[cell setFunction:^NSNumber *(IMSpreadSheet *calculator, IMSpreadSheetCell *cell) {
		return [ssSens subtractCell:@"D5" fromCell:@"D2"];
	}];
	
	cell = [ssSens spreadSheetCellForIdentifier:@"G7"];
	cell.watchList = [ssSens arrayOfCells:@[@"D7", @"B7"]];
	[cell setFunction:^NSNumber *(IMSpreadSheet *calculator, IMSpreadSheetCell *cell) {
		return [ssSens divideCells:@[@"D7", @"B7"]];
	}];
	
	cell = [ssSens spreadSheetCellForIdentifier:@"D10"];
	cell.watchList = [ssSens arrayOfCells:@[@"D5", @"D2"]];
	[cell setFunction:^NSNumber *(IMSpreadSheet *calculator, IMSpreadSheetCell *cell) {
		return [ssSens divideCells:@[@"D5", @"D2"]];
	}];
	
	cell = [ssSens spreadSheetCellForIdentifier:@"D11"];
	cell.watchList = [ssSens arrayOfCells:@[@"D7", @"E2"]];
	[cell setFunction:^NSNumber *(IMSpreadSheet *calculator, IMSpreadSheetCell *cell) {
		return [ssSens divideCells:@[@"D7", @"E2"]];
	}];
	
	cell = [ssSens spreadSheetCellForIdentifier:@"B15"];
	cell.watchList = [ssSens arrayOfCells:@[@"D15", @"E15"]];
	[cell setFunction:^NSNumber *(IMSpreadSheet *calculator, IMSpreadSheetCell *cell) {
		return [ssSens sumOfCells:@[@"D15", @"E15"]];
	}];
	
	cell = [ssSens spreadSheetCellForIdentifier:@"D15"];
	cell.watchList = @[[ssSens spreadSheetCellForIdentifier:@"D2"]
					   ,[ssTSPOT spreadSheetCellForIdentifier:@"B4"]];
	[cell setFunction:^NSNumber *(IMSpreadSheet *calculator, IMSpreadSheetCell *cell) {
		double val1 = [ssSens valueForIdentifier:@"D2"].doubleValue * [ssTSPOT valueForIdentifier:@"B4"].doubleValue;
		return [NSNumber numberWithDouble:val1];
	}];
	
	cell = [ssSens spreadSheetCellForIdentifier:@"E15"];
	cell.watchList = [ssSens arrayOfCells:@[@"E2", @"D17"]];
	[cell setFunction:^NSNumber *(IMSpreadSheet *calculator, IMSpreadSheetCell *cell) {
		return [ssSens subtractCell:@"D17" fromCell:@"E2"];
	}];
	
	cell = [ssSens spreadSheetCellForIdentifier:@"G15"];
	cell.watchList = [ssSens arrayOfCells:@[@"D15", @"B15"]];
	[cell setFunction:^NSNumber *(IMSpreadSheet *calculator, IMSpreadSheetCell *cell) {
		return [ssSens divideCells:@[@"D15", @"B15"]];
	}];
	
	cell = [ssSens spreadSheetCellForIdentifier:@"B17"];
	cell.watchList = [ssSens arrayOfCells:@[@"D17", @"E17"]];
	[cell setFunction:^NSNumber *(IMSpreadSheet *calculator, IMSpreadSheetCell *cell) {
		return [ssSens sumOfCells:@[@"D17", @"E17"]];
	}];
	
	cell = [ssSens spreadSheetCellForIdentifier:@"D17"];
	cell.watchList = @[[ssSens spreadSheetCellForIdentifier:@"E2"]
					   ,[ssTSPOT spreadSheetCellForIdentifier:@"B5"]];
	[cell setFunction:^NSNumber *(IMSpreadSheet *calculator, IMSpreadSheetCell *cell) {
		double val1 = [ssSens valueForIdentifier:@"E2"].doubleValue * [ssTSPOT valueForIdentifier:@"B5"].doubleValue;
		return [NSNumber numberWithDouble:val1];
	}];
	
	cell = [ssSens spreadSheetCellForIdentifier:@"E17"];
	cell.watchList = [ssSens arrayOfCells:@[@"D2", @"D15"]];
	[cell setFunction:^NSNumber *(IMSpreadSheet *calculator, IMSpreadSheetCell *cell) {
		return [ssSens subtractCell:@"D15" fromCell:@"D2"];
	}];
	
	cell = [ssSens spreadSheetCellForIdentifier:@"G17"];
	cell.watchList = [ssSens arrayOfCells:@[@"D17", @"B17"]];
	[cell setFunction:^NSNumber *(IMSpreadSheet *calculator, IMSpreadSheetCell *cell) {
		return [ssSens divideCells:@[@"D17", @"B17"]];
	}];
	
	cell = [ssSens spreadSheetCellForIdentifier:@"D20"];
	cell.watchList = [ssSens arrayOfCells:@[@"D15", @"D2"]];
	[cell setFunction:^NSNumber *(IMSpreadSheet *calculator, IMSpreadSheetCell *cell) {
		return [ssSens divideCells:@[@"D15", @"D2"]];
	}];
	
	cell = [ssSens spreadSheetCellForIdentifier:@"D21"];
	cell.watchList = [ssSens arrayOfCells:@[@"D17", @"E2"]];
	[cell setFunction:^NSNumber *(IMSpreadSheet *calculator, IMSpreadSheetCell *cell) {
		return [ssSens divideCells:@[@"D17", @"E2"]];
	}];
	
	[[ssSens spreadSheetCellForIdentifier:@"B26"] mapToCell:[ssSens spreadSheetCellForIdentifier:@"B5"]];
	[[ssSens spreadSheetCellForIdentifier:@"D26"] mapToCell:[ssSens spreadSheetCellForIdentifier:@"D5"]];
	cell = [ssSens spreadSheetCellForIdentifier:@"E26"];
	cell.watchList = [ssSens arrayOfCells:@[@"B26", @"D26"]];
	[cell setFunction:^NSNumber *(IMSpreadSheet *calculator, IMSpreadSheetCell *cell) {
		return [ssSens subtractCell:@"D26" fromCell:@"B26"];
	}];
	
	cell = [ssSens spreadSheetCellForIdentifier:@"G26"];
	cell.watchList = [ssSens arrayOfCells:@[@"D26", @"B26"]];
	[cell setFunction:^NSNumber *(IMSpreadSheet *calculator, IMSpreadSheetCell *cell) {
		return [ssSens divideCells:@[@"D26", @"B26"]];
	}];
	
	cell = [ssSens spreadSheetCellForIdentifier:@"B28"];
	cell.watchList = [ssSens arrayOfCells:@[@"D28", @"E28"]];
	[cell setFunction:^NSNumber *(IMSpreadSheet *calculator, IMSpreadSheetCell *cell) {
		return [ssSens sumOfCells:@[@"D28", @"E28"]];
	}];
	
	cell = [ssSens spreadSheetCellForIdentifier:@"D28"];
	cell.watchList = @[[ssSens spreadSheetCellForIdentifier:@"D26"]
					   ,[ssTSPOT spreadSheetCellForIdentifier:@"B4"]];
	[cell setFunction:^NSNumber *(IMSpreadSheet *calculator, IMSpreadSheetCell *cell) {
		double val1 = [ssSens valueForIdentifier:@"D26"].doubleValue * [ssTSPOT valueForIdentifier:@"B4"].doubleValue;
		return [NSNumber numberWithDouble:val1];
	}];
	
	cell = [ssSens spreadSheetCellForIdentifier:@"E28"];
	cell.watchList = [ssSens arrayOfCells:@[@"E26", @"D30"]];
	[cell setFunction:^NSNumber *(IMSpreadSheet *calculator, IMSpreadSheetCell *cell) {
		return [ssSens subtractCell:@"D30" fromCell:@"E26"];
	}];
	
	cell = [ssSens spreadSheetCellForIdentifier:@"G28"];
	cell.watchList = [ssSens arrayOfCells:@[@"D28", @"B28"]];
	[cell setFunction:^NSNumber *(IMSpreadSheet *calculator, IMSpreadSheetCell *cell) {
		return [ssSens divideCells:@[@"D28", @"B28"]];
	}];
	
	cell = [ssSens spreadSheetCellForIdentifier:@"B30"];
	cell.watchList = [ssSens arrayOfCells:@[@"D30", @"E30"]];
	[cell setFunction:^NSNumber *(IMSpreadSheet *calculator, IMSpreadSheetCell *cell) {
		return [ssSens sumOfCells:@[@"D30", @"E30"]];
	}];
	
	cell = [ssSens spreadSheetCellForIdentifier:@"D30"];
	cell.watchList = @[[ssSens spreadSheetCellForIdentifier:@"E26"]
					   ,[ssTSPOT spreadSheetCellForIdentifier:@"B5"]];
	[cell setFunction:^NSNumber *(IMSpreadSheet *calculator, IMSpreadSheetCell *cell) {
		double val1 = [ssSens valueForIdentifier:@"E26"].doubleValue * [ssTSPOT valueForIdentifier:@"B5"].doubleValue;
		return [NSNumber numberWithDouble:val1];
	}];
	
	cell = [ssSens spreadSheetCellForIdentifier:@"E30"];
	cell.watchList = [ssSens arrayOfCells:@[@"D26", @"D28"]];
	[cell setFunction:^NSNumber *(IMSpreadSheet *calculator, IMSpreadSheetCell *cell) {
		return  [ssSens subtractCell:@"D28" fromCell:@"D26"];
	}];
	
	cell = [ssSens spreadSheetCellForIdentifier:@"G30"];
	cell.watchList = [ssSens arrayOfCells:@[@"D30", @"B30"]];
	[cell setFunction:^NSNumber *(IMSpreadSheet *calculator, IMSpreadSheetCell *cell) {
		return [ssSens divideCells:@[@"D30", @"B30"]];
	}];
	
	cell = [ssSens spreadSheetCellForIdentifier:@"E32"];
	cell.watchList = [ssSens arrayOfCells:@[@"E30", @"E7"]];
	[cell setFunction:^NSNumber *(IMSpreadSheet *calculator, IMSpreadSheetCell *cell) {
		return [ssSens sumOfCells:@[@"E30", @"E7"]];
	}];
	
	cell = [ssSens spreadSheetCellForIdentifier:@"D34"];
	cell.watchList = [ssSens arrayOfCells:@[@"D28", @"D2"]];
	[cell setFunction:^NSNumber *(IMSpreadSheet *calculator, IMSpreadSheetCell *cell) {
		return [ssSens divideCells:@[@"D28", @"D2"]];
	}];
	
	cell = [ssSens spreadSheetCellForIdentifier:@"D35"];
	cell.watchList = [ssSens arrayOfCells:@[@"E28", @"E2"]];
	[cell setFunction:^NSNumber *(IMSpreadSheet *calculator, IMSpreadSheetCell *cell) {
		double val1 = 1.0 - [ssSens divideCells:@[@"E28", @"E2"]].doubleValue;
		return [NSNumber numberWithDouble:val1];
	}];
}

@end
