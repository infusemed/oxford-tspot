//
//  IMUKNewEntrantTSPOTvTST.h
//  Tspot Calculator
//
//  Created by Anthony Long on 4/18/15.
//  Copyright (c) 2015 Infuse Medical. All rights reserved.
//

#import "IMSpreadSheet.h"

@interface IMUKNewEntrantTSPOTvTST : IMWorkbook

extern NSString *const kIMUKNewEntrantTSPOTvTSTCurrentPractice;
extern NSString *const kIMUKNewEntrantTSPOTvTSTPageTSPOT;
extern NSString *const kIMUKNewEntrantTSPOTvTSTCharts;

@end
