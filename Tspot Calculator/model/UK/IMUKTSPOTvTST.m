//
//  IMUKOccupationalTSPOTvTST.m
//  Tspot Calculator
//
//  Created by Anthony Long on 4/7/15.
//  Copyright (c) 2015 Infuse Medical. All rights reserved.
//

#import "IMUKTSPOTvTST.h"

@implementation IMUKTSPOTvTST

NSString *const kIMUKOccupationalTSPOTvTSTCurrentPractice = @"CurrentPractice";
NSString *const kIMUKOccupationalTSPOTvTSTPageTSPOT = @"TSPOT";
NSString *const kIMUKOccupationalTSPOTvTSTCharts = @"ChartsAndTotals";


- (void) setCustomerName:(NSString *)customerName
{
	[super setCustomerName:customerName];
	[[self spreadSheetWithName:kIMUKOccupationalTSPOTvTSTCurrentPractice] spreadSheetCellForIdentifier:@"B30"].textValue = customerName;
}

- (NSString *) customerName
{
	return [[self spreadSheetWithName:kIMUKOccupationalTSPOTvTSTCurrentPractice] spreadSheetCellForIdentifier:@"B30"].textValue;
}

- (void) initializeWorkbook
{
	[super initializeWorkbook];
	[self setSpreadSheet:[[IMSpreadSheet alloc] init] WithName:kIMUKOccupationalTSPOTvTSTCurrentPractice];
	[self setSpreadSheet:[[IMSpreadSheet alloc] init] WithName:kIMUKOccupationalTSPOTvTSTPageTSPOT];
	[self setSpreadSheet:[[IMSpreadSheet alloc] init] WithName:kIMUKOccupationalTSPOTvTSTCharts];
	
	[self initCurrentPractice];
	[self initTSPOT];
	[self initCharts];
}

- (void) initCurrentPractice
{
	IMSpreadSheet *ssCurrent = [self spreadSheetWithName:kIMUKOccupationalTSPOTvTSTCurrentPractice];
	[ssCurrent spreadSheetCellForIdentifier:@"B2"];
	[ssCurrent spreadSheetCellForIdentifier:@"B5"].value = @17.00;
	[ssCurrent spreadSheetCellForIdentifier:@"B6"].value = @0.00;
	[ssCurrent spreadSheetCellForIdentifier:@"B7"].value = @0.00;
	[ssCurrent spreadSheetCellForIdentifier:@"B8"].value = @0.1;
	[ssCurrent spreadSheetCellForIdentifier:@"B8"].maxValue = @1.0;
	[ssCurrent spreadSheetCellForIdentifier:@"B10"].value = @0.64;
	[ssCurrent spreadSheetCellForIdentifier:@"B10"].maxValue = @1.0;
	[ssCurrent spreadSheetCellForIdentifier:@"B11"].value = @0.57;
	[ssCurrent spreadSheetCellForIdentifier:@"B11"].maxValue = @1.0;
	[ssCurrent spreadSheetCellForIdentifier:@"B14"].value = @0.2;
	[ssCurrent spreadSheetCellForIdentifier:@"B14"].maxValue = @1.0;
	[ssCurrent spreadSheetCellForIdentifier:@"B15"].value = @0.5;
	[ssCurrent spreadSheetCellForIdentifier:@"B15"].maxValue = @1.0;
	[ssCurrent spreadSheetCellForIdentifier:@"B16"].value = @35.0;
	[ssCurrent spreadSheetCellForIdentifier:@"B17"].value = @943.0;
	[ssCurrent spreadSheetCellForIdentifier:@"B18"].value = @0.1;
	[ssCurrent spreadSheetCellForIdentifier:@"B18"].maxValue = @1.0;
	[ssCurrent spreadSheetCellForIdentifier:@"B19"].value = @1399.0;
}

- (void) initTSPOT
{
	IMSpreadSheet *ssTSPOT = [self spreadSheetWithName:kIMUKOccupationalTSPOTvTSTPageTSPOT];
	[ssTSPOT spreadSheetCellForIdentifier:@"B2"];
	[ssTSPOT spreadSheetCellForIdentifier:@"B3"].value = @1.5;
	[ssTSPOT spreadSheetCellForIdentifier:@"B4"].value = @0.90;
	[ssTSPOT spreadSheetCellForIdentifier:@"B4"].maxValue = @1.0;
	[ssTSPOT spreadSheetCellForIdentifier:@"B5"].value = @0.77;
	[ssTSPOT spreadSheetCellForIdentifier:@"B5"].maxValue = @1.0;
}

- (void) initCharts
{
	IMSpreadSheet *ssCurrent = [self spreadSheetWithName:kIMUKOccupationalTSPOTvTSTCurrentPractice];
	IMSpreadSheet *ssTSPOT = [self spreadSheetWithName:kIMUKOccupationalTSPOTvTSTPageTSPOT];
	IMSpreadSheet *ssCharts = [self spreadSheetWithName:kIMUKOccupationalTSPOTvTSTCharts];
	IMSpreadSheetCell *cell;
	
	
	cell = [ssCharts spreadSheetCellForIdentifier:@"B2"];
	cell.watchList = [ssCurrent arrayOfCells:@[@"B2"]];
	[cell setFunction:^NSNumber *(IMSpreadSheet *calculator, IMSpreadSheetCell *cell) {
		return [ssCurrent valueForIdentifier:@"B2"];
	}];

	
	cell = [ssCharts spreadSheetCellForIdentifier:@"B3"];
	cell.watchList = [ssCurrent arrayOfCells:@[@"B2", @"B14"]];
	[cell setFunction:^NSNumber *(IMSpreadSheet *calculator, IMSpreadSheetCell *cell) {
		return [ssCurrent multiplyCells:@[@"B2", @"B14"]];
	}];
	
	[[ssCharts spreadSheetCellForIdentifier:@"C3"] mapToCell:[ssCharts spreadSheetCellForIdentifier:@"B3"]];
	
	cell = [ssCharts spreadSheetCellForIdentifier:@"E3"];
	cell.watchList = [ssCharts arrayOfCells:@[@"B3", @"C3"]];
	[cell setFunction:^NSNumber *(IMSpreadSheet *calculator, IMSpreadSheetCell *cell) {
		return [ssCharts subtractCell:@"C3" fromCell:@"B3"];
	}];
	
	cell = [ssCharts spreadSheetCellForIdentifier:@"B4"];
	cell.watchList = [ssCharts arrayOfCells:@[@"H6", @"H7"]];
	[cell setFunction:^NSNumber *(IMSpreadSheet *calculator, IMSpreadSheetCell *cell) {
		double val1 = [ssCharts valueForIdentifier:@"H6"].doubleValue +  [ssCharts valueForIdentifier:@"H7"].doubleValue;
		return [NSNumber numberWithDouble:val1];
	}];
	
	cell = [ssCharts spreadSheetCellForIdentifier:@"C4"];
	cell.watchList = [ssCharts arrayOfCells:@[@"H19", @"H20"]];
	[cell setFunction:^NSNumber *(IMSpreadSheet *calculator, IMSpreadSheetCell *cell) {
		double val1 = [ssCharts valueForIdentifier:@"H19"].doubleValue +  [ssCharts valueForIdentifier:@"H20"].doubleValue;
		return [NSNumber numberWithDouble:val1];
	}];
	
	cell = [ssCharts spreadSheetCellForIdentifier:@"E4"];
	cell.watchList = [ssCharts arrayOfCells:@[@"B4", @"C4"]];
	[cell setFunction:^NSNumber *(IMSpreadSheet *calculator, IMSpreadSheetCell *cell) {
		return [ssCharts subtractCell:@"C4" fromCell:@"B4"];
	}];
	
	
	cell = [ssCharts spreadSheetCellForIdentifier:@"B5"];
	cell.watchList = [ssCharts arrayOfCells:@[@"H3", @"H6"]];
	[cell setFunction:^NSNumber *(IMSpreadSheet *calculator, IMSpreadSheetCell *cell) {
		double val1 = [ssCharts valueForIdentifier:@"H3"].doubleValue - [ssCharts valueForIdentifier:@"H6"].doubleValue;
		return [NSNumber numberWithDouble:val1];
	}];
	
	cell = [ssCharts spreadSheetCellForIdentifier:@"C5"];
	cell.watchList = [ssCharts arrayOfCells:@[@"H3", @"H19"]];
	[cell setFunction:^NSNumber *(IMSpreadSheet *calculator, IMSpreadSheetCell *cell) {
		double val1 = [ssCharts valueForIdentifier:@"H3"].doubleValue - [ssCharts valueForIdentifier:@"H19"].doubleValue;
		return [NSNumber numberWithDouble:val1];
	}];
	
	cell = [ssCharts spreadSheetCellForIdentifier:@"E5"];
	cell.watchList = [ssCharts arrayOfCells:@[@"B5", @"C5"]];
	[cell setFunction:^NSNumber *(IMSpreadSheet *calculator, IMSpreadSheetCell *cell) {
		return [ssCharts subtractCell:@"C5" fromCell:@"B5"];
	}];
	
	// ************************************************
	
	cell = [ssCharts spreadSheetCellForIdentifier:@"H2"];
	cell.watchList = [ssCharts arrayOfCells:@[@"B2", @"B3"]];
	[cell setFunction:^NSNumber *(IMSpreadSheet *calculator, IMSpreadSheetCell *cell) {
		return [ssCharts subtractCell:@"B3" fromCell:@"B2"];
	}];
	
	[[ssCharts spreadSheetCellForIdentifier:@"H3"] mapToCell:[ssCharts spreadSheetCellForIdentifier:@"B3"]];
	
	cell = [ssCharts spreadSheetCellForIdentifier:@"H6"];
	cell.watchList = @[[ssCharts spreadSheetCellForIdentifier:@"H3"], [ssCurrent spreadSheetCellForIdentifier:@"B10"]];
	[cell setFunction:^NSNumber *(IMSpreadSheet *calculator, IMSpreadSheetCell *cell) {
		double val1 = [ssCurrent valueForIdentifier:@"B10"].doubleValue * [ssCharts valueForIdentifier:@"H3"].doubleValue;
		return [NSNumber numberWithDouble:val1];
	}];
	
	cell = [ssCharts spreadSheetCellForIdentifier:@"H8"];
	cell.watchList = @[[ssCharts spreadSheetCellForIdentifier:@"H2"], [ssCurrent spreadSheetCellForIdentifier:@"B11"]];
	[cell setFunction:^NSNumber *(IMSpreadSheet *calculator, IMSpreadSheetCell *cell) {
		double val1 = [ssCurrent valueForIdentifier:@"B11"].doubleValue * [ssCharts valueForIdentifier:@"H2"].doubleValue;
		return [NSNumber numberWithDouble:val1];
	}];
	
	cell = [ssCharts spreadSheetCellForIdentifier:@"H7"];
	cell.watchList = [ssCharts arrayOfCells:@[@"H2", @"H8"]];
	[cell setFunction:^NSNumber *(IMSpreadSheet *calculator, IMSpreadSheetCell *cell) {
		double val1 = [ssCharts valueForIdentifier:@"H2"].doubleValue - [ssCharts valueForIdentifier:@"H8"].doubleValue;
		return [NSNumber numberWithDouble:val1];
	}];
	
	cell = [ssCharts spreadSheetCellForIdentifier:@"H19"];
	cell.watchList = @[[ssCharts spreadSheetCellForIdentifier:@"H3"], [ssTSPOT spreadSheetCellForIdentifier:@"B4"]];
	[cell setFunction:^NSNumber *(IMSpreadSheet *calculator, IMSpreadSheetCell *cell) {
		double val1 = [ssTSPOT valueForIdentifier:@"B4"].doubleValue * [ssCharts valueForIdentifier:@"H3"].doubleValue;
		return [NSNumber numberWithDouble:val1];
	}];
	
	cell = [ssCharts spreadSheetCellForIdentifier:@"H21"];
	cell.watchList = @[[ssCharts spreadSheetCellForIdentifier:@"H2"], [ssTSPOT spreadSheetCellForIdentifier:@"B5"]];
	[cell setFunction:^NSNumber *(IMSpreadSheet *calculator, IMSpreadSheetCell *cell) {
		double val1 = [ssTSPOT valueForIdentifier:@"B5"].doubleValue * [ssCharts valueForIdentifier:@"H2"].doubleValue;
		return [NSNumber numberWithDouble:val1];
	}];
	
	cell = [ssCharts spreadSheetCellForIdentifier:@"H20"];
	cell.watchList = [ssCharts arrayOfCells:@[@"H2", @"H21"]];
	[cell setFunction:^NSNumber *(IMSpreadSheet *calculator, IMSpreadSheetCell *cell) {
		double val1 = [ssCharts valueForIdentifier:@"H2"].doubleValue - [ssCharts valueForIdentifier:@"H21"].doubleValue;
		return [NSNumber numberWithDouble:val1];
	}];
	
	// ************************************************
	
	cell = [ssCharts spreadSheetCellForIdentifier:@"B8"];
	cell.watchList = @[[ssCharts spreadSheetCellForIdentifier:@"B4"], [ssCurrent spreadSheetCellForIdentifier:@"B15"]];
	[cell setFunction:^NSNumber *(IMSpreadSheet *calculator, IMSpreadSheetCell *cell) {
		double val1 = [ssCharts valueForIdentifier:@"B4"].doubleValue * [ssCurrent valueForIdentifier:@"B15"].doubleValue;
		return [NSNumber numberWithDouble:val1];
	}];
	
	cell = [ssCharts spreadSheetCellForIdentifier:@"C8"];
	cell.watchList = @[[ssCharts spreadSheetCellForIdentifier:@"C4"], [ssCurrent spreadSheetCellForIdentifier:@"B15"]];
	[cell setFunction:^NSNumber *(IMSpreadSheet *calculator, IMSpreadSheetCell *cell) {
		double val1 = [ssCharts valueForIdentifier:@"C4"].doubleValue * [ssCurrent valueForIdentifier:@"B15"].doubleValue;
		return [NSNumber numberWithDouble:val1];
	}];
	
	cell = [ssCharts spreadSheetCellForIdentifier:@"B9"];
	cell.watchList = @[[ssCharts spreadSheetCellForIdentifier:@"B5"], [ssCurrent spreadSheetCellForIdentifier:@"B18"], [ssCharts spreadSheetCellForIdentifier:@"B3"], [ssCharts spreadSheetCellForIdentifier:@"H6"]];
	[cell setFunction:^NSNumber *(IMSpreadSheet *calculator, IMSpreadSheetCell *cell) {
		double val1 = ([ssCharts valueForIdentifier:@"B5"].doubleValue * [ssCurrent valueForIdentifier:@"B18"].doubleValue) + (([ssCharts valueForIdentifier:@"B3"].doubleValue - [ssCharts valueForIdentifier:@"H6"].doubleValue) * [ssCurrent valueForIdentifier:@"B18"].doubleValue);
		return [NSNumber numberWithDouble:val1];
	}];
	
	cell = [ssCharts spreadSheetCellForIdentifier:@"C9"];
	cell.watchList = @[[ssCharts spreadSheetCellForIdentifier:@"C5"], [ssCurrent spreadSheetCellForIdentifier:@"B18"], [ssCharts spreadSheetCellForIdentifier:@"C3"], [ssCharts spreadSheetCellForIdentifier:@"H19"]];
	[cell setFunction:^NSNumber *(IMSpreadSheet *calculator, IMSpreadSheetCell *cell) {
		double val1 = ([ssCharts valueForIdentifier:@"C5"].doubleValue * [ssCurrent valueForIdentifier:@"B18"].doubleValue) + (([ssCharts valueForIdentifier:@"C3"].doubleValue - [ssCharts valueForIdentifier:@"H19"].doubleValue) * [ssCurrent valueForIdentifier:@"B18"].doubleValue);
		return [NSNumber numberWithDouble:val1];
	}];
	
	cell = [ssCharts spreadSheetCellForIdentifier:@"B12"];
	cell.watchList = [ssCurrent arrayOfCells:@[@"B2", @"B5", @"B6", @"B7", @"B8"]];
	[cell setFunction:^NSNumber *(IMSpreadSheet *calculator, IMSpreadSheetCell *cell) {
		double val1 = [ssCurrent valueForIdentifier:@"B2"].doubleValue * [ssCurrent sumOfCells:@[@"B5", @"B6", @"B7"]].doubleValue * (1.0 - [ssCurrent valueForIdentifier:@"B8"].doubleValue);
		return [NSNumber numberWithDouble:val1];
	}];
	
	cell = [ssCharts spreadSheetCellForIdentifier:@"C12"];
	cell.watchList = @[[ssCurrent spreadSheetCellForIdentifier:@"B2"], [ssTSPOT spreadSheetCellForIdentifier:@"B2"], [ssTSPOT spreadSheetCellForIdentifier:@"B3"]];
	[cell setFunction:^NSNumber *(IMSpreadSheet *calculator, IMSpreadSheetCell *cell) {
		double val1 = [ssCurrent valueForIdentifier:@"B2"].doubleValue * ([ssTSPOT valueForIdentifier:@"B2"].doubleValue + [ssTSPOT valueForIdentifier:@"B3"].doubleValue);
		return [NSNumber numberWithDouble:val1];
	}];
	
	cell = [ssCharts spreadSheetCellForIdentifier:@"B13"];
	cell.watchList = @[[ssCharts spreadSheetCellForIdentifier:@"B4"], [ssTSPOT spreadSheetCellForIdentifier:@"B2"], [ssTSPOT spreadSheetCellForIdentifier:@"B3"]];
	[cell setFunction:^NSNumber *(IMSpreadSheet *calculator, IMSpreadSheetCell *cell) {
		double val1 = [ssCharts valueForIdentifier:@"B4"].doubleValue * ([ssTSPOT valueForIdentifier:@"B2"].doubleValue + [ssTSPOT valueForIdentifier:@"B3"].doubleValue);
		return [NSNumber numberWithDouble:val1];
	}];
	
	cell = [ssCharts spreadSheetCellForIdentifier:@"B14"];
	cell.watchList = @[[ssCharts spreadSheetCellForIdentifier:@"B8"], [ssCurrent spreadSheetCellForIdentifier:@"B17"]];
	[cell setFunction:^NSNumber *(IMSpreadSheet *calculator, IMSpreadSheetCell *cell) {
		double val1 = [ssCharts valueForIdentifier:@"B8"].doubleValue * [ssCurrent valueForIdentifier:@"B17"].doubleValue;
		return [NSNumber numberWithDouble:val1];
	}];
	
	cell = [ssCharts spreadSheetCellForIdentifier:@"C14"];
	cell.watchList = @[[ssCharts spreadSheetCellForIdentifier:@"C8"], [ssCurrent spreadSheetCellForIdentifier:@"B17"]];
	[cell setFunction:^NSNumber *(IMSpreadSheet *calculator, IMSpreadSheetCell *cell) {
		double val1 = [ssCharts valueForIdentifier:@"C8"].doubleValue * [ssCurrent valueForIdentifier:@"B17"].doubleValue;
		return [NSNumber numberWithDouble:val1];
	}];
	
	cell = [ssCharts spreadSheetCellForIdentifier:@"B15"];
	cell.watchList = @[[ssCharts spreadSheetCellForIdentifier:@"B9"], [ssCurrent spreadSheetCellForIdentifier:@"B19"]];
	[cell setFunction:^NSNumber *(IMSpreadSheet *calculator, IMSpreadSheetCell *cell) {
		double val1 = [ssCharts valueForIdentifier:@"B9"].doubleValue * [ssCurrent valueForIdentifier:@"B19"].doubleValue;
		return [NSNumber numberWithDouble:val1];
	}];
	
	cell = [ssCharts spreadSheetCellForIdentifier:@"C15"];
	cell.watchList = @[[ssCharts spreadSheetCellForIdentifier:@"C9"], [ssCurrent spreadSheetCellForIdentifier:@"B19"]];
	[cell setFunction:^NSNumber *(IMSpreadSheet *calculator, IMSpreadSheetCell *cell) {
		double val1 = [ssCharts valueForIdentifier:@"C9"].doubleValue * [ssCurrent valueForIdentifier:@"B19"].doubleValue;
		return [NSNumber numberWithDouble:val1];
	}];
	
	cell = [ssCharts spreadSheetCellForIdentifier:@"B16"];
	cell.watchList = [ssCharts arrayOfCells:@[@"B12", @"B13", @"B14", @"B15"]];
	[cell setFunction:^NSNumber *(IMSpreadSheet *calculator, IMSpreadSheetCell *cell) {
		return [ssCharts sumOfCells:@[@"B12", @"B14", @"B15"]];
	}];
	
	cell = [ssCharts spreadSheetCellForIdentifier:@"C16"];
	cell.watchList = [ssCharts arrayOfCells:@[@"C12", @"C13", @"C14", @"C15"]];
	[cell setFunction:^NSNumber *(IMSpreadSheet *calculator, IMSpreadSheetCell *cell) {
		return [ssCharts sumOfCells:@[@"C12", @"C13", @"C14", @"C15"]];
	}];
	
	cell = [ssCharts spreadSheetCellForIdentifier:@"E16"];
	cell.watchList = [ssCharts arrayOfCells:@[@"B17", @"C16"]];
	[cell setFunction:^NSNumber *(IMSpreadSheet *calculator, IMSpreadSheetCell *cell) {
		return [ssCharts subtractCell:@"C16" fromCell:@"B16"];
	}];
	
	cell = [ssCharts spreadSheetCellForIdentifier:@"B19"];
	cell.watchList = @[[ssCurrent spreadSheetCellForIdentifier:@"B2"]];
	[cell setFunction:^NSNumber *(IMSpreadSheet *calculator, IMSpreadSheetCell *cell) {
		double val = [ssCurrent valueForIdentifier:@"B2"].doubleValue * 2.0;
		return [NSNumber numberWithDouble:val];
	}];
	
	cell = [ssCharts spreadSheetCellForIdentifier:@"C19"];
	cell.watchList = @[[ssCurrent spreadSheetCellForIdentifier:@"B2"]];
	[cell setFunction:^NSNumber *(IMSpreadSheet *calculator, IMSpreadSheetCell *cell) {
		return [ssCurrent valueForIdentifier:@"B2"];
	}];
	
	cell = [ssCharts spreadSheetCellForIdentifier:@"E19"];
	cell.watchList = [ssCharts arrayOfCells:@[@"B19", @"C19"]];
	[cell setFunction:^NSNumber *(IMSpreadSheet *calculator, IMSpreadSheetCell *cell) {
		return [ssCharts subtractCell:@"C19" fromCell:@"B19"];
	}];
}

@end
