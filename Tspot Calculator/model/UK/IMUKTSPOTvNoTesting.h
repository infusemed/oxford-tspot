//
//  IMUKTSPOTvNoTesting.h
//  Tspot Calculator
//
//  Created by Anthony Long on 4/17/15.
//  Copyright (c) 2015 Infuse Medical. All rights reserved.
//

#import "IMSpreadSheet.h"

@interface IMUKTSPOTvNoTesting : IMWorkbook

extern NSString *const kIMUKTSPOTvNoTestingCurrentPractice;
extern NSString *const kIMUKTSPOTvNoTestingTSPOT;
extern NSString *const kIMUKTSPOTvNoTestingCharts;

@end
