//
//  IMTSPOTvQFTWorkbook.m
//  Tspot Calculator
//
//  Created by Anthony Long on 7/24/14.
//  Copyright (c) 2014 Infuse Medical. All rights reserved.
//

#import "IMTSPOTvQFTWorkbook.h"

@implementation IMTSPOTvQFTWorkbook

NSString * const kIMTvQInput = @"Input Tab";
NSString * const kIMTvQAnnualScreening = @"Annual TB Screening Program Tab";
NSString * const kIMTvQNewHireScreening = @"New Hire TB Screening Program Tab";
NSString * const kIMTvQTSPOT = @"T-SPOT";
NSString * const kIMTvQEmpCostData = @"Employee Cost Data";
NSString * const kIMTvQCharts = @"Charts and Totals";
NSString * const kIMTvQAnalysis = @"Analysis";
NSString * const kIMTvQEmpTimeData = @"Employee Time Reference Data";

//- (id) init
//{
//	self = [super init];
//	if(self)
//	{
//		[self initializeWorkbook];
//	}
//	
//	return self;
//}
//
//- (id) initWithFile:(NSString *)filePath
//{
//	self = [super init];
//	if(self)
//	{
//		self.fileName = filePath.lastPathComponent;
//		[self initializeWorkbook];
//		NSDictionary * d = [NSDictionary dictionaryWithContentsOfFile:filePath];
//		[d enumerateKeysAndObjectsUsingBlock:^(id key, id obj, BOOL *stop) {
//			NSDictionary * d2 = (NSDictionary *)obj;
//			IMSpreadSheet * spreadsheet = [self spreadSheetWithName:(NSString *)key];
//			[d2 enumerateKeysAndObjectsUsingBlock:^(id key, id obj, BOOL *stop) {
//				NSArray * a = [(NSString *)key componentsSeparatedByString:@"-"];
//				NSUInteger indexes[] = {[[a objectAtIndex:0] intValue], [[a objectAtIndex:1] intValue]};
//				if([obj isKindOfClass:[NSString class]])
//				{
//					[spreadsheet setTextValue:obj forIndexPath:[NSIndexPath indexPathWithIndexes:indexes length:2]];
//				}
//				else
//				{
//					[spreadsheet setValue:obj forIndexPath:[NSIndexPath indexPathWithIndexes:indexes length:2]];
//				}
//			}];
//		}];
//	}
//	
//	return self;
//}

- (void) setCustomerName:(NSString *)customerName
{
	[super setCustomerName:customerName];
	[[self spreadSheetWithName:kIMTvQAnnualScreening] spreadSheetCellForIdentifier:@"B3"].textValue = customerName;
}

- (NSString *) customerName
{
	return [[self spreadSheetWithName:kIMTvQAnnualScreening] spreadSheetCellForIdentifier:@"B3"].textValue;
}

- (void) initializeWorkbook
{
	[super initializeWorkbook];
	[self setSpreadSheet:[[IMSpreadSheet alloc] init]
				WithName:kIMTvQInput];
	[self setSpreadSheet:[[IMSpreadSheet alloc] init]
				WithName:kIMTvQAnnualScreening];
	[self setSpreadSheet:[[IMSpreadSheet alloc] init]
				WithName:kIMTvQNewHireScreening];
	[self setSpreadSheet:[[IMSpreadSheet alloc] init]
				WithName:kIMTvQTSPOT];
	[self setSpreadSheet:[[IMSpreadSheet alloc] init]
				WithName:kIMTvQEmpCostData];
	[self setSpreadSheet:[[IMSpreadSheet alloc] init]
				WithName:kIMTvQCharts];
	[self setSpreadSheet:[[IMSpreadSheet alloc] init]
				WithName:kIMTvQAnalysis];
	[self setSpreadSheet:[[IMSpreadSheet alloc] init]
				WithName:kIMTvQEmpTimeData];
	
	[self initInputTab];
	[self initScreeningTab];
	[self initNewHireScreening];
	[self initEmployeeCostData];
	[self initTSPOT];
	[self initEmployeeTimeReferenceData];
	[self initChartsAndTotals];
	[self initAnalysis];
}

- (void) initInputTab
{
	IMSpreadSheet *spreadSheet = [self spreadSheetWithName:kIMTvQInput];
	IMSpreadSheet *ssScreening = [self spreadSheetWithName:kIMTvQAnnualScreening];
	IMSpreadSheet *ssNewHire = [self spreadSheetWithName:kIMTvQNewHireScreening];
	IMSpreadSheet *ssTspot = [self spreadSheetWithName:kIMTvQTSPOT];
	IMSpreadSheetCell *cell = [spreadSheet spreadSheetCellForIdentifier:@"B3"];
	[cell setSetterFunction:^(IMSpreadSheetCell *cell) {
		[ssScreening spreadSheetCellForIdentifier:@"B5"].value = cell.value;
	}];
	cell = [spreadSheet spreadSheetCellForIdentifier:@"B5"];
	[cell setSetterFunction:^(IMSpreadSheetCell *cell) {
		[ssNewHire spreadSheetCellForIdentifier:@"B4"].value = cell.value;
	}];
	cell = [spreadSheet spreadSheetCellForIdentifier:@"B8"];
	[cell setSetterFunction:^(IMSpreadSheetCell *cell) {
		[ssScreening spreadSheetCellForIdentifier:@"B8"].value = cell.value;
	}];
	cell = [spreadSheet spreadSheetCellForIdentifier:@"D8"];
	[cell setSetterFunction:^(IMSpreadSheetCell *cell) {
		[ssTspot spreadSheetCellForIdentifier:@"B2"].value = cell.value;
	}];
	cell = [spreadSheet spreadSheetCellForIdentifier:@"B10"];
	[cell setSetterFunction:^(IMSpreadSheetCell *cell) {
		[ssScreening spreadSheetCellForIdentifier:@"B16"].value = cell.value;
	}];
	cell = [spreadSheet spreadSheetCellForIdentifier:@"D10"];
	[cell setSetterFunction:^(IMSpreadSheetCell *cell) {
		[ssTspot spreadSheetCellForIdentifier:@"B7"].value = cell.value;
	}];
	cell.value = @0.006;
}

- (void) initScreeningTab
{
	IMSpreadSheet *spreadSheet = [self spreadSheetWithName:kIMTvQAnnualScreening];
	IMSpreadSheet *ssNewHire = [self spreadSheetWithName:kIMTvQNewHireScreening];
	IMSpreadSheetCell *cell;
	[spreadSheet spreadSheetCellForIdentifier:@"B3"];
	[spreadSheet spreadSheetCellForIdentifier:@"B5"];
	cell = [spreadSheet spreadSheetCellForIdentifier:@"B8"];
	[cell setSetterFunction:^(IMSpreadSheetCell *cell) {
		[ssNewHire spreadSheetCellForIdentifier:@"B8"].value = cell.value;
	}];
	cell = [spreadSheet spreadSheetCellForIdentifier:@"B13"];
	[cell setSetterFunction:^(IMSpreadSheetCell *cell) {
		[ssNewHire spreadSheetCellForIdentifier:@"B12"].value = cell.value;
	}];
	[spreadSheet spreadSheetCellForIdentifier:@"B14"];
	cell = [spreadSheet spreadSheetCellForIdentifier:@"B16"];
	[cell setSetterFunction:^(IMSpreadSheetCell *cell) {
		[ssNewHire spreadSheetCellForIdentifier:@"B15"].value = cell.value;
	}];
}

- (void) initNewHireScreening
{
	IMSpreadSheet *spreadSheet = [self spreadSheetWithName:kIMTvQNewHireScreening];
	IMSpreadSheetCell * cell = [spreadSheet spreadSheetCellForIdentifier:@"B4"];
	
	cell = [spreadSheet spreadSheetCellForIdentifier:@"B8"];
	
	cell = [spreadSheet spreadSheetCellForIdentifier:@"B12"];
	
	cell = [spreadSheet spreadSheetCellForIdentifier:@"B15"];
}

- (void) initTSPOT
{
	IMSpreadSheet *spreadSheet = [self spreadSheetWithName:kIMTvQTSPOT];
	[spreadSheet spreadSheetCellForIdentifier:@"B2"];
	[spreadSheet spreadSheetCellForIdentifier:@"B5"].value = @0.011;
	[spreadSheet spreadSheetCellForIdentifier:@"B7"];
}

- (void) initEmployeeCostData
{
	IMSpreadSheet * spreadSheet = [self spreadSheetWithName:kIMTvQEmpCostData];
	
	IMSpreadSheetCell * cell = [spreadSheet spreadSheetCellForIdentifier:@"B8"];
	cell.value = @54.44;
	cell = [spreadSheet spreadSheetCellForIdentifier:@"F8"];
	cell.value = @54.44;
	cell = [spreadSheet spreadSheetCellForIdentifier:@"B12"];
	cell.value = @110.52;
	cell = [spreadSheet spreadSheetCellForIdentifier:@"C12"];
	cell.value = @0.159;
	cell = [spreadSheet spreadSheetCellForIdentifier:@"D12"];
	cell.watchList = [spreadSheet arrayOfCells:@[@"C12", @"B12"]];
	[cell setFunction:^NSNumber *(IMSpreadSheet *calculator, IMSpreadSheetCell *cell) {
		return [calculator multiplyCells:@[@"C12", @"B12"]];
	}];
	
	cell = [spreadSheet spreadSheetCellForIdentifier:@"B13"];
	cell.value = @34.65;
	cell = [spreadSheet spreadSheetCellForIdentifier:@"C13"];
	cell.value = @0.363;
	cell = [spreadSheet spreadSheetCellForIdentifier:@"D13"];
	cell.watchList = [spreadSheet arrayOfCells:@[@"B13", @"C13"]];
	[cell setFunction:^NSNumber *(IMSpreadSheet *calculator, IMSpreadSheetCell *cell) {
		return [spreadSheet multiplyCells:@[@"B13", @"C13"]];
	}];
	
	cell = [spreadSheet spreadSheetCellForIdentifier:@"B14"];
	cell.value = @139.04;
	cell = [spreadSheet spreadSheetCellForIdentifier:@"C14"];
	cell.value = @0.063;
	cell = [spreadSheet spreadSheetCellForIdentifier:@"D14"];
	cell.watchList = [spreadSheet arrayOfCells:@[@"B14", @"C14"]];
	[cell setFunction:^NSNumber *(IMSpreadSheet *calculator, IMSpreadSheetCell *cell) {
		return [calculator multiplyCells:@[@"B14", @"C14"]];
	}];
	
	cell = [spreadSheet spreadSheetCellForIdentifier:@"B15"];
	cell.value = @26.9;
	cell = [spreadSheet spreadSheetCellForIdentifier:@"C15"];
	cell.value = @0.225;
	cell = [spreadSheet spreadSheetCellForIdentifier:@"D15"];
	cell.watchList = [spreadSheet arrayOfCells:@[@"B15", @"C15"]];
	[cell setFunction:^NSNumber *(IMSpreadSheet *calculator, IMSpreadSheetCell *cell) {
		return [calculator multiplyCells:@[@"B15", @"C15"]];
	}];
	
	cell = [spreadSheet spreadSheetCellForIdentifier:@"B16"];
	cell.value = @23.27;
	cell = [spreadSheet spreadSheetCellForIdentifier:@"C16"];
	cell.value = @0.19;
	cell = [spreadSheet spreadSheetCellForIdentifier:@"D16"];
	cell.watchList = [spreadSheet arrayOfCells:@[@"B16", @"C16"]];
	[cell setFunction:^NSNumber *(IMSpreadSheet *calculator, IMSpreadSheetCell *cell) {
		return [calculator multiplyCells:@[@"B16", @"C16"]];
	}];
	
	cell = [spreadSheet spreadSheetCellForIdentifier:@"F12"];
	cell.value = @110.52;
	cell = [spreadSheet spreadSheetCellForIdentifier:@"G12"];
	cell.value = @0.159;
	cell = [spreadSheet spreadSheetCellForIdentifier:@"H12"];
	cell.watchList = [spreadSheet arrayOfCells:@[@"G12", @"F12"]];
	[cell setFunction:^NSNumber *(IMSpreadSheet *calculator, IMSpreadSheetCell *cell) {
		return [calculator multiplyCells:@[@"G12", @"F12"]];
	}];
	
	cell = [spreadSheet spreadSheetCellForIdentifier:@"F13"];
	cell.value = @34.65;
	cell = [spreadSheet spreadSheetCellForIdentifier:@"G13"];
	cell.value = @0.363;
	cell = [spreadSheet spreadSheetCellForIdentifier:@"H13"];
	cell.watchList = [spreadSheet arrayOfCells:@[@"F13", @"G13"]];
	[cell setFunction:^NSNumber *(IMSpreadSheet *calculator, IMSpreadSheetCell *cell) {
		return [spreadSheet multiplyCells:@[@"F13", @"G13"]];
	}];
	
	cell = [spreadSheet spreadSheetCellForIdentifier:@"F14"];
	cell.value = @139.04;
	cell = [spreadSheet spreadSheetCellForIdentifier:@"G14"];
	cell.value = @0.063;
	cell = [spreadSheet spreadSheetCellForIdentifier:@"H14"];
	cell.watchList = [spreadSheet arrayOfCells:@[@"F14", @"G14"]];
	[cell setFunction:^NSNumber *(IMSpreadSheet *calculator, IMSpreadSheetCell *cell) {
		return [calculator multiplyCells:@[@"F14", @"G14"]];
	}];
	
	cell = [spreadSheet spreadSheetCellForIdentifier:@"F15"];
	cell.value = @26.9;
	cell = [spreadSheet spreadSheetCellForIdentifier:@"G15"];
	cell.value = @0.225;
	cell = [spreadSheet spreadSheetCellForIdentifier:@"H15"];
	cell.watchList = [spreadSheet arrayOfCells:@[@"F15", @"G15"]];
	[cell setFunction:^NSNumber *(IMSpreadSheet *calculator, IMSpreadSheetCell *cell) {
		return [calculator multiplyCells:@[@"F15", @"G15"]];
	}];
	
	cell = [spreadSheet spreadSheetCellForIdentifier:@"F16"];
	cell.value = @23.27;
	cell = [spreadSheet spreadSheetCellForIdentifier:@"G16"];
	cell.value = @0.19;
	cell = [spreadSheet spreadSheetCellForIdentifier:@"H16"];
	cell.watchList = [spreadSheet arrayOfCells:@[@"F16", @"G16"]];
	[cell setFunction:^NSNumber *(IMSpreadSheet *calculator, IMSpreadSheetCell *cell) {
		return [calculator multiplyCells:@[@"F16", @"G16"]];
	}];
	
	cell = [spreadSheet spreadSheetCellForIdentifier:@"C18"];
	cell.watchList = [spreadSheet arrayOfCells:@[@"C12", @"C13", @"C14", @"C15", @"C16"]];
	[cell setFunction:^NSNumber *(IMSpreadSheet *calculator, IMSpreadSheetCell *cell) {
		return [calculator sumOfCellRange:@"C12:C16"];
	}];
	
	cell = [spreadSheet spreadSheetCellForIdentifier:@"C19"];
	cell.watchList = [spreadSheet arrayOfCells:@[@"D12", @"D13", @"D14", @"D15", @"D16"]];
	[cell setFunction:^NSNumber *(IMSpreadSheet *calculator, IMSpreadSheetCell *cell) {
		return [calculator sumOfCellRange:@"D12:D16"];
	}];
	
	
	cell = [spreadSheet spreadSheetCellForIdentifier:@"G18"];
	cell.watchList = [spreadSheet arrayOfCells:@[@"G12", @"G13", @"G14", @"G15", @"G16"]];
	[cell setFunction:^NSNumber *(IMSpreadSheet *calculator, IMSpreadSheetCell *cell) {
		return [calculator sumOfCellRange:@"G12:G16"];
	}];
	
	cell = [spreadSheet spreadSheetCellForIdentifier:@"G19"];
	cell.watchList = [spreadSheet arrayOfCells:@[@"H12", @"H13", @"H14", @"H15", @"H16"]];
	[cell setFunction:^NSNumber *(IMSpreadSheet *calculator, IMSpreadSheetCell *cell) {
		return [calculator sumOfCellRange:@"H12:H16"];
	}];
	
	[spreadSheet spreadSheetCellForIdentifier:@"B22"].value = @1.0;
}

- (void) initChartsAndTotals
{
	IMSpreadSheet *spreadSheet = [self spreadSheetWithName:kIMTvQCharts];
	IMSpreadSheet *ssScreening = [self spreadSheetWithName:kIMTvQAnnualScreening];
	IMSpreadSheet *ssNewHire = [self spreadSheetWithName:kIMTvQNewHireScreening];
	IMSpreadSheet *ssAnalysis = [self spreadSheetWithName:kIMTvQAnalysis];
	IMSpreadSheet *ssEmpCost = [self spreadSheetWithName:kIMTvQEmpCostData];
	IMSpreadSheet *ssTspot = [self spreadSheetWithName:kIMTvQTSPOT];
	
	IMSpreadSheetCell *cell = [spreadSheet spreadSheetCellForIdentifier:@"B2"];
	cell.watchList = @[[ssScreening spreadSheetCellForIdentifier:@"B5"]
					   ,[ssScreening spreadSheetCellForIdentifier:@"B8"]
					   ,[ssNewHire spreadSheetCellForIdentifier:@"B8"]
					   ,[ssNewHire spreadSheetCellForIdentifier:@"B4"]
					   ,[spreadSheet spreadSheetCellForIdentifier:@"B28"]
					   ,[ssAnalysis spreadSheetCellForIdentifier:@"B43"]];
	[cell setFunction:^NSNumber *(IMSpreadSheet *calculator, IMSpreadSheetCell *cell) {
		double val1 = [ssScreening valueForIdentifier:@"B5"].doubleValue * [ssScreening valueForIdentifier:@"B8"].doubleValue;
		double val2 = [ssNewHire valueForIdentifier:@"B8"].doubleValue * [ssNewHire valueForIdentifier:@"B4"].doubleValue;
		double val3 = val1 + val2 + [calculator valueForIdentifier:@"B28"].doubleValue + [ssAnalysis valueForIdentifier:@"B43"].doubleValue;
		return [NSNumber numberWithDouble:val3];
	}];
	
	cell = [spreadSheet spreadSheetCellForIdentifier:@"C2"];
	cell.watchList = @[[ssTspot spreadSheetCellForIdentifier:@"B2"]
					   ,[ssScreening spreadSheetCellForIdentifier:@"B5"]
					   ,[ssNewHire spreadSheetCellForIdentifier:@"B8"]
					   ,[ssNewHire spreadSheetCellForIdentifier:@"B4"]
					   ,[ssAnalysis spreadSheetCellForIdentifier:@"C43"]
					   ,[ssAnalysis spreadSheetCellForIdentifier:@"B30"]
					   ,[ssTspot spreadSheetCellForIdentifier:@"B2"]];
	[cell setFunction:^NSNumber *(IMSpreadSheet *calculator, IMSpreadSheetCell *cell) {
		double val1 = [ssTspot valueForIdentifier:@"B2"].doubleValue * [ssScreening valueForIdentifier:@"B5"].doubleValue;
		double val2 = [ssNewHire valueForIdentifier:@"B8"].doubleValue * [ssNewHire valueForIdentifier:@"B4"].doubleValue;
		double val3 = [ssAnalysis valueForIdentifier:@"C43"].doubleValue;
		double val4 = [ssAnalysis valueForIdentifier:@"B30"].doubleValue * [ssTspot valueForIdentifier:@"B2"].doubleValue;
		double val5 = val1 + val2 + val3 - val4;
		return [NSNumber numberWithDouble:val5];
	}];
	
	cell = [spreadSheet spreadSheetCellForIdentifier:@"B3"];
	cell.watchList = @[[spreadSheet spreadSheetCellForIdentifier:@"B10"]
					   ,[ssEmpCost spreadSheetCellForIdentifier:@"B8"]];
	[cell setFunction:^NSNumber *(IMSpreadSheet *calculator, IMSpreadSheetCell *cell) {
		double val1 = [spreadSheet valueForIdentifier:@"B10"].doubleValue * [ssEmpCost valueForIdentifier:@"B8"].doubleValue;
		return [NSNumber numberWithDouble:val1];
	}];
	
	cell = [spreadSheet spreadSheetCellForIdentifier:@"C3"];
	cell.watchList = @[[spreadSheet spreadSheetCellForIdentifier:@"C10"]
					   ,[ssEmpCost spreadSheetCellForIdentifier:@"B8"]];
	[cell setFunction:^NSNumber *(IMSpreadSheet *calculator, IMSpreadSheetCell *cell) {
		double val1 = [spreadSheet valueForIdentifier:@"C10"].doubleValue * [ssEmpCost valueForIdentifier:@"B8"].doubleValue;
		return [NSNumber numberWithDouble:val1];
	}];
	
	cell = [spreadSheet spreadSheetCellForIdentifier:@"B4"];
	cell.watchList = @[[spreadSheet spreadSheetCellForIdentifier:@"B11"]
					   ,[ssEmpCost spreadSheetCellForIdentifier:@"C19"]];
	[cell setFunction:^NSNumber *(IMSpreadSheet *calculator, IMSpreadSheetCell *cell) {
		double val1 = [spreadSheet valueForIdentifier:@"B11"].doubleValue * [ssEmpCost valueForIdentifier:@"C19"].doubleValue;
		return [NSNumber numberWithDouble:val1];
	}];
	
	cell = [spreadSheet spreadSheetCellForIdentifier:@"C4"];
	cell.watchList = @[[spreadSheet spreadSheetCellForIdentifier:@"C11"]
					   ,[ssEmpCost spreadSheetCellForIdentifier:@"C19"]];
	[cell setFunction:^NSNumber *(IMSpreadSheet *calculator, IMSpreadSheetCell *cell) {
		double val1 = [spreadSheet valueForIdentifier:@"C11"].doubleValue * [ssEmpCost valueForIdentifier:@"C19"].doubleValue;
		return [NSNumber numberWithDouble:val1];
	}];
	
	cell = [spreadSheet spreadSheetCellForIdentifier:@"B6"];
	cell.watchList = [spreadSheet arrayOfCells:@[@"B2", @"B3", @"B4"]];
	[cell setFunction:^NSNumber *(IMSpreadSheet *calculator, IMSpreadSheetCell *cell) {
		return [calculator sumOfCellRange:@"B2:B4"];
	}];
	
	cell = [spreadSheet spreadSheetCellForIdentifier:@"C6"];
	cell.watchList = [spreadSheet arrayOfCells:@[@"C2", @"C3", @"C4"]];
	[cell setFunction:^NSNumber *(IMSpreadSheet *calculator, IMSpreadSheetCell *cell) {
		return [calculator sumOfCellRange:@"C2:C4"];
	}];
	
	cell = [spreadSheet spreadSheetCellForIdentifier:@"D6"];
	cell.watchList = [spreadSheet arrayOfCells:@[@"B6", @"C6"]];
	[cell setFunction:^NSNumber *(IMSpreadSheet *calculator, IMSpreadSheetCell *cell) {
		double val1 = [spreadSheet valueForIdentifier:@"B6"].doubleValue - [spreadSheet valueForIdentifier:@"C6"].doubleValue;
		return [NSNumber numberWithDouble:floor(val1)];
	}];
	
	cell = [spreadSheet spreadSheetCellForIdentifier:@"E6"];
	cell.watchList = [spreadSheet arrayOfCells:@[@"B6", @"C6"]];
	[cell setFunction:^NSNumber *(IMSpreadSheet *calculator, IMSpreadSheetCell *cell) {
		double val1 = 1.0 - ([spreadSheet valueForIdentifier:@"C6"].doubleValue / [spreadSheet valueForIdentifier:@"B6"].doubleValue);
		return [NSNumber numberWithDouble:val1];
	}];
	
	cell = [spreadSheet spreadSheetCellForIdentifier:@"B10"];
	cell.watchList = @[[ssScreening spreadSheetCellForIdentifier:@"B5"]
					   ,[ssNewHire spreadSheetCellForIdentifier:@"B4"]
					   ,[ssAnalysis spreadSheetCellForIdentifier:@"B18"]
					   ,[ssAnalysis spreadSheetCellForIdentifier:@"B26"]
					   ,[ssAnalysis spreadSheetCellForIdentifier:@"B16"]];
	[cell setFunction:^NSNumber *(IMSpreadSheet *calculator, IMSpreadSheetCell *cell) {
		double val1 = [ssScreening valueForIdentifier:@"B5"].doubleValue + [ssNewHire valueForIdentifier:@"B4"].doubleValue;
		double val2 = (val1 * [ssAnalysis valueForIdentifier:@"B18"].doubleValue) / 60.0;
		double val3 = ([ssAnalysis valueForIdentifier:@"B26"].doubleValue * [ssAnalysis valueForIdentifier:@"B16"].doubleValue) / 60.0;
		double val4 = val2 + val3;
		return [NSNumber numberWithDouble:val4];
	}];
	
	cell = [spreadSheet spreadSheetCellForIdentifier:@"C10"];
	cell.watchList = @[[ssScreening spreadSheetCellForIdentifier:@"B5"]
					   ,[ssNewHire spreadSheetCellForIdentifier:@"B4"]
					   ,[ssAnalysis spreadSheetCellForIdentifier:@"C18"]];
	[cell setFunction:^NSNumber *(IMSpreadSheet *calculator, IMSpreadSheetCell *cell) {
		double val1 = (([ssScreening valueForIdentifier:@"B5"].doubleValue + [ssNewHire valueForIdentifier:@"B4"].doubleValue) * [ssAnalysis valueForIdentifier:@"C18"].doubleValue) / 60.0;
		return [NSNumber numberWithDouble:val1];
	}];
	
	cell = [spreadSheet spreadSheetCellForIdentifier:@"B11"];
	cell.watchList = @[[ssAnalysis spreadSheetCellForIdentifier:@"B18"]
					   ,[ssScreening spreadSheetCellForIdentifier:@"B5"]
					   ,[ssNewHire spreadSheetCellForIdentifier:@"B4"]
					   ,[ssAnalysis spreadSheetCellForIdentifier:@"B26"]];
	[cell setFunction:^NSNumber *(IMSpreadSheet *calculator, IMSpreadSheetCell *cell) {
		double val1 = [ssAnalysis valueForIdentifier:@"B18"].doubleValue * [ssScreening valueForIdentifier:@"B5"].doubleValue;
		double val2 = [ssAnalysis valueForIdentifier:@"B18"].doubleValue * [ssNewHire valueForIdentifier:@"B4"].doubleValue;
		double val3 = [ssAnalysis valueForIdentifier:@"B26"].doubleValue * [ssAnalysis valueForIdentifier:@"B18"].doubleValue;
		double val4 = (val1 + val2 + val3) / 60.0;
		return [NSNumber numberWithDouble:val4];
	}];
	
	cell = [spreadSheet spreadSheetCellForIdentifier:@"C11"];
	cell.watchList = @[[ssAnalysis spreadSheetCellForIdentifier:@"B18"]
					   ,[ssScreening spreadSheetCellForIdentifier:@"B5"]
					   ,[ssNewHire spreadSheetCellForIdentifier:@"B4"]
					   ,[ssAnalysis spreadSheetCellForIdentifier:@"B30"]];
	[cell setFunction:^NSNumber *(IMSpreadSheet *calculator, IMSpreadSheetCell *cell) {
		double val1 = [ssAnalysis valueForIdentifier:@"B18"].doubleValue * [ssScreening valueForIdentifier:@"B5"].doubleValue;
		double val2 = [ssAnalysis valueForIdentifier:@"B18"].doubleValue * [ssNewHire valueForIdentifier:@"B4"].doubleValue;
		double val3 = [ssAnalysis valueForIdentifier:@"B30"].doubleValue * [ssAnalysis valueForIdentifier:@"B18"].doubleValue;
		double val4 = (val1 + val2 + val3) / 60.0;
		return [NSNumber numberWithDouble:val4];
	}];
	
	cell = [spreadSheet spreadSheetCellForIdentifier:@"B12"];
	cell.watchList = [spreadSheet arrayOfCells:@[@"B10", @"B11"]];
	[cell setFunction:^NSNumber *(IMSpreadSheet *calculator, IMSpreadSheetCell *cell) {
		return [spreadSheet sumOfCells:@[@"B10", @"B11"]];
	}];
	
	cell = [spreadSheet spreadSheetCellForIdentifier:@"C12"];
	cell.watchList = [spreadSheet arrayOfCells:@[@"C10", @"C11"]];
	[cell setFunction:^NSNumber *(IMSpreadSheet *calculator, IMSpreadSheetCell *cell) {
		return [spreadSheet sumOfCells:@[@"C10", @"C11"]];
	}];
	
	cell = [spreadSheet spreadSheetCellForIdentifier:@"D12"];
	cell.watchList = [spreadSheet arrayOfCells:@[@"B12", @"C12"]];
	[cell setFunction:^NSNumber *(IMSpreadSheet *calculator, IMSpreadSheetCell *cell) {
		double val1 = round([spreadSheet valueForIdentifier:@"B12"].doubleValue) - round([spreadSheet valueForIdentifier:@"C12"].doubleValue);
		return [NSNumber numberWithDouble:floor(val1)];
	}];
	
	cell = [spreadSheet spreadSheetCellForIdentifier:@"E12"];
	cell.watchList = [spreadSheet arrayOfCells:@[@"B12", @"C12"]];
	[cell setFunction:^NSNumber *(IMSpreadSheet *calculator, IMSpreadSheetCell *cell) {
		double val1 = 1.0 - ([spreadSheet valueForIdentifier:@"C12"].doubleValue / [spreadSheet valueForIdentifier:@"B12"].doubleValue);
		return [NSNumber numberWithDouble:val1];
	}];
	
	cell = [spreadSheet spreadSheetCellForIdentifier:@"B22"];
	cell.watchList = @[[ssAnalysis spreadSheetCellForIdentifier:@"B26"]];
	[cell setFunction:^NSNumber *(IMSpreadSheet *calculator, IMSpreadSheetCell *cell) {
		return [ssAnalysis valueForIdentifier:@"B26"];
	}];
	
	cell = [spreadSheet spreadSheetCellForIdentifier:@"C22"];
	cell.watchList = @[[ssAnalysis spreadSheetCellForIdentifier:@"B30"]];
	[cell setFunction:^NSNumber *(IMSpreadSheet *calculator, IMSpreadSheetCell *cell) {
		return [ssAnalysis valueForIdentifier:@"B30"];
	}];
	
	cell = [spreadSheet spreadSheetCellForIdentifier:@"D22"];
	cell.watchList = [spreadSheet arrayOfCells:@[@"B22", @"C22"]];
	[cell setFunction:^NSNumber *(IMSpreadSheet *calculator, IMSpreadSheetCell *cell) {
		double val1 = [spreadSheet valueForIdentifier:@"B22"].doubleValue - [spreadSheet valueForIdentifier:@"C22"].doubleValue;
		return [NSNumber numberWithDouble:floor(val1)];
	}];
	
	cell = [spreadSheet spreadSheetCellForIdentifier:@"E22"];
	cell.watchList = [spreadSheet arrayOfCells:@[@"B22", @"C22"]];
	[cell setFunction:^NSNumber *(IMSpreadSheet *calculator, IMSpreadSheetCell *cell) {
		double val1 = 1.0 - ([spreadSheet valueForIdentifier:@"C22"].doubleValue / [spreadSheet valueForIdentifier:@"B22"].doubleValue);
		return [NSNumber numberWithDouble:val1];
	}];
	
	cell = [spreadSheet spreadSheetCellForIdentifier:@"B28"];
	cell.watchList = @[[ssAnalysis spreadSheetCellForIdentifier:@"B27"]];
	[cell setFunction:^NSNumber *(IMSpreadSheet *calculator, IMSpreadSheetCell *cell) {
		return [ssAnalysis valueForIdentifier:@"B27"];
	}];
	
	cell = [spreadSheet spreadSheetCellForIdentifier:@"C28"];
	cell.watchList = @[[ssAnalysis spreadSheetCellForIdentifier:@"B31"]];
	[cell setFunction:^NSNumber *(IMSpreadSheet *calculator, IMSpreadSheetCell *cell) {
		return [ssAnalysis valueForIdentifier:@"B31"];
	}];
	
	cell = [spreadSheet spreadSheetCellForIdentifier:@"B29"];
	cell.watchList = @[[ssAnalysis spreadSheetCellForIdentifier:@"B29"]];
	[cell setFunction:^NSNumber *(IMSpreadSheet *calculator, IMSpreadSheetCell *cell) {
		return [ssAnalysis valueForIdentifier:@"B29"];
	}];
	
	cell = [spreadSheet spreadSheetCellForIdentifier:@"C29"];
	cell.watchList = @[[ssAnalysis spreadSheetCellForIdentifier:@"B33"]];
	[cell setFunction:^NSNumber *(IMSpreadSheet *calculator, IMSpreadSheetCell *cell) {
		return [ssAnalysis valueForIdentifier:@"B33"];
	}];
	
	cell = [spreadSheet spreadSheetCellForIdentifier:@"B30"];
	cell.watchList = @[[ssAnalysis spreadSheetCellForIdentifier:@"B28"]];
	[cell setFunction:^NSNumber *(IMSpreadSheet *calculator, IMSpreadSheetCell *cell) {
		return [ssAnalysis valueForIdentifier:@"B28"];
	}];
	
	cell = [spreadSheet spreadSheetCellForIdentifier:@"C30"];
	cell.watchList = @[[ssAnalysis spreadSheetCellForIdentifier:@"B32"]];
	[cell setFunction:^NSNumber *(IMSpreadSheet *calculator, IMSpreadSheetCell *cell) {
		return [ssAnalysis valueForIdentifier:@"B32"];
	}];
	
	cell = [spreadSheet spreadSheetCellForIdentifier:@"B31"];
	cell.watchList = [spreadSheet arrayOfCells:@[@"B28", @"B29", @"B30"]];
	[cell setFunction:^NSNumber *(IMSpreadSheet *calculator, IMSpreadSheetCell *cell) {
		return [spreadSheet sumOfCellRange:@"B28:B30"];
	}];
	
	cell = [spreadSheet spreadSheetCellForIdentifier:@"C31"];
	cell.watchList = [spreadSheet arrayOfCells:@[@"C28", @"C29", @"C30"]];
	[cell setFunction:^NSNumber *(IMSpreadSheet *calculator, IMSpreadSheetCell *cell) {
		return [spreadSheet sumOfCellRange:@"C28:C30"];
	}];
	
	cell = [spreadSheet spreadSheetCellForIdentifier:@"D31"];
	cell.watchList = [spreadSheet arrayOfCells:@[@"B31", @"C31"]];
	[cell setFunction:^NSNumber *(IMSpreadSheet *calculator, IMSpreadSheetCell *cell) {
		double val1 = [spreadSheet valueForIdentifier:@"B31"].doubleValue - [spreadSheet valueForIdentifier:@"C31"].doubleValue;
		return [NSNumber numberWithDouble:floor(val1)];
	}];
	
	cell = [spreadSheet spreadSheetCellForIdentifier:@"E31"];
	cell.watchList = [spreadSheet arrayOfCells:@[@"B31", @"C31"]];
	[cell setFunction:^NSNumber *(IMSpreadSheet *calculator, IMSpreadSheetCell *cell) {
		double val1 = 1.0 - ([spreadSheet valueForIdentifier:@"C31"].doubleValue / [spreadSheet valueForIdentifier:@"B31"].doubleValue);
		return [NSNumber numberWithDouble:val1];
	}];
	
}

- (void) initAnalysis
{
	IMSpreadSheet *spreadSheet = [self spreadSheetWithName:kIMTvQAnalysis];
	IMSpreadSheet *ssScreening = [self spreadSheetWithName:kIMTvQAnnualScreening];
	IMSpreadSheet *ssNewHire = [self spreadSheetWithName:kIMTvQNewHireScreening];
	IMSpreadSheet *ssAnalysis = [self spreadSheetWithName:kIMTvQAnalysis];
	IMSpreadSheet *ssEmpCost = [self spreadSheetWithName:kIMTvQEmpCostData];
	IMSpreadSheet *ssTspot = [self spreadSheetWithName:kIMTvQTSPOT];
	
	[spreadSheet spreadSheetCellForIdentifier:@"B3"].value = @12.4;
	[spreadSheet spreadSheetCellForIdentifier:@"C3"].value = @12.4;
	[spreadSheet spreadSheetCellForIdentifier:@"B4"].value = @0.0;
	[spreadSheet spreadSheetCellForIdentifier:@"C4"].value = @0.0;
	[spreadSheet spreadSheetCellForIdentifier:@"B5"].value = @23.6;
	[spreadSheet spreadSheetCellForIdentifier:@"C5"].value = @23.6;
	[spreadSheet spreadSheetCellForIdentifier:@"B8"].value = @11.25;
	[spreadSheet spreadSheetCellForIdentifier:@"C8"].value = @11.25;
	
	IMSpreadSheetCell *cell = [spreadSheet spreadSheetCellForIdentifier:@"B9"];
	cell.watchList = @[[ssEmpCost spreadSheetCellForIdentifier:@"C19"]];
	[cell setFunction:^NSNumber *(IMSpreadSheet *calculator, IMSpreadSheetCell *cell) {
		double val1 = (23.6 * [ssEmpCost valueForIdentifier:@"C19"].doubleValue) / 60.0;
		return [NSNumber numberWithDouble:val1];
	}];
	
	cell = [spreadSheet spreadSheetCellForIdentifier:@"C9"];
	cell.watchList = @[[ssEmpCost spreadSheetCellForIdentifier:@"C19"]];
	[cell setFunction:^NSNumber *(IMSpreadSheet *calculator, IMSpreadSheetCell *cell) {
		double val1 = (23.6 * [ssEmpCost valueForIdentifier:@"C19"].doubleValue) / 60.0;
		return [NSNumber numberWithDouble:val1];
	}];
	
	[spreadSheet spreadSheetCellForIdentifier:@"B16"].value = @12.4;
	[spreadSheet spreadSheetCellForIdentifier:@"C16"].value = @12.4;
	[spreadSheet spreadSheetCellForIdentifier:@"B17"].value = @0.0;
	[spreadSheet spreadSheetCellForIdentifier:@"C17"].value = @0.0;
	[spreadSheet spreadSheetCellForIdentifier:@"B18"].value = @23.6;
	[spreadSheet spreadSheetCellForIdentifier:@"C18"].value = @23.6;
	[spreadSheet spreadSheetCellForIdentifier:@"B22"].value = @11.25;
	[spreadSheet spreadSheetCellForIdentifier:@"C22"].value = @11.25;
	
	cell = [spreadSheet spreadSheetCellForIdentifier:@"B26"];
	cell.watchList = @[[ssScreening spreadSheetCellForIdentifier:@"B5"]
					   ,[ssScreening spreadSheetCellForIdentifier:@"B16"]
					   ,[ssNewHire spreadSheetCellForIdentifier:@"B4"]];
	[cell setFunction:^NSNumber *(IMSpreadSheet *calculator, IMSpreadSheetCell *cell) {
		double val1 = [ssScreening valueForIdentifier:@"B5"].doubleValue * [ssScreening valueForIdentifier:@"B16"].doubleValue;
		double val2 = [ssNewHire valueForIdentifier:@"B4"].doubleValue * [ssScreening valueForIdentifier:@"B16"].doubleValue;
		double val3 = val1 + val2;
		return [NSNumber numberWithDouble:val3];
	}];
	
	cell = [spreadSheet spreadSheetCellForIdentifier:@"B27"];
	cell.watchList = @[[spreadSheet spreadSheetCellForIdentifier:@"B26"]
					   ,[ssScreening spreadSheetCellForIdentifier:@"B8"]
					   ,[ssEmpCost spreadSheetCellForIdentifier:@"B22"]];
	[cell setFunction:^NSNumber *(IMSpreadSheet *calculator, IMSpreadSheetCell *cell) {
		double val1 = [spreadSheet valueForIdentifier:@"B26"].doubleValue * [ssScreening valueForIdentifier:@"B8"].doubleValue * [ssEmpCost valueForIdentifier:@"B22"].doubleValue;
		return [NSNumber numberWithDouble:val1];
	}];
	
	cell = [spreadSheet spreadSheetCellForIdentifier:@"B28"];
	cell.watchList = [spreadSheet arrayOfCells:@[@"B9", @"B26"]];
	[cell setFunction:^NSNumber *(IMSpreadSheet *calculator, IMSpreadSheetCell *cell) {
		double val1 = [spreadSheet valueForIdentifier:@"B9"].doubleValue * [spreadSheet valueForIdentifier:@"B26"].doubleValue;
		return [NSNumber numberWithDouble:val1];
	}];
	
	cell = [spreadSheet spreadSheetCellForIdentifier:@"B29"];
	cell.watchList = [spreadSheet arrayOfCells:@[@"B8", @"B26"]];
	[cell setFunction:^NSNumber *(IMSpreadSheet *calculator, IMSpreadSheetCell *cell) {
		double val1 = [spreadSheet valueForIdentifier:@"B8"].doubleValue * [spreadSheet valueForIdentifier:@"B26"].doubleValue;
		return [NSNumber numberWithDouble:val1];
	}];
	
	cell = [spreadSheet spreadSheetCellForIdentifier:@"B30"];
	cell.watchList = @[[ssScreening spreadSheetCellForIdentifier:@"B5"]
					   ,[ssNewHire spreadSheetCellForIdentifier:@"B4"]
					   ,[ssTspot spreadSheetCellForIdentifier:@"B7"]];
	[cell setFunction:^NSNumber *(IMSpreadSheet *calculator, IMSpreadSheetCell *cell) {
		double val1 = ([ssScreening valueForIdentifier:@"B5"].doubleValue + [ssNewHire valueForIdentifier:@"B4"].doubleValue) * [ssTspot valueForIdentifier:@"B7"].doubleValue;
		return [NSNumber numberWithDouble:val1];
	}];
	
	[spreadSheet spreadSheetCellForIdentifier:@"B31"].value = @0.0;
	
	cell = [spreadSheet spreadSheetCellForIdentifier:@"B32"];
	cell.watchList = [spreadSheet arrayOfCells:@[@"C9", @"B30"]];
	[cell setFunction:^NSNumber *(IMSpreadSheet *calculator, IMSpreadSheetCell *cell) {
		return [spreadSheet multiplyCells:@[@"C9", @"B30"]];
	}];
	
	cell = [spreadSheet spreadSheetCellForIdentifier:@"B33"];
	cell.watchList = [spreadSheet arrayOfCells:@[@"C8", @"B30"]];
	[cell setFunction:^NSNumber *(IMSpreadSheet *calculator, IMSpreadSheetCell *cell) {
		return [spreadSheet multiplyCells:@[@"C8", @"B30"]];
	}];
	
	cell = [spreadSheet spreadSheetCellForIdentifier:@"B34"];
	cell.watchList = @[[ssScreening spreadSheetCellForIdentifier:@"B5"]
					   ,[ssScreening spreadSheetCellForIdentifier:@"B13"]
					   ,[ssNewHire spreadSheetCellForIdentifier:@"B4"]
					   ,[ssNewHire spreadSheetCellForIdentifier:@"B12"]];
	[cell setFunction:^NSNumber *(IMSpreadSheet *calculator, IMSpreadSheetCell *cell) {
		double val1 = [ssScreening valueForIdentifier:@"B5"].doubleValue * [ssScreening valueForIdentifier:@"B13"].doubleValue;
		double val2 = [ssNewHire valueForIdentifier:@"B4"].doubleValue * [ssNewHire valueForIdentifier:@"B12"].doubleValue;
		return [NSNumber numberWithDouble:val1+val2];
	}];
	
	cell = [spreadSheet spreadSheetCellForIdentifier:@"B35"];
	cell.watchList = @[[ssScreening spreadSheetCellForIdentifier:@"B5"]
					   ,[ssTspot spreadSheetCellForIdentifier:@"B5"]
					   ,[ssNewHire spreadSheetCellForIdentifier:@"B4"]];
	[cell setFunction:^NSNumber *(IMSpreadSheet *calculator, IMSpreadSheetCell *cell) {
		double val1 = [ssScreening valueForIdentifier:@"B5"].doubleValue * [ssTspot valueForIdentifier:@"B5"].doubleValue;
		double val2 = [ssNewHire valueForIdentifier:@"B4"].doubleValue * [ssTspot valueForIdentifier:@"B5"].doubleValue;
		double val3 = val1 + val2;
		return [NSNumber numberWithDouble:val3];
	}];
	
	IMSpreadSheet *ssEmpData = [self spreadSheetWithName:kIMTvQEmpTimeData];
	cell = [spreadSheet spreadSheetCellForIdentifier:@"B39"];
	cell.watchList = @[[ssScreening spreadSheetCellForIdentifier:@"B5"]
					   ,[ssTspot spreadSheetCellForIdentifier:@"B5"]
					   ,[ssNewHire spreadSheetCellForIdentifier:@"B4"]
					   ,[ssNewHire spreadSheetCellForIdentifier:@"C12"]];
	[cell setFunction:^NSNumber *(IMSpreadSheet *calculator, IMSpreadSheetCell *cell) {
		double val1 = [ssEmpData valueForIdentifier:@"B15"].doubleValue + [ssEmpData valueForIdentifier:@"B16"].doubleValue + [ssEmpData valueForIdentifier:@"B17"].doubleValue;
		double val2 = val1 / 60.0;
		double val3 = [ssScreening valueForIdentifier:@"B5"].doubleValue * [ssTspot valueForIdentifier:@"B5"].doubleValue;
		double val4 = [ssNewHire valueForIdentifier:@"B4"].doubleValue * [ssNewHire valueForIdentifier:@"C12"].doubleValue;
		double val5 = val2 * (val3 + val4);
		return [NSNumber numberWithDouble:val5];
	}];
	
	cell = [spreadSheet spreadSheetCellForIdentifier:@"C39"];
	cell.watchList = @[[ssScreening spreadSheetCellForIdentifier:@"B5"]
					   ,[ssTspot spreadSheetCellForIdentifier:@"B5"]
					   ,[ssNewHire spreadSheetCellForIdentifier:@"B4"]
					   ,[ssNewHire spreadSheetCellForIdentifier:@"C12"]];
	[cell setFunction:^NSNumber *(IMSpreadSheet *calculator, IMSpreadSheetCell *cell) {
		double val1 = [ssEmpData valueForIdentifier:@"B15"].doubleValue + [ssEmpData valueForIdentifier:@"B16"].doubleValue + [ssEmpData valueForIdentifier:@"B17"].doubleValue;
		double val2 = val1 / 60.0;
		double val3 = [ssScreening valueForIdentifier:@"B5"].doubleValue * [ssTspot valueForIdentifier:@"B5"].doubleValue;
		double val4 = [ssNewHire valueForIdentifier:@"B4"].doubleValue * [ssNewHire valueForIdentifier:@"C12"].doubleValue;
		double val5 = val2 * (val3 + val4);
		return [NSNumber numberWithDouble:val5];
	}];
	
	cell = [spreadSheet spreadSheetCellForIdentifier:@"B40"];
	cell.watchList = @[[ssScreening spreadSheetCellForIdentifier:@"B5"]
					   ,[ssTspot spreadSheetCellForIdentifier:@"B5"]
					   ,[ssNewHire spreadSheetCellForIdentifier:@"B4"]
					   ,[ssNewHire spreadSheetCellForIdentifier:@"C12"]];
	[cell setFunction:^NSNumber *(IMSpreadSheet *calculator, IMSpreadSheetCell *cell) {
		double val1 = [ssEmpData valueForIdentifier:@"B25"].doubleValue + [ssEmpData valueForIdentifier:@"B26"].doubleValue;
		double val2 = val1 / 60.0;
		double val3 = [ssScreening valueForIdentifier:@"B5"].doubleValue * [ssTspot valueForIdentifier:@"B5"].doubleValue;
		double val4 = [ssNewHire valueForIdentifier:@"B4"].doubleValue * [ssNewHire valueForIdentifier:@"C12"].doubleValue;
		double val5 = val2 * (val3 + val4);
		return [NSNumber numberWithDouble:val5];
	}];
	
	cell = [spreadSheet spreadSheetCellForIdentifier:@"C40"];
	cell.watchList = @[[ssScreening spreadSheetCellForIdentifier:@"B5"]
					   ,[ssTspot spreadSheetCellForIdentifier:@"B5"]
					   ,[ssNewHire spreadSheetCellForIdentifier:@"B4"]
					   ,[ssNewHire spreadSheetCellForIdentifier:@"C12"]];
	[cell setFunction:^NSNumber *(IMSpreadSheet *calculator, IMSpreadSheetCell *cell) {
		double val1 = [ssEmpData valueForIdentifier:@"B25"].doubleValue + [ssEmpData valueForIdentifier:@"B26"].doubleValue;
		double val2 = val1 / 60.0;
		double val3 = [ssScreening valueForIdentifier:@"B5"].doubleValue * [ssTspot valueForIdentifier:@"B5"].doubleValue;
		double val4 = [ssNewHire valueForIdentifier:@"B4"].doubleValue * [ssNewHire valueForIdentifier:@"C12"].doubleValue;
		double val5 = val2 * (val3 + val4);
		return [NSNumber numberWithDouble:val5];
	}];
	
	cell = [spreadSheet spreadSheetCellForIdentifier:@"B43"];
	cell.watchList = @[[ssAnalysis spreadSheetCellForIdentifier:@"B34"]
					   ,[ssScreening spreadSheetCellForIdentifier:@"B14"]];
	[cell setFunction:^NSNumber *(IMSpreadSheet *calculator, IMSpreadSheetCell *cell) {
		double val1 = [ssAnalysis valueForIdentifier:@"B34"].doubleValue * [ssScreening valueForIdentifier:@"B14"].doubleValue;
		return [NSNumber numberWithDouble:val1];
	}];
	
	cell = [spreadSheet spreadSheetCellForIdentifier:@"C43"];
	cell.watchList = @[[spreadSheet spreadSheetCellForIdentifier:@"B35"]
					   ,[ssScreening spreadSheetCellForIdentifier:@"B14"]];
	[cell setFunction:^NSNumber *(IMSpreadSheet *calculator, IMSpreadSheetCell *cell) {
		double val1 = [spreadSheet valueForIdentifier:@"B35"].doubleValue * [ssScreening valueForIdentifier:@"B14"].doubleValue;
		return [NSNumber numberWithDouble:val1];
	}];
	
}

- (void) initEmployeeTimeReferenceData
{
	IMSpreadSheet *ssEmployeeTime = [self spreadSheetWithName:kIMTvQEmpTimeData];
	IMSpreadSheetCell *cell;
	NSArray *values = @[@5.0, @7.5, @6.7, @7.4, @10.0, @6.2, @13.7, @5.0, @25.0];
	for(int i=9;i<18;i++)
	{
		cell = [ssEmployeeTime spreadSheetCellForIdentifier:[NSString stringWithFormat:@"B%d", i]];
		cell.value = values[i-9];
	}
	
	values = @[@22.9, @22.3, @23.6, @15.0, @33.2, @45.0];
	for(int i=21;i<27;i++)
	{
		cell = [ssEmployeeTime spreadSheetCellForIdentifier:[NSString stringWithFormat:@"B%d", i]];
		cell.value = values[i-21];
	}
	
	cell = [ssEmployeeTime spreadSheetCellForIdentifier:@"B35"];
	cell.value = @4.0;
	cell = [ssEmployeeTime spreadSheetCellForIdentifier:@"B36"];
	cell.value = @1.0;
	cell = [ssEmployeeTime spreadSheetCellForIdentifier:@"B39"];
	cell.value = @8.0;
}

@end
