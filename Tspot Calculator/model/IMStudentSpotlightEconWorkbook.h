//
//  IMStudentSpotlightEconWorkbook.h
//  Tspot Calculator
//
//  Created by Anthony Long on 1/2/15.
//  Copyright (c) 2015 Infuse Medical. All rights reserved.
//

#import "IMSpreadSheet.h"

@interface IMStudentSpotlightEconWorkbook : IMWorkbook

extern NSString * const kIMStudentTBScreening;
extern NSString * const kIMStudentSPOTLIGHT;
extern NSString * const kIMStudentAnalysis;
extern NSString * const kIMStudentChartsAndTotals;
extern NSString * const kIMStudenEmpTimeReference;

@end
