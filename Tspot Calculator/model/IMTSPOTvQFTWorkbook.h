//
//  IMTSPOTvQFTWorkbook.h
//  Tspot Calculator
//
//  Created by Anthony Long on 7/24/14.
//  Copyright (c) 2014 Infuse Medical. All rights reserved.
//

#import "IMSpreadSheet.h"

@interface IMTSPOTvQFTWorkbook : IMWorkbook

extern NSString * const kIMTvQInput;
extern NSString * const kIMTvQAnnualScreening;
extern NSString * const kIMTvQNewHireScreening;
extern NSString * const kIMTvQTSPOT;
extern NSString * const kIMTvQEmpCostData;
extern NSString * const kIMTvQCharts;
extern NSString * const kIMTvQAnalysis;
extern NSString * const kIMTvQEmpTimeData;

@end
