//
//  IMSpotlightvQFTWorkbook.h
//  Tspot Calculator
//
//  Created by Anthony Long on 7/26/14.
//  Copyright (c) 2014 Infuse Medical. All rights reserved.
//

#import "IMSpreadSheet.h"

@interface IMSpotlightvQFTWorkbook : IMWorkbook

extern NSString * const kIMSvQAnnualScreening;
extern NSString * const kIMSvQNewHireScreening;
extern NSString * const kIMSvQSpotlight;
extern NSString * const kIMSvQEmpCostData;
extern NSString * const kIMSvQCharts;
extern NSString * const kIMSvQAnalysis;
extern NSString * const kIMSvQEmpTimeData;

@end