//
//  IMWageCostsWorkbook.h
//  Tspot Calculator
//
//  Created by Anthony Long on 4/1/15.
//  Copyright (c) 2015 Infuse Medical. All rights reserved.
//

#import "IMSpreadSheet.h"

@interface IMWageCostsWorkbook : IMWorkbook

extern NSString * const kIMWageCostsSpreadsheet;

@end
