
//
//  IMSpreadSheetCell.m
//  IMSpreadSheet
//
//  Created by Anthony Long on 4/18/13.
//  Copyright (c) 2013 Infuse Medical. All rights reserved.
//

#import "IMSpreadSheetCell.h"
#import "IMSpreadSheet.h"


@implementation IMSpreadSheetCell
{
	IMSpreadSheetCellFunc_t _function;
	void(^_setterFunction)(IMSpreadSheetCell *cell);
	IMSpreadSheetCell *_mappedToCell;
}

@synthesize value=_value;

- (void) dealloc
{
	[self setWatchList:nil];
}

- (void)calculate
{
	if(_mappedToCell && _mappedToCell.value != _value)
	{
		[self willChangeValueForKey:@"value"];
		_value = _mappedToCell.value;
		[self didChangeValueForKey:@"value"];
		return;
	}
	if (_function)
	{
		NSNumber * n = _function(self.spreadSheet, self);
		if(self.maxValue && n.doubleValue > self.maxValue.doubleValue)
		{
			n = self.maxValue;
		}
		
		if(self.minValue && n.doubleValue < self.minValue.doubleValue)
		{
			n = self.minValue;
		}
		if(!self.value || ![n isEqualToNumber:self.value])
		{
			[self willChangeValueForKey:@"value"];
			_value = n;
			[self didChangeValueForKey:@"value"];
		}
	}
}

- (void) setValue:(NSNumber *)value
{
	if(self.maxValue && value.doubleValue > self.maxValue.doubleValue)
	{
		value = self.maxValue;
	}
	
	if(self.minValue && value.doubleValue < self.minValue.doubleValue)
	{
		value = self.minValue;
	}
	[self willChangeValueForKey:@"value"];
	_value = value;
	[self calculate];
	[self didChangeValueForKey:@"value"];
	if(_setterFunction)
	{
		_setterFunction(self);
	}
}

- (void) setWatchList:(NSArray *)list
{
	for(IMSpreadSheetCell * cell in _watchList)
	{
		[cell removeObserver:self forKeyPath:@"value"];
	}
	_watchList = list;
	self.watchListEnabled = YES;
	for(IMSpreadSheetCell * cell in list)
	{
		[cell addObserver:self forKeyPath:@"value" options:NSKeyValueObservingOptionNew context:nil];
	}
}

- (void) observeValueForKeyPath:(NSString *)keyPath ofObject:(id)object change:(NSDictionary *)change context:(void *)context
{
	if(!self.watchListEnabled)
	{
		return;
	}
	if([_watchList containsObject:object])
	{
		[self calculate];
	}
}

- (void)setFunction:(IMSpreadSheetCellFunc_t)cellFunc
{
	self.hasFunction = NO;
    if (_function)
    {
//        Block_release(_function);
        _function = NULL;
    }
    
    if (cellFunc)
    {
		self.hasFunction = YES;
		_function = [cellFunc copy];
    }
	[self calculate];
}

- (void)setSetterFunction:(void(^)(IMSpreadSheetCell *cell))cellFunc
{
	if(_setterFunction)
	{
		_setterFunction = NULL;
	}
	
	if(cellFunc)
	{
		_setterFunction = [cellFunc copy];
	}
}

- (void) mapToCell:(IMSpreadSheetCell *)cell
{
	self.watchList = @[cell];
	_mappedToCell = cell;
}

- (void) setFormula:(NSString *)formula
{
	_formula = formula;
	[self parseFormula:_formula];
}

- (void) parseFormula:(NSString *)formula 
{
	//SUM(A1:A5) * SUM(B1:B5) * SUM(C1,C2,C3)
	//SUM(SUM(A1:A5) * SUM(B1:B5) * SUM(C1,C2,C3), 10)
//	NSRegularExpression * reg1 = [NSRegularExpression regularExpressionWithPattern:@"([A-Z]+)\\((.*)\\)"
//																		   options:NSRegularExpressionCaseInsensitive
//																			 error:NULL];
}

#pragma mark lookups

- (SEL) selectorForKey:(NSString *)key
{
	static NSDictionary * methodKeys;
	if(!methodKeys)
	{
		methodKeys = [NSDictionary dictionaryWithObjectsAndKeys:[NSValue valueWithPointer:@selector(sumOfValues:)], @"SUM",
					  @selector(productOfValues:), @"PRODUCT",
					  @selector(quotientOfValues:), @"QUOTIENT", nil];
	}
	
	SEL returnSelector = [[methodKeys objectForKey:key] pointerValue];
	return returnSelector;
}


+ (float) floatValueForValue:(NSObject *)value
{
	if([value isKindOfClass:[NSNumber class]])
	{
		return [(NSNumber *)value floatValue];
	}
	if([value isKindOfClass:[IMSpreadSheetCell class]])
	{
		return [(IMSpreadSheetCell *)value value].floatValue;
	}
	
	return nanf("");
}

#pragma mark calculations

+ (float) sumOfValues:(NSArray *)values
{
	if(!values || values.count == 0)
	{
		//make throw exception
		return 0.0f;
	}
	
	float returnValue = [self floatValueForValue:[values objectAtIndex:0]];
	NSObject * value;
	for(int i=1;i<values.count;i++)
	{
		value = [values objectAtIndex:i];
		returnValue += [self floatValueForValue:value];
	}
	
	return returnValue;
}

+ (float) productOfValues:(NSArray *) values
{
	if(!values || values.count == 0)
	{
		//make throw exception
		return 0.0f;
	}
	
	float returnValue = [self floatValueForValue:[values objectAtIndex:0]];
	NSObject * value;
	for(int i=1;i<values.count;i++)
	{
		value = [values objectAtIndex:i];
		returnValue *= [self floatValueForValue:value];
	}
	
	return returnValue;
}

+ (float) quotientOfValues:(NSArray *) values
{
	if(!values || values.count == 0)
	{
		//make throw exception
		return 0.0f;
	}
	
	float returnValue = [self floatValueForValue:[values objectAtIndex:0]];
	NSObject * value;
	for(int i=1;i<values.count;i++)
	{
		value = [values objectAtIndex:i];
		returnValue /= [self floatValueForValue:value];
	}
	
	return returnValue;
}

@end