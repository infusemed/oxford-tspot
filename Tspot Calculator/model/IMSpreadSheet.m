//
//  IMSpreadSheet.m
//  IMSpreadSheet
//
//  Created by Anthony Long on 4/18/13.
//  Copyright (c) 2013 Infuse Medical. All rights reserved.
//

#import "IMSpreadSheet.h"

@implementation IMSpreadSheet
{
//	NSMutableDictionary * _cells;
}

- (id)init
{
	self = [super init];
	if(self)
	{
		_cells = [NSMutableDictionary dictionary];
	}
	
	return self;
}

- (id) valueForKey:(NSString *)key
{
	if([key isEqualToString:@"customer name"]){
		return self.workbook.customerName;
	}
	if([key isEqualToString:@"info"]){
		return self.workbook.additionalInfo;
	}
	if([key isEqualToString:@"dislaimer"]){
		return self.workbook.disclaimer;
	}
	if([key isEqualToString:@"initialContent"]){
		return self.workbook.initialContent;
	}
	if([key isEqualToString:@"yearSolution"]) {
		NSArray *nums = @[@"One", @"Two", @"Three", @"Four", @"Five", @"Six", @"Seven", @"Eight", @"Nine", @"Ten"];
		return nums[[self valueForIdentifier:@"N39"].integerValue-1];
	}
	if ([key isEqualToString:@"phasedContent"]) {
		return self.workbook.phasedContent;
	}
	
	NSString *s = [self spreadSheetCellForIdentifier:key].textValue;
	if (s != nil) {
		return s;
	}
	
	return [self valueForIdentifier:key];
}

- (void) calculateAll
{
	for(IMSpreadSheetCell *cell in self.cells)
	{
		[cell calculate];
	}
}

- (NSString *)description
{
	__block NSString *s = @"";
	[_cells enumerateKeysAndObjectsUsingBlock:^(NSIndexPath *key, IMSpreadSheetCell *obj, BOOL *stop) {
		s = [s stringByAppendingFormat:@"\n\r\t%@%ld : %f", key.spreadsheetColumn, (long)key.spreadsheetRow, obj.value.doubleValue];
	}];
	
	return s;
}

- (IMSpreadSheetCell *) spreadSheetCellForIndexPath:(NSIndexPath *)indexPath
{
	return [self spreadSheetCellForIndexPath:indexPath createIfNil:YES];
}

- (IMSpreadSheetCell *) spreadSheetCellForIndexPath:(NSIndexPath *)indexPath createIfNil:(BOOL)shouldCreate
{
	IMSpreadSheetCell * cell = [_cells objectForKey:indexPath];
	if(!cell && shouldCreate)
	{
		cell = [[IMSpreadSheetCell alloc] init];
		cell.spreadSheet = self;
		cell.indexPath = indexPath;
		[_cells setObject:cell forKey:indexPath];
	}
	
	return cell;
}

- (IMSpreadSheetCell *) spreadSheetCellForIdentifier:(NSString *)identifier
{
	NSString * column = [identifier stringByTrimmingCharactersInSet:[NSCharacterSet decimalDigitCharacterSet]];
	NSString * row = [identifier stringByTrimmingCharactersInSet:[NSCharacterSet letterCharacterSet]];
	return [self spreadSheetCellForIndexPath:[NSIndexPath indexPathForRow:row.intValue column:column] createIfNil:YES];
}

- (void) setSpreadSheetCell:(IMSpreadSheetCell *)cell forIndexPath:(NSIndexPath *)indexPath
{
	[_cells setObject:cell forKey:indexPath];
	cell.spreadSheet = self;
}

- (void) setValue:(NSNumber *)value forIndexPath:(NSIndexPath *)indexPath
{
	IMSpreadSheetCell * cell = [self spreadSheetCellForIndexPath:indexPath createIfNil:YES];
	cell.value = value;
}

- (void) setValue:(NSNumber *)value forIdentifier:(NSString *)identifier
{
	IMSpreadSheetCell * cell = [self spreadSheetCellForIdentifier:identifier];
	cell.value = value;
}

- (void) setTextValue:(NSString *)value forIndexPath:(NSIndexPath *)indexPath
{
	IMSpreadSheetCell * cell = [self spreadSheetCellForIndexPath:indexPath createIfNil:YES];
	cell.textValue = value;
}

- (NSNumber *) valueForIndexPath:(NSIndexPath *)indexPath
{
	IMSpreadSheetCell * cell = [_cells objectForKey:indexPath];
	return cell.value;
}

- (NSNumber *) valueForIdentifier:(NSString *)identifier
{
	NSString * column = [identifier stringByTrimmingCharactersInSet:[NSCharacterSet decimalDigitCharacterSet]];
	NSString * row = [identifier stringByTrimmingCharactersInSet:[NSCharacterSet letterCharacterSet]];
	return [self valueForIndexPath:[NSIndexPath indexPathForRow:row.intValue column:column]];
}

- (NSString *) textValueForIndexPath:(NSIndexPath *)indexPath
{
	IMSpreadSheetCell * cell = [_cells objectForKey:indexPath];
	return cell.textValue;
}

- (void) setFormula:(NSString *)formula forIndexPath:(NSIndexPath *)indexPath
{
	IMSpreadSheetCell * cell = [self spreadSheetCellForIndexPath:indexPath createIfNil:YES];
	cell.formula = formula;
}

- (NSString *) formulaForIndexPath:(NSIndexPath *)indexPath
{
	IMSpreadSheetCell * cell = [_cells objectForKey:indexPath];
	return cell.formula;
}

- (void) removeIndexPath:(NSIndexPath *)indexPath
{
	[_cells removeObjectForKey:indexPath];
}

- (NSNumber *) subtractCell:(NSString *)cellIdentifier1 fromCell:(NSString *)cellIdentifier2
{
	return [NSNumber numberWithDouble:[self valueForIdentifier:cellIdentifier2].doubleValue - [self valueForIdentifier:cellIdentifier1].doubleValue];
}

- (NSNumber *) multiplyCells:(NSArray *)cellIdentifiers
{
	//TODO: add other workbook spreadsheets to parsing and maybe move to IMAnalysisWorkbook
	double f = NAN;
	for(NSString * s in cellIdentifiers)
	{
		NSString * column = [s stringByTrimmingCharactersInSet:[NSCharacterSet decimalDigitCharacterSet]];
		NSString * row = [s stringByTrimmingCharactersInSet:[NSCharacterSet letterCharacterSet]];
		if(isnan(f))
		{
			f = [self valueForIndexPath:[NSIndexPath indexPathForRow:row.intValue column:column]].floatValue;
		}
		else
		{
			f *= [self valueForIndexPath:[NSIndexPath indexPathForRow:row.intValue column:column]].floatValue;
		}
	}
	
	return [NSNumber numberWithFloat:f];
}


- (NSNumber *) divideCells:(NSArray *)cellIdentifiers
{
	//TODO: add other workbook spreadsheets to parsing and maybe move to IMAnalysisWorkbook
	double f = NAN;
	for(NSString * s in cellIdentifiers)
	{
		NSString * column = [s stringByTrimmingCharactersInSet:[NSCharacterSet decimalDigitCharacterSet]];
		NSString * row = [s stringByTrimmingCharactersInSet:[NSCharacterSet letterCharacterSet]];
		if(isnan(f))
		{
			f = [self valueForIndexPath:[NSIndexPath indexPathForRow:row.intValue column:column]].floatValue;
		}
		else
		{
			f /= [self valueForIndexPath:[NSIndexPath indexPathForRow:row.intValue column:column]].floatValue;
		}
	}
	if(isnan(f) || isinf(f))
	{
		return [NSNumber numberWithFloat:0.0f];
	}
	return [NSNumber numberWithFloat:f];
}

- (NSArray *) arrayOfCells:(NSArray *) arrayIdentifiers
{
	NSMutableArray * ma = [NSMutableArray array];
	for(NSString * s in arrayIdentifiers)
	{
		NSString * column = [s stringByTrimmingCharactersInSet:[NSCharacterSet decimalDigitCharacterSet]];
		NSString * row = [s stringByTrimmingCharactersInSet:[NSCharacterSet letterCharacterSet]];
		IMSpreadSheetCell * cell = [self spreadSheetCellForIndexPath:[NSIndexPath indexPathForRow:row.intValue column:column]];
		[ma addObject:cell];
	}
	
	return ma;
}

- (NSNumber *) sumOfCells:(NSArray *)cellIdentifiers
{
	//TODO: add other workbook spreadsheets to parsing and maybe move to IMAnalysisWorkbook
	double f = NAN;
	for(NSString * s in cellIdentifiers)
	{
		NSString * column = [s stringByTrimmingCharactersInSet:[NSCharacterSet decimalDigitCharacterSet]];
		NSString * row = [s stringByTrimmingCharactersInSet:[NSCharacterSet letterCharacterSet]];
		if(isnan(f))
		{
			f = [self valueForIndexPath:[NSIndexPath indexPathForRow:row.intValue column:column]].floatValue;
		}
		else
		{
			f += [self valueForIndexPath:[NSIndexPath indexPathForRow:row.intValue column:column]].floatValue;
		}
	}
	
	return [NSNumber numberWithFloat:f];
}

- (NSNumber *)sumOfCellsInArray:(NSArray *)cells
{
	double d = 0.0;
	for(IMSpreadSheetCell *cell in cells)
	{
		d += cell.value.doubleValue;
	}
	
	return [NSNumber numberWithDouble:d];
}

- (NSNumber *) sumOfCellRange:(NSString *)cellRange
{
	NSArray * a = [cellRange componentsSeparatedByString:@":"];
	NSString * cell1 = [a objectAtIndex:0];
	NSString * cell2 = [a objectAtIndex:1];
	NSString * column = [cell1 stringByTrimmingCharactersInSet:[NSCharacterSet decimalDigitCharacterSet]];
	NSString * row1 = [cell1 stringByTrimmingCharactersInSet:[NSCharacterSet letterCharacterSet]];
	NSString * row2 = [cell2 stringByTrimmingCharactersInSet:[NSCharacterSet letterCharacterSet]];
	NSMutableArray * ma = [NSMutableArray array];
	for(int i=row1.intValue;i<=row2.intValue;i++)
	{
		[ma addObject:[NSString stringWithFormat:@"%@%d", column, i]];
	}
	return [self sumOfCells:ma];
}

@end

@interface IMWorkbook()


@end

@implementation IMWorkbook
{
	NSMutableDictionary * _spreadSheets;
}

- (id) init
{
	self = [super init];
	if(self)
	{
		[self initializeWorkbook];
	}
	
	return self;
}

- (id) initWithFile:(NSString *)filePath
{
	self = [super init];
	if(self)
	{
		self.fileName = filePath.lastPathComponent;
		[self initializeWorkbook];
		NSDictionary * d = [NSDictionary dictionaryWithContentsOfFile:filePath];
		[d enumerateKeysAndObjectsUsingBlock:^(id key, id obj, BOOL *stop) {
			NSDictionary * d2 = (NSDictionary *)obj;
			IMSpreadSheet * spreadsheet = [self spreadSheetWithName:(NSString *)key];
			if(!spreadsheet)
			{
				spreadsheet = [[IMSpreadSheet alloc] init];
				[self setSpreadSheet:spreadsheet WithName:(NSString *)key];
			}
			[d2 enumerateKeysAndObjectsUsingBlock:^(id key, id obj, BOOL *stop) {
				NSArray * a = [(NSString *)key componentsSeparatedByString:@"-"];
				NSUInteger indexes[] = {[[a objectAtIndex:0] intValue], [[a objectAtIndex:1] intValue]};
				if([obj isKindOfClass:[NSString class]])
				{
					[spreadsheet setTextValue:obj forIndexPath:[NSIndexPath indexPathWithIndexes:indexes length:2]];
				}
				else
				{
					[spreadsheet setValue:obj forIndexPath:[NSIndexPath indexPathWithIndexes:indexes length:2]];
				}
			}];
		}];
	}
	
	return self;
}

- (void) initializeWorkbook
{
	_spreadSheets = [NSMutableDictionary dictionary];
}

- (void) setCustomerName:(NSString *)customerName
{
	[[self spreadSheetWithName:@"Customer"] spreadSheetCellForIdentifier:@"customerName"].textValue = customerName;
}

- (NSString *) customerName
{
	return [[self spreadSheetWithName:@"Customer"] spreadSheetCellForIdentifier:@"customerName"].textValue;
}

- (id) valueForKey:(NSString *)key
{
	if([key isEqualToString:@"customer name"]){
		return self.customerName;
	}
	if([key isEqualToString:@"info"]){
		return self.additionalInfo;
	}
	if([key isEqualToString:@"dislaimer"]){
		return self.disclaimer;
	}
	if([key isEqualToString:@"initialContent"]){
		return self.initialContent;
	}
	if([key isEqualToString:@"phasedContent"]){
		return self.phasedContent;
	}
	
	return [super valueForKey:key];
}


//- (id) initWithFile:(NSString *)filePath
//{
////	return [self init];
//	self = [super init];
//	if(self)
//	{
//		self.fileName = filePath.lastPathComponent;
//		_spreadSheets = [NSMutableDictionary dictionary];
//		NSDictionary * d = [NSDictionary dictionaryWithContentsOfFile:filePath];
//		[d enumerateKeysAndObjectsUsingBlock:^(id key, id obj, BOOL *stop) {
//			NSDictionary * d2 = (NSDictionary *)obj;
//			IMSpreadSheet * spreadsheet = [[IMSpreadSheet alloc] init];
//			[d2 enumerateKeysAndObjectsUsingBlock:^(id key, id obj, BOOL *stop) {
//				NSArray * a = [(NSString *)key componentsSeparatedByString:@"-"];
//				NSUInteger indexes[] = {[[a objectAtIndex:0] intValue], [[a objectAtIndex:1] intValue]};
//				[spreadsheet setValue:obj
//						 forIndexPath:[NSIndexPath indexPathWithIndexes:indexes length:2]];
//			}];
//			[self setSpreadSheet:spreadsheet WithName:(NSString *)key];
//		}];
//	}
//	
//	return self;
//}

- (NSString *)description
{
	__block NSString *s = @"";
	[_spreadSheets enumerateKeysAndObjectsUsingBlock:^(NSString *key, IMSpreadSheet *obj, BOOL *stop) {
		s = [s stringByAppendingFormat:@"\n\r%@ : %@", key, obj];
	}];
	
	return s;
}

- (void)writeToFile:(NSString *)filePath
{
	self.fileName = filePath.lastPathComponent;
	NSString *directoryPath = [filePath stringByDeletingLastPathComponent];
	if(![[NSFileManager defaultManager] fileExistsAtPath:directoryPath])
	{
		NSError *error = nil;
		[[NSFileManager defaultManager] createDirectoryAtPath:directoryPath
								  withIntermediateDirectories:YES
												   attributes:nil
														error:&error];
		
		if(error)
		{
			NSLog(@"error creating save directory : %@", error.description);
		}
	}
	NSMutableDictionary * d = [NSMutableDictionary dictionary];
	[_spreadSheets enumerateKeysAndObjectsUsingBlock:^(id key, id obj, BOOL *stop) {
		NSMutableDictionary * d2 = [NSMutableDictionary dictionary];
		IMSpreadSheet * spreadsheet = (IMSpreadSheet *)obj;
		[spreadsheet.cells enumerateKeysAndObjectsUsingBlock:^(id key, id obj, BOOL *stop) {
			IMSpreadSheetCell * cell = (IMSpreadSheetCell *)obj;
			NSIndexPath * indexPath = (NSIndexPath *)key;
			NSString * newKey = [NSString stringWithFormat:@"%ld-%lu", (long)indexPath.spreadsheetRow, (unsigned long)[indexPath indexAtPosition:1]];
			if(cell.textValue)
			{
				[d2 setValue:cell.textValue forKey:newKey];
			}
			else
			{
				[d2 setValue:cell.value forKey:newKey];
			}
		}];
		[d setObject:d2 forKey:key];
	}];
	
	BOOL b = [d writeToFile:filePath atomically:YES];
	if(!b)
	{
		NSLog(@"save fail");
	}
}

- (IMSpreadSheet *) spreadSheetWithName:(NSString *)name
{
	return [_spreadSheets objectForKey:name];
}

- (void) setSpreadSheet:(IMSpreadSheet *)spreadSheet WithName:(NSString *)name
{
	[_spreadSheets setObject:spreadSheet forKey:name];
	spreadSheet.workbook = self;
}

@end