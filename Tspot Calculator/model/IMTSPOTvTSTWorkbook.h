//
//  IMTSPOTvTSTWorkbook.h
//  Tspot Calculator
//
//  Created by Anthony Long on 7/25/14.
//  Copyright (c) 2014 Infuse Medical. All rights reserved.
//

#import "IMSpreadSheet.h"

@interface IMTSPOTvTSTWorkbook : IMWorkbook

extern NSString * const kIMTvTInput;
extern NSString * const kIMTvTAnnualScreening;
extern NSString * const kIMTvTNewHireScreening;
extern NSString * const kIMTvTTSPOT;
extern NSString * const kIMTvTEmpCostData;
extern NSString * const kIMTvTCharts;
extern NSString * const kIMTvTAnalysis;
extern NSString * const kIMTvTEmpTimeData;


@end
