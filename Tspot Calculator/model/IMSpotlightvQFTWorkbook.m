//
//  IMSpotlightvQFTWorkbook.m
//  Tspot Calculator
//
//  Created by Anthony Long on 7/26/14.
//  Copyright (c) 2014 Infuse Medical. All rights reserved.
//

#import "IMSpotlightvQFTWorkbook.h"

@implementation IMSpotlightvQFTWorkbook

NSString * const kIMSvQAnnualScreening = @"Annual TB Screening Program Tab";
NSString * const kIMSvQNewHireScreening = @"New Hire TB Screening Program Tab";
NSString * const kIMSvQSpotlight = @"Spotlight";
NSString * const kIMSvQEmpCostData = @"Employee Cost Data";
NSString * const kIMSvQCharts = @"Charts and Totals";
NSString * const kIMSvQAnalysis = @"Analysis";
NSString * const kIMSvQEmpTimeData = @"Employee Time Reference Data";


//- (id) init
//{
//	self = [super init];
//	if(self)
//	{
//		[self initializeWorkbook];
//	}
//	
//	return self;
//}
//
//- (id) initWithFile:(NSString *)filePath
//{
//	self = [super init];
//	if(self)
//	{
//		self.fileName = filePath.lastPathComponent;
//		[self initializeWorkbook];
//		NSDictionary * d = [NSDictionary dictionaryWithContentsOfFile:filePath];
//		[d enumerateKeysAndObjectsUsingBlock:^(id key, id obj, BOOL *stop) {
//			NSDictionary * d2 = (NSDictionary *)obj;
//			IMSpreadSheet * spreadsheet = [self spreadSheetWithName:(NSString *)key];
//			[d2 enumerateKeysAndObjectsUsingBlock:^(id key, id obj, BOOL *stop) {
//				NSArray * a = [(NSString *)key componentsSeparatedByString:@"-"];
//				NSUInteger indexes[] = {[[a objectAtIndex:0] intValue], [[a objectAtIndex:1] intValue]};
//				if([obj isKindOfClass:[NSString class]])
//				{
//					[spreadsheet setTextValue:obj forIndexPath:[NSIndexPath indexPathWithIndexes:indexes length:2]];
//				}
//				else
//				{
//					[spreadsheet setValue:obj forIndexPath:[NSIndexPath indexPathWithIndexes:indexes length:2]];
//				}
//			}];
//		}];
//	}
//	
//	return self;
//}

- (void) setCustomerName:(NSString *)customerName
{
	[super setCustomerName:customerName];
	[[self spreadSheetWithName:kIMSvQAnnualScreening] spreadSheetCellForIdentifier:@"B3"].textValue = customerName;
}

- (NSString *) customerName
{
	return [[self spreadSheetWithName:kIMSvQAnnualScreening] spreadSheetCellForIdentifier:@"B3"].textValue;
}

- (void) initializeWorkbook
{
	[super initializeWorkbook];
	[self setSpreadSheet:[[IMSpreadSheet alloc] init]
				WithName:kIMSvQAnnualScreening];
	[self setSpreadSheet:[[IMSpreadSheet alloc] init]
				WithName:kIMSvQNewHireScreening];
	[self setSpreadSheet:[[IMSpreadSheet alloc] init]
				WithName:kIMSvQSpotlight];
	[self setSpreadSheet:[[IMSpreadSheet alloc] init]
				WithName:kIMSvQEmpCostData];
	[self setSpreadSheet:[[IMSpreadSheet alloc] init]
				WithName:kIMSvQCharts];
	[self setSpreadSheet:[[IMSpreadSheet alloc] init]
				WithName:kIMSvQAnalysis];
	[self setSpreadSheet:[[IMSpreadSheet alloc] init]
				WithName:kIMSvQEmpTimeData];
	
	[self initScreeningTab];
	[self initSpotlight];
	[self initNewHireScreening];
	[self initEmployeeCostData];
	[self initEmployeeTimeReferenceData];
	[self initChartsAndTotals];
	[self initAnalysis];
}

- (void) initScreeningTab
{
	IMSpreadSheet *spreadSheet = [self spreadSheetWithName:kIMSvQAnnualScreening];
	IMSpreadSheet *ssNewHire = [self spreadSheetWithName:kIMSvQNewHireScreening];
	IMSpreadSheet *ssEmpCost = [self spreadSheetWithName:kIMSvQEmpCostData];
	IMSpreadSheetCell *cell = [spreadSheet spreadSheetCellForIdentifier:@"B3"];
	[cell setSetterFunction:^(IMSpreadSheetCell *cell) {
		[ssEmpCost spreadSheetCellForIdentifier:@"B6"].value = cell.value;
	}];
	[spreadSheet spreadSheetCellForIdentifier:@"B5"];
	[spreadSheet spreadSheetCellForIdentifier:@"B7"].value = @208.0;
	cell = [spreadSheet spreadSheetCellForIdentifier:@"B10"];
	[cell setSetterFunction:^(IMSpreadSheetCell *cell) {
		[ssNewHire spreadSheetCellForIdentifier:@"B8"].value = cell.value;
	}];
	cell = [spreadSheet spreadSheetCellForIdentifier:@"B15"];
	[cell setSetterFunction:^(IMSpreadSheetCell *cell) {
		[ssNewHire spreadSheetCellForIdentifier:@"B12"].value = cell.value;
	}];
	[spreadSheet spreadSheetCellForIdentifier:@"B16"];
	cell = [spreadSheet spreadSheetCellForIdentifier:@"B18"];
	[cell setSetterFunction:^(IMSpreadSheetCell *cell) {
		[ssNewHire spreadSheetCellForIdentifier:@"B15"].value = cell.value;
	}];
}

- (void) initSpotlight
{
	IMSpreadSheet *spreadSheet = [self spreadSheetWithName:kIMSvQSpotlight];
	IMSpreadSheet *ssNewHire = [self spreadSheetWithName:kIMSvQNewHireScreening];
	[spreadSheet spreadSheetCellForIdentifier:@"B2"];
	[spreadSheet spreadSheetCellForIdentifier:@"B4"].value = @20.0;
	[spreadSheet spreadSheetCellForIdentifier:@"B6"].value = @4.0;
	[spreadSheet spreadSheetCellForIdentifier:@"B9"].value = @0.011;
	IMSpreadSheetCell *cell = [spreadSheet spreadSheetCellForIdentifier:@"B11"];
	cell.value = @0.006;
	[cell setSetterFunction:^(IMSpreadSheetCell *cell) {
		[ssNewHire spreadSheetCellForIdentifier:@"C15"].value = cell.value;
	}];
}

- (void) initNewHireScreening
{
	IMSpreadSheet *spreadSheet = [self spreadSheetWithName:kIMSvQNewHireScreening];
	
	[spreadSheet spreadSheetCellForIdentifier:@"B4"];
	[spreadSheet spreadSheetCellForIdentifier:@"B8"];
	[spreadSheet spreadSheetCellForIdentifier:@"C8"];
	[spreadSheet spreadSheetCellForIdentifier:@"B12"];
	[spreadSheet spreadSheetCellForIdentifier:@"C12"].value = @0.011;
	[spreadSheet spreadSheetCellForIdentifier:@"B15"];
	[spreadSheet spreadSheetCellForIdentifier:@"C15"].value = @0.006;
}

- (void) initEmployeeCostData
{
	IMSpreadSheet * spreadSheet = [self spreadSheetWithName:kIMSvQEmpCostData];
	
	[spreadSheet spreadSheetCellForIdentifier:@"B6"].value = @0.0;
	
	IMSpreadSheetCell * cell = [spreadSheet spreadSheetCellForIdentifier:@"B8"];
	cell.value = @54.44;
	cell = [spreadSheet spreadSheetCellForIdentifier:@"F8"];
	cell.value = @54.44;
	cell = [spreadSheet spreadSheetCellForIdentifier:@"B12"];
	cell.value = @110.52;
	cell = [spreadSheet spreadSheetCellForIdentifier:@"C12"];
	cell.value = @0.159;
	cell = [spreadSheet spreadSheetCellForIdentifier:@"D12"];
	cell.watchList = [spreadSheet arrayOfCells:@[@"C12", @"B12"]];
	[cell setFunction:^NSNumber *(IMSpreadSheet *calculator, IMSpreadSheetCell *cell) {
		return [calculator multiplyCells:@[@"C12", @"B12"]];
	}];
	
	cell = [spreadSheet spreadSheetCellForIdentifier:@"B13"];
	cell.value = @34.65;
	cell = [spreadSheet spreadSheetCellForIdentifier:@"C13"];
	cell.value = @0.363;
	cell = [spreadSheet spreadSheetCellForIdentifier:@"D13"];
	cell.watchList = [spreadSheet arrayOfCells:@[@"B13", @"C13"]];
	[cell setFunction:^NSNumber *(IMSpreadSheet *calculator, IMSpreadSheetCell *cell) {
		return [spreadSheet multiplyCells:@[@"B13", @"C13"]];
	}];
	
	cell = [spreadSheet spreadSheetCellForIdentifier:@"B14"];
	cell.value = @139.04;
	cell = [spreadSheet spreadSheetCellForIdentifier:@"C14"];
	cell.value = @0.063;
	cell = [spreadSheet spreadSheetCellForIdentifier:@"D14"];
	cell.watchList = [spreadSheet arrayOfCells:@[@"B14", @"C14"]];
	[cell setFunction:^NSNumber *(IMSpreadSheet *calculator, IMSpreadSheetCell *cell) {
		return [calculator multiplyCells:@[@"B14", @"C14"]];
	}];
	
	cell = [spreadSheet spreadSheetCellForIdentifier:@"B15"];
	cell.value = @26.9;
	cell = [spreadSheet spreadSheetCellForIdentifier:@"C15"];
	cell.value = @0.225;
	cell = [spreadSheet spreadSheetCellForIdentifier:@"D15"];
	cell.watchList = [spreadSheet arrayOfCells:@[@"B15", @"C15"]];
	[cell setFunction:^NSNumber *(IMSpreadSheet *calculator, IMSpreadSheetCell *cell) {
		return [calculator multiplyCells:@[@"B15", @"C15"]];
	}];
	
	cell = [spreadSheet spreadSheetCellForIdentifier:@"B16"];
	cell.value = @23.27;
	cell = [spreadSheet spreadSheetCellForIdentifier:@"C16"];
	cell.value = @0.19;
	cell = [spreadSheet spreadSheetCellForIdentifier:@"D16"];
	cell.watchList = [spreadSheet arrayOfCells:@[@"B16", @"C16"]];
	[cell setFunction:^NSNumber *(IMSpreadSheet *calculator, IMSpreadSheetCell *cell) {
		return [calculator multiplyCells:@[@"B16", @"C16"]];
	}];
	
	cell = [spreadSheet spreadSheetCellForIdentifier:@"F12"];
	cell.value = @110.52;
	cell = [spreadSheet spreadSheetCellForIdentifier:@"G12"];
	cell.value = @0.159;
	cell = [spreadSheet spreadSheetCellForIdentifier:@"H12"];
	cell.watchList = [spreadSheet arrayOfCells:@[@"G12", @"F12"]];
	[cell setFunction:^NSNumber *(IMSpreadSheet *calculator, IMSpreadSheetCell *cell) {
		return [calculator multiplyCells:@[@"G12", @"F12"]];
	}];
	
	cell = [spreadSheet spreadSheetCellForIdentifier:@"F13"];
	cell.value = @34.65;
	cell = [spreadSheet spreadSheetCellForIdentifier:@"G13"];
	cell.value = @0.363;
	cell = [spreadSheet spreadSheetCellForIdentifier:@"H13"];
	cell.watchList = [spreadSheet arrayOfCells:@[@"F13", @"G13"]];
	[cell setFunction:^NSNumber *(IMSpreadSheet *calculator, IMSpreadSheetCell *cell) {
		return [spreadSheet multiplyCells:@[@"F13", @"G13"]];
	}];
	
	cell = [spreadSheet spreadSheetCellForIdentifier:@"F14"];
	cell.value = @139.04;
	cell = [spreadSheet spreadSheetCellForIdentifier:@"G14"];
	cell.value = @0.063;
	cell = [spreadSheet spreadSheetCellForIdentifier:@"H14"];
	cell.watchList = [spreadSheet arrayOfCells:@[@"F14", @"G14"]];
	[cell setFunction:^NSNumber *(IMSpreadSheet *calculator, IMSpreadSheetCell *cell) {
		return [calculator multiplyCells:@[@"F14", @"G14"]];
	}];
	
	cell = [spreadSheet spreadSheetCellForIdentifier:@"F15"];
	cell.value = @26.9;
	cell = [spreadSheet spreadSheetCellForIdentifier:@"G15"];
	cell.value = @0.225;
	cell = [spreadSheet spreadSheetCellForIdentifier:@"H15"];
	cell.watchList = [spreadSheet arrayOfCells:@[@"F15", @"G15"]];
	[cell setFunction:^NSNumber *(IMSpreadSheet *calculator, IMSpreadSheetCell *cell) {
		return [calculator multiplyCells:@[@"F15", @"G15"]];
	}];
	
	cell = [spreadSheet spreadSheetCellForIdentifier:@"F16"];
	cell.value = @23.27;
	cell = [spreadSheet spreadSheetCellForIdentifier:@"G16"];
	cell.value = @0.19;
	cell = [spreadSheet spreadSheetCellForIdentifier:@"H16"];
	cell.watchList = [spreadSheet arrayOfCells:@[@"F16", @"G16"]];
	[cell setFunction:^NSNumber *(IMSpreadSheet *calculator, IMSpreadSheetCell *cell) {
		return [calculator multiplyCells:@[@"F16", @"G16"]];
	}];
	
	cell = [spreadSheet spreadSheetCellForIdentifier:@"C18"];
	cell.watchList = [spreadSheet arrayOfCells:@[@"C12", @"C13", @"C14", @"C15", @"C16"]];
	[cell setFunction:^NSNumber *(IMSpreadSheet *calculator, IMSpreadSheetCell *cell) {
		return [calculator sumOfCellRange:@"C12:C16"];
	}];
	
	cell = [spreadSheet spreadSheetCellForIdentifier:@"C19"];
	cell.watchList = [spreadSheet arrayOfCells:@[@"D12", @"D13", @"D14", @"D15", @"D16"]];
	[cell setFunction:^NSNumber *(IMSpreadSheet *calculator, IMSpreadSheetCell *cell) {
		return [calculator sumOfCellRange:@"D12:D16"];
	}];
	
	
	cell = [spreadSheet spreadSheetCellForIdentifier:@"G18"];
	cell.watchList = [spreadSheet arrayOfCells:@[@"G12", @"G13", @"G14", @"G15", @"G16"]];
	[cell setFunction:^NSNumber *(IMSpreadSheet *calculator, IMSpreadSheetCell *cell) {
		return [calculator sumOfCellRange:@"G12:G16"];
	}];
	
	cell = [spreadSheet spreadSheetCellForIdentifier:@"G19"];
	cell.watchList = [spreadSheet arrayOfCells:@[@"H12", @"H13", @"H14", @"H15", @"H16"]];
	[cell setFunction:^NSNumber *(IMSpreadSheet *calculator, IMSpreadSheetCell *cell) {
		return [calculator sumOfCellRange:@"H12:H16"];
	}];
	
	[spreadSheet spreadSheetCellForIdentifier:@"B22"].value = @1.0;
}

- (void)initChartsAndTotals
{
	IMSpreadSheet *spreadSheet = [self spreadSheetWithName:kIMSvQCharts];
	IMSpreadSheet *ssScreening = [self spreadSheetWithName:kIMSvQAnnualScreening];
	IMSpreadSheet *ssNewHire = [self spreadSheetWithName:kIMSvQNewHireScreening];
	IMSpreadSheet *ssAnalysis = [self spreadSheetWithName:kIMSvQAnalysis];
	IMSpreadSheet *ssEmpCost = [self spreadSheetWithName:kIMSvQEmpCostData];
	IMSpreadSheet *ssSpotlight = [self spreadSheetWithName:kIMSvQSpotlight];
	
	IMSpreadSheetCell *cell = [spreadSheet spreadSheetCellForIdentifier:@"B2"];
	cell.watchList = @[[ssScreening spreadSheetCellForIdentifier:@"B5"]
					   ,[ssScreening spreadSheetCellForIdentifier:@"B10"]
					   ,[ssNewHire spreadSheetCellForIdentifier:@"B8"]
					   ,[ssNewHire spreadSheetCellForIdentifier:@"B4"]
					   ,[spreadSheet spreadSheetCellForIdentifier:@"B28"]
					   ,[ssAnalysis spreadSheetCellForIdentifier:@"B43"]];
	[cell setFunction:^NSNumber *(IMSpreadSheet *calculator, IMSpreadSheetCell *cell) {
		double val1 = [ssScreening valueForIdentifier:@"B5"].doubleValue * [ssScreening valueForIdentifier:@"B10"].doubleValue;
		double val2 = [ssNewHire valueForIdentifier:@"B8"].doubleValue * [ssNewHire valueForIdentifier:@"B4"].doubleValue;
		double val3 = [spreadSheet valueForIdentifier:@"B28"].doubleValue;
		double val4 = [ssAnalysis valueForIdentifier:@"B43"].doubleValue;
		double val5 = val1 + val2 + val3 + val4;
		return [NSNumber numberWithDouble:val5];
	}];
	
	cell = [spreadSheet spreadSheetCellForIdentifier:@"C2"];
	cell.watchList = @[[ssSpotlight spreadSheetCellForIdentifier:@"B2"]
					   ,[ssScreening spreadSheetCellForIdentifier:@"B5"]
					   ,[ssNewHire spreadSheetCellForIdentifier:@"C8"]
					   ,[ssNewHire spreadSheetCellForIdentifier:@"B4"]
					   ,[ssAnalysis spreadSheetCellForIdentifier:@"C43"]
					   ,[ssAnalysis spreadSheetCellForIdentifier:@"B30"]];
	[cell setFunction:^NSNumber *(IMSpreadSheet *calculator, IMSpreadSheetCell *cell) {
		double val1 = [ssSpotlight valueForIdentifier:@"B2"].doubleValue * [ssScreening valueForIdentifier:@"B5"].doubleValue;
		double val2 = [ssNewHire valueForIdentifier:@"C8"].doubleValue * [ssNewHire valueForIdentifier:@"B4"].doubleValue;
		double val3 = [ssAnalysis valueForIdentifier:@"C43"].doubleValue;
		double val4 = [ssAnalysis valueForIdentifier:@"B30"].doubleValue * [ssSpotlight valueForIdentifier:@"B2"].doubleValue;
		double val5 = val1 + val2 + val3 - val4;
		return [NSNumber numberWithDouble:val5];
	}];
	
	cell = [spreadSheet spreadSheetCellForIdentifier:@"B3"];
	cell.watchList = @[[spreadSheet spreadSheetCellForIdentifier:@"B10"]
					   ,[ssEmpCost spreadSheetCellForIdentifier:@"B8"]];
	[cell setFunction:^NSNumber *(IMSpreadSheet *calculator, IMSpreadSheetCell *cell) {
		double val1 = [spreadSheet valueForIdentifier:@"B10"].doubleValue * [ssEmpCost valueForIdentifier:@"B8"].doubleValue;
		return [NSNumber numberWithDouble:val1];
	}];
	
	cell = [spreadSheet spreadSheetCellForIdentifier:@"C3"];
	cell.watchList = @[[spreadSheet spreadSheetCellForIdentifier:@"C10"]
					   ,[ssEmpCost spreadSheetCellForIdentifier:@"B8"]];
	[cell setFunction:^NSNumber *(IMSpreadSheet *calculator, IMSpreadSheetCell *cell) {
		double val1 = [spreadSheet valueForIdentifier:@"C10"].doubleValue * [ssEmpCost valueForIdentifier:@"B8"].doubleValue;
		return [NSNumber numberWithDouble:val1];
	}];
	
	cell = [spreadSheet spreadSheetCellForIdentifier:@"B4"];
	cell.watchList = @[[spreadSheet spreadSheetCellForIdentifier:@"B11"]
					   ,[ssEmpCost spreadSheetCellForIdentifier:@"C19"]];
	[cell setFunction:^NSNumber *(IMSpreadSheet *calculator, IMSpreadSheetCell *cell) {
		double val1 = [spreadSheet valueForIdentifier:@"B11"].doubleValue * [ssEmpCost valueForIdentifier:@"C19"].doubleValue;
		return [NSNumber numberWithDouble:val1];
	}];
	
	cell = [spreadSheet spreadSheetCellForIdentifier:@"C4"];
	cell.watchList = @[[spreadSheet spreadSheetCellForIdentifier:@"C11"]
					   ,[ssEmpCost spreadSheetCellForIdentifier:@"C19"]];
	[cell setFunction:^NSNumber *(IMSpreadSheet *calculator, IMSpreadSheetCell *cell) {
		double val1 = [spreadSheet valueForIdentifier:@"C11"].doubleValue * [ssEmpCost valueForIdentifier:@"C19"].doubleValue;
		return [NSNumber numberWithDouble:val1];
	}];
	
	cell = [spreadSheet spreadSheetCellForIdentifier:@"B6"];
	cell.watchList = [spreadSheet arrayOfCells:@[@"B2", @"B4"]];
	[cell setFunction:^NSNumber *(IMSpreadSheet *calculator, IMSpreadSheetCell *cell) {
		return [spreadSheet sumOfCellRange:@"B2:B4"];
	}];
	
	cell = [spreadSheet spreadSheetCellForIdentifier:@"C6"];
	cell.watchList = [spreadSheet arrayOfCells:@[@"C2", @"C4"]];
	[cell setFunction:^NSNumber *(IMSpreadSheet *calculator, IMSpreadSheetCell *cell) {
		return [spreadSheet sumOfCellRange:@"C2:C4"];
	}];
	
	cell = [spreadSheet spreadSheetCellForIdentifier:@"D6"];
	cell.watchList = [spreadSheet arrayOfCells:@[@"B6", @"C6"]];
	[cell setFunction:^NSNumber *(IMSpreadSheet *calculator, IMSpreadSheetCell *cell) {
		double val1 = [spreadSheet valueForIdentifier:@"B6"].doubleValue - [spreadSheet valueForIdentifier:@"C6"].doubleValue;
		return [NSNumber numberWithDouble:floor(val1)];
	}];
	
	cell = [spreadSheet spreadSheetCellForIdentifier:@"B10"];
	cell.watchList = @[[ssScreening spreadSheetCellForIdentifier:@"B5"]
					   ,[ssAnalysis spreadSheetCellForIdentifier:@"B3"]
					   ,[ssNewHire spreadSheetCellForIdentifier:@"B4"]
					   ,[ssAnalysis spreadSheetCellForIdentifier:@"B16"]
					   ,[ssAnalysis spreadSheetCellForIdentifier:@"B26"]];
	[cell setFunction:^NSNumber *(IMSpreadSheet *calculator, IMSpreadSheetCell *cell) {
		double val1 = [ssScreening valueForIdentifier:@"B5"].doubleValue * [ssAnalysis valueForIdentifier:@"B3"].doubleValue / 60.0;
		double val2 = [ssNewHire valueForIdentifier:@"B4"].doubleValue * [ssAnalysis valueForIdentifier:@"B16"].doubleValue / 60.0;
		double val3 = [ssAnalysis valueForIdentifier:@"B26"].doubleValue * [ssAnalysis valueForIdentifier:@"B16"].doubleValue / 60.0;
		double val4 = val1 + val2 + val3;
		return [NSNumber numberWithDouble:val4];
	}];
	
	cell = [spreadSheet spreadSheetCellForIdentifier:@"C10"];
	cell.watchList = @[[ssSpotlight spreadSheetCellForIdentifier:@"B4"]
					   ,[ssSpotlight spreadSheetCellForIdentifier:@"B6"]
					   ,[ssAnalysis spreadSheetCellForIdentifier:@"C16"]
					   ,[ssNewHire spreadSheetCellForIdentifier:@"B4"]
					   ,[ssAnalysis spreadSheetCellForIdentifier:@"B30"]
					   ,[ssAnalysis spreadSheetCellForIdentifier:@"B16"]];
	[cell setFunction:^NSNumber *(IMSpreadSheet *calculator, IMSpreadSheetCell *cell) {
		double val1 = [ssSpotlight valueForIdentifier:@"B4"].doubleValue * [ssSpotlight valueForIdentifier:@"B6"].doubleValue;
		double val2 = [ssAnalysis valueForIdentifier:@"C16"].doubleValue * [ssNewHire valueForIdentifier:@"B4"].doubleValue / 60.0;
		double val3 = [ssAnalysis valueForIdentifier:@"B30"].doubleValue * [ssAnalysis valueForIdentifier:@"C16"].doubleValue / 60.0;
		double val4 = val1 + val2 + val3;
		return [NSNumber numberWithDouble:val4];
	}];
	
	cell = [spreadSheet spreadSheetCellForIdentifier:@"B11"];
	cell.watchList = @[[ssAnalysis spreadSheetCellForIdentifier:@"B18"]
					   ,[ssScreening spreadSheetCellForIdentifier:@"B5"]
					   ,[ssNewHire spreadSheetCellForIdentifier:@"B4"]
					   ,[ssAnalysis spreadSheetCellForIdentifier:@"B26"]];
	[cell setFunction:^NSNumber *(IMSpreadSheet *calculator, IMSpreadSheetCell *cell) {
		double val1 = [ssAnalysis valueForIdentifier:@"B18"].doubleValue * [ssScreening valueForIdentifier:@"B5"].doubleValue;
		double val2 = [ssAnalysis valueForIdentifier:@"B18"].doubleValue * [ssNewHire valueForIdentifier:@"B4"].doubleValue;
		double val3 = [ssAnalysis valueForIdentifier:@"B26"].doubleValue * [ssAnalysis valueForIdentifier:@"B18"].doubleValue;
		double val4 = (val1 + val2 + val3) / 60.0;
		return [NSNumber numberWithDouble:val4];
	}];
	
	cell = [spreadSheet spreadSheetCellForIdentifier:@"C11"];
	cell.watchList = @[[ssScreening spreadSheetCellForIdentifier:@"B5"]
					   ,[ssAnalysis spreadSheetCellForIdentifier:@"C5"]
					   ,[ssNewHire spreadSheetCellForIdentifier:@"B4"]
					   ,[ssAnalysis spreadSheetCellForIdentifier:@"C18"]
					   ,[ssAnalysis spreadSheetCellForIdentifier:@"B26"]];
	[cell setFunction:^NSNumber *(IMSpreadSheet *calculator, IMSpreadSheetCell *cell) {
		double val1 = [ssScreening valueForIdentifier:@"B5"].doubleValue * [ssAnalysis valueForIdentifier:@"C5"].doubleValue / 60.0;
		double val2 = [ssNewHire valueForIdentifier:@"B4"].doubleValue * [ssAnalysis valueForIdentifier:@"C18"].doubleValue / 60.0;
		double val3 = [ssAnalysis valueForIdentifier:@"B26"].doubleValue * [ssAnalysis valueForIdentifier:@"C18"].doubleValue / 60.0;
		double val4 = val1 + val2 + val3;
		return [NSNumber numberWithDouble:val4];
	}];
	
	cell = [spreadSheet spreadSheetCellForIdentifier:@"B12"];
	cell.watchList = [spreadSheet arrayOfCells:@[@"B10", @"B11"]];
	[cell setFunction:^NSNumber *(IMSpreadSheet *calculator, IMSpreadSheetCell *cell) {
		return [spreadSheet sumOfCellRange:@"B10:B11"];
	}];
	
	cell = [spreadSheet spreadSheetCellForIdentifier:@"C12"];
	cell.watchList = [spreadSheet arrayOfCells:@[@"C10", @"C11"]];
	[cell setFunction:^NSNumber *(IMSpreadSheet *calculator, IMSpreadSheetCell *cell) {
		return [spreadSheet sumOfCellRange:@"C10:C11"];
	}];
	
	cell = [spreadSheet spreadSheetCellForIdentifier:@"D12"];
	cell.watchList = [spreadSheet arrayOfCells:@[@"B12", @"C12"]];
	[cell setFunction:^NSNumber *(IMSpreadSheet *calculator, IMSpreadSheetCell *cell) {
		double val1 = [spreadSheet valueForIdentifier:@"B12"].doubleValue - [spreadSheet valueForIdentifier:@"C12"].doubleValue;
		return [NSNumber numberWithDouble:floor(val1)];
	}];
	
	cell = [spreadSheet spreadSheetCellForIdentifier:@"B22"];
	cell.watchList = @[[ssAnalysis spreadSheetCellForIdentifier:@"B26"]];
	[cell setFunction:^NSNumber *(IMSpreadSheet *calculator, IMSpreadSheetCell *cell) {
		return [ssAnalysis valueForIdentifier:@"B26"];
	}];
	
	cell = [spreadSheet spreadSheetCellForIdentifier:@"C22"];
	cell.watchList = @[[ssAnalysis spreadSheetCellForIdentifier:@"B30"]];
	[cell setFunction:^NSNumber *(IMSpreadSheet *calculator, IMSpreadSheetCell *cell) {
		return [ssAnalysis valueForIdentifier:@"B30"];
	}];
	
	cell = [spreadSheet spreadSheetCellForIdentifier:@"D22"];
	cell.watchList = [spreadSheet arrayOfCells:@[@"B22", @"C22"]];
	[cell setFunction:^NSNumber *(IMSpreadSheet *calculator, IMSpreadSheetCell *cell) {
		double val1 = [spreadSheet valueForIdentifier:@"B22"].doubleValue - [spreadSheet valueForIdentifier:@"C22"].doubleValue;
		return [NSNumber numberWithDouble:floor(val1)];
	}];
	
	cell = [spreadSheet spreadSheetCellForIdentifier:@"B28"];
	cell.watchList = @[[ssAnalysis spreadSheetCellForIdentifier:@"B27"]];
	[cell setFunction:^NSNumber *(IMSpreadSheet *calculator, IMSpreadSheetCell *cell) {
		return [ssAnalysis valueForIdentifier:@"B27"];
	}];
	
	cell = [spreadSheet spreadSheetCellForIdentifier:@"C28"];
	cell.watchList = @[[ssAnalysis spreadSheetCellForIdentifier:@"B31"]];
	[cell setFunction:^NSNumber *(IMSpreadSheet *calculator, IMSpreadSheetCell *cell) {
		return [ssAnalysis valueForIdentifier:@"B31"];
	}];
	
	cell = [spreadSheet spreadSheetCellForIdentifier:@"B29"];
	cell.watchList = @[[ssAnalysis spreadSheetCellForIdentifier:@"B29"]];
	[cell setFunction:^NSNumber *(IMSpreadSheet *calculator, IMSpreadSheetCell *cell) {
		return [ssAnalysis valueForIdentifier:@"B29"];
	}];
	
	cell = [spreadSheet spreadSheetCellForIdentifier:@"C29"];
	cell.watchList = @[[ssAnalysis spreadSheetCellForIdentifier:@"B33"]];
	[cell setFunction:^NSNumber *(IMSpreadSheet *calculator, IMSpreadSheetCell *cell) {
		return [ssAnalysis valueForIdentifier:@"B33"];
	}];
	
	cell = [spreadSheet spreadSheetCellForIdentifier:@"B30"];
	cell.watchList = @[[ssAnalysis spreadSheetCellForIdentifier:@"B28"]];
	[cell setFunction:^NSNumber *(IMSpreadSheet *calculator, IMSpreadSheetCell *cell) {
		return [ssAnalysis valueForIdentifier:@"B28"];
	}];
	
	cell = [spreadSheet spreadSheetCellForIdentifier:@"C30"];
	cell.watchList = @[[ssAnalysis spreadSheetCellForIdentifier:@"B32"]];
	[cell setFunction:^NSNumber *(IMSpreadSheet *calculator, IMSpreadSheetCell *cell) {
		return [ssAnalysis valueForIdentifier:@"B32"];
	}];
	
	cell = [spreadSheet spreadSheetCellForIdentifier:@"B31"];
	cell.watchList = [spreadSheet arrayOfCells:@[@"B28", @"B29", @"B30"]];
	[cell setFunction:^NSNumber *(IMSpreadSheet *calculator, IMSpreadSheetCell *cell) {
		return [spreadSheet sumOfCellRange:@"B28:B30"];
	}];
	
	cell = [spreadSheet spreadSheetCellForIdentifier:@"C31"];
	cell.watchList = [spreadSheet arrayOfCells:@[@"C28", @"C29", @"C30"]];
	[cell setFunction:^NSNumber *(IMSpreadSheet *calculator, IMSpreadSheetCell *cell) {
		return [spreadSheet sumOfCellRange:@"C28:C30"];
	}];
	
	cell = [spreadSheet spreadSheetCellForIdentifier:@"D31"];
	cell.watchList = [spreadSheet arrayOfCells:@[@"B31", @"C31"]];
	[cell setFunction:^NSNumber *(IMSpreadSheet *calculator, IMSpreadSheetCell *cell) {
		double val1 = [spreadSheet valueForIdentifier:@"B31"].doubleValue - [spreadSheet valueForIdentifier:@"C31"].doubleValue;
		return [NSNumber numberWithDouble:floor(val1)];
	}];
}

- (void)initAnalysis
{
	IMSpreadSheet *spreadSheet = [self spreadSheetWithName:kIMSvQAnalysis];
	IMSpreadSheet *ssScreening = [self spreadSheetWithName:kIMSvQAnnualScreening];
	IMSpreadSheet *ssNewHire = [self spreadSheetWithName:kIMSvQNewHireScreening];
	IMSpreadSheet *ssEmpCost = [self spreadSheetWithName:kIMSvQEmpCostData];
	IMSpreadSheet *ssEmpData = [self spreadSheetWithName:kIMSvQEmpTimeData];
	IMSpreadSheet *ssSpotlight = [self spreadSheetWithName:kIMSvQSpotlight];
	
	IMSpreadSheetCell *cell = [spreadSheet spreadSheetCellForIdentifier:@"B3"];
	cell.watchList = @[[ssEmpData spreadSheetCellForIdentifier:@"B12"]
					   ,[ssEmpData spreadSheetCellForIdentifier:@"B9"]];
	[cell setFunction:^NSNumber *(IMSpreadSheet *calculator, IMSpreadSheetCell *cell) {
		double val1 = [ssEmpData valueForIdentifier:@"B12"].doubleValue + [ssEmpData valueForIdentifier:@"B9"].doubleValue;
		return [NSNumber numberWithDouble:val1];
	}];
	
	cell = [spreadSheet spreadSheetCellForIdentifier:@"C3"];
	cell.watchList = @[[ssSpotlight spreadSheetCellForIdentifier:@"B4"]
					   ,[ssEmpCost spreadSheetCellForIdentifier:@"B8"]
					   ,[ssSpotlight spreadSheetCellForIdentifier:@"B6"]
					   ,[ssScreening spreadSheetCellForIdentifier:@"B5"]];
	[cell setFunction:^NSNumber *(IMSpreadSheet *calculator, IMSpreadSheetCell *cell) {
		double val1 = [ssSpotlight valueForIdentifier:@"B4"].doubleValue * [ssEmpCost valueForIdentifier:@"B8"].doubleValue * [ssSpotlight valueForIdentifier:@"B6"].doubleValue;
		double val2 = val1 / [ssScreening valueForIdentifier:@"B5"].doubleValue;
		return [NSNumber numberWithDouble:val2];
	}];
	
	[spreadSheet spreadSheetCellForIdentifier:@"B4"].value = @0.0;
	[spreadSheet spreadSheetCellForIdentifier:@"C4"].value = @0.0;
	
	cell = [spreadSheet spreadSheetCellForIdentifier:@"B5"];
	cell.watchList = @[[ssEmpData spreadSheetCellForIdentifier:@"B23"]];
	[cell setFunction:^NSNumber *(IMSpreadSheet *calculator, IMSpreadSheetCell *cell) {
		return [ssEmpData valueForIdentifier:@"B23"];
	}];
	
	cell = [spreadSheet spreadSheetCellForIdentifier:@"C5"];
	cell.watchList = @[[ssEmpData spreadSheetCellForIdentifier:@"B24"]];
	[cell setFunction:^NSNumber *(IMSpreadSheet *calculator, IMSpreadSheetCell *cell) {
		return [ssEmpData valueForIdentifier:@"B24"];
	}];
	
	cell = [spreadSheet spreadSheetCellForIdentifier:@"B8"];
	cell.watchList = @[[spreadSheet spreadSheetCellForIdentifier:@"B3"]
					   ,[ssEmpCost spreadSheetCellForIdentifier:@"B8"]];
	[cell setFunction:^NSNumber *(IMSpreadSheet *calculator, IMSpreadSheetCell *cell) {
		double val1 = [spreadSheet valueForIdentifier:@"B3"].doubleValue * [ssEmpCost valueForIdentifier:@"B8"].doubleValue / 60.0;
		return [NSNumber numberWithDouble:val1];
	}];
	
	cell = [spreadSheet spreadSheetCellForIdentifier:@"C8"];
	cell.watchList = @[[spreadSheet spreadSheetCellForIdentifier:@"C3"]
					   ,[ssEmpCost spreadSheetCellForIdentifier:@"B8"]];
	[cell setFunction:^NSNumber *(IMSpreadSheet *calculator, IMSpreadSheetCell *cell) {
		double val1 = [spreadSheet valueForIdentifier:@"C3"].doubleValue * [ssEmpCost valueForIdentifier:@"B8"].doubleValue / 60.0;
		return [NSNumber numberWithDouble:val1];
	}];
	
	cell = [spreadSheet spreadSheetCellForIdentifier:@"B9"];
	cell.watchList = @[[ssEmpData spreadSheetCellForIdentifier:@"B23"]
					   ,[ssEmpCost spreadSheetCellForIdentifier:@"C19"]];
	[cell setFunction:^NSNumber *(IMSpreadSheet *calculator, IMSpreadSheetCell *cell) {
		double val1 = [ssEmpData valueForIdentifier:@"B23"].doubleValue * [ssEmpCost valueForIdentifier:@"C19"].doubleValue / 60.0;
		return [NSNumber numberWithDouble:val1];
	}];
	
	cell = [spreadSheet spreadSheetCellForIdentifier:@"C9"];
	cell.watchList = @[[ssEmpData spreadSheetCellForIdentifier:@"B24"]
					   ,[ssEmpCost spreadSheetCellForIdentifier:@"C19"]];
	[cell setFunction:^NSNumber *(IMSpreadSheet *calculator, IMSpreadSheetCell *cell) {
		double val1 = [ssEmpData valueForIdentifier:@"B24"].doubleValue * [ssEmpCost valueForIdentifier:@"C19"].doubleValue / 60.0;
		return [NSNumber numberWithDouble:val1];
	}];
	
	cell = [spreadSheet spreadSheetCellForIdentifier:@"B16"];
	cell.watchList = @[[ssEmpData spreadSheetCellForIdentifier:@"B12"]
					   ,[ssEmpData spreadSheetCellForIdentifier:@"B9"]];
	[cell setFunction:^NSNumber *(IMSpreadSheet *calculator, IMSpreadSheetCell *cell) {
		double val1 = [ssEmpData valueForIdentifier:@"B12"].doubleValue + [ssEmpData valueForIdentifier:@"B9"].doubleValue;
		return [NSNumber numberWithDouble:val1];
	}];
	
	cell = [spreadSheet spreadSheetCellForIdentifier:@"C16"];
	cell.watchList = @[[ssEmpData spreadSheetCellForIdentifier:@"B12"]
					   ,[ssEmpData spreadSheetCellForIdentifier:@"B9"]];
	[cell setFunction:^NSNumber *(IMSpreadSheet *calculator, IMSpreadSheetCell *cell) {
		double val1 = [ssEmpData valueForIdentifier:@"B12"].doubleValue + [ssEmpData valueForIdentifier:@"B9"].doubleValue;
		return [NSNumber numberWithDouble:val1];
	}];
	
	[spreadSheet spreadSheetCellForIdentifier:@"B17"].value = @0.0;
	[spreadSheet spreadSheetCellForIdentifier:@"C17"].value = @0.0;
	
	cell = [spreadSheet spreadSheetCellForIdentifier:@"B18"];
	cell.watchList = @[[ssEmpData spreadSheetCellForIdentifier:@"B23"]];
	[cell setFunction:^NSNumber *(IMSpreadSheet *calculator, IMSpreadSheetCell *cell) {
		return [ssEmpData valueForIdentifier:@"B23"];
	}];
	
	cell = [spreadSheet spreadSheetCellForIdentifier:@"C18"];
	cell.watchList = @[[ssEmpData spreadSheetCellForIdentifier:@"B23"]];
	[cell setFunction:^NSNumber *(IMSpreadSheet *calculator, IMSpreadSheetCell *cell) {
		return [ssEmpData valueForIdentifier:@"B23"];
	}];
	
	cell = [spreadSheet spreadSheetCellForIdentifier:@"B22"];
	cell.watchList = @[[spreadSheet spreadSheetCellForIdentifier:@"B16"]
					   ,[ssEmpCost spreadSheetCellForIdentifier:@"B8"]];
	[cell setFunction:^NSNumber *(IMSpreadSheet *calculator, IMSpreadSheetCell *cell) {
		double val1 = [spreadSheet valueForIdentifier:@"B16"].doubleValue * [ssEmpCost valueForIdentifier:@"B8"].doubleValue / 60.0;
		return [NSNumber numberWithDouble:val1];
	}];
	
	cell = [spreadSheet spreadSheetCellForIdentifier:@"C22"];
	cell.watchList = @[[spreadSheet spreadSheetCellForIdentifier:@"C16"]
					   ,[ssEmpCost spreadSheetCellForIdentifier:@"B8"]];
	[cell setFunction:^NSNumber *(IMSpreadSheet *calculator, IMSpreadSheetCell *cell) {
		double val1 = [spreadSheet valueForIdentifier:@"C16"].doubleValue * [ssEmpCost valueForIdentifier:@"B8"].doubleValue / 60.0;
		return [NSNumber numberWithDouble:val1];
	}];
	
	cell = [spreadSheet spreadSheetCellForIdentifier:@"B26"];
	cell.watchList = @[[ssScreening spreadSheetCellForIdentifier:@"B5"]
					   ,[ssScreening spreadSheetCellForIdentifier:@"B18"]
					   ,[ssNewHire spreadSheetCellForIdentifier:@"B4"]];
	[cell setFunction:^NSNumber *(IMSpreadSheet *calculator, IMSpreadSheetCell *cell) {
		double val1 = [ssScreening valueForIdentifier:@"B5"].doubleValue * [ssScreening valueForIdentifier:@"B18"].doubleValue;
		double val2 = [ssNewHire valueForIdentifier:@"B4"].doubleValue * [ssScreening valueForIdentifier:@"B18"].doubleValue;
		double val3 = val1 + val2;
		return [NSNumber numberWithDouble:val3];
	}];
	
	cell = [spreadSheet spreadSheetCellForIdentifier:@"B27"];
	cell.watchList = @[[spreadSheet spreadSheetCellForIdentifier:@"B26"]
					   ,[ssScreening spreadSheetCellForIdentifier:@"B10"]
					   ,[ssEmpCost spreadSheetCellForIdentifier:@"B22"]];
	[cell setFunction:^NSNumber *(IMSpreadSheet *calculator, IMSpreadSheetCell *cell) {
		double val1 = [spreadSheet valueForIdentifier:@"B26"].doubleValue * [ssScreening valueForIdentifier:@"B10"].doubleValue * [ssEmpCost valueForIdentifier:@"B22"].doubleValue;
		return [NSNumber numberWithDouble:val1];
	}];
	
	cell = [spreadSheet spreadSheetCellForIdentifier:@"B28"];
	cell.watchList = [spreadSheet arrayOfCells:@[@"B9", @"B26"]];
	[cell setFunction:^NSNumber *(IMSpreadSheet *calculator, IMSpreadSheetCell *cell) {
		double val1 = [spreadSheet valueForIdentifier:@"B9"].doubleValue * [spreadSheet valueForIdentifier:@"B26"].doubleValue;
		return [NSNumber numberWithDouble:val1];
	}];
	
	cell = [spreadSheet spreadSheetCellForIdentifier:@"B29"];
	cell.watchList = [spreadSheet arrayOfCells:@[@"B8", @"B26"]];
	[cell setFunction:^NSNumber *(IMSpreadSheet *calculator, IMSpreadSheetCell *cell) {
		double val1 = [spreadSheet valueForIdentifier:@"B8"].doubleValue * [spreadSheet valueForIdentifier:@"B26"].doubleValue;
		return [NSNumber numberWithDouble:val1];
	}];
	
	cell = [spreadSheet spreadSheetCellForIdentifier:@"B30"];
	cell.watchList = @[[ssScreening spreadSheetCellForIdentifier:@"B5"]
					   ,[ssNewHire spreadSheetCellForIdentifier:@"B4"]
					   ,[ssNewHire spreadSheetCellForIdentifier:@"C15"]];
	[cell setFunction:^NSNumber *(IMSpreadSheet *calculator, IMSpreadSheetCell *cell) {
		double val1 = [ssScreening valueForIdentifier:@"B5"].doubleValue + [ssNewHire valueForIdentifier:@"B4"].doubleValue;
		double val2 = val1 * [ssNewHire valueForIdentifier:@"C15"].doubleValue;
		return [NSNumber numberWithDouble:val2];
	}];
	
	[spreadSheet spreadSheetCellForIdentifier:@"B31"].value = @0.0;
	
	cell = [spreadSheet spreadSheetCellForIdentifier:@"B32"];
	cell.watchList = [spreadSheet arrayOfCells:@[@"C9", @"B30"]];
	[cell setFunction:^NSNumber *(IMSpreadSheet *calculator, IMSpreadSheetCell *cell) {
		double val1 = [spreadSheet valueForIdentifier:@"C9"].doubleValue * [spreadSheet valueForIdentifier:@"B30"].doubleValue;
		return [NSNumber numberWithDouble:val1];
	}];
	
	cell = [spreadSheet spreadSheetCellForIdentifier:@"B33"];
	cell.watchList = [spreadSheet arrayOfCells:@[@"C8", @"B30"]];
	[cell setFunction:^NSNumber *(IMSpreadSheet *calculator, IMSpreadSheetCell *cell) {
		double val1 = [spreadSheet valueForIdentifier:@"C8"].doubleValue * [spreadSheet valueForIdentifier:@"B30"].doubleValue;
		return [NSNumber numberWithDouble:val1];
	}];
	
	cell = [spreadSheet spreadSheetCellForIdentifier:@"B34"];
	cell.watchList = @[[ssScreening spreadSheetCellForIdentifier:@"B5"]
					   ,[ssScreening spreadSheetCellForIdentifier:@"B15"]
					   ,[ssNewHire spreadSheetCellForIdentifier:@"B4"]
					   ,[ssNewHire spreadSheetCellForIdentifier:@"B12"]];
	[cell setFunction:^NSNumber *(IMSpreadSheet *calculator, IMSpreadSheetCell *cell) {
		double val1 = [ssScreening valueForIdentifier:@"B5"].doubleValue * [ssScreening valueForIdentifier:@"B15"].doubleValue;
		double val2 = [ssNewHire valueForIdentifier:@"B4"].doubleValue * [ssNewHire valueForIdentifier:@"B12"].doubleValue;
		return [NSNumber numberWithDouble:val1+val2];
	}];
	
	cell = [spreadSheet spreadSheetCellForIdentifier:@"B35"];
	cell.watchList = @[[ssScreening spreadSheetCellForIdentifier:@"B4"]
					   ,[ssSpotlight spreadSheetCellForIdentifier:@"B9"]
					   ,[ssNewHire spreadSheetCellForIdentifier:@"B4"]
					   ,[ssNewHire spreadSheetCellForIdentifier:@"C12"]];
	[cell setFunction:^NSNumber *(IMSpreadSheet *calculator, IMSpreadSheetCell *cell) {
		double val1 = [ssScreening valueForIdentifier:@"B4"].doubleValue * [ssSpotlight valueForIdentifier:@"B9"].doubleValue;
		double val2 = [ssNewHire valueForIdentifier:@"B4"].doubleValue * [ssNewHire valueForIdentifier:@"C12"].doubleValue;
		double val3 = val1 + val2;
		return [NSNumber numberWithDouble:val3];
	}];
	
	cell = [spreadSheet spreadSheetCellForIdentifier:@"B39"];
	cell.watchList = @[[ssEmpData spreadSheetCellForIdentifier:@"B15"]
					   ,[ssEmpData spreadSheetCellForIdentifier:@"B16"]
					   ,[ssEmpData spreadSheetCellForIdentifier:@"B17"]
					   ,[ssScreening spreadSheetCellForIdentifier:@"B5"]
					   ,[ssScreening spreadSheetCellForIdentifier:@"B15"]
					   ,[ssNewHire spreadSheetCellForIdentifier:@"B4"]
					   ,[ssNewHire spreadSheetCellForIdentifier:@"B12"]];
	[cell setFunction:^NSNumber *(IMSpreadSheet *calculator, IMSpreadSheetCell *cell) {
		double val1 = [ssEmpData valueForIdentifier:@"B15"].doubleValue + [ssEmpData valueForIdentifier:@"B16"].doubleValue + [ssEmpData valueForIdentifier:@"B17"].doubleValue;
		val1 = val1 / 60.0;
		double val2 = [ssScreening valueForIdentifier:@"B5"].doubleValue * [ssScreening valueForIdentifier:@"B15"].doubleValue;
		double val3 = [ssNewHire valueForIdentifier:@"B4"].doubleValue * [ssNewHire valueForIdentifier:@"B12"].doubleValue;
		double val4 = val1 * (val2 + val3);
		return [NSNumber numberWithDouble:val4];
	}];
	
	cell = [spreadSheet spreadSheetCellForIdentifier:@"C39"];
	cell.watchList = @[[ssEmpData spreadSheetCellForIdentifier:@"B15"]
					   ,[ssEmpData spreadSheetCellForIdentifier:@"B16"]
					   ,[ssEmpData spreadSheetCellForIdentifier:@"B17"]
					   ,[ssScreening spreadSheetCellForIdentifier:@"B5"]
					   ,[ssSpotlight spreadSheetCellForIdentifier:@"B9"]
					   ,[ssNewHire spreadSheetCellForIdentifier:@"B4"]
					   ,[ssNewHire spreadSheetCellForIdentifier:@"B12"]];
	[cell setFunction:^NSNumber *(IMSpreadSheet *calculator, IMSpreadSheetCell *cell) {
		double val1 = [ssEmpData valueForIdentifier:@"B15"].doubleValue + [ssEmpData valueForIdentifier:@"B16"].doubleValue + [ssEmpData valueForIdentifier:@"B17"].doubleValue;
		val1 = val1 / 60.0;
		double val2 = [ssScreening valueForIdentifier:@"B5"].doubleValue * [ssSpotlight valueForIdentifier:@"B9"].doubleValue;
		double val3 = [ssNewHire valueForIdentifier:@"B4"].doubleValue * [ssNewHire valueForIdentifier:@"B12"].doubleValue;
		double val4 = val1 * (val2 + val3);
		return [NSNumber numberWithDouble:val4];
	}];
	
	cell = [spreadSheet spreadSheetCellForIdentifier:@"B40"];
	cell.watchList = @[[ssEmpData spreadSheetCellForIdentifier:@"B25"]
					   ,[ssEmpData spreadSheetCellForIdentifier:@"B26"]
					   ,[ssScreening spreadSheetCellForIdentifier:@"B5"]
					   ,[ssScreening spreadSheetCellForIdentifier:@"B15"]
					   ,[ssNewHire spreadSheetCellForIdentifier:@"B4"]
					   ,[ssNewHire spreadSheetCellForIdentifier:@"C12"]];
	[cell setFunction:^NSNumber *(IMSpreadSheet *calculator, IMSpreadSheetCell *cell) {
		double val1 = ([ssEmpData valueForIdentifier:@"B25"].doubleValue + [ssEmpData valueForIdentifier:@"B26"].doubleValue) / 60.0;
		double val2 = [ssScreening valueForIdentifier:@"B5"].doubleValue * [ssScreening valueForIdentifier:@"B15"].doubleValue;
		double val3 = [ssNewHire valueForIdentifier:@"B4"].doubleValue * [ssNewHire valueForIdentifier:@"C12"].doubleValue;
		double val4 = val1 * (val2 + val3);
		return [NSNumber numberWithDouble:val4];
	}];
	
	cell = [spreadSheet spreadSheetCellForIdentifier:@"C40"];
	cell.watchList = @[[ssEmpData spreadSheetCellForIdentifier:@"B25"]
					   ,[ssEmpData spreadSheetCellForIdentifier:@"B26"]
					   ,[ssScreening spreadSheetCellForIdentifier:@"B5"]
					   ,[ssSpotlight spreadSheetCellForIdentifier:@"B9"]
					   ,[ssNewHire spreadSheetCellForIdentifier:@"B4"]
					   ,[ssNewHire spreadSheetCellForIdentifier:@"C12"]];
	[cell setFunction:^NSNumber *(IMSpreadSheet *calculator, IMSpreadSheetCell *cell) {
		double val1 = ([ssEmpData valueForIdentifier:@"B25"].doubleValue + [ssEmpData valueForIdentifier:@"B26"].doubleValue) / 60.0;
		double val2 = [ssScreening valueForIdentifier:@"B5"].doubleValue * [ssSpotlight valueForIdentifier:@"B9"].doubleValue;
		double val3 = [ssNewHire valueForIdentifier:@"B4"].doubleValue * [ssNewHire valueForIdentifier:@"C12"].doubleValue;
		double val4 = val1 * (val2 + val3);
		return [NSNumber numberWithDouble:val4];
	}];
	
	cell = [spreadSheet spreadSheetCellForIdentifier:@"B43"];
	cell.watchList = @[[spreadSheet spreadSheetCellForIdentifier:@"B34"]
					   ,[ssScreening spreadSheetCellForIdentifier:@"B16"]];
	[cell setFunction:^NSNumber *(IMSpreadSheet *calculator, IMSpreadSheetCell *cell) {
		double val1 = [spreadSheet valueForIdentifier:@"B34"].doubleValue * [ssScreening valueForIdentifier:@"B16"].doubleValue;
		return [NSNumber numberWithDouble:val1];
	}];
	
	cell = [spreadSheet spreadSheetCellForIdentifier:@"C43"];
	cell.watchList = @[[spreadSheet spreadSheetCellForIdentifier:@"B35"]
					   ,[ssScreening spreadSheetCellForIdentifier:@"B16"]];
	[cell setFunction:^NSNumber *(IMSpreadSheet *calculator, IMSpreadSheetCell *cell) {
		double val1 = [spreadSheet valueForIdentifier:@"B35"].doubleValue * [ssScreening valueForIdentifier:@"B16"].doubleValue;
		return [NSNumber numberWithDouble:val1];
	}];
	
	cell = [spreadSheet spreadSheetCellForIdentifier:@"B46"];
	cell.watchList = @[[ssScreening spreadSheetCellForIdentifier:@"B5"]
					   ,[ssSpotlight spreadSheetCellForIdentifier:@"B6"]];
	[cell setFunction:^NSNumber *(IMSpreadSheet *calculator, IMSpreadSheetCell *cell) {
		double val1 = [ssScreening valueForIdentifier:@"B5"].doubleValue / [ssSpotlight valueForIdentifier:@"B6"].doubleValue;
		return [NSNumber numberWithDouble:val1];
	}];
	
	cell = [spreadSheet spreadSheetCellForIdentifier:@"B47"];
	cell.watchList = @[[spreadSheet spreadSheetCellForIdentifier:@"B46"]
					   ,[ssSpotlight spreadSheetCellForIdentifier:@"B6"]];
	[cell setFunction:^NSNumber *(IMSpreadSheet *calculator, IMSpreadSheetCell *cell) {
		double val1 = [spreadSheet valueForIdentifier:@"B46"].doubleValue;
		double val2 = 0.0;
		if(val1 >= 5000)
		{
			val2 = 8.0;
		}else if(val1 >= 4000){
			val2 = 7.0;
		}else if(val1 >= 3000){
			val2 = 6.0;
		}else if(val1 >= 2500){
			val2 = 5.0;
		}else if(val1 >= 2000){
			val2 = 4.0;
		}else if(val1 >= 1000){
			val2 = 3.0;
		}else if(val1 >= 500){
			val2 = 2.0;
		}else if(val1 >= 250){
			val2 = 1.0;
		}
		
		double val3 = val2 * [ssSpotlight valueForIdentifier:@"B6"].doubleValue;
		return [NSNumber numberWithDouble:val3];
	}];
}


- (void) initEmployeeTimeReferenceData
{
	IMSpreadSheet *ssEmployeeTime = [self spreadSheetWithName:kIMSvQEmpTimeData];
	IMSpreadSheetCell *cell;
	NSArray *values = @[@5.0, @7.5, @6.7, @7.4, @10.0, @6.2, @13.7, @5.0, @25.0];
	for(int i=9;i<18;i++)
	{
		cell = [ssEmployeeTime spreadSheetCellForIdentifier:[NSString stringWithFormat:@"B%d", i]];
		cell.value = values[i-9];
	}
	
	values = @[@22.9, @22.3, @23.6, @15.0, @33.2, @45.0];
	for(int i=21;i<27;i++)
	{
		cell = [ssEmployeeTime spreadSheetCellForIdentifier:[NSString stringWithFormat:@"B%d", i]];
		cell.value = values[i-21];
	}
	
	cell = [ssEmployeeTime spreadSheetCellForIdentifier:@"B35"];
	cell.value = @4.0;
	cell = [ssEmployeeTime spreadSheetCellForIdentifier:@"B36"];
	cell.value = @1.0;
	cell = [ssEmployeeTime spreadSheetCellForIdentifier:@"B39"];
	cell.value = @8.0;
}

@end
