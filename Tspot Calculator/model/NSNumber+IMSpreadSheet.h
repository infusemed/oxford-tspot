//
//  NSNumber+IMSpreadSheet.h
//  IMSpreadSheet
//
//  Created by Anthony Long on 4/18/13.
//  Copyright (c) 2013 Infuse Medical. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSNumber (IMSpreadSheet)

- (NSString *) percentStringValue;
- (NSString *) numericValue;
- (NSString *) currencyValue;
- (NSString *) intStringValue;
- (NSString *) intCurrencyValue;

+ (NSNumber *) numberWithCurrencyString:(NSString *) currencyString;
+ (NSNumber *) numberWithPercentString:(NSString *) percentString;
+ (NSNumber *) numberWithNumericString:(NSString *) numberString;

@end
