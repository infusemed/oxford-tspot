//
//  IMAnalysisWorkbook.h
//  Practice Productivity Analysis
//
//  Created by Anthony Long on 5/1/13.
//  Copyright (c) 2013 Infuse Medical. All rights reserved.
//

#import "IMSpreadSheet.h"

@interface IMSpreadSheet(PPA)

@end

@interface IMSpotlightEconWorkbook : IMWorkbook

extern NSString * const kIMAnnualTBScreeningProgramSheet;
extern NSString * const kIMSPOTLIGHTSheet;
extern NSString * const kIMNewHireScreeningSheet;
extern NSString * const kIMEmployeeCostSheet;
extern NSString * const kIMAnalysisSheet;
extern NSString * const kIMEmployeeTimeReferenceDataSheet;
extern NSString * const kIMSpotlightReferenceDataSheet;
extern NSString * const kIMChartsAndTotals;

@end