//
//  IMCostOfIndeterminates.m
//  Tspot Calculator
//
//  Created by Anthony Long on 6/25/14.
//  Copyright (c) 2014 Infuse Medical. All rights reserved.
//

#import "IMCostOfIndeterminates.h"

@implementation IMCostOfIndeterminates
//
//- (id) init
//{
//	self = [super init];
//	if(self)
//	{
//		[self initializeWorkbook];
//	}
//	
//	return self;
//}
//
//- (id) initWithFile:(NSString *)filePath
//{
//	self = [super init];
//	if(self)
//	{
//		self.fileName = filePath.lastPathComponent;
//		[self initializeWorkbook];
//		NSDictionary * d = [NSDictionary dictionaryWithContentsOfFile:filePath];
//		[d enumerateKeysAndObjectsUsingBlock:^(id key, id obj, BOOL *stop) {
//			NSDictionary * d2 = (NSDictionary *)obj;
//			IMSpreadSheet * spreadsheet = [self spreadSheetWithName:(NSString *)key];
//			[d2 enumerateKeysAndObjectsUsingBlock:^(id key, id obj, BOOL *stop) {
//				NSArray * a = [(NSString *)key componentsSeparatedByString:@"-"];
//				NSUInteger indexes[] = {[[a objectAtIndex:0] intValue], [[a objectAtIndex:1] intValue]};
//				if([obj isKindOfClass:[NSString class]])
//				{
//					[spreadsheet setTextValue:obj forIndexPath:[NSIndexPath indexPathWithIndexes:indexes length:2]];
//				}
//				else
//				{
//					[spreadsheet setValue:obj forIndexPath:[NSIndexPath indexPathWithIndexes:indexes length:2]];
//				}
//			}];
//		}];
//	}
//	
//	return self;
//}

- (void) setCustomerName:(NSString *)customerName
{
	[super setCustomerName:customerName];
	[[self spreadSheetWithName:@"Sheet1"] spreadSheetCellForIdentifier:@"B3"].textValue = customerName;
}

- (NSString *) customerName
{
	return [[self spreadSheetWithName:@"Sheet1"] spreadSheetCellForIdentifier:@"B3"].textValue;
}


- (void) initializeWorkbook
{
	[super initializeWorkbook];
	[self setSpreadSheet:[[IMSpreadSheet alloc] init] WithName:@"Sheet1"];
	IMSpreadSheet *spreadsheet = [self spreadSheetWithName:@"Sheet1"];
	IMSpreadSheetCell * cell = [spreadsheet spreadSheetCellForIdentifier:@"B6"];
	[cell setSetterFunction:^(IMSpreadSheetCell *cell) {
		[spreadsheet spreadSheetCellForIdentifier:@"D6"].value = cell.value;
	}];
	[spreadsheet spreadSheetCellForIdentifier:@"D6"];
	[spreadsheet spreadSheetCellForIdentifier:@"B8"];
	[spreadsheet spreadSheetCellForIdentifier:@"D8"].value = @0.006;
	[spreadsheet spreadSheetCellForIdentifier:@"B10"];
	[spreadsheet spreadSheetCellForIdentifier:@"D10"];
	
	cell = [spreadsheet spreadSheetCellForIdentifier:@"B18"];
	cell.watchList = [spreadsheet arrayOfCells:@[@"B6", @"B8"]];
	[cell setFunction:^NSNumber *(IMSpreadSheet *calculator, IMSpreadSheetCell *cell) {
		return [calculator multiplyCells:@[@"B6", @"B8"]];
	}];
	
	cell = [spreadsheet spreadSheetCellForIdentifier:@"D18"];
	cell.watchList = [spreadsheet arrayOfCells:@[@"B6", @"D8"]];
	[cell setFunction:^NSNumber *(IMSpreadSheet *calculator, IMSpreadSheetCell *cell) {
		return [NSNumber numberWithDouble:[calculator valueForIdentifier:@"B6"].doubleValue * [calculator valueForIdentifier:@"D8"].doubleValue];
	}];
	
	cell = [spreadsheet spreadSheetCellForIdentifier:@"E18"];
	cell.watchList = [spreadsheet arrayOfCells:@[@"B18", @"D18"]];
	[cell setFunction:^NSNumber *(IMSpreadSheet *calculator, IMSpreadSheetCell *cell) {
		double val1 = 1.0 - ([spreadsheet valueForIdentifier:@"D18"].doubleValue / [spreadsheet valueForIdentifier:@"B18"].doubleValue);
		return [NSNumber numberWithDouble:val1];
	}];
	
	cell = [spreadsheet spreadSheetCellForIdentifier:@"F18"];
	cell.watchList = [spreadsheet arrayOfCells:@[@"B18", @"D18"]];
	[cell setFunction:^NSNumber *(IMSpreadSheet *calculator, IMSpreadSheetCell *cell) {
		double val1 = [spreadsheet valueForIdentifier:@"B18"].doubleValue - [spreadsheet valueForIdentifier:@"D18"].doubleValue;
		return [NSNumber numberWithDouble:val1];
	}];
	
	cell = [spreadsheet spreadSheetCellForIdentifier:@"B20"];
	cell.watchList = [spreadsheet arrayOfCells:@[@"B18", @"B10"]];
	[cell setFunction:^NSNumber *(IMSpreadSheet *calculator, IMSpreadSheetCell *cell) {
		return [calculator multiplyCells:@[@"B18", @"B10"]];
	}];
	
	[spreadsheet spreadSheetCellForIdentifier:@"D20"].value = @0.0;
	
	cell = [spreadsheet spreadSheetCellForIdentifier:@"B22"];
	cell.watchList = [spreadsheet arrayOfCells:@[@"B6", @"B10", @"B20"]];
	[cell setFunction:^NSNumber *(IMSpreadSheet *calculator, IMSpreadSheetCell *cell) {
		double val1 = [spreadsheet valueForIdentifier:@"B6"].doubleValue * [spreadsheet valueForIdentifier:@"B10"].doubleValue;
		double val2 = [spreadsheet valueForIdentifier:@"B20"].doubleValue;
		double val3 = [spreadsheet valueForIdentifier:@"B6"].doubleValue;
		return [NSNumber numberWithDouble:(val1 + val2)/val3];
	}];
	
	cell = [spreadsheet spreadSheetCellForIdentifier:@"D22"];
	cell.watchList = [spreadsheet arrayOfCells:@[@"D6", @"D10", @"D20"]];
	[cell setFunction:^NSNumber *(IMSpreadSheet *calculator, IMSpreadSheetCell *cell) {
		double val1 = [spreadsheet valueForIdentifier:@"D6"].doubleValue * [spreadsheet valueForIdentifier:@"D10"].doubleValue;
		double val2 = [spreadsheet valueForIdentifier:@"D21"].doubleValue;
		double val3 = [spreadsheet valueForIdentifier:@"D6"].doubleValue;
		return [NSNumber numberWithDouble:(val1 + val2)/val3];
	}];
	
	cell = [spreadsheet spreadSheetCellForIdentifier:@"E22"];
	cell.watchList = [spreadsheet arrayOfCells:@[@"B22", @"D22"]];
	[cell setFunction:^NSNumber *(IMSpreadSheet *calculator, IMSpreadSheetCell *cell) {
		double val1 = 1.0 - ([spreadsheet valueForIdentifier:@"D22"].doubleValue / [spreadsheet valueForIdentifier:@"B22"].doubleValue);
		return [NSNumber numberWithDouble:val1];
	}];
}

@end
