//
//  UIView+IMFrame.m
//  EOS Product Sim
//
//  Created by Anthony Long on 11/25/14.
//  Copyright (c) 2014 Infuse Medical. All rights reserved.
//

#import "UIView+IMFrame.h"



@implementation UIView (IMFrame)

- (CGFloat) frameWidth
{
	return self.frame.size.width;
}

- (void) setFrameWidth:(CGFloat)frameWidth
{
	self.frame = CGRectMake(self.frame.origin.x, self.frame.origin.y, frameWidth, self.frame.size.height);
}

- (CGFloat) frameHeight
{
	return self.frame.size.height;
}

- (void) setFrameHeight:(CGFloat)frameHeight
{
	self.frame = CGRectMake(self.frame.origin.x, self.frame.origin.y, self.frame.size.width, frameHeight);
}

- (CGFloat) frameX
{
	return self.frame.origin.x;
}

- (void) setFrameX:(CGFloat)frameX
{
	self.frame = CGRectMake(frameX, self.frame.origin.y, self.frame.size.width, self.frame.size.height);
}

- (CGFloat) frameY
{
	return self.frame.origin.y;
}

- (void) setFrameY:(CGFloat)frameY
{
	self.frame = CGRectMake(self.frame.origin.x, frameY, self.frame.size.width, self.frame.size.height);
}

//- (CGFloat)cornerRadius
//{
//	return self.layer.cornerRadius;
//}
//
//- (void)setCornerRadius:(CGFloat)cornerRadius {
//	self.layer.cornerRadius = cornerRadius;
//}
//
//- (void) setBorderColor:(UIColor *)borderColor
//{
//	self.layer.borderColor = borderColor.CGColor;
//}
//
//- (UIColor *)borderColor
//{
//	return [UIColor colorWithCGColor:self.layer.borderColor];
//}
//
//- (void) setBorderSize:(CGFloat)borderSize
//{
//	self.layer.borderWidth = borderSize;
//}
//
//- (CGFloat) borderSize
//{
//	return self.layer.borderWidth;
//}

@end
