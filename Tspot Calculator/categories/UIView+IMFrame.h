//
//  UIView+IMFrame.h
//  EOS Product Sim
//
//  Created by Anthony Long on 11/25/14.
//  Copyright (c) 2014 Infuse Medical. All rights reserved.
//

#import <UIKit/UIKit.h>


@interface UIView (IMFrame)

@property (nonatomic) CGFloat frameWidth;
@property (nonatomic) CGFloat frameHeight;
@property (nonatomic) CGFloat frameX;
@property (nonatomic) CGFloat frameY;

//@property (nonatomic) IBInspectable CGFloat cornerRadius;
//@property (nonatomic, strong) IBInspectable UIColor *borderColor;
//@property (nonatomic) IBInspectable CGFloat borderSize;

@end