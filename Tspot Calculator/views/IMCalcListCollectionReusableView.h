//
//  IMCalcListCollectionReusableView.h
//  Tspot Calculator
//
//  Created by Anthony Long on 3/31/15.
//  Copyright (c) 2015 Infuse Medical. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface IMCalcListCollectionReusableView : UICollectionReusableView

@property (nonatomic, strong) UILabel *textLabel;

@end
