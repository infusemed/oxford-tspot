//
//  IMCalcListCollectionReusableView.m
//  Tspot Calculator
//
//  Created by Anthony Long on 3/31/15.
//  Copyright (c) 2015 Infuse Medical. All rights reserved.
//

#import "IMCalcListCollectionReusableView.h"

@implementation IMCalcListCollectionReusableView

- (instancetype)initWithCoder:(NSCoder *)aDecoder
{
	self = [super initWithCoder:aDecoder];
	if(self)
	{
		
	}
	
	return self;
}

- (instancetype)initWithFrame:(CGRect)frame
{
	self = [super initWithFrame:frame];
	if(self)
	{
		
	}
	
	return self;
}

- (UILabel *)textLabel
{
	if(!_textLabel)
	{
		_textLabel = [[UILabel alloc] initWithFrame:CGRectZero];
		_textLabel.font = [UIFont boldSystemFontOfSize:19.0f];
		_textLabel.textColor = [UIColor whiteColor];
		_textLabel.backgroundColor = [UIColor clearColor];
		_textLabel.shadowColor = [UIColor colorWithRed:51.0f/255.0f
												  green:28.0f/255.0f
												   blue:104.0f/255.0f
												  alpha:0.3f];
		_textLabel.shadowOffset = CGSizeMake(2.0, 1.7f);
		[self addSubview:_textLabel];
	}
	
	return _textLabel;
}

- (void) layoutSubviews
{
	[super layoutSubviews];
	_textLabel.frame = self.bounds;
}

@end
