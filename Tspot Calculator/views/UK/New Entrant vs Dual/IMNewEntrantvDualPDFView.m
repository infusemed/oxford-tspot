//
//  IMNewEntrantvDualPDFView.m
//  Tspot Calculator
//
//  Created by Jeff Spaulding on 5/8/15.
//  Copyright (c) 2015 Infuse Medical. All rights reserved.
//

#import "IMNewEntrantvDualPDFView.h"
@interface IMNewEntrantvDualPDFView()
@property (strong, nonatomic) IBOutletCollection(UITextView) NSArray *borderedTextViews;
@end

@implementation IMNewEntrantvDualPDFView

-(void)awakeFromNib{
    for (UITextView* borderView in _borderedTextViews) {
        [borderView.layer setBorderColor:[UIColor darkGrayColor].CGColor];
        [borderView.layer setBorderWidth:1];
    }
}
@end
