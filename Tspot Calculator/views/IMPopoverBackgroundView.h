//
//  IMPopoverBackgroundView.h
//  LeadManagment
//
//  Created by Anthony Long on 3/13/13.
//  Copyright (c) 2013 Infuse Medical. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <QuartzCore/QuartzCore.h>

@interface IMPopoverBackgroundView : UIPopoverBackgroundView

@end
