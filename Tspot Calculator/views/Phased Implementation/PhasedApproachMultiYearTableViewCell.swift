//
//  PhasedApproachMultiYearTableViewCell.swift
//  Tspot Calculator
//
//  Created by Anthony Long on 6/15/16.
//  Copyright © 2016 Infuse Medical. All rights reserved.
//

import UIKit

enum PhasedApproachMultiYearCellMoveDirection:CGFloat {
	case left = -160
	case right = 160
}

class PhasedApproachMultiYearTableViewCell: UITableViewCell {
	
	@IBOutlet weak var tfPopulationName: UITextField!
	@IBOutlet var tfPopulationCounts: [UITextField]!
	
	func moveFields (_ direction: PhasedApproachMultiYearCellMoveDirection) {
		let xoffset = direction.rawValue
		UIView.animate(withDuration: 0.3, animations: { [unowned self] in
			
			for tf in self.tfPopulationCounts {
				var rect = tf.frame
				rect.origin.x += xoffset
				tf.frame = rect
			}
			
			}, completion: {[unowned self] (completed) in
				if direction == .left {
					self.tfPopulationCounts.append(self.tfPopulationCounts.removeFirst())
					var rect = self.tfPopulationCounts.last!.frame
					rect.origin.x = 498.0
					self.tfPopulationCounts.last?.frame = rect
				}else{
					self.tfPopulationCounts.insert(self.tfPopulationCounts.removeLast(), at: 0)
					var rect = self.tfPopulationCounts.last!.frame
					rect.origin.x = -142.0
					self.tfPopulationCounts.first?.frame = rect
				}
		}) 
	}
	
	override func awakeFromNib() {
		super.awakeFromNib()
		// Initialization code
	}
	
	override func setSelected(_ selected: Bool, animated: Bool) {
		super.setSelected(selected, animated: animated)
		
		// Configure the view for the selected state
	}

}
