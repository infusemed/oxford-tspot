//
//  IMPhasedPDFTableViewCell.swift
//  Tspot Calculator
//
//  Created by Anthony Long on 6/22/16.
//  Copyright © 2016 Infuse Medical. All rights reserved.
//

import UIKit

class IMPhasedPDFTableViewCell: UITableViewCell {
	
	@IBOutlet weak var labelPopulation: UILabel!
	@IBOutlet weak var labelYear1: UILabel!
	@IBOutlet weak var labelYear2: UILabel!
	@IBOutlet weak var labelYear3: UILabel!
	

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
