//
//  PhasedApproachPhasedResultsForPDFView.swift
//  Tspot Calculator
//
//  Created by Anthony Long on 6/23/16.
//  Copyright © 2016 Infuse Medical. All rights reserved.
//

import UIKit

class PhasedApproachPhasedResultsForPDFView: UIView, PDFEditorDrawableView {
	
	@IBOutlet weak var viewU: UIView!
	@IBOutlet weak var viewT: UIView!
	
	var keyValues: NSObject?
}
