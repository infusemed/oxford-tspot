//
//  PhasedApproachPhasedImpPDFTableView.swift
//  Tspot Calculator
//
//  Created by Anthony Long on 6/23/16.
//  Copyright © 2016 Infuse Medical. All rights reserved.
//

import UIKit

class PhasedApproachPhasedImpPDFTableView: UIView, PDFEditorDrawableView, UITableViewDataSource, UITableViewDelegate {

	let cellId = "PhasedCell"
	
	@IBOutlet weak var viewHeader: UIView?
	@IBOutlet weak var viewTotals: PhasedApproachPhasedResultsForPDFView!
	@IBOutlet weak var labelYear5: UILabel?
	@IBOutlet weak var labelYear6: UILabel?
	
	
	let yearColumns = ["P", "Q", "R", "S", "T", "U", "V", "W", "X", "Y"]
	
	var keyValues: NSObject? {
		didSet {
			self.columns = []
			let offset = self.tag == 0 ? 0 : 3
			let totalYears = (keyValues!.value(forKey: "N39")! as! NSNumber).intValue
			let end = self.tag == 0 ? min(3, totalYears) : totalYears
			if offset > end {
				return
			}
			for i in offset..<end {
				self.columns.append(self.yearColumns[i])
			}
			self.tableView.reloadData()
			var r = self.tableView.frame
			r.size.height = 96.0 * CGFloat((keyValues!.value(forKey: "M39")! as! NSNumber).intValue)
			self.tableView.frame = r
			r = self.viewTotals.frame
			r.origin.y = self.tableView.frame.maxY + 26.0
			self.viewTotals.frame = r
			r = self.frame
			r.size.height = self.viewTotals.frame.maxY
			self.frame = r
			
			self.labelYear5?.isHidden = end < offset + 2
			self.labelYear6?.isHidden = end < offset + 3
			self.viewTotals.viewT.isHidden = self.labelYear5!.isHidden
			self.viewTotals.viewU.isHidden = self.labelYear6!.isHidden
		}
	}
	
	var columns: [String] = []
	
	@IBOutlet var tableView:UITableView!
	
	let rowHeight = 96.0
	
	func numberOfSections(in tableView: UITableView) -> Int {
		return 1
	}
	
	func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
		if let vals = keyValues {
			return (vals.value(forKey: "M39")! as AnyObject).intValue
		}
		return 0
	}
	
	func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
		let cell = tableView.dequeueReusableCell(withIdentifier: cellId, for: indexPath) as! IMPhasedPDFTableViewCell
		let rowToUse = (indexPath as NSIndexPath).row
		cell.labelPopulation.oxfordText = keyValues?.value(forKey: "M\(rowToUse + 40)") as? String
		
		if columns.count >= 1, let val = keyValues?.value(forKey: "\(columns[0])\(rowToUse + 40)") as? NSNumber {
			cell.labelYear1.text = val.intStringValue()
		}else{
			cell.labelYear1.text = ""
		}
		
		if columns.count >= 2, let val = keyValues?.value(forKey: "\(columns[1])\(rowToUse + 40)") as? NSNumber {
			cell.labelYear2.text = val.intStringValue()
		}else{
			cell.labelYear2.text = ""
		}
		
		if columns.count == 3, let val = keyValues?.value(forKey: "\(columns[2])\(rowToUse + 40)") as? NSNumber {
			cell.labelYear3.text = val.intStringValue()
		}else{
			cell.labelYear3.text = ""
		}
		
		return cell
	}
	
	func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
		if (indexPath as NSIndexPath).row % 2 == 0 {
			cell.backgroundColor = UIColor.white
		}else{
			cell.backgroundColor = UIColor(white: 0.92, alpha: 1.0)
		}
	}
	
	override func awakeFromNib() {
		self.tableView.register(UINib(nibName: "IMPhasedPDFTableViewCell", bundle: nil), forCellReuseIdentifier: cellId)
		self.tableView.delegate = self
		self.tableView.layer.borderColor = UIColor(white: 0.572, alpha: 1.0).cgColor
		self.tableView.layer.borderWidth = 4.0
	}

}
