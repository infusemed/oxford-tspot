//
//  PhasedApproachPDFTableView.swift
//  Tspot Calculator
//
//  Created by Anthony Long on 6/22/16.
//  Copyright © 2016 Infuse Medical. All rights reserved.
//

import UIKit

class PhasedApproachImmediateImpPDFTableView: UIView, PDFEditorDrawableView, UITableViewDataSource, UITableViewDelegate {
	
	let cellId = "PhasedCell"
	
	@IBOutlet weak var viewHeader: UIView!
	@IBOutlet weak var viewTotals: UIView!
	
	
	var keyValues: NSObject? {
		didSet {
			self.tableView.reloadData()
			var r = self.tableView.frame
			r.size.height = 96.0 * CGFloat((keyValues!.value(forKey: "M39")! as AnyObject).intValue - (badRowNumber == 100 ? 0 : 1))
			self.tableView.frame = r
			r = self.frame
			r.size.height = self.tableView.frame.maxY + 346
			self.frame = r
			r = self.viewTotals.frame
			r.origin.y = self.tableView.frame.maxY
			self.viewTotals.frame = r
			if let vt = self.viewTotals as? PhasedApproachPhasedResultsForPDFView {
				vt.keyValues = keyValues
			}
		}
	}
	
	@IBOutlet var tableView:UITableView!

	let rowHeight = 96.0
	
	func numberOfSections(in tableView: UITableView) -> Int {
		return 1
	}
	
	var badRowNumber = 100
	
	func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
		if let vals = keyValues {
			for cell in 40..<55 {
				let textVal = vals.value(forKey: "M\(cell)") as? String
				if textVal == "Annual employees who received the T-SPOT.TB test the previous year upon new hire" {
					badRowNumber = cell
					return (vals.value(forKey: "M39")! as AnyObject).intValue - 1
				}
			}
			return (vals.value(forKey: "M39")! as AnyObject).intValue
		}
		return 0
	}
	
	func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
		let cell = tableView.dequeueReusableCell(withIdentifier: cellId, for: indexPath) as! IMPhasedPDFTableViewCell
		let rowToUse = (indexPath as NSIndexPath).row + 40 < badRowNumber ? (indexPath as NSIndexPath).row + 40 : (indexPath as NSIndexPath).row + 41
		cell.labelPopulation.oxfordText = keyValues?.value(forKey: "M\(rowToUse)") as? String
		cell.labelYear1.isHidden = true
		cell.labelYear2.isHidden = true
		if let val = keyValues?.value(forKey: "N\(rowToUse)") as? NSNumber {
			cell.labelYear3.text = val.intStringValue()
		}else{
			cell.labelYear3.text = ""
		}
		
		return cell
	}
	
	func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
		if (indexPath as NSIndexPath).row % 2 == 0 {
			cell.backgroundColor = UIColor.white
		}else{
			cell.backgroundColor = UIColor(white: 0.92, alpha: 1.0)
		}
	}
	
	override func awakeFromNib() {
		self.tableView.register(UINib(nibName: "IMPhasedPDFTableViewCell", bundle: nil), forCellReuseIdentifier: cellId)
		self.tableView.delegate = self
		self.tableView.layer.borderColor = UIColor(white: 0.45, alpha: 1.0).cgColor
		self.tableView.layer.borderWidth = 4.0
	}
	
}
