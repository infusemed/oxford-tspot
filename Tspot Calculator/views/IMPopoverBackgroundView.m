//
//  IMPopoverBackgroundView.m
//  LeadManagment
//
//  Created by Anthony Long on 3/13/13.
//  Copyright (c) 2013 Infuse Medical. All rights reserved.
//

#import "IMPopoverBackgroundView.h"

@implementation IMPopoverBackgroundView
{
	UIImageView * _arrowView;
	UILabel *_labelHeader;
	UIView *_bgView;
	UIPopoverArrowDirection _arrowDirection;
	CGFloat _arrowOffset;
}

+ (CGFloat)arrowHeight
{
	return 10.0f;
}

+ (CGFloat)arrowBase
{
	return 20.0f;
}

+ (UIEdgeInsets)contentViewInsets
{
	return UIEdgeInsetsMake(2.0f, 2.0f, 2.0f, 2.0f);
}

- (void) setArrowDirection:(UIPopoverArrowDirection)arrowDirection
{
	_arrowDirection = arrowDirection;
	[self setNeedsLayout];
}

- (UIPopoverArrowDirection) arrowDirection
{
	return _arrowDirection;
}

- (void) setArrowOffset:(CGFloat)arrowOffset
{
	_arrowOffset = arrowOffset;
	[self setNeedsLayout];
}

- (CGFloat) arrowOffset
{
	return _arrowOffset;
}

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
		_arrowOffset = 0.0f;
        _bgView = [[UIView alloc] init];
		_bgView.layer.borderWidth = 3.0f;
		_bgView.layer.borderColor = [UIColor colorWithRed:154.0f/255.0f
													green:159.0f/255.0f
													 blue:207.0f/255.0f
													alpha:1.0f].CGColor;
		_bgView.layer.masksToBounds = YES;
		_bgView.layer.cornerRadius = 12.0f;
		_bgView.backgroundColor = [UIColor whiteColor];
		_arrowView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"popover_arrow.png"]];
		[self addSubview:_bgView];
		[self addSubview:_arrowView];
		self.layer.shadowColor = [UIColor blackColor].CGColor;
		self.layer.shadowOpacity = 0.6;
		self.layer.shadowRadius = 10.0f;
		self.layer.shadowOffset = CGSizeMake(1.0f, 1.0f);
    }
    return self;
}

- (void) layoutSubviews
{
	CGFloat arrowHeight = [self.class arrowHeight];
	CGFloat arrowWidth = [self.class arrowBase];
	if(_arrowDirection == UIPopoverArrowDirectionDown)
	{
		_bgView.frame = CGRectMake(0.0f, 0.0, self.bounds.size.width, self.bounds.size.height-arrowHeight);
		_arrowView.transform = CGAffineTransformMakeRotation(M_PI);
		_arrowView.frame = CGRectMake((CGRectGetMidX(self.bounds) + _arrowOffset) - (arrowWidth/2), self.bounds.size.height-arrowHeight, arrowWidth, arrowHeight);
	}
	else if(_arrowDirection == UIPopoverArrowDirectionLeft)
	{
		_bgView.frame = CGRectMake(arrowHeight, 0.0, self.bounds.size.width-arrowHeight, self.bounds.size.height);
		_arrowView.transform = CGAffineTransformMakeRotation(-M_PI_2);
		_arrowView.center = CGPointMake(arrowHeight/2.0, CGRectGetMidY(self.bounds) + _arrowOffset);
	}
	else if(_arrowDirection == UIPopoverArrowDirectionRight)
	{
		_bgView.frame = CGRectMake(0.0f, 0.0, self.bounds.size.width-arrowHeight, self.bounds.size.height);
		_arrowView.transform = CGAffineTransformMakeRotation(M_PI_2);
		_arrowView.center = CGPointMake(self.bounds.size.width-(arrowHeight/2.0), CGRectGetMidY(self.bounds) + _arrowOffset);
	}
	else
	{
		_bgView.frame = CGRectMake(0.0f, arrowHeight, self.bounds.size.width, self.bounds.size.height-arrowHeight);
		_arrowView.frame = CGRectMake((CGRectGetMidX(self.bounds) + _arrowOffset) - (arrowWidth/2), 0.0f, arrowWidth, arrowHeight);
	}
}


//// to remove inner shadow
//- (void)willMoveToWindow:(UIWindow *)newWindow {
//    [super willMoveToWindow:newWindow];
////    if ([UIPopoverBackgroundView respondsToSelector:@selector(wantsDefaultContentAppearance)])
////        return;
//	
////    if (![[self class] wantsDefaultContentAppearance]) {
//        for (UIView *view in self.superview.subviews) {
//            for (UIView *subview in view.subviews) {
//                if (subview.layer.cornerRadius != 0.0) {
//					//remove corner radius from popover contents
////					subview.layer.cornerRadius = 0.0;
//                }
//				for (UIView *subsubview in subview.subviews) {
//					if (subsubview.class == [UIImageView class] && subsubview != _ivHeader)
//					{
////						subsubview.hidden = YES;
//					}
//					if(subsubview.layer.cornerRadius > 0.0)
//					{
////						subsubview.layer.cornerRadius = 0.0;
//					}
//				}
//            }
//        }
////    }
//}

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect
{
    // Drawing code
}
*/

@end
