//
//  PhasedApproachTableViewCell.swift
//  Tspot Calculator
//
//  Created by Anthony Long on 6/9/16.
//  Copyright © 2016 Infuse Medical. All rights reserved.
//

import UIKit

class PhasedApproachTableViewCell: UITableViewCell {
	
	
	@IBOutlet weak var tfPopulationName: UITextField!
	@IBOutlet weak var tfPopulationCount: UITextField!
	@IBOutlet weak var buttonDelete: UIButton!
	

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
