//
//  IMBarGraphView.swift
//  Tspot Calculator
//
//  Created by Anthony Long on 9/19/16.
//  Copyright © 2016 Infuse Medical. All rights reserved.
//

import UIKit

class IMBarGraphView: UIView {
	
	var barTitles = [String]()
	private var barTitleLabels = [UILabel]()
	private var barValueLabels = [UILabel]()
	
	private var maxValue: Double {
		var _maxValue = 0.0
		if let aBarValues = self.barValues {
			for barVal in aBarValues {
				var totalBarVal = 0.0
				for val in barVal.1 {
					totalBarVal += val
				}
				_maxValue = Double.maximum(_maxValue, totalBarVal)
			}
		}
		return _maxValue
	}
	
	var barValues: [(String, [Double])]? = nil {
		didSet {
			self.setNeedsLayout()
		}
	}
	
	
	override func layoutSubviews() {
		super.layoutSubviews()
		
	}
	
    /*
    // Only override draw() if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    override func draw(_ rect: CGRect) {
        // Drawing code
    }
    */

}
