//
//  IMSpreadSheetLabel.m
//  Practice Productivity Analysis
//
//  Created by Anthony Long on 9/6/13.
//  Copyright (c) 2013 Infuse Medical. All rights reserved.
//

#import "IMSpreadSheetLabel.h"

@implementation IMSpreadSheetLabel

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
    }
    return self;
}

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect
{
    // Drawing code
}
*/

@end
