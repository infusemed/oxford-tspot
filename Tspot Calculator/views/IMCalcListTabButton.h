//
//  IMCalcListTabButton.h
//  Tspot Calculator
//
//  Created by Anthony Long on 4/1/15.
//  Copyright (c) 2015 Infuse Medical. All rights reserved.
//

#import <UIKit/UIKit.h>

IB_DESIGNABLE

@interface IMCalcListTabButton : UIButton

@end
