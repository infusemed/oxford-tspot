//
//  IMSessionTableViewCell.h
//  Tspot Calculator
//
//  Created by Anthony Long on 6/25/14.
//  Copyright (c) 2014 Infuse Medical. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface IMSessionTableViewCell : UITableViewCell

@property (nonatomic, weak) UIButton *trashButton;
@property (nonatomic, weak) NSIndexPath *currentIndexPath;

@end
