//
//  IMCalcListCollectionViewCell.m
//  temp
//
//  Created by Anthony Long on 3/30/15.
//  Copyright (c) 2015 Infuse Medical. All rights reserved.
//

#import "IMCalcListCollectionViewCell.h"
#import "UIView+IMFrame.h"

@implementation IMCalcListCollectionViewCell

- (void)setText:(NSString *)text
{
	_text = [text copy];
	if(!text || text.length < 1)
	{
		self.textLabel.text = @"";
//		return;
	}
	NSRange tbRange = [text rangeOfString:@".TB"];
	self.textLabel.attributedText = nil;
	if(tbRange.location != NSNotFound)
	{
		NSMutableAttributedString *atrString = [[NSMutableAttributedString alloc] initWithString:text];
		[atrString setAttributes:@{NSFontAttributeName: [UIFont fontWithName:@"Arial-ItalicMT" size:18.0]} range:tbRange];
		self.textLabel.attributedText = atrString;
	}
	
	NSMutableAttributedString *attString = [self.textLabel.attributedText mutableCopy];
	if(!attString)
	{
		attString = [[NSMutableAttributedString alloc] initWithString:text];
	}
	__weak UIFont *superscriptFont = [UIFont fontWithName:@"Arial-ItalicMT" size:10.0];
	NSRegularExpression *superscriptRegex = [NSRegularExpression regularExpressionWithPattern:@"®" options:NSRegularExpressionCaseInsensitive error:nil];
	[superscriptRegex enumerateMatchesInString:text
									   options:0
										 range:NSMakeRange(0, [text length])
									usingBlock:^(NSTextCheckingResult *result, NSMatchingFlags flags, BOOL *stop) {
										NSMutableDictionary *atts = [[attString attributesAtIndex:result.range.location effectiveRange:NULL] mutableCopy];
										atts[NSFontAttributeName] = superscriptFont;
										atts[NSBaselineOffsetAttributeName] = @5.0;
										[attString setAttributes:atts range:result.range];
									}];
	self.textLabel.attributedText = attString;
}

- (void) setIsBottom:(BOOL)isBottom
{
	if(isBottom == _isBottom)
	{
		return;
	}
	_isBottom = isBottom;
	[self setNeedsDisplay];
}

- (void) setIsTop:(BOOL)isTop
{
	if(isTop == _isTop)
	{
		return;
	}
	
	_isTop = isTop;
	[self setNeedsDisplay];
}

- (instancetype)initWithCoder:(NSCoder *)aDecoder
{
	self = [super initWithCoder:aDecoder];
	if(self)
	{
		[self initializeDefaults];
	}
	
	return self;
}

- (instancetype)initWithFrame:(CGRect)frame
{
	self = [super initWithFrame:frame];
	if(self)
	{
		[self initializeDefaults];
	}
	
	return self;
}

- (void)prepareForInterfaceBuilder
{
	[self initializeDefaults];
}

- (void) initializeDefaults
{
	self.backgroundColor = [UIColor clearColor];
	self.layer.shadowColor = [UIColor colorWithRed:51.0f/255.0f
											 green:28.0f/255.0f
											  blue:104.0f/255.0f
											 alpha:1.0f].CGColor;
	self.layer.shadowOffset = CGSizeMake(2.0f, 1.7f);
	self.layer.shadowRadius = 2.0f;
	self.layer.shadowOpacity = 0.3f;
	self.cornerRadius = 15.0f;
	self.iconView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"icon-calc"]];
	self.iconView.frameWidth = 26.0f;
	self.iconView.frameHeight = 34.0f;
//	self.iconView.backgroundColor = [UIColor blackColor];
	[self addSubview:self.iconView];
	
	self.textLabel = [[UILabel alloc] initWithFrame:CGRectZero];
	self.textLabel.textColor = [UIColor whiteColor];
	self.textLabel.numberOfLines = 0;
	self.textLabel.lineBreakMode = NSLineBreakByWordWrapping;
	[self addSubview:self.textLabel];
	[self setNeedsLayout];
}

- (void) layoutSubviews
{
	[super layoutSubviews];
	self.iconView.frameX = 15.0f;
	self.iconView.frameY = (self.frameHeight - self.iconView.frameHeight)/2;
	self.textLabel.frameX = self.iconView.frameX + self.iconView.frameWidth + 20.0f;
	self.textLabel.frameY = 0.0f;
	self.textLabel.frameHeight = self.frameHeight;
	self.textLabel.frameWidth = self.frameWidth - 10.0f - self.textLabel.frameX;
}

- (void) drawRect:(CGRect)rect
{
	UIBezierPath *path = nil;
	if(self.isBottom && self.isTop)
	{
		path = [UIBezierPath bezierPathWithRoundedRect:rect cornerRadius:self.cornerRadius];
	}else if(self.isTop){
		path = [UIBezierPath bezierPath];
		[path moveToPoint:CGPointMake(0.0f, rect.size.height)];
		[path addLineToPoint:CGPointMake(0.0f, self.cornerRadius)];
		[path addQuadCurveToPoint:CGPointMake(self.cornerRadius, 0.0f) controlPoint:CGPointMake(0.0f, 0.0f)];
		[path addLineToPoint:CGPointMake(rect.size.width-self.cornerRadius, 0.0f)];
		[path addQuadCurveToPoint:CGPointMake(rect.size.width, self.cornerRadius) controlPoint:CGPointMake(rect.size.width, 0.0f)];
		[path addLineToPoint:CGPointMake(rect.size.width, rect.size.height)];
	}else if(self.isBottom){
		path = [UIBezierPath bezierPath];
		[path moveToPoint:CGPointMake(0.0f, 0.0f)];
		[path addLineToPoint:CGPointMake(0.0f, rect.size.height - self.cornerRadius)];
		[path addQuadCurveToPoint:CGPointMake(self.cornerRadius, rect.size.height) controlPoint:CGPointMake(0.0f, rect.size.height)];
		[path addLineToPoint:CGPointMake(rect.size.width - self.cornerRadius, rect.size.height)];
		[path addQuadCurveToPoint:CGPointMake(rect.size.width, rect.size.height - self.cornerRadius) controlPoint:CGPointMake(rect.size.width, rect.size.height)];
		[path addLineToPoint:CGPointMake(rect.size.width, 0.0f)];
		[path addLineToPoint:CGPointMake(self.textLabel.frameX, 0.0f)];
	}else{
		path = [UIBezierPath bezierPath];
		[path moveToPoint:CGPointMake(0.0f, 0.0f)];
		[path addLineToPoint:CGPointMake(0.0f, rect.size.height)];
		[path moveToPoint:CGPointMake(rect.size.width, rect.size.height)];
		[path addLineToPoint:CGPointMake(rect.size.width, 0.0f)];
		[path addLineToPoint:CGPointMake(self.textLabel.frameX, 0.0f)];
	}
	[[UIColor colorWithWhite:1.0f alpha:0.4f] setStroke];
	path.lineWidth = 2.0f;
	path.flatness = 0.1f;
	[path stroke];
}

@end