//
//  IMGraphView.m
//  Tspot Calculator
//
//  Created by Anthony Long on 6/28/14.
//  Copyright (c) 2014 Infuse Medical. All rights reserved.
//

#import "IMGraphView.h"

typedef enum {
	IMGraphLegendColorRed=0,
	IMGraphLegendColorBlue,
	IMGraphLegendColorPurple
}IMGraphLegendColor;


@interface IMGraphView ()

@property (nonatomic, strong) NSMutableArray *legendKeys;
@property (nonatomic, strong) UIImageView *ivDelta;
@property (nonatomic, strong) UILabel *labelDelta;

@end

@implementation IMGraphView
{
	NSMutableArray *_yAxisLabels;
	double _widthMultiplier;
	double _heightMultiplier;
}

static double labelLeftFrame = 0.0;

- (void) setBarStyle:(IMGraphViewBarStyle)barStyle
{
	if(_barStyle != barStyle)
	{
		_barStyle = barStyle;
		[self setNeedsLayout];
	}
}

- (void) setValuesForLeftBar:(NSArray *)valuesForLeftBar
{
	_valuesForLeftBar = valuesForLeftBar;
	self.barStyle = (IMGraphViewBarStyle)(valuesForLeftBar.count - 1);
	[self setNeedsLayout];
}

- (void) setValuesForRightBar:(NSArray *)valuesForRightBar
{
	_valuesForRightBar = valuesForRightBar;
	self.barStyle = (IMGraphViewBarStyle)(valuesForRightBar.count - 1);
	[self setNeedsLayout];
}

- (UILabel *)labelLeft
{
	if(!_labelLeft)
	{
		_labelLeft = [[UILabel alloc] initWithFrame:CGRectMake(140.0f * _widthMultiplier, self.bounds.size.height - (70.0f * _heightMultiplier), 140.0f * _widthMultiplier, 24.0f * _heightMultiplier)];
		_labelLeft.font = [UIFont fontWithName:@"Arial" size:18.0f * _heightMultiplier];
		_labelLeft.backgroundColor = [UIColor clearColor];
		_labelLeft.textColor = [UIColor blackColor];
		_labelLeft.textAlignment = NSTextAlignmentCenter;
		_labelLeft.adjustsFontSizeToFitWidth = YES;
		_labelLeft.minimumScaleFactor = 0.6;
		[self addSubview:_labelLeft];
	}
	
	return _labelLeft;
}

- (UILabel *)labelRight
{
	if(!_labelRight)
	{
		_labelRight = [[UILabel alloc] initWithFrame:CGRectMake(435.0f * _widthMultiplier, self.bounds.size.height - (70.0f * _heightMultiplier), 140.0f * _widthMultiplier, 24.0f * _heightMultiplier)];
		_labelRight.font = [UIFont fontWithName:@"Arial" size:18.0f * _heightMultiplier];
		_labelRight.backgroundColor = [UIColor clearColor];
		_labelRight.textColor = [UIColor blackColor];
		_labelRight.textAlignment = NSTextAlignmentCenter;
		[self addSubview:_labelRight];
	}
	
	return _labelRight;
}

- (UIImage *) barWithValues:(NSArray *)values
{
	double graphHeight = self.bounds.size.height - (75.0f * _heightMultiplier);
	if (self.isForPDF) {
		
		NSArray *barColors = @[
							   [UIColor colorWithRed:144.0/255.0 green:135.0/255.0 blue:210.0/255.0 alpha:1.0]
							   ,[UIColor colorWithRed:186.0/255.0 green:128.0/255.0 blue:131.0/255.0 alpha:1.0]
							   ,[UIColor colorWithRed:125.0/255.0 green:128.0/255.0 blue:131.0/255.0 alpha:1.0]
							   ,[UIColor blackColor]
							   ];
		
		UIGraphicsBeginImageContext(CGSizeMake(140.0f * _widthMultiplier, graphHeight));
		CGContextRef context = UIGraphicsGetCurrentContext();
		__block CGFloat previousY = 0.0f;
		[values enumerateObjectsUsingBlock:^(NSNumber *obj, NSUInteger idx, BOOL *stop) {
			previousY += graphHeight * obj.doubleValue;
		}];
		
		UIBezierPath *path = [UIBezierPath bezierPathWithRoundedRect:CGRectMake(0.0f, graphHeight-previousY, 140.0f * _widthMultiplier, graphHeight)
												   byRoundingCorners:UIRectCornerTopLeft | UIRectCornerTopRight
														 cornerRadii:CGSizeMake(0.0, 0.0)];
		CGContextAddPath(context, path.CGPath);
		CGContextClip(context);
		[[UIColor whiteColor] setStroke];
		previousY = 0.0f;
		[values enumerateObjectsUsingBlock:^(NSNumber *obj, NSUInteger idx, BOOL *stop) {
//			UIImage *imageToDraw = barImages[idx];
			CGFloat barY = graphHeight - previousY - (graphHeight * obj.doubleValue);
//			CGContextDrawImage(context, CGRectMake(0.0f, barY, 140.0f * _widthMultiplier, graphHeight * obj.doubleValue), imageToDraw.CGImage);
//			CGContextMoveToPoint(context, 0.0f, barY);
//			CGContextAddLineToPoint(context, 140.0f * _widthMultiplier, barY);
//			CGContextStrokePath(context);
//
			CGRect rect = CGRectMake(0.0f, barY, 140.0f * _widthMultiplier, graphHeight * obj.doubleValue);
			UIColor *colorToUse = barColors[idx];
			[colorToUse setFill];
			CGContextFillRect(context, rect);
			previousY += graphHeight * obj.doubleValue;
		}];
		
		
		UIImage *barImage = UIGraphicsGetImageFromCurrentImageContext();
		UIGraphicsEndImageContext();
		return barImage;
	}else{
	
		CGFloat strokeWidth = 5.0f * _widthMultiplier;
		CGFloat cornerRadius = (self.isForPDF?0.0f:15.0f) * _widthMultiplier;
		NSArray *barImages = @[
								[UIImage imageNamed:@"graph-bar-violet"]
							   ,[UIImage imageNamed:@"graph-bar-gray"]
							   ,[UIImage imageNamed:@"graph-bar-squared-blue"]
							   ,[UIImage imageNamed:@"graph-bar-squared-red"]
							   ];
		
		UIGraphicsBeginImageContext(CGSizeMake(140.0f * _widthMultiplier, graphHeight));
		CGContextRef context = UIGraphicsGetCurrentContext();
		
		__block CGFloat previousY = 0.0f;
		[values enumerateObjectsUsingBlock:^(NSNumber *obj, NSUInteger idx, BOOL *stop) {
			previousY += graphHeight * obj.doubleValue;
		}];
		
		UIBezierPath *path = [UIBezierPath bezierPathWithRoundedRect:CGRectMake(0.0f, graphHeight-previousY, 140.0f * _widthMultiplier, graphHeight)
												   byRoundingCorners:UIRectCornerTopLeft | UIRectCornerTopRight
														 cornerRadii:CGSizeMake(cornerRadius, cornerRadius)];
		CGContextAddPath(context, path.CGPath);
		CGContextClip(context);
		[[UIColor whiteColor] setStroke];
		CGContextSetLineWidth(context, strokeWidth);
		previousY = 0.0f;
		[values enumerateObjectsUsingBlock:^(NSNumber *obj, NSUInteger idx, BOOL *stop) {
			UIImage *imageToDraw = barImages[idx];
			CGFloat barY = graphHeight - previousY - (graphHeight * obj.doubleValue);
			CGContextDrawImage(context, CGRectMake(0.0f, barY, 140.0f * _widthMultiplier, graphHeight * obj.doubleValue), imageToDraw.CGImage);
			CGContextMoveToPoint(context, 0.0f, barY);
			CGContextAddLineToPoint(context, 140.0f * _widthMultiplier, barY);
			CGContextStrokePath(context);
			previousY += graphHeight * obj.doubleValue;
		}];
		
		UIBezierPath *pathToStroke = [UIBezierPath bezierPathWithRoundedRect:CGRectMake(0.0f, graphHeight-previousY, 140.0f * _widthMultiplier, graphHeight + (strokeWidth * 2))
														   byRoundingCorners:UIRectCornerTopRight | UIRectCornerTopLeft
																 cornerRadii:CGSizeMake(cornerRadius, cornerRadius)];
		
		
		pathToStroke.lineWidth = strokeWidth;
		[pathToStroke stroke];
		UIImage *barImage = UIGraphicsGetImageFromCurrentImageContext();
		UIGraphicsEndImageContext();
	
		return barImage;
	}
}

- (double)maxValue
{
	double val1 = 0.0;
	for(NSNumber *n in self.valuesForLeftBar)
	{
		val1 += n.doubleValue;
	}
	
	double val2 = 0.0;
	for(NSNumber *n in self.valuesForRightBar)
	{
		val2 += n.doubleValue;
	}
	
	return MAX(val1, val2);
}

- (UILabel *)labelMaxValLeft
{
	if(!_labelMaxValLeft)
	{
		_labelMaxValLeft = [[UILabel alloc] initWithFrame:CGRectMake(140.0f * _widthMultiplier, 0.0f, 140.0f * _widthMultiplier, 30.0f * _heightMultiplier)];
		_labelMaxValLeft.textColor = [UIColor colorWithRed:82.0f/255.0f
									 green:88.0f/255.0f
									  blue:134.0f/255.0f
									 alpha:1.0f];
		_labelMaxValLeft.font = [UIFont fontWithName:@"Arial" size:18.0f * _heightMultiplier];
		_labelMaxValLeft.textAlignment = NSTextAlignmentCenter;
		[self addSubview:_labelMaxValLeft];
	}
	
	return _labelMaxValLeft;
}

- (UILabel *)labelMaxValRight
{
	if(!_labelMaxValRight)
	{
		_labelMaxValRight = [[UILabel alloc] initWithFrame:CGRectMake(435.0f * _widthMultiplier, 0.0f, 140.0f * _widthMultiplier, 30.0f * _heightMultiplier)];
		_labelMaxValRight.textColor = [UIColor colorWithRed:82.0f/255.0f
													 green:88.0f/255.0f
													  blue:134.0f/255.0f
													 alpha:1.0f];
		_labelMaxValRight.font = [UIFont fontWithName:@"Arial" size:18.0f * _heightMultiplier];
		_labelMaxValRight.textAlignment = NSTextAlignmentCenter;
		[self addSubview:_labelMaxValRight];
	}
	
	return _labelMaxValRight;
}

- (void) createLabels
{
	if(!self.valuesForRightBar || !self.valuesForLeftBar)
	{
		return;
	}
	double startVal = [self maxValue];
	double d = startVal;
	
	//calculate y axis values
	int p = 0;
	while (d>10) {
		p++;
		d/=10;
	}
	
	double incBase = d > 4.5 ? 10 : 5;
	double inc = 0.0;
	if(d > 4.5)
	{
		incBase = 10.0;
		inc = pow(10, p-1)*incBase;
	}else{
		incBase = 5.0;
		inc = pow(10, p-1)*incBase;
		while ((inc*6) - startVal > (inc*1.5)) {
			incBase --;
			inc = pow(10, p-1)*incBase;
		}
	}
	
	if(inc < 1.0)
	{
		inc = 1.0;
	}
	double maxGraph = inc;
	
	if(!_yAxisLabels)
	{
		_yAxisLabels = [NSMutableArray array];
	}
	
	NSMutableArray *ma = [NSMutableArray array];
	int i=0;
//	while (maxGraph < startVal + (inc*0.5)) {
	while (maxGraph < startVal + (inc*1.5)) {
		if(i < _yAxisLabels.count)
		{
			UILabel *l = _yAxisLabels[i];
			[ma addObject:l];
		}else{
			UILabel *l = [[UILabel alloc] initWithFrame:CGRectMake(0.0f, 0.0f, 85.0f * _widthMultiplier, 20.0f * _widthMultiplier)];
			l.font = [UIFont fontWithName:@"Arial" size:11.0f * _widthMultiplier];
			l.textColor = [UIColor colorWithRed:82.0f/255.0f
										  green:88.0f/255.0f
										   blue:134.0f/255.0f
										  alpha:1.0f];
			l.textAlignment = NSTextAlignmentRight;
			[self addSubview:l];
			[_yAxisLabels addObject:l];
			[ma addObject:l];
		}
		maxGraph += inc;
		i++;
	}
	
	maxGraph -= inc;
	
	
	// remove any extra labels from view heirarchy but save for if needed later
	for(i=i;i<_yAxisLabels.count;i++)
	{
		UILabel *l = _yAxisLabels[i];
		if(l.superview)
		{
			[l removeFromSuperview];
		}
		
	}
	
	double pixelInc = (self.bounds.size.height - (76.0f * _heightMultiplier))/ma.count;
	
	NSNumber *largest = [NSNumber numberWithDouble:maxGraph];
	NSString *s = self.isCurrencyFormat?largest.intCurrencyValue:largest.intStringValue;
	CGSize largestSize = [s sizeWithAttributes:@{NSFontAttributeName:[UIFont fontWithName:@"Arial" size:11.0f * _widthMultiplier]}];
	
	for(int i=0;i<ma.count;i++)
	{
		UILabel *l = ma[i];
		l.frame = ({
			CGRect rect = l.frame;
			rect.size.width = largestSize.width + (3.0f * _widthMultiplier);
			rect.origin.y = (self.bounds.size.height - (76.0f * _widthMultiplier)) - (pixelInc * (i+1));
			rect;
		});
		l.text = self.isCurrencyFormat?[NSNumber numberWithDouble:(i+1)*inc].intCurrencyValue:[NSNumber numberWithDouble:(i+1)*inc].intStringValue;
	}
	
	[self createBarsWithMaxVal:maxGraph];
}

- (UIImage *)roundTheCornersOfThisBar:(UIImage *)image
{
	CGRect imageRect = CGRectMake(0.0f, 0.0f, 140.0f * _widthMultiplier, 15.0f * _heightMultiplier);
	UIBezierPath *path = [UIBezierPath bezierPathWithRoundedRect:imageRect byRoundingCorners:UIRectCornerTopLeft | UIRectCornerTopRight cornerRadii:CGSizeMake(14.0f * _widthMultiplier, 14.0f * _widthMultiplier)];
	UIGraphicsBeginImageContext(imageRect.size);
	CGContextRef context = UIGraphicsGetCurrentContext();
	CGContextDrawImage(context, CGRectMake(0.0f, 0.0f, imageRect.size.width, imageRect.size.height), image.CGImage);
	CGContextAddPath(context, path.CGPath);
	CGContextClip(context);
	UIImage *roundedImage = UIGraphicsGetImageFromCurrentImageContext();
	UIGraphicsEndImageContext();
	roundedImage  = [roundedImage resizableImageWithCapInsets:UIEdgeInsetsMake(14.0f * _widthMultiplier, 0.0f, 1.0f, 0.0f)];
	return roundedImage;
}

- (void) createBarsWithMaxVal:(double)maxGraph
{
	int i=0;
	double prevVal = 0.0;
	double graphHeight = self.bounds.size.height - (75.0f * _heightMultiplier);
	
	self.leftBar.frame = CGRectMake(140.0f * _widthMultiplier, 0.0f, 140.0f * _widthMultiplier, graphHeight);
	self.rightBar.frame = CGRectMake(435.0f * _widthMultiplier, 0.0f, 140.0f * _widthMultiplier, graphHeight);
	if(self.barStyle == IMGraphViewBarStyleQuad)
	{
		self.leftBar.image = [self barWithValues:@[
												   [NSNumber numberWithDouble:[self.valuesForLeftBar[0] doubleValue]/maxGraph]
												   ,[NSNumber numberWithDouble:[self.valuesForLeftBar[1] doubleValue]/maxGraph]
												   ,[NSNumber numberWithDouble:[self.valuesForLeftBar[2] doubleValue]/maxGraph]
												   ,[NSNumber numberWithDouble:[self.valuesForLeftBar[3] doubleValue]/maxGraph]
												   ]];
		
		self.rightBar.image = [self barWithValues:@[
												   [NSNumber numberWithDouble:[self.valuesForRightBar[0] doubleValue]/maxGraph]
												   ,[NSNumber numberWithDouble:[self.valuesForRightBar[1] doubleValue]/maxGraph]
												   ,[NSNumber numberWithDouble:[self.valuesForRightBar[2] doubleValue]/maxGraph]
												   ,[NSNumber numberWithDouble:[self.valuesForRightBar[3] doubleValue]/maxGraph]
												   ]];
	}else if(self.barStyle == IMGraphViewBarStyleTriple){
		self.leftBar.image = [self barWithValues:@[
												   [NSNumber numberWithDouble:[self.valuesForLeftBar[0] doubleValue]/maxGraph]
												   ,@0.0
												   ,[NSNumber numberWithDouble:[self.valuesForLeftBar[1] doubleValue]/maxGraph]
												   ,[NSNumber numberWithDouble:[self.valuesForLeftBar[2] doubleValue]/maxGraph]
												   ]];
		
		self.rightBar.image = [self barWithValues:@[
												   [NSNumber numberWithDouble:[self.valuesForRightBar[0] doubleValue]/maxGraph]
												   ,@0.0
												   ,[NSNumber numberWithDouble:[self.valuesForRightBar[1] doubleValue]/maxGraph]
												   ,[NSNumber numberWithDouble:[self.valuesForRightBar[2] doubleValue]/maxGraph]
												   ]];
	}else if(self.barStyle == IMGraphViewBarStyleDouble){
		self.leftBar.image = [self barWithValues:@[
												   [NSNumber numberWithDouble:[self.valuesForLeftBar[0] doubleValue]/maxGraph]
												   ,@0.0
												   ,@0.0
												   ,[NSNumber numberWithDouble:[self.valuesForLeftBar[1] doubleValue]/maxGraph]
												   ]];
		
		self.rightBar.image = [self barWithValues:@[
												   [NSNumber numberWithDouble:[self.valuesForRightBar[0] doubleValue]/maxGraph]
												   ,@0.0
												   ,@0.0
												   ,[NSNumber numberWithDouble:[self.valuesForRightBar[1] doubleValue]/maxGraph]
												   ]];
	}else if(self.barStyle == IMGraphViewBarStyleSingle){
		self.leftBar.image = [self barWithValues:@[
												   [NSNumber numberWithDouble:[self.valuesForLeftBar[0] doubleValue]/maxGraph]
												   ,@0.0
												   ,@0.0
												   ,@0.0
												   ]];
		
		self.rightBar.image = [self barWithValues:@[
												   [NSNumber numberWithDouble:[self.valuesForRightBar[0] doubleValue]/maxGraph]
												   ,@0.0
												   ,@0.0
												   ,@0.0
												   ]];
	}
	
	for(NSNumber *val in self.valuesForLeftBar)
	{
		prevVal += val.doubleValue;
		i++;
	}
	self.labelMaxValLeft.frame = ({
		CGRect rect = self.labelMaxValLeft.frame;
		rect.origin.y = graphHeight - ((prevVal/maxGraph) * graphHeight) - (30.0f * _heightMultiplier);
		rect;
	});
	self.labelMaxValLeft.text = self.isCurrencyFormat?[NSNumber numberWithDouble:prevVal].intCurrencyValue:[NSNumber numberWithDouble:prevVal].intStringValue;
	double leftVal = prevVal;
	
	prevVal = 0.0f;
	i=0;
	for(NSNumber *val in self.valuesForRightBar)
	{
		prevVal += val.doubleValue;
		i++;
	}
	double rightVal = prevVal;
	self.labelMaxValRight.frame = ({
		CGRect rect = self.labelMaxValRight.frame;
		rect.origin.y = graphHeight - ((prevVal/maxGraph) * graphHeight) - (30.0f * _heightMultiplier);
		rect;
	});
	self.labelMaxValRight.text = self.isCurrencyFormat?[NSNumber numberWithDouble:prevVal].intCurrencyValue:[NSNumber numberWithDouble:prevVal].intStringValue;
	
	self.labelDelta.text = [[NSNumber numberWithDouble:1.0 - (rightVal/leftVal)] percentStringValue];
	
	if(self.textForLeft)
	{
		self.labelLeft.text = self.textForLeft;
	}
	
	if(self.textForRight)
	{
		self.labelRight.text = self.textForRight;
	}
}

- (void) createLegend
{
	if(!self.legendKeys)
	{
		self.legendKeys = [NSMutableArray array];
		if(self.legendRed)
		{
			[self.legendKeys addObject:[self legendKeyWithText:self.legendRed andColor:IMGraphLegendColorPurple]];
		}
		if(self.legendBlue)
		{
			[self.legendKeys addObject:[self legendKeyWithText:self.legendBlue andColor:IMGraphLegendColorBlue]];
		}
		if(self.legendPurple)
		{
			[self.legendKeys addObject:[self legendKeyWithText:self.legendPurple andColor:IMGraphLegendColorRed]];
		}
	}
	
	CGFloat totalWidth = 0.0;
	for(UIView *v in self.legendKeys)
	{
		totalWidth += v.frame.size.width+(40.0 * _widthMultiplier);
	}
	
	CGFloat beginning = (20.0f * _widthMultiplier) + ((self.frame.size.width - totalWidth)/2.0);
	for(UIView *v in self.legendKeys)
	{
		CGRect rect = v.frame;
		rect.origin.x = beginning;
		rect.origin.y = self.frame.size.height - (22.0f * _heightMultiplier);
		v.frame = rect;
		beginning += rect.size.width + (40.0f * _widthMultiplier);
		[self addSubview:v];
	}
}

- (UIView *) legendKeyWithText:(NSString *)text andColor:(IMGraphLegendColor)legendColor
{
	NSString *imageName = legendColor == IMGraphLegendColorRed ? @"legend-key-red.png" : legendColor == IMGraphLegendColorBlue ? @"legend-key-blue.png" : @"legend-key-purple";

//	NSArray *barColors = @[
//						   [UIColor colorWithRed:144.0/255.0 green:135.0/255.0 blue:210.0/255.0 alpha:1.0]
//						   ,[UIColor colorWithRed:186.0/255.0 green:128.0/255.0 blue:131.0/255.0 alpha:1.0]
//						   ,[UIColor colorWithRed:125.0/255.0 green:128.0/255.0 blue:131.0/255.0 alpha:1.0]
//						   ,[UIColor blackColor]
//						   ];
	
	UIImageView *iv = [[UIImageView alloc] initWithImage:[UIImage imageNamed:imageName]];
	CGRect ivFrame = iv.frame;
	ivFrame.size.width *= _widthMultiplier;
	ivFrame.size.height *= _heightMultiplier;
	iv.frame = ivFrame;
	if(self.isForPDF) {
		UIColor *bgColor;
		if (legendColor == IMGraphLegendColorRed) {
			bgColor = [UIColor blackColor];
		}else if(legendColor == IMGraphLegendColorBlue){
			bgColor = [UIColor colorWithRed:125.0/255.0 green:128.0/255.0 blue:131.0/255.0 alpha:1.0];
		}else{
			bgColor = [UIColor colorWithRed:144.0/255.0 green:135.0/255.0 blue:210.0/255.0 alpha:1.0];
		}
		iv.backgroundColor = bgColor;
		iv.image = nil;
	}
	
	self.ivDelta.contentMode = UIViewContentModeScaleToFill;
	
	UILabel *l = [[UILabel alloc] initWithFrame:CGRectZero];
	l.font = [UIFont fontWithName:@"Arial" size:11.0 * _widthMultiplier];
	l.textColor = [UIColor colorWithRed:82.0f/255.0f
								  green:88.0f/255.0f
								   blue:134.0f/255.0f
								  alpha:1.0f];
	l.text = text;
	CGRect rect = [text boundingRectWithSize:CGSizeMake(400.0f * _widthMultiplier, 16.0f * _heightMultiplier)
									 options:NSStringDrawingUsesLineFragmentOrigin
								  attributes:@{NSFontAttributeName:l.font}
									 context:nil];
	l.frame = CGRectMake(32.0f * _widthMultiplier, 6.0f * _heightMultiplier, rect.size.width, rect.size.height);
	UIView *v = [[UIView alloc] initWithFrame:CGRectMake(0.0f, 0.0f, CGRectGetMaxX(l.frame), iv.frame.size.height)];
	[v addSubview:iv];
	[v addSubview:l];
	return v;
}

- (CGRect)scaledRect:(double)x :(double)y :(double)width :(double)height
{
	return CGRectMake(x * _widthMultiplier, y * _heightMultiplier, width * _widthMultiplier, height * _heightMultiplier);
}

- (void) initInstance
{
	_widthMultiplier = self.bounds.size.width / 711.0;
	_heightMultiplier = self.bounds.size.height / 455.0;
	self.barStyle = IMGraphViewBarStyleTriple;
	self.backgroundColor = [UIColor clearColor];
	self.ivDelta = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"delta.png"]];
	self.ivDelta.contentMode = UIViewContentModeScaleToFill;
	
//	self.labelDelta = [[UILabel alloc] initWithFrame:CGRectMake(304.0f, 182.0f, self.ivDelta.frame.size.width, 22.0f)];
	self.labelDelta = [[UILabel alloc] initWithFrame:[self scaledRect:304.0f :182.0f :self.ivDelta.frame.size.width :22.0f]];
	self.labelDelta.font = [UIFont fontWithName:@"Arial" size:18.0f * _widthMultiplier];
	self.labelDelta.textColor = [UIColor colorWithRed:42.0f/255.0f
												green:39.0f/255.0f
												 blue:96.0f/255.0f
												alpha:1.0f];
	self.labelDelta.textAlignment = NSTextAlignmentCenter;
	self.leftBar = [[UIImageView alloc] initWithFrame:CGRectZero];
	self.rightBar = [[UIImageView alloc] initWithFrame:CGRectZero];
	[self addSubview:self.leftBar];
	[self addSubview:self.rightBar];
	if (!self.hideDelta) {
		[self addSubview:self.ivDelta];
		[self addSubview:self.labelDelta];
	}
}

#pragma mark - UIView

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        [self initInstance];
    }
    return self;
}

- (id)initWithCoder:(NSCoder *)aDecoder
{
	self = [super initWithCoder:aDecoder];
	if(self)
	{
		[self initInstance];
	}
	return self;
}


- (void) layoutSubviews
{
	[super layoutSubviews];
	
	[self createLabels];
	[self createLegend];
	CGRect deltaFrame = self.ivDelta.frame;
	deltaFrame.size.width = 94.0 * _widthMultiplier;
	deltaFrame.size.height = 52.0 * _widthMultiplier;
	self.ivDelta.frame = deltaFrame;
	self.ivDelta.center = CGPointMake(CGRectGetMidX(self.bounds), CGRectGetMidY(self.bounds));
	self.ivDelta.hidden = self.hideDelta;
	self.labelDelta.hidden = self.hideDelta;
	
	self.labelDelta.center = CGPointMake(self.ivDelta.center.x - (5*_widthMultiplier), self.ivDelta.center.y - (9.0f * _widthMultiplier));
	
	NSRange tbRange = [self.labelRight.text rangeOfString:@".TB"];
	if(tbRange.location != NSNotFound)
	{
		NSMutableAttributedString *atrString = [[NSMutableAttributedString alloc] initWithString:self.labelRight.text];
		[atrString setAttributes:@{NSFontAttributeName: [UIFont fontWithName:@"Arial-ItalicMT" size:(18.0 * _heightMultiplier)]} range:tbRange];
		self.labelRight.attributedText = atrString;
	}
	
	NSMutableAttributedString *attString = [self.labelRight.attributedText mutableCopy];
	__weak UIFont *superscriptFont = [UIFont fontWithName:@"Arial-ItalicMT" size:14.0 * _heightMultiplier];
	NSRegularExpression *superscriptRegex = [NSRegularExpression regularExpressionWithPattern:@"®" options:NSRegularExpressionCaseInsensitive error:nil];
	[superscriptRegex enumerateMatchesInString:self.labelRight.text
									   options:0
										 range:NSMakeRange(0, [self.labelRight.text length])
									usingBlock:^(NSTextCheckingResult *result, NSMatchingFlags flags, BOOL *stop) {
										NSMutableDictionary *atts = [[attString attributesAtIndex:result.range.location effectiveRange:NULL] mutableCopy];
										atts[NSFontAttributeName] = superscriptFont;
										atts[NSBaselineOffsetAttributeName] = @4.0;
										[attString setAttributes:atts range:result.range];
									}];
	self.labelRight.attributedText = attString;
}

// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect
{
    UIBezierPath *bp = [[UIBezierPath alloc] init];
	[[UIColor colorWithRed:146.0f/255.0f
					 green:151.0f/255.0f
					  blue:203.0f/255.0f
					 alpha:1.0f] setStroke];
	[bp setLineWidth:2.0f * _heightMultiplier];
	[bp moveToPoint:CGPointMake(0.0f, rect.size.height - (74.0f * _heightMultiplier))];
	[bp addLineToPoint:CGPointMake(rect.size.width, rect.size.height - (74.0f * _heightMultiplier))];
	[bp stroke];
}

@end
