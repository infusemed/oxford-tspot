//
//  IMSpreadSheetTextField.m
//  Practice Productivity Analysis
//
//  Created by Anthony Long on 9/6/13.
//  Copyright (c) 2013 Infuse Medical. All rights reserved.
//

#import "IMSpreadSheetTextField.h"

@implementation IMSpreadSheetTextField

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        
    }
    return self;
}

- (CGRect) textRectForBounds:(CGRect)bounds
{
	return CGRectInset(bounds, 10.0f, 0.0f);
}

- (CGRect) editingRectForBounds:(CGRect)bounds
{
	return [self textRectForBounds:bounds];
}

- (void) setIntFormatType:(NSInteger)intFormatType
{
	_intFormatType = intFormatType;
	self.formatType = [NSNumber numberWithInteger:intFormatType];
}

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect
{
    // Drawing code
}
*/

@end
