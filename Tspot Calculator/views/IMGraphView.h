//
//  IMGraphView.h
//  Tspot Calculator
//
//  Created by Anthony Long on 6/28/14.
//  Copyright (c) 2014 Infuse Medical. All rights reserved.
//

#import <UIKit/UIKit.h>

typedef enum {
	IMGraphViewBarStyleSingle=0,
	IMGraphViewBarStyleDouble,
	IMGraphViewBarStyleTriple,
	IMGraphViewBarStyleQuad
}IMGraphViewBarStyle;

//IB_DESIGNABLE

@interface IMGraphView : UIView

@property (nonatomic, assign) IBInspectable BOOL isForPDF;
@property (nonatomic, assign) IBInspectable BOOL hideDelta;

@property (nonatomic, strong) UILabel *labelLeft;
@property (nonatomic, strong) UILabel *labelRight;

@property (nonatomic, strong) UIImageView *leftBar;
@property (nonatomic, strong) UIImageView *rightBar;
@property (nonatomic, strong) IBInspectable NSString *textForLeft;
@property (nonatomic, strong) IBInspectable NSString *textForRight;
@property (nonatomic, strong) UILabel *labelMaxValLeft;
@property (nonatomic, strong) UILabel *labelMaxValRight;

@property (nonatomic, strong) NSArray *valuesForLeftBar;
@property (nonatomic, strong) NSArray *valuesForRightBar;

@property (nonatomic, assign) IBInspectable BOOL isCurrencyFormat;
@property (nonatomic, assign) IMGraphViewBarStyle barStyle;

@property (nonatomic, strong) IBInspectable  NSString *cellIdsForLeftBar;
@property (nonatomic, strong) IBInspectable  NSString *cellIdsForRightBar;
@property (nonatomic, strong) IBInspectable  NSString *legendRed;
@property (nonatomic, strong) IBInspectable  NSString *legendBlue;
@property (nonatomic, strong) IBInspectable  NSString *legendPurple;

@end
