//
//  IMCalculatorSelectionTableViewCell.m
//  Tspot Calculator
//
//  Created by Anthony Long on 6/23/14.
//  Copyright (c) 2014 Infuse Medical. All rights reserved.
//

#import "IMCalculatorSelectionTableViewCell.h"

@implementation IMCalculatorSelectionTableViewCell
{
	UIImageView *_ivCell;
	UIImageView *_ivHighlight;
}

- (void) initInstance
{
	self.backgroundColor = [UIColor clearColor];
	_ivHighlight = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"selection-selected.png"]];
	_ivHighlight.contentMode = UIViewContentModeCenter;
	[self addSubview:_ivHighlight];
	_ivHighlight.hidden = YES;
	_ivCell = [[UIImageView alloc] init];
	_ivCell.contentMode = UIViewContentModeCenter;
	[self addSubview:_ivCell];
	
	self.selectionStyle = UITableViewCellSelectionStyleNone;
	self.textLabel.font = [UIFont fontWithName:@"Arial" size:18];
	self.textLabel.textColor = [UIColor whiteColor];
	self.textLabel.numberOfLines = 2;
}

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        [self initInstance];
    }
    return self;
}

- (void) setPosition:(IMCalcSelectionPosition )position
{
	_position = position;
	if(position == IMCalcSelectionPositionStandard)
	{
		_ivCell.image = [UIImage imageNamed:@"calc-list-cell.png"];
	}else if(position == IMCalcSelectionPositionBottom){
		_ivCell.image = [UIImage imageNamed:@"calc-list-cell-bottom.png"];
	}else{
		_ivCell.image = [UIImage imageNamed:@"calc-list-cell-top.png"];
	}
}

- (void)awakeFromNib
{
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];

    _ivHighlight.hidden = !selected;
}

- (void) layoutSubviews
{
	[super layoutSubviews];
	_ivHighlight.frame = self.bounds;
	_ivCell.frame = self.bounds;
	self.textLabel.frame = ({
		CGRect rect = self.textLabel.frame;
		rect.size.width -= 88.0f;
		rect.origin.x = 88.0f;
		rect;
	});
}

@end
