//
//  IMCalculatorSelectionTableViewCell.h
//  Tspot Calculator
//
//  Created by Anthony Long on 6/23/14.
//  Copyright (c) 2014 Infuse Medical. All rights reserved.
//

#import <UIKit/UIKit.h>

typedef enum {
	IMCalcSelectionPositionTop,
	IMCalcSelectionPositionBottom,
	IMCalcSelectionPositionStandard
} IMCalcSelectionPosition;

@interface IMCalculatorSelectionTableViewCell : UITableViewCell

@property (nonatomic, assign) IMCalcSelectionPosition position;

@end
