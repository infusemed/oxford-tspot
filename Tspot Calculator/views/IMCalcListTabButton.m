//
//  IMCalcListTabButton.m
//  Tspot Calculator
//
//  Created by Anthony Long on 4/1/15.
//  Copyright (c) 2015 Infuse Medical. All rights reserved.
//

#import "IMCalcListTabButton.h"
#import "UIView+IMFrame.h"

@implementation IMCalcListTabButton



- (void) layoutSubviews
{
	[super layoutSubviews];
	self.imageView.frameX = (self.frameWidth - self.imageView.frameWidth + 8.0f)/2;
	self.imageView.frameY = 10.0f;
	self.titleLabel.numberOfLines = 2;
	self.titleLabel.lineBreakMode = NSLineBreakByWordWrapping;
	self.titleLabel.textAlignment = NSTextAlignmentCenter;
	CGFloat titleY = self.imageView.frameY + self.imageView.frameHeight - 5.0f;
	self.titleLabel.frame = CGRectMake(5.0f, titleY, self.frameWidth - 5.0, self.frameHeight - titleY);
}

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

@end
