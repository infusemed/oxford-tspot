//
//  IMCalcListCollectionViewCell.h
//  temp
//
//  Created by Anthony Long on 3/30/15.
//  Copyright (c) 2015 Infuse Medical. All rights reserved.
//

#import <UIKit/UIKit.h>

IB_DESIGNABLE

@interface IMCalcListCollectionViewCell : UICollectionViewCell

@property (nonatomic) IBInspectable CGFloat cornerRadius;
@property (nonatomic) IBInspectable BOOL isTop;
@property (nonatomic) IBInspectable BOOL isBottom;

@property (nonatomic, copy) IBInspectable NSString *text;
@property (nonatomic, strong) UIImageView *iconView;
@property (nonatomic, strong) UILabel *textLabel;

@end
