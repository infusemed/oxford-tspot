//
//  IMSessionTableViewCell.m
//  Tspot Calculator
//
//  Created by Anthony Long on 6/25/14.
//  Copyright (c) 2014 Infuse Medical. All rights reserved.
//

#import "IMSessionTableViewCell.h"

@implementation IMSessionTableViewCell
{
	UIImageView *_ivSelected;
}


- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
		_ivSelected = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"selection-selected.png"]];
		_ivSelected.hidden = YES;
		_ivSelected.alpha = 0.4;
		[self addSubview:_ivSelected];
		self.backgroundColor = [UIColor clearColor];
        self.textLabel.font = [UIFont fontWithName:@"Arial" size:15.0];
		self.textLabel.textColor = [UIColor colorWithRed:82.0f/255.0f
												   green:88.0f/255.0f
													blue:134.0f/255.0f
												   alpha:1.0f];
		
		self.trashButton = [UIButton buttonWithType:UIButtonTypeCustom];
		[self.trashButton setBackgroundImage:[UIImage imageNamed:@"icon-trash.png"] forState:UIControlStateNormal];
		[self addSubview:self.trashButton];
		self.selectionStyle = UITableViewCellSelectionStyleNone;
    }
    return self;
}


- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];

    _ivSelected.hidden = !selected;
}

- (void) layoutSubviews
{
	[super layoutSubviews];
	
	self.trashButton.frame = CGRectMake(self.bounds.size.width-20.0f, 12.0f, 18.0, 28.0f);
	_ivSelected.frame = self.bounds;
}

@end
