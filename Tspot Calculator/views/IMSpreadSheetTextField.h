//
//  IMSpreadSheetTextField.h
//  Practice Productivity Analysis
//
//  Created by Anthony Long on 9/6/13.
//  Copyright (c) 2013 Infuse Medical. All rights reserved.
//

#import <UIKit/UIKit.h>

IB_DESIGNABLE

@interface IMSpreadSheetTextField : UITextField

@property (nonatomic, strong) IBInspectable NSString *cellId;
@property (nonatomic, strong) IBInspectable NSNumber *formatType;
@property (nonatomic) NSInteger intFormatType;

@end