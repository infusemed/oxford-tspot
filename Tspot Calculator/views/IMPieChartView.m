//
//  IMPieChartView.m
//  Tspot Calculator
//
//  Created by Anthony Long on 4/20/15.
//  Copyright (c) 2015 Infuse Medical. All rights reserved.
//

#import "IMPieChartView.h"

@implementation IMPieChartView
{
	
}

- (double) totalValue
{
	return self.value1 + self.value2 + self.value3 + self.value4 + self.value5;
}

- (void) setValues:(NSArray *)values
{
	[self setNeedsDisplay];
}


- (void)drawRect:(CGRect)rect {
	static CGFloat radius = 195.0f;
	CGPoint centerPoint = CGPointMake(CGRectGetMidX(rect), CGRectGetMidY(rect));
	[[UIColor colorWithRed:213.0f/255.0f
					green:31.0f/255.0f
					 blue:42.0f/255.0f
					 alpha:1.0f] setFill];
	CGFloat startAngle = -M_PI_2;
	CGFloat endAngle = startAngle + (M_PI * 2.0) * (self.value1/[self totalValue]);
	UIBezierPath *path = [UIBezierPath bezierPathWithArcCenter:centerPoint radius:radius startAngle:startAngle endAngle:endAngle clockwise:YES];
	[path addLineToPoint:centerPoint];
	[path fill];
	startAngle = endAngle;
	endAngle += (M_PI * 2.0) * (self.value2/[self totalValue]);
	
	[[UIColor colorWithRed:37.0f/255.0f
					 green:144.0f/255.0f
					  blue:252.0f/255.0f
					 alpha:1.0f] setFill];
	[path removeAllPoints];
	[path addArcWithCenter:centerPoint radius:radius startAngle:startAngle endAngle:endAngle clockwise:YES];
	[path addLineToPoint:centerPoint];
	[path fill];
	
	
	startAngle = endAngle;
	endAngle += (M_PI * 2.0) * (self.value3/[self totalValue]);
	
	[[UIColor colorWithRed:184.0f/255.0f
					 green:185.0f/255.0f
					  blue:211.0f/255.0f
					 alpha:1.0f] setFill];
	[path removeAllPoints];
	[path addArcWithCenter:centerPoint radius:radius startAngle:startAngle endAngle:endAngle clockwise:YES];
	[path addLineToPoint:centerPoint];
	[path fill];
	
	startAngle = endAngle;
	endAngle += (M_PI * 2.0) * (self.value4/[self totalValue]);
	
	[[UIColor colorWithRed:89.0f/255.0f
					 green:89.0f/255.0f
					  blue:160.0f/255.0f
					 alpha:1.0f] setFill];
	[path removeAllPoints];
	[path addArcWithCenter:centerPoint radius:radius startAngle:startAngle endAngle:endAngle clockwise:YES];
	[path addLineToPoint:centerPoint];
	[path fill];
	
	startAngle = endAngle;
	endAngle += (M_PI * 2.0) * (self.value5/[self totalValue]);
	
	[[UIColor colorWithRed:122.0f/255.0f
					 green:122.0f/255.0f
					  blue:179.0f/255.0f
					 alpha:1.0f] setFill];
	[path removeAllPoints];
	[path addArcWithCenter:centerPoint radius:radius startAngle:startAngle endAngle:endAngle clockwise:YES];
	[path addLineToPoint:centerPoint];
	[path fill];
}


@end
