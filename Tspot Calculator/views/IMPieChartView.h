//
//  IMPieChartView.h
//  Tspot Calculator
//
//  Created by Anthony Long on 4/20/15.
//  Copyright (c) 2015 Infuse Medical. All rights reserved.
//

#import <UIKit/UIKit.h>

//IB_DESIGNABLE

@interface IMPieChartView : UIView

@property (nonatomic) double value1;
@property (nonatomic) double value2;
@property (nonatomic) double value3;
@property (nonatomic) double value4;
@property (nonatomic) double value5;
@property (nonatomic) NSArray *values;

@property (nonatomic, copy) IBInspectable NSString *cellIds;

@property (nonatomic, copy) IBInspectable NSString *legend1;
@property (nonatomic, copy) IBInspectable NSString *legend2;
@property (nonatomic, copy) IBInspectable NSString *legend3;
@property (nonatomic, copy) IBInspectable NSString *legend4;
@property (nonatomic, copy) IBInspectable NSString *legend5;

@property (nonatomic, strong) IBOutlet UILabel *titleLabel;

@end
